<?php
	if (@$is_included && !empty($restaurantAdmin)) {
?>
	<div class="main-content clearfix">
		<?php require_once 'inc/logged-restaurant-header.inc.php'; ?>
 		<div id="restaurant-gallery-body">
			<div class="white-container" id="gallery-banner">
				<a class="green-button" data-related="edit-gallery-banner" href="javascript:;"><?php echo EDIT; ?></a>
				<strong class="title"><?php echo RESTAURANT_BANNER; ?></strong>

				<?php if ( $banner ) { ?>
				<div class="image-previewer" style="background-image: url('<?php echo IMG . 'restaurants/' . $banner->file ?>');">
					<a class="delete-image" href="javascript:;" data-id="<?php echo ($banner)?$banner->idImage:-1 ?>">&times;</a>
				</div>
				<?php } else { ?>
				<div>No Image</div>
				<?php } ?>
			</div>
			<form id="gallery-banner-form" method="post" enctype="multipart/form-data">
				<div class="white-container" id="edit-gallery-banner">
					<input type="file" name="res_banner" />
					<input type="hidden" name="res_banner_id" value="<?php echo ($banner)?$banner->idImage:-1 ?>" />

					<div class="controls">
						<a class="disabled rounded-red-button" data-related="gallery-banner"><?php echo CANCEL; ?></a>
						<input class="rounded-red-button" type="submit" value="Apply" />
					</div>
				</div>
			</form>

			<div class="white-container" id="gallery-thumbnail">
				<a class="green-button" data-related="edit-gallery-thumbnail" href="javascript:;"><?php echo EDIT; ?></a>
				<strong class="title"><?php echo RESTAURANT_THUMBNAIL; ?></strong>

				<?php if ( $thumbnail ) { ?>
				<div class="image-previewer" style="background-image: url('<?php echo IMG . 'restaurants/' . $thumbnail->file ?>');">
					<a class="delete-image" href="javascript:;" data-id="<?php echo ($thumbnail)?$thumbnail->idImage:-1 ?>">&times;</a>
				</div>
				<?php } else { ?>
				<div>No Image</div>
				<?php } ?>
			</div>
			<form id="gallery-thumbnail-form" method="post" enctype="multipart/form-data">
				<div class="white-container" id="edit-gallery-thumbnail">
					<input type="file" name="res_thumbnail" />
					<input type="hidden" name="res_thumbnail_id" value="<?php echo ($thumbnail)?$thumbnail->idImage:-1 ?>" />

					<div class="controls">
						<a class="disabled rounded-red-button" data-related="edit-gallery-thumbnail"><?php echo CANCEL; ?></a>
						<input class="rounded-red-button" type="submit" value="Apply" />
					</div>
				</div>
			</form>

			<div class="white-container" id="gallery-gallery">
				<a class="green-button" data-related="edit-gallery-gallery" href="javascript:;"><?php echo ADD; ?></a>
				<strong class="title"><?php echo RESTAURANT_GALLERY; ?></strong>

				<div class="image-previewer-container">
					<?php if ( sizeof( $gallery ) ) { ?>
					<?php foreach( $gallery as $gg ) { ?>
					<div class="image-previewer" style="background-image: url('<?php echo IMG . 'restaurants/' . $gg->file ?>');">
						<a class="delete-image" href="javascript:;" data-id="<?php echo ($gg)?$gg->idImage:0 ?>">&times;</a>
					</div>
					<?php } ?>
					<?php } else { ?>
					<div>No Image</div>
					<?php } ?>
				</div>
			</div>
			<form id="gallery-gallery-form" method="post" enctype="multipart/form-data">
				<div class="white-container" id="edit-gallery-gallery">
					<input type="file" name="res_gallery" />
					<input type="hidden" name="res_gallery_id" value="0" />

					<div class="controls">
						<a class="disabled rounded-red-button" data-related="edit-gallery-gallery"><?php echo CANCEL; ?></a>
						<input class="rounded-red-button" type="submit" value="Apply" />
					</div>
				</div>
			</form>

		</div>
	</div>
<?php
	}
