<?php
	if (@$is_included && !empty($restaurantAdmin)) {
		echo '<div class="main-content">';
		require_once 'inc/logged-restaurant-header.inc.php';
?>
				<div id="financial-details-body">
						<!--h1><?php echo FINANCIAL_INFO; ?></h1-->

						<div class="white-container" id="entity-finance">
							<a class="green-button" data-related="edit-entity-finance" href="javascript:;"><?php echo EDIT; ?></a>
							
							<strong class="title"><?php echo FINANCIAL_INFO; ?></strong>
							<span id="entity-company"><?php echo htmlspecialchars($restaurantAdmin->restaurant->bankAccountName); ?></span><br />
							<span id="entity-iban"><?php echo htmlspecialchars($restaurantAdmin->restaurant->iban); ?></span><br />
							<span id="entity-bank"><?php echo htmlspecialchars($restaurantAdmin->restaurant->bankName); ?></span><br />
							<span id="entity-swift"><?php echo htmlspecialchars($restaurantAdmin->restaurant->swiftCode); ?></span>
						</div>
						<form id="entity-finance-form" method="post">
							<div class="white-container" id="edit-entity-finance">
								<strong class="title"><?php echo EDIT_DETAILS; ?></strong>
								
								<label class="half"><span><?php echo BUSINESS_NAME; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="company" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->bankAccountName); ?>" type="text" /></label>
								<label class="half right"><span><?php echo IBAN; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="iban" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->iban); ?>" type="text" /></label>
								<label class="clear half"><span><?php echo BANK; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="bank" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->bankName); ?>" type="text" /></label>
								<label class="half right"><span><?php echo SWIFT; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="swift" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->swiftCode); ?>" type="text" /></label>
								
								<a class="clear disabled rounded-red-button" data-related="entity-finance"><?php echo CANCEL; ?></a><a class="rounded-red-button"><?php echo APPLY; ?></a>
							</div>
						</form>
						
						<div class="disabled-container white-container">
							<strong class="title"><?php echo COMMISSION_AGREEMENT; ?></strong>
							<span><?php echo str_replace('{$percentage}', round($restaurantAdmin->restaurant->commission, 1), COMMISSION_PERCENTAGE); ?></span><br />
							<span><?php echo COMMISSION_PAYMENT; ?></span><br />
							<br />
							<br />
							<span><?php echo THANK_YOU; ?></span><br />
							<span><?php echo QUESTIONS_EMAIL; ?></span>
						</div>
					</div>
		</div>
<?php
	}
