<?php
	if (@$is_included && !empty($entity)) {
		echo '<div class="main-content">';
		require_once 'inc/logged-user-header.inc.php';
?>
				<div id="my-addresses-body">
						<a class="rounded-red-button" data-related="add-address" href="javascript:;">Add new address</a>
						<!--h1><?php echo ADDRESSES; ?></h1-->
						
						<form id="add-address-form" method="post">
							<div class="white-container" id="add-address">
								<strong class="title">New address</strong>
								
								<label class="half"><span>First name: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" name="first_name" maxlength="50" value="<?php echo htmlspecialchars(get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName); ?>" type="text" /></label>
								<label class="half right"><span>Last name: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" name="last_name" maxlength="50" value="<?php echo htmlspecialchars(get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName); ?>" type="text" /></label>
								
								<div class="clear"></div>
								<a class="disabled rounded-red-button" data-related=""><?php echo CANCEL; ?></a><a class="rounded-red-button"><?php echo APPLY; ?></a>
							</div>
						</form>
<?php
		if ($delivery_addresses) {
			foreach ($delivery_addresses as $delivery_address) {
?>
						<div class="white-container">
							<a class="green-button" data-related="edit-address" href="javascript:;"><?php echo EDIT; ?></a>
							
							<?php echo $delivery_address->address, ' ', $delivery_address->buildingNo, '<br />', 
								$delivery_address->zipCode, ' ', $delivery_address->city; ?>
						</div>
<?php
			}
			
		} else {
			echo '<p>You have no delivery addresses stored in our system yet.</p>';
		}
?>
						
					</div>
				</div>
<?php
	}
