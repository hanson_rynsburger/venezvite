<?php
	if (@$is_included===true && !empty($current_page)) {
		
		switch($current_page) {
			case 'restaurants-list':
				$back_link = './';
				break;
			case 'restaurant':
				$back_link = RESTAURANTS_LIST_CUSTOM_PATH . '/restaurants-list.html';
				break;
			case 'checkout':
				$back_link = $restaurant->customURL . '.html';
				break;
			default:
				$back_link = 'javascript:;';
		}
		
		$image = ($current_page=='restaurant' && count($restaurant->images) ? 
			(!strstr(IMG, 'http:') ? 'http:' : '') . IMG . 'restaurants/' . $restaurant->images[0]->file : 
			(!strstr(IMG, 'http:') ? 'http:' : '') . IMG . 'fb-image.jpg');
?><!DOCTYPE html>
<html lang="<?php echo $_SESSION['s_venezvite']['language']->languageAcronym; ?>"<?php echo $current_page=='restaurant' ? ' itemscope itemtype="http://schema.org/LocalBusiness"' : ''; ?>>
	<head>
		<base href="<?php echo ROOT; ?>" />
		<meta charset="utf-8" />
		<meta content="IE=edge" http-equiv="X-UA-Compatible" />
		
		<title><?php echo $seoTitle; ?></title>
		<meta content="<?php echo $seoTitle; ?>" name="title" />
		<meta content="<?php echo $seoKeywords; ?>" name="keywords" />
		<meta content="<?php echo $seoDescription; ?>" name="description" />
		<meta content="<?php echo $seoKeywords; ?>" name="subject" />
		
		<meta content="CH-GE" name="geo.region" />
		<meta content="Geneve" name="geo.placename" />
		<meta content="46.21128;6.14607" name="geo.position" />
		<meta content="46.21128,6.14607" name="ICBM" />

		<meta content="Venezvite, LLC." name="copyright" />
		<meta content="https://www.facebook.com/cosmin.paltinescu" name="creator" />
		<meta content="Venezvite LLC." name="designer" />
		<meta content="Venezvite, LLC." name="publisher" />
		<meta content="Venezvite LLC." name="rights" />

		<meta content="2015-05" name="date" />
		<meta content="Service" name="type" />
		<meta content="<?php echo $_SESSION['s_venezvite']['language']->languageAcronym; ?>" name="language" />
		<meta content="Lac Leman, Switzerland" name="coverage" />
		<!--meta content="Global" name="distribution" /-->
		<!--meta content="General" name="rating" /-->
		<meta content="order food,food delivery" name="classification" />
		<meta content="index, follow" name="robots" />

		<meta content="<?php echo FB_ADMINS; ?>" property="fb:admins" />
		<meta content="<?php echo FB_APP_ID; ?>" property="fb:app_id" />
		<meta content="<?php echo $seoTitle; ?>" property="og:title" />
		<meta content="<?php echo $seoDescription; ?>" property="og:description" />
		<meta content="<?php echo $image; ?>" property="og:image" />
		<meta content="website" property="og:type" />

		<meta content="initial-scale=1, maximum-scale=1" name="viewport" />
		<meta content="yes" name="apple-touch-fullscreen" />
		<!--meta content="yes" name="apple-mobile-web-app-capable" /-->
		<meta content="#11a3a9" name="theme-color" /><!-- Chrome, Firefox OS and Opera -->
		<meta content="#11a3a9" name="msapplication-navbutton-color" /><!-- Windows Phone -->
		<meta content="#11a3a9" name="apple-mobile-web-app-status-bar-style" /><!-- iOS Safari -->
<?php
		if ($current_page=='restaurant') {
?>
		<meta content="<?php echo $seoTitle; ?>" itemprop="name" />
		<meta content="<?php echo $seoDescription; ?>" itemprop="description" />
<?php
		}
		foreach ($languages as $language) {
			if ($language->languageAcronym != $_SESSION['s_venezvite']['language']->languageAcronym) {
?>
		<link href="<?php echo ROOT, $language->languageAcronym, '/', ($current_page!='home' ? ($current_page!='restaurant' ? implode('/', $_GET['page']) . '.html' : $restaurant->customURL . '.html') : ''); ?>" hreflang="<?php echo $language->languageAcronym; ?>" rel="alternate" />
<?php
			}
		}
?>
		<link href="<?php echo ROOT, CSS_PATH; ?>select2.min.css" rel="stylesheet" />
		<link href="<?php echo ROOT, CSS_PATH; ?>venezvite.css?v=1.22" rel="stylesheet" />
<?php
		if (in_array($current_page, array('menu-items', 'menu-categories', 'menu-option-groups', 'business-info'))) {
			echo '<link href="', ROOT, CSS_PATH, 'jquery-ui.min.css" rel="stylesheet" />';
		}

		if (in_array($current_page, array('orders-history', 'my-profile', 'my-addresses', 'my-ccs', 'restaurant-orders', 'business-info', 'financial-details', 'menu-items', 'menu-categories', 'menu-option-groups', 'gallery', 'my-favorited'))) {
			echo '<link href="', ROOT, CSS_PATH, 'my-account.css?v=1.9" rel="stylesheet" />';
		}

		if (is_file(REL . DS . CSS_PATH . $current_page . '.css')) {
			echo '<link href="', ROOT, CSS_PATH, $current_page, '.css?v=1.20" rel="stylesheet" />';
		}
?>
		<link href="<?php echo ROOT, JS_PATH; ?>new/lightbox/css/lightbox.min.css?v=1.22" rel="stylesheet" />
		<link href="<?php echo ROOT, JS_PATH; ?>new/intl/css/intlTelInput.min.css?v=1.22" rel="stylesheet" />
		<link href="<?php echo ROOT, JS_PATH; ?>new/flexslider/flexslider.min.css?v=1.22" rel="stylesheet" />
		<link href="<?php echo ROOT, JS_PATH; ?>new/rateit/rateit.min.css?v=1.22" rel="stylesheet" />
		<link href="<?php echo ROOT, CSS_PATH; ?>font-awesome/css/font-awesome.min.css?v=1.22" rel="stylesheet" />
		<link href="<?php echo ROOT, CSS_PATH; ?>less-compiled/main.css?v=1.22" rel="stylesheet" />

<?php 
		if (is_file(REL . DS . CSS_PATH . 'less-compiled/' . $current_page . '.css')) {
			echo '<link href="', ROOT, CSS_PATH, 'less-compiled/', $current_page, '.css?v=1.22" rel="stylesheet" />';
		}
?>
		<!--script src="<?php echo ROOT, JS_PATH; ?>prefixfree.min.js"></script-->

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
<?php
		if (!in_array($_SERVER['HTTP_HOST'], array('localhost'))) {
?>
		<script>
			var _gaq=_gaq||[];
			_gaq.push(['_setAccount','UA-30208305-1']);
			_gaq.push(['_setDomainName','venezvite.com']);
			_gaq.push(['_setAllowLinker',true]);
			_gaq.push(['_trackPageview']);
			(function(){
					var ga=document.createElement('script');
					ga.type='text/javascript';
					ga.async=true;
					//ga.src='//www.google-analytics.com/ga.js';
					ga.src=('https:'==document.location.protocol?'https://':'http://')+'stats.g.doubleclick.net/dc.js';
					var s=document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(ga,s);
				})();</script>
<?php
		}
?>
		<link rel="shortcut icon" href="<?php echo ROOT; ?>favicon.ico" />
	</head>
	<body>
		<div id="body">
			<header>
				<div class="main-content">
					<nav id="main-navigation" class="clearfix">
						<a class="left" id="main-menu-handle" href="javascript:;"></a>
						<a class="left" id="list-filter-handle" href="javascript:;"></a>
						<a class="left" id="header-back" href="<?php echo $back_link; ?>"></a>
						<a href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/'; ?>"><img alt="Venezvite" src="i/logo.png" /></a>
						<span id="main-slogan"><?php echo LOGO_SLOGAN; ?></span>
						<?php
						$shopping_cart_items = 0;
						if (($restaurant_path = @json_decode($_COOKIE['c_venezvite'])->restaurant) && !empty($_SESSION['s_venezvite']['carts'][$restaurant_path])) {
							foreach( @$_SESSION['s_venezvite']['carts'][$restaurant_path] as $obj ) {
								$shopping_cart_items += intval( $obj["quantity"] ); 
							}
							//$shopping_cart_items = count($_SESSION['s_venezvite']['carts'][$restaurant_path]);
						}
						?>
						<a class="right" id="shopping-cart-handle" href="javascript:;"><?php echo ($shopping_cart_items ? '<strong>' . $shopping_cart_items . '</strong>' : ''); ?></a>
						<a class="right" id="list-sort-handle" href="javascript:;"></a>
						<ul class="right clearfix">
							<?php
								if ($current_page=='restaurant') {
							?>
							<li class="left">
								<div class="fb-like" data-href="http://www.facebook.com/likevenezvite" data-layout="button_count" data-show-faces="false"></div>
							</li>
							<?php
								}
							?>

							<li class="top-level-menu top-menu-arrow" id="cities-handle"><a href="javascript:;"><?php echo CITIES; ?></a>
								<ul class="top-sub-menu" id="cities-list">
								<?php
									require 'inc/cities-list.inc.php';
								?>
								</ul>
							</li>
							
							<li class="top-level-menu top-menu-arrow" id="language-handle"><a href="javascript:;"><?php echo $_SESSION['s_venezvite']['language']->languageName; ?></a>
								<ul class="top-sub-menu" id="languages-list">
									<?php
									foreach ($languages as $language) {
										if ($language->languageAcronym != $_SESSION['s_venezvite']['language']->languageAcronym) {
											$url = ROOT . $language->languageAcronym . '/' . ($current_page!='home' ? ($current_page!='restaurant' ? implode('/', $_GET['page']) . '.html' : $restaurant->customURL . '.html') : '');
									?>
									<li><a href="<?php echo $url; ?>"><?php echo $language->languageName; ?></a></li>
									<?php
										}
									}
									?>
								</ul>
							</li>
							<li class="top-level-menu" id="helper-handle">
								<a href="javascript:;"><?php echo HELP ?></a>
							</li>
							<?php
							if (empty($entity) && empty($restaurantAdmin)/* && $current_page!='confirmation'*/) {
							?>
							<li class="top-level-menu"><a href="javascript:;" id="login-button"><?php echo LOGIN; ?></a></li>
							<?php
							} else {
								$first_name = htmlspecialchars(!empty($entity) ? 
									(get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName) : 
									$restaurantAdmin->restaurant->restaurantName);
							?>
							<li class="top-level-menu top-menu-arrow" id="my-account-menu">
								<a href="javascript:;">
									<span class="nowrap"><?php echo HELLO; ?>, <span id="top-user-name"><?php echo $first_name; ?></span></span>
								</a>
							
								<ul class="top-sub-menu" id="account-sub-menu">
									<?php
										if (!empty($restaurantAdmin)) {
											require REL . '/views/inc/logged-restaurant-menu.inc.php';
										} else {
											require REL . '/views/inc/logged-user-menu.inc.php';
										}
									?>
									<li><a href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/logout.html"><?php echo LOGOUT; ?></a></li>
								</ul>
							</li>
							<?php
							}
							?>
						</ul>
					</nav>
				</div>
			</header>
			<div id="main-content"><!-- class="parallax" -->
				<?php
					if (is_file(REL . DS . 'views' . DS . $current_page . '.tpl.php')) {
						require_once REL . DS . 'views' . DS . $current_page . '.tpl.php';
					}
				?>
			</div>
			<footer>
				<div class="main-content">
					<ul id="footer-payments">
						<li title="Visa"></li>
						<li title="MasterCard"></li>
						<li title="American Express"></li>
						<li title="PayCash"></li>
						<li title="Lunch-Check"></li>
					</ul>
					<div class="full-horizontal-separator"></div>
					<ul id="footer-nav">
						<li class="left"><strong><?php echo FOOTER_WHO_WE_ARE; ?></strong><ul>
								<li><a href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/about-us.html"><?php echo FOOTER_ABOUT_US; ?></a></li>
								<li><a href="http://www.venezvite.com/blog/"><?php echo FOOTER_BLOG; ?></a></li>
							</ul></li>
						<li class="left"><strong><?php echo FOOTER_YOUR_BUSINESS; ?></strong><ul>
								<li><a href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/corporate-join.html"><?php echo FOOTER_CREATE_ACCOUNT; ?></a></li>
								<li><a href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/restaurant-join.html"><?php echo FOOTER_BECOME_PARTNER; ?></a></li>
								<li><a href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/login-restaurant.html"><?php echo PARTNER_LOGIN; ?></a></li>
							</ul></li>
						<li class="left"><strong><?php echo FOOTER_CONTACT_HELP; ?></strong><ul>
								<li><a href="javascript:;" id="footer-contact"><?php echo FOOTER_CONTACT; ?></a></li>
								<li><a href="javascript:;" id="footer-suggestion"><?php echo FOOTER_SUGGEST; ?></a></li>
							</ul></li>
						<li class="left"><strong><?php echo FOOTER_MORE; ?></strong><ul>
								<li><a href="javascript:;" id="footer-specials"><?php echo FOOTER_SPECIALS; ?></a></li>
							</ul></li>
					</ul>
					<div class="clear"></div>
					
					<div id="footer-payment-social">
						<div class="left">
							<div class="clear"></div>
						</div>
						<ul class="right">
							<li><a class="fb popup" href="https://www.facebook.com/likevenezvite">
								<i class="fa fa-facebook"></i>
							</a></li>
							<li><a class="popup twitter" href="https://twitter.com/venezvite">
								<i class="fa fa-twitter"></i>
							</a></li>
							<li><a class="instagram popup" href="https://www.instagram.com/venezvite/">
								<i class="fa fa-instagram"></i>
							</a></li>
							<li><a class="gplus popup" href="https://plus.google.com/+Venezvite/">
								<i class="fa fa-google-plus"></i>
							</a></li>
						</ul>
						<div class="clear"></div>
					</div>
					
					<div class="full-horizontal-separator"></div>
					<div id="footer-copyright">
						<div class="left">&copy; <?php echo date('Y'); ?> Venezvite - <?php echo FOOTER_COPYRIGHT; ?> <a href="tou-customers.html"><?php echo FOOTER_USE_TERMS; ?></a> <a href="privacy-policy.html"><?php echo FOOTER_PRIVACY; ?></a></div>
						<div class="right"><?php echo FOOTER_SITE_BY; ?> <a class="popup" href="http://www.vmgroupe.com/"><strong>VMGROUPE</strong></a></div>
						<div class="clear"></div>
					</div>
				</div>
			</footer>
			<?php require_once REL . DS . 'views' . DS . 'inc' . DS . 'shopping-cart-container.tpl.php'; ?>
		</div>
		<a href="#" id="main-to-top"></a>
<?php
		if (empty($entity)) {
?>
		<div class="modal-body" id="login-panel">
			<form method="post">
				<a class="close" href="javascript:;">&times;</a>
				<span class="title"><?php echo LOGIN; ?></span>
				<a class="rounded-red-button" href="javascript:;" id="fb-login-button"><?php echo FB_LOGIN; ?></a>
				<div class="or-bar"><?php echo OR_; ?></div>
				<label><span>Email: <small>(<?php echo USERNAME_CORPORATE; ?>)</small></span>
					<input class="rounded-input" maxlength="100" name="email" value="" type="text" /></label>
				<label><span><?php echo PASSWORD; ?>:</span>
					<input class="rounded-input" maxlength="50" name="password" value="" type="password" /></label>
				<label><input name="remember" value="Y" type="checkbox" /> &nbsp; <?php echo KEEP_LOGGED; ?></label>
				<input class="rounded-red-button" type="submit" value="<?php echo LOGIN; ?>" />
				<em><?php echo str_replace('{$link}', ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/forgot-password.html', OOPS_FORGOT); ?></em>
				<strong><?php echo LOGIN_INCENTIVE_TITLE; ?></strong>
				<br />
				<p><?php echo LOGIN_INCENTIVE_DESC; ?></p>
			</form>
		</div>
<?php
		}
?>
		<div id="fb-root"></div>
		<script src="<?php echo ROOT, JS_PATH; ?>jquery.min.js"></script><!-- https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/ -->
		<script src="<?php echo ROOT, JS_PATH; ?>jquery.mobile.custom.min.js"></script>
		<script src="<?php echo ROOT, JS_PATH; ?>jquery-validate.min.js"></script>
<?php
		if ($_SESSION['s_venezvite']['language']->languageAcronym!='en' && 
			is_file(REL . DS . JS_PATH . 'localization/messages_' . $_SESSION['s_venezvite']['language']->languageAcronym . '.min.js')) {
?>
		<script src="<?php echo ROOT, JS_PATH; ?>localization/messages_<?php echo $_SESSION['s_venezvite']['language']->languageAcronym; ?>.min.js"></script>
<?php
		}

		if (in_array($current_page, array('menu-items', 'menu-categories', 'menu-option-groups', 'business-info' ))) {
			echo '<script src="', ROOT, JS_PATH, 'jquery-ui.min.js"></script>', 
				'<script src="', ROOT, JS_PATH, 'jquery.ui.touch-punch.min.js"></script>';
		}
?>
		<script src="<?php echo ROOT, JS_PATH; ?>jquery-cookie.min.js"></script>
		<script src="<?php echo ROOT, JS_PATH; ?>hide-address-bar.min.js"></script>
		<script src="<?php echo ROOT, JS_PATH; ?>select2.400.min.js"></script>
		<script src="<?php echo ROOT, JS_PATH, "new", DS; ?>plugins.js"></script>
<?php
		if (!in_array($current_page, array(/*'business-info', */'financial-details', 'menu-items', 'menu-categories', 'menu-option-groups'))) {
?>
		<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBe4T2ibwnkDMX65ZuHDOkTROwT5VXLrrI&amp;sensor=false&amp;libraries=<?php echo ($current_page=='business-info' ? 'drawing' : 'places'); ?>&amp;language=<?php echo $_SESSION['s_venezvite']['language']->languageAcronym; ?>"></script><!--AIzaSyChxFC2dYYquWh1zRsbXPwSpPmLI4Qgv2Q-->
<?php
		}
?>
		<script src="<?php echo ROOT, JS_PATH; ?>retina.min.js"></script>

		<script src="<?php echo ROOT, JS_PATH; ?>new/lightbox/js/lightbox.min.js"></script>
		<script src="<?php echo ROOT, JS_PATH; ?>new/intl/js/intlTelInput.min.js"></script>
		<script src="<?php echo ROOT, JS_PATH; ?>new/flexslider/jquery.flexslider-min.js"></script>
		<script src="<?php echo ROOT, JS_PATH; ?>new/rateit/jquery.rateit.min.js"></script>
		<script src="<?php echo ROOT, JS_PATH; ?>new/greensock/TweenMax.min.js"></script>
		<script src="<?php echo ROOT, JS_PATH; ?>new/greensock/jquery.gsap.min.js"></script>
		
		<script>var img_root = '<?php echo IMG; ?>', 
	lang_code = '<?php echo $_SESSION['s_venezvite']['language']->localeCode; ?>', 
	current_datetime = '<?php echo date('D, d M Y H:i', time()); ?>', 
	search_datetime = '<?php echo date('D, d M Y H:i', strtotime(!empty($_SESSION['s_venezvite']['search']['date']) && !empty($_SESSION['s_venezvite']['search']['time']) ? 
		$_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time'] : date('Y-m-d') . ' ' . getCurrentOrderTime())); ?>', 
	restaurant_url = '<?php echo (empty($restaurant) ? @json_decode($_COOKIE['c_venezvite'])->restaurant : $restaurant->customURL); // To be used as index in the shopping cart ?>', 
	current_delivery_zone = <?php echo ((!empty($restaurant) || ($restaurant = restaurant::getByURL(@json_decode($_COOKIE['c_venezvite'])->restaurant))) && $restaurant->current_delivery_zone ? json_encode($restaurant->current_delivery_zone) : 'null'); ?>, 
	
	langEdit = '<?php echo EDIT; ?>', 
	langDelete = '<?php echo DELETE; ?>', 
	langNextDeliveryTime = '<?php echo str_replace('\'', '\\\'', NEXT_DELIVERY_TIME); ?>', 
	langNextTakeawayTime = '<?php echo str_replace('\'', '\\\'', NEXT_TAKEAWAY_TIME); ?>', 
	langMoreMoney = '<?php echo str_replace('\'', '\\\'', MORE_MONEY); ?>', 
	langEmailInUse = '<?php echo str_replace('\'', '\\\'', EMAIL_IN_USE); ?>';</script>
		<script src="<?php echo ROOT, JS_PATH; ?>venezvite.js?v=1.11"></script>
<?php
		if (in_array($current_page, array('orders-history', 'my-profile', 'my-addresses', 'my-ccs', 'restaurant-orders', 'business-info', 'financial-details', 'menu-items', 'menu-categories', 'menu-option-groups', 'gallery', 'my-favorited'))) {
			echo '<script src="', ROOT, JS_PATH, 'my-account.js?v=1.1"></script>';
		}
		
		if (is_file(REL . DS . JS_PATH . $current_page . '.js')) {
			echo '<script src="', ROOT, JS_PATH, $current_page, '.js?v=1.11"></script>';
		}
		
		if (!in_array($_SERVER['HTTP_HOST'], array('localhost'))) {
?>
		<script src="//static.getclicky.com/js"></script>
		<script>try{ clicky.init(100848979); }catch(e){}</script>
		<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100848979ns.gif" /></p></noscript>
<?php
		}
		
		checkErrors();
?>
	</body>
</html>
<?php
	}