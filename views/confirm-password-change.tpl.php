<?php
	if (@$is_included) {
?>
				<div id="hero-image" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo IMG; ?>home-bg/home-bg-0<?php echo mt_rand(1, 8); ?>.jpg" >
					<strong><?php echo CPC_ADMIN_PANEL; ?></strong>
				</div>
				<?php /* ?><div id="main-private-menu">
					<ul>
						<li><a class="selected" href="javascript:;"><?php echo CPC_TITLE; ?></a></li>
					</ul>
				</div> */?>
				
				<div id="confirm-password-change-body">
					<div class="center main-content">
						<form method="post">
							<input class="rounded-input" maxlength="20" name="new_password" placeholder="<?php echo CPC_PASSWORD; ?>: *" type="password" value="" />
							<input class="rounded-input" maxlength="20" name="confirm_new_password" placeholder="<?php echo CPC_CONFIRM_PASSWORD; ?>: *" type="password" value="" />
							<br />
							<input class="rounded-red-button" type="submit" value="<?php echo CPC_UPDATE; ?>" />
						</form>
					</div>
				</div>
<?php
	}
