<?php
	if (@$is_included && !empty($restaurantAdmin) && isset($orders) && is_array($orders) && isset($total_orders) && isset($accepted) && isset($declined)) {
?>
	<div class="main-content">
		<?php require_once 'inc/logged-restaurant-header.inc.php'; ?>
 		<div id="restaurant-orders-body">

						<!--h1 class="left"><?php echo ORDERS_HISTORY; ?></h1-->
						<form method="post">
							<table cellspacing="0" id="sales-summary">
								<thead>
									<tr>
										<th><?php echo TIME_PERIOD; ?></th>
										<th><?php echo TOTAL_ORDERS; ?></th>
										<th><?php echo ACCEPTED; ?></th>
										<th><?php echo DECLINED; ?></th>
										<th><?php echo TOTAL_SALES; ?></th>
										<th><?php echo TOTAL_EARNINGS; ?></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><select name="sales_period" onchange="this.form.submit();">
												<!--option value="CW"><?php echo THIS_WEEK; ?></option-->
												<option value="CM"><?php echo THIS_MONTH; ?></option>
												<option<?php echo @$_POST['sales_period']=='PM' ? ' selected="selected"' : ''; ?> value="PM"><?php echo PAST_MONTH; ?></option>
												<option<?php echo @$_POST['sales_period']=='CY' ? ' selected="selected"' : ''; ?> value="CY"><?php echo THIS_YEAR; ?></option>
												<option<?php echo @$_POST['sales_period']=='AT' ? ' selected="selected"' : ''; ?> value="AT"><?php echo ALL_TIME; ?></option>
											</select></td>
										<td><?php echo number_format($total_orders, 0); ?></td>
										<td><?php echo number_format($accepted, 0); ?></td>
										<td><?php echo number_format($declined, 0); ?></td>
										<td><?php echo number_format($total_sales), ' ', $restaurantAdmin->restaurant->currency; ?></td>
										<td><?php echo number_format($total_earnings), ' ', $restaurantAdmin->restaurant->currency; ?></td>
									</tr>
								</tbody>
							</table>
						</form>
						<form method="post">
							<table cellspacing="0" id="sales-summary-mobile">
								<thead>
									<tr>
										<th colspan="2"><?php echo TIME_PERIOD; ?><br />
											<select name="sales_period" onchange="this.form.submit();">
												<!--option value="CW"><?php echo THIS_WEEK; ?></option-->
												<option value="CM"><?php echo THIS_MONTH; ?></option>
												<option<?php echo @$_POST['sales_period']=='PM' ? ' selected="selected"' : ''; ?> value="PM"><?php echo PAST_MONTH; ?></option>
												<option<?php echo @$_POST['sales_period']=='CY' ? ' selected="selected"' : ''; ?> value="CY"><?php echo THIS_YEAR; ?></option>
												<option<?php echo @$_POST['sales_period']=='AT' ? ' selected="selected"' : ''; ?> value="AT"><?php echo ALL_TIME; ?></option>
											</select></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo TOTAL_ORDERS; ?></td>
										<td><?php echo number_format($total_orders, 0); ?></td>
									</tr>
									<tr>
										<td><?php echo ACCEPTED; ?></td>
										<td><?php echo number_format($accepted, 0); ?></td>
									</tr>
									<tr>
										<td><?php echo DECLINED; ?></td>
										<td><?php echo number_format($declined, 0); ?></td>
									</tr>
									<tr>
										<td><?php echo TOTAL_SALES; ?></td>
										<td style="white-space: nowrap;"><?php echo number_format($total_sales), ' ', $restaurantAdmin->restaurant->currency; ?></td>
									</tr>
									<tr>
										<td><?php echo TOTAL_EARNINGS; ?></td>
										<td></td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2" style="text-align: center !important;"><?php echo number_format($total_earnings), ' ', $restaurantAdmin->restaurant->currency; ?></td>
									</tr>
								</tfoot>
							</table>
						</form>
<?php
		if (count($orders)) {
?>
						<ul id="restaurant-sub-menu">
							<li><a<?php echo !isset($_GET['past']) ? ' class="current"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/restaurant-orders.html'; ?>"><?php echo NEW_ORDERS; ?></a></li>
							<li><a<?php echo isset($_GET['past']) ? ' class="current"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/restaurant-orders.html?past'; ?>"><?php echo PAST_ORDERS; ?></a></li>
						</ul>
						<table cellspacing="0" id="orders-list">
							<thead>
								<tr>
									<th><?php echo RECEIVED_DATE; ?></th>
									<th><?php echo RECEIVED_FROM; ?></th>
									<th><?php echo ORDER_TYPE; ?></th>
									<!--th style="width:65px"><?php echo ORDER_AMOUNT; ?></th!-->
									<th style="width:130px"><?php echo ORDER_REQUIRED; ?></th>
									<th><?php echo ORDER_STATUS; ?></th>
								</tr>
							</thead>
							<tbody>
<?php
			setlocale(LC_TIME, $_SESSION['s_venezvite']['language']->localeCode); // 'fra'
			$counter = 0;
			foreach ($orders as $order) {
				if (strtotime($order->dateAdded)<strtotime('2016-01-01')) {
					break;
				}
				/*if (strtotime($order->dateAdded)<strtotime('-1 month') && $counter>=25) {
					break;
				}
				*/
				if ($order->user) {
					$recipient = $order->user->firstName . ' ' . $order->user->lastName . '<br />' . 
						'<span>' . $order->user->phone . '</span>';
				} else {
					$recipient = $order->corporateAccount->contactFirstName . ' ' . $order->corporateAccount->contactLastName . '<br />' . 
						'<span>' . $order->corporateAccount->mobile . '</span>';
				}
				
				if ($order->type=='D') {
					$order_type = DELIVERY . '<br />' . 
						'<span>' . $order->userAddress->longFormat() . '</span>';
				} else {
					$order_type = TAKEAWAY;
				}
?>
								<tr data-order="<?php echo $order->idOrder; ?>">
									<td><?php echo ucwords(utf8_encode(strftime('%e %b'/*date('j M'*/, strtotime($order->dateAdded)))); ?><br />
										<span><?php echo strftime('%H:%M', strtotime($order->dateAdded)); ?></span></td>
									<td><?php echo $recipient; ?></td>
									<td><?php echo $order_type; ?></td>
									<!--td></td-->
									<td><?php echo ucwords(utf8_encode(strftime('%e %b - %H:%M'/*date('j M - H:i'*/, strtotime($order->dateDesired)))); ?><br />
										<span><?php echo number_format($order->value, 2), ' ', $restaurantAdmin->restaurant->currency; ?></span></td>
									<td><?php
				
				if (empty($order->dateProcessed)) {
?>
										<a class="accept" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/accept-order.html?id=', $order->idOrder; ?>"><?php echo ACCEPT_ORDER; ?></a><br />
										<a class="decline" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/decline-order.html?id=', $order->idOrder; ?>"><?php echo DECLINE_ORDER; ?></a>
<?php
				} else {
					$status = ACCEPTED;
					$status_class = ' accepted';
					
					if ($order->rejectionReason) {
						$status = DECLINED;
						$status_class = ' declined';
					}
?>
										<span class="status<?php echo $status_class; ?>"><?php echo $status; ?></span>
<?php
				}
				
?></td>
								</tr>
<?php
				$counter++;
			}
?>
							</tbody>
						</table>
						<script>var langConfirmAccept = '<?php echo str_replace('\'', '\\\'', CONFIRM_ACCEPT); ?>', langConfirmDecline = '<?php echo CONFIRM_DECLINE; ?>';</script>
<?php
		} else {
			if (!isset($_GET['past'])) {
				echo '<p>', NO_PENDING_ORDERS, ' <a href="', ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/restaurant-orders.html?past">', VIEW_PAST_ORDERS, '</a></p>';
			} else {
				echo '<p>', NO_PAST_ORDERS, '</p>';
			}
		}
?>
						
				</div>
	</div>
<?php
	}
