<?php
	if (@$is_included && !empty($countries)) {
?>
				<?php /* ?><div id="hero-image">
					<strong><?php echo HELLO_CORPORATE; ?></strong>
					<span><?php echo SIGN_UP_CORPORATE; ?></span>
				</div>
				
				<div id="main-private-menu">
					<ul>
						<li><a class="selected" href="javascript:;"><?php echo SIGN_UP_LINK; ?></a></li>
					</ul>
				</div> */ ?>

				<div id="hero-image" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo IMG; ?>home-bg/home-bg-0<?php echo mt_rand(1, 8); ?>.jpg" >
					<strong><?php echo HELLO_CORPORATE; ?></strong>
					<span><?php echo SIGN_UP_CORPORATE; ?></span>
				</div>

				<div id="corporate-join-body">
					<form method="post" enctype="multipart/form-data">
						<div class="main-content">
<?php
		if (!empty($_SESSION['s_venezvite']['corporate_registration_message'])) {
?>
							<div<?php echo empty($error) ? ' class="success"' : ''; ?> id="error-container" style="height:auto">
								<div>
									<strong><?php echo !empty($error) ? SOME_ERRORS : REGISTRATION_SUCCESS; ?></strong>
									<span><?php echo $_SESSION['s_venezvite']['corporate_registration_message']; ?></span>
								</div>
							</div>
<?php
			unset($_SESSION['s_venezvite']['corporate_registration_message']);
		}
?>
							<ol>
								<li><?php echo CONTACT_INFO; ?><br />
									<div class="half left">
										<input class="rounded-input" maxlength="50" name="last_name" placeholder="<?php echo LAST_NAME; ?> *" type="text" value="<?php echo !empty($_POST['last_name']) ? htmlspecialchars($_POST['last_name']) : ''; ?>" />
										<input class="rounded-input" maxlength="50" name="job_title" placeholder="<?php echo JOB_TITLE; ?> *" type="text" value="<?php echo !empty($_POST['job_title']) ? htmlspecialchars($_POST['job_title']) : ''; ?>" />
										<input class="rounded-input" maxlength="20" name="mobile" placeholder="<?php echo MOBILE; ?> *" type="text" value="<?php echo !empty($_POST['mobile']) ? htmlspecialchars($_POST['mobile']) : ''; ?>" />
									</div>
									<div class="half right">
										<input class="rounded-input" maxlength="50" name="first_name" placeholder="<?php echo FIRST_NAME; ?> *" type="text" value="<?php echo !empty($_POST['first_name']) ? htmlspecialchars($_POST['first_name']) : ''; ?>" />
										<input class="rounded-input" maxlength="50" name="department" placeholder="<?php echo DEPARTMENT; ?>" type="text" value="<?php echo !empty($_POST['department']) ? htmlspecialchars($_POST['department']) : ''; ?>" />
									</div>
									<div class="clear"></div>
								</li>
								
								<li><?php echo COMPANY_INFO; ?> <span>*</span><br />
									<div class="half left" style="margin-bottom:-13px">
										<select class="rounded-select" name="country">
											<option data-abbr="CH" value=""><?php echo SELECT_COUNTRY; ?></option>
<?php
		foreach ($countries as $country) {
?>
											<option data-abbr="<?php echo $country->countryAcronym; ?>" value="<?php echo $country->idCountry; ?>"><?php echo $country->countryName; ?></option>
<?php
		}
?>
										</select>
									</div>
									
									<div class="clear half left">
										<input class="rounded-input" maxlength="50" name="city" placeholder="<?php echo COMPANY_CITY; ?>" type="text" value="<?php echo !empty($_POST['city']) ? htmlspecialchars($_POST['city']) : ''; ?>" />
										<input class="rounded-input" maxlength="100" name="address" placeholder="<?php echo COMPANY_ADDRESS; ?> *" type="text" value="<?php echo !empty($_POST['address']) ? htmlspecialchars($_POST['address']) : ''; ?>" />
										<input name="latitude" type="hidden" value="<?php echo !empty($_POST['latitude']) ? htmlspecialchars($_POST['latitude']) : ''; ?>" />
										<input name="longitude" type="hidden" value="<?php echo !empty($_POST['longitude']) ? htmlspecialchars($_POST['longitude']) : ''; ?>" />
										<input class="rounded-input" maxlength="20" name="phone" placeholder="<?php echo COMPANY_PHONE; ?> *" type="text" value="<?php echo !empty($_POST['phone']) ? htmlspecialchars($_POST['phone']) : ''; ?>" />
									</div>
									<div class="half right">
										<input class="rounded-input" maxlength="50" name="company_name" placeholder="<?php echo COMPANY_NAME; ?> *" type="text" value="<?php echo !empty($_POST['company_name']) ? htmlspecialchars($_POST['company_name']) : ''; ?>" />
										<input class="rounded-input" maxlength="10" name="zip" placeholder="<?php echo POSTAL_CODE; ?> *" type="text" value="<?php echo !empty($_POST['zip']) ? htmlspecialchars($_POST['zip']) : ''; ?>" />
										<input class="rounded-input" maxlength="5" name="employees" placeholder="<?php echo EMPLOYEES_NO; ?>" type="text" value="<?php echo !empty($_POST['employees']) ? htmlspecialchars($_POST['employees']) : ''; ?>" />
									</div>
									<div class="clear"></div>
								</li>

								<li id="email-addresses"><?php echo ATTACH_EMAIL_ADDRESSES; ?> <span>*</span><br />
									<div class="clearfix">
										<div class="half left">
											<input class="rounded-input" maxlength="50" name="email[0]" placeholder="<?php echo YOUR_EMAIL; ?>" type="text" value="<?php echo @$_POST['email'][0] ? htmlspecialchars($_POST['email'][0]) : ''; ?>" />
										</div>
										<div class="half right">
											<input class="rounded-input" maxlength="50" name="confirm_email[0]" placeholder="<?php echo CONFIRM_YOUR_EMAIL; ?>" type="text" value="<?php echo @$_POST['confirm_email'][0] ? htmlspecialchars($_POST['confirm_email'][0]) : ''; ?>" />
										</div>
									</div>
<?php
		if (is_array(@$_POST['email']) && is_array(@$_POST['confirm_email']) && 
			count($_POST['email'])==count($_POST['confirm_email']) && count($_POST['email'])>1) {
			
			for ($i=1; $i<count($_POST['email']); $i++) {
?>
									<div>
										<div class="half left">
											<input class="rounded-input" maxlength="50" name="email[<?php echo $i; ?>]" placeholder="<?php echo YOUR_EMAIL; ?>" type="text" value="<?php echo htmlspecialchars(@$_POST['email'][$i]); ?>" />
										</div>
										<div class="half right">
											<input class="rounded-input" maxlength="50" name="confirm_email[<?php echo $i; ?>]" placeholder="<?php echo CONFIRM_YOUR_EMAIL; ?>" type="text" value="<?php echo htmlspecialchars(@$_POST['confirm_email'][$i]); ?>" />
										</div>
									</div>
<?php
			}
		}
?>
									<a class="clear" href="javascript:;">+ <?php echo ATTACH_MORE_EMAILS; ?></a>
								</li>
							</ol>
							<div class="clear"></div>
							
							<h1><?php echo CREATE_ACCOUNT_TITLE; ?></h1>
							<div class="center">
								<input class="rounded-input" maxlength="50" name="username" placeholder="<?php echo USERNAME; ?>" type="text" value="<?php echo !empty($_POST['username']) ? htmlspecialchars($_POST['username']) : ''; ?>" />
								<input class="rounded-input" maxlength="50" name="confirm_username" placeholder="<?php echo CONFIRM_USERNAME; ?>" type="text" value="<?php echo !empty($_POST['confirm_username']) ? htmlspecialchars($_POST['confirm_username']) : ''; ?>" />
								<input class="rounded-input" maxlength="20" name="password" placeholder="<?php echo PASSWORD; ?>" type="password" value="<?php echo !empty($_POST['password']) ? htmlspecialchars($_POST['password']) : ''; ?>" />
								<input class="rounded-input" maxlength="20" name="confirm_password" placeholder="<?php echo CONFIRM_PASSWORD; ?>" type="password" value="<?php echo !empty($_POST['confirm_password']) ? htmlspecialchars($_POST['confirm_password']) : ''; ?>" /><br />
								<br />
								<label><input name="terms" type="checkbox" value="Y" /> <?php echo str_replace('{$link}', ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/tou-customers.html', AGREE_TERMS); ?></label>
							</div>
							
							<input class="rounded-red-button" type="submit" value="<?php echo CREATE_ACCOUNT; ?>" />
						</div>
					</form>
				</div>
				<script>var langInvalidAddress = '<?php echo str_replace('\'', '\\\'', INVALID_ADDRESS) ; ?>', 
langUsernameInUse = '<?php echo str_replace('\'', '\\\'', USERNAME_IN_USE) ; ?>';</script>
<?php
	}
