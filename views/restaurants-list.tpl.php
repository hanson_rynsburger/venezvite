<?php
	if (@$is_included && isset($open_restaurants) && isset($closed_restaurants) && isset($restaurant_locations) && isset($cuisine_types)) {
?>
				<form method="post" id="restaurant-list-form">
					<div id="list-search-form">
						<div class="main-content text-center">
							<div class="search-form">
								<div class="search-form-type">
									<?php
									hs_drop_down( "type", array(
										"icon-tail" => "<i class='fa fa-chevron-down'></i>",
										"text-align" => "center",
										"values" => array(
											'D' => DELIVERY,
											'T' => TAKEAWAY
										),
										"active" => @$_SESSION['s_venezvite']['search']['type']=='T'?'T':'D',
										"callback" => "change"
									) );
									?>
								</div>
								<div class="text-center">
	                                <?php require_once 'views/inc/search-location.inc.php'; ?>
								</div>
								<div class="hidden visible-mobile">
								<?php
									hs_drop_down( "time_m", array(
										"icon-tail" 	=> "<i class='fa fa-chevron-down'></i>",
										"text-align" 	=> "center",
										"values" 		=> commonTimesList(@$_SESSION['s_venezvite']['search']['date'], @$_SESSION['s_venezvite']['search']['time'], null, null, true),
										"active" 		=> isset( $_SESSION['s_venezvite']['search']['time']) ? $_SESSION['s_venezvite']['search']['time'] : ""
									) );
									?>
								</div>
								<div class="hidden visible-mobile">
								<?php
									hs_drop_down( "date_m", array(
										"icon-tail" 	=> "<i class='fa fa-chevron-down'></i>",
										"text-align" 	=> "center",
										"values" 		=> commonDatesList(@$_SESSION['s_venezvite']['search']['date'], true),
										"active" 		=> isset( $_SESSION['s_venezvite']['search']['date'] ) ? $_SESSION['s_venezvite']['search']['date'] : ''
									) );
								?>
								</div>
								<div class="hidden visible-mobile">
									<button class="rounded-red-button" id="find-mobile"><?php echo FIND; ?></button>
								</div>
							</div>
						</div>
					</div>

					<div id="list-body">
						<div class="main-content">
							<div class="clearfix">
								<div class="left left-sidebar">
									<div id="list-filter-form">
										<div>
											<h4><?php echo FILTER_BY; ?></h4>
											<?php if (count($cuisine_types)) { ?>
											<div class="list-filter-cuisine-types">
												<span class="filter-caption">
													<?php echo CUISINES; ?><a href="javascript:;"><?php echo CLEAR; ?></a>
												</span>
	
												<ul id="cuisines-filter">
												<?php
												foreach ($cuisine_types as $id => $cuisine_type) {
													echo '<li><label><input', (@in_array($id, @$_SESSION['s_venezvite']['search']['cuisines']) ? ' checked="checked"' : ''), ' name="cuisines[]" type="checkbox" value="', $id, '" /> ', $cuisine_type['cuisine_type'], ' (', $cuisine_type['restaurants'], ')</label></li>';
												}
												?>
												</ul>

												<a id="show-more-cuisines" href="javascript:;"><?php echo SHOW_MORE; ?></a>
											</div>
											<?php } ?>
	
											<div class="list-filter-rating">
												<span class="filter-caption">
													<?php echo RATINGS; ?><a href="javascript:;"><?php echo CLEAR; ?></a>
												</span>
												<ul>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['rating']=='5' ? 'checked="checked"' : ''; ?> name="rating" type="radio" value="5" /> <span class="review-stars">*****</span> <em><!--(14)--></em></label></li>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['rating']=='4' ? 'checked="checked"' : ''; ?> name="rating" type="radio" value="4" /> <span class="review-stars">****</span> <?php echo AND_UP; ?> <em><!--(14)--></em></label></li>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['rating']=='3' ? 'checked="checked"' : ''; ?> name="rating" type="radio" value="3" /> <span class="review-stars">***</span> <?php echo AND_UP; ?> <em><!--(8)--></em></label></li>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['rating']=='2' ? 'checked="checked"' : ''; ?> name="rating" type="radio" value="2" /> <span class="review-stars">**</span> <?php echo AND_UP; ?> <em><!--(5)--></em></label></li>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['rating']=='1' ? 'checked="checked"' : ''; ?> name="rating" type="radio" value="1" /> <span class="review-stars">*</span> <?php echo AND_UP; ?> <em><!--(4)--></em></label></li>
												</ul>
											</div>

											<div class="list-filter-price">
												<span class="filter-caption">
													<?php echo PRICE; ?><a href="javascript:;"><?php echo CLEAR; ?></a>
												</span>
												<ul>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['price_level']=='1' ? 'checked="checked"' : ''; ?> name="price_level" type="radio" value="1" /> <span class="price-range">$</span> <?php echo AND_LESS; ?> <em><!--(2)--></em></label></li>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['price_level']=='2' ? 'checked="checked"' : ''; ?> name="price_level" type="radio" value="2" /> <span class="price-range">$$</span> <?php echo AND_LESS; ?> <em><!--(5)--></em></label></li>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['price_level']=='3' ? 'checked="checked"' : ''; ?> name="price_level" type="radio" value="3" /> <span class="price-range">$$$</span> <?php echo AND_LESS; ?> <em><!--(18)--></em></label></li>
													<li><label><input <?php echo $_SESSION['s_venezvite']['search']['price_level']=='4' ? 'checked="checked"' : ''; ?> name="price_level" type="radio" value="4" /> <span class="price-range">$$$$</span> <?php echo AND_LESS; ?> <em><!--(6)--></em></label></li>
						 							<li><label><input <?php echo $_SESSION['s_venezvite']['search']['price_level']=='5' ? 'checked="checked"' : ''; ?> name="price_level" type="radio" value="5" /> <span class="price-range">$$$$$</span> <em><!--(2)--></em></label></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="right right-contents">
									<?php
									if (!empty($search_restaurants)) { // have matching restaurants
										if (@$_SESSION['s_venezvite']['search']['cuisines'] || @$_SESSION['s_venezvite']['search']['rating'] || @$_SESSION['s_venezvite']['search']['price_level']) {
									?>
									<div id="mobile-filter-notice"><span></span><?php echo LIST_IS_FILTERED; ?> <a href="javascript:;"><?php echo CLEAR; ?></a></div>
									<?php } ?>

									<div id="map-container"><div id="restaurants-map"></div></div>

									<h1><?php echo str_replace('{datetime}', prettyDateTime($_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time'], true, _AT), str_replace('{x}', count($open_restaurants), OPEN_RESTAURANTS_TITLE)); ?></h1>
	
									<?php if (order::getDeliverySchedule(strtotime($_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time'])) && 
										count($closed_restaurants)) { ?>
									<p class="text-center"><?php echo str_replace('{x}', count($closed_restaurants), SEE_MORE_RESTAURANTS); ?></p>
									<?php } ?>

									<a href="javascript:;" id="view-map"><strong><?php echo VIEW_MAP; ?></strong></a>

									<!-- Filters -->
									<div class="more-filters row">
										<div class="col-2-5t">
											<div class="form-control-group">
												<i class="fa fa-search"></i>
												<input class="rounded-input" name="search_term" placeholder="<?php echo SEARCH_HINT; ?>" value="<?php echo htmlspecialchars(@$_SESSION['s_venezvite']['search']['search_term']); ?>" type="text" />
											</div>
										</div>
										<div class="col-1-5t">
											<?php
												hs_drop_down( "time", array(
													"icon-tail" 	=> "<i class='fa fa-chevron-down'></i>",
													"text-align" 	=> "center",
													"values" 		=> commonTimesList(@$_SESSION['s_venezvite']['search']['date'], @$_SESSION['s_venezvite']['search']['time'], null, null, true),
													"active" 		=> isset( $_SESSION['s_venezvite']['search']['time']) ? $_SESSION['s_venezvite']['search']['time'] : "",
													"callback"		=> "change"
												) );
											?>
										</div>
										<div class="col-1-5t">
											<?php
												hs_drop_down( "date", array(
													"icon-tail" 	=> "<i class='fa fa-chevron-down'></i>",
													"text-align" 	=> "center",
													"values" 		=> commonDatesList(@$_SESSION['s_venezvite']['search']['date'], true),
													"active" 		=> isset( $_SESSION['s_venezvite']['search']['date'] ) ? $_SESSION['s_venezvite']['search']['date'] : '',
													"callback"		=> 'change'
												) );
											?>
										</div>
										<div class="col-1-5t">
											<?php
												hs_drop_down( "sort", array(
													"icon-tail" 	=> "<i class='fa fa-chevron-down'></i>",
													"text-align" 	=> "center",
													"values" => array(
														'distance'			=> DISTANCE,
														'alphabetically' 	=> DELIVERY,
														'price' 			=> PRICE,
														'delivery_minimum' 	=> DELIVERY_MIN,
														'ratings' 			=> RATINGS
													),
													"active" 	=> isset( $_SESSION['s_venezvite']['search']['sort'] ) ? $_SESSION['s_venezvite']['search']['sort'] : "distance",
													"callback"		=> "change"
												) );
											?>
										</div>
									</div>

									<div id="results-list">
										<ul>
											<?php echo implode('', $open_restaurants); ?>
										</ul>

										<?php if (order::getDeliverySchedule(strtotime($_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time'])) && 
											count($closed_restaurants)) { ?>
										<h2><span class="red"><?php echo ADV_ORDERS_TITLE; ?></span></h2>

										<p><?php echo str_replace('{x}', count($closed_restaurants), ADV_ORDERS_DESC); ?></p>

										<ul>
											<?php echo implode('', $closed_restaurants); ?>
										</ul>
										<?php }	?>
									</div>

									<?php } else { // don't have matching restaurant ?>

									<h5><?php echo NO_MATCHING_RESTAURANT; ?></h5>

									<p class="recommend-restaurant text-center"><?php echo MODIFY_SEARCH ?><br />
										<strong><?php echo RECOMMEND_RESTAURANT; ?></strong></p>

									<div id="recommend-restaurant">
										<div class="row">
											<div class="col-3-12t"><input class="rounded-input" name="recommended_zip_code" placeholder="<?php echo POSTAL_CODE; ?>" value="<?php echo htmlspecialchars(@$_SESSION['s_venezvite']['search']['zip_code']); ?>" type="text" /></div>
											<div class="col-3-12t"><input class="rounded-input" name="recommended_email" placeholder="<?php echo YOUR_EMAIL; ?>" value="" type="text" /></div>
											<div class="col-3-12t"><input class="rounded-input" name="recommended_restaurant" placeholder="<?php echo RESTAURANT_NAME; ?>" value="" type="text" /></div>
											<div class="col-3-12t"><input class="rounded-red-button" type="submit" value="<?php echo SUBMIT; ?>" /></div>
										</div>
									</div>

									<div id="venezvite-cities">
										<div class="vcmap"></div>
										<div class="overlays">
											<h6><?php echo CITIES_WERE_IN; ?></h6>
											<ul>
												<?php require 'inc/cities-list.inc.php'; ?>
											</ul>
										</div>
									</div>

									<?php } ?>
								</div> <!-- right -->
							</div> <!-- .clearfix -->
						</div> <!-- .main-content -->
					</div><!-- #list-body -->
				</form>

				<div class="hidden visible-tablet" id="mobile-filters">
					<div class="slide slide-1">
						<div class="headers">
							<a class="big-white-button" href="javascript:;"><?php echo CLEAR_ALL_SORT_FILTER; ?></a>
						</div>
						<div class="filters">
							<div class="filter-caption"><?php echo SORT_BY; ?></div>
							<div>
								<?php
									hs_drop_down( "sort_m", array(
										"icon-tail" 	=> "<i class='fa fa-chevron-down'></i>",
										"text-align" 	=> "center",
										"values" => array(
											'distance'			=> DISTANCE,
											'alphabetically' 	=> DELIVERY,
											'price' 			=> PRICE,
											'delivery_minimum' 	=> DELIVERY_MIN,
											'ratings' 			=> RATINGS
										),
										"active" 	=> isset( $_SESSION['s_venezvite']['search']['sort'] ) ? $_SESSION['s_venezvite']['search']['sort'] : "distance",
									) );
								?>
							</div>
							<div class="filter-caption"><?php echo FILTER_BY; ?></div>
							<div>
								<a class="cuisines" href="javascript:;">Cuisines<i class="fa fa-chevron-right"></i></a>
							</div>
							<div>
								<?php 
									hs_drop_down( "reviews", array(
										"icon-tail" 	=> "<i class='fa fa-chevron-down'></i>",
										"text-align" 	=> "center",
										"values" => array(
											''			=> ANY_RATING,
											'5'			=> '<span class="review-stars">*****</span>',
											'4' 		=> '<span class="review-stars">****</span> ' . AND_UP,
											'3' 		=> '<span class="review-stars">***</span> ' . AND_UP,
											'2' 		=> '<span class="review-stars">**</span> ' . AND_UP,
											'1' 		=> '<span class="review-stars">*</span> ' . AND_UP,
										),
										"active" 	=> isset( $_SESSION['s_venezvite']['search']['sort'] ) ? $_SESSION['s_venezvite']['search']['sort'] : "distance",
									) );
								?>	
							</div>
							<div>
								<?php 
									hs_drop_down( "prices", array(
										"icon-tail" 	=> "<i class='fa fa-chevron-down'></i>",
										"text-align" 	=> "center",
										"values" => array(
											''			=> ANY_PRICE,
											'1'			=> '<span class="price-range">$</span>' . AND_LESS,
											'2'			=> '<span class="price-range">$$</span>' . AND_LESS,
											'3'			=> '<span class="price-range">$$$</span>' . AND_LESS,
											'4'			=> '<span class="price-range">$$$$</span>' . AND_LESS,
											'5'			=> '<span class="price-range">$$$$$</span>' . AND_LESS,
										),
										"active" 	=> isset( $_SESSION['s_venezvite']['search']['sort'] ) ? $_SESSION['s_venezvite']['search']['sort'] : "distance",
									) );
								?>
							</div>
						</div>
						<div class="commands">
							<a class="half-button btn-cancel" href="javascript:;"><?php echo CANCEL; ?></a>
							<a class="action half-button btn-apply" href="javascript:;"><?php echo APPLY; ?></a>
						</div>
					</div>
					<div class="slide slide-2">
						<div class="headers">
							<input class="rounded-input" name="search_cuisines" placeholder="<?php echo SEARCH_CUISINES; ?>" value="" type="text">
							<a class="big-white-button" href="javascript:;"><?php echo CLEAR_ALL; ?></a>
						</div>
						<ul class="filters">
						</ul>
						<div class="commands">
							<a class="half-button btn-cancel" href="javascript:;"><?php echo CANCEL; ?></a>
							<a class="action half-button btn-apply" href="javascript:;"><?php echo APPLY; ?></a>
						</div>
					</div>
				</div>

				<script>var restaurant_locations = <?php echo json_encode($restaurant_locations); ?>, 
langViewMap = '<?php echo VIEW_MAP; ?>', 
langHideMap = '<?php echo HIDE_MAP; ?>', 
langShowMore = '<?php echo SHOW_MORE; ?>', 
langShowLess = '<?php echo SHOW_LESS; ?>', 
langNoDelivery = '<?php echo str_replace('\'', '\\\'', NO_DELIVERY); ?>', 
langClosed = '<?php echo CLOSED_ALL_WEEK; ?>';</script>
<?php
	}
