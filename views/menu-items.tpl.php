<?php
	if (@$is_included && !empty($restaurantAdmin) && @is_array($menu_categories) && @is_array($menu_options_groups) && @is_array($menu_items)) {
		echo '<div class="main-content">';
		require_once 'inc/logged-restaurant-header.inc.php';
?>
				<div id="menu-items-body">
						<!--h1><?php echo MENU; ?></h1-->

						<form enctype="multipart/form-data" id="add-menu-item-form" method="post">
							<div class="white-container">
								<input name="id" type="hidden" value="" />
								<strong class="title"><?php echo ADD_ITEM; ?></strong>
								
								<label class="half"><select class="rounded-select" name="category">
										<option value=""><?php echo SELECT_CATEGORY; ?></option>
<?php
		foreach ($menu_categories as $menu_category) {
			echo '<option value="', htmlspecialchars($menu_category->idMenuCategory), '">', htmlspecialchars($menu_category->category), '</option>';
		}
?>
										<option value="-1">... <?php echo NEW_CATEGORY; ?></option>
									</select></label>
								<label class="half right"><input class="rounded-input" maxlength="100" name="menu_item" placeholder="<?php echo MENU_ITEM_NAME; ?>" value="" type="text" /></label>
								<label class="clear half">
									<a class="rounded-element" href="javascript:;" id="select-photo"><strong><?php echo CHOOSE_PHOTO; ?></strong></a>
									<div id="selected-photo"></div>
									<input accept="image/*" name="photo" type="file" />
								</label>
								<label class="half right"><textarea class="rounded-textarea" maxlength="550" name="description" placeholder="<?php echo MENU_ITEM_DESCRIPTION; ?>"></textarea></label>
								
								<div class="half left">
									<label class="half left"><input class="rounded-input" maxlength="10" name="price" placeholder="<?php echo str_replace('{$currency}', $restaurantAdmin->restaurant->currency, PRICE_CURRENCY); ?>" value="" type="text" /></label>
									<label class="half right"><input class="rounded-input" maxlength="10" name="discount" placeholder="<?php echo DISCOUNT_PERCENT; ?>" value="" type="text" /></label>
									<div class="clear"></div>
								</div>
								
								<div class="food-types nowrap">
									<label class="quarter left"><a class="pill-element" href="javascript:;"><span class="food-type spicy"><?php echo SPICY; ?></span></a>
										<input name="spicy" type="checkbox" value="Y" /></label>
									<label class="quarter right"><a class="pill-element" href="javascript:;"><span class="food-type lact-free"><?php echo NO_LACTOSE; ?></span></a>
										<input name="lact_free" type="checkbox" value="Y" /></label>
									<label class="quarter right"><a class="pill-element" href="javascript:;"><span class="food-type glut-free"><?php echo NO_GLUTEN; ?></span></a>
										<input name="glut_free" type="checkbox" value="Y" /></label>
									<label class="quarter right"><a class="pill-element" href="javascript:;"><span class="food-type veggie"><?php echo VEGETARIAN; ?></span></a>
										<input name="veggie" type="checkbox" value="Y" /></label>
								</div>
								
								<label class="clear half"><select class="rounded-select" id="menu-item-option-category">
										<option value=""><?php echo ADD_OPTIONS_SET; ?></option>
<?php
		foreach ($menu_options_groups as $menu_options_group) {
			echo '<option value="', htmlspecialchars($menu_options_group->idMenuItemCategory), '">', htmlspecialchars($menu_options_group->menuItemCategory), ' (', count($menu_options_group->menuItemOptions), ' options)</option>';
		}
?>
										<option value="-1">... <?php echo NEW_OPTIONS_SET; ?></option>
									</select></label>
								<div class="clear" id="menu-item-option-categories"></div>
								<input name="option_groups" type="hidden" value="" />
								
								<label class="clear half"><?php echo RECOMMENDED; ?> <input name="recommended" type="checkbox" value="Y" /></label>
								<label class="half right"><?php echo ENABLED; ?> <input checked="checked" name="enabled" type="checkbox" value="Y" /></label>
								
								<label class="clear half"><input class="rounded-red-button" type="submit" value="<?php echo ADD_ITEM_BUTTON; ?>" /></label>
								<label class="half right"><input class="disabled rounded-red-button" type="button" value="<?php echo CANCEL; ?>" /></label>
							</div>
						</form>
						
						<div class="white-container" id="menu-items">
<?php
		if (count($menu_categories)) {
			foreach ($menu_categories as $menu_category) {
				$used_category = false;
				foreach ($menu_items as $menu_item) {
					if ($menu_item->menuCategory->idMenuCategory==$menu_category->idMenuCategory) {
						$used_category = true;
						break;
					}
				}
				
				if ($used_category) {
					echo '<div class="menu-item-category" data-id="', $menu_category->idMenuCategory, '">', 
						'<span class="drag-icon"></span>', 
						'<h2>', htmlspecialchars($menu_category->category), '</h2>', 
						'<ul class="menu-items">';
					
					foreach ($menu_items as $menu_item) {
						if ($menu_item->menuCategory->idMenuCategory==$menu_category->idMenuCategory) {
							
							$menu_item_options_groups = array();
							foreach ($menu_item->menuItemGroups as $menu_item_options_group) {
								$menu_item_options_groups[] = $menu_item_options_group->idMenuItemCategory;
							}
							
							$menu_item_object = json_encode(array(
									'id'			=> $menu_item->idMenuItem, 
									'category'		=> $menu_item->menuCategory->idMenuCategory, 
									'option_groups'	=> $menu_item_options_groups, 
									'menu_item'		=> $menu_item->menuItemName, 
									'photo'			=> $menu_item->photo, 
									'description'	=> $menu_item->menuItemDescription, 
									'price'			=> $menu_item->price, 
									'discount'		=> $menu_item->discount, 
									'spicy'			=> $menu_item->spicy, 
									'veggie'		=> $menu_item->vegetarian, 
									'glut_free'		=> $menu_item->glutenFree, 
									'lact_free'		=> $menu_item->lactoseFree, 
									'recommended'	=> $menu_item->recommended, 
									'enabled'		=> $menu_item->enabled
								));
							
							$menu_item_specs = $menu_item->getSpecsList();
							
							echo '<li', ($menu_item->enabled=='Y' ? '' : ' class="disabled"'), ' data-menu-item="', htmlspecialchars($menu_item_object), '" data-id="', $menu_item->idMenuItem, '">', 
									'<div class="left"><img src="', IMG, 'dot.png"', (!empty($menu_item->photo) ? ' style="background-image:url(\'' . IMG . 'restaurants/' . $menu_item->photo . '\')"' : ''), ' /></div>', 
									'<div class="left"><span class="drag-icon"></span>', 
										'<strong class="name">', htmlspecialchars($menu_item->menuItemName), $menu_item_specs['html'], '</strong>
										<p>', htmlspecialchars($menu_item->menuItemDescription), '</p>';
							
							if (count($menu_item->menuItemGroups)) {
								foreach ($menu_item->menuItemGroups as $menu_item_group) {
									$menu_item_options = array();
									foreach ($menu_item_group->menuItemOptions as $menu_item_option) {
										$menu_item_options[] = $menu_item_option->menuItemOption;
									}
									natcasesort($menu_item_options);
									
									echo '<strong><span class="red">', htmlspecialchars($menu_item_group->menuItemCategory), ':</span> ', 
										str_replace('&amp;nbsp;', '&nbsp;', 
										str_replace('&amp;bull;', '&bull;', 
										htmlspecialchars(implode(' &nbsp;&bull;&nbsp; ', $menu_item_options)))), '</strong><br />';
								}
							}
							
							echo '</div>', 
									'<div class="right">', number_format($menu_item->getPrice(), 2), '<br />', 
										'<span class="actions edit"></span><br />', 
										'<span class="actions delete"></span><br />', 
										'<span class="actions enabled"></span></div>', 
									'<div class="clear"></div>', 
								'</li>';
						}
					}
					
					echo '</ul>', 
						'</div>';
				}
			}
			
		} else {
			echo '<p>', NO_MENU_CATEGORIES, '</p>';
		}
?>
						</div>
						<script>var langAddItem = '<?php echo ADD_ITEM; ?>', langEditItem = '<?php echo EDIT_ITEM; ?>', langAddButton = '<?php echo ADD_ITEM_BUTTON; ?>', langEditButton = '<?php echo EDIT_ITEM_BUTTON; ?>', langRemoveConfirmation = '<?php echo REMOVE_CONFIRMATION; ?>';</script>
					</div>
			</div>
<?php
	}
