<?php
	if (@$is_included && !empty($best_restaurants)) {
?>
				<div id="hero-banner" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo IMG; ?>home-bg/home-bg-0<?php echo mt_rand(1, 7); ?>.jpg" data-z-index="-104">
					<div id="home-search-area">
						<div class="main-content">
							<div id="home-search-form">
								<form action="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/', RESTAURANTS_LIST_CUSTOM_PATH; ?>/restaurants-list.html" method="post">
									<h1 class="wow fadeInUp" data-wow-duration="1s"><?php echo HUNGRY_TITLE; ?></h1>

									<div>
										<?php
											hs_drop_down( "type", array(
												"icon-tail" => "<i class='fa fa-chevron-down'></i>",
												"text-align" => "center",
												"values" => array(
													'D' => DELIVERY,
													'T' => TAKEAWAY
												),
												"active" => "D"
											) );
										?>

										<?php require_once 'views/inc/search-location.inc.php'; ?>

										<input type="submit" value="<?php echo FIND; ?>" />
									</div>
								</form>

								<?php if (empty($entity)) { ?>
								<a href="javascript:;" id="home-login"><?php echo ALREADY_MEMBER; ?></a>
								<?php } ?>
							</div>
						</div>
					</div>
					
					<div id="lunch-check"><?php echo NOW_ACCEPTING; ?><br />
						<strong>Lunch-Check!</strong></div>
					
					<a href="javascript:void(0);" class="slide-down"><img src="<?php echo IMG;?>/icons/slide-down.png" /></a>
				</div>

				<div id="home-top-callouts">
					<div>
						<div class="main-content">
							<ul class="bullet-panel">
								<li class="bullet-item wow slideInLeft"><span></span><strong><?php echo EASY_ORDERING_TITLE; ?></strong>
									<?php echo EASY_ORDERING_BODY; ?></li><!-- To remove the empty space between inline-block elements
								--><li class="bullet-item wow slideInLeft"><span></span><strong><?php echo MORE_FOOD_TITLE; ?></strong>
									<?php echo MORE_FOOD_BODY; ?></li><!--
								--><li class="bullet-item wow slideInRight"><span></span><strong><?php echo DISCOUNTS_TITLE; ?></strong>
									<?php echo DISCOUNTS_BODY; ?></li><!--
								--><li class="bullet-item wow slideInRight"><span></span><strong><?php echo TELL_FRIENDS_TITLE; ?></strong>
									<?php echo TELL_FRIENDS_BODY; ?></li>
								<div class="clear"></div>
							</ul>
							<!--div class="bullet-links"></div-->
						</div>
					</div>
				</div>

				<div id="home-fb">
					<div class="main-content">
						<strong><?php echo DONT_BE_SHY; ?></strong>
						<?php echo CLICKS_AWAY; ?>

						<div class="fb-like" data-header="false" data-href="http://www.facebook.com/likevenezvite" data-send="false" data-show-faces="false" data-stream="false" data-width="320"></div>
					</div>
				</div>

				<div id="home-corporate" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo ROOT ?>i/home-corporate-bg.jpg" data-z-index="-103">
					<div class="main-content">
						<h2><?php echo nl2br(PERFECT_BUSINESS); ?></h2>
						<?php echo CREATE_CORP_ACCOUNT; ?>
						<a class="rounded-red-button" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/corporate-join.html"><?php echo CREATE_CORP_ACCOUNT2; ?></a>
					</div>
				</div>

				<div id="home-best-restaurants" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo ROOT ?>i/home-best-restaurants.jpg" data-z-index="-102">
					<div>
						<div class="main-content">
							<h3><?php echo BEST_RESTAURANT ?></h3>
							<ul class="bullet-panel">
<?php
		foreach ($best_restaurants as $index => $best_restaurant) {
			if ($index > 2) {
				// Maximum 3 restaurants
				break;
			}
			$url = ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/' . $best_restaurant->customURL . '.html';
								?><li class="bullet-item"><div>
										<a href="<?php echo $url; ?>"><img src="<?php echo IMG; ?>dot.png" style="background-image:url('<?php echo IMG . 'restaurants/' . $best_restaurant->images[0]->file; ?>')" />
										<strong title="<?php echo htmlspecialchars($best_restaurant->restaurantName); ?>"><?php echo htmlspecialchars($best_restaurant->restaurantName); ?></strong>
										<div class="nowrap"><?php echo htmlspecialchars(implode(', ', $best_restaurant->getCuisineTypesList())); ?></div>
										<span class="review-stars"><?php echo str_repeat('*', round($best_restaurant->rating)); ?></span> (<?php echo $best_restaurant->reviews, ' ', REVIEWS; ?>)<br />
										<span class="price-range"><?php echo str_repeat('$', $best_restaurant->priceLevel); ?></span></a>
									</div></li><?php
		}
?>
							</ul>
							<!--div class="bullet-links"></div-->
						</div>
					</div>
				</div>

				<!--div id="home-social">
					<div class="main-content">
						<h3><?php echo STAY_CONNECTED; ?> #venezvite</h3>
						<ul>
							<li><a class="fb" href="https://www.facebook.com/likevenezvite"></a></li>
							<li><a class="twitter" href="https://twitter.com/venezvite"></a></li>
							<li><a class="instagram" href="https://instagram.com/venezvite/"></a></li>
							<li><a class="gplus" href="https://plus.google.com/+Venezvite"></a></li>
						</ul>

						<strong><?php echo NEWSLETTER_TITLE; ?></strong>
						<p><?php echo NEWSLETTER_BODY; ?></p>

						<form method="post">
							<div>
								<input class="big-rounded-input" name="email" placeholder="<?php echo ENTER_EMAIL; ?>" type="text" /><input type="button" value="<?php echo SUBMIT; ?>" />
							</div>
						</form>
					</div>
				</div-->

				<div id="home-works">
					<div class="main-content">
						<h4><?php echo HOW_WORKS_TITLE; ?></h4>
						
						<ul>
							<li><?php echo HOW_WORKS_BODY1; ?></li>
							<li><?php echo HOW_WORKS_BODY2; ?></li>
							<li><?php echo HOW_WORKS_BODY3; ?></li>
						</ul>
					</div>
				</div>
				
				<div id="how-partner-image" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo ROOT ?>i/homepage-how-partner-bg.jpg" data-z-index="-101"></div>
				
				<div id="home-partners">
					<div class="main-content">
						<h5><?php echo nl2br(HELLO_RESTAURANT_OWNERS); ?></h5>
						<a class="rounded-red-button" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/restaurant-join.html"><?php echo JOIN_NOW; ?></a>
						
						<img id="process-icons" src="<?php echo IMG; ?>dot.png" />
						
						<strong><?php echo NO_DELIVER_TITLE; ?></strong>
						<p><?php echo NO_DELIVER_BODY; ?></p>
						<p><?php echo JOIN_BODY; ?></p>
						
						<h6>#venezvite</h6>
						<p><?php echo ABOUT_BODY; ?></p>
						
						<ul>
							<li>
								<a class="fb" href="https://www.facebook.com/likevenezvite">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a class="twitter" href="https://twitter.com/venezvite">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<li>
								<a class="instagram" href="https://instagram.com/venezvite/">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
							<li>
								<a class="gplus" href="https://plus.google.com/+Venezvite">
									<i class="fa fa-google-plus"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				
				<!--div id="home-about">
					<div class="main-content">
						<h6><?php echo ABOUT_TITLE; ?></h6>
						<p><?php echo ABOUT_BODY; ?></p>
					</div>
				</div-->
				
				<div id="home-partner-login">
					<div class="main-content">
						<a class="rounded-red-button" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/login-restaurant.html"><?php echo PARTNER_LOGIN; ?></a>
					</div>
				</div>
<?php
	}
