<?php
	if (@$is_included && !empty($restaurantAdmin) && @is_array($menu_categories) && @is_array($menu_items)) {
		echo '<div class="main-content">';
		require_once 'inc/logged-restaurant-header.inc.php';
?>
				<div id="menu-categories-body">
						<!--h1><?php echo MENU_CATEGORIES; ?></h1-->
						<form id="add-menu-category-form" method="post">
							<div class="white-container">
								<input name="id" type="hidden" value="" />
								<strong class="title"><?php echo ADD_CATEGORY; ?></strong>
								
								<label class="half"><input class="rounded-input" maxlength="50" name="category" value="" type="text" /></label>
								
								<label class="clear half"><input class="rounded-red-button" type="submit" value="<?php echo ADD_CATEGORY_BUTTON; ?>" /></label>
								<label class="half right"><input class="disabled rounded-red-button" type="button" value="<?php echo CANCEL; ?>" /></label>
							</div>
						</form>
<?php
		if (count($menu_categories)) {
			echo '<ul id="menu-categories">';
			
			foreach ($menu_categories as $menu_category) {
				$used_menu_items = array();
				foreach ($menu_items as $menu_item) {
					if ($menu_item->menuCategory->idMenuCategory==$menu_category->idMenuCategory) {
						$used_menu_items[] = $menu_item->menuItemName;
					}
				}
				
				$menu_category_object = json_encode(array(
						'id'		=> $menu_category->idMenuCategory, 
						'category'	=> $menu_category->category
					));
				
				echo '<li data-menu-category="', htmlspecialchars($menu_category_object), '" data-id="', $menu_category->idMenuCategory, '" data-menu-items="', htmlspecialchars(json_encode($used_menu_items)), '">', 
						'<div class="left"><span class="drag-icon"></span>', 
							'<strong class="name">', htmlspecialchars($menu_category->category), '</strong></div>', 
						'<div class="right">', 
							'<span class="actions edit"></span><br />', 
							'<span class="actions delete', (count($used_menu_items) ? ' disabled' : ''), '"></span>', 
						'</div>', 
						'<div class="clear"></div>', 
					'</li>';
			}
			
			echo '</ul>';
			
		} else {
			echo '<div class="white-container">', 
					'<p>', NO_MENU_CATEGORIES, '</p>', 
				'</div>';
		}
?>
						<script>var langAddItem = '<?php echo ADD_CATEGORY; ?>', langEditItem = '<?php echo EDIT_ITEM; ?>', langAddButton = '<?php echo ADD_CATEGORY_BUTTON; ?>', langEditButton = '<?php echo EDIT_ITEM_BUTTON; ?>', langDuplicateCategory = '<?php echo str_replace('\'', '\\\'', DUPLICATE_ALERT) ; ?>', langDisabledCategory = '<?php echo str_replace('\'', '\\\'', DISABLED_ALERT) ; ?>', langRemoveConfirmation = '<?php echo REMOVE_CONFIRMATION; ?>';</script>
					</div>
				</div>
<?php
	}
