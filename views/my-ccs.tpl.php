<?php
	if (@$is_included && !empty($entity) && isset($user_payment_tokens) && is_array($user_payment_tokens)) {
		echo '<div class="main-content">';
		require_once 'inc/logged-user-header.inc.php';
?>
				<div id="my-ccs-body">
						<a class="rounded-red-button" data-related="add-cc" href="javascript:;" id="add-cc-button">Add new payment</a>
						<!--h1><?php echo PAYMENTS; ?></h1-->
						
						<form id="add-cc-form" method="post">
							<div class="white-container" id="add-cc">
								<strong class="title">Add New Payment</strong>
								
								<div class="left" id="cc-logos"><span class="visa"></span><span class="mc"></span><span class="amex"></span></div>
								<div class="clear"></div>
								
								<label class="half"><span>&nbsp;</span>
									<input class="rounded-input" maxlength="50" name="name" placeholder="Name on Card *" value="" type="text" /></label>
								<label class="half right"><span>Currency (optional)</span>
									<select class="rounded-select" name="currency">
										<option value="CHF">CHF</option>
										<option value="EUR">EUR</option>
										<option value="USD">USD</option>
									</select></label>
								
								<label class="clear half"><span>&nbsp;</span>
									<select class="rounded-select" name="cc_type">
										<option value="">Credit Card Type*</option>
										<option value="Visa">Visa</option>
										<option value="MasterCard">MasterCard</option>
										<option value="Amex">American Express</option>
									</select></label>
								<label class="half right"><span>&nbsp;</span>
									<input class="rounded-input" maxlength="20" name="name" placeholder="Credit Card Number *" value="" type="text" /></label>
								<label class="clear quarter"><span>Expiration date</span>
									<select class="rounded-select" name="cc_month">
										<option value="">MM</option>
<?php
		for ($i=1; $i<=12; $i++) {
			$month = $i;
			if ($month<10) {
				$month = '0' . $month;
			}
?>
										<option value="<?php echo $month; ?>"><?php echo $month; ?></option>
<?php
		}
?>
									</select>
									<select class="rounded-select" name="cc_year">
										<option value="">YY</option>
<?php
		for ($i=date('Y'); $i<(date('Y') + 15); $i++) {
			$year = substr($i, 2);
?>
										<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
<?php
		}
?>
									</select></label>
								<?php /* ?><label class="quarter"><span>CVC</span>
									<input class="rounded-input" maxlength="4" name="cvc" placeholder="XXX" value="" type="text" /></label> */ ?>
								
								<div class="clear"></div>
								<a class="disabled rounded-red-button" data-related=""><?php echo CANCEL; ?></a><a class="rounded-red-button"><?php echo APPLY; ?></a><a class="delete rounded-red-button"><?php echo 'Delete Payment Method'; ?></a>
							</div>
						</form>
<?php
		if ($user_payment_tokens) {
			foreach ($user_payment_tokens as $user_payment_token) {
				$token_length = strlen($user_payment_token->token) - 4;
				if ($token_length < 0) {
					$token_length = 0;
				}
?>
						<div class="white-container">
							<a class="green-button" data-id="<?php echo $user_payment_token->id; ?>" data-related="edit-cc" href="javascript:;"><?php echo EDIT; ?></a>
							
							<strong class="title"><?php echo $user_payment_token->card_type; ?></strong>
							<?php echo str_repeat('*', $token_length), substr($user_payment_token->token, -4), '<br />', 
								'Expires ', date('F Y', strtotime($user_payment_token->expiration_date)); ?>
						</div>
<?php
			}
			
		} else {
			echo '<p>You have no credit cards stored in our system yet.</p>';
		}
?>
						
					</div>
				</div>
<?php
	}
