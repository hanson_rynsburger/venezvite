<?php
	if (@$is_included) {
		require_once 'inc/restaurant-join-menu.inc.php';
?>
				<div id="how-it-works-body">
					<div class="main-content">
						
						<div class="centered" id="intro-text">
							<h2><?php echo FIND_NEW_CUSTOMERS; ?></h2>
							<p><?php echo JOIN_PRESENTATION; ?></p>
							<a class="inline rounded-red-button" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/restaurant-join.html"><?php echo JOIN_BUTTON; ?></a>
							
							<h3><?php echo HOW_IT_WORKS; ?></h3>
						</div>
						<div class="centered left third">
							<img alt="" class="border" src="<?php echo IMG; ?>order-to-restaurant.png" /><br />
							<strong><?php echo INSTANT_PRESENCE_TITLE; ?></strong>
							<p><?php echo INSTANT_PRESENCE_DESC; ?></p>
						</div>
						<div class="centered left third">
							<img alt="" src="<?php echo IMG; ?>prepare-food.png" /><br />
							<strong><?php echo PREPARE_FOOD_TITLE; ?></strong>
							<p><?php echo PREPARE_FOOD_DESC; ?></p>
						</div>
						<div class="centered left third">
							<img alt="" src="<?php echo IMG; ?>currency.png" /><br />
							<strong><?php echo GET_PAID_TITLE; ?></strong>
							<p><?php echo GET_PAID_DESC; ?></p>
						</div>
						<div class="clear"></div>
						
					</div>
				</div>
<?php
	}
