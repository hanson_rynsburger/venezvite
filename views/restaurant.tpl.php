<?php
	if (@$is_included && !empty($restaurant) && /*isset($popularity) && */!empty($open_status) && !empty($search_datetime) && isset($current_date_hours) && !empty($menu_categories)) {
?>
				<div id="list-search-form">
					<form method="post">
						<strong><?php echo WHERE_WHEN; ?></strong>
						<a class="close" href="javascript:;" title="Close">&times;</a>
						<p><?php echo WHEN_WHERE_DESC; ?></p>
						<label>
							<select class="rounded-select" name="type">
								<?php if ($restaurant->delivery=='Y') { ?>
								<option value="D"><?php echo DELIVERY; ?></option>
								<?php } ?>
								<option<?php echo @$_SESSION['s_venezvite']['search']['type']=='T' ? ' selected="selected"' : ''; ?> value="T"><?php echo TAKEAWAY; ?></option>
							</select>
						</label>
						<label>
							<?php require_once 'views/inc/search-location.inc.php'; ?>
						</label>
						<label>
							<select class="rounded-select" name="time">
								<?php commonTimesList(@$_SESSION['s_venezvite']['search']['date'], @$_SESSION['s_venezvite']['search']['time'], @$restaurant->current_delivery_zone['estimated_time'], @$restaurant->preparationTime); ?>
							</select>
						</label>
						<label>
							<select class="rounded-select" name="date">
								<?php commonDatesList(@$_SESSION['s_venezvite']['search']['date']); ?>
							</select>
						</label>

						<input class="rounded-red-button" type="submit" value="Select" />
					</form>
				</div>
				<div id="restaurant-intro-image"<?php echo count($restaurant->images) ? ' style="background-image:url(\'' . IMG . 'restaurants/' . $restaurant->images[0]->file . '\')"' : ''; ?>>
				</div>
				<div id="restaurant-intro">
					<div class="main-content">
						<h1 class="clear" itemprop="name"><?php echo htmlspecialchars($restaurant->restaurantName); ?></h1>
						<?php foreach($restaurant->images as $image) { ?>
							<a href="<?php echo IMG . "restaurants/" . $image->file?>" class="view-images hidden" data-lightbox="restaurant"><?php echo VIEW_IMAGES ?></a>
						<?php } ?>
					</div>
				</div>
			<div class="restaurant-menu-main-content">
				<div id="restaurant-main-menu">
					<div class="main-content">
						<ul>
							<li><a data-href="restaurant-menu-container" href="#restaurant-menu-list"><?php echo MENU; ?></a></li>
							<li><a data-href="restaurant-details" href="#restaurant-details"><?php echo MORE_INFO; ?></a></li>
						</ul>
					</div>
				</div>
				<div id="restaurant-stats">
					<div class="main-content">
						<div class="right" id="like-boxes">
							<div class="fb-like" data-href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/', $restaurant->customURL; ?>.html" data-send="false" data-layout="button_count" data-width="90" data-show-faces="false"></div>
							<div class="g-plusone" data-size="medium" data-annotation="inline" data-width="120" data-href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/', $restaurant->customURL; ?>.html"></div>
						</div>

						<div class="left" id="favorite">
							<?php if ( empty($entity) ) { // if not logged in, just open up login modal ?>
							<a href="javascript:;" class='open-login-modal'><?php echo ADD_TO_FAVES; ?></a>
							<?php } else if ( $is_faved ) { ?>
								<a href="javascript:;" class="faved to-be-unfaved" data-faved-lang="<?php echo ADDED_TO_FAVES; ?>" data-unfaved-lang="<?php echo ADD_TO_FAVES; ?>"><?php echo ADDED_TO_FAVES; ?></a>
							<?php } else { ?>
								<a href="javascript:;" class="to-be-faved" data-faved-lang="<?php echo ADDED_TO_FAVES; ?>" data-unfaved-lang="<?php echo ADD_TO_FAVES; ?>"><?php echo ADD_TO_FAVES; ?></a>
							<?php } ?>
						</div>

						<!--div class="center"><?php //echo $popularity; ?></div-->
						<div class="center meta-info"><span><?php echo htmlspecialchars(implode(', ', $restaurant->getCuisineTypesList())); ?></span></div>
						<div class="clear"></div>
						<div class="center meta-info">
							<span><span class="review-stars"><?php echo str_repeat('*', round($restaurant->rating)); ?></span> (<?php echo $restaurant->reviews, ' ', REVIEWS; ?>)</span>
							<span><strong><span class="<?php echo $restaurant->opened ? 'open' : 'closed'; ?>" id="restaurant-status"><span>&bull;</span> <?php echo $open_status; ?></span> &bull; <?php echo prettyDateTime($search_datetime, false, '', null, $restaurant->preparationTime); ?></strong><?php /* echo !empty($current_date_hours) ? ' (' . implode(' / ', $current_date_hours) . ')' : ''; */ ?></span>
							<span class="price-range"><?php echo str_repeat('$', $restaurant->priceLevel); ?></span></div>
					</div>
				</div>
				<div id="restaurant-categories-search">
					<div class="main-content">
						<input class="big-rounded-input left" placeholder="<?php echo SEARCH_HINT; ?>" type="text" />
						<div class="right"><strong><?php echo FILTER; ?></strong>
							<ul id="restaurant-menu-filter">
								<li class="spicy"><label><input name="filter[]" type="checkbox" /> <?php echo SPICY; ?><!-- (4)--></label></li>
								<li class="veggie"><label><input name="filter[]" type="checkbox" /> <?php echo VEGETARIAN; ?><!-- (2)--></label></li>
								<li class="glut-free"><label><input name="filter[]" type="checkbox" /> <?php echo NO_GLUTEN; ?><!-- (2)--></label></li>
								<li class="lact-free"><label><input name="filter[]" type="checkbox" /> <?php echo NO_LACTOSE; ?><!-- (1)--></label></li>
								<li class="photos"><label><input name="filter[]" type="checkbox" /> <?php echo FILTER_PHOTOS; ?><!-- (8)--></label></li>
							</ul></div>
						<div class="clear"></div>
					</div>
				</div>
				<div id="restaurant-content" itemscope itemtype="http://schema.org/Restaurant">
					<div class="main-content">
						<div id="restaurant-details">
							<div class="details-panel">
								<div class="left" id="restaurant-contact-data">
									<h3><?php echo htmlspecialchars($restaurant->restaurantName . ($restaurant->delivery=='Y' ? ' - ' . DELIVERY : '')); ?></h3>
									<p><?php echo htmlspecialchars($restaurant->address . ' ' . $restaurant->city . ', ' . $restaurant->zipCode); ?><br /> 
										<?php echo htmlspecialchars($restaurant->phone); ?><br />
										<?php echo DELIVERY_MIN, ': ', ($restaurant->current_delivery_zone ? $restaurant->current_delivery_zone['minimum_delivery'] : '0.00'), ' ', $restaurant->currency; ?><br />
										<?php echo DELIVERY_FEE, ': ', ($restaurant->current_delivery_zone ? $restaurant->current_delivery_zone['delivery_fee'] : '0.00'), ' ', $restaurant->currency; ?><br />

										<?php
										if ($restaurant->website) {
											if (strpos($restaurant->website, 'http')!==0) {
												$website = 'http://' . $restaurant->website;
											} else {
												$website = $restaurant->website;
											}
										?>
										<br />
										<a class="popup" href="<?php echo $website; ?>"><?php echo VISIT_WEB; ?></a>
										<?php 
										} ?></p>
								</div>
								<div class="right" id="restaurant-map">
									<p><?php echo SERVING_IN, ' ', htmlspecialchars($restaurant->city); ?></p>
									<div data-lat="<?php echo $restaurant->latitude; ?>" data-lng="<?php echo $restaurant->longitude; ?>"></div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="details-panel">
								<h3><?php echo HOURS_DELIVERY_ZONES; ?></h3>
								<?php
								if (!empty($restaurant->timetable)) {
								?>
								<table class="left">
									<caption><?php echo TAKEAWAY_HOURS; ?></caption>
									<tbody>
									<?php
										$timestamp = strtotime('next Monday');
										for($i=1; $i<=7; $i++){
										$operation_hours = array();
											foreach ($restaurant->timetable as $timetable) {
												if ($timetable['dow']==$i) {
													// The restaurant is open for the current day of week
													$opening_hour = substr($timetable['openingHour'], 0, 5);
													$closing_hour = substr($timetable['closingHour'], 0, 5);
													$operation_hours[] = $opening_hour . '-' . $closing_hour;
												}
											}
									?>
										<tr<?php echo (date('N', strtotime($search_datetime))==$i ? ' class="current"' : ''), (count($operation_hours)>0 ? ' itemprop="openingHours" datetime="' . substr(date('D', $timestamp), 0, 2) . ' ' . implode('/', $operation_hours) . '"' : ''); ?>>
											<td><?php echo ucfirst(strftime('%A', $timestamp)); ?></td>
											<td><?php echo count($operation_hours) ? implode(' / ', $operation_hours) : CLOSED; ?></td>
										</tr>
									<?php
											$timestamp = strtotime('+1 day', $timestamp);
										}
									?>
									</tbody>
								</table>
								<?php
								}
								if (!empty($restaurant->deliveryHours)) {
								?>
								<table class="left">
									<caption><?php echo DELIVERY_HOURS; ?></caption>
									<tbody>
									<?php
										$timestamp = strtotime('next Monday');
										for ($i=1; $i<=7; $i++) {
											$delivery_hours = array();

											foreach ($restaurant->deliveryHours as $delivery_hour) {
												if ($delivery_hour['dow']==$i) {
													// The restaurant has a delivery schedule for the current day of week
													$hour_start = substr($delivery_hour['hourStart'], 0, 5);
													$hour_end = substr($delivery_hour['hourEnd'], 0, 5);
													
													$delivery_hours[] = $hour_start . '-' . $hour_end;
												}
								 			}
									?>
										<tr<?php echo date('N', strtotime($search_datetime))==$i ? ' class="current"' : ''; ?>>
											<td><?php echo ucfirst(strftime('%A', $timestamp)); ?></td>
											<td><?php echo count($delivery_hours) ? implode(' / ', $delivery_hours) : NO_DELIVERY; ?></td>
										</tr>
										<?php
											$timestamp = strtotime('+1 day', $timestamp);
										}
										?>
									</tbody>
								</table>
								<?php
								}
								?>
								<div class="clear"></div>
								<?php
								$delivery_zones_paths = array();
								if (!empty($restaurant->deliveryZones)) {
								?>
								<table>
									<caption><?php echo DELIVERY_ZONES; ?></caption>
									<tbody>
									<?php
									foreach ($restaurant->deliveryZones as $index => $delivery_zone) {
									?>
										<tr<?php echo $restaurant->current_delivery_zone && $restaurant->current_delivery_zone['id']==$delivery_zone->idDeliveryZone ? ' class="current"' : ''; ?>>
											<td>Zone <?php echo ($index + 1); ?></td>
											<td><?php echo DELIVERY_MIN; ?>: <?php echo $delivery_zone->minimumDelivery, ' ', $restaurant->currency; ?></td>
											<td><?php echo DELIVERY_FEE; ?>: <?php echo $delivery_zone->deliveryFee, ' ', $restaurant->currency; ?></td>
										</tr>
									<?php
										$delivery_zones_paths[] = json_decode($delivery_zone->coordinates);
									} ?>
									</tbody>
								</table>
								<?php
								} ?>
							</div>
							<div class="details-panel">
								<h3 class="left"><?php echo ABOUT; ?></h3>
								<p class="right" itemprop="description"><?php echo nl2br(htmlspecialchars($restaurant->restaurantDescription)); ?></p>
								<div class="clear"></div>
							</div>
						</div>
						<div id="restaurant-menu-container">
							<!--div class="left"-->
								<ul id="restaurant-menu-list">
									<?php
									foreach ($menu_categories as $id_menu_category => $menu_category_name) {
									?>
									<li><strong class="category-name"><?php echo htmlspecialchars(key($menu_category_name)); ?></strong>
										<ul>
											<?php $cid = 0;
											foreach (current($menu_category_name) as $menu_item) {
												$menu_item_specs = $menu_item->getSpecsList();
												$menu_item_details = $menu_item->getDetails();

												$image = '';
												if ($id_menu_category=='0' && !empty($menu_item->photo)) {
													$image = '<img class="left" src="' . IMG . 'restaurants/' . $menu_item->photo . '" />';
												}
											?>
											<li class="left" data-details="<?php echo htmlspecialchars($menu_item_details); ?>" data-menu-item-id="<?php echo $menu_item->idMenuItem; ?>"><div data-filter="<?php echo join(',', $menu_item_specs['classes']); ?>">
												<div class="left"><?php echo $image; ?>
													<strong class="title"><?php echo htmlspecialchars($menu_item->menuItemName), $menu_item_specs['html']; ?></strong>
													<p><?php echo htmlspecialchars($menu_item->menuItemDescription); ?></p></div>
													<div class="right"><?php echo number_format($menu_item->getPrice(), 2); ?></div>
													<div class="clear"></div>
												</div>
											</li>
											<?php
												$cid ++;
											} 
											if ( $cid % 2 == 1 ) {
											?>
											<li class="menu-placeholder"></li>
											<?php } ?>
										</ul>
									</li>
									<?php 
									} ?>
									<li id="no-results"><p><?php echo NO_MENUS_FOR_SEARCH; ?></p></li>
								</ul>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<?php require_once REL . DS . 'views' . DS . 'inc' . DS . 'reviews.tpl.php'; ?>
				<form id="menu-item-popup" method="post">
					<div id="menu-item-details">
						<input name="menu_item_id" type="hidden" />
						<input name="edit" type="hidden" />
						<!--strong><?php echo ADD_ITEM; ?></strong> -->
						<a class="close" href="javascript:;" title="Close">&times;</a>
						<div id="menu-item-photo">
							<div><img src="<?php echo IMG; ?>dot.png" /></div>
							<a href="javascript:;"><?php echo SHOW_IMAGE; ?></a>
						</div>
						<div id="menu-item-description">
							<strong></strong>
							<p></p>
							<!--a href="javascript:;"><?php echo VIEW_DETAILS; ?></a-->
							<div id="menu-item-price"></div>
						</div>
						<div id="menu-item-actions">
							<div id="menu-item-quantity">
								<a href="javascript:;">-</a>
								<input name="quantity" readonly="readonly" type="text" value="1" />
								<a href="javascript:;">+</a>
							</div>
							<input class="rounded-red-button" type="submit" value="<?php echo ADD2BAG; ?>" />
							<!-- <a class="close disabled left rounded-red-button"><?php echo CANCEL; ?></a> -->
							<div class="clear"></div>
						</div>
						<div id="menu-item-options">
							<span class="ribbon"></span>
							<div></div>
							<strong><?php echo SPECIAL_INSTRUCTIONS; ?> <small><?php echo SPECIAL_INSTRUCTIONS_EG; ?></small></strong>
							<textarea cols="80" name="instructions" rows="3"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div id="write-review-modal" class="modal-body">
				<a class="close" href="javascript:;">&times;</a>
				<span class="title"><?php echo WRITE_REVIEW ?></span>
				<form method="post">
					<div class="slider-container">
						<ul class="slides">
							<li class="item slider-1">
								<div class="desc">
									<?php echo DELIVERED_ON_TIME ?>
								</div>
								<div class="answer yes-no clearfix">
									<input type="radio" value="No" name="isOnTime" />
									<input type="radio" value="Yes" name="isOnTime" />
								</div>
								<div class="skip"><a href="javascript:void(0)"><?php echo SKIP_QUESTION ?></a></div>
							</li>
							<li class="item slider-2">
								<div class="desc">
									<?php echo ORDER_CORRECT ?>
								</div>
								<div class="answer yes-no clearfix">
									<input type="radio" value="No" name="isCorrect" />
									<input type="radio" value="Yes" name="isCorrect" />
								</div>
								<div class="skip"><a href="javascript:void(0)"><?php echo SKIP_QUESTION ?></a></div>
							</li>
							<li class="item slider-3">
								<div class="desc">
									<?php echo FOOD_GOOD ?>
								</div>
								<div class="answer yes-no clearfix">
									<input type="radio" value="No" name="isGood" />
									<input type="radio" value="Yes" name="isGood" />
								</div>
								<div class="skip"><a href="javascript:void(0)"><?php echo SKIP_QUESTION ?></a></div>
							</li>
							<li class="item slider-4">
								<div class="desc">
									<p><?php echo OVERALL_EXPERIENCE ?></p>
									<div class="overall-rating">
										<input type="range" name="rating" min="0" max="5" value="0" step="1" id="newrating">
										<div class="rateit" data-rateit-backingfld="#newrating"></div>
									</div>
									<p><?php echo REVIEW_YOUR_ORDER ?></p>
									<textarea class="review-content" name="reviewContent"></textarea>
									<div class="row">
										<div class="col-6-12m">
											<a href="javascript:void(0);"><?php echo GUIDELINE; ?></a>
										</div>
										<div class="col-6-12m">
											<span class="left-characters">2000</span> <?php echo REMAINING; ?>
										</div>
									</div>
								</div>
								<div class="answer clearfix">
									<input type="hidden" name="writing-review" value="1" />
									<button class="rounded-red-button submit-review"><?php echo SUBMIT_REVIEW ?></button>
								</div>
							</li>
							<li class="item slider-5">
								<div class="desc">
									<p><?php echo THANK_YOU_REVIEW ?></p>
								</div>
							</li>
						</ul>
					</div>
				</form>
			</div>
<script>(function(){var po=document.createElement('script');po.async=true;po.src='//apis.google.com/js/plusone.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(po,s);})();
var delivery_paths = <?php echo json_encode($delivery_zones_paths); ?>, 
	timetable = <?php echo json_encode($restaurant->buildDatetimeList($restaurant->timetable)); ?>, 
	delivery_hours = <?php echo json_encode($restaurant->buildDatetimeList($restaurant->deliveryHours)); ?>, 
	current_currency = '<?php echo $restaurant->currency; ?>', 
	
	langAddItem = '<?php echo ADD_ITEM; ?>', 
	langViewImage = '<?php echo str_replace('\'', '\\\'', SHOW_IMAGE); ?>', 
	langHideImage = '<?php echo str_replace('\'', '\\\'', HIDE_IMAGE); ?>', 
	langEmptyCart = '<?php echo EMPTY_BAG; ?>', 
	langAdd2Basket = '<?php echo ADD2BAG; ?>', 
	langEditItem = '<?php echo EDIT_ITEM; ?>', 
	langUpdateBag = '<?php echo UPDATE_BAG; ?>', 
	langSelectExact = '<?php echo OPTIONS_EXACTLY; ?>', 
	langSelectAtLeast = '<?php echo OPTIONS_AT_LEAST; ?>', 
	langSelectUpTo = '<?php echo str_replace('\'', '\\\'', OPTIONS_UP_TO); ?>', 
	langItemAdded = '<?php echo str_replace('\'', '\\\'', ITEM_ADDED); ?>', 
	langGoCheckout = '<?php echo str_replace('\'', '\\\'', GO2CHECKOUT); ?>';</script>
<?php
	}
