<?php
	if (@$is_included && !empty($restaurant) && isset($can_place_cash_orders) && !empty($exchange_rate)) {
?>
				<div id="restaurant-intro-image"<?php echo count($restaurant->images) ? ' style="background-image:url(\'' . IMG . 'restaurants/' . $restaurant->images[0]->file . '\')"' : ''; ?>>
				</div>
				<div id="restaurant-intro">
					<div class="main-content">
						<div class="restaurant-name"><?php echo $restaurant->restaurantName ?></div>
						<strong><?php echo CHECKOUT ?></strong>
						
						<?php foreach($restaurant->images as $image) { ?>
							<a href="<?php echo IMG . "restaurants/" . $image->file?>" class="view-images hidden" data-lightbox="restaurant"><?php echo VIEW_IMAGES ?></a>
						<?php } ?>
					</div>
				</div>

				<div id="checkout-body">
					<div class="main-content">
						<div id="error-container"<?php echo !empty($_SESSION['s_venezvite']['checkout_error']) ? ' style="height:auto"' : ''; ?>>
							<div>
								<strong><?php echo ORDER_ERRORS; ?></strong>
								<span><?php echo !empty($_SESSION['s_venezvite']['checkout_error']) ? $_SESSION['s_venezvite']['checkout_error'] : ''; ?></span>
							</div>
						</div>
						<?php
						if (!empty($_SESSION['s_venezvite']['checkout_error'])) {
							// We don't need it anymore
							$_SESSION['s_venezvite']['checkout_error'] = null;
							unset($_SESSION['s_venezvite']['checkout_error']);
						}
						?>
						<form method="post">
							<div class="" id="checkout-form">
								<div class="panel">
									<?php
									if (empty($entity)) {
									?>
									<h2>1. <?php echo CREATE_ACCOUNT, ' ', OR_; ?> <a href="javascript:;" id="checkout-login"><?php echo LOGIN; ?></a>
										<span>* (<?php echo REQUIRED; ?>)</span></h2>
									<a class="rounded-red-button" href="javascript:;" id="fb-register-button"><?php echo FB_LOGIN; ?></a>
									<div class="or-bar"><?php echo OR_; ?></div>
									<?php
									require_once 'views/inc/account-edit.inc.php';
									?>
									<label class="checkout-checkbox"><input checked="checked" name="newsletter" type="checkbox" value="Y" /> <span><?php echo NEWSLETTER_ACCEPT; ?></span><br class="clear" /></label>
									<?php
									} else {
										$name = (get_class($entity)=='user' ? $entity->firstName . ' ' . $entity->lastName : $entity->contactFirstName . ' ' . $entity->contactLastName);
										$email = (get_class($entity)=='user' ? $entity->email : $entity->emails[0]);
										
										$force_edit = (!@(get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName) || 
											!@(get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName) || 
											!@(get_class($entity)=='user' ? $entity->email : $entity->emails[0]) || 
											!@($entity->phone));
									?>
									<h2>1. <?php echo ACCOUNT_INFO; ?>:</h2>
									<div id="account-info"<?php echo $force_edit ? ' style="height:0"' : ''; ?>>
										<?php echo $name; ?><br />
										<?php echo $email; ?><br />
										<?php echo $entity->phone; ?><br />
										<br />
										<a href="javascript:;" id="edit-account"><?php echo EDIT; ?> <i class="fa fa-pencil"></i></a>
									</div>
									<div id="account-fields"<?php echo $force_edit ? ' style="height: auto; overflow: visible"' : ''; ?>>
									<?php
									require_once 'views/inc/account-edit.inc.php';
									?>
									</div>
									<?php
									}
									?>
								</div>

								<div class="full-horizontal-separator"></div>

								<div class="panel" id="transportation-details">
									<div class="clearfix">
										<h2 class="left">2. <?php echo DELIVERY_DETAILS; ?>:</h2>
										<label class="right" id="transportation-switch">
											<strong><?php echo FOR_DELIVERY; ?></strong>
											<a href="javascript:;"><?php echo CHANGE_TAKEAWAY; ?></a>
										</label>
									</div>

									<input name="type" type="hidden" value="<?php echo @$_SESSION['s_venezvite']['search']['type']=='T' ? 'T' : 'D'; ?>" />

									<div id="delivery-details">
										<div class="clearfix">
											<label class="half left"><span><?php echo SCHEDULED_FOR; ?></span>
												<select class="rounded-select" name="date">
													<?php commonDatesList(@$_SESSION['s_venezvite']['search']['date']); ?>
												</select>
											</label>
											<label class="half right"><span><?php echo SCHEDULED_TIME; ?></span>
												<select class="rounded-select" name="time">
													<?php commonTimesList(@$_SESSION['s_venezvite']['search']['date'], @$_SESSION['s_venezvite']['search']['time'], @$restaurant->current_delivery_zone['estimated_time'], @$restaurant->preparationTime); ?>
												</select>
											</label>
										</div>
										<div class="clearfix">
											<select class="rounded-select" name="delivery_address">
												<?php
												if (!empty($delivery_addresses)) {
													foreach ($delivery_addresses as $delivery_address) {
														$selected = false;
														if (@$_SESSION['s_venezvite']['search']['id']==$delivery_address->idAddress) {
															$selected = true;
														}
														
														$data = array(
															'id'			=> $delivery_address->idAddress, 
											 				'street'		=> $delivery_address->address, 
															//'building_no'	=> $delivery_address->buildingNo, 
															'floor_suite'	=> $delivery_address->floorSuite, 
															'access_code'	=> $delivery_address->accessCode, 
															'city'			=> $delivery_address->city, 
															'zip_code'		=> $delivery_address->zipCode, 
															'latitude'		=> $delivery_address->latitude, 
															'longitude'		=> $delivery_address->longitude, 
															'phone'			=> $delivery_address->phone, 
															'instructions'	=> $delivery_address->instructions
														);
												?>
												<option data-address="<?php echo htmlspecialchars(json_encode($data)); ?>"<?php echo $selected ? ' selected="selected"' : ''; ?> value="<?php echo $delivery_address->idAddress; ?>"><?php echo htmlspecialchars($delivery_address->shortFormat()); ?></option>
												<?php
													}
												}
												?>
												<option<?php echo !@$_SESSION['s_venezvite']['search']['id'] ? ' selected="selected"' : ''; ?> value=""><?php echo ENTER_NEW_ADDRESS; ?></option>
											</select>
										</div>

										<div class="clearfix" id="delivery-new-address">
											<?php $delivery_street = (@$_POST['delivery_street'] ? 
												$_POST['delivery_street'] : 
												@$_SESSION['s_venezvite']['search']['address'] . (@$_SESSION['s_venezvite']['search']['building_no'] ? ', ' . @$_SESSION['s_venezvite']['search']['building_no'] : '')); ?>
											<div><input class="rounded-input" maxlength="200" name="delivery_street" placeholder="<?php echo STREET_ADDRESS; ?> *" type="text" value="<?php echo htmlspecialchars($delivery_street); ?>" /></div>
											<?php /*<div class="half right"><input class="rounded-input" maxlength="10" name="delivery_building_no" placeholder="<?php echo BUILDING_NO; ?> *" type="text" value="<?php echo @$_SESSION['s_venezvite']['search']['building_no']; ?>" /></div>
											<div class="clear"></div>*/ ?>
											<?php $delivery_city = (@$_POST['delivery_city'] ? 
												$_POST['delivery_city'] : 
												@$_SESSION['s_venezvite']['search']['city']); ?>
											<div class="half left"><input class="rounded-input" maxlength="100" name="delivery_city" placeholder="<?php echo CITY; ?> *" type="text" value="<?php echo htmlspecialchars($delivery_city); ?>" /></div>
											<?php $delivery_zip_code = (@$_POST['delivery_zip_code'] ? 
												$_POST['delivery_zip_code'] : 
												@$_SESSION['s_venezvite']['search']['zip_code']); ?>
											<div class="half right"><input class="rounded-input" maxlength="5" name="delivery_zip_code" placeholder="<?php echo POSTAL_CODE; ?> *" type="text" value="<?php echo htmlspecialchars($delivery_zip_code); ?>" /></div>
											<div class="clear"></div>
											<?php $delivery_floor_suite = (@$_POST['delivery_floor_suite'] ? 
												$_POST['delivery_floor_suite'] : 
												''); ?>
											<div class="half left"><input class="rounded-input" maxlength="10" name="delivery_floor_suite" placeholder="<?php echo FLOOR_SUITE; ?> *" type="text" value="<?php echo htmlspecialchars($delivery_floor_suite); ?>" /></div>
											<?php $delivery_access_code = (@$_POST['delivery_access_code'] ? 
												$_POST['delivery_access_code'] : 
												''); ?>
											<div class="half right"><input class="rounded-input" maxlength="10" name="delivery_access_code" placeholder="<?php echo ACCESS_CODE; ?>" type="text" value="<?php echo htmlspecialchars($delivery_access_code); ?>" /></div>
											<div class="clear"></div>
											<?php $delivery_instructions = (@$_POST['delivery_instructions'] ? 
												$_POST['delivery_instructions'] : 
												''); ?>
											<textarea class="rounded-textarea" cols="60" name="delivery_instructions" placeholder="<?php echo SPECIAL_INSTR_HINT; ?>" rows="3"><?php echo htmlspecialchars($delivery_instructions); ?></textarea>
											<div class="clearfix addr-type">
												<input<?php echo @$_POST['addr_type']=='Home' ? ' checked="checked"' : ''; ?> name="addr_type" type="radio" value="Home" />
												<input<?php echo @$_POST['addr_type']=='Work' ? ' checked="checked"' : ''; ?> name="addr_type" type="radio" value="Work" />
												<input<?php echo @$_POST['addr_type']=='Other' ? ' checked="checked"' : ''; ?> name="addr_type" type="radio" value="Other" />
											</div>
											
											<label class="checkout-checkbox"><input checked="checked" name="save_address" type="checkbox" value="Y" /> <span><?php echo SAVE_ADDRESS; ?></span><br class="clear" /></label>
											<!--input name="delivery_id" type="hidden" /-->
											<div class="clear"></div>
										</div>
										<div id="delivery-existing-address">
											<div>
												<table>
													<tbody>
														<tr><td><?php echo STREET_ADDRESS; ?>:</td><td id="view-address-street"></td></tr>
														<!--tr><td><?php echo BUILDING_NO; ?>:</td><td id="view-address-building_no"></td></tr-->
														<tr><td><?php echo CITY; ?>:</td><td id="view-address-city"></td></tr>
														<tr><td><?php echo POSTAL_CODE; ?>:</td><td id="view-address-zip_code"></td></tr>
														<tr><td><?php echo FLOOR_SUITE; ?>:</td><td id="view-address-floor_suite"></td></tr>
														<tr><td><?php echo ACCESS_CODE; ?>:</td><td id="view-address-access_code"></td></tr>
														<tr><td><?php echo SPECIAL_INSTR; ?>:</td><td id="view-address-instructions"></td></tr>
													</tbody>
												</table>

												<a href="javascript:;"><?php echo EDIT; ?> <i class="fa fa-pencil"></i></a>
											</div>
										</div>
									</div>

									<div id="takeaway-details">
										<p><?php echo TAKEAWAY_DESC; ?></p>
									</div>
								</div>

								<div class="full-horizontal-separator"></div>

								<div class="panel" id="payment-panel">
									<h2 class="left">3. <?php echo PAYMENT_OPTIONS; ?>:</h2>
									<div class="left" id="cc-logos"><span class="visa"></span><span class="mc"></span><span class="amex"></span></div>
									<div class="clear"></div>
									<?php
									$buttons_count = 0;
									if (!in_array($restaurant->idRestaurant, array(105, 106))) {
										$buttons_count++;
									}
									if ($restaurant->cashPayment=='Y') {
										$buttons_count++;
									}
									if ($restaurant->country->idCountry=='1') {
										// CH restaurant
										$buttons_count++;
									}
									?>
									<div class="buttons<?php echo $buttons_count; ?> toggle-buttons" data-set="payment">
									<?php
									if (!in_array($restaurant->idRestaurant, array(105, 106))) {
									// These restaurants don't offer CC payment
									?>
										<a data-target="credit" href="javascript:;"><?php echo CREDIT; ?></a>
									<?php
									}
									if ($restaurant->cashPayment=='Y') {
									?>
										<a data-can-place-orders="<?php echo $can_place_cash_orders; ?>" data-target="cash" href="javascript:;"><?php echo CASH; ?></a>
									<?php 
									}
									if ($restaurant->country->idCountry=='1') {
									?>
										<a data-target="luncheck" href="javascript:;">Lunch-Check</a>
									<?php } ?>
									</div><br />
									<div class="clear"></div>
									<input name="payment" type="hidden" value="" />
									
									<div class="toggle-content" data-set="payment">
									<?php
										if (!in_array($restaurant->idRestaurant, array(105, 106))) {
											// These restaurants don't offer CC payment
									?>
										<div data-target="credit">
										<?php
											$is_payment_token = is_array(@$user_payment_tokens);
											if ($is_payment_token) {
										?>
											<div id="saved-ccs">
												<div class="eighth left"><span>&nbsp;</span>
													<select class="rounded-select" name="payment_token">
													<?php
														foreach ($user_payment_tokens as $user_payment_token) {
															$hidden_token = str_repeat('*', strlen($user_payment_token->token) - 4) . substr($user_payment_token->token, -4);
													?>
														<option value="<?php echo $user_payment_token->id; ?>"><?php echo $user_payment_token->card_type, ' ', $hidden_token; ?></option>
													<?php
														}
													?>
														<option value=""><?php echo USE_NEW_CARD; ?></option>
													</select></div>

												<div class="eighth right"><span><?php echo CURRENCY; ?></span>
													<select class="rounded-select" name="currency">
														<option data-exchange="<?php echo $exchange_rate->chf; ?>"<?php echo $restaurant->currency=='CHF' ? ' selected="selected"' : ''; ?> value="CHF">CHF</option>
														<option data-exchange="<?php echo $exchange_rate->eur; ?>"<?php echo $restaurant->currency=='EUR' ? ' selected="selected"' : ''; ?> value="EUR">EUR</option>
														<option data-exchange="<?php echo $exchange_rate->usd; ?>"<?php echo $restaurant->currency=='USD' ? ' selected="selected"' : ''; ?> value="USD">USD</option>
													</select></div>
											</div>
											<div class="disabled" id="new-cc">
											<?php
												}
											?>
												<input autocomplete="off" class="rounded-input"<?php echo $is_payment_token ? ' disabled="disabled"' : ''; ?> maxlength="100" name="cc_name" placeholder="<?php echo CARD_NAME; ?> *" type="text" />
												<input autocomplete="off" class="rounded-input"<?php echo $is_payment_token ? ' disabled="disabled"' : ''; ?> maxlength="20" name="cc_number" placeholder="<?php echo CCN; ?> *" type="text" />

												<div class="quarter"><span><?php echo EXPIRATION_DATE; ?></span>
													<input autocomplete="off" class="rounded-input"<?php echo $is_payment_token ? ' disabled="disabled"' : ''; ?> maxlength="5" name="cc_month_year" placeholder="MM/YY" type="text" />
													<input type="hidden" name="cc_month" />
													<input type="hidden" name="cc_year" />
												</div>
												
												<?php /*?><div class="eighth right"><span>CVC</span>
													<input autocomplete="off" class="rounded-input"<?php echo $is_payment_token ? ' disabled="disabled"' : ''; ?> maxlength="4" name="cvc" placeholder="XXX" type="text" />
												</div> */ ?>
												<div class="clear"></div>
												
												<div class="quarter"><span><?php echo CURRENCY; ?></span>
													<select class="rounded-select"<?php echo $is_payment_token ? ' disabled="disabled"' : ''; ?> name="currency">
														<option data-exchange="<?php echo $exchange_rate->chf; ?>"<?php echo $restaurant->currency=='CHF' ? ' selected="selected"' : ''; ?> value="CHF">CHF</option>
														<option data-exchange="<?php echo $exchange_rate->eur; ?>"<?php echo $restaurant->currency=='EUR' ? ' selected="selected"' : ''; ?> value="EUR">EUR</option>
														<option data-exchange="<?php echo $exchange_rate->usd; ?>"<?php echo $restaurant->currency=='USD' ? ' selected="selected"' : ''; ?> value="USD">USD</option>
													</select></div>
												
												<label class="checkout-checkbox"><input checked="checked" name="save_payment" type="checkbox" value="Y" /> <span><?php echo SAVE_INFO; ?></span><br class="clear" /></label>
											<?php
												if ($is_payment_token) {
											?>
											</div>
											<?php
												}
											?>
										</div>
										<?php
											}
										?>
										<div data-target="cash">
											<p><?php echo $cash_payment_desc; ?></p>
										</div>
										<div data-target="luncheck">
											<input autocomplete="off" class="rounded-input" maxlength="20" name="lc_number" placeholder="<?php echo CCN; ?> *" type="text" />
											
											<div class="quarter"><span>CVC</span>
												<input autocomplete="off" class="rounded-input" maxlength="4" name="cvc" placeholder="XXX" type="text" />
											</div>
										</div>
									</div>
								<?php
									if (empty($entity) || !count(order::list_($entity, null, true, true))) {
								?>
									<label class="checkout-checkbox"><input name="terms" type="checkbox" value="Y" /> <span><?php echo AGREE_TERMS; ?></span><br class="clear" /></label>
								<?php
									}
								?>
								</div>
								<input class="rounded-red-button" type="submit" value="<?php echo str_replace('{x}', '0.00', PLACE_ORDER_X); ?>" />
								<div class="geotrust"><a href="https://smarticon.geotrust.com/smarticonprofile?Referer=https://www.venezvite.com"><i class="fa fa-lock"></i> Secured Geotrust</a></div>
							</div>
						</form>
						<div class="clear"></div>
					</div>
				</div>

				<script>var timetable = <?php echo json_encode($restaurant->buildDatetimeList($restaurant->timetable)); ?>, 
	delivery_hours = <?php echo json_encode($restaurant->buildDatetimeList($restaurant->deliveryHours)); ?>, 
	current_currency = '<?php echo $restaurant->currency; ?>', 
	
	lang4Delivery = '<?php echo str_replace('\'', '\\\'', FOR_DELIVERY); ?>', 
	langChange2Takeaway = '<?php echo str_replace('\'', '\\\'', CHANGE_TAKEAWAY); ?>', 
	lang4Takeaway = '<?php echo str_replace('\'', '\\\'', FOR_TAKEAWAY); ?>', 
	langChange2Delivery = '<?php echo str_replace('\'', '\\\'', CHANGE_DELIVERY); ?>', 
	langTooMuchCash = '<?php echo str_replace('\'', '\\\'', str_replace('{$currency}', $restaurant->currency, TOO_MUCH_CASH)); ?>', 
	langPlaceOrderX = '<?php echo str_replace('\'', '\\\'', PLACE_ORDER_X); ?>';</script>
<?php
	}
