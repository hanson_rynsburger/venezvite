<?php
	if (@$is_included && !empty($entity)) {
		echo '<div class="main-content">';
		require_once 'inc/logged-user-header.inc.php';
?>
				<div id="my-profile-body">
					
						<!--h1><?php echo PROFILE; ?></h1-->
						
						<div class="white-container" id="entity-name">
							<a class="green-button" data-related="edit-entity-name" href="javascript:;"><?php echo EDIT; ?></a>
							
							<strong class="title">Name</strong>
							<span id="entity-first-name"><?php echo htmlspecialchars(get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName); ?></span>
							<span id="entity-last-name"><?php echo htmlspecialchars(get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName); ?></span>
						</div>
						<form id="entity-name-form" method="post">
							<div class="white-container" id="edit-entity-name">
								<strong class="title">Edit name</strong>
								
								<label class="half"><span>First name: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="first_name" value="<?php echo htmlspecialchars(get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName); ?>" type="text" /></label>
								<label class="half right"><span>Last name: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="last_name" value="<?php echo htmlspecialchars(get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName); ?>" type="text" /></label>
								
								<div class="clear"></div>
								<a class="disabled rounded-red-button" data-related="entity-name"><?php echo CANCEL; ?></a><a class="rounded-red-button"><?php echo APPLY; ?></a>
							</div>
						</form>
						
						<div class="white-container" id="entity-email">
							<a class="green-button" data-related="edit-entity-email" href="javascript:;"><?php echo EDIT; ?></a>
							
							<strong class="title">Email</strong>
							<span id="entity-email-address"><?php echo htmlspecialchars($entity->email); ?></span>
						</div>
						<form id="entity-email-form" method="post">
							<div class="white-container" id="edit-entity-email">
								<strong class="title">Edit email</strong>
								
								<label class="half"><span>Email: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="email" value="<?php echo htmlspecialchars($entity->email); ?>" type="text" /></label>
								
								<div class="clear"></div>
								<a class="disabled rounded-red-button" data-related="entity-email"><?php echo CANCEL; ?></a><a class="rounded-red-button"><?php echo APPLY; ?></a>
							</div>
						</form>
						
						<div class="white-container" id="entity-password">
							<a class="green-button" data-related="edit-entity-password" href="javascript:;"><?php echo EDIT; ?></a>
							
							<strong class="title">Password</strong>
							**********
						</div>
						<form id="entity-password-form" method="post">
							<div class="white-container" id="edit-entity-password">
								<strong class="title">Edit password</strong>
								
								<label class="half"><span>New password: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" id="entity-password-value" maxlength="50" name="password" value="" type="password" /></label>
								<label class="half right"><span>Confirm password: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="confirm_password" value="" type="password" /></label>

								<div class="clear"></div>
								<a class="disabled rounded-red-button" data-related="entity-password"><?php echo CANCEL; ?></a><a class="rounded-red-button"><?php echo APPLY; ?></a>
							</div>
						</form>

						<div class="white-container" id="entity-phone">
							<a class="green-button" data-related="edit-entity-phone" href="javascript:;"><?php echo EDIT; ?></a>

							<strong class="title">Phone</strong>
							<span id="entity-phone-number"><?php echo htmlspecialchars($entity->phone); ?></span>
						</div>
						<form id="entity-phone-form" method="post">
							<div class="white-container" id="edit-entity-phone">
								<strong class="title">Phone</strong>
								
								<label class="half"><span>Phone: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="phone" value="<?php echo htmlspecialchars($entity->phone); ?>" type="text" /></label>
								<div class="clear"></div>
								<a class="disabled rounded-red-button" data-related="entity-phone"><?php echo CANCEL; ?></a>
								<a class="rounded-red-button"><?php echo APPLY; ?></a>
							</div>
						</form>
					</div>
				</div>
<?php
	}
