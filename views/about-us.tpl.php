<?php
	if (@$is_included) {
		$submenu_title = WHO_ARE_WE;
		require_once 'inc/others-sub-menu.inc.php';
?>
				<div id="about-us-title">
					<div class="main-content">
						<div class="centered" id="intro-text">
							<h2><?php echo ABOUT_US_TITLE; ?></h2>
							<p><?php echo ABOUT_US_P1; ?></p>
							<p><?php echo ABOUT_US_P2; ?></p>
						</div>
					</div>
				</div>
				<div id="about-us-body">
					<div class="main-content">
						<div class="centered">
							<h3><?php echo MEET_TEAM_TITLE; ?></h3>

							<div class="row team-members">
								<div class="col-6-12 col-12-12m">
									<div class="team-desc">
										<div class="desc">
											<strong>Team Players</strong>
											<p>We have a team with 15 outstanding employees from many different countries. We are focus in food delivery.</p>
										</div>
									</div>
								</div>

								<div class="col-3-12 col-6-12t col-12-12m">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/addia.jpg' )"></div>
										<div class="member-meta">
											<div class="member-name">Addia Cooper-Henry</div>
											<div class="member-title">CEO &amp; Founder</div>
											<ul class="member-links">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/ursula.jpg' )"></div>
										<div class="member-meta">
											<div class="member-name">Ursula  Trist&#227;o-Joho</div>
											<div class="member-title">CFO &amp; Co-Founder</div>
											<ul class="member-links">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/adam.jpg' )"></div>
										<div class="member-meta">
											<div class="member-name">Carl Adam Freiholtz</div>
											<div class="member-title">COO</div>
											<ul class="member-links">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/cosmin.jpg' )"></div>
										<div class="member-meta">
											<div class="member-name">Cosmin Păltinescu</div>
											<div class="member-title">Technical Advisor</div>
											<ul class="member-links">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/alice.jpg' )"></div>
										<div class="member-meta">
											<div class="member-name">Alice Boudreau</div>
											<div class="member-title">Art designer</div>
											<ul class="member-links">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/guillaume.jpg' )"></div>
										<div class="member-meta">
											<div class="member-name">Guillaume LaPorte</div>
											<div class="member-title">Developer</div>
											<ul class="member-links">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/laurent.jpg' )"></div>
										<div class="member-meta">
											<div class="member-name">Laurent Sommer</div>
											<div class="member-title">Staff</div>
											<ul class="member-links">
												<li><a href="#"><i class="fa fa-facebook"></i></a></li>
												<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/_join.png' )"></div>
										<div class="member-meta">
											<div class="member-new"><i class="fa fa-suitcase"></i><a href="javascript:;" id="footer-contact">Want to work with us!</a><i class="fa fa-arrow-right"></i></div>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m hidden-tablet">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/_join.png' )"></div>
										<div class="member-meta">
											<div class="member-new"><i class="fa fa-suitcase"></i><a href="javascript:;" id="footer-contact">Want to work with us!</a><i class="fa fa-arrow-right"></i></div>
										</div>
									</div>
								</div>
								<div class="col-3-12 col-6-12t col-12-12m hidden-tablet">
									<div class="team-member">
										<div class="member-image" style="background-image: url( '<?php echo IMG; ?>team/_join.png' )"></div>
										<div class="member-meta">
											<div class="member-new"><i class="fa fa-suitcase"></i><a href="javascript:;" id="footer-contact">Want to work with us!</a><i class="fa fa-arrow-right"></i></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php /* ?>
						<div class="centered">
							<h3><?php echo MEET_TEAM_TITLE; ?></h3>
							<ul>
								<li><img src="<?php echo IMG; ?>team/addia.jpg" />
									<strong>Addia Cooper-Henry</strong></li>
								<li><img src="<?php echo IMG; ?>team/ursula.jpg" />
									<strong>Ursula  Trist&#227;o-Joho</strong></li>
								<li><img src="<?php echo IMG; ?>team/adam.jpg" />
									<strong>Carl Adam Freiholtz</strong></li>
								<li><img src="<?php echo IMG; ?>team/cosmin.jpg" />
									<strong>Cosmin P&#259;ltinescu</strong></li>
								<li><img src="<?php echo IMG; ?>team/alice.jpg" />
									<strong>Alice Boudreau</strong></li>
								<li><img src="<?php echo IMG; ?>team/guillaume.jpg" />
									<strong>Guillaume LaPorte</strong></li>
								<li><img src="<?php echo IMG; ?>team/_join.png" />
									<strong><?php echo JOIN_TEAM; ?></strong></li>
								<li><img src="<?php echo IMG; ?>team/_join.png" />
									<strong><?php echo JOIN_TEAM; ?></strong></li>
							</ul>
						</div>
						
						<div class="centered">
							<h4><?php echo MEET_DELIVERY_TEAM_TITLE; ?></h4>
							<ul>
								<li><img src="<?php echo IMG; ?>team/laurent.jpg" />
									<strong>Laurent Sommer</strong></li>
								<li><img src="<?php echo IMG; ?>team/_join.png" />
									<strong><?php echo VP_MEMBER; ?></strong></li>
								<li><img src="<?php echo IMG; ?>team/_join.png" />
									<strong><?php echo VP_MEMBER; ?></strong></li>
								<li><img src="<?php echo IMG; ?>team/_join.png" />
									<strong><?php echo VP_MEMBER; ?></strong></li>
							</ul>
						</div>
						*/ ?>
					</div>
				</div>
<?php
	}
