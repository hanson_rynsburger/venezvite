<?php
	if (@$is_included) {
?>
				<div id="hero-image" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo IMG; ?>home-bg/home-bg-0<?php echo mt_rand(1, 8); ?>.jpg" >
					<strong><?php echo HELLO_RESTAURANT; ?></strong>
				</div>
				
				<!-- <div id="main-private-menu">
					<ul>
						<li><a class="selected" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/login-restaurant.html"><?php echo LOGIN_ACCOUNT; ?></a></li>
					</ul>
				</div> -->
				
				<div id="login-restaurant-body">
					<div class="center main-content">
						<h3 class="text-center"><?php echo LOGIN_ACCOUNT ?></h3>
						<form method="post">
							<input class="rounded-input" maxlength="50" name="username" placeholder="<?php echo USERNAME; ?>: *" type="text" value="<?php echo !empty($_POST['username']) ? htmlspecialchars($_POST['username']) : ''; ?>" />
							<input class="rounded-input" maxlength="20" name="password" placeholder="<?php echo PASSWORD; ?>: *" type="password" value="<?php echo !empty($_POST['password']) ? htmlspecialchars($_POST['password']) : ''; ?>" />
							<br />
							<a href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/forgot-password-restaurant.html"><?php echo FORGOT; ?></a>
							
							<input class="rounded-red-button" type="submit" value="<?php echo LOGIN; ?>" />
						</form>
						
						<ul class="centered">
							<li><strong><?php echo CONFIRM_ORDERS; ?></strong>
								<?php echo CONFIRM_ORDERS_DESC; ?></li>
							<li><strong><?php echo MANAGE_RESTAURANT; ?></strong>
								<?php echo MANAGE_RESTAURANT_DESC; ?></li>
							<li><strong><?php echo SALES; ?></strong>
								<?php echo SALES_DESC; ?></li>
						</ul>
					</div>
				</div>
<?php
	}
