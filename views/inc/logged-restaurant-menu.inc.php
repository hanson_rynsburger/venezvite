<?php
	if (@$is_included && !empty($restaurantAdmin)) {
		if ($current_page=='confirmation') {
?>
						<li><a class="selected" href="javascript:;"><?php echo ORDER_CONFIRMATION; ?></a></li>
<?php
		}
?>
						<li><a<?php echo $current_page=='restaurant-orders' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/restaurant-orders.html"><?php echo ORDERS_HISTORY; ?></a></li>
						<li><a<?php echo $current_page=='business-info' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/business-info.html"><?php echo BUSINESS_INFO; ?></a></li>
						<li><a<?php echo $current_page=='financial-details' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/financial-details.html"><?php echo FINANCIAL_INFO; ?></a></li>
						<li><a<?php echo $current_page=='gallery' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/gallery.html"><?php echo RESTAURANT_GALLERY; ?></a></li>
						<li><a<?php echo in_array($current_page, array('menu-items', 'menu-categories', 'menu-option-groups')) ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/menu-items.html"><?php echo MENU; ?></a>
							<ul>
								<li><a<?php echo $current_page=='menu-categories' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/menu-categories.html"><?php echo MENU_CATEGORIES; ?></a></li>
								<li><a<?php echo $current_page=='menu-option-groups' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/menu-option-groups.html"><?php echo MENU_OPTION_GROUPS; ?></a></li>
							</ul></li>
						<li><a href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/', $restaurantAdmin->restaurant->customURL, '.html'; ?>"><?php echo SEE_PROFILE; ?></a></li>
<?php
	}
