<?php
	if (@$is_included && 
		(!empty($entity) || !empty($restaurantAdmin)) && 
		!empty($order)
	) {
		setlocale(LC_TIME, $_SESSION['s_venezvite']['language']->localeCode); // 'fra'
		
		if ($order->payment=='cc') {
			$payment = CC;
		} else {
			if ($order->type=='D') {
				$payment = DELIVERY_CASH;
			} else {
				$payment = TAKEAWAY_CASH;
			}
		}
		
		$is_asap = (strtotime($order->dateDesired)>=strtotime('-45 minutes', strtotime($order->dateAdded)) && 
			strtotime($order->dateDesired)<=strtotime('-15 minutes', strtotime($order->dateAdded)));
?>
	<tr class="order-row">
		<td colspan="5">
			<table cellspacing="0" class="order-row">
<?php
		if (!empty($restaurantAdmin)) {
?>
				<thead>
					<tr>
						<th colspan="6"><?php echo PAYMENT_TYPE; ?>: <?php echo $payment; ?></th>
					</tr>
					<tr>
						<th colspan="6"><span class="red"><?php echo DELIVERY, ($is_asap ? ' ' . ASAP : ''); ?></span><?php echo $is_asap ? ' / ' . NO_LATER_THAN : ''; ?> <span class="red"><?php echo (date('N', strtotime($order->dateDesired))==date('N') ? TODAY . ', ' : ''), ucwords(utf8_encode(strftime('%b %e, %Y @ %H:%M'/*date('M j, Y @ H:i'*/, strtotime($order->dateDesired)))); ?> +/- 15 <?php echo MINUTES; ?></span></th>
					</tr>
				</thead>
<?php
		}
?>
				<tbody>
<?php
		$order_total = 0;
		foreach ($order->items as $index => $item) {
?>
					<tr>
						<td><?php echo $item->quantity; ?></td>
						<td><strong><?php echo htmlspecialchars($item->menuItemName); ?></strong><br />
<?php
			if ($item->instructions) {
?>
							<em class="red"><?php echo NOTES; ?>: <?php echo htmlspecialchars($item->instructions); ?></em><br />
<?php
			}
			
			$options_price = 0;
			if ($item->options) {
?>
							<ul class="order-items">
<?php
				foreach ($item->options as $option) {
?>
								<li><?php echo htmlspecialchars($option->menuItemOptionName); ?></li>
<?php
					$options_price += $option->menuItemOption->price;
				}
?>
							</ul>
<?php
			}
?>
						</td>
						<td><strong><?php echo number_format($item->pricePerItem, 2); ?></strong><br />
<?php
			if ($item->instructions) {
?>
							<br />
<?php
			}
			
			if ($item->options) {
?>
							<ul>
<?php
				foreach ($item->options as $option) {
?>
								<li><?php echo number_format($option->menuItemOption->price, 2); ?></li>
<?php
				}
?>
							</ul>
<?php
			}
?>
						</td>
						<td><strong>x<?php echo $item->quantity; ?></strong></td>
						<td style="width:65px"><strong><?php echo  number_format($item->quantity * ($item->pricePerItem + $options_price), 2); ?></strong></td>
<?php
			if ($index===0) {
				if ($order->user) {
					$recipient = $order->user->firstName . ' ' . $order->user->lastName . '<br />' . 
						$order->user->phone;
				} else {
					$recipient = $order->corporateAccount->contactFirstName . ' ' . $order->corporateAccount->contactLastName . '<br />' . 
						$order->corporateAccount->mobile;
				}
?>
						<td rowspan="<?php echo count($order->items); ?>">
							<strong><?php echo CUSTOMER; ?>:</strong><br />
							<?php echo $recipient; ?><br />
							<br />
<?php
				if ($order->type=='D') {
?>
							<strong><?php echo DELIVERY_DATETIME; ?>:</strong><br />
							<?php echo (date('N', strtotime($order->dateDesired))==date('N') ? TODAY . ', ' : ''), ucwords(utf8_encode(strftime('%A, %b %e'/*date('l, M j'*/, strtotime($order->dateDesired)))); ?><br />
<?php
					if ($is_asap) {
?>
							<strong>(<?php echo ASAP; ?>)</strong><br />
<?php
					}
?>
							<?php echo ($is_asap ? NO_LATER_THAN . ': ': ''), strftime('%H:%M'/*date('H:i'*/, strtotime($order->dateDesired)); ?> +/- 15 <?php echo MINUTES; ?><br /> 
							<br />
							<strong><?php echo DELIVERY_TO; ?>:</strong><br />
							<?php echo $order->userAddress->address, ($order->userAddress->buildingNo ? ' ' . $order->userAddress->buildingNo : ''); ?><br />
							<?php echo $order->userAddress->zipCode, ', ', $order->userAddress->city; ?><br />
							<?php echo FLOOR, '/', ACCESS_CODE; ?>: <?php echo ($order->userAddress->floorSuite ? $order->userAddress->floorSuite : '-'), '/', ($order->userAddress->accessCode ? $order->userAddress->accessCode : '-'); ?><br />
<?php
					if ($order->notes) {
?>
							<br />
							<strong><?php echo DELIVERY_INSTRUCTIONS; ?>:</strong><br />
							<?php echo $order->notes; ?>
<?php
					}
					
				} else {
?>
							<strong><?php echo TAKEAWAY_DATETIME; ?>:</strong><br />
							<?php echo (date('N', strtotime($order->dateDesired))==date('N') ? TODAY . ', ' : ''), ucwords(utf8_encode(strftime('%A, %b %e'/*date('l, M j'*/, strtotime($order->dateDesired)))); ?><br />
<?php
					if ($is_asap) {
?>
							<strong>(<?php echo ASAP; ?>)</strong><br />
<?php
					}
?>
							<?php echo ($is_asap ? NO_LATER_THAN . ': ': ''), strftime('%H:%M'/*date('H:i'*/, strtotime($order->dateDesired)); ?> +/- 15 <?php echo MINUTES; ?>
<?php
				}
?>
						</td>
<?php
			}
			
			$order_total += $item->quantity * ($item->pricePerItem + $options_price);
		}
		
		$discount = $order->value - $order_total;
		if (round($discount, 2)==0) {
			$discount = 0;
		}
?>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td colspan="2"><strong><?php echo TOTAL_PRODUCTS; ?>:</strong></td>
						<td><strong><?php echo count($order->items); ?></strong></td>
						<td><strong><?php echo number_format($order_total, 2); ?></strong></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2"><strong><?php echo DISCOUNT; ?>:</strong></td>
						<td></td>
						<td><strong><?php echo number_format($discount, 2); ?></strong></td>
						<td></td>
					</tr>
<?php
		if ($order->type=='D') {
?>
					<tr>
						<td></td>
						<td colspan="2"><strong><?php echo DELIVERY_FEE; ?>:</strong></td>
						<td></td>
						<td><strong><?php echo number_format($order->deliveryCost, 2); ?></strong></td>
						<td></td>
					</tr>
<?php
		}
?>
					<tr>
						<td></td>
						<td colspan="2"><?php echo TOTAL; ?>:</td>
						<td></td>
						<td><strong><strong><?php echo number_format($order->value + $order->deliveryCost, 2); ?></strong></strong></td>
						<td><?php echo PAYMENT_TYPE; ?>: <strong><?php echo $payment; ?></strong></td>
					</tr>
				</tfoot>
			</table>
		</td>
	</tr>
<?php
	}
