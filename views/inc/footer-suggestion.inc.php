<?php
	if (@$is_included) {
?>
								<span class="title"><?php echo FOOTER_SUGGEST; ?></span>
								<?php echo SUGGESTION_PANEL_BODY; ?>
								<br />
								<form id="suggest-restaurant" method="post">
									<div>
										<label><input class="rounded-input" maxlength="100" name="suggestion_name" placeholder="<?php echo SUGGESTION_PANEL_NAME; ?>" required="required" value="" type="text" /></label>
										<label><input class="rounded-input" maxlength="100" name="suggestion_restaurant" placeholder="<?php echo SUGGESTION_PANEL_RESTAURANT; ?>" required="required" value="" type="text" /></label>
										<label><input class="rounded-input" maxlength="100" name="suggestion_address" placeholder="<?php echo SUGGESTION_PANEL_ADDRESS; ?>" required="required" value="" type="text" /></label>
										<label><textarea class="rounded-textarea" name="suggestion_comments" placeholder="<?php echo SUGGESTION_PANEL_COMMENTS; ?>"></textarea></label>
										<label><input class="rounded-red-button" type="submit" value="<?php echo SUBMIT; ?>" /></label>
									</div>
								</form>
<?php
	}
