<?php
	if (@$is_included && @$current_page) {
?>
				<div id="hero-image" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo IMG; ?>home-bg/home-bg-0<?php echo mt_rand(1, 8); ?>.jpg" >
					<strong><?php echo HELLO_RESTAURANT; ?></strong>
					<span><?php echo HUNGRY_FOR_BUSINESS; ?></span>
				</div>
				
				<div id="main-private-menu">
					<ul>
						<li><a<?php echo $current_page=='restaurant-join' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/restaurant-join.html"><?php echo JOIN; ?></a></li>
						<li><a<?php echo $current_page=='how-it-works' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/how-it-works.html"><?php echo HOW_IT_WORKS; ?></a></li>
						<li><a<?php echo $current_page=='how-we-help' ? ' class="selected"' : ''; ?> href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/how-we-help.html"><?php echo HOW_WE_HELP; ?></a></li>
					</ul>
				</div>
<?php
	}
