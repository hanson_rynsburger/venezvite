<?php
	if (@$is_included && @$checkout_button_text) {
		if (empty($restaurant) && ($restaurant_path = @json_decode($_COOKIE['c_venezvite'])->restaurant)) {
			$restaurant = restaurant::getByURL($restaurant_path);
		}
?>
										<strong class="title"><?php echo MY_ORDER; ?></strong>

										<ul id="shopping-cart-items"></ul>
										<div id="empty-cart"><?php echo EMPTY_BAG; ?></div>
<?php
		if (!empty($restaurant)) {
?>
										<div id="order-delivery-costs">
											<div><?php echo DELIVERY_MIN; ?>: <strong id="delivery-minimum"><?php echo ($restaurant->current_delivery_zone ? $restaurant->current_delivery_zone['minimum_delivery'] : '0.00'), ' ', $restaurant->currency; ?></strong><br /> 
												<?php echo DELIVERY_FEE; ?>: <strong id="delivery-fee"><?php echo ($restaurant->current_delivery_zone ? $restaurant->current_delivery_zone['delivery_fee'] : '0.00'), ' ', $restaurant->currency; ?></strong></div>
											<!--strong><?php echo NO_TAKEAWAY_MIN; ?></strong--></div>

										<div id="cart-checkout">
											<div>
												<table id="cart-total">
													<tbody>
														<tr><td><?php echo PRODUCT_TOTAL; ?></td><td><span>0.00</span> <strong><?php echo $restaurant->currency; ?></strong></td></tr>
														<tr id="total-delivery-fee">
															<td><?php echo DELIVERY_FEE; ?>:</td>
															<?php $delivery_fee = ($restaurant->current_delivery_zone ? $restaurant->current_delivery_zone['delivery_fee'] : '0.00'); ?>
															<td><span data-value="<?php echo $delivery_fee; ?>"><?php echo $delivery_fee; ?></span> <strong><?php echo $restaurant->currency; ?></strong></td>
														</tr>
													</tbody>
												</table>

												<label id="promo-code"><span><?php echo HAVE_PROMO; ?></span>
													<input class="rounded-input" maxlength="25" name="promo" placeholder="<?php echo ENTER_PROMO; ?>" type="text" value="<?php echo htmlspecialchars(@$_POST['promo']); ?>" /><input type="button" value="<?php echo APPLY; ?>" /></label>

												<input name="location" type="hidden" value="<?php echo htmlspecialchars(@$_SESSION['s_venezvite']['search']['location']); ?>" />
												<input name="building_no" type="hidden" value="<?php echo @$_SESSION['s_venezvite']['search']['building_no']; ?>" />
												<input name="address" type="hidden" value="<?php echo htmlspecialchars(@$_SESSION['s_venezvite']['search']['address']); ?>" />
												<input name="city_name" type="hidden" value="<?php echo htmlspecialchars(@$_SESSION['s_venezvite']['search']['city']); ?>" />
												<input name="country_code" type="hidden" value="<?php echo @$_SESSION['s_venezvite']['search']['country_code']; ?>" />
												<input name="zip_code" type="hidden" value="<?php echo @$_SESSION['s_venezvite']['search']['zip_code']; ?>" />
												<input name="latitude" type="hidden" value="<?php echo @$_SESSION['s_venezvite']['search']['coords']['lat']; ?>" />
												<input name="longitude" type="hidden" value="<?php echo @$_SESSION['s_venezvite']['search']['coords']['lng']; ?>" />
												
												<!-- <input class="rounded-red-button" type="submit" value="<?php echo $checkout_button_text; ?>" /> -->
											</div>
										</div>
<?php
		}
	}
