<?php
	if (!@$is_included) {
		die();
	}
?>
<div id="restaurant-reviews">
	<div class="main-content">
		<div class="bg-container">
			<h2><?php echo REVIEWS ?></h2>
			
			<?php if ( $eligible_review ) { ?>
			<a class="write-review" href="javascript:;"><?php echo WRITE_REVIEW ?></a>
			<?php } ?>
			
			<?php if ( $reviews["total"] == 0 ) {
				echo "<p>" . NO_REVIEWS . "</p>";
			} else { ?>
			<?php foreach( $reviews["content"] as $rr ) { ?>
			<div class="clearfix" itemprop="review" itemscope itemtype="http://schema.org/Review">
				<div class="user-avatar">
					<?php echo $rr["firstInitial"] . $rr["lastInitial"] ?>
				</div>
				<div class="review-desc">
					<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
						<span itemprop="author"><?php echo $rr["firstName"] . " " . $rr["lastName"] . "."?></span> (<meta itemprop="worstRating" content = "1"/><span itemprop="ratingValue"><?php echo $rr["rating"]?></span> out of <span itemprop="bestRating">5</span>)
						<div class="star-rating">
							<span class="review-stars no-change">
								<?php for ($i=0; $i<5; $i++) {
									if ( $i < $rr["rating"] ) { 
								?>
								<span class="selected"></span>
								<?php } else { ?>
								<span></span>
								<?php } } ?>
							</span>
						</div>
					</div>
					<div itemprop="description"><?php echo $rr["review"] ?></div>
					<meta itemprop="datePublished" content="<?php echo $rr["dateAdded"] ?>" />
					<div class="last-order">
						<?php echo LAST_ORDER ?> <?php echo $rr["lastOrder"] ?>
					</div>
				</div>
			</div>
			<?php }
			
			if ( $reviews['totalPage'] > 1 ) { ?>
			<ul class="review-pagination">
				<?php for($i=max(1, $reviews['currentPage'] - 2); $i<=min( $reviews['totalPage'], $reviews['currentPage'] + 2); $i++) { ?>
				<?php if ( $i != $reviews["currentPage"] ) { ?>
				<li><a href="javascript:void(0);" data-page="<?php echo $i ?>"><?php echo $i ?></a></li>
				<?php } else { ?>
				<li><span class="current-page"><?php echo $i ?></span></li>
				<?php } } ?>
			</ul>
			<?php } } ?>
		</div>
	</div>
</div>
