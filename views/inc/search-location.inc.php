<?php
	if (@$is_included && @$current_page) {
		// The current user's delivery addresses SHOULD be displayed, 
		// UNLESS the user has explicitly searched for a new address 
		$show_addresses = (
		    !empty($delivery_addresses) && 
			(
			    $current_page=='home' || 
				( 	!empty($_SESSION['s_venezvite']['search']['id']) && 
				    !empty($_SESSION['s_venezvite']['search']['location']) ) || 
				empty($_SESSION['s_venezvite']['search']['location'])
			)
		);

		if ($show_addresses) {
			$kvp = array();
			$active = "";
			foreach ($delivery_addresses as $delivery_address) {
				$kvp[ $delivery_address->idAddress ] = htmlspecialchars($delivery_address->shortFormat());
		
				if ($current_page!='home' && @$_SESSION['s_venezvite']['search']['id']==$delivery_address->idAddress) {
					$active = $delivery_address->idAddress;
				}
			}

			$kvp["-1"] = SUBMIT_NEW_LOCATION;

			hs_drop_down( "location", array(
				"icon-tail" => "<i class='fa fa-chevron-down'></i>",
				"text-align" => "center",
				"values" => $kvp,
				"active" => $active,
				"callback" => "change"
			) );
		}?>

		<?php /* if ($show_addresses) { ?>
									<select<?php echo $current_page!='home' ? ' class="rounded-select"' : ''; ?> name="location">
										<!--option value=""><?php echo SELECT_SAVED_LOCATION; ?></option-->
<?php
			foreach ($delivery_addresses as $delivery_address) {
				$selected = false;
				if ($current_page!='home' && @$_SESSION['s_venezvite']['search']['id']==$delivery_address->idAddress) {
					$selected = true;
				} ?>
										<option<?php echo $selected ? ' selected="selected"' : ''; ?> value="<?php echo $delivery_address->idAddress; ?>"><?php echo htmlspecialchars($delivery_address->shortFormat()); ?></option>
<?php } ?>
										<option value="-1"><?php echo SUBMIT_NEW_LOCATION; ?></option>
									</select>
<?php } */ ?>
									<div class="form-control-group">
										<input class="<?php echo $current_page=='home' ? 'big-' : ''; ?>rounded-input"<?php echo $show_addresses ? ' disabled="disabled"' : ''; ?> name="location" placeholder="<?php echo ENTER_ADDRESS; ?>" type="text" value="<?php echo $current_page!='home' && empty($_SESSION['s_venezvite']['search']['id']) ? htmlspecialchars(@$_SESSION['s_venezvite']['search']['location']) : ''; ?>" />
									</div>
									<input name="building_no" type="hidden" value="<?php echo $current_page!='home' ? @$_SESSION['s_venezvite']['search']['building_no'] : ''; ?>" />
									<input name="address" type="hidden" value="<?php echo $current_page!='home' ? htmlspecialchars(@$_SESSION['s_venezvite']['search']['address']) : ''; ?>" />
									<input name="city_name" type="hidden" value="<?php echo $current_page!='home' ? htmlspecialchars(@$_SESSION['s_venezvite']['search']['city']) : ''; ?>" />
									<input name="country_code" type="hidden" value="<?php echo $current_page!='home' ? @$_SESSION['s_venezvite']['search']['country_code'] : ''; ?>" />
									<input name="zip_code" type="hidden" value="<?php echo $current_page!='home' ? @$_SESSION['s_venezvite']['search']['zip_code'] : ''; ?>" />
									<input name="latitude" type="hidden" value="<?php echo $current_page!='home' ? @$_SESSION['s_venezvite']['search']['coords']['lat'] : ''; ?>" />
									<input name="longitude" type="hidden" value="<?php echo $current_page!='home' ? @$_SESSION['s_venezvite']['search']['coords']['lng'] : ''; ?>" />
<?php }