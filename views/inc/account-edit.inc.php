<?php
	if (@$is_included) {
		$last_name = (@$entity ? (get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName) : '');
		$first_name = (@$entity ? (get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName) : '');
		$email = (@$entity ? (get_class($entity)=='user' ? $entity->email : $entity->emails[0]) : '');
		$phone = (@$entity ? $entity->phone : '');
?>
									<div class="half left"><input class="rounded-input"<?php echo @$entity && empty($force_edit) ? ' disabled="disabled"' : ''; ?> maxlength="50" name="last_name" placeholder="<?php echo LAST_NAME; ?> *" type="text" value="<?php echo $last_name; ?>" /></div>
									<div class="half right"><input class="rounded-input"<?php echo @$entity && empty($force_edit) ? ' disabled="disabled"' : ''; ?> maxlength="50" name="first_name" placeholder="<?php echo FIRST_NAME; ?> *" type="text" value="<?php echo $first_name; ?>" /></div>
									<div class="clear"></div>
									<div class="half left"><input class="rounded-input"<?php echo @$entity && empty($force_edit) ? ' disabled="disabled"' : ''; ?> maxlength="50" name="email" placeholder="<?php echo ENTER_EMAIL; ?> *" type="text" value="<?php echo $email; ?>" /></div>
									<div class="half right"><input class="rounded-input<?php echo @$entity ? ' not-mandatory' : ''; ?>"<?php echo @$entity && empty($force_edit) ? ' disabled="disabled"' : ''; ?> maxlength="20" name="password" placeholder="<?php echo CREATE_PASS; ?> *" type="password" /></div>
									<div class="clear"></div>
									<div class="phone-number">
										<input class="rounded-input"<?php echo @$entity && empty($force_edit) ? ' disabled="disabled"' : ''; ?> maxlength="20" name="phone" placeholder="<?php echo PHONE_NO; ?> * (+xx xx xxx xx xx)" type="text" value="<?php echo $phone; ?>" />
									</div><div class="clearfix"></div>
									
<?php
	}
