<?php
	if (@$is_included && !empty($search_restaurant)) {
		$url = ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/' . $search_restaurant->customURL . '.html';
		$new = ((strtotime($search_restaurant->dateApproved) + (3600 * 24 * 60))>time() ? '<span class="new">' . NEW_ . '</span>' : '');
		$vite = ($search_restaurant->customDelivery=='Y' ? '<span class="vite">' . VITE . '</span>' : '');
		$thumb = (count($search_restaurant->images) && !empty($search_restaurant->images[0]) ? ' style="background:url(\'' . IMG . 'restaurants/' . $search_restaurant->images[0] . '\')"' : '');
		$cuisine_types_ = htmlspecialchars(implode(', ', $search_restaurant->getCuisineTypesList()));
		$datetime = @$_SESSION['s_venezvite']['search']['date'] . ' ' . @$_SESSION['s_venezvite']['search']['time'];

		if ($search_restaurant->customDelivery!='Y' && $search_restaurant->cashPayment=='Y') {
			if (!empty($entity)) {
				$cash_orders_status = $entity->canPlaceCashOrders();

				if ($cash_orders_status['paid'] && !$cash_orders_status['pending']) {
					$can_place_cash_orders = true;
					
				} else {
					$can_place_cash_orders = false;
				}

			} else {
				$can_place_cash_orders = true;
			}

		} else {
			$can_place_cash_orders = false;
		}

		$output = '' . 
			'<li id="restaurant-' . $search_restaurant->customURL . '">' . 
				'<div class="left thumb">';

		if ($search_restaurant->opened) {
			$output .= '' . 
					'<div class="labels">' . $new . $vite . '</div>' . 
					'<img src="' . IMG . 'dot.png"' . $thumb . ' />';

		} else {
			$reopen_datetime = $search_restaurant->getReopenTime(@$_SESSION['s_venezvite']['search']['type'], $datetime);
			$output .= '' . 
					'<span class="closed">' . (@$_SESSION['s_venezvite']['search']['type']!='T' ? NEXT_DELIVERY : REOPENS) . '<br />' . 
						$reopen_datetime . '</span>' . 
					'<div class="labels">' . $new . $vite . '</div>' . 
					'<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAC0lEQVR42mNgAAIAAAUAAen63NgAAAAASUVORK5CYII="' . $thumb . ' />';
		}

		$output .= '' . 
				'</div>' . 
				'<h3 title="' . htmlspecialchars($search_restaurant->restaurantName) . '">' . htmlspecialchars($search_restaurant->restaurantName) . '</h3>' . 
				'<div class="description">' . 
					'<div class="left"><span class="nowrap" title="' . $cuisine_types_ . '">' . $cuisine_types_ . '</span><br />' . 
						'<span class="restaurant-reviews"><span class="review-stars">' . str_repeat('*', round($search_restaurant->rating)) . '</span> <span class="nowrap">(' . $search_restaurant->reviews . ' ' . REVIEWS . ')</span></span><br />' . 
						'<span class="price-range">' . str_repeat('$', $search_restaurant->priceLevel) . '</span>' . 
						($can_place_cash_orders ? '<span class="cash-ok">' . ACCEPTS_CASH . '</span>' : '') . 
						'<br /></div>';

		if (@$_SESSION['s_venezvite']['search']['type']!='T') {
			$max_delivery_time = $search_restaurant->preparationTime + $search_restaurant->current_delivery_zone->estimatedTime;
			$delivery_time = $max_delivery_time . ' - ' . ($max_delivery_time + 15);

			$output .= '' . 
					'<div class="left">' . DELIVERY_FEE . ': ' . $search_restaurant->current_delivery_zone->deliveryFee . ' ' . $search_restaurant->currency . '<br />' . 
						MINIMUM_ORDER . ': ' . $search_restaurant->current_delivery_zone->minimumDelivery . ' ' . $search_restaurant->currency . '<br />' . 
						'<div class="list-delivery-hours">' . 
							'<strong>' . str_replace('{$time_interval}', $delivery_time, DELIVERED_IN) . '</strong></div>' . 
					'</div>';

		} else {
			$ordering_hours = implode(' / ', $search_restaurant->getDateHours(@$_SESSION['s_venezvite']['search']['type'], $datetime));
			
			$output .= '' . 
					'<div class="left">' . 
						'<div class="list-delivery-hours">' . 
							'<strong>' . ORDERING_HOURS . ':</strong><br />' . 
							$ordering_hours . '</div>' . 
					'</div>';
		}

		if ($search_restaurant->opened) {
			$output .= '' . 
					'<a class="right rounded-red-button" href="' . $url . '">' . ORDER . '</a>' . 
					'<a class="right red-arrow" href="' . $url . '">&rsaquo;</a>';
			
		} else {
			$output .= '' . 
					'<a class="rounded-green-button" href="' . $url . '"><!--span>' . /*CLOSED_NOW . */'</span-->' . PRE_ORDER . '</a>' . 
					'<a class="red-arrow right" href="' . $url . '">&rsaquo;</a>';
		}

		$output .= '' . 
					'<div class="clear"></div>' . 
				'</div>' . 
				'<div class="clear"></div>' . 
			'</li>';
		
		return $output;
	}
