<?php
	if (@$is_included) {
		if (empty($restaurant) && ($restaurant_path = @json_decode($_COOKIE['c_venezvite'])->restaurant)) {
			$restaurant = restaurant::getByURL($restaurant_path);
		}
?>
		<div id="shopping-cart-container">
			<form action="<?php echo 'https://', str_replace(array('http:', '//'), '', ROOT), $_SESSION['s_venezvite']['language']->languageAcronym; ?>/checkout.html" method="post">
				<div id="shopping-cart">
					<div>
						<?php
							if (!empty($restaurant)) {
						?>
						<div id="checkout-button"><span class="subtitle"><?php echo ORDER_FROM; ?>:
							<a href="<?php echo $restaurant_path ?>.html"><strong><?php echo htmlspecialchars($restaurant->restaurantName); ?></strong></a></span>

							<label class="half left"><!--span><?php echo SCHEDULED_FOR; ?></span-->
								<select class="rounded-select" name="date">
									<?php commonDatesList(@$_SESSION['s_venezvite']['search']['date']); ?>
								</select></label>
							<label class="half right">
								<select class="rounded-select" name="time">
								<?php commonTimesList(@$_SESSION['s_venezvite']['search']['date'], @$_SESSION['s_venezvite']['search']['time'], @$restaurant->current_delivery_zone['estimated_time'], @$restaurant->preparationTime); ?>
								</select></label>

							<select class="clear rounded-select" name="type">
								<option value="D"><?php echo DELIVERY; ?></option>
								<option<?php echo @$_SESSION['s_venezvite']['search']['type']=='T' ? ' selected="selected"' : ''; ?> value="T"><?php echo TAKEAWAY; ?></option>
							</select>

							<a class="disabled rounded-red-button" href="javascript:;"><?php echo CHECKOUT; ?></a></div>
						<?php
							}
							
							$checkout_button_text = CHECKOUT;
							require_once REL . '/views/inc/shopping-cart.inc.php';
						?>
					</div>
				</div>
				
				<div class="grand-total">
					<span class="label"><?php echo GRAND_TOTAL; ?></span>
					<span class="grand-total-value">0.00</span>
					<span class='currency'><?php echo !empty($restaurant) ? $restaurant->currency : ''; ?></span>
				</div>
			</form>
		</div>
		<div class="hidden visible-tablet visible-mobile preceed-to-checkout" style="display: none">
			<a href="javascript:;" class="preceed"><?php echo CONTINUE_TO_CHECKOUT ?></a>
		</div>
<?php
	}
