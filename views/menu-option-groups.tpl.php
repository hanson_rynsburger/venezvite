<?php
	if (@$is_included && !empty($restaurantAdmin) && @is_array($menu_options_groups) && @is_array($menu_options) && @is_array($menu_items)) {
		echo '<div class="main-content">';
		require_once 'inc/logged-restaurant-header.inc.php';
?>
				<div id="menu-option-groups-body">
						<!--h1><?php echo MENU_OPTION_GROUPS; ?></h1-->
						
						<form id="add-menu-option-group-form" method="post">
							<div class="white-container">
								<input name="id" type="hidden" value="" />
								<strong class="title"><?php echo ADD_OPTIONS_GROUP; ?></strong>
								
								<label class="half left"><input class="rounded-input" maxlength="100" name="options_group" placeholder="<?php echo OPTIONS_GROUP_NAME; ?>" value="" type="text" /></label>
								<div class="half right">
									<label class="half left"><select class="rounded-select" name="selectables_rule">
											<option value=""><?php echo HOW_MANY; ?></option>
											<option value="E"><?php echo EXACTLY; ?></option>
											<option value="UT"><?php echo MAX_OF; ?></option>
											<option value="AL"><?php echo MIN_OF; ?></option>
										</select></label>
									<label class="half right"><input class="rounded-input" maxlength="3" name="selectables" value="" type="text" /></label>
									<div class="clear"></div>
								</div>
								
								<label class="clear half"><select class="rounded-select" id="menu-item-option">
										<option value=""><?php echo ADD_MENU_OPTIONS; ?></option>
<?php
		foreach ($menu_options as $menu_option) {
			$data = array(
				'id'			=> $menu_option->idMenuItemOption, 
				'option_name'	=> $menu_option->menuItemOption, 
				'price'			=> $menu_option->price
			);
			
			echo '<option data-option="', htmlspecialchars(json_encode($data)), '" value="', htmlspecialchars($menu_option->idMenuItemOption), '">', 
				htmlspecialchars($menu_option->menuItemOption), ' (', $menu_option->price, ' ', $restaurantAdmin->restaurant->currency, ')</option>';
		}
?>
										<option value="-1"><?php echo NEW_MENU_OPTION; ?></option>
									</select></label>
								<div class="clear" id="menu-item-options"></div>
								
								<div id="new-menu-item-option">
									<input disabled="disabled" name="option_id" value="" type="hidden" />
									
									<label class="half left"><input class="rounded-input" disabled="disabled" maxlength="50" name="menu_option" placeholder="<?php echo MENU_OPTION_NAME; ?>" value="" type="text" /></label>
									<div class="half right">
										<label class="half left"><input class="rounded-input" disabled="disabled" maxlength="6" name="price" placeholder="<?php echo str_replace('{$currency}', $restaurantAdmin->restaurant->currency, PRICE_CURRENCY); ?>" value="" type="text" /></label>
										<div class="half right">
											<label class="half left"><a class="rounded-red-button" formnovalidate href="javascript:;"><?php echo SAVE; ?></a></label>
											<label class="half right"><a class="disabled rounded-red-button" formnovalidate href="javascript:;"><?php echo DELETE; ?></a></label>
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</div>
								</div>
								<input name="options" type="hidden" value="" />
								
								<label class="clear half"><input class="rounded-red-button" type="submit" value="<?php echo ADD_OPTIONS_GROUP_BUTTON; ?>" /></label>
								<label class="half right"><input class="disabled rounded-red-button" type="button" value="<?php echo CANCEL; ?>" /></label>
							</div>
						</form>
<?php
		if (count($menu_options_groups)) {
			echo '<ul id="menu-option-groups">';
			
			foreach ($menu_options_groups as $menu_options_group) {
				$used_menu_items = array();
				foreach ($menu_items as $menu_item) {
					if (!empty($menu_item->menuItemGroups) && in_array($menu_options_group, $menu_item->menuItemGroups)) {
						$used_menu_items[] = trim($menu_item->menuItemName);
					}
				}
				
				$menu_options = $menu_option_ids = array();
				foreach ($menu_options_group->menuItemOptions as $menu_option) {
					$menu_options[] = htmlspecialchars(trim($menu_option->menuItemOption)) . ' (' . $menu_option->price . ' ' . $restaurantAdmin->restaurant->currency . ')';
					$menu_option_ids[] = $menu_option->idMenuItemOption;
				}
				natcasesort($menu_options);
				
				$menu_item_group_object = json_encode(array(
						'id'			=> $menu_options_group->idMenuItemCategory, 
						'group'			=> $menu_options_group->menuItemCategory, 
						'selectables'	=> $menu_options_group->selectables, 
						'rule'			=> $menu_options_group->selectablesRule, 
						'options'		=> $menu_option_ids
					));
				
				echo '<li data-menu-option-group="', htmlspecialchars($menu_item_group_object), '" data-id="', $menu_options_group->idMenuItemCategory, '" data-menu-items="', htmlspecialchars(json_encode($used_menu_items)), '">', 
						'<div class="left"><span class="drag-icon"></span>', 
							'<strong class="name">', htmlspecialchars($menu_options_group->menuItemCategory), ' (', count($menu_options_group->menuItemOptions), ' options)</strong><br />', 
							implode(' &nbsp;&bull;&nbsp; ', $menu_options), '</div>', 
						'<div class="right">', 
							'<span class="actions edit"></span><br />', 
							'<span class="actions delete', (count($used_menu_items) ? ' disabled' : ''), '"></span>', 
						'</div>', 
						'<div class="clear"></div>', 
					'</li>';
			}
			
			echo '</ul>';
			
		} else {
			echo '<div class="white-container">', 
					'<p>', NO_MENU_OPTION_GROUPS, '</p>', 
				'</div>';
		}
?>
						<script>var langAddItem = '<?php echo str_replace('\'', '\\\'', ADD_OPTIONS_GROUP); ?>', 
langEditItem = '<?php echo EDIT_ITEM; ?>', 
langAddButton = '<?php echo str_replace('\'', '\\\'', ADD_OPTIONS_GROUP_BUTTON); ?>', 
langEditButton = '<?php echo EDIT_ITEM_BUTTON; ?>', 
langOptionDeleteConfirmation = '<?php echo OPTION_DELETE_CONFIRMATION; ?>', 
langDisabledCategory = '<?php echo str_replace('\'', '\\\'', DISABLED_ALERT) ; ?>', 
langRemoveConfirmation = '<?php echo str_replace('\'', '\\\'', REMOVE_CONFIRMATION); ?>';</script>
					</div>
				</div>
<?php
	}
