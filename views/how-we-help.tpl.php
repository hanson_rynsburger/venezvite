<?php
	if (@$is_included) {
		require_once 'inc/restaurant-join-menu.inc.php';
?>
				<div id="how-we-help-body">
					<div class="main-content">
						
						<div class="centered" id="intro-text">
							<h2>Find new customers, generate repeat business, increase revenues and grow your bottom line.</h2>
							<p class="tall">By joining the Venezvite restaurant network, you'll give thousands of hungry people the ability to order delivery or pickup from your restaurant, online or on a mobile phone.</p>
							<a class="inline rounded-red-button" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/restaurant-join.html">Join Our Network</a>
							
							<h3>What is Venezvite?</h3>
							<p>We offer a personal marketing program and provide an efficient and fun way to take orders without the hassle of you spending precious minutes on the phone, we cover processing and credit card fees allowing your customers to use more payment forms, we advertise and market your business everyday to our loyal customer base. In order to offer these services we collect a commission fee of the total sales generated from the site by our online customer orders only.</p>
							<p>Our job is to bring hungry people to your establishment. At the end of the agreed upon payment schedule, Venezvite pays your restaurant for the ordered meals. All you need to dois join now!</p>
						</div>
						
						<h4>Let's explain how it works</h4>
						<ol>
							<li><strong>Create an Account on Venezvite.com</strong><ul>
									<li>Restaurant Name</li>
									<li>Address</li>
									<li>Hours of Operation</li>
									<li>Fill in your Menu with Prices</li>
									<li>Upload images of your Restaurant and/or Cuisine</li>
									<li>E-mail address and Mobile phone number</li>
								</ul></li>
							<li><strong>Start Receiving Orders.</strong> As the orders come in, you will receive it via email. To confirm the order, click Accept. To decline the order, click decline and leave a message (ex. We are currently closed for the holiday).</li>
						</ol>
						
						<h4>3 Types of Customers</h4>
						<ol>
							<li><strong>The Dining Customer</strong>
								<p>"Let's go out to eat". This is a dining customer. This customer has made the decision to eat at a particular restaurant prior to leaving their house.</p></li>
							<li><strong>The To Go Customer</strong>
								<p>"I'll pick something up on the way back from work." This is a restaurant TO GO customer. This customer has already made the decision to pick up a meal at a particular restaurant prior to leaving their office or home.</p></li>
							<li><strong>YOUR NEW CUSTOMER!</strong>
								<p>"Let's get something delivered". This is YOUR NEW CUSTOMER with VenezVite. The customer has decided upon a cuisine ("I feel like Italian") but has not yet decided on a restaurant. They visit our website, type in their address, and our amazing system delivers a list of restaurants that are within the delivery area to their address.</p></li>
						</ol>
						
						<h4>What's Next?</h4>
						<p>The Venezvite customer places their order with our extremely user-friendly site that makes ordering food online fun and exciting. Their orders are saved for future repeat-orders, and they can rate/recommend/like specific items on the menu to encourage new customers to purchase their favored dish from their favored restaurants.</p>
						
						<h4>Benefits</h4>
						<ol>
							<li>Reach new loyal customers &amp; increase your visibility</li>
							<li>Increase sales and profitability</li>
							<li>Free direct marketing / advertising</li>
							<li>Access to a database of existing and potential new customers to promote specials/promotions</li>
							<li>Reap the benefits of our Loyalty reward program which encourages customers to return to your business inStore and online!</li>
						</ol>
						
						<div class="centered">
							<h5>Free membership! There are no fees to join Venezvite</h5>
							<a class="inline rounded-red-button" href="<?php echo ROOT, $_SESSION['s_venezvite']['language']->languageAcronym; ?>/restaurant-join.html">Join Our Network</a>
						</div>
					</div>
				</div>
<?php
	}
