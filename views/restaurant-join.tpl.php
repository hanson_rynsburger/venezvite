<?php
	if (@$is_included && !empty($countries) && !empty($cuisine_types)) {
		require_once 'inc/restaurant-join-menu.inc.php';
?>
				<div id="restaurant-join-body">
					<form method="post" enctype="multipart/form-data">
						<div class="main-content">
							<?php
							if (!empty($_SESSION['s_venezvite']['restaurant_registration_message'])) {
							?>
							<div<?php echo empty($error) ? ' class="success"' : ''; ?> id="error-container" style="height:auto">
								<div>
									<strong><?php echo !empty($error) ? SOME_ERRORS : REGISTRATION_SUCCESS; ?></strong>
									<span><?php echo $_SESSION['s_venezvite']['restaurant_registration_message']; ?></span>
								</div>
							</div>
							<?php
								unset($_SESSION['s_venezvite']['restaurant_registration_message']);
							}
							?>
							<ol>
								<li><?php echo CONTACT_INFO; ?><br />
									<div class="half left">
										<input class="rounded-input" maxlength="50" name="last_name" placeholder="<?php echo LAST_NAME; ?> *" type="text" value="<?php echo !empty($_POST['last_name']) ? htmlspecialchars($_POST['last_name']) : ''; ?>" />
										<input class="rounded-input" maxlength="50" name="first_name" placeholder="<?php echo FIRST_NAME; ?> *" type="text" value="<?php echo !empty($_POST['first_name']) ? htmlspecialchars($_POST['first_name']) : ''; ?>" />
									</div>
									<div class="half right">
										<input class="rounded-input" maxlength="20" name="mobile" placeholder="<?php echo MOBILE; ?> *" type="text" value="<?php echo !empty($_POST['mobile']) ? htmlspecialchars($_POST['mobile']) : ''; ?>" />
										<!--input class="rounded-input" maxlength="50" name="email" placeholder="<?php echo RESTAURANT_EMAIL; ?> *" type="text" value="<?php echo !empty($_POST['email']) ? htmlspecialchars($_POST['email']) : ''; ?>" /-->
									</div>
									<div class="clear"></div>
								</li>
								
								<li><?php echo RESTAURANT_INFO; ?> <span>*</span><br />
									<div class="half left" style="margin-bottom:-13px">
										<select class="rounded-select" name="country">
											<option data-abbr="CH" value=""><?php echo SELECT_COUNTRY; ?></option>
											<?php
											foreach ($countries as $country) {
											?>
											<option data-abbr="<?php echo $country->countryAcronym; ?>" value="<?php echo $country->idCountry; ?>"><?php echo $country->countryName; ?></option>
											<?php
											}
											?>
										</select>
									</div>

									<div class="clear half left">
										<input class="rounded-input" maxlength="50" name="restaurant" placeholder="<?php echo RESTAURANT_NAME; ?> *" type="text" value="<?php echo !empty($_POST['restaurant']) ? htmlspecialchars($_POST['restaurant']) : ''; ?>" />
										<input class="rounded-input" maxlength="100" name="address" placeholder="<?php echo RESTAURANT_ADDRESS; ?> *" type="text" value="<?php echo !empty($_POST['address']) ? htmlspecialchars($_POST['address']) : ''; ?>" />
										<input class="rounded-input" maxlength="50" name="city" placeholder="<?php echo RESTAURANT_CITY; ?>" type="text" value="<?php echo !empty($_POST['city']) ? htmlspecialchars($_POST['city']) : ''; ?>" />
										<input name="latitude" type="hidden" value="<?php echo !empty($_POST['latitude']) ? htmlspecialchars($_POST['latitude']) : ''; ?>" />
										<input name="longitude" type="hidden" value="<?php echo !empty($_POST['longitude']) ? htmlspecialchars($_POST['longitude']) : ''; ?>" />
									</div>
									<div class="half right">
										<input class="rounded-input" maxlength="10" name="zip" placeholder="<?php echo POSTAL_CODE; ?> *" type="text" value="<?php echo !empty($_POST['zip']) ? htmlspecialchars($_POST['zip']) : ''; ?>" />
										<input class="rounded-input" maxlength="20" name="phone" placeholder="<?php echo RESTAURANT_PHONE; ?> *" type="text" value="<?php echo !empty($_POST['phone']) ? htmlspecialchars($_POST['phone']) : ''; ?>" />
										<input class="rounded-input" maxlength="100" name="website" placeholder="<?php echo RESTAURANT_URL; ?> *" type="text" value="<?php echo !empty($_POST['website']) ? htmlspecialchars($_POST['website']) : ''; ?>" />
									</div>

									<textarea class="clear rounded-textarea" cols="80" name="description" placeholder="<?php echo RESTAURANT_DESC; ?>" rows="8"><?php echo !empty($_POST['description']) ? htmlspecialchars($_POST['description']) : ''; ?></textarea>
								</li>

								<li class="half left"><?php echo PRIMARY_CUISINE_TYPE; ?><br />
									<div id="cuisines-list">
										<select class="rounded-select" multiple="multiple" name="cuisines[]" data-placeholder="<?php echo CHOOSE_CUISINE; ?>">
										<?php
										foreach ($cuisine_types as $cuisine_type) {
											$selected = (is_array(@$_POST['cuisines']) && in_array($cuisine_type->idCuisineType, $_POST['cuisines']));
											echo '<option', ($selected ? ' selected="selected"' : ''), ' value="', $cuisine_type->idCuisineType, '">', $cuisine_type->cuisineType, '</option>';
										}
										?>
										</select>
									</div>
								</li>

								<li class="half right"><?php echo CHOOSE_SERVICES; ?><br />
									<div>
										<label><input<?php echo @$_POST['delivery']=='Y' ? ' checked="checked"' : ''; ?> name="delivery" type="checkbox" value="Y" /> <?php echo DELIVERY; ?></label> &nbsp; <label><input<?php echo @$_POST['takeaway']=='Y' ? ' checked="checked"' : ''; ?> name="takeaway" type="checkbox" value="Y" /> <?php echo TAKEAWAY; ?></label> &nbsp; <label><input<?php echo @$_POST['catering']=='Y' ? ' checked="checked"' : ''; ?> name="catering" type="checkbox" value="Y" /> <?php echo CATERING; ?></label> &nbsp; <label><input<?php echo @$_POST['custom_delivery']=='Y' ? ' checked="checked"' : ''; ?> name="custom_delivery" type="checkbox" value="Y" /> <?php echo CUSTOM_DELIVERY; ?></label>
									</div>
								</li>

								<li class="clear half left" id="timetable"><?php echo HOURS_OPERATION; ?> <span>*</span><br />
									<div>
										<label><input type="checkbox" value="Y" /> <?php echo SAME_EVERY_DAY; ?></label>
										<?php
										setlocale(LC_TIME, $_SESSION['s_venezvite']['language']->localeCode); // 'fra'
										$timestamp = strtotime('next Monday');
										for ($i=1; $i<=7; $i++) {
										?>
										<div class="first time">
											<span class="left"><?php echo ucfirst(utf8_encode(strftime('%A', $timestamp))); ?></span>
											<?php
											draw_time_interval('hour_start', $i, 0, @$_POST['hour_start'][$i][0]);
											draw_time_interval('hour_end', $i, 0, @$_POST['hour_end'][$i][0]);
											?>
											<a class="right" href="javascript:;"><?php echo @$_POST['hour_start'][$i][1] && @$_POST['hour_end'][$i][1] ? '-' : '+'; ?></a>
											<div class="clear"></div>
										</div>
										<?php
										if (@$_POST['hour_start'][$i][1] && @$_POST['hour_end'][$i][1]) {
										?>
										<div class="second time">
											<span class="left"></span>
											<?php
											draw_time_interval('hour_start', $i, 1, @$_POST['hour_start'][$i][1]);
											draw_time_interval('hour_end', $i, 1, @$_POST['hour_end'][$i][1]);
											?>
											<div class="clear"></div>
										</div>
										<?php
											}
											$timestamp = strtotime('+1 day', $timestamp);
										}
										?>
									</div>
								</li>
								
								<li class="half right" id="delivery-zones"><?php echo DELIVERY_ZONES; ?> <span>*</span><br />
									<div>
										<span><?php echo ZONE; ?></span>
										<input class="rounded-input" disabled="disabled" maxlength="3" name="zone_range[0]" placeholder="<?php echo ZONE_RANGE; ?>" type="text" value="<?php echo @$_POST['zone_range'][0] ? htmlspecialchars($_POST['zone_range'][0]) : ''; ?>" />
										<input class="rounded-input" disabled="disabled" maxlength="4" name="zone_minimum[0]" placeholder="<?php echo MIN_DELIVERY; ?>" type="text" value="<?php echo @$_POST['zone_minimum'][0] ? htmlspecialchars($_POST['zone_minimum'][0]) : ''; ?>" />
										<input class="rounded-input" disabled="disabled" maxlength="3" name="zone_fee[0]" placeholder="<?php echo DELIVERY_FEE_ZONE; ?>" type="text" value="<?php echo @$_POST['zone_fee'][0] ? htmlspecialchars($_POST['zone_fee'][0]) : ''; ?>" />
										<select class="rounded-select" disabled="disabled" name="zone_estimated_time[0]">
											<option value=""><?php echo SELECT_TIME_INTERVAL; ?></option>
											<option value="15">5-15 minutes</option>
											<option value="20">10-20 minutes</option>
											<option value="25">15-25 minutes</option>
											<option value="30">20-30 minutes</option>
											<option value="35">25-35 minutes</option>
											<option value="40">30-40 minutes</option>
											<option value="45">35-45 minutes</option>
										</select>
									</div>
									<?php
									if (is_array(@$_POST['zone_range']) && is_array(@$_POST['zone_minimum']) && is_array(@$_POST['zone_fee']) && 
										count($_POST['zone_range'])==count($_POST['zone_minimum']) && count($_POST['zone_range'])==count($_POST['zone_fee']) && 
										count($_POST['zone_range'])>1) {
										
										for ($i=1; $i<count($_POST['zone_range']); $i++) {
									?>
									<div>
										<span><?php echo str_replace('1', ($i + 1), ZONE); ?></span>
										
										<input class="rounded-input" maxlength="3" name="zone_range[<?php echo $i; ?>]" placeholder="<?php echo str_replace('1', ($i + 1), ZONE_RANGE); ?>" type="text" value="<?php echo htmlspecialchars(@$_POST['zone_range'][$i]); ?>" />
										<input class="rounded-input" maxlength="4" name="zone_minimum[<?php echo $i; ?>]" placeholder="<?php echo str_replace('1', ($i + 1), MIN_DELIVERY); ?>" type="text" value="<?php echo htmlspecialchars(@$_POST['zone_minimum'][$i]); ?>" />
										<input class="rounded-input" maxlength="3" name="zone_fee[<?php echo $i; ?>]" placeholder="<?php echo str_replace('1', ($i + 1), DELIVERY_FEE_ZONE); ?>" type="text" value="<?php echo htmlspecialchars(@$_POST['zone_fee'][$i]); ?>" />
									</div>
									<?php
										}
									}
									?>
									<a href="javascript:;">+ <?php echo ADD_ZONE; ?></a>
								</li>
							</ol>
							<div class="clear"></div>
							
							<h1><?php echo CREATE_ACCOUNT_TITLE; ?></h1>
							<div class="center">
								<input class="rounded-input" maxlength="50" name="username" placeholder="<?php echo USERNAME; ?>" type="text" value="<?php echo !empty($_POST['username']) ? htmlspecialchars($_POST['username']) : ''; ?>" />
								<input class="rounded-input" maxlength="50" name="confirm_username" placeholder="<?php echo CONFIRM_USERNAME; ?>" type="text" value="<?php echo !empty($_POST['confirm_username']) ? htmlspecialchars($_POST['confirm_username']) : ''; ?>" />
								<input class="rounded-input" maxlength="20" name="password" placeholder="<?php echo PASSWORD; ?>" type="password" value="<?php echo !empty($_POST['password']) ? htmlspecialchars($_POST['password']) : ''; ?>" />
								<input class="rounded-input" maxlength="20" name="confirm_password" placeholder="<?php echo CONFIRM_PASSWORD; ?>" type="password" value="<?php echo !empty($_POST['confirm_password']) ? htmlspecialchars($_POST['confirm_password']) : ''; ?>" /><br />
								<br />
								<label><input name="terms" type="checkbox" value="Y" /> <?php echo str_replace('{$link}', ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/tou-restaurants.html', AGREE_TERMS); ?></label>
							</div>
							
							<input class="rounded-red-button" type="submit" value="<?php echo CREATE_ACCOUNT; ?>" />
						</div>
					</form>
				</div>
				<script>
var langInvalidAddress = '<?php echo str_replace('\'', '\\\'', INVALID_ADDRESS) ; ?>', 
langEnterValidPhone = '<?php echo ENTER_VALID_PHONE; ?>';</script>
<?php
	}
