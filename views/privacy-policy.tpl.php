<?php
	if (@$is_included) {
		$submenu_title = 'PRIVACY POLICY';
		require_once 'inc/others-sub-menu.inc.php';
?>
				<div id="privacy-policy-body">
					<div class="main-content">
						
						<h2>Privacy Policy</h2>
						<strong>Introduction</strong><br />
						<p>[VENEZVITE, LLC] (we" or "us") values its visitors' privacy. This privacy policy is effective [04/15/12]; it summarizes what information we might collect from a registered user or other visitor ("you"), and what we will and will not do with it.</p>
						<p>Please note that this privacy policy does not govern the collection and use of information by companies that [VENEZVITE, LLC] does not control, nor by individuals not employed or managed by [VENEZVITE, LLC]. If you visit a Web site that we mention or link to, be sure to review its privacy policy before providing the site with information.</p>
						
						<strong>What we do with your personally identifiable information</strong><br />
						<p>It is always up to you whether to disclose personally identifiable information to us, although if you elect not to do so, we reserve the right not to register you as a user or provide you with any products or services. "Personally identifiable information" means information that can be used to identify you as an individual, such as, for example:</p>
						<ul>
							<li>your name, company, email address, phone number, billing address, and shipping address</li>
							<li>your [VENEZVITE, LLC] user ID and password (if applicable)</li>
							<li>credit card information (if applicable)</li>
							<li>any account-preference information you provide us</li>
							<li>your computer's domain name and IP address, indicating where your computer is located on the Internet</li>
							<li>session data for your login session, so that our computer can 'talk' to yours while you are logged in</li>
						</ul>
						<p>If you do provide personally identifiable information to us, either directly or through a reseller or other business partner, we will:</p>
						<ul>
							<li>not sell or rent it to a third party without your permission - although unless you opt out (see below), we may use your contact information to provide you with information we believe you need to know or may find useful, such as (for example) news about our services and products and modifications to the Terms of Service;</li>
							<li>take commercially reasonable precautions to protect the information from loss, misuse and unauthorized access, disclosure, alteration and destruction;</li>
							<li>not use or disclose the information except:
								<ul>
									<li>as necessary to provide services or products you have ordered, such as (for example) by providing it to a carrier to deliver products you have ordered;</li>
									<li>in other ways described in this privacy policy or to which you have otherwise consented;</li>
									<li>in the aggregate with other information in such a way so that your identity cannot reasonably be determined (for example, statistical compilations);</li>
									<li>as required by law, for example, in response to a subpoena or search warrant;</li>
									<li>to outside auditors who have agreed to keep the information confidential;</li>
									<li>to a successor organization in the event of a merger, acquisition, bankruptcy, or other sale or disposition of all or a portion of [VENEZVITE, LLC]'s assets. The successor organization's use and disclosure of your personally-identifiable information will continue to be subject to this privacy policy unless (i) a court orders otherwise, for example a bankruptcy court; or (ii) the successor organization gives you notice that your personally-identifiable information will be subject to the successor organization's own privacy policy, along with an opportunity for you to opt out (which may cause you not to be able to continue to use the [WEB SITE OR SOFTWARE NAME]). If you submit personally-identifiable information after such a transfer, that information may be subject to the successor entity's privacy policy;</li>
									<li>as necessary to enforce the Terms of Use;</li>
									<li>as necessary to protect the rights, safety, or property of [VENEZVITE, LLC], its users, or others; this may include (for example) exchanging information with other organizations for fraud protection and/or risk reduction.</li>
								</ul>
							</li>
						</ul>
						<p></p>
						
						<strong>Other information we collect</strong>
						<p>We may collect other information that cannot be readily used to identify you, such as (for example) the domain name and IP address of your computer. We may use this information, individually or in the aggregate, for technical administration of our Web site(s); research and development; customer- and account administration; and to help us focus our marketing efforts more precisely.</p>
						
						<strong>Cookies</strong>
						<p>[VENEZVITE, LLC] uses "<a href="http://simple.wikipedia.org/wiki/HTTP_cookie" rel="nofollow" target="_blank">cookies</a>" to store personal data on your computer. We may also link information stored on your computer in cookies with personal data about specific individuals stored on our servers. If you set up your Web browser (for example, Internet Explorer or Firefox) so that cookies are not allowed, you might not be able to use some or all of the features of our Web site(s).</p>
						
						<strong>External data storage sites</strong>
						<p>We may store your data on servers provided by third party hosting vendors with whom we have contracted.</p>
						
						<strong>Your privacy responsibilities</strong>
						<p>To help protect your privacy, be sure:</p>
						<ul>
							<li>not to share your user ID or password with anyone else;</li>
							<li>to log off the [VENEZVITE, LLC] Web site when you are finished;</li>
							<li>to take customary precautions to guard against "malware" (viruses, Trojan horses, bots, etc.), for example by installing and updating suitable anti-virus software.</li>
						</ul>
						
						<strong>Notice to European Union users</strong>
						<p>[VENEZVITE, LLC]'s operations are located primarily in the United States. If you provide information to us, the information will be transferred out of the European Union (EU) to the United States. By providing personal information to us, you are consenting to its storage and use as described herein.</p>
						
						<strong>Information collected from children</strong>
						<p>You must be at least 13 years old to use [VENEZVITE, LLC]'s Web site(s) and service(s). [VENEZVITE, LLC] does not knowingly collect information from children under 13. (See the [U.S.] <a href="http://en.wikipedia.org/wiki/Children's_Online_Privacy_Protection_Act" rel="nofollow" target="_blank">Children's Online Privacy Protection Act</a>.)</p>
						
						<strong>Changes to this privacy policy</strong>
						<p>We reserve the right to change this privacy policy as we deem necessary or appropriate because of legal compliance requirements or changes in our business practices. If you have provided us with an email address, we will endeavor to notify you, by email to that address, of any material change to how we will use personally identifiable information.</p>
						
						<strong>Questions or comments?</strong>
						<p>If you have questions or comments about [VENEZVITE, LLC]'s privacy policy, send email to support@[venezvite].com, or contact us via any of the ways described in the About Us page at [www.venezvite.com]. Thank you for choosing [VENEZVITE, LLC]!</p>
					</div>
				</div>
<?php
	}
