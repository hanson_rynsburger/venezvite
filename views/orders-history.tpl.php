<?php
	if (@$is_included && !empty($entity) && isset($orders) && is_array($orders)) {
		echo '<div class="main-content">';
		require_once 'inc/logged-user-header.inc.php';
?>
				<div id="orders-history-body">
					
						<!--h1><?php echo ORDERS_HISTORY; ?></h1-->
<?php
		if (count($orders)) {
?>
						<table class="white-container">
							<tbody>
<?php
			foreach ($orders as $order) {
				$status = PENDING;
				if ($order->dateProcessed) {
					$status = ACCEPTED;
					
					if ($order->rejectionReason) {
						$status = DECLINED;
					}
				}
?>
								<tr data-order="<?php echo $order->idOrder; ?>">
									<td><strong class="title"><?php echo $order->restaurant->restaurantName; ?></strong>
										(<?php echo $order->restaurant->address; ?>)</td>
									<td><?php echo utf8_encode(strftime('%x', strtotime($order->dateDesired))); ?> &nbsp;|&nbsp; <?php echo $order->type=='D' ? DELIVERY : TAKEAWAY; ?><br />
										<?php echo $order->type=='D' ? $order->userAddress->address . '<br />' : ''; ?>
										<?php echo ($order->value + $order->deliveryCost), ' ', $order->restaurant->currency; ?> &nbsp;<a class="view-order" href="javascript:;"><?php echo VIEW_ORDER; ?></a></td>
									<td><?php echo $status; ?></td>
								</tr>
<?php
			}
?>
							</tbody>
						</table>
<?php
		} else {
			if (!isset($_GET['past'])) {
				echo '<p>', NO_PENDING_ORDERS, ' <a href="', ROOT, $_SESSION['s_venezvite']['language']->languageAcronym, '/orders-history.html?past">', VIEW_PAST_ORDERS, '</a></p>';
			} else {
				echo '<p>', NO_PAST_ORDERS, '</p>';
			}
		}
?>

					</div>
				</div>
<?php
	}
