<?php
	if (@$is_included && is_array(@$_SESSION['s_venezvite']['order']) && !empty($entity) && 
		@$current_datetime && @$order_datetime_type) {

		// require_once 'inc/logged-user-header.inc.php';
		if (empty($restaurant) && ($restaurant_path = @json_decode($_COOKIE['c_venezvite'])->restaurant)) {
			$restaurant = restaurant::getByURL($restaurant_path);
		}
		
		$multiplier = $_SESSION['s_venezvite']['order']['multiplier'];
		$currency = $_SESSION['s_venezvite']['order']['currency'];
?>
				<div id="restaurant-intro-image"<?php echo count($restaurant->images) ? ' style="background-image:url(\'' . IMG . 'restaurants/' . $restaurant->images[0]->file . '\')"' : ''; ?>>
				</div>
				<div id="restaurant-intro">
					<div class="main-content">
						<h1 class="clear" itemprop="name">
							<?php echo str_replace( '{name}', $entity->firstName, THANK_YOU_YOUR_ORDER ); ?>
						</h1>
						<span class="confirm-check"></span>
					</div>
				</div>

				<div id="confirmation-body">
					<div class="main-content">
						<div class="text-center pre-header">
							<span class="red"><?php echo $order_date; ?></span>
							<?php if ( $_SESSION['s_venezvite']['order']['type']!='T' ) { ?>
							<span class="big">
							<?php 
								echo str_replace("{time}", $order_time, ESTIMATED_DELIVERY);
								echo ' ' . PLUS_MINUS;
							?></span><?php } ?>
						</div>
						<div class="order-detail clearfix">
							<div class="header row">
								<h2 class="col-6-12t"><?php echo ORDER_DETAIL ?></h2>
								<div class="fb-like col-6-12t text-right" data-header="false" data-href="http://www.facebook.com/likevenezvite" data-send="false" data-show-faces="false" data-stream="false" data-width="320"></div>
							</div>
							<div class="left">
								<p><?php echo str_replace('{order_no}', '#' . $_SESSION['s_venezvite']['order']['id'], 
									str_replace('{order_value}', $_SESSION['s_venezvite']['order']['value'], 
									str_replace('{order_date}', $current_datetime, 
									str_replace('{restaurant}', $_SESSION['s_venezvite']['order']['restaurant'], ORDER_DETAILS)))); ?></p>
								<p><?php echo YOUR_ORDER_FOR; ?><br />
									<strong class="red"><?php echo $order_datetime_type; ?></strong><?php echo $_SESSION['s_venezvite']['order']['type']!='T' ? ' +/- 15 minutes.' : ''; ?></p>

								<p class="ordered-from"><?php echo ORDERED_FROM ?><br/>
									<strong class="green"><?php echo $_SESSION['s_venezvite']['order']['restaurant']; ?></strong>
									<span class="green"><?php echo $_SESSION['s_venezvite']['order']['contact_phone'] ?></span>
								</p>
								<?php $addr = $_SESSION['s_venezvite']['order']['address']; ?>
								<p class="delivered-to"><?php echo DELIVER_TO ?><br/>
									<strong><?php echo $addr->user->firstName . " " . $addr->user->lastName ?></strong>
									<span>
										<?php echo "<em>" . $addr->addrType . "</em> " . /*$addr->buildingNo . " " . */$addr->address . ", " . $addr->city ?>
									</span>
									<span>
										<?php echo FLOOR_SUITE; ?> <?php echo $addr->floorSuite; ?>/<?php echo $addr->accessCode; ?>
									</span>
								</p>

								<p><strong class="green"><?php echo WHAT_NEXT_TITLE; ?></strong></p>
								<ol>
									<li><?php echo str_replace('{restaurant}', $_SESSION['s_venezvite']['order']['restaurant'], WHAT_NEXT_BODY1); ?></li>
									<li><?php echo str_replace('{restaurant}', $_SESSION['s_venezvite']['order']['restaurant'], WHAT_NEXT_BODY2); ?></li>
									<li><?php echo str_replace('{restaurant}', $_SESSION['s_venezvite']['order']['restaurant'], WHAT_NEXT_BODY3); ?></li>
								</ol>
							</div>
							<?php $subtotal = 0; ?>
							<div class="right">
								<h2 class="order-title"><?php echo MY_ORDER; ?></h2>
								<ul class="order-items">
									<?php foreach( $_SESSION['s_venezvite']['order']['cart_content'] as $cc ) { ?>
									<li class="menu-item clearfix">
										<span class="left quantity"><?php echo $cc["quantity"] ?></span>
										<div class="order-item-desc">
											<strong><?php echo $cc["name"] ?></strong>
											<?php if ( $cc["options"] ) { ?>
											<ul>
												<?php foreach( $cc["options"] as $cco ) { ?>
												<li class="menu-item-option" data-menu-item-option="<?php echo $cco["name"] ?>">
													<div class="menu-item-option-name"><?php echo $cco["name"] ?></div>
													<span class="price"><?php echo round(floatval( $cco["price"] ) * floatval( $cc["quantity"] ) * $multiplier, 2), $currency; ?></span>
												</li>
												<?php $subtotal += floatval( $cco["price"] ) * floatval( $cc["quantity"] ) * $multiplier; ?>
												<?php } ?>
											</ul>
											<?php } ?>
										</div>
										<span class="price"><?php echo round(floatval( $cc["unit_price"] ) * floatval( $cc["quantity"] ) * $multiplier, 2), $currency; ?></span>
										<?php $subtotal += floatval( $cc["unit_price"] ) * floatval( $cc["quantity"] ) * $multiplier; ?>
									</li>
									<?php } ?>
								</ul>
								<div class="order-summary">
									<table>
										<tbody>
											<tr>
												<td><?php echo SUB_TOTAL ?>:</td>
												<td><?php echo round($subtotal, 2), $currency; ?></td>
											</tr>
											<tr>
												<td><?php echo DELIVERY_FEE ?>:</td>
												<td><?php echo round($_SESSION['s_venezvite']['order']['delivery_fee'] * $multiplier, 2), $currency; ?></td>
											</tr>
											<tr>
												<td><?php echo PROMO_CODE ?>:</td>
												<td>0.00</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td><?php echo GRAND_TOTAL; ?>:</td>
												<td><?php echo $_SESSION['s_venezvite']['order']['value']?></td>
											</tr>
										</tfoot>
									</table>
									
								</div>
							</div>
						</div>

						<?php /* ?><div class="right">
							<img src="i/bs/banner-right-order.jpg" />
						</div>
						<div class="clear"></div> */ ?>
					</div>
				</div>
<?php /* ?>
				<form method="post">
					<div id="tell-a-friend-body">
						<h3>Help a friend. Tell a friend.</h3>
						<h4>Get 4.- off when your friends place their first order!</h4>

						<textarea class="rounded-textarea" cols="80" placeholder="Enter your friend's email adress. You may enter up to 5, separated by commas.
example: yourfriend@gmail.com, yourfriend2@gmail.com, yourfriend3@gmail.com" rows="4"></textarea>
						<input class="rounded-red-button" type="submit" value="Submit" />
					</div>
				</form>
				*/?>
				<script>var google_conversion_id=995221579,google_conversion_language='en',google_conversion_format='3',google_conversion_color='ffffff',google_conversion_label='H2_6CKWDpQsQy8DH2gM',google_remarketing_only=false;</script>
				<script src="//www.googleadservices.com/pagead/conversion.js"></script>
				<noscript><div style="display:inline"><img alt="" height="1" src="//www.googleadservices.com/pagead/conversion/995221579/?label=H2_6CKWDpQsQy8DH2gM&amp;guid=ON&amp;script=0" style="border-style:none" width="1" /></div></noscript>
<?php
		unset($_SESSION['s_venezvite']['order']);
	}
