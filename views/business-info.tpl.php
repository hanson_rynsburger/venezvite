<?php
	if (@$is_included && !empty($restaurantAdmin) && @is_array($timetable) && @is_array($delivery_zones)) {
		echo '<div class="main-content">';

		require_once 'inc/logged-restaurant-header.inc.php';
		
		$hours = array();
		for ($j=0; $j<=96; $j++) {
			$hour = floor($j/4);
			
			if (($j/4 - $hour)==.25) {
				$hour = $hour . ':15';
			} elseif (($j/4 - $hour)==.5) {
				$hour = $hour . ':30';
			} elseif (($j/4 - $hour)==.75) {
				$hour = $hour . ':45';
			} else {
				$hour = $hour . ':00';
			}
			if (strlen($hour)==4) {
				$hour = '0' . $hour;
			}
			
			$hours[] = $hour;
		}
?>
				<div id="my-profile-body">
					
						<!--h1><?php echo BUSINESS_INFO; ?></h1-->
						
						<div class="white-container" id="entity-details">
							<a class="green-button" data-related="edit-entity-details" href="javascript:;"><?php echo EDIT; ?></a>
							
							<strong class="title"><?php echo RESTAURANT_DETAILS; ?></strong>
							<span id="entity-name"><?php echo htmlspecialchars($restaurantAdmin->restaurant->restaurantName); ?></span><br />
							<span id="entity-address"><?php echo htmlspecialchars($restaurantAdmin->restaurant->address), '<br />', 
								htmlspecialchars($restaurantAdmin->restaurant->city . ', ' . $restaurantAdmin->restaurant->zipCode); ?></span><br />
							<span id="entity-phone"><?php echo htmlspecialchars($restaurantAdmin->restaurant->phone); ?></span><br />
							<span id="entity-mobile"><?php echo trim($restaurantAdmin->restaurant->mobile)!='' ? htmlspecialchars($restaurantAdmin->restaurant->mobile) . '<br />' : ''; ?></span>
							<span id="entity-website"><?php echo trim($restaurantAdmin->restaurant->website)!='' ? htmlspecialchars($restaurantAdmin->restaurant->website) . '<br />' : ''; ?></span>
							
							<small><?php echo RESTAURANT_DESC; ?>:</small>
							<span id="entity-description"><?php echo nl2br(htmlspecialchars($restaurantAdmin->restaurant->restaurantDescription)); ?></span>
						</div>
						<form id="entity-details-form" method="post">
							<div class="white-container" id="edit-entity-details">
								<strong class="title"><?php echo EDIT_RESTAURANT; ?></strong>

								<label class="half"><span><?php echo RESTAURANT_NAME; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="name" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->restaurantName); ?>" type="text" /></label>
								<label class="half right"><span><?php echo ADDRESS; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="address" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->address); ?>" type="text" /></label>
								<label class="clear half"><span><?php echo PHONE; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="phone" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->phone); ?>" type="text" /></label>
								<label class="half right"><span><?php echo MOBILE; ?>:</span>
									<input class="rounded-input" maxlength="50" name="mobile" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->mobile); ?>" type="text" /></label>
								<label class="clear half"><span><?php echo WEBSITE; ?>:</span>
									<input class="rounded-input" maxlength="50" name="website" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->website); ?>" type="text" /></label>

								<label class="clear wide"><span><?php echo RESTAURANT_INFO; ?>: (<?php echo REQUIRED; ?>)</span>
									<textarea class="rounded-textarea" name="description"><?php echo htmlspecialchars($restaurantAdmin->restaurant->restaurantDescription); ?></textarea></label>

								<div class="clear"></div>
								<a class="disabled rounded-red-button" data-related="entity-details"><?php echo CANCEL; ?></a><a class="rounded-red-button"><?php echo APPLY; ?></a>
							</div>
						</form>
						
						<div class="white-container" id="entity-admin">
							<a class="green-button" data-related="edit-entity-admin" href="javascript:;"><?php echo EDIT; ?></a>
							
							<strong class="title"><?php echo ADMIN_DETAILS; ?></strong>
							<span id="entity-first-name"><?php echo htmlspecialchars($restaurantAdmin->restaurant->contactFirstName); ?></span>
							<span id="entity-last-name"><?php echo htmlspecialchars($restaurantAdmin->restaurant->contactLastName); ?></span><br />
							<span id="entity-admin-email"><?php echo htmlspecialchars($restaurantAdmin->email); ?></span><br />
							
							<small><?php echo PASSWORD; ?></small>
							<span>**********</span>
							
							<small><?php echo NOTICE_EMAIL; ?>:</small>
							<span id="entity-email"><?php echo htmlspecialchars($restaurantAdmin->restaurant->email); ?></span>
						</div>
						<form id="entity-admin-form" method="post">
							<div class="white-container" id="edit-entity-admin">
								<strong class="title"><?php echo EDIT_ADMIN_DETAILS; ?></strong>
								
								<label class="half"><span><?php echo FIRST_NAME; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="first_name" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->contactFirstName); ?>" type="text" /></label>
								<label class="half right"><span><?php echo LAST_NAME; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="last_name" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->contactLastName); ?>" type="text" /></label>
								<label class="clear half"><span><?php echo LOGIN_EMAIL; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="50" name="admin_email" value="<?php echo htmlspecialchars($restaurantAdmin->email); ?>" type="text" /></label>
								
								<label class="clear half"><span><?php echo NEW_PASS; ?>: (<?php echo OPTIONAL; ?>)</span>
									<input class="rounded-input" id="entity-password-value" maxlength="50" name="password" value="" type="password" /></label>
								<label class="half right"><span><?php echo CONFIRM_PASS; ?>: (<?php echo OPTIONAL; ?>)</span>
									<input class="rounded-input" maxlength="50" name="confirm_password" value="" type="password" /></label>
								
								<label class="clear half"><span><?php echo NOTICE_EMAIL; ?>: (<?php echo REQUIRED; ?>)</span>
									<input class="rounded-input" maxlength="250" name="email" value="<?php echo htmlspecialchars($restaurantAdmin->restaurant->email); ?>" type="text" /></label>
								
								<div class="clear"></div>
								<a class="disabled rounded-red-button" data-related="entity-admin"><?php echo CANCEL; ?></a><a class="rounded-red-button"><?php echo APPLY; ?></a>
							</div>
						</form>
						
						<div class="white-container" id="entity-hours">
							<a class="green-button" data-related="edit-entity-hours" href="javascript:;"><?php echo EDIT; ?></a>
							
							<strong class="title"><?php echo HOURS_OPERATIONS; ?></strong>
							<table>
								<tbody>
<?php
		setlocale(LC_TIME, $_SESSION['s_venezvite']['language']->localeCode);
		$timestamp = strtotime('next Monday');
		
		for ($i=1; $i<=7; $i++) {
			// Let's get today's hours of operation sets (if any)
			$hours_string = array();
			
			foreach ($timetable as $dow_hours) {
				if ($dow_hours['dow']==$i) {
					// The restaurant is open for the current day of week
					$hours_string[] = substr($dow_hours['openingHour'], 0, 5) . ' - ' . substr($dow_hours['closingHour'], 0, 5);
				}
			}
?>
									<tr>
										<td><?php echo ucfirst(strftime('%A', $timestamp)); ?></td>
										<td><?php echo count($hours_string) ? implode(' / ', $hours_string) : CLOSED; ?></td>
									</tr>
<?php
			$timestamp = strtotime('+1 day', $timestamp);
		}
?>
								</tbody>
							</table>
						</div>
						<form id="entity-hours-form" method="post">
							<div class="white-container" id="edit-entity-hours">
								<strong class="title"><?php echo EDIT_HOURS_OPERATIONS; ?></strong>
								
<?php
		$timestamp = strtotime('next Monday');
		
		for ($i=1; $i<=7; $i++) {
			// Let's get today's hours of operation sets (if any)
			$hours_of_operations = array();
			
			foreach ($timetable as $dow_hours) {
				if ($dow_hours['dow']==$i) {
					// The restaurant is open for the current day of week
					$hours_of_operations[] = array(
						'opening_hour'	=> substr($dow_hours['openingHour'], 0, 5), 
						'closing_hour'	=> substr($dow_hours['closingHour'], 0, 5)
					);
				}
			}
?>
								<strong><?php echo ucfirst(strftime('%A', $timestamp)); ?></strong><br />
								<label class="quarter"><span><?php echo MAIN_INTERVAL; ?></span></label>
								<label class="quarter"><select class="rounded-select" name="first_start[]">
										<option value=""><?php echo CLOSED; ?></option>
<?php
			foreach ($hours as $hour) {
				if ($hour=='24:00') {
					continue;
				}
				
				$selected = false;
				if (count($hours_of_operations)>0 && $hours_of_operations[0]['opening_hour']==$hour) {
					$selected = true;
				}
?>
										<option<?php echo $selected ? ' selected="selected"' : ''; ?> value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
<?php
			}
?>
									</select> - <select class="rounded-select" name="first_end[]">
										<option value=""><?php echo CLOSED; ?></option>
<?php
			foreach ($hours as $hour) {
				if ($hour=='00:00') {
					continue;
				}
				
				$selected = false;
				if (count($hours_of_operations)>0 && $hours_of_operations[0]['closing_hour']==$hour) {
					$selected = true;
				}
?>
										<option<?php echo $selected ? ' selected="selected"' : ''; ?> value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
<?php
			}
?>
									</select></label>
								<label class="quarter"><span><?php echo SECONDARY_INTERVAL; ?></span></label>
								<label class="quarter"><select class="rounded-select" name="last_start[]">
										<option value=""><?php echo CLOSED; ?></option>
<?php
			foreach ($hours as $hour) {
				if ($hour=='24:00') {
					continue;
				}
				
				$selected = false;
				if (count($hours_of_operations)>0 && !empty($hours_of_operations[1]) && $hours_of_operations[1]['opening_hour']==$hour) {
					$selected = true;
				}
?>
										<option<?php echo $selected ? ' selected="selected"' : ''; ?> value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
<?php
			}
?>
									</select> - <select class="rounded-select" name="last_end[]">
										<option value=""><?php echo CLOSED; ?></option>
<?php
			foreach ($hours as $hour) {
				if ($hour=='00:00') {
					continue;
				}
				
				$selected = false;
				if (count($hours_of_operations)>0 && !empty($hours_of_operations[1]) && $hours_of_operations[1]['closing_hour']==$hour) {
					$selected = true;
				}
?>
										<option<?php echo $selected ? ' selected="selected"' : ''; ?> value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
<?php
			}
?>
									</select></label>
								<div class="clear"></div>
<?php
			$timestamp = strtotime('+1 day', $timestamp);
		}
?>
								
								<a class="disabled rounded-red-button" data-related="entity-hours"><?php echo CANCEL; ?></a><input class="rounded-red-button" type="submit" value="<?php echo APPLY; ?>" />
							</div>
						</form>
						
						<div class="<?php echo ($restaurantAdmin->restaurant->customDelivery=='Y' ? 'disabled-container ' : ''); ?>white-container" id="entity-delivery-zones">
							<strong class="title"><?php echo DELIVERY_ZONES; ?></strong>
<?php
		if ($delivery_zones) {
?>
							<table>
								<tbody>
<?php
			$delivery_zones_ = array();
			foreach ($delivery_zones as $index => $delivery_zone) {
				if (!empty($delivery_zone->coordinates)) {
					$paths = json_decode($delivery_zone->coordinates);
					
					if ($paths) {
						$delivery_zones_[] = array(
							'data'	=> json_encode($delivery_zone), 
							'paths'	=> $paths, 
							'index'	=> count($delivery_zones) - $index
						);
					}
				}
?>
									<tr>
										<td><?php echo ZONE, ' ', ($index + 1); ?></td>
										<td><?php echo $restaurantAdmin->restaurant->currency, ' ', $delivery_zone->minimumDelivery, ' ', MINIMUM; ?></td>
										<td><?php echo FEE, ': ', $restaurantAdmin->restaurant->currency, ' ', $delivery_zone->deliveryFee; ?></td>
										<td><?php echo $delivery_zone->estimatedTime, ' ', MINUTES; ?></td>
										<td><?php echo (!empty($delivery_zone->coordinates) ? '<a class="green-button" href="javascript:;">' . EDIT . '</a>' : ''); ?></td>
									</tr>
<?php
			}
?>
								</tbody>
							</table>
<?php
		} else {
?>
							<p>You have no delivery zones defined.</p>
<?php
		}
?>
						</div>
						<div class="white-container" id="entity-holidays">
							<?php /* ?><a class="green-button" data-related="edit-entity-holidays" href="javascript:;"><?php echo ADD; ?></a> */ ?>
							<strong class="title"><?php echo HOLIDAYS; ?></strong>

							<table id='holiday-list' class="left half">
							<?php
							if ( count($customTimetable) > 0 ) {
								foreach( $customTimetable as $ctt ) {
									echo "<tr>";
										echo "<td>{$ctt['date']}";
										if ( !empty($ctt['openingHour']) && $ctt['openingHour'] != '00:00:00' ) { 
											echo " " . substr( $ctt['openingHour'], 0, -3 ) . " - " . substr( $ctt['closingHour'], 0, -3 ); 
										}  
										echo "</td>";
										echo "<td class='btn-delete-wrapper'><a href='javascript:void(0);' class='delete-holiday' data-id='{$ctt['idCustomTimetable']}'>" . DELETE . " <i class='fa fa-trash-o'></i></a></td>";
									echo "</tr>";
								}
							}
							else {
								echo "<tr><td colspan='2'>" . NO_HOLIDAYS . "</td></tr>";
							}
							?>
							</table>
							<div id="datepicker-container" class="right half">
								<div id="datepicker"></div>
								<div id="date-open-close" class="clearfix">
									<select class="rounded-select" name="holiday_start">
										<option value=""><?php echo CLOSED; ?></option>
										<?php
											foreach ($hours as $hour) {
												if ($hour=='24:00') {
													continue;
												}
										?>
										<option<?php echo $selected ? ' selected="selected"' : ''; ?> value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
										<?php } ?>
									</select><select class="rounded-select" name="holiday_end">
										<option value=""><?php echo CLOSED; ?></option>
										<?php
											foreach ($hours as $hour) {
												if ($hour=='00:00') {
													continue;
												}
										?>
										<option<?php echo $selected ? ' selected="selected"' : ''; ?> value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
									<?php } ?>
									</select>
								</div>
							</div><div class="clear"></div>
							<div id="holiday-translation" class="hidden"><?php echo NO_HOLIDAYS ?></div>
						</div>

						<div data-lat="<?php echo $restaurantAdmin->restaurant->latitude; ?>" data-lng="<?php echo $restaurantAdmin->restaurant->longitude; ?>" id="delivery-zone-map"><a href="javascript:;">&times;</a>
							<div></div></div>
						<script>var langRange = '<?php echo DELIVERY_RANGE; ?>', 
langFee = '<?php echo str_replace('$currency', $restaurantAdmin->restaurant->currency, DELIVERY_FEE2); ?>', 
langMinDelivery = '<?php echo str_replace('$currency', $restaurantAdmin->restaurant->currency, MIN_DELIVERY); ?>', 
langEstimation = '<?php echo ESTIMATED_TIME; ?>', 
langUpdate = '<?php echo ZONE_UPDATE; ?>', 
langAdd = '<?php echo ZONE_ADD; ?>', 
langDelete = '<?php echo DELETE; ?>', 
langConfirmDeliveryZone = '<?php echo CONFIRM_DELIVERY_ZONE; ?>', 

delivery_zones = <?php echo json_encode($delivery_zones_); ?>;</script>
				</div>
	</div>
<?php
	}
