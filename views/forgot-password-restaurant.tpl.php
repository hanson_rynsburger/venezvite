<?php
	if (@$is_included) {
?>
				<div id="hero-image" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo IMG; ?>home-bg/home-bg-0<?php echo mt_rand(1, 8); ?>.jpg" >
					<strong><?php echo FPR_ADMIN_PANEL; ?></strong>
				</div>
				
				<!-- <div id="main-private-menu">
					<ul>
						<li><a class="selected" href="javascript:;"><?php echo FPR_TITLE; ?></a></li>
					</ul>
				</div> -->
				
				<div id="forgot-password-restaurant-body">
					<div class="center main-content">
						<form method="post">
							<input class="rounded-input" maxlength="50" name="username" placeholder="<?php echo FPR_USERNAME; ?>: *" type="text" value="<?php echo !empty($_POST['username']) ? htmlspecialchars($_POST['username']) : ''; ?>" />
							<br />
							<input class="rounded-red-button" type="submit" value="<?php echo FPR_RECOVER; ?>" />
						</form>
					</div>
				</div>
<?php
	}
