module.exports = function(grunt) {
	require( 'jit-grunt' )(grunt);

	grunt.loadNpmTasks('grunt-ftp-deploy');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-newer');

	grunt.initConfig( {
		less: {
			development: {
				options: {
					compress: true,
					yuicompress: true,
					optimization: 2
				},
				files: {
					"css/less-compiled/main.css": "less/main.less",
					"css/less-compiled/restaurants-list.css": "less/restaurants-list.less",
					"css/less-compiled/home.css": "less/home.less",
					"css/less-compiled/restaurant.css": "less/restaurant.less",
					"css/less-compiled/about-us.css": "less/about-us.less"
				}
			}
		},
		watch: {
			styles: {
				files: ['less/**/*.less'],
				tasks: ['less', 'newer:cssmin'],
				options: {
					nospawn: true
				}
			},
			css: {
				files: ['css/**/*.css'],
				tasks: ['newer:cssmin'],
				options: {
					nospawn: true
				}
			},
			scripts: {
				files: ['js/**/*.js'],
			    tasks: ['newer:uglify'],
			    options: {
			      spawn: false,
			    },
			}
		},
		'ftp-deploy': {
			build: {
				auth: {
					host: 'venezvite.com',
					port: 21,
					authKey: 'key1'
				},
				src: '/media/hanson/data/works/php/venezvite.com/',
				dest: '/web/',
				exclusions: ['/media/hanson/data/works/php/venezvite.com/**/.DS_Store', 
							 '/media/hanson/data/works/php/venezvite.com/**/Thumbs.db',
							 // '/media/hanson/data/works/php/venezvite.com/**/.htaccess',
							 '/media/hanson/data/works/php/venezvite.com/less/**',			// ignoring css precompiler
							 '/media/hanson/data/works/php/venezvite.com/node_modules/**',	// ignoring npm modules 
							 '/media/hanson/data/works/php/venezvite.com/i/**',				// ignoring images folder
							 // '/media/hanson/data/works/php/venezvite.com/classes/**',			// ignoring php libraries
							 '/media/hanson/data/works/php/venezvite.com/actions/**',			// ignoring controllers
							 '/media/hanson/data/works/php/venezvite.com/lang/**',			// ignoring controllers
							 'package.json',
							 'gruntfile.js',
							 'venezvitecom.sql',
							 '.gitignore',
							 '.git',
							 '.buildpath',
							 '.settings',
							 '.project',
							 '.ftppass',
							 // '.htaccess'
							 // 'config.php'
							 ]
			}
		},

		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
			    files: [{
			      expand: true,
			      src: ['css/**/*.css', '!css/**/*.min.css', 'js/**/*.css', '!js/**/*.min.css'],
			      dest: '',
			      ext: '.min.css'
			    }]
			  }
		},

		uglify: {
		    my_target: {
		    	files: [{
		    		expand: true,
	    			cwd: 'js',
	    			src: ['**/*.js', '!**/*.min.js'],
	    			dest: 'js',
	    			ext: ".min.js"
		    	}]
		    }
		}
	});

	grunt.registerTask( 'default', ['less', 'watch'] );
	grunt.registerTask( 'build', ['less', 'cssmin', 'uglify'] );
	grunt.registerTask( 'deploy', [ 'ftp-deploy:build' ] );
};