<?php
	ini_set('session.use_trans_sid', '0'); // For security purposes...
	session_start();
	
	date_default_timezone_set('Europe/Zurich');
	require_once 'config.php';
	
	function __autoload($className){
		if (!defined('REL')) {
			// That's weird...
			require_once 'config.php';
		}
		
		if (is_file(REL . VERSION_REL_PATH . 'classes' . DS . $className . '.class.php')) {
			require_once REL . VERSION_REL_PATH . 'classes' . DS . $className . '.class.php';
		} else if(is_file(REL . VERSION_REL_PATH . 'model' . DS . $className . '.class.php')) {
			require_once REL . VERSION_REL_PATH . 'model' . DS . $className . '.class.php';
		} else {
			//die('Incorrect path for class ' . $className . '!');
		}
	}
	$db = new db;
	
	// First let's check if the current visitor's IP is not blocked
	if (in_array($_SERVER['REMOTE_ADDR'], blockedIP::list_())) {
		// It is - die!
		die();
	}
	
	
	
	$current_page = 'home';
	if (!empty($_GET['page'])) {
		$_GET['page'] = explode('/', $_GET['page']);
		
		if (end($_GET['page'])!='restaurant' && is_file(REL . DS . 'actions' . DS . end($_GET['page']) . '.php')) {
			$current_page = end($_GET['page']);
		} else if($restaurant = restaurant::getByURL(end($_GET['page']))) {
			$current_page = 'restaurant';
		} else {
			// Unexisting page
			// Ideally, we should display a custom 404 error page, containing a search box and such
			header('HTTP/1.0 404 Not Found');
			header('Location: ' . ROOT);
			die(/*end($_GET['page']) . ' page doesn\'t exist!'*/);
		}
	}
	
	
	
	$is_included = true;
	
	header('Content-type: text/html; charset=utf-8');
	mb_internal_encoding('UTF-8');
	
	include_once REL . DS . 'actions' . DS . 'public.php';
	if (is_file(REL . DS . 'actions' . DS . $current_page . '.php')) {
		require_once REL . DS . 'actions' . DS . $current_page . '.php';
	} else {
		// How would this happen? Only if there's no restaurant.inc.php actions page?
		header('HTTP/1.0 404 Not Found');
		header('Location: ' . ROOT);
		die(/*$current_page . ' page doesn\'t exist!'*/);
	}
	require_once REL . DS . 'actions' . DS . 'seo.php';
	
	
	
	@ob_end_clean();
	@ob_start('ob_gzhandler'); // GZip compress the script...
	if (is_file(REL . DS . 'views' . DS . $current_page . '.tpl.php')) {
		require_once REL . DS . 'views' . DS . 'public.mp.php';
	} else {
		header('HTTP/1.0 404 Not Found');
		header('Location: ' . ROOT);
		die(/*$current_page . ' page doesn\'t exist!'*/);
	}
