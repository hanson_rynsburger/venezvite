<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ORDER_CONFIRMATION', 'Confirmação do pedido');
	
	define('_AT', 'no');
	define('FOR_A', 'para a');
	
	define('THANKS', 'Obrigado.');
	define('ORDER_SENT', 'O seu pedido foi enviado para o restaurante.');
	define('ORDER_DETAILS', 'O seu pedido (<strong class="green">{order_no}</strong>) para <strong class="green">{order_value}</strong> a {order_date} foi enviado para <strong class="green">{restaurant}</strong>. Um e-mail ser-lhe-á enviado quando o restaurante confirmar o seu pedido.');
	define('YOUR_ORDER_FOR', 'O seu pedido é para:');
	define('CONTACT_RESTAURANT', 'Se tiver alguma questão relacionada com o seu pedido por favor contacte <strong class="green">{restaurant}</strong> no <strong class="green">{restaurant_phone}</strong>');
	
	define('WHAT_NEXT_TITLE', 'O que vai acontecer agora?');
	define('WHAT_NEXT_BODY1', '{restaurant} vai confirmar o seu pedido através do nosso sistema, o qual:');
	define('WHAT_NEXT_BODY2', 'vai gerar um email de confirmação que lhe será enviado para a sua caixa de correio, e:');
	define('WHAT_NEXT_BODY3', '{restaurant} vai preparar e o entregador vai-lhe entregar a sua refeição.');
