﻿<?php
    define('CR_SUCCESS', 'Obrigado por confirmar o seu endereço de e-mail!' . "\n" . 
        'De seguida, por favor espere até que alguém da nossa equipa aprove a conta do seu restaurante. Assim que isto acontecer, será notificado via e-mail!');
