<?php
	define('PAYMENT_TYPE', 'Tipo de pagamento');
	define('CC', 'Cartão de crédito');
	define('DELIVERY_CASH', 'Pagamento com dinheiro na entrega');
	define('TAKEAWAY_CASH', 'Pagamento com dinheiro no Takeaway');
	define('NO_LATER_THAN', 'No máximo até');
	define('MINUTES', 'minutos');
	define('NOTES', 'Notas');
	define('CUSTOMER', 'Cliente');
	define('DELIVERY_DATETIME', 'Data/hora de entrega');
	define('DELIVERY_TO', 'Entrega para');
	define('FLOOR', 'Andar');
	define('DELIVERY_INSTRUCTIONS', 'Instruções de entrega');
	define('TAKEAWAY_DATETIME', 'Data/hora do takeaway');
	define('TOTAL_PRODUCTS', 'Total dos productos');
	define('DISCOUNT', 'Desconto');
	define('TOTAL', 'Total');
