﻿<?php
	define('DO_EMAIL_TITLE', 'O seu pedido no Venezvite foi recusado');
	define('DO_EMAIL_BODY', 'Olá {$userName},

O seu pedido de comida efectuado no Venezvite acabou de ser recusado pelo {$restaurantName}.
{$reason}
Tanto pode tentar entrar em contacto com o restaurante para receber mais detalhes, ou tente efectuar outro pedido num restaurante diferente.

O seu valor pago (se existe algum) será reembolsado para a sua conta bancária o mais rapidamente possível.');
	
	define('DO_EMAIL_REASON', 'A razão para a recusa do restaurante é: ');
