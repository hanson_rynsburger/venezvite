﻿<?php
	define('AO_EMAIL_TITLE', 'O seu pedido no Venezvite foi aceite');
	define('AO_EMAIL_BODY', 'Olá {$userName},

O seu pedido efectuado no Venezvite foi aceite pelo {$restaurantName}.

Obrigado por encomendar com o Venezvite.');
