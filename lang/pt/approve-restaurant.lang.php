﻿<?php
    define('AR_EMAIL_UNCONFIRMED', 'A conta deste restaurante não pode ser aprovada uma vez que ainda não foi confirmado o seu e-mail!');
    define('AR_SUCCESS', 'O restaurante foi aprovado com sucesso!' . "\n" . 
        'O seu representante acabou de receber um e-mail de confirmação que contém as instruções que devem ser utilizadas para gerir os detalhes do restaurante.');
    define('AR_FAILURE', 'Infelizmente não foi possível enviar o e-mail de confirmação da aprovação do restaurante.' . "\n" . 
        'Por favor tente novamente ou contacte o administrador da página para reportar este problema!' . "\n\n" . 
        'A conta do restaurante não foi activada.');
    
    define('AR_EMAIL_CONFIRMATION_SUBJECT', 'A sua conta no Vezenvite foi aprovada');
    define('AR_EMAIL_CONFIRMATION_BODY', 'Olá {$restaurantName},

A sua conta do seu restaurante foi aprovada. Parabéns!

Tudo o que tem que fazer agora é aceder ao seu login no Venezvite <a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html</a>, usando os dados que submeteu no processo de registo do seu restaurante e finalize o perfil do mesmo definindo o seu menu.

Bem-vindo ao Venezvite!');
