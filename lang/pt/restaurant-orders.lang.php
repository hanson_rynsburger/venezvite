<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('TIME_PERIOD', 'Período de tempo');
	define('TOTAL_ORDERS', 'Total de encomendas');
	define('TOTAL_SALES', 'Total das vendas');
	define('TOTAL_EARNINGS', 'Total Earnings');
	define('ALL_TIME', 'O todo tempo');
	define('THIS_WEEK', 'Esta semana');
	define('THIS_MONTH', 'Este mês');
	define('PAST_MONTH', 'Mês passado');
	define('THIS_YEAR', 'Este ano');
	
	define('NEW_ORDERS', 'Novos pedidos');
	define('PAST_ORDERS', 'Pedidos anteriores');
	
	define('RECEIVED_DATE', 'Recebido<br />' . 
		'<span>Data / Hora</span>');
	define('RECEIVED_FROM', 'De');
	define('ORDER_TYPE', 'Tipo de pedido');
	define('ORDER_AMOUNT', 'Quantia');
	define('ORDER_REQUIRED', 'Quando');
	define('ORDER_STATUS', 'Estado');
	
	define('PENDING', 'Pendente');
	define('ACCEPT_ORDER', 'Aceitar');
	define('DECLINE_ORDER', 'Recusar');
	define('CONFIRM_ACCEPT', 'Tem a certeza que quer ACEITAR este pedido?');
	define('CONFIRM_DECLINE', 'Tem a certeza que quer RECUSAR este pedido?');
	define('ACCEPTED', 'Aceite');
	define('DECLINED', 'Recusado');
	
	define('NO_PENDING_ORDERS', 'Não tem pedidos pendentes.');
	define('VIEW_PAST_ORDERS', 'Veja pedidos anteriores');
	define('NO_PAST_ORDERS', 'Não tem pedidos anteriores disponíveis.');
