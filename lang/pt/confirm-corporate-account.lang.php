﻿<?php
	define('CCA_SUCCESS', 'Thank you for confirming your email address!' . "\n" . 
		'Next please wait until one of our team\'s members will approve your corporate account. You will be notified via email as soon as this happens!');
