<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('EDIT_DETAILS', 'Edite detalhes financeiros');
	define('BUSINESS_NAME', 'Nome da empresa / conta');
	define('IBAN', 'Número do IBAN');
	define('BANK', 'Nome do banco');
	define('SWIFT', 'Código SWIFT');
	
	define('COMMISSION_AGREEMENT', 'Venezvite Commission Agreement');
	define('COMMISSION_PERCENTAGE', 'Venezvite Comissao: {$percentage}% + As despesas de entrega');
	define('COMMISSION_PAYMENT', 'Pagamento: O dia 15 de cada mes, enquanto há um mínimo de 500.');
	define('THANK_YOU', 'Obrigado por ser um parceiro Venezvite.');
	define('QUESTIONS_EMAIL', 'Para qualquer dúvida por favor enviar e-mail: hello@venezvite.com');
