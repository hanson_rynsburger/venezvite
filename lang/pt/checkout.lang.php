<?php
	// SEO settings
	define('SEO_TITLE', 'Entre ou crie uma conta - Entrega de comida em Lisboa | Venezvite');
	define('SEO_KEYWORDS', 'Crie uma conta, entrega de comida, entrega de vinho');
	define('SEO_DESCRIPTION', 'Entre ou crie uma conta para encomendar comida e vinho para entrega em sua casa');
	
	
	define('ORDER_ERRORS', 'Erro com o seu pedido:');
	define('USED_EMAIL', 'Este endereço de e-mail já existe no sistema. Entre, clique em esqueci-me da password.');
	define('PHONE_MANDATORY', 'Antes de continuar a usar o Venezvite, por favor actualize o seu número de telefone!');
	define('CANT_FIND_ADDRESS', 'Não conseguimos determinar a localização da morada introduzida. Por favor tente novamente, ou contacte-nos para reportar este problema!');
	define('NO_BUILDING_NO', 'We couldn\'t find a building number in the address you\'ve just submitted.');
	define('OUTSIDE_ADDRESS', 'A morada introduzida encontra-se fora da área de entrega do restaurante.');
	define('DIFFERENT_CHARGES_MINIMUM', 'A morada introduzida alterou o valor da taxa de entrega e requer um pedido mínimo. Por favor adicione mais items ao seu pedido de modo a alcançar o novo mínimo.');
	define('DIFFERENT_MINIMUM', 'A morada introduzida alterou o pedido mínimo. Por favor adicione mais items ao seu pedido de modo a alcançar o novo mínimo.');
	define('DIFFERENT_CHARGES', 'Por favor tenha em atenção que a nova morada introduzida alterou o valor da taxa de entrega. Por favor reveja e confirme o seu pedido.<br />' . 
		'Sugestão: use sempre a sua morada exacta para procura de restaurantes.');
	define('WRONG_CC_TYPE', 'O tipo de cartão de crédito é desconhecido ou não é admitido. Por favor tente outro diferente ou contacte-nos para reportar este problema!');
	define('FILL_FIELDS', 'Por favor preencha todos os campos necessários.');
	
	define('ALMOST_DONE', '<span>Quase pronto!</span> Introduza a sua informação');
	define('CREATE_ACCOUNT', 'Crie uma conta');
	define('ACCOUNT_INFO', 'Sua informação de conta');
	define('EDIT_MY_INFO', 'Editar a minha informação'); 
	define('ENTER_EMAIL', 'Introduza o seu endereço de e-mail');
	define('CREATE_PASS', 'Crie uma Password');
	define('NEWSLETTER_ACCEPT', 'Sim, eu gostaria de receber descontos e de ser notificado por e-mail de novos restaurantes.');
	
	define('DELIVERY_DETAILS', 'Detalhes da entrega');
	define('FOR_DELIVERY', 'Para entrega');
	define('CHANGE_TAKEAWAY', 'Alterar para Take-away');
	define('FOR_TAKEAWAY', 'Para Take-away');
	define('CHANGE_DELIVERY', 'Alterar para entrega');
	
	define('ENTER_NEW_ADDRESS', 'Introduza nova morada');
	define('EDIT_ADDRESS', 'Edite esta morada');
	define('SAVE_ADDRESS', 'Guarde a sua informação.');
	define('TAKEAWAY_DESC', 'Está a efectuar um pedido de Take-away, a sua comida estará pronta para ser levantada no restaurante.');
	
	define('PAYMENT_OPTIONS', 'Opções de pagamento');
	define('INVOICE', 'Invoice');
	define('CASH', 'Numerário');
	define('CREDIT', 'Crédito');
	define('CARD_NAME', 'Nome no cartão crédito');
	define('CCN', 'Número do cartão de crédito');
	define('EXPIRATION_DATE', 'Data de expiração');
	define('CURRENCY', 'Moeda (' . OPTIONAL . ')');
	define('USE_NEW_CARD', 'Use um cartão novo');
	
	define('TOO_MUCH_CASH', 'Apenas pode efectuar pedidos a pagar com dinheiro por valores inferiores a ' . MAX_CASH_VALUE . ' {$currency}.');
	define('UNPAID_CASH', 'Os nossos registos mostram que tem um pedido a pagar com dinheiro que não foi pago. Esta opção vai continuar desactivada até que o pagamento seja recebido.<br />' . 
		'Por favor continue o seu pedido usando um cartão de crédito.');
	define('UNAPPROVED_CASH', 'O seu anterior pedido a pagar com dinheiro ainda não foi aprovado. Por favor aguarde até que este seja aprovado ou continue o seu pedido usando um cartão de crédito.');
	define('NO_CASH', 'Este restaurante não aceita pagamento com dinheiro.');
	define('CASH_PAYMENT_DESC', 'Pague com dinheiro na entrega.<br />
(Só é aceite pagamentos com dinheiro. O entregador deste restaurante não aceita pagamentos com cartão de crédito na entrega.)');
	define('INVOICING_DESC', 'If your Corporate Account is approved for ordering food on Venezvite.com without paying online in advance (but receiving an invoice to be paid at a later time), you can use this option to place your order.' . "\n\n" . 
        'Be warned that, in case your account is NOT approved for such an option, your order will be ignored!' . "\n\n" . 
        'For details about how your account could become approved for invoice ordering, please <a class="popup" href="' . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/contact.html">contact us</a>!');
	
	define('AGREE_TERMS', 'Eu aceito <a class="popup" href="tou-customers.html">os termos e condições de uso</a>.');
	define('SAVE_INFO', 'Guarde a sua informação.');
	define('PLACE_ORDER_X', 'Efectue o meu pedido ({x})');
	
	
	
	define('P_ORDER_EMAIL_TITLE', 'Novo pedido no Venezvite');
	define('P_ORDER_EMAIL_ORDER_TYPE', 'Tipo de pedido');
	define('P_ORDER_EMAIL_DELIVERY', 'Entrega');
	define('P_ORDER_EMAIL_TAKEAWAY', 'Take away');
	define('P_ORDER_EMAIL_ORDER_NO', 'Número do pedido');
	define('P_ORDER_EMAIL_DELIVERY_DATE_HOUR', 'Data e hora de entrega');
	define('P_ORDER_EMAIL_RESTAURANT', 'Restaurante');
	define('P_ORDER_EMAIL_PHONE', 'Telefone');
	define('P_ORDER_EMAIL_ORDER', 'Pedido');
	define('P_ORDER_EMAIL_PAYMENT_DONE', 'Pagamento: Já pago');
	define('P_ORDER_EMAIL_PAYMENT_TBD', 'Pagamento: A pagar');
	define('P_ORDER_EMAIL_TOTAL', 'Total dos produtos');
	define('P_ORDER_EMAIL_ADDRESS', 'Morada');
	define('P_ORDER_EMAIL_LAST_NAME', 'Sobrenome');
	define('P_ORDER_EMAIL_FIRST_NAME', 'Nome');
	define('P_ORDER_EMAIL_COMPANY', 'Empresa');
	define('P_ORDER_EMAIL_STREET', 'Rua');
	define('P_ORDER_EMAIL_ZIP', 'Código Postal');
	define('P_ORDER_EMAIL_CITY', 'Cidade');
	define('P_ORDER_EMAIL_FLOOR_CODE', 'Andar e apartamento');
	define('P_ORDER_EMAIL_NOTES', 'Notas');
	define('P_ORDER_EMAIL_CASH_ON_DELIVERY', 'Pagamento com dinheiro na entrega');
	define('P_ORDER_EMAIL_ORDER_READY', 'O pedido necessita de estar pronto para <span style="color:#c00">{$datetime}</span>');
	define('P_ORDER_EMAIL_ACTIVATION_LINK', 'Acesso ao pedido aceite:');
	define('P_ORDER_EMAIL_DELIVERY_DELAY', 'Hora estimada de entrega: {$time} <span style="color:#c00">+/- 15 minutos</span>');
	define('P_ORDER_EMAIL_TAKEAWAY_TIME', 'Hora para levantar: {$time}');

	define('VIEW_IMAGES', 'View images');
