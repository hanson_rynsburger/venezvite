<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ADD_CATEGORY', 'Insert a Menu Category');
	define('EDIT_ITEM', 'Update Menu Category');
	define('ADD_CATEGORY_BUTTON', 'Add Menu Category');
	define('EDIT_ITEM_BUTTON', 'Update');
	
	define('DUPLICATE_ALERT', 'It seems there is already a menu category matching the one you\'re trying to insert.');
	define('DISABLED_ALERT', 'The selected category has the following menu items attached to it:\n\n' . 
		'{used_menu_items}\n\n' . 
		'In order to remove it, please re-assign these menu items to a different category, or delete them.');
	define('REMOVE_CONFIRMATION', 'Are you sure you want to remove this category?');
	define('NO_MENU_CATEGORIES', 'You have no menu categories defined.');
