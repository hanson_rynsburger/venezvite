<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
    define('LR_LOGIN_UNAPPROVED', 'A sua conta de restaurante não foi ainda aprovada. Pode ainda não ter confirmado o seu endereço de email, ou a nossa equipa está ainda a analizar os dados submetidos por si.\n' . 
        'Por favor espere mais um pouco, ou se já subescreveu à alguns dias atrás e ainda não recebeu um e-mail de confirmação da nossa parte, por favor contacte-nos para confirmar o estado do seu pedido.');
    define('LR_LOGIN_ERROR', 'Oops, nós não reconhecemos a informação do seu acesso de conta. Por favor tente outra vez ou se tiver esquecido da sua password clique no link acima.');
	
	define('LOGIN_ACCOUNT', 'Aceda à sua conta');
	define('USERNAME', 'Nome do usuário (o seu endereço de e-mail)');
	define('FORGOT', 'Esqueci-me');
	
	define('CONFIRM_ORDERS', 'Confirme o seu pedido');
	define('CONFIRM_ORDERS_DESC', 'Confirme pedidos recebidos de computador/tablet/telemóvel');
	define('MANAGE_RESTAURANT', 'Gira o seu Restaurante');
	define('MANAGE_RESTAURANT_DESC', 'Actualize o seu menu e as definições do seu restaurante');
	define('SALES', 'Vendas');
	define('SALES_DESC', 'Análise completa das vendas do seu restaurante');
