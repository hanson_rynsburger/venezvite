<?php
	// SEO settings
	//define('SEO_TITLE', '');
	//define('SEO_KEYWORDS', '');
	//define('SEO_DESCRIPTION', '');
	
	
	define('WHEN_WHERE_DESC', 'Para confirmar que este restaurante entrega na sua zona, por favor introduza a sua morada exacta e a hora de entrega desejada.');
	
	define('RETURN2LIST', 'Volte à lista de restaurantes');
	define('ADD_TO_FAVES', 'Adicione aos favoritos');
	define('VERY_POPULAR_RESTAURANT', 'Um restaurante muito popular');
	define('POPULAR_RESTAURANT', 'Um restaurante popular');
	define('OPEN', 'Aberto');
	define('CLOSED', 'Fechado');
	define('CLOSES_IN', 'Fecha em {x} minutos');
	
	define('MORE_INFO', 'Mais informações');
	
	define('VISIT_WEB', 'Visite a página');
	define('SERVING_IN', 'Atendendo a clientes em');
	define('HOURS_DELIVERY_ZONES', 'Horas &amp; Zonas de entrega');
	define('TAKEAWAY_HOURS', 'Horas de Take-Away');
	define('DELIVERY_HOURS', 'Horas de entrega');
	define('NO_DELIVERY', 'Nenhuma entrega');
	define('DELIVERY_ZONES', 'Zonas de entrega');
	define('ABOUT', 'Sobre');
	
	define('SEARCH_HINT', 'Procure por items do menu');
	define('FILTER', 'Filtro');
	define('FILTER_PHOTOS', 'Fotos');
	
	define('ADD_ITEM', 'Adicione Item');
	define('SHOW_IMAGE', 'Maximizar a imagem');
	define('HIDE_IMAGE', 'Minimizar a imagem');
	define('VIEW_DETAILS', 'Veja os detalhes');
	define('ADD2BAG', 'Adicione à cesta');
	define('SPECIAL_INSTRUCTIONS', 'Instruções especiais &amp; Notas');
	define('SPECIAL_INSTRUCTIONS_EG', '(ex: molho extra)');
	define('EDIT_ITEM', 'Edite Item');
	define('UPDATE_BAG', 'Actualize a sua cesta');
 	define('OPTIONS_EXACTLY', 'Selecione exactamente {x} opção(s)');
	define('OPTIONS_AT_LEAST', 'Selecione pelo menos {x} opção(s)');
	define('OPTIONS_UP_TO', 'Select up to {x} option(s)');
	
	define('ITEM_ADDED', 'Item adicionado');
	define('GO2CHECKOUT', 'Proceda para o checkout');
	
	define('NO_MENUS_FOR_SEARCH', 'Nenhum item de menu que satisfazem os seus critérios de pesquisa.');
	
	define('REVIEWS_TITLE', 'Comentários');
	define('WRITE_REVIEW', 'Escreva um comentário');
	
	define('DELIVERY_SCHEDULE', 'Hora de entrega');
