<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('INVALID_FILE', 'Um ou mais ficheiros não foram carregados. Este tipo de ficheiro não é suportado.');
	define('CANT_SAVE_RESTAURANT', 'Infelizmente não nos foi possível guardar os detalhes do seu restaurante. Por favor tente novamente ou contacte-nos para reportar este problema.');
	define('REGISTRATION_CONFIRMATION', 'Em alguns segundos vai receber um e-mail. Para completar o seu registo por favor clique no link para confirmar o seu endereço de e-mail.');
	define('SOME_ERRORS', 'Ocorreram alguns erros enquanto tentou criar a sua conta:');
	define('REGISTRATION_SUCCESS', 'O seu restaurante foi registado com sucesso!');
	
	define('CONTACT_INFO', 'Informação de contacto <span>* (' . REQUIRED . ')</span>');
	define('MOBILE', 'Telemóvel');
	
	define('RESTAURANT_INFO', 'Informação do restaurante');
	define('SELECT_COUNTRY', 'Selecione o seu país');
	define('RESTAURANT_EMAIL', 'Endereço de e-mail');
	define('RESTAURANT_NAME', 'Nome do restaurante');
	define('RESTAURANT_ADDRESS', 'Rua do restaurante');
	define('RESTAURANT_CITY', 'Cidade / Aldeia / Município * (Tem de selecionar desta lista)');
	define('RESTAURANT_PHONE', 'Número de telefone do restaurante');
	define('ENTER_VALID_PHONE', 'Enter a valid phone number');
	define('RESTAURANT_URL', 'Página Web do restaurante');
	define('RESTAURANT_DESC', 'Informação do restaurante (Descreva o seu restaurante)');
	
	define('PRIMARY_CUISINE_TYPE', 'Selecione a sua preferência de tipos de cozinha <span> Selecione tantos quanto</span>');
	define('CHOOSE_CUISINE', 'Escolha a cozinha');
	define('CHOOSE_SERVICES', 'Por favor confirme os serviços que fornece');
	define('CATERING', 'Serviço de Catering');
	define('CUSTOM_DELIVERY', 'Não fazemos entrega e gostariamos de usar o serviço de parceria de entrega.');
	define('SELECT_TIME', 'Selecione a hora');
	
	define('HOURS_OPERATION', 'Horas de funcionamento');
	define('SAME_EVERY_DAY', 'Aplicar as mesmas horas de funcionamento para todos os dias');
	define('DELIVERY_ZONES', 'Zonas de entrega');
	define('ZONE', 'Zona 1');
	define('ZONE_RANGE', 'Alcance em km da zona 1');
	define('MIN_DELIVERY', 'Pedido mínimo para entrega na zona 1');
	define('DELIVERY_FEE_ZONE', 'Taxa de entrega para a zona 1');
	define('SELECT_TIME_INTERVAL', 'Tempo médio de entrega (somente o transporte)');
	define('ADD_ZONE', 'Adicione outra zona');
	define('PHOTOS', 'Fotos');
	define('UPLOAD_PHOTOS', 'Carregue fotos (.jpg ou .png)');
	define('UPLOAD_MENU', 'Carregue menu (.docx, .pdf ou .jpg)');
	
	define('CREATE_ACCOUNT_TITLE', 'Crie a sua conta');
	define('USERNAME', 'Nome de usuário: Introduza o seu endereço de e-mail');
	define('CONFIRM_USERNAME', 'Confirme o seu endereço de e-mail');
	define('CONFIRM_PASSWORD', 'Confirme a sua password');
	define('AGREE_TERMS', 'Eu aceito os <a class="popup" href="{$link}">termos e condições</a>.');
	define('CREATE_ACCOUNT', 'Crie a sua conta');
	
	define('INVALID_ADDRESS', 'Oops! A {address} morada não parece ser válida!');
	
	define('RR_EMAIL_CONFIRMATION_SUBJECT', 'Confirma a sua conta Venezvite');
	define('RR_EMAIL_CONFIRMATION_BODY', 'Olá {$restaurantName},

De modo a que a sua conta de restaurante seja autorizada por um membro da nossa equipa, por favor confirma o seu endereço de e-mail, clicando no link abaixo:

<a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'pt') . '/confirm-restaurant.html?check={$uniqueHash}">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'pt') . '/confirm-restaurant.html?check={$uniqueHash}</a>

Obrigado por escolher Venezvite!');
