<?php
	// SEO settings
	define('DEFAULT_SEO_TITLE', 'Venezvite | Entrega de comida de restaurantes locais em Lisboa');
	define('DEFAULT_SEO_KEYWORDS', 'entrega de comida em Lisboa, entrega de comida, entrega de vinho em Lisboa, entrega de vinho, entrega de comida em Lisboa');
	define('DEFAULT_SEO_DESCRIPTION', 'Venezvite é a melhor maneira de encomendar comida para entrega ou takeaway em Lisboa. Venezvite é uma plataforma de marketing que conecta bons restaurantes com pessoas com fome! Está com fome? Comece o seu pedido ou junte-se agora à nossa rede de restaurantes!');
	
	define('LOGO_SLOGAN', 'Entrega para a sua casa ou escritório');
	
	define('FAVORITED_RESTAURANT', 'Favorited Restaurant');
	define('HELLO', 'Olá');
	define('CITIES', 'Cidades');
	define('HELP', 'Help');
	define('LOGIN', 'Aceda à sua conta');
	define('ORDERS_HISTORY', 'Histórico de pedidos');
	define('MY_ACCOUNT', 'A minha conta');
	define('PROFILE', 'Perfil');
	define('ADDRESSES', 'Moradas');
	define('PAYMENTS', 'Pagamentos');
	define('RESTAURANT_GALLERY', 'Gallery');
	define('BUSINESS_INFO', 'Informaçăo empresarial');
	define('FINANCIAL_INFO', 'Detalhes financeiros');
	define('MENU', 'Menu');
	define('SPICY', 'Picante');
	define('VEGETARIAN', 'Vegetariano');
	define('NO_GLUTEN', 'Livre de glúten');
	define('NO_LACTOSE', 'Sem lactose');
	define('RECOMMENDED', 'Recomendado');
	define('MENU_CATEGORIES', 'Menu Categories');
	define('MENU_OPTION_GROUPS', 'Menu Option Groups');
	define('SEE_PROFILE', 'Veja o meu perfil');
	define('LOGOUT', 'Sair da conta');
	
	define('FB_LOGIN', '<strong>Aceda a sua conta</strong> através do <strong>facebook</strong>');
	define('OR_', 'ou');
	define('USERNAME_CORPORATE', 'Nome de usuário para membros corporativos');
	define('PASSWORD', 'Password');
	define('KEEP_LOGGED', 'Mantenha-me ligado');
	define('OOPS_FORGOT', 'Oops, esqueci-me <a href="{$link}">da minha password</a>');
	define('LOGIN_INCENTIVE_TITLE', 'Primeiro pedido com o Venezvite?');
	define('LOGIN_INCENTIVE_DESC', '<strong>Năo necessita de criar uma conta ou aceder à sua conta.</strong> Introduza a sua morada actual, faça o seu primeiro pedido e nós tratamos do resto!');
	define('INVALID_USER_PASS', 'Nome de usuário / senha inválido');
	define('UNABLE_TO_REGISTER', 'Infelizmente năo nos é possivel registar a sua conta!\n' . 
		'Tente registar-se outra vez ou contacte-nos para nos reportar este problema!');
	define('LAST_NAME', 'Sobrenome');
	define('FIRST_NAME', 'Nome');

	define('PHONE_PREFIX', 'Prefix');
	define('PHONE_NO', 'Número de telefone');
	
	define('RESTAURANTS_LIST_CUSTOM_PATH', 'entrega-de-comida');
	define('BACK', 'Voltar');
	
	define('BREADCRUMBS_ENTER_ADDRESS', 'Introduza a sua morada');
	define('BREADCRUMBS_SELECT_RESTAURANT', 'Selecione o seu restaurante');
	define('BREADCRUMBS_CHOOSE_FOOD', 'Escolha a sua comida');
	
	define('REQUIRED', 'Necessário');
	define('OPTIONAL', 'opcional');
	define('WHERE_WHEN', 'Onde e quando?');
	define('DELIVERY', 'Entrega');
	define('TAKEAWAY', 'Take Away');
	define('SELECT_SAVED_LOCATION', 'Escolha uma localizaçăo guardada');
	define('SUBMIT_NEW_LOCATION', '... ou introduza uma nova');
	define('SCHEDULED_FOR', 'Agendar para:');
	define('SCHEDULED_TIME', 'Horas:');
	define('FIND', 'Procurar');
	define('SUBMIT', 'Enviar');
	
	define('TODAY', 'Hoje');
	define('TOMORROW', 'Amanhă');
	define('YESTERDAY', 'Ontem');
	define('ASAP', 'O mais cedo possível');
	
	define('NEW_', 'Novo');
	define('VITE', 'Rapidamente');
	define('REVIEWS', 'comentários');
	define('DELIVERY_FEE', 'Taxa de entrega');
	define('MINIMUM_ORDER', 'Pedido minimo');
	define('APPLY', 'Solicitar');
	define('CANCEL', 'Cancelar');
	define('PRODUCT_TOTAL', 'Total de productos:');
	define('CLOSED_NOW', 'Fechado por agora');
	
	define('NEXT_DELIVERY_TIME', 'A próxima hora de entrega disponível é {datetime}'); // L'heure choisi a changé.
	define('NEXT_TAKEAWAY_TIME', 'A próxima hora disponível para pedido de take away é {datetime}'); // L'heure choisi a changé.
	
	define('ITEMS', 'itens');
	define('CHECKOUT', 'Checkout');
	define('MY_ORDER', 'O meu pedido');
	define('EMPTY_BAG', 'A sua cesta está vazia!');
	define('EDIT', 'Editar');
	define('ADD', 'Add');
	define('DELETE', 'Apagar');
	define('DELIVERY_MIN', 'Entrega mínima');
	define('NO_TAKEAWAY_MIN', 'Năo existe mínimo para pedidos de take-away!');
	define('HAVE_PROMO', 'Tem algum código de promoçăo?');
	define('ENTER_PROMO', 'Introduza código de promoçăo');
	//define('DELIVERY_ADDRESS_LABEL', 'Por favor introduza a sua morada e confirme se este restaurante entrega nessa zona.');
	define('ENTER_ADDRESS', 'Introduza a sua morada actual');
	define('GRAND_TOTAL', 'Total:');
	
	define('STREET_ADDRESS', 'Rua');
	define('BUILDING_NO', 'Número da porta');
	define('CITY', 'Cidade');
	define('POSTAL_CODE', 'Código Postal');
	define('FLOOR_SUITE', 'Andar / Apartamento');
	define('ACCESS_CODE', 'Código de acesso');
	define('SPECIAL_INSTR', 'Instruçőes especiais');
	define('SPECIAL_INSTR_HINT', 'Instruçőes especiais de entrega');
	
	define('PARTNER_LOGIN', 'Parceiro aceda à sua conta');
	
	define('FOOTER_WHO_WE_ARE', 'Quem nós somos');
	define('FOOTER_ABOUT_US', 'Sobre nós');
	define('FOOTER_NEWS_PRESS', 'Notícias &amp; Imprensa');
	define('FOOTER_BLOG', 'Blog');
	define('FOOTER_YOUR_BUSINESS', 'Para a sua empresa');
	define('FOOTER_BECOME_PARTNER', 'Torne-se um restaurante parceiro');
	
	define('FOOTER_CREATE_ACCOUNT', 'Cria uma conta corporativa');
	define('FOOTER_CATERING_ORDER', 'Efectue um pedido de catering');
	define('FOOTER_CONTACT_HELP', 'Contacte &amp; Ajuda');
	define('FOOTER_HELP_FAQ', 'Ajuda &amp; Perguntas frequentes');
	define('FOOTER_CONTACT', 'Contacte-nos');
	define('FOOTER_SUGGEST', 'Sugira um restaurante');
	define('FOOTER_MORE', 'Mais+');
	define('FOOTER_SPECIALS', 'Ofertas especiais');
	
	define('SUCCESSFUL_RECOMMANDATION', 'Obrigado por nos sugerir este restaurante.');
	
	define('FOOTER_COPYRIGHT', 'Entrega para sua casa ou escritório. Todos os direitos reservados.');
	define('FOOTER_USE_TERMS', 'Termos de utilizaçăo');
	define('FOOTER_PRIVACY', 'Política de privacidade');
	define('FOOTER_SITE_BY', 'Site criado por');
	
	define('MORE_MONEY', 'O pedido mínimo năo foi atingido' . 
		(@$current_page=='checkout' ? '<br />(OU o restaurante atual não permite que os pagamentos em dinheiro)' : '') . '.');
	define('CODE_ALREADY_USED', 'Parece que já usou o código introduzido!' . "\n" . 
		'Năo pode utilizar um código de promoçăo mais que uma vez.');
	define('DA_PROMO_CODE', 'Código de promoçăo');
	define('PROMO_VALID_FROM', 'Este código de promoçăo é válido {$date}.');
	define('PROMO_VALID_UNTIL', 'Este código de promoçăo é inválido. Já expirou {$date}.');
	define('INVALID_PROMO_CODE', 'Código de promoçăo inválido!');
	define('LOGIN_FOR_PROMO', 'Por favor aceda à sua conta ou registe-se antes de utilizar o código de promoçăo.');
	define('PROMO_USED', 'Este código de promoçăo já foi utilizado.');
	define('PROMO_MIN_VALUE', 'Este código de promoçăo é apenas válido em pedidos de  {$value} EUR ou mais.');
	define('INVALID_OR_REGISTERED_PROMO', 'O seu código é inválido ou é dedicado a um usuário específico. Por favor aceda à sua conta e tente novamente!');
	define('EMAIL_IN_USE', 'Este e-mail já está em uso');
	
	define('R_EMAIL_CONFIRMATION_SUBJECT', 'A sua conta Venezvite');
	define('R_EMAIL_CONFIRMATION_BODY', 'Olá {$firstName},

Isto é uma confirmaçăo de que a sua conta no Venezvite está agora activa. Parabéns! Os detalhes da sua conta săo:

 - username: {$username}
 - password: {$password}

Por favor guarde esta informaçăo num sítio seguro, para que consiga aceder facilmente assim que precise. Pode aceder agora à sua conta na página Web e encomendar comida ao seu gosto!

Obrigado por usar o Venezvite!');
	
	define('HELLO_RESTAURANT', 'Olá, Restaurante parceiro &amp; Dono');
	define('HUNGRY_FOR_BUSINESS', 'Tem fome para mais negócios? É fácil. Adira venezvite.com');
	
	define('HOW_IT_WORKS', 'Como funciona');
	define('HOW_WE_HELP', 'Como podemos ajudar');
	define('JOIN', 'Adira');
	
	define('WHO_ARE_WE', 'Quem somos nós?');
	define('FOR_YOUR_COMPANY', 'Para a sua empresa');
	define('CONTACT_HELP', 'Contacte &amp; Ajuda');
	define('SUBMENU_PLUS', 'Mais +');

	define('ORDER_FROM', 'Order from');
	define('CONTINUE_TO_CHECKOUT', 'Continue to checkout');
	
	define('NO_FAVORITED_RESTAURANT', 'No Favorited Restaurants');
