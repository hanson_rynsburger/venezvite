<?php
	// SEO settings
	define('SEO_TITLE', HOW_IT_WORKS);
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('FIND_NEW_CUSTOMERS', 'Find new customers, generate repeat business, increase revenues and grow your bottom line.');
	define('JOIN_PRESENTATION', 'By joining the Venezvite restaurant network, you\'ll give thousands of hungry people the ability to order delivery or pickup from your restaurant, online or on a mobile phone.');
	define('JOIN_BUTTON', 'Join Our Network');
	
	define('INSTANT_PRESENCE_TITLE', 'Instant Online Presence');
	define('INSTANT_PRESENCE_DESC', 'We market your restaurant to thousands of hungry people everyday. Get your restaurant and daily specials sent directly to hungry people looking for your exact cuisine/dish.');
	define('PREPARE_FOOD_TITLE', 'Prepare Your Food');
	define('PREPARE_FOOD_DESC', 'You electronically receive an order, and then prepare the food to be delivered or picked up.');
	define('GET_PAID_TITLE', 'Get Paid &amp; Increase Sales!');
	define('GET_PAID_DESC', 'You get a wire transfer from Venezvite for all the orders you received. It\'s that simple.');
