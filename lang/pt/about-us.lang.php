<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ABOUT_US_TITLE', 'Sobre nós');
	define('ABOUT_US_P1', 'Venezvite é a melhor maneira de encomendar comida e vinho online para entrega. Não necessita de esperar numa fila, de passar por mal entendidos e ter que se repetir. O nosso serviço é seguro, fácil de usar e rápido.');
	define('ABOUT_US_P2', 'Venezvite é uma plataforma de marketing que conecta os melhores restaurantes e fornecedores com os clientes que estão com fome, com sede, ou com necessidade de que outros produtos lhes sejam entregues rapidamente! Os restaurantes e fornecedores em parceria connosco oferecem entrega em menos de 1 hora.');
	
	define('MEET_TEAM_TITLE', 'Conheça a equipa');
	define('JOIN_TEAM', 'Junte-se à equipa!');
	
	define('MEET_DELIVERY_TEAM_TITLE', 'Conheça a equipa de entrega');
	define('VP_MEMBER', 'Membro da equipa ' . VITE);
