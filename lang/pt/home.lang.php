<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('HUNGRY_TITLE', 'Com fome? <span>Comece seu Pedido</span>');
	define('ALREADY_MEMBER', 'Já é um membro? Aceda a sua conta');
	define('NOW_ACCEPTING', 'Aceitamos');
	
	define('EASY_ORDERING_TITLE', 'Pedido fácil on-line');
	define('EASY_ORDERING_BODY', 'Não precisa de pegar no telefone. Pedido rápido e preciso!');
	define('MORE_FOOD_TITLE', 'Mais que comida');
	define('MORE_FOOD_BODY', 'Pode também obter cerveja, vinho, álcool, artigos de mercearia e outros.');
	define('DISCOUNTS_TITLE', 'Promoções &amp; ofertas');
	define('DISCOUNTS_BODY', 'Receba ofertas exclusivas e poupe nos seus restaurantes favoritos.');
	define('TELL_FRIENDS_TITLE', 'Partilhe com os seus amigos');
	define('TELL_FRIENDS_BODY', 'Fale aos seus amigos sobre nós e cada um de vocês pode receber 4 EUR.');
	
	define('DONT_BE_SHY', 'Não seja envergonhado. Peça a sua comida on-line.');
	define('CLICKS_AWAY', 'Apenas a 4 cliques de distância da sua comida/vinho de sonho!');
	
	define('BROWSE_CITY', 'Procure por cidade:');
	
	define('PERFECT_BUSINESS', 'Perfeito para
almoços de negócios');
	define('CREATE_CORP_ACCOUNT', 'Crie a sua conta de empresa para pedidos de grupo!');
	define('CREATE_CORP_ACCOUNT2', 'Create a corporate account');
	define('LEARN_MORE', 'Aprenda mais');
	
	define('BEST_RESTAURANT', 'Os melhores restaurantes aqui');
	
	define('STAY_CONNECTED', 'Continue conectado.');
	define('NEWSLETTER_TITLE', 'Junte-se à nossa lista de e-mails para receber ofertas especiais');
	define('NEWSLETTER_BODY', 'Venezvite é a melhor maneira de pedir comida para entrega e take-away. Introduza o seu endereço de e-mail para receber promoções e ofertas.');
	define('ENTER_EMAIL', 'Introduza o seu endereço de e-mail');
	
	define('HOW_WORKS_TITLE', 'Como funciona?');
	define('HOW_WORKS_BODY1', 'Faça o seu pedido');
	define('HOW_WORKS_BODY2', 'O seu pedido está a ser enviado para o restaurante');
	define('HOW_WORKS_BODY3', 'Yuppie! A sua comida está entregue!');
	
	define('HELLO_RESTAURANT_OWNERS', 'Olá dono do restaurante,
torne-se hoje um restaurante parceiro!');
	define('JOIN_TITLE', 'Junte-se à nossa rede de restaurantes');
	define('JOIN_BODY', 'Torne-se um restaurante parceiro. Descubra como nós podemos ajudá-lo a crescer o seu negócio, arranjar novos clientes e aumentar a sua facturação diária.');
	define('NO_DELIVER_TITLE', 'Não faz entregas? Não se preocupe!');
	define('NO_DELIVER_BODY', 'Os nossos parceiros de entrega na sua cidade vão fazer a entrega por si. Simplesmente prepare a comida e o nosso pareceiro de entrega vai levantá-la e entregá-la.');
	define('JOIN_NOW', 'Junte-se agora');
	
	define('ABOUT_TITLE', 'Sobre nós');
	define('ABOUT_BODY', 'Venezvite é o melhor, mais fácil e mais rápido modo de pedir comida e vinho directamente para sua casa ou escritório... ou qualquer outro sítio! Receba a sua comida e vinho dos melhores restaurantes perto de si entregues directamente à sua porta.');
