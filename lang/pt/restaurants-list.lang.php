<?php
	// SEO settings
	define('SEO_TITLE', 'Os melhores restaurantes dísponiveis para entrega de comida e vinho | Venezvite');
	define('SEO_KEYWORDS', 'Restaurantes que entregam, restaurantes que entregam em Lisboa, entrega de comida Lisboa, entrega de comida, entrega de vinho Lisboa, entrega de vinho, entrega de comida em Lisboa');
	define('SEO_DESCRIPTION', 'Os melhores restaurantes disponíveis para entrega em Lisboa!');
	
	
	define('FOR_', 'Para?');
	define('WHERE_ARE_U', 'Onde está?');
	define('SEARCH_HINT', 'Items de comida, tipos de cozinha, nomes de restaurantes');
	define('SORT_HINT', '<strong>Ordenar / Filtrar por</strong> Preço, tipos de cozinha ... ');
	define('SORT_BY', 'Ordenar por');
	define('FILTER_BY', 'Filtrar por');
	define('DISTANCE', 'Distância');
	define('ALPHABETICALLY', 'Alfabeticamente');
	define('CUISINES', 'Cozinhas');
	define('RATINGS', 'Pontuação');
	define('ANY_RATING', 'Qualquer pontuação');
	define('PRICE', 'Preço');
	define('ANY_PRICE', 'Qualquer preço');
	define('CLEAR', 'Apagar');
	define('CLEAR_ALL_SORT_FILTER', 'Apagar todo o tipo / filtros');
	define('SEARCH_CUISINES', 'Pesquisa cozinhas');
	define('CLEAR_ALL', 'Apagar todo');
	define('SHOW_MORE', '+ Mostrar mais');
	define('SHOW_LESS', '- Mostrar menos');
	define('AND_UP', '&amp; cima');
	define('AND_LESS', '&amp; menos');
	define('LIST_IS_FILTERED', 'A sua lista foi filtrada.');
	
	define('_AT', ' ');
	define('OPEN_RESTAURANTS_TITLE', '<span>{x}</span> restaurantes abertos disponíveis para: <span class="red">{datetime}</span>');
	define('SEE_MORE_RESTAURANTS', '<a id="see-advanced-orders" href="javascript:;">Clique aqui para ver</a> {x} mais restaurantes dentro da sua zona mas não disponíveis durante o tempo selecionado.');
	define('ORDERING_HOURS', 'Horas para efectuar pedidos');
	define('DELIVERED_IN', 'Entregue em: {$time_interval} minutos');
	define('ORDER', 'Pedido');
	
	define('VIEW_MAP', 'Veja a lista no mapa');
	define('HIDE_MAP', 'Esconder o mapa');
	
	define('ADV_ORDERS_TITLE', 'Apenas pedidos avançados');
	define('ADV_ORDERS_DESC', '{x} mais restaurantes existem na sua zona, mas não disponíveis durante a hora selecionada.');
	define('REOPENS', 'Reabre');
	define('NEXT_DELIVERY', 'Próxima entrega');
	define('ACCEPTS_CASH', 'Aceita dinheiro');
	define('PRE_ORDER', 'Pre-encomenda');
	
	define('NO_DELIVERY', 'A morada introduzida é fora da zona de entrega do restaurante. Por favor veja a lista de restaurantes disponíveis abaixo.');
	define('CLOSED_ALL_WEEK', 'O restaurante está fechado durante toda a próxima semana. Por favor selecione um novo restaurante da lista em baixo.');
	
	
	define('NO_MATCHING_RESTAURANT', 'Desculpe, não há restaurantes que correspondam aos seus critérios...');
	define('MODIFY_SEARCH', 'Por favor modifique a sua procura, ou recomende um restaurante que gostaria de ter no Venezvite.');
	define('RECOMMEND_RESTAURANT', 'Tem algum restaurante que gostaria de recomendar?');
	define('YOUR_EMAIL', 'O seu endereço de e-mail');
	define('RESTAURANT_NAME', 'Nome de restaurante');
	
	define('CITIES_WERE_IN', 'Aqui estão as cidades em que funcionamos');
