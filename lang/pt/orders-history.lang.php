<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('VIEW_ORDER', 'Veja o seu pedido');
	
	define('PENDING', 'Pendente');
	define('ACCEPTED', 'Aceite');
	define('DECLINED', 'Rejeitado');
	
	define('NO_PENDING_ORDERS', 'Não existem pedidos pendentes disponíveis.');
	define('VIEW_PAST_ORDERS', 'Veja pedidos anteriores');
	define('NO_PAST_ORDERS', 'Não existem pedidos anteriores.');
