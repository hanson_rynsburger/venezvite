﻿<?php
	define('CPC_ADMIN_PANEL', 'Password dimenticata?');
	
	define('CPC_TITLE', 'Change your Venezvite account\'s password');
	define('CPC_PASSWORD', 'New password');
	define('CPC_CONFIRM_PASSWORD', 'Confirm new password');
	define('CPC_UPDATE', 'Update password');
	
	define('CPC_EMAIL_SUBJECT', 'La vostra nuova Venezvite Password');
	define('CPC_EMAIL_BODY', 'Ciao {$userName},

Ecco la vostra nuova password per Venezvite, cambiata come avete richiesto:

<strong>{$password}</strong>

Potreste usarla da subito e Accedere a Venezvite.');
	
	define('CPC_SUCCESS', 'La richiesta della vostra password è stata confermata!' . "\n" . 
		'Per favore controllate la vostra e-mail per avere la vostra nuova Venezvite password.');
	define('CPC_ERROR', 'Sfortunatamente non siamo riusciti a mandarvi la nuova password via e-mail, la vostra vecchia password non è ancora cambiata.\n' . 
		'Per favore riprovate di nuovo, o contattateci per segnalarcelo!');
