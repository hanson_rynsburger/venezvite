﻿<?php
    define('CR_SUCCESS', 'Grazie per aver confermato il vostro indirizzo e-mail!' . "\n" . 
        'Per favore attendete mentre uno dei nostri supervisori approvino l\'account del vostro ristorante. Sarete notificati via e-mail appena approvato!');
