﻿<?php
	define('CPCR_ADMIN_PANEL', 'Impostazioni per i nostri partner');
	
	define('CPCR_TITLE', 'Change your Venezvite account\'s password');
	define('CPCR_PASSWORD', 'New password');
	define('CPCR_CONFIRM_PASSWORD', 'Confirm new password');
	define('CPCR_UPDATE', 'Update password');
	
	define('CPCR_EMAIL_SUBJECT', 'La vostra nuova Venezvite Password');
	define('CPCR_EMAIL_BODY', 'Ciao {$restaurantName},

Ecco la vostra nuova password per Venezvite, cambiata come avete richiesto:

<strong>{$password}</strong>

Potreste usarla da subito e Accedere a Venezvite qui: <a href="{$link}">{$link}</a>');
	
	define('CPCR_SUCCESS', 'La richiesta della vostra password è stata confermata!' . "\n" . 
		'Per favore controllate la vostra e-mail per avere la vostra nuova Venezvite password.');
	define('CRCR_ERROR', 'Sfortunatamente non siamo riusciti a mandarvi la nuova password via e-mail, la vostra vecchia password non è ancora cambiata.\n' . 
		'Per favore riprovate di nuovo, o contattateci per segnalarcelo!');
