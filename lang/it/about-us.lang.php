<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ABOUT_US_TITLE', 'Chi siamo');
	define('ABOUT_US_P1', 'Venezvite è il miglior metodo per ordinare online i vostri piatti preferiti e del delizioso vino da abbinarci. Niente più coda o comunicazioni complicate al telefono. Il nostro servizio è sicuro, facile e veloce.');
	define('ABOUT_US_P2', 'Venezvite è un marketplace che connette i migliori ristoranti e gastronomie a clienti locali che vogliono gustare un pasto gourmet in casa o in ufficio. I nostri ristoranti e partner offrono il servizio di consegna entro 1 ora.');
	
	define('MEET_TEAM_TITLE', 'Il Team');
	define('JOIN_TEAM', 'Lavora con noi!');
	
	define('MEET_DELIVERY_TEAM_TITLE', 'Il team di consegna');
	define('VP_MEMBER', VITE . ' Membro');
