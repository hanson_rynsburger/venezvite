<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
    define('LR_LOGIN_UNAPPROVED', 'Il ristorante non è stato ancora approvato. O non avete confermato l\'email, oppure il nostro team sta ancora visionando i dati.\n' . 
        'Per favore aspettate ancora un pò, oppure se è passato qualche giorno e non avete ricevuto notifiche, contattateci per sapere il vostro status.');
    define('LR_LOGIN_ERROR', 'Oops, le vostre credenziali sono errate. Per favore provate ancora o se avete dimenticato la password cliccate direttamente il link sopra.');
	
	define('LOGIN_ACCOUNT', 'Accedi nel tuo account');
	define('USERNAME', 'Nome Utente (il tuo indirizzo email)');
	define('FORGOT', 'Ho dimenticato il Nome Utente');
	
	define('CONFIRM_ORDERS', 'Conferma Ordini');
	define('CONFIRM_ORDERS_DESC', 'Confermate ordini in arrivo dal computer/tablet/Natel');
	define('MANAGE_RESTAURANT', 'Gestite il ristorante');
	define('MANAGE_RESTAURANT_DESC', 'Aggiornate il menù e le impostazioni del ristorante');
	define('SALES', 'Vendite');
	define('SALES_DESC', 'Analisi completa delle vendite del vostro ristorante');
