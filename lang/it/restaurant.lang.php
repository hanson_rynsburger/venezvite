<?php
	// SEO settings
	//define('SEO_TITLE', '');
	//define('SEO_KEYWORDS', '');
	//define('SEO_DESCRIPTION', '');
	
	
	define('WHEN_WHERE_DESC', 'Per confermare che questo ristorante consegna nella vostra zona, per favore inserite il vostro indirizzo esatto e l\'orario desiderato.');
	
	define('RETURN2LIST', 'Ritornate alla lista dei Ristoranti');
	define('ADD_TO_FAVES', 'Aggiungi ai favoriti');
	define('VERY_POPULAR_RESTAURANT', 'Un ristorante molto in Voga');
	define('POPULAR_RESTAURANT', 'Ristorante in Voga');
	define('OPEN', 'Aperto');
	define('CLOSED', 'Chiuso');
	define('CLOSES_IN', 'Chiude in {x} minuti');
	
	define('MORE_INFO', 'Maggiori informazioni');
	
	define('VISIT_WEB', 'Visita il Website');
	define('SERVING_IN', 'Posti a sedere');
	define('HOURS_DELIVERY_ZONES', 'Orario &amp; Zone di Consegna');
	define('TAKEAWAY_HOURS', 'Orario Take-Away');
	define('DELIVERY_HOURS', 'Orario Consegna');
	define('NO_DELIVERY', 'Non effettua Consegna');
	define('DELIVERY_ZONES', 'Zone di Consegna');
	define('ABOUT', 'Dettagli');
	
	define('SEARCH_HINT', 'Cercate nel Menù');
	define('FILTER', 'Filtri');
	define('FILTER_PHOTOS', 'Foto');
	
	define('ADD_ITEM', 'Aggiungi Prodotto');
	define('SHOW_IMAGE', 'Mostra immagine');
	define('HIDE_IMAGE', 'Nascondi immagine');
	define('VIEW_DETAILS', 'Dettagli');
	define('ADD2BAG', 'Aggiungiete alla borsa della spesa');
	define('SPECIAL_INSTRUCTIONS', 'Istruzioni speciali &amp; Note');
	define('SPECIAL_INSTRUCTIONS_EG', '(es. salsa extra)');
	define('EDIT_ITEM', 'Modifica Prodotto');
	define('UPDATE_BAG', 'Aggiorna la borsa');
 	define('OPTIONS_EXACTLY', 'Selezionate esattamente {x} opzioni');
	define('OPTIONS_AT_LEAST', 'Selezionate almeno {x} opzioni');
	define('OPTIONS_UP_TO', 'Select up to {x} option(s)');
	
	define('ITEM_ADDED', 'Prodotto aggiunto');
	define('GO2CHECKOUT', 'Procedete al Pagamento');
	
	define('NO_MENUS_FOR_SEARCH', 'La tua ricerca non ha trovato prodotti nel Menù.');
	
	define('REVIEWS_TITLE', 'Recensioni');
	define('WRITE_REVIEW', 'Scrivete una recensione');
	
	define('DELIVERY_SCHEDULE', 'Preferenze di Consegna');
