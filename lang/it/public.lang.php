<?php
	// SEO settings
	define('DEFAULT_SEO_TITLE', 'Venezvite | Gastronomia e vino a domicilio da ristoranti locali in Ticino.');
	define('DEFAULT_SEO_KEYWORDS', 'food delivery ticino, food delivery, wine delivery ticino, wine delivery, delivery food ticino, consegna a domicilio ticino');
	define('DEFAULT_SEO_DESCRIPTION', 'Venezvite is the best way to order food for delivery or takeaway in the Canton of Geneva and the Canton of Vaud. Venezvite is a marketing platform that connects great restaurants with hungry people! Are you hungry? Start your order or join our restaurant network now!');
	
	define('LOGO_SLOGAN', 'Consegna a domicilio o in ufficio.');
	
	define('FAVORITED_RESTAURANT', 'Favorited Restaurant');
	define('HELLO', 'Ciao');
	define('CITIES', 'Città');
	define('HELP', 'Help');
	define('LOGIN', 'Accedi');
	define('ORDERS_HISTORY', 'Storia degli ordini');
	define('MY_ACCOUNT', 'Il Mio Account');
	define('PROFILE', 'Profilo');
	define('ADDRESSES', 'Indirizi');
	define('PAYMENTS', 'Pagamenti');
	define('RESTAURANT_GALLERY', 'Gallery');
	define('BUSINESS_INFO', 'Informazioni commerciali');
	define('FINANCIAL_INFO', 'Dettagli finanziari');
	define('MENU', 'Menù');
	define('SPICY', 'Speziato');
	define('VEGETARIAN', 'Vegetariano');
	define('NO_GLUTEN', 'Senza Glutine');
	define('NO_LACTOSE', 'Senza Lattosio');
	define('RECOMMENDED', 'Consigliato');
	define('MENU_CATEGORIES', 'Menu Categories');
	define('MENU_OPTION_GROUPS', 'Menu Option Groups');
	define('SEE_PROFILE', 'Visualizza il mio profilo');
	define('LOGOUT', 'Disconnettersi');
	
	define('FB_LOGIN', '<strong>Accedi</strong> con <strong>facebook</strong>');
	define('OR_', 'oppure');
	define('USERNAME_CORPORATE', 'Nome Utente per membri corporate');
	define('PASSWORD', 'Password');
	define('KEEP_LOGGED', 'Tienimi connesso');
	define('OOPS_FORGOT', 'Oops, Ho dimenticato la mia <a href="{$link}">password</a>');
	define('LOGIN_INCENTIVE_TITLE', 'Primo ordine con Venezvite?');
	define('LOGIN_INCENTIVE_DESC', '<strong>Non avete bisogno di Accedere o creare un Account.</strong> Inserite il vostro indirizzo esatto, compilate il primo ordine, e noi faremo il resto!');
	define('INVALID_USER_PASS', 'Combinazione Credenziali non valide');
	define('UNABLE_TO_REGISTER', 'Sfortunatamente non siamo riusciti a registrare il vostro Account!\n' . 
		'Provate a ripetere la registrazione, o contattate Venezvite per segnalarlo, grazie!!');
	define('LAST_NAME', 'Cognome');
	define('FIRST_NAME', 'Nome');

	define('PHONE_PREFIX', 'Prefix');
	define('PHONE_NO', 'Numero Natel');
	
	define('RESTAURANTS_LIST_CUSTOM_PATH', 'consegna-del-cibo');
	define('BACK', 'Indietro');
	
	define('BREADCRUMBS_ENTER_ADDRESS', 'Inserite il vostro indirizzo');
	define('BREADCRUMBS_SELECT_RESTAURANT', 'Selezionate un ristorante');
	define('BREADCRUMBS_CHOOSE_FOOD', 'Scegliete i vostri piatti');
	
	define('REQUIRED', 'campo obbligatorio');
	define('OPTIONAL', 'opzionale');
	define('WHERE_WHEN', 'Dove e Quando?');
	define('DELIVERY', 'Consegna');
	define('TAKEAWAY', 'Take Away');
	define('SELECT_SAVED_LOCATION', 'selezionate un\'indirizzo già registrato');
	define('SUBMIT_NEW_LOCATION', '... o inseritene uno nuovo');
	define('SCHEDULED_FOR', 'Programmato per:');
	define('SCHEDULED_TIME', 'Ora:');
	define('FIND', 'Cerca');
	define('SUBMIT', 'Invia');
	
	define('TODAY', 'Oggi');
	define('TOMORROW', 'Domani');
	define('YESTERDAY', 'Ieri');
	define('ASAP', 'Appena possibile');
	
	define('NEW_', 'Nuovo');
	define('VITE', 'DP');
	define('REVIEWS', 'recensioni');
	define('DELIVERY_FEE', 'Spese di Consegna');
	define('MINIMUM_ORDER', 'Ordine Minimo');
	define('APPLY', 'Applica');
	define('CANCEL', 'Cancella');
	define('PRODUCT_TOTAL', 'Totale Prodotto:');
	define('CLOSED_NOW', 'Chiuso ora');
	
	define('NEXT_DELIVERY_TIME', 'La consegna tornerà disponibile il {datetime}'); // L'heure choisi a changé.
	define('NEXT_TAKEAWAY_TIME', 'Il take away tornerà disponobile il {datetime}'); // L'heure choisi a changé.
	
	define('ITEMS', 'prodotti');
	define('CHECKOUT', 'Pagamento');
	define('MY_ORDER', 'Il mio Ordine');
	define('EMPTY_BAG', 'La tua busta è vuota!');
	define('EDIT', 'Modifica');
	define('ADD', 'Add');
	define('DELETE', 'Cancella');
	define('DELIVERY_MIN', 'Ordine minimo per la Consegna');
	define('NO_TAKEAWAY_MIN', 'Non c\'è minimo di Ordine per il Take Away!');
	define('HAVE_PROMO', 'Avete un codice promozionale?');
	define('ENTER_PROMO', 'Inserite il codice promozionale');
	//define('DELIVERY_ADDRESS_LABEL', 'Please enter your address to confirm if this restaurant delivers to you.');
	define('ENTER_ADDRESS', 'Inserite il vostro indirizzo esatto');
	define('GRAND_TOTAL', 'Totale:');
	
	define('STREET_ADDRESS', 'Via');
	define('BUILDING_NO', 'Numero civico');
	define('CITY', 'Città');
	define('POSTAL_CODE', 'NPA');
	define('FLOOR_SUITE', 'Piano / Apt');
	define('ACCESS_CODE', 'Codice accesso cancello');
	define('SPECIAL_INSTR', 'Istruzioni Speciali - Note');
	define('SPECIAL_INSTR_HINT', 'Istruzioni Speciali sulla consegna');
	
	define('PARTNER_LOGIN', 'Accesso Partner');
	
	define('FOOTER_WHO_WE_ARE', 'Chi Siamo');
	define('FOOTER_ABOUT_US', 'Su di Noi');
	define('FOOTER_NEWS_PRESS', 'Novità &amp; Stampa');
	define('FOOTER_BLOG', 'Blog');
	define('FOOTER_YOUR_BUSINESS', 'Per la tua impresa');
	define('FOOTER_BECOME_PARTNER', 'Diventa un Ristorante Partner');
	
	define('FOOTER_CREATE_ACCOUNT', 'Create un Account Corporate in ufficio');
	define('FOOTER_CATERING_ORDER', 'Inoltrate una richiesta di Catering');
	define('FOOTER_CONTACT_HELP', 'Contatto &amp; Aiuto');
	define('FOOTER_HELP_FAQ', 'Help &amp; FAQ');
	define('FOOTER_CONTACT', 'Contattateci');
	define('FOOTER_SUGGEST', 'Suggerite un Ristorante');
	define('FOOTER_MORE', 'diPiù+');
	define('FOOTER_SPECIALS', 'Offerte Speciali');
	
	define('SUCCESSFUL_RECOMMANDATION', 'Grazie per averci suggerito questo ristorante.');
	
	define('FOOTER_COPYRIGHT', 'Consegna a domicilio o in ufficio. Tutti i diritti riservati.');
	define('FOOTER_USE_TERMS', 'Termini d\'uso');
	define('FOOTER_PRIVACY', 'Privacy');
	define('FOOTER_SITE_BY', 'Sito prodotto da');
	
	define('MORE_MONEY', 'Il minimo d\'ordine non è stato raggiunto' . 
		(@$current_page=='checkout' ? '<br />(O il ristorante attuale non consente pagamenti in contanti)' : '') . '.');
	define('CODE_ALREADY_USED', 'Sembra che il vostro codice sia già stato utilizzato!' . "\n" . 
		'Non potete utilizzare un codice promozionale più di una volta.');
	define('DA_PROMO_CODE', 'Codice Promozionale');
	define('PROMO_VALID_FROM', 'Questo codice promozionale è valido fino a {$date}.');
	define('PROMO_VALID_UNTIL', 'Questo codice promozionale è scaduto il {$date}.');
	define('INVALID_PROMO_CODE', 'Codice Promozionale Invalido!');
	define('LOGIN_FOR_PROMO', 'Per favore accedete o registratevi prima di usare un codice promozionale.');
	define('PROMO_USED', 'Questo codice promozionale è stato già utilizzato.');
	define('PROMO_MIN_VALUE', 'Questo codice promozionale è valido solo per ordini superiori a CHF {$value}.');
	define('INVALID_OR_REGISTERED_PROMO', 'Il vostro codice promozionale o non è valido, oppure è stato destinato ad un altro utente. Per favore Accedete e riprovate di nuovo!');
	define('EMAIL_IN_USE', 'Questa Email è già stata registrata nel nostro sistema');
	
	define('R_EMAIL_CONFIRMATION_SUBJECT', 'Il vostro Account Venezvite');
	define('R_EMAIL_CONFIRMATION_BODY', 'Ciao {$firstName},

Questa notifica vi conferma che il vostro Account Venezvite è ora attivo. Congratulazioni! Le credenziali del vostro account sono:

 - nome utente: {$username}
 - password: {$password}

Per favore siate accorti nel mantenere queste informazioni private e accessibili, così da poter accedervi velocemente. Potete accedere ora al sito ed ordinare i vostri piatti preferiti, ed un buon vino!

Vi ringraziamo per usare Venezvite!');
	
	define('HELLO_RESTAURANT', 'Salve, Ristorante Partner');
	define('HUNGRY_FOR_BUSINESS', 'Fame di aumentare il business? E\' Facile. Iscrivitevi su venezvite.com');
	
	define('HOW_IT_WORKS', 'Come Funziona');
	define('HOW_WE_HELP', 'Come vi siamo di Aiuto');
	define('JOIN', 'Iscrivetevi');
	
	define('WHO_ARE_WE', 'Chi siamo?');
	define('FOR_YOUR_COMPANY', 'Per la tua Impresa');
	define('CONTACT_HELP', 'Contatto &amp; Aiuto');
	define('SUBMENU_PLUS', 'di Più+');

	define('ORDER_FROM', 'Order from');
	define('CONTINUE_TO_CHECKOUT', 'Continue to checkout');
	
	define('NO_FAVORITED_RESTAURANT', 'No Favorited Restaurants');
