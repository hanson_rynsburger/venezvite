<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ORDER_CONFIRMATION', 'Conferma dell\'ordine');
	
	define('_AT', 'a');
	define('FOR_A', 'per');
	
	define('THANKS', 'Grazie.');
	define('ORDER_SENT', 'Il vostro ordine è stato spedito al ristorante.');
	define('ORDER_DETAILS', 'Il vostro ordine (<strong class="green">{order_no}</strong>) per <strong class="green">{order_value}</strong> alla {order_date} è stato spedito a <strong class="green">{restaurant}</strong>. Vi verrà spedita un\'email non appena il ristorante avrà confermato l\'ordine.');
	define('YOUR_ORDER_FOR', 'Il vostro ordine è per:');
	define('CONTACT_RESTAURANT', 'Se avete domande riguardo l\'ordine per favore contattate il ristorante: <strong class="green">{restaurant}</strong> al <strong class="green">{restaurant_phone}</strong>');
	
	define('WHAT_NEXT_TITLE', 'Cosa succede ora?');
	define('WHAT_NEXT_BODY1', '{restaurant} confermerà il vostro ordine nel sistema, che');
	define('WHAT_NEXT_BODY2', 'Genererà una email di conferma che vi verrà spedita, poi il');
	define('WHAT_NEXT_BODY3', '{restaurant} preparerà i piatti e il nostro partner ve li consegnerà.');
