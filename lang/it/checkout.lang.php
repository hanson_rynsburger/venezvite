<?php
	// SEO settings
	define('SEO_TITLE', 'Accedi o Iscriviti - Gastronomia e vino a domicilio | Venezvite');
	define('SEO_KEYWORDS', 'Iscriviti, vino a domicilio, cucina a domicilio');
	define('SEO_DESCRIPTION', 'Accedi o Iscriviti per ordinare online gastronomia e vino consegnati a casa tua.');
	
	
	define('ORDER_ERRORS', 'C\'è un errore nel tuo ordine:');
	define('USED_EMAIL', 'E-mail già registrata. Per favore Accedi, o clicca su Ho dimenticato la password.');
	define('PHONE_MANDATORY', 'Prima di continuare in Venezvite per favore aggiornate il vostro numero di telefono!');
	define('CANT_FIND_ADDRESS', 'Il sistema non riesce a determinare la posizione del vostro indirizzo. Per favore riprovate, o contattateci per segnalarlo!');
	define('NO_BUILDING_NO', 'We couldn\'t find a building number in the address you\'ve just submitted.');
	define('OUTSIDE_ADDRESS', 'Il vostro indirizzo è fuori dall\' area di consegna disponibile.');
	define('DIFFERENT_CHARGES_MINIMUM', 'Il vostro indirizzo ha cambiato la tariffa di consegna e l\'ordine minimo. Per favore aggiungete altri prodotti per raggiungere il nuovo minimo.');
	define('DIFFERENT_MINIMUM', 'Il vostro indirizzo ha cambiato la tariffa di consegna e l\'ordine minimo. Per favore aggiungete altri prodotti per raggiungere il nuovo minimo.');
	define('DIFFERENT_CHARGES', 'Il vostro nuovo indirizzo ha cambiato la tariffa di consegna. Per favore ricontrollate il vostro ordine.<br />' . 
		'Suggerimento: usate sempre il vostro indirizzo esatto quando cercate un ristorante.');
	define('WRONG_CC_TYPE', 'La vostra carta di credito non è supportata. Per favore provate un\'altra carta o contattateci per segnalarlo!');
	define('FILL_FIELDS', 'Per favore compilare tutti i campi richiesti.');
	
	define('ALMOST_DONE', '<span>Ci siamo quasi!</span> Inserite pure i dettagli.');
	define('CREATE_ACCOUNT', 'Create un Account');
	define('ACCOUNT_INFO', 'Informazioni sul vostro Account');
	define('EDIT_MY_INFO', 'Modifica i miei dettagli'); 
	define('ENTER_EMAIL', 'La vostra e-mail');
	define('CREATE_PASS', 'Create una Password');
	define('NEWSLETTER_ACCEPT', 'Si, vorrei ricevere sconti, ed avere novità sui ristoranti locali nella mia e-mail.');
	
	define('DELIVERY_DETAILS', 'Dettagli sulla consegna');
	define('FOR_DELIVERY', 'Consegna');
	define('CHANGE_TAKEAWAY', 'Cambia in Take-away');
	define('FOR_TAKEAWAY', 'Take-away');
	define('CHANGE_DELIVERY', 'Cambia in Consegna');
	
	define('ENTER_NEW_ADDRESS', 'Il tuo nuovo Indirizzo');
	define('EDIT_ADDRESS', 'Modifica questo indirizzo');
	define('SAVE_ADDRESS', 'Salva i dettagli.');
	define('TAKEAWAY_DESC', 'State inoltrando un ordine in Take-away, i piatti saranno pronti tra poco nel ristorante scelto per l\'asporto.');
	
	define('PAYMENT_OPTIONS', 'Opzioni di pagamento');
	define('INVOICE', 'Invoice');
	define('CASH', 'Contanti');
	define('CREDIT', 'Carta di Credito');
	define('CARD_NAME', 'Nome sulla carta');
	define('CCN', 'Numero carta');
	define('EXPIRATION_DATE', 'Data scadenza');
	define('CURRENCY', 'Valuta (' . OPTIONAL . ')');
	define('USE_NEW_CARD', 'Usa un\'altra carta');
	
	define('TOO_MUCH_CASH', 'Puoi inoltrare ordini in contanti per un massimo di ' . MAX_CASH_VALUE . ' {$currency}.');
	define('UNPAID_CASH', 'Il nostro archivio segnala che avete un ordine in contanti non saldato. Questa opzione rimarrà disabilitata fino a che il pagamento non sarà saldato.<br />' . 
		'Per favore inoltrate il vostro ordine pagando con la Carta di Credito.');
	define('UNAPPROVED_CASH', 'Il vostro ordine precedente in contanti non è ancora stato approvato. Per favore aspettate fino a quando non sarà approvato, o continuate usando la Carta di Credito.');
	define('NO_CASH', 'Questo ristorante non accetta pagamenti in contanti.');
	define('CASH_PAYMENT_DESC', 'Pagamento contanti alla consegna.<br />
(Solo Contanti. Il corriere di questo ristorante non accetta Carte di Credito.)');
	define('INVOICING_DESC', 'If your Corporate Account is approved for ordering food on Venezvite.com without paying online in advance (but receiving an invoice to be paid at a later time), you can use this option to place your order.' . "\n\n" . 
        'Be warned that, in case your account is NOT approved for such an option, your order will be ignored!' . "\n\n" . 
        'For details about how your account could become approved for invoice ordering, please <a class="popup" href="' . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/contact.html">contact us</a>!');
	
	define('AGREE_TERMS', 'Io accetto i <a class="popup" href="tou-customers.html">Termini di utilizzo.</a>.');
	define('SAVE_INFO', 'Salvate le vostre informazioni.');
	define('PLACE_ORDER_X', 'Inoltra il mio ordine ({x})');
	
	
	
	define('P_ORDER_EMAIL_TITLE', 'Nuovo ordine Venezvite');
	define('P_ORDER_EMAIL_ORDER_TYPE', 'Tipo di ordine');
	define('P_ORDER_EMAIL_DELIVERY', 'Consegna');
	define('P_ORDER_EMAIL_TAKEAWAY', 'Take away');
	define('P_ORDER_EMAIL_ORDER_NO', 'Ordine Numero');
	define('P_ORDER_EMAIL_DELIVERY_DATE_HOUR', 'Data e ora consegna');
	define('P_ORDER_EMAIL_RESTAURANT', 'Ristorante');
	define('P_ORDER_EMAIL_PHONE', 'Telefono');
	define('P_ORDER_EMAIL_ORDER', 'Ordine');
	define('P_ORDER_EMAIL_PAYMENT_DONE', 'Pagamento: PAGATO ');
	define('P_ORDER_EMAIL_PAYMENT_TBD', 'Pagamento: DA PAGARE');
	define('P_ORDER_EMAIL_TOTAL', 'Totale Prodotti');
	define('P_ORDER_EMAIL_ADDRESS', 'Indirizzo');
	define('P_ORDER_EMAIL_LAST_NAME', 'Cognome');
	define('P_ORDER_EMAIL_FIRST_NAME', 'Nome');
	define('P_ORDER_EMAIL_COMPANY', 'Nome Azienda');
	define('P_ORDER_EMAIL_STREET', 'Via');
	define('P_ORDER_EMAIL_ZIP', 'NPA');
	define('P_ORDER_EMAIL_CITY', 'Città');
	define('P_ORDER_EMAIL_FLOOR_CODE', 'Piano e Codice d\'ingresso.');
	define('P_ORDER_EMAIL_NOTES', 'Note');
	define('P_ORDER_EMAIL_CASH_ON_DELIVERY', 'CONTANTI ALLA CONSEGNA');
	define('P_ORDER_EMAIL_ORDER_READY', 'L\'ordine deve essere pronto per il <span style="color:#c00">{$datetime}</span>');
	define('P_ORDER_EMAIL_ACTIVATION_LINK', 'Accedere per accettare l\'ordine:');
	define('P_ORDER_EMAIL_DELIVERY_DELAY', 'Tempo stimato per la consegna: {$time} <span style="color:#c00">+/- 15 minuti</span>');
	define('P_ORDER_EMAIL_TAKEAWAY_TIME', 'Pronto per l\'asporto alle: {$time}');

	define('VIEW_IMAGES', 'View images');
