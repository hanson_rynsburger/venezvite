<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('VIEW_ORDER', 'Visualizza Ordine');
	
	define('PENDING', 'In Conferma');
	define('ACCEPTED', 'Confermato');
	define('DECLINED', 'Respinto');
	
	define('NO_PENDING_ORDERS', 'Non ci sono ordini da Confermare.');
	define('VIEW_PAST_ORDERS', 'Visuallizare Ordini Precedenti');
	define('NO_PAST_ORDERS', 'Non ci sono ordini precedenti.');
