<?php
	define('PAYMENT_TYPE', 'Metodo di Pagamento');
	define('CC', 'Carta di Credito');
	define('DELIVERY_CASH', 'Pagamento alla Consegna');
	define('TAKEAWAY_CASH', 'Pagamento al Takeaway');
	define('NO_LATER_THAN', 'Non più tardi di');
	define('MINUTES', 'minuti');
	define('NOTES', 'Note');
	define('CUSTOMER', 'Cliente');
	define('DELIVERY_DATETIME', 'Data/Orario della Consegna');
	define('DELIVERY_TO', 'Consegna a');
	define('FLOOR', 'Piano');
	define('DELIVERY_INSTRUCTIONS', 'Istruzioni per la Consegna');
	define('TAKEAWAY_DATETIME', 'Data/Orario del Takeaway');
	define('TOTAL_PRODUCTS', 'Totale dei Prodotti');
	define('DISCOUNT', 'Sconti');
	define('TOTAL', 'Montante');
