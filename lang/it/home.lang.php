<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('HUNGRY_TITLE', 'Fame? <span>Ordinate Ora</span>');
	define('ALREADY_MEMBER', 'Già Iscritto? Accedi');
	define('NOW_ACCEPTING', 'Accettiamo');
	
	define('EASY_ORDERING_TITLE', 'Comodi ordini online');
	define('EASY_ORDERING_BODY', 'Non dovrete neanche telefonare. Ordinazioni facili e veloci!');
	define('MORE_FOOD_TITLE', 'Non solo cibo');
	define('MORE_FOOD_BODY', 'Ordinate anche birra, vino, liquori, salumi e altro.');
	define('DISCOUNTS_TITLE', 'Sconti &amp; promo');
	define('DISCOUNTS_BODY', 'Riceverete offerte esclusive e azioni sui vostri ordini più frequenti.');
	define('TELL_FRIENDS_TITLE', 'Dillo a un amico');
	define('TELL_FRIENDS_BODY', 'Raccontate ai vostri amici di Venezvite e ognuno di voi riceverà uno sconto di CHF 4.');
	
	define('DONT_BE_SHY', 'Non siate timidi. Ordinate la Consegna Online.');
	define('CLICKS_AWAY', 'Vi basteranno 4 click per la vostra felicità eno-gastronomica!');
	
	define('BROWSE_CITY', 'Ricerca per città:');
	
	define('PERFECT_BUSINESS', 'Perfetto per 
pranzi di lavoro');
	define('CREATE_CORP_ACCOUNT', 'Create un account aziendale per le ordinazioni di gruppo!');
	define('CREATE_CORP_ACCOUNT2', 'Create a corporate account');
	define('LEARN_MORE', 'Maggiori informazioni');
	
	define('BEST_RESTAURANT', 'I migliori ristoranti sono qui');
	
	define('STAY_CONNECTED', 'Restiamo in Contatto.');
	define('NEWSLETTER_TITLE', 'Iscrivetevi alla nostra mailing list per ricevere le offerte speciali');
	define('NEWSLETTER_BODY', 'Venezvite è il miglior modo per ordinare la consegna a domicilio o il takeaway dei vostri piatti preferiti. Inserite il vostro indirizzo email per le ricevere le nostre azioni.');
	define('ENTER_EMAIL', 'Inserite il vostro indirizzo email');
	
	define('HOW_WORKS_TITLE', 'Come funziona?');
	define('HOW_WORKS_BODY1', 'Sfogliate i menù e ordinate');
	define('HOW_WORKS_BODY2', 'L\'ordine viene inoltrato al ristorante ');
	define('HOW_WORKS_BODY3', 'Yuppi! L\'ordine vi viene consegnato!');
	
	define('HELLO_RESTAURANT_OWNERS', 'Sei un Ristoratore?
Diventa oggi Nostro Partner!');
	define('JOIN_TITLE', 'Iscrivetevi al nostro Network');
	define('JOIN_BODY', 'Diventa nostro partner. Scopri come possiamo aiutarti a far crescere il tuo business, avere nuovi clienti, aumentare i profitti quotidiani.');
	define('NO_DELIVER_TITLE', 'Non consegnate? Non c\'è problema!');
	define('NO_DELIVER_BODY', 'I nostri partner nella vostra città effettueranno la consegna per voi. Dovrete solo preparare i piatti ed i nostri partner penseranno al ritiro e alla consegna.');
	define('JOIN_NOW', 'Iscrivetevi Ora');
	
	define('ABOUT_TITLE', 'Chi Siamo');
	define('ABOUT_BODY', 'Venezvite è il modo migliore, più facile e più veloce, per ordinare piatti o bevande consegnati direttamente a casa vostra o in ufficio... o ovunque vogliate! La consegna a domicilio avverrà direttamente alla vostra porta dai migliori ristoranti della vostra zona.');
