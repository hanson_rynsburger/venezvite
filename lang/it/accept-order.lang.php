﻿<?php
	define('AO_EMAIL_TITLE', 'Il tuo ordine su Venezvite è stato accettato');
	define('AO_EMAIL_BODY', 'Ciao {$userName},

il tuo ordine su Venezvite è stato appena confermato da {$restaurantName}.

Grazie per aver ordinato con Venezvite.');
