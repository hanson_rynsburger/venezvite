﻿<?php
	define('ACA_EMAIL_UNCONFIRMED', 'You cannot approve this corporate account, since it hasn\'t confirmed its email address yet!');
	define('ACA_SUCCESS', 'Corporate account successfully approved!' . "\n" . 
		'Its representative has just received an email confirmation, containing the path they should use for managing their corporate account\'s details.');
	define('ACA_FAILURE', 'Unfortunately the corporate account approval confirmation email message couldn\'t be sent.' . "\n" . 
		'Please try again, or contact the system administrator to report this issue!' . "\n\n" . 
		'The corporate account hasn\'t been activated.');
	
	define('ACA_EMAIL_CONFIRMATION_SUBJECT', 'Your Vezenvite account has been approved');
	define('ACA_EMAIL_CONFIRMATION_BODY', 'Hello {$companyName},

Your corporate account has just been approved. Congratulations!

All you have to do now is log in Venezvite using the details you submitted in your registration process, and start ordering food!

Welcome to Venezvite!');
