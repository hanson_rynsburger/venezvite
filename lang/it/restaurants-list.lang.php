<?php
	// SEO settings
	define('SEO_TITLE', 'The Best Restaurants Available for Food and Wine Delivery | Venezvite');
	define('SEO_KEYWORDS', 'Restaurant who deliver, restaurants who deliver in geneva, food delivery geneva, food delivery, wine delivery geneva, wine delivery, delivery food geneva');
	define('SEO_DESCRIPTION', 'The best restaurants available now for delivery in geneva!');
	
	
	define('FOR_', 'Per?');
	define('WHERE_ARE_U', 'Dove sei?');
	define('SEARCH_HINT', 'Ricette, cucine, nomi di ristoranti');
	define('SORT_HINT', '<strong>Ordina / Filtra per</strong> Prezzo, Cucina...');
	define('SORT_BY', 'Ordina per');
	define('FILTER_BY', 'Filtra per');
	define('DISTANCE', 'Distanza');
	define('ALPHABETICALLY', 'Alfabetico');
	define('CUISINES', 'Cucine');
	define('RATINGS', 'Punteggio');
	define('ANY_RATING', 'qualsiasi punteggio');
	define('PRICE', 'Prezzo');
	define('ANY_PRICE', 'qualsiasi prezzo');
	define('CLEAR', 'Elimina');
	define('CLEAR_ALL_SORT_FILTER', 'Cancella tutti i filtri');
	define('SEARCH_CUISINES', 'Cerca per Cucine');
	define('CLEAR_ALL', 'Elimina tutto');
	define('SHOW_MORE', '+ Mostra di più');
	define('SHOW_LESS', '- Mostra di meno');
	define('AND_UP', '&amp; più');
	define('AND_LESS', '&amp; meno');
	define('LIST_IS_FILTERED', 'La selezione è filtrata.');
	
	define('_AT', ' ');
	define('OPEN_RESTAURANTS_TITLE', '<span>{x}</span> ristoranti aperti disponibili per: <span class="red">{datetime}</span>');
	define('SEE_MORE_RESTAURANTS', '<a id="see-advanced-orders" href="javascript:;">Cliccate qui per vedere</a> {x} più ristoranti nella vostra area, ma chiusi durante l\'orario selezionato.');
	define('ORDERING_HOURS', 'Orario di Apertura');
	define('DELIVERED_IN', 'Consegnato in: {$time_interval} minuti');
	define('ORDER', 'Ordine');
	
	define('VIEW_MAP', 'Guarda la lista sulla Mappa');
	define('HIDE_MAP', 'Nascondi Mappa');
	
	define('ADV_ORDERS_TITLE', 'solo Ordini Avanzati');
	define('ADV_ORDERS_DESC', '{x} più ristoranti nella vostra area, ma chiusi durante l\'orario selezionato');
	define('REOPENS', 'Riapre');
	define('NEXT_DELIVERY', 'Prossima Consegna disponibile');
	define('ACCEPTS_CASH', 'Accetta contanti');
	define('PRE_ORDER', 'Pre-Ordinate');
	
	define('NO_DELIVERY', 'L\'indirizzo inserito è fuori dalla zona di consegna del ristorante. Per favore controllate la lista dei ristoranti disponibili qui sotto.');
	define('CLOSED_ALL_WEEK', 'Il ristorante resterà chiuso per tutta la prossima settimana. Per favore selezionate un\\\'altro ristorante dalla lista qui sotto.');
	
	
	define('NO_MATCHING_RESTAURANT', 'Ci scusiamo, ma non ci sono ristoranti corrispondenti al vostro criterio di ricerca...');
	define('MODIFY_SEARCH', 'Per favore, modificate la vostra ricerca, o suggerite un ristorante che vorreste vedere su Venezvite.');
	define('RECOMMEND_RESTAURANT', 'Avete un ristorante da suggerire/raccomandare?');
	define('YOUR_EMAIL', 'Il vostro indirizzo email');
	define('RESTAURANT_NAME', 'Nome del Ristorante');
	
	define('CITIES_WERE_IN', 'Ecco la lista di città in cui siamo presenti');
