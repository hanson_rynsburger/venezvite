<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('EDIT_DETAILS', 'Modifica i dettagli finanziari');
	define('BUSINESS_NAME', 'Conto / Ragione sociale');
	define('IBAN', 'Codice IBAN');
	define('BANK', 'Nome della banca');
	define('SWIFT', 'Codice SWIFT');
	
	define('COMMISSION_AGREEMENT', 'Venezvite Commission Agreement');
	define('COMMISSION_PERCENTAGE', 'Venezvite Commission: {$percentage}% + Spese di consegna');
	define('COMMISSION_PAYMENT', 'Pagamento: Il 15 ° giorno di ogni mese, mentre vi e un minimo di 500.');
	define('THANK_YOU', 'Grazie per essere un partner Venezvite.');
	define('QUESTIONS_EMAIL', 'Per qualsiasi informazione si prega di e-mail: hello@venezvite.com');
