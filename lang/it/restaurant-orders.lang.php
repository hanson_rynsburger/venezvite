<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('TIME_PERIOD', 'Periodo di tempo');
	define('TOTAL_ORDERS', 'Totale ordini');
	define('TOTAL_SALES', 'Totale vendite');
	define('TOTAL_EARNINGS', 'Total Earnings');
	define('ALL_TIME', 'Sempre');
	define('THIS_WEEK', 'Questa settimana');
	define('THIS_MONTH', 'Questo mese');
	define('PAST_MONTH', 'Mese scorso');
	define('THIS_YEAR', 'Quest\'anno');
	
	define('NEW_ORDERS', 'Nuovi Ordini');
	define('PAST_ORDERS', 'Ordini Precedenti');
	
	define('RECEIVED_DATE', 'Ricevuto il <br />' . 
		'<span>Data / Ora</span>');
	define('RECEIVED_FROM', 'Da');
	define('ORDER_TYPE', 'Tipo di Ordine');
	define('ORDER_AMOUNT', 'Montante');
	define('ORDER_REQUIRED', 'Quando');
	define('ORDER_STATUS', 'Status');
	
	define('PENDING', 'Non Confermato');
	define('ACCEPT_ORDER', 'Confermato');
	define('DECLINE_ORDER', 'Respinto');
	define('CONFIRM_ACCEPT', 'Siete sicuri di voler ACCETTARE quest\'ordine?');
	define('CONFIRM_DECLINE', 'Siete sicuri di voler RIFIUTARE quest\'ordine?');
	define('ACCEPTED', 'Confermato');
	define('DECLINED', 'Respinto');
	
	define('NO_PENDING_ORDERS', 'Non ci sono ordini da Confermare.');
	define('VIEW_PAST_ORDERS', 'Visualizzare ordini precedenti');
	define('NO_PAST_ORDERS', 'Nessun ordine precedente disponibile.');
