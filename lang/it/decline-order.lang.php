﻿<?php
	define('DO_EMAIL_TITLE', 'Il vostro ordine su Venezvite è stato respinto');
	define('DO_EMAIL_BODY', 'Ciao {$userName},

Il vostro ordine su Venezvite è stato respinto da {$restaurantName}.
{$reason}
Potete contattare il ristorante per maggiori dettagli o ordinare ad un altro ristorante.

Il montante pagato (se fosse già stato pagato) verrà accreditato sul vostro conto corrente appena possibile.');
	
	define('DO_EMAIL_REASON', 'La ragione per cui il ristorante ha respinto il vostro ordine è: ');
