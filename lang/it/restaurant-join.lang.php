<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('INVALID_FILE', 'Uno o più file non sono stati caricati. Non supportiamo quel tipo di file.');
	define('CANT_SAVE_RESTAURANT', 'Sfortunatamente non siamo riusciti a registrare i dettagli del vostro ristorante. Per favore provate ancora o contattaci per segnalarcelo, grazie!');
	define('REGISTRATION_CONFIRMATION', 'Riceverete un email fra pochi secondi. Per completare la registrazione, per favore cliccate sul link di conferma di indirizzo.');
	define('SOME_ERRORS', 'C\'è stato un errore in fase di registrazione del vostro Account:');
	define('REGISTRATION_SUCCESS', 'Siete riusciti a registrare il vostro ristorante con successo!');
	
	define('CONTACT_INFO', 'Informazione Contatto <span>* (' . REQUIRED . ')</span>');
	define('MOBILE', 'Natel');
	
	define('RESTAURANT_INFO', 'Dettagli Ristorante');
	define('SELECT_COUNTRY', 'Seleziona il tuo paese');
	define('RESTAURANT_EMAIL', 'Indirizzo Email');
	define('RESTAURANT_NAME', 'Nome del Ristorante');
	define('RESTAURANT_ADDRESS', 'Indirizzo del Ristorante');
	define('RESTAURANT_CITY', 'Città / Paese * (Selezionate dalla lista)');
	define('RESTAURANT_PHONE', 'Telefono Ristorante');
	define('ENTER_VALID_PHONE', 'Enter a valid phone number');
	define('RESTAURANT_URL', 'Sito web Ristorante');
	define('RESTAURANT_DESC', 'Info sul ristorante (Descrizione)');
	
	define('PRIMARY_CUISINE_TYPE', 'Selezionate i vostri tipi di cucina <span>Selezione Multipla</span>');
	define('CHOOSE_CUISINE', 'Scegliete la Cucina');
	define('CHOOSE_SERVICES', 'Per favore selezionate i servizi che già mettete a disposizione.');
	define('CATERING', 'Catering');
	define('CUSTOM_DELIVERY', 'Noi non consegnamo a domicilio e vorremmo usare il vostro partner.');
	define('SELECT_TIME', 'Selezionate Orario');
	
	define('HOURS_OPERATION', 'Orario di apertura');
	define('SAME_EVERY_DAY', 'Applica lo stesso orario ogni giorno');
	define('DELIVERY_ZONES', 'Zone di Consegna');
	define('ZONE', 'Zona 1');
	define('ZONE_RANGE', 'Zona 1 raggio in km');
	define('MIN_DELIVERY', 'Minimo d\'ordine per consegna in zona 1');
	define('DELIVERY_FEE_ZONE', 'Spese di consegna per la zona 1');
	define('SELECT_TIME_INTERVAL', 'Tempo medio di consegna (solo trasporto)');
	define('ADD_ZONE', 'Aggiungiete un\'altra zona');
	define('PHOTOS', 'Foto');
	define('UPLOAD_PHOTOS', 'Caricate foto (.jpg or .png)');
	define('UPLOAD_MENU', 'Caricate Menù (.docx, .pdf or .jpg)');
	
	define('CREATE_ACCOUNT_TITLE', 'Create il vostro Account');
	define('USERNAME', 'Nome Utente: il vostro indirizzo email');
	define('CONFIRM_USERNAME', 'Confermate il vostro indirizzo email');
	define('CONFIRM_PASSWORD', 'Confermate la password');
	define('AGREE_TERMS', 'Io accetto i <a class="popup" href="{$link}">termini e condizioni</a>.');
	define('CREATE_ACCOUNT', 'Create Account');
	
	define('INVALID_ADDRESS', 'Oops! {address} non sembra essere un indirizzo valido!');
	
	define('RR_EMAIL_CONFIRMATION_SUBJECT', 'Confermate il vostro Account Venezvite');
	define('RR_EMAIL_CONFIRMATION_BODY', 'Ciao {$restaurantName},

Per fare in modo che il vostro ristorante venga abiitato a ricevere ordini da uno dei nostri membri del team, per favore confermate il vostro indirizzo email, cliccando il link di seguito:

<a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'it') . '/confirm-restaurant.html?check={$uniqueHash}">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'it') . '/confirm-restaurant.html?check={$uniqueHash}</a>

Grazie per aver scelto Venezvite!');
