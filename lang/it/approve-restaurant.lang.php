﻿<?php
    define('AR_EMAIL_UNCONFIRMED', 'Non è possibile approvare l\'account di questo ristorante, perchè il suo indirizzo e-mail non è stato ancora confermato!');
    define('AR_SUCCESS', 'Il ristorante è stato approvato!' . "\n" . 
        'Avete già ricevuto una conferma via e-mail che contiene il percorso al proprio account ristorante.');
    define('AR_FAILURE', 'Sfortunatamente l\'e-mail che contiene l\'approvazione del ristorante non è stata inviata.' . "\n" . 
        'Per favore provate ancora, o contattate Venezvite per segnalarlo, grazie!' . "\n\n" . 
        'L\'account ristorante non è stato attivato.');
    
    define('AR_EMAIL_CONFIRMATION_SUBJECT', 'Il tuo account Venezvite è stato approvato');
    define('AR_EMAIL_CONFIRMATION_BODY', 'Ciao {$restaurantName},

Il vostro account ristorante è stato appena approvato. Congratulazioni!

Tutto quello che dovete fare è accedere a <a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html</a>, usando le credenziali che avete registrato, per finalizzare il profilo e compilare il menù.

Benvenuti su Venezvite!');
