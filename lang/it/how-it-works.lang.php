<?php
	// SEO settings
	define('SEO_TITLE', HOW_IT_WORKS);
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('FIND_NEW_CUSTOMERS', 'Trovate nuovi clienti, vi assicurate entrate frequenti, aumentate i ricavi e fate crescere i profitti.');
	define('JOIN_PRESENTATION', 'Iscrivendovi al network dei ristoranti Venezvite darete, a migliaia di persone, la possibilità di ordinare la consegna a domicilio o il takeaway dal vostro ristorante, online o con lo smartphone.');
	define('JOIN_BUTTON', 'Iscrivetevi');
	
	define('INSTANT_PRESENCE_TITLE', 'Presenza Online Istantanea');
	define('INSTANT_PRESENCE_DESC', 'Noi pubblicizziamo il vostro ristorante a migliaia di persone affamate tutti i giorni. Il vostro menù e gli speciali del giorno saranno mandati a quelle persone che stanno cercano proprio il vostro stile o quel particolare piatto.');
	define('PREPARE_FOOD_TITLE', 'Preparate i Vostri Piatti');
	define('PREPARE_FOOD_DESC', 'Riceverete un ordine elettronico e poi preparerete i vostri piatti, pronti per essere consegnati a domicilio o ritirati direttamente dal cliente.');
	define('GET_PAID_TITLE', 'Aumentate Vendite e Profitti!');
	define('GET_PAID_DESC', 'Riceverete un bonifico bancario da Venezvite per tutti gli ordini ricevuti. E\' davvero così semplice.');
