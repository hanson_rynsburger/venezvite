<?php
	define('PAYMENT_TYPE', 'Type de paiement');
	define('CC', 'Carte de crédit');
	define('DELIVERY_CASH', 'Espèce à la livraison');
	define('TAKEAWAY_CASH', 'Espèce (sur place pour les commandes à emporter)');
	define('NO_LATER_THAN', 'Pas plus tard que');
	define('MINUTES', 'minutes');
	define('NOTES', 'Notes');
	define('CUSTOMER', 'Client');
	define('DELIVERY_DATETIME', 'Date/heure de livraison');
	define('DELIVERY_TO', 'Livraison à');
	define('FLOOR', 'Etage');
	define('DELIVERY_INSTRUCTIONS', 'Instructions de livraison');
	define('TAKEAWAY_DATETIME', 'Date/heure à emporter');
	define('TOTAL_PRODUCTS', 'Total produits');
	define('DISCOUNT', 'Réduction');
	define('TOTAL', 'Total');
