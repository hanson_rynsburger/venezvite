﻿<?php
	define('AO_EMAIL_TITLE', 'Votre commande Venezvite a été acceptée!');
	define('AO_EMAIL_BODY', 'Bonjour {$userName},

Cet email confirme que la commande enregistrée sur Venezvite a été acceptée par {$restaurantName}.

Vous serez prévenu(e) lorsque votre plat sera prêt.');
