<?php
	define('CPCR_ADMIN_PANEL', 'Interface d\'administration de nos Restaurants Partenaires');
	
	define('CPCR_TITLE', 'Changer le mot de passe de votre compte Venezvite');
	define('CPCR_PASSWORD', 'Nouveau mot de passe');
	define('CPCR_CONFIRM_PASSWORD', 'Confirmer le nouveau mot de passe');
	define('CPCR_UPDATE', 'Mise à jour le mot de passe');
	
	define('CPCR_EMAIL_SUBJECT', 'Votre nouveau mot de passe Venezvite');
	define('CPCR_EMAIL_BODY', 'Cher {$restaurantName},

Suite à votre demande, voici votre nouveau mot de passe Venezvite:

<strong>{$password}</strong>

Vous pouvez vous servir de ce mot de passe afin d\'accéder à votre compte en cliquant <a href="{$link}">{$link}</a>');
	
	define('CPCR_SUCCESS', 'Vous avez confirmé votre demande de modification de mot de passe Venezvite.' . "\n" . 
		'Veuillez consulter votre compte email pour prendre connaissance de votre nouveau mot de passe.');
	define('CRCR_ERROR', 'Désolé, nous n\'avons pas réussi à vous transmettre votre nouveau mot de passe par email. Votre mot de passe reste inchangé.\n' . 
		'Veuillez nous confirmer votre demande de modification de mot de passe ou contactez-nous pour nous signaler ce problème.');
