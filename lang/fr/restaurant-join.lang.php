<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('INVALID_FILE', 'Un ou plusieurs fichiers n\'ont pas été téléchargés. Nous ne prenons pas en charge ce type de fichier.');
	define('CANT_SAVE_RESTAURANT', 'Malheureusement, nous ne pouvons pas enregistrer les détails de votre restaurant. Veuillez essayer de nouveau ou contactez-nous pour signaler ce problème.');
	define('REGISTRATION_CONFIRMATION', 'Vous recevrez dans quelques secondes un e-mail. Pour compléter votre inscription, veuillez cliquer sur le lien figurant dans l\'email pour confirmer votre adresse email.');
	define('SOME_ERRORS', 'Il y a eu quelques erreurs lors de la création de votre compte:');
	define('REGISTRATION_SUCCESS', 'Vous avez enregistré votre restaurant avec succès!');
	
	define('CONTACT_INFO', 'Informations de contact <span>* (' . REQUIRED . ')</span>');
	define('MOBILE', 'Mobile');
	
	define('RESTAURANT_INFO', 'Informations sur le restaurant');
	define('SELECT_COUNTRY', 'Sélectionnez votre pays');
	define('RESTAURANT_EMAIL', 'Adresse email');
	define('RESTAURANT_NAME', 'Nom du restaurant');
	define('RESTAURANT_ADDRESS', 'Adresse du restaurant (rue)');
	define('RESTAURANT_CITY', 'Lieu * (Sélectionnez votre ville dans la liste ci-dessous)');
	define('RESTAURANT_PHONE', 'Numéro de téléphone du restaurant');
	define('ENTER_VALID_PHONE', 'Soumettre un numéro de téléphone valide');
	define('RESTAURANT_URL', 'Site internet du restaurant');
	define('RESTAURANT_DESC', 'Info Restaurant (À propos de votre restaurant)');
	
	define('PRIMARY_CUISINE_TYPE', 'Sélectionnez votre type(s) de cuisine principale(s) <span>Sélectionnez autant que nécessaire</span>');
	define('CHOOSE_CUISINE', 'Choisir une cuisine');
	define('CHOOSE_SERVICES', 'Veuillez sélectionner les services que vous fournissez');
	define('CATERING', 'Service Traiteur');
	define('CUSTOM_DELIVERY', 'Nous ne livrons pas et nous voudrions utiliser un partenaire de livraison.');
	define('SELECT_TIME', 'Sélectionnez');
	
	define('HOURS_OPERATION', 'Horaires d\'ouverture');
	define('SAME_EVERY_DAY', 'Appliquer les mêmes horaires d\'ouverture pour chaque jour');
	define('DELIVERY_ZONES', 'Zones de livraison');
	define('ZONE', 'Zone 1');
	define('ZONE_RANGE', 'Surface correspondant à la zone 1 (en km)');
	define('MIN_DELIVERY', 'Minimum de commande pour les livraisons en zone 1');
	define('DELIVERY_FEE_ZONE', 'Frais de livraison pour la zone 1');
	define('SELECT_TIME_INTERVAL', 'Sélectionnez la durée');
	define('ADD_ZONE', 'Ajouter une autre zone');
	define('PHOTOS', 'Photos');
	define('UPLOAD_PHOTOS', 'Télécharger des photos (.jpg ou .png)');
	define('UPLOAD_MENU', 'Téléchargez le menu (.docx, .pdf ou .jpg)');
	
	define('CREATE_ACCOUNT_TITLE', 'Créez votre compte');
	define('USERNAME', 'Nom d\'utilisateur: Entrez votre adresse e-mail');
	define('CONFIRM_USERNAME', 'Confirmez votre adresse e-mail');
	define('CONFIRM_PASSWORD', 'Confirmez le mot de passe');
	define('AGREE_TERMS', 'J\'accepte les <a class="popup" href="{$link}">termes et les conditions</a>.');
	define('CREATE_ACCOUNT', 'Créer un compte');
	
	define('INVALID_ADDRESS', 'Oups! {address} ne semble pas être une adresse valide!');
	
	define('RR_EMAIL_CONFIRMATION_SUBJECT', 'Confirmez votre compte Venezvite');
	define('RR_EMAIL_CONFIRMATION_BODY', 'Bonjour {$restaurantName},

Afin d\'activer votre compte, veuillez confirmer votre adresse email en cliquant sur le lien ci-dessous:

<a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'fr') . '/confirm-restaurant.html?check={$uniqueHash}">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'fr') . '/confirm-restaurant.html?check={$uniqueHash}</a>

Merci d\'avoir choisi Venezvite!');
