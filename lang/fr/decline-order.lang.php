﻿<?php
	define('DO_EMAIL_TITLE', 'Votre commande Venezvite a été refusée!');
	define('DO_EMAIL_BODY', 'Bonjour {$userName},

Cet email confirme que la commande enregistrée sur Venezvite a été refusée par {$restaurantName}.
{$reason}
Vous pouvez soit contacter le restaurant pour plus de détails, ou essayer de commander chez un autre restaurant.

Le montant payé (dans le cas échéant) vous sera remboursé sur votre compte bancaire dès que possible.');
	
	define('DO_EMAIL_REASON', 'La raison pour laquelle le restaurant a refusé cette commande est: ');
