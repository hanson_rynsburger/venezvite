<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ORDER_CONFIRMATION', 'Confirmation de commande');
	
	define('_AT', 'à');
	define('FOR_A', 'pour un');
	
	define('THANKS', 'Merci.');
	define('ORDER_SENT', 'Le détails de votre commande a été envoyé au restaurant.');
	define('ORDER_DETAILS', 'Votre commande (<strong class="green">{order_no}</strong>) de <strong class="green">{order_value}</strong> du {order_date} a été envoyé à <strong class="green">{restaurant}</strong>. Vous recevrez un message de confirmation du restaurant.');
	define('YOUR_ORDER_FOR', 'Votre commande est pour:');
	define('CONTACT_RESTAURANT', 'Si vous avez des questions concernant votre commande, veuillez contactez <strong class="green">{restaurant}</strong> à <strong class="green">{restaurant_phone}</strong>');
	
	define('WHAT_NEXT_TITLE', 'Les étapes:');
	define('WHAT_NEXT_BODY1', '{restaurant} reçoit votre commande;');
	define('WHAT_NEXT_BODY2', '{restaurant} vous envoie un message de confirmation;');
	define('WHAT_NEXT_BODY3', '{restaurant} prépare votre commande et le partenaire de livraison s’occupe du reste!');

	define('THANK_YOU_YOUR_ORDER', 'Thank you<br/> for your order, {name}');
	define('ESTIMATED_DELIVERY', 'Estimated Delivery: {time}');
	define('PLUS_MINUS', '&plusmn; 15 minutes'); 
	define('ORDER_DETAIL', 'Order details');
	define('ORDERED_FROM', 'Ordered from: ');
	define('DELIVER_TO', 'Deliver to: ');
	
	define('SUB_TOTAL', 'Sub Total');
	define('PROMO_CODE', 'Promo Code');
