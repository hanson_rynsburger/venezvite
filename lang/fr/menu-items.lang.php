<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ADD_ITEM', 'Ajouter un plat');
	define('EDIT_ITEM', 'Mettre un plat à jour');
	define('SELECT_CATEGORY', 'Choisir un catégorie');
	define('NEW_CATEGORY', 'Créer une nouvelle catégorie');
	define('MENU_ITEM_NAME', 'Nom de plat');
	define('CHOOSE_PHOTO', 'Choisir une photo');
	define('MENU_ITEM_DESCRIPTION', 'Description du plat');
	define('PRICE_CURRENCY', 'Prix ({$currency})');
	define('DISCOUNT_PERCENT', 'Pourcentage de réduction');
	define('ADD_OPTIONS_SET', 'Ajouter un groupe d\'options');
	define('NEW_OPTIONS_SET', 'Créer un nouveau groupe d\'options');
	define('ENABLED', 'Activé');
	define('ADD_ITEM_BUTTON', 'Ajouter au menu');
	define('EDIT_ITEM_BUTTON', 'Mettre à jour');
	
	define('REMOVE_CONFIRMATION', 'Êtes-vous sûr de vouloir supprimer cet élément?');
	define('NO_MENU_CATEGORIES', 'Vous n\'avez pas de catégories de menu définies.');
