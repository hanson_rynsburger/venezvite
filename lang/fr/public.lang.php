<?php
	// SEO settings
	define('DEFAULT_SEO_TITLE', 'Venezvite | Livraison à Domicile Genève');
	define('DEFAULT_SEO_KEYWORDS', 'livraison a domicile geneve, venez vite, venezvite, livraison geneve, livraison domicile geneve, pad thai geneve, livraison à domicile genève, livraison a domicile, livraison genève, repas à domivile genève, repas livré, livraison repas, livraison domicile genève, livraison repas geneve, geneve livraison domicile, repas a domicile, restaurant a domivile geneve, livraison domicile, commander a domicile geneve, commande repas en ligne, pizza geneve livraison, couscous a domicile, livraison sushi geneve, geneve livraison, livraison nourriture geneve, pad thai geneve, green mango geneve, café du centre, maloca geneve, i feel bio geneve, barraco chic, l\'entracte geneve, blaqk restaurant geneve, wiine, camilos place, restaurant pad thai geneve');
	define('DEFAULT_SEO_DESCRIPTION', 'Livraison de Repas à Domicile ou au Bureau dans le canton de Genève. Venezvite est la meilleure façon de commander à manger à emporter ou à livrer. Venezvite est une plateforme marketing qui connecte d\'excellents restaurants à des gens qui ont faim! Avez-vous faim? Passez votre commande, ou rejoignez nos restaurants.');
	
	define('LOGO_SLOGAN', 'Livraison à domicile ou au bureau');
	
	define('FAVORITED_RESTAURANT', 'Favorited Restaurant');
	define('HELLO', 'Salut');
	define('CITIES', 'Des villes');
	define('HELP', 'Aide');
	define('LOGIN', 'Se connecter');
	define('ORDERS_HISTORY', 'Mes commandes');
	define('MY_ACCOUNT', 'Mon compte');
	define('PROFILE', 'Profil');
	define('ADDRESSES', 'Adresses');
	define('PAYMENTS', 'Paiements');
	define('RESTAURANT_GALLERY', 'Gallery');
	define('BUSINESS_INFO', 'Infos entreprise');
	define('FINANCIAL_INFO', 'Informations bancaires');
	define('MENU', 'Menu');
	define('SPICY', 'Épicé');
	define('VEGETARIAN', 'Végétarien');
	define('NO_GLUTEN', 'Sans gluten');
	define('NO_LACTOSE', 'Sans lactose');
	define('RECOMMENDED', 'Recommandé');
	define('MENU_CATEGORIES', 'Catégories');
	define('MENU_OPTION_GROUPS', 'Groupes d\'options de menu');
	define('SEE_PROFILE', 'Voir mon profil');
	define('LOGOUT', 'Se déconnecter');
	
	define('FB_LOGIN', '<strong>Se connecter</strong> avec <strong>Facebook</strong>');
	define('OR_', 'où');
	define('USERNAME_CORPORATE', 'nom d\'utilisateur pour les membres d\'entreprise');
	define('PASSWORD', 'Mot de passe');
	define('KEEP_LOGGED', 'Sauvegarder les données');
	define('OOPS_FORGOT', 'Oups, j\'ai oublié mon <a href="{$link}">mot de passe</a>');
	define('LOGIN_INCENTIVE_TITLE', 'C\'est votre première commande sur Venezvite?');
	define('LOGIN_INCENTIVE_DESC', '<strong>Vous n\'avez pas besoin de créer un compte.</strong> Entrez votre adresse exacte, passez votre commande et nous nous occupons du reste!');
	define('INVALID_USER_PASS', 'Combinaison de nom d\'utilisateur et de mot de passe incorrecte');
	define('UNABLE_TO_REGISTER', 'Malheureusement nous n\'avons pas enregistré votre compte!\n' . 
		'Essayez de vous enregistrer à nouveau, ou contactez-nous pour nous signaler ce probleme.');
	define('LAST_NAME', 'Nom');
	define('FIRST_NAME', 'Prénom');
	
	define('PHONE_PREFIX', 'Prefix');
	define('PHONE_NO', 'Numéro de téléphone');
	
	define('RESTAURANTS_LIST_CUSTOM_PATH', 'restaurants-livraison-de-repas-a-domicile');
	define('BACK', 'Arrière');
	
	define('BREADCRUMBS_ENTER_ADDRESS', 'Votre adresse');
	define('BREADCRUMBS_SELECT_RESTAURANT', 'Choisir un restaurant');
	define('BREADCRUMBS_CHOOSE_FOOD', 'Choisir votre plat');
	
	define('REQUIRED', 'obligatoire');
	define('OPTIONAL', 'facultatif');
	define('WHERE_WHEN', 'Où et quand?');
	define('DELIVERY', 'Livraison à domicile');
	define('TAKEAWAY', 'À emporter');
	define('SELECT_SAVED_LOCATION', 'sélectionner une adresse enregistrée');
	define('SUBMIT_NEW_LOCATION', '... ou soumettre une nouvelle adresse');
	define('SCHEDULED_FOR', 'Quand:');
	define('SCHEDULED_TIME', 'Heure:');
	define('FIND', 'Trouver');
	define('SUBMIT', 'Envoyer');
	
	define('TODAY', 'Aujourd\'hui');
	define('TOMORROW', 'Demain');
	define('YESTERDAY', 'Hier');
	define('ASAP', 'Au plus tôt');
	
	define('NEW_', 'Nouveau');
	define('VITE', 'DP');
	define('REVIEWS', 'avis publiés');
	define('DELIVERY_FEE', 'Frais de livraison');
	define('MINIMUM_ORDER', 'Minimum de livraison');
	define('APPLY', 'Soumettre');
	define('CANCEL', 'Annuler');
	define('PRODUCT_TOTAL', 'Total des produits:');
	define('CLOSED_NOW', 'Ferme bientôt');
	
	define('NEXT_DELIVERY_TIME', 'Prochaine livraison disponible å {datetime}'); // L'heure choisie a changé.
	define('NEXT_TAKEAWAY_TIME', 'Les commandes à emporter seront à nouveau disponible à partir de {datetime}'); // L'heure choisie a changé.
	
	define('ITEMS', 'Articles');
	define('CHECKOUT', 'Passer à la caisse');
	define('MY_ORDER', 'Mon panier');
	define('EMPTY_BAG', 'Votre panier est vide!');
	define('EDIT', 'Modifier');
	define('ADD', 'Add');
	define('DELETE', 'Supprimer');
	define('DELIVERY_MIN', 'Livraison Minimum');
	define('NO_TAKEAWAY_MIN', 'Pas de minimum pour à emporter!');
	define('HAVE_PROMO', 'Avez-vous un code promo?');
	define('ENTER_PROMO', 'Entrer code promo');
	//define('DELIVERY_ADDRESS_LABEL', 'Veuillez taper votre adresse afin de vous assurer que vous êtes dans la zone de livraison de ce restaurant.');
	define('ENTER_ADDRESS', 'Veuillez saisir votre adresse exacte');
	define('GRAND_TOTAL', 'Montant total:');
	
	define('STREET_ADDRESS', 'Adresse');
	define('BUILDING_NO', 'N˚ d\'immeuble');
	define('CITY', 'Ville');
	define('POSTAL_CODE', 'Code postale');
	define('FLOOR_SUITE', 'Étage / bureau');
	define('ACCESS_CODE', 'Code d\'accès');
	define('SPECIAL_INSTR', 'Instructions de livraison');
	define('SPECIAL_INSTR_HINT', 'Instructions de livraison');
	
	define('PARTNER_LOGIN', 'Se connecter au Portail des partenaires');
	
	define('FOOTER_WHO_WE_ARE', 'Qui sommes-nous?');
	define('FOOTER_ABOUT_US', 'A propos de nous');
	define('FOOTER_NEWS_PRESS', 'Actualités &amp; Presse');
	define('FOOTER_BLOG', 'Blog');
	define('FOOTER_YOUR_BUSINESS', 'Coin partenaires');
	define('FOOTER_BECOME_PARTNER', 'Devenez un restaurant partenaire');
	
	define('FOOTER_CREATE_ACCOUNT', 'Créer un compte d\'entreprise');
	define('FOOTER_CATERING_ORDER', 'Service traiteur disponible');
	define('FOOTER_CONTACT_HELP', 'Contact &amp; Aide');
	define('FOOTER_HELP_FAQ', 'Aide &amp; FAQ');
	define('FOOTER_CONTACT', 'Contactez-Nous');
	define('FOOTER_SUGGEST', 'Suggérer un restaurant');
	define('FOOTER_MORE', 'Plus');
	define('FOOTER_SPECIALS', 'Offres &amp; Promotions');
	
	define('SUCCESSFUL_RECOMMANDATION', 'Merci de votre suggestion.');
	
	define('FOOTER_COPYRIGHT', 'Le meilleur moyen de commander et de se faire livrer des repas et du vin. Tous droits réservés.');
	define('FOOTER_USE_TERMS', 'Conditions d\'utilisations');
	define('FOOTER_PRIVACY', 'Politique de confidentialité');
	define('FOOTER_SITE_BY', 'Site créé par');
	
	define('MORE_MONEY', 'Le minimum n\'est pas atteint' . 
		(@$current_page=='checkout' ? '<br />(OU le restaurant sélectionné ne permet pas les paiements en espèces)' : '') . '.');
	define('CODE_ALREADY_USED', 'Il semblerait que vous ayez déjà utilisé ce code!' . "\n" . 
		'Vous ne pouvez pas utiliser un code promotionnel plus d\'une fois.');
	define('DA_PROMO_CODE', 'Code promotionnel');
	define('PROMO_VALID_FROM', 'Ce code promo est valable {$date}.');
	define('PROMO_VALID_UNTIL', 'Ce code promo est incorrect. Il a expiré {$date}.');
	define('INVALID_PROMO_CODE', 'Code de promotion incorrect!');
	define('LOGIN_FOR_PROMO', 'Veuillez vous connecter ou vous enregistrer avant d\'utiliser un code promo.');
	define('PROMO_USED', 'Ce code promo a déjà été utilisé.');
	define('PROMO_MIN_VALUE', 'Ce code promo est valable uniquement sur les commandes {$value} CHF ou plus.');
	define('INVALID_OR_REGISTERED_PROMO', 'Votre code est incorrect ou destiné à un utilisateur spécifique. Veuillez vous connecter et essayer à nouveau!');
	define('EMAIL_IN_USE', 'Ce nom d\'utilisateur est déjà utilisé');
	
	define('R_EMAIL_CONFIRMATION_SUBJECT', 'Votre compte Venezvite');
	define('R_EMAIL_CONFIRMATION_BODY', 'Bonjour {$firstName},

Ceci est la confirmation que votre compte Venezvite est actif. Félicitations! Les informations relatives à votre compte sont:

 - nom d\'utilisateur: {$username}
 - mot de passe: {$password}

Veuillez garder une copie des ces informations en lieu sûr. Vous pouvez dès maintenant accéder au site et commander le plat de votre choix!

Merci d\'avoir choisi Venezvite!');
	
	define('HELLO_RESTAURANT', 'Chers partenaires et restaurateurs');
	define('HUNGRY_FOR_BUSINESS', 'Vous voulez trouver de nouveaux clients? C\'est simple, rejoignez le réseau de restaurateurs de venezvite.com');
	
	define('HOW_IT_WORKS', 'Comment ça marche?');
	define('HOW_WE_HELP', 'Notre rôle');
	define('JOIN', 'Nous rejoindre');
	
	define('WHO_ARE_WE', 'Qui sommes-nous?');
	define('FOR_YOUR_COMPANY', 'Pour votre entreprise');
	define('CONTACT_HELP', 'Contact &amp; Aide');
	define('SUBMENU_PLUS', 'Plus +');

	define('ORDER_FROM', 'Commander de');
	define('CONTINUE_TO_CHECKOUT', 'Passer à la caisse');
	
	define('NO_FAVORITED_RESTAURANT', 'No Favorited Restaurants');
