<?php
	// FOOTER CONTACT
	define('CONTACT_PANEL_BODY', '<strong class="subtitle">Nous sommes en ligne!</strong>
<p>Envoyez-nous un message instantané!. Nous sommes connectés de 9h00 à minuit tous les jours.</p>
<br />
<strong class="subtitle">Service Client</strong>
<p>Si vous rencontrez un problème lié à la nourriture ou à la livraison, veuillez contacter le restaurant directement.</p> 
<p>Dans le cas de problème relatif à la facturation ou pour toute autre question, envoyez-nous un message instantané ou un email. </p>
<p>email: hello@venezvite.com</p>
<br />
<strong class="subtitle">Assistance pour restaurants partenaires</strong>
<p>Si vous êtes restaurateur et souhaitez parler à un membre de l\'équipe Venezvite<br />
	veuillez nous appeler entre 14h00 et 19h00 (GMT+1)</p>
<p>téléphone: +41 22 575 29 07</p>');
	
	
	// FOOTER RESTAURANT SUGGESTION
	define('SUGGESTION_PANEL_BODY', '<strong class="subtitle">Vous cherchez un restaurant mais il n\'apparaît pas dans notre liste? Dîtes-leur et dîtes-le nous!</strong>
<p>Êtes vous restaurateur? Inscrivez-vous aujourd\'hui et un membre de l\'EEquipe Venezvite vous contactera.</p>');
	define('SUGGESTION_PANEL_NAME', 'Votre nom');
	define('SUGGESTION_PANEL_RESTAURANT', 'Nom de votre restaurant');
	define('SUGGESTION_PANEL_ADDRESS', 'Adresse de votre restaurant');
	define('SUGGESTION_PANEL_COMMENTS', 'Commentaires');
	
	
	// FOOTER SPECIAL OFFERS
	define('SPECIALS_PANEL_BODY', '<strong class="subtitle">Car nous vous aimons aussi!</strong>
<p>Bénéficiez de 7.00 de remise sur votre première commande sur Venezvite.com. Valie même si vous avez déjà commandé sur Venezvite.com en n\'ayant jamis utilisé votre code promo. Uniquement valide une fois.</p>
<p>Promo code: <strong>likevenezvite</strong></p>

<strong class="subtitle">Parlez-en à un ami</strong>
<p>Offrez des plats ou vins à domicile avec venezvite.com. Invitez un ami et il/elle recevra 4.00 de remise. VOus recevrez également 4.00 de remise. Un seul code promo ne peut être utilisé par commande.</p>
<p><strong>Pas de code promo requis.</strong></p>

<strong class="subtitle">Joyeuses Fêtes</em>!</strong>
<p>Bénéficiez de 10% de remise pour les fêtes. L\'offre expire le 1/1/16.</p>
<p>Code promo: <strong>happyholidays2015</strong></p>');
