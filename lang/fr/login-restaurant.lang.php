<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('LR_LOGIN_UNAPPROVED', 'Votre compte n\'a pas encore été approuvé. Soit vous n\'avez pas confirmé votre adresse email, soit notre équipe n\'a pas fini d\'analyser les données fournies.\n' . 
		'Nous vous prions de patienter encore un peu. Toutefois, si vous n\'avez pas eu de nouvelles depuis plusieurs jours n\'hésitez pas à nous contacter.');
	define('LR_LOGIN_ERROR', 'Désolé, nous ne reconnaissons pas les données fournies. Veuillez réessayer et si vous avez oublié votre mot de passe cliquez sur le lien ci-dessous.');
	
	define('LOGIN_ACCOUNT', 'Connectez-vous à votre compte');
	define('USERNAME', 'Nom d\'utilisateur (votre adresse e-mail)');
	define('FORGOT', 'J\'ai oublié');
	
	define('CONFIRM_ORDERS', 'Confirmez les commandes');
	define('CONFIRM_ORDERS_DESC', 'Confirmez les commandes entrantes');
	define('MANAGE_RESTAURANT', 'Gérez votre compte Restaurant');
	define('MANAGE_RESTAURANT_DESC', 'Mettre à jour votre menu et votre compte');
	define('SALES', 'Ventes');
	define('SALES_DESC', 'Analyse de vos ventes');
