<?php
	// SEO settings
	define('SEO_TITLE', 'Historique des commandes');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('VIEW_ORDER', 'Voir la commande');
	
	define('PENDING', 'En attente');
	define('ACCEPTED', 'Acceptée');
	define('DECLINED', 'Refusée');
	
	define('NO_PENDING_ORDERS', 'Vous n\'avez pas de commandes en attente');
	define('VIEW_PAST_ORDERS', 'Voir les commandes passées');
	define('NO_PAST_ORDERS', 'Pas de commandes passées disponibles.');
