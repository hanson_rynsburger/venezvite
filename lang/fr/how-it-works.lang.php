<?php
	// SEO settings
	define('SEO_TITLE', HOW_IT_WORKS);
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('FIND_NEW_CUSTOMERS', 'Accès à une nouvelle clientèle. Augmentation du chiffre d\'affaires. Marketing & publicité gratuite.');
	define('JOIN_PRESENTATION', 'En rejoignant le réseau Venezvite, vous donnez à des milliers de personnes la possibilité de commander chez votre restaurant, aussi bien en ligne qu\'avec leur téléphone mobile.');
	define('JOIN_BUTTON', 'Rejoignez notre réseau');
	
	define('INSTANT_PRESENCE_TITLE', 'Vous êtes référencés en ligne');
	define('INSTANT_PRESENCE_DESC', 'Nous mettons en relation votre restaurant avec des milliers de clients tous les jours. Vos spécialités et l\'ensemble de votre menu sont accessibles en ligne et envoyés directement à nos clients affamés.');
	define('PREPARE_FOOD_TITLE', 'Vous préparez vos plats');
	define('PREPARE_FOOD_DESC', 'Vous recevez chaque commande par email. Vous n\'avez qu\'a préparer la commande et attendre le livreur qui viendra la chercher à l\'heure indiquée.');
	define('GET_PAID_TITLE', 'Vous êtes payés et augmentez vos ventes');
	define('GET_PAID_DESC', 'Venezvite vous paye par virement bancaire pour toutes les commandes réalisées. C\'est aussi simple que ça.');
