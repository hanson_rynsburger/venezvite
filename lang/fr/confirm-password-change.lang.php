<?php
	define('CPC_ADMIN_PANEL', 'J\'ai oublié mon mot de passe');
	
	define('CPC_TITLE', 'Changer le mot de passe de votre compte Venezvite');
	define('CPC_PASSWORD', 'Nouveau mot de passe');
	define('CPC_CONFIRM_PASSWORD', 'Confirmer le nouveau mot de passe');
	define('CPC_UPDATE', 'Mise à jour le mot de passe');
	
	define('CPC_EMAIL_SUBJECT', 'Votre nouveau mot de passe Venezvite');
	define('CPC_EMAIL_BODY', 'Cher {$userName},

Suite à votre demande, voici votre nouveau mot de passe Venezvite :

<strong>{$password}</strong>

Vous pouvez vous servir de ce mot de passe afin d\'accéder à votre compte.');
	
	define('CPC_SUCCESS', 'Vous avez confirmé votre demande de modification de mot de passe Venezvite.' . "\n" . 
		'Veuillez consulter votre compte email pour prendre connaissance de votre nouveau mot de passe.');
	define('CPC_ERROR', 'Désolé, nous n\'avons pas réussi à vous transmettre votre nouveau mot de passe par email. Votre mot de passe reste inchangé.\n' . 
		'Veuillez nous confirmer votre demande de modification de mot de passe ou contactez-nous pour nous signaler ce problème.');
