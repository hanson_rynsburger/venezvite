﻿<?php
	define('ACA_EMAIL_UNCONFIRMED', 'You cannot approve this corporate account, since it hasn\'t confirmed its email address yet!');
	define('ACA_SUCCESS', 'Corporate account successfully approved!' . "\n" . 
		'Its representative has just received an email confirmation, containing the path they should use for managing their corporate account\'s details.');
	define('ACA_FAILURE', 'Unfortunately the corporate account approval confirmation email message couldn\'t be sent.' . "\n" . 
		'Please try again, or contact the system administrator to report this issue!' . "\n\n" . 
		'The corporate account hasn\'t been activated.');
	
	define('ACA_EMAIL_CONFIRMATION_SUBJECT', 'Votre compte Venezvite a été accepté');
	define('ACA_EMAIL_CONFIRMATION_BODY', 'Bonjour {$companyName},

Votre compte Venezvite a été accepté. Félicitations!

Tout ce qu\'il vous reste à faire est de vous connecter à Venezvite à l\'aide des informations utilisées lors du processus d\'enregistrement, et commencer à commander vos plats!

Bienvenue sur Venezvite!');
