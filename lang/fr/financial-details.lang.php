<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('EDIT_DETAILS', 'Modifier les informations bancaires');
	define('BUSINESS_NAME', 'Nom de compte / l\'entreprise');
	define('IBAN', 'Numéro IBAN');
	define('BANK', 'Nom de votre banque');
	define('SWIFT', 'Code SWIFT');
	
	define('COMMISSION_AGREEMENT', 'Contrat de commission Venezvite');
	define('COMMISSION_PERCENTAGE', 'Commission prise par Venezvite: {$percentage}% + Frais de livraison');
	define('COMMISSION_PAYMENT', 'Paiement: Le 15ème jour de chaque mois, tant qu\'il y a un minimum de 500 euros.');
	define('THANK_YOU', 'Merci d\'être un partenaire Venezvite.');
	define('QUESTIONS_EMAIL', 'Pour toute question, nous vous prions d\'envoyez-nous un e-mail à hello@venezvite.com');
