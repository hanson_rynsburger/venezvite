<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('TIME_PERIOD', 'Période de temps');
	define('TOTAL_ORDERS', 'Total des commandes');
	define('TOTAL_SALES', 'Total des ventes');
	define('TOTAL_EARNINGS', 'Total Earnings');
	define('ALL_TIME', 'Tout le temps');
	define('THIS_WEEK', 'Cette semaine');
	define('THIS_MONTH', 'Ce mois');
	define('PAST_MONTH', 'Mois passé');
	define('THIS_YEAR', 'Cette année');
	
	define('NEW_ORDERS', 'Nouvelles commandes');
	define('PAST_ORDERS', 'Commandes passées');
	
	define('RECEIVED_DATE', 'Reçu<br />' . 
		'<span>date / heure</span>');
	define('RECEIVED_FROM', 'Reçu de');
	define('ORDER_TYPE', 'Type de commande');
	define('ORDER_AMOUNT', 'Montant');
	define('ORDER_REQUIRED', 'Quand');
	define('ORDER_STATUS', 'Statut');
	
	define('PENDING', 'En attente');
	define('ACCEPT_ORDER', 'Accepter');
	define('DECLINE_ORDER', 'Refuser');
	define('CONFIRM_ACCEPT', 'Etes-vous sûr de vouloir ACCEPTER cette commande?');
	define('CONFIRM_DECLINE', 'Etes-vous sûr de vouloir DECLINER cette commande?');
	define('ACCEPTED', 'Accepté');
	define('DECLINED', 'Refusé');
	
	define('NO_PENDING_ORDERS', 'Vous n\'avez pas de commandes en attente');
	define('VIEW_PAST_ORDERS', 'Voir vos commandes passées');
	define('NO_PAST_ORDERS', 'Pas de commandes passées disponibles.');
