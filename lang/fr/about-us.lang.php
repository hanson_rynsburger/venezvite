<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ABOUT_US_TITLE', 'A propos de nous');
	define('ABOUT_US_P1', 'Venezvite est le meilleur moyen pour commander à manger à emporter ou à livrer en ligne. Inutile de faire la queue, pas d\'erreurs de commande, inutile de se répéter. Notre service est sécurisé, facile à utiliser et rapide.');
	define('ABOUT_US_P2', 'Venezvite est une plateforme de marketing qui relie les meilleurs restaurants aux clients qui ont faim! Avez-vous faim? Passez commande! Vous souhaitez partager votre cuisine avec de nouveaux clients? Rejoignez notre réseau de restaurateurs maintenant.');
	
	define('MEET_TEAM_TITLE', 'Rencontrez l\'équipe');
	define('JOIN_TEAM', 'Rejoignez l\'équipe!');
	
	define('MEET_DELIVERY_TEAM_TITLE', 'Rencontrez l\'équipe de livraison');
	define('VP_MEMBER', 'Membre de l\'équipe ' . VITE);
