<?php
	// SEO settings
	define('SEO_TITLE', 'Les meilleurs restaurants - livraison à Domicile Genève | Venezvite');
	define('SEO_KEYWORDS', ' Un service de livraison à domicile å genève, livraison à domicile genève, venez vite, venezvite, livraison genève, livraison domicile Genève, pad thai Genève, pizzeria, pizza livré, livraison à domicile genève, livraison a domicile, livraison genève, repas à domicile genève, repas livré');
	define('SEO_DESCRIPTION', 'Une liste des meilleurs restaurants disponibles pour la livraison a domicile genève');
	
	
	define('FOR_', 'Pour?');
	define('WHERE_ARE_U', 'Où êtes vous?');
	define('SEARCH_HINT', 'Plats, Cuisines, noms de restaurant');
	define('SORT_HINT', '<strong>Trier / Filtrer par</strong> Prix, Cuisine ...');
	define('SORT_BY', 'Trier par');
	define('FILTER_BY', 'Filtrer par');
	define('DISTANCE', 'Distance');
	define('ALPHABETICALLY', 'Par ordre alphabétique');
	define('CUISINES', 'Types de cuisine');
	define('RATINGS', 'Notes');
	define('ANY_RATING', 'Toute notation');
	define('PRICE', 'Prix');
	define('ANY_PRICE', 'Tous les prix');
	define('CLEAR', 'Effacer');
	define('CLEAR_ALL_SORT_FILTER', 'Tout effacer');
	define('SEARCH_CUISINES', 'Rechercher par types de cuisine');
	define('CLEAR_ALL', 'Tout effacer');
	define('SHOW_MORE', '+ voir plus');
	define('SHOW_LESS', '- voir moins');
	define('AND_UP', '&amp; plus');
	define('AND_LESS', '&amp; moins');
	define('LIST_IS_FILTERED', 'Votre liste est filtrée.');
	
	define('_AT', ' ');
	define('OPEN_RESTAURANTS_TITLE', '<span>{x}</span> restaurants ouverts sont disponibles pour: <span class="red">{datetime}</span>');
	define('SEE_MORE_RESTAURANTS', '<a id="see-advanced-orders" href="javascript:;">Cliquer ici pour découvrir</a> {x} autres restaurants dans votre zone de livraison mais qui ne livrent pas à l’heure sélectionnée.');
	define('ORDERING_HOURS', 'Commandez entre');
	define('DELIVERED_IN', 'Livré en: {$time_interval} minutes');
	define('ORDER', 'Commander');
	
	define('VIEW_MAP', 'Sur une carte');
	define('HIDE_MAP', 'Cacher la carte');
	
	define('ADV_ORDERS_TITLE', 'Commandes pour plus tard');
	define('ADV_ORDERS_DESC', '{x} autres restaurants sont disponibles dans votre zone de livraison mais pas à l\'heure sélectionnée.');
	define('REOPENS', 'Réouvre');
	define('NEXT_DELIVERY', 'Prochaine Livraison');
	define('ACCEPTS_CASH', 'Accepte les espèces');
	define('PRE_ORDER', 'Pré-commander');
	
	define('NO_DELIVERY', 'L\'adresse saisie se situe en dehors des zones de livraison du restaurant. Veuillez voir la liste des restaurants disponibles ci-dessous.');
	define('CLOSED_ALL_WEEK', 'Ce restaurant est fermé toute la semaine. Veuillez choisir un nouveau restaurant dans la liste ci-dessous.');
	
	
	define('NO_MATCHING_RESTAURANT', 'Désolé, aucun restaurant ne correspond à vos critères...');
	define('MODIFY_SEARCH', 'Veuillez modifiez votre recherche, ou recommandez un restaurant que vous souhaiteriez retrouver sur Venezvite.');
	define('RECOMMEND_RESTAURANT', 'Avez-vous un restaurant à nous recommander?');
	define('YOUR_EMAIL', 'Votre email');
	define('RESTAURANT_NAME', 'Le restaurant');
	
	define('CITIES_WERE_IN', 'Voici les villes où nous sommes');
