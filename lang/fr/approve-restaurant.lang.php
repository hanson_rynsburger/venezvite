<?php
	define('AR_EMAIL_UNCONFIRMED', 'Vous ne pouvez pas approuver le compte de ce restaurant car l\'adresse email n\'a pas encore été confirmée.');
	define('AR_SUCCESS', 'Bravo, ce restaurant est maintenant approuvé!' . "\n" . 
		'Son représentant vient de recevoir un email de confirmation contenant le lien qu\'il doit utiliser pour gérer ses données.');
	define('AR_FAILURE', 'Malheureusement l\'email d\'approbation du restaurant n\'a pas pu être envoyé.' . "\n" . 
		'Veuillez essayer à nouveau, ou contactez l\'administrateur pour signaler ce problème!' . "\n\n" . 
		'Le compte du restaurant n\'a pas été activé');
	
	define('AR_EMAIL_CONFIRMATION_SUBJECT', 'Votre compte Venezvite a été accepté');
	define('AR_EMAIL_CONFIRMATION_BODY', 'Bonjour {$restaurantName},

Votre compte Venezvite a été accepté. Félicitations!

Vous pouvez maintenant compléter votre profil en mettant en ligne le détail de votre menu. Il vous suffit d\'accéder au <a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html</a> en fournissant les détails que vous avez choisi lors de l\'enregistrement.

Bienvenue sur Venezvite!');
