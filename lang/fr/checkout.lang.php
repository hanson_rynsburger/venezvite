<?php
	// SEO settings
	define('SEO_TITLE', 'Créer un compte ou vous connecter - Livraison à Domicile Genève | Venezvite');
	define('SEO_KEYWORDS', ' Une service de livraison à domicile à Genève, livraison à domicile Genève, venez vite, venezvite, livraison Genève, livraison domicile Genève, pad thai Genève, livraison à domicile Genève, livraison a domicile, livraison Genève, repas à domivile genève, repas livré');
	define('SEO_DESCRIPTION', 'Créer un compte pour commander en ligne la livraison à domicile à Genève');
	
	
	define('ORDER_ERRORS', 'Erreur avec votre commande:');
	define('USED_EMAIL', 'Cette adresse e-mail est déjà dans notre système. Avez-vous oublié votre mot de passe? Le récupérer.');
	define('PHONE_MANDATORY', 'Avant de continuer à naviguer sur Venezvite, veuillez nous communiquer votre numéro de téléphone.');
	define('CANT_FIND_ADDRESS', 'Nous ne pouvons pas déterminer l\'emplacement de l\'adresse soumise. Veuillez essayer à nouveau, ou contactez-nous pour signaler ce problème.');
	define('NO_BUILDING_NO', 'Nous ne reconnaissons pas ce numéro d\'immeuble.');
	define('OUTSIDE_ADDRESS', 'L\'adresse saisie est en dehors des zones de livraison du restaurant.');
	define('DIFFERENT_CHARGES_MINIMUM', 'L\'adresse que vous avez entrée a changé les frais de livraison et le minimum requis. Veuillez ajouter des articles à votre commande pour répondre au nouveau minimum requis.');
	define('DIFFERENT_MINIMUM', 'L\'adresse que vous avez entrée a changé le minimum requis. Veuillez ajouter des articles à votre commande pour répondre au nouveau minimum requis.');
	define('DIFFERENT_CHARGES', 'Veuillez noter que la nouvelle adresse que vous avez entrée a changé les frais de livraison. Veuillez revoir puis confirmer votre commande.<br />' . 
		'Attention: utilisez toujours votre adresse exacte pour la recherche des restaurants.');
	define('WRONG_CC_TYPE', 'Votre type de carte de crédit est inconnu ou n\'est pas pris en charge par notre site internet. Veuillez essayer avec une autre carte, ou contactez-nous pour signaler ce problème.');
	define('FILL_FIELDS', 'Veuillez remplir tous les champs nécessaires.');
	
	define('ALMOST_DONE', '<span>Dernière étape</span> Entrez vos informations');
	define('CREATE_ACCOUNT', 'Créer un compte');
	define('ACCOUNT_INFO', 'Vos informations bancaires');
	define('EDIT_MY_INFO', 'Modifier mes informations'); 
	define('ENTER_EMAIL', 'Adresse email');
	define('CREATE_PASS', 'Créer un mot de passe');
	define('NEWSLETTER_ACCEPT', 'Oui, je voudrais recevoir des offres et des notifications lorsque de nouveaux restaurants sont disponibles par email.');
	
	define('DELIVERY_DETAILS', 'Détails de Livraison');
	define('FOR_DELIVERY', 'Livraison à domicile');
	define('CHANGE_TAKEAWAY', 'Changer à emporter');
	define('FOR_TAKEAWAY', 'À emporter');
	define('CHANGE_DELIVERY', 'Changer en livraison à domicile');
	
	define('ENTER_NEW_ADDRESS', 'Entrer une nouvelle adresse');
	define('EDIT_ADDRESS', 'Modifier cette adresse');
	define('SAVE_ADDRESS', 'Sauvegarder vos informations.');
	define('TAKEAWAY_DESC', 'Vous êtes en train de passer une commande à emporter, vous pourrez récupéper votre commande directement au restaurant.');
	
	define('PAYMENT_OPTIONS', 'Options de Paiement');
	define('INVOICE', 'Invoice');
	define('CASH', 'Espèces');
	define('CREDIT', 'Crédit');
	define('CARD_NAME', 'Nom sur carte');
	define('CCN', 'Numéro de carte');
	define('EXPIRATION_DATE', 'Date d\'expiration');
	define('CURRENCY', 'Devise (' . OPTIONAL . ')');
	define('USE_NEW_CARD', 'Nouvelle carte de crédit');
	
	define('TOO_MUCH_CASH', 'Vous ne pouvez pas passer de commandes en espèces si le montant total dépasse ' . MAX_CASH_VALUE . ' {$currency}.');
	define('UNPAID_CASH', 'Vous avez une commande en espèces non reglée. Cette option de paiement restera indisponible jusqu\'à ce que le paiement soit reçu.<br />' . 
		'Veuillez continuer votre commande avec une carte de crédit.');
	define('UNAPPROVED_CASH', 'Votre commande précédente en espèces n\'a pas été confirmée. Veuillez attendre que votre commande soit confirmée ou continuez avec une carte de crédit.');
	define('NO_CASH', 'Ce restaurant n\'accepte pas de paiements en espèces.');
	define('CASH_PAYMENT_DESC', 'Payer en espèces à la livraison.<br />
(Le livreur de ce restaurant n\'accepte que les paiement en espèces.)');
	define('INVOICING_DESC', 'If your Corporate Account is approved for ordering food on Venezvite.com without paying online in advance (but receiving an invoice to be paid at a later time), you can use this option to place your order.' . "\n\n" . 
        'Be warned that, in case your account is NOT approved for such an option, your order will be ignored!' . "\n\n" . 
        'For details about how your account could become approved for invoice ordering, please <a class="popup" href="' . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/contact.html">contact us</a>!');
	
	define('AGREE_TERMS', 'J\'accepte les <a class="popup" href="tou-customers.html">Conditions d\'utilisation</a>.');
	define('SAVE_INFO', 'Sauvegardez vos informations.');
	define('PLACE_ORDER_X', 'Passer ma commande ({x})');
	
	
	
	define('P_ORDER_EMAIL_TITLE', 'Nouvelle Commande Venezvite');
	define('P_ORDER_EMAIL_ORDER_TYPE', 'Type de Commande');
	define('P_ORDER_EMAIL_DELIVERY', 'Livraison');
	define('P_ORDER_EMAIL_TAKEAWAY', 'A Emporter');
	define('P_ORDER_EMAIL_ORDER_NO', 'Numéro de commande');
	define('P_ORDER_EMAIL_DELIVERY_DATE_HOUR', 'Date et heure de livraison');
	define('P_ORDER_EMAIL_RESTAURANT', 'Restaurant');
	define('P_ORDER_EMAIL_PHONE', 'Téléphone');
	define('P_ORDER_EMAIL_ORDER', 'Commande');
	define('P_ORDER_EMAIL_PAYMENT_DONE', 'Paiment: DÉJA PAYÉE');
	define('P_ORDER_EMAIL_PAYMENT_TBD', 'Paiment: À EFFECTUER');
	define('P_ORDER_EMAIL_TOTAL', 'Total Produits');
	define('P_ORDER_EMAIL_ADDRESS', 'Adresse');
	define('P_ORDER_EMAIL_LAST_NAME', 'Nom');
	define('P_ORDER_EMAIL_FIRST_NAME', 'Prénom');
	define('P_ORDER_EMAIL_COMPANY', 'Société');
	define('P_ORDER_EMAIL_STREET', 'Rue');
	define('P_ORDER_EMAIL_ZIP', 'Code Postal');
	define('P_ORDER_EMAIL_CITY', 'Ville');
	define('P_ORDER_EMAIL_FLOOR_CODE', 'Etage et code d\'entrée');
	define('P_ORDER_EMAIL_NOTES', 'Notes');
	define('P_ORDER_EMAIL_CASH_ON_DELIVERY', 'ESPÈCES À LA LIVRAISON');
	define('P_ORDER_EMAIL_ORDER_READY', 'La commande doit être prête le <span style="color:#c00">{$datetime}</span>');
	define('P_ORDER_EMAIL_ACTIVATION_LINK', 'Lien pour accepter la commande:');
	define('P_ORDER_EMAIL_DELIVERY_DELAY', 'Délai de livraison estimé: {$time} <span style="color:#c00">+/- 15 minutes</span>');
	define('P_ORDER_EMAIL_TAKEAWAY_TIME', 'Récupérer la commmande à : {$time}');
	
	define('VIEW_IMAGES', 'Voir les images');
