<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('HUNGRY_TITLE', 'Commandez. <span>Vous êtes livrés.</span>');
	define('ALREADY_MEMBER', 'Déja membre? Se connecter');
	define('NOW_ACCEPTING', 'Désormais nous acceptons');
	
	define('EASY_ORDERING_TITLE', 'Plus Facile');
	define('EASY_ORDERING_BODY', 'Plus besoin d\'appels téléphoniques! Un moyen rapide et efficace.');
	define('MORE_FOOD_TITLE', 'Bien plus que des plats!');
	define('MORE_FOOD_BODY', 'Vous trouverez également de la bière, du vin, des liqueurs, et bien plus encore…');
	define('DISCOUNTS_TITLE', 'Offres et Promotions');
	define('DISCOUNTS_BODY', 'Avec nos offres exclusives, vous économiserez au quotidien.');
	define('TELL_FRIENDS_TITLE', 'Parlez-en à vos amis');
	define('TELL_FRIENDS_BODY', 'Recommandez Venezvite.com à vos amis et recevez 4 CHF.');
	
	define('DONT_BE_SHY', 'Le choix des connaisseurs');
	define('CLICKS_AWAY', '4 simples clics pour assouvir votre faim');
	
	define('BROWSE_CITY', 'Trier par ville:');
	
	define('PERFECT_BUSINESS', 'Parfait pour
les déjeuners d\'affaires.');
	define('CREATE_CORP_ACCOUNT', 'Créer un compte d\'entreprise pour les commandes groupées.');
	define('CREATE_CORP_ACCOUNT2', 'Créer un compte d\'entreprise');
	define('LEARN_MORE', 'En savoir plus');

	define('BEST_RESTAURANT', 'Les meilleurs restaurants sont ici');
	
	define('STAY_CONNECTED', 'Restez Connecté.');
	define('NEWSLETTER_TITLE', 'Inscrivez-vous à notre mailing list.');
	define('NEWSLETTER_BODY', 'Entrez votre adresse email et vous recevrez des offres et des promotions.');
	define('ENTER_EMAIL', 'Entrez votre adresse e-mail');
	
	define('HOW_WORKS_TITLE', 'Comment ça marche');
	define('HOW_WORKS_BODY1', 'Placez votre commande');
	define('HOW_WORKS_BODY2', 'Le restaurant reçoit les informations concernant votre commande');
	define('HOW_WORKS_BODY3', 'Les plats vous sont livrés à domicile.');
	
	define('HELLO_RESTAURANT_OWNERS', 'Bonjour propriétaires de restaurants!
Devenez aujourd\'hui un restaurant partenaire.');
	define('JOIN_TITLE', 'Rejoignez notre réseau de restaurants');
	define('JOIN_BODY', 'Devenez un restaurant partenaire. Découvrez comment nous pouvons vous aider à augmenter votre revenu et trouver de nouveaux clients.');
	define('NO_DELIVER_TITLE', 'Vous ne livrez pas à domicile? Pas de soucis!');
	define('NO_DELIVER_BODY', 'Nos partenaires de livraison livreront vos plats pour vous. Vous n\'avez qu\'à préparer vos commandes et les livreurs s\'occuperont du reste.');
	define('JOIN_NOW', 'Rejoignez-nous');
	
	define('ABOUT_TITLE', 'À propos de nous');
	define('ABOUT_BODY', 'Venezvite est le moyen le plus efficace et le plus rapide de se faire livrer des plats ou boissons de vos restaurants préférés, à votre domicile ou sur votre lieu de travail.');
