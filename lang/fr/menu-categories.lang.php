<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ADD_CATEGORY', 'Insérer une catégorie de menu');
	define('EDIT_ITEM', 'Mettre à jour');
	define('ADD_CATEGORY_BUTTON', 'Ajouter une catégorie');
	define('EDIT_ITEM_BUTTON', 'Mettre à jour');
	
	define('DUPLICATE_ALERT', 'Il semblerait que la catégorie que vous essayez de créer existe déjà.');
	define('DISABLED_ALERT', 'Les produits suivants sont liés à la catégorie sélectionnée:\n\n' . 
		'{used_menu_items}\n\n' . 
		'Afin de les enlever, veuillez les lier à une autre catégorie, ou les supprimer.');
	define('REMOVE_CONFIRMATION', 'Êtes-vous sûr(e) de vouloir supprimer cette catégorie?');
	define('NO_MENU_CATEGORIES', 'Vous n\'avez pas de catégorie(s) de menu définie(s).');
