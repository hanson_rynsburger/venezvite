<?php
	// SEO settings
	//define('SEO_TITLE', '');
	//define('SEO_KEYWORDS', '');
	//define('SEO_DESCRIPTION', '');
	
	
	define('WHEN_WHERE_DESC', 'Pour s\'assurer que le restaurant vous livre, veuillez entrer votre adresse exacte et l\'heure désirée.');
	
	define('RETURN2LIST', 'Retour à la liste de restaurants');
	define('ADD_TO_FAVES', 'Ajouter aux favoris');
	define('ADDED_TO_FAVES', 'Favorited');
	define('VERY_POPULAR_RESTAURANT', 'Un restaurant très populaire');
	define('POPULAR_RESTAURANT', 'Un restaurant populaire');
	define('OPEN', 'Ouvert');
	define('CLOSED', 'Fermé');
	define('CLOSES_IN', 'Ferme dans {x} minutes');
	
	define('MORE_INFO', 'Plus d\'informations');
	
	define('VISIT_WEB', 'Visiter le site web');
	define('SERVING_IN', 'Servir les clients à');
	define('HOURS_DELIVERY_ZONES', 'Les Heures &amp; Zones de livraison');
	define('TAKEAWAY_HOURS', 'Heures pour la vente à emporter');
	define('DELIVERY_HOURS', 'Heures de Livraison');
	define('NO_DELIVERY', 'Pas de livraison');
	define('DELIVERY_ZONES', 'Zones de Livraison');
	define('ABOUT', 'À Propos du Restaurant');
	
	define('SEARCH_HINT', 'Rechercher un élément du menu');
	define('FILTER', 'Filtrer');
	define('FILTER_PHOTOS', 'Photos');
	
	define('ADD_ITEM', 'Ajouter');
	define('SHOW_IMAGE', 'Agrandir l\'image');
	define('HIDE_IMAGE', 'Réduire l\'image');
	define('VIEW_DETAILS', 'Voir les détails');
	define('ADD2BAG', 'Ajouter à mon panier');
	define('SPECIAL_INSTRUCTIONS', 'Notes / Instructions supplémentaires');
	define('SPECIAL_INSTRUCTIONS_EG', '(par exemple: sauce supplémentaire s\'il vous plaît)');
	define('EDIT_ITEM', 'Modifier');
	define('UPDATE_BAG', 'Mettre à jour');
	define('OPTIONS_EXACTLY', 'Sélectionnez exactement {x} option(s)');
	define('OPTIONS_AT_LEAST', 'Sélectionnez au moins {x} option(s)');
	define('OPTIONS_UP_TO', 'Sélectionnez jusqu\'à {x} option(s)');
	
	define('ITEM_ADDED', 'Article ajouté au panier');
	define('GO2CHECKOUT', 'Passer la commande');
	
	define('NO_MENUS_FOR_SEARCH', 'Aucun élément du menu ne correspond à vos critères de recherche.');
	
	define('REVIEWS_TITLE', 'Avis Publiés');
	define('WRITE_REVIEW', 'Publier un avis');
	define('LAST_ORDER', 'Dernière commandé: ');
	
	define('DELIVERY_SCHEDULE', 'Heures de Livraison');
	define('VIEW_IMAGES', 'Voir les images');
	
	define('DELIVERED_ON_TIME', 'Was your delivery on time?');
	define('ORDER_CORRECT', 'Était votre livraison à l\'heure?');
	define('FOOD_GOOD', 'Votre repas était délicieux?');
	define('SKIP_QUESTION', 'Passez cette question');

	define('OVERALL_EXPERIENCE', 'Évaluez votre expérience entière');
	define('REVIEW_YOUR_ORDER', 'Voulez-vous écrire un commentaire de votre commande?');
	define('GUIDELINE', 'Directives pour les avis');
	define('REMAINING', 'caractères restants. Minimum 3 mots');
	define('SUBMIT_REVIEW', 'Soumettre');
	
	define('NO_REVIEWS', 'Aucun avis' );
	
	define('THANK_YOU_REVIEW', 'Nous vous remercions de soumettre votre commentaire sur ce restaurant');
	define('CLOSE_MODAL', 'Fermer');
