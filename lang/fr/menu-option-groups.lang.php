<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ADD_OPTIONS_GROUP', 'Insérer un groupe d\'options');
	define('EDIT_ITEM', 'Mettre à jour');
	define('ADD_OPTIONS_GROUP_BUTTON', 'Ajouter un groupe d\'options');
	define('EDIT_ITEM_BUTTON', 'Mettre à jour');
	
	define('OPTIONS_GROUP_NAME', 'Nom du groupe d\'options (ex: salades)');
	define('HOW_MANY', 'Combien d\'articles?');
	define('EXACTLY', 'exactement');
	define('MAX_OF', 'au maximum');
	define('MIN_OF', 'au moins');
	
	define('ADD_MENU_OPTIONS', 'Ajouter des options de menu');
	define('NEW_MENU_OPTION', '... ou ajouter une nouvelle option de menu');
	define('MENU_OPTION_NAME', 'Nom de l\'option de menu');
	define('PRICE_CURRENCY', 'Prix ({$currency})');
	define('SAVE', 'Sauvegarder');
	define('OPTION_DELETE_CONFIRMATION', 'Êtes-vous sûr de vouloir supprimer cette option?');
	
	define('DISABLED_ALERT', 'Les options sélectionnées sont déjà liées à la liste d\'éléments suivante:\n\n' . 
		'{used_menu_items}\n\n' . 
		'Afin de les enlever, veuillez supprimer tout lien à d\'autres éléments du menu.');
	define('REMOVE_CONFIRMATION', 'Êtes-vous sûr(e) de vouloir supprimer ce groupe d\'options?');
	define('NO_MENU_OPTION_GROUPS', 'Vous n\'avez pas de groupe d\'options de menu définie(s).');
