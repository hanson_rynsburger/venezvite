<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('RESTAURANT_DETAILS', 'Détails sur le restaurant');
	define('RESTAURANT_DESC', 'Description du restaurant');
	define('EDIT_RESTAURANT', 'Modifier les détails du restaurant');
	define('RESTAURANT_NAME', 'Nom du restaurant');
	define('ADDRESS', 'Adresse');
	define('PHONE', 'Téléphone');
	define('MOBILE', 'Mobile');
	define('WEBSITE', 'Site Internet');
	define('RESTAURANT_INFO', 'Informations sur le restaurant');
	
	define('ADMIN_DETAILS', 'Détails admin');
	define('NOTICE_EMAIL', 'Notifications email');
	define('EDIT_ADMIN_DETAILS', 'Modifier admin');
	define('LOGIN_EMAIL', 'Email pour la connexion');
	define('NEW_PASS', 'Nouveau mot de passe');
	define('CONFIRM_PASS', 'Confirmez le mot de passe');
	
	define('HOLIDAYS', 'Holidays / Vacations');
	define('NO_HOLIDAYS', 'No holidays or vacations');
	
	define('EDIT_HOLIDAYS', 'Edit Holidays / Vacations');
	
	define('HOURS_OPERATIONS', 'Horaires d\'ouverture');
	define('CLOSED', 'Fermé');
	define('EDIT_HOURS_OPERATIONS', 'Modifier les horaires d\'ouverture');
	define('MAIN_INTERVAL', 'Horaires de service principales');
	define('SECONDARY_INTERVAL', 'Horaires de service secondaires');
	
	define('DELIVERY_ZONES', 'Zone de livraison');
	define('ZONE', 'Zone');
	define('MINIMUM', 'minimum');
	define('FEE', 'Frais');
	define('MINUTES', 'minutes');
	define('DELIVERY_RANGE', 'Surface de livraison (km)');
	define('DELIVERY_FEE2', 'Frais de livraison ($currency)');
	define('MIN_DELIVERY', 'Minimum pour livraison ($currency)');
	define('ESTIMATED_TIME', 'Durée estimée (min.)');
	define('ZONE_UPDATE', 'Mise à jour');
	define('ZONE_ADD', 'Ajouter');
	define('CONFIRM_DELIVERY_ZONE', 'Êtes-vous sûr(e) de vouloir supprimer la zone de livraison actuelle?');
