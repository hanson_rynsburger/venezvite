<?php
	// SEO settings
	define('SEO_TITLE', ''); 
	define('SEO_KEYWORDS', ''); 
	define('SEO_DESCRIPTION', ''); 
	
	
	define('HUNGRY_TITLE', 'Hungrig? <span>Reihenfolge</span>'); 
	define('ALREADY_MEMBER', 'Schon Mitglied? Loggen Sie sich ein.'); 
	define('NOW_ACCEPTING', 'Wir nehmen');
	
	define('EASY_ORDERING_TITLE', 'Noch einfacher'); 
	define('EASY_ORDERING_BODY', 'Kein Telefonieren mehr! Schnell und leistungsfähig'); 
	define('MORE_FOOD_TITLE', 'Weit mehr als nur essen!'); 
	define('MORE_FOOD_BODY', 'Sie finden auch Biere, Weine, Liköre und viel mehr.'); 
	define('DISCOUNTS_TITLE', 'Angebote und Sonderangebote'); 
	define('DISCOUNTS_BODY', 'Mit unseren Exclusivangeboten können Sie täglich sparen.');
	define('TELL_FRIENDS_TITLE', 'Reden Sie mit Ihren Freunden über uns.'); 
	define('TELL_FRIENDS_BODY', 'Empfehlen Sie Venezvite.com weiter und gewinnen 4 CHF.');
	
	define('DONT_BE_SHY', 'Scheuen Sie sich nicht und bestellen online!'); 
	define('CLICKS_AWAY', '4 einfache Mausklicke um Ihren Hunger zu stillen!'); 
	
	define('BROWSE_CITY', 'Sortieren Sie nach Städten:'); 
	
	define('PERFECT_BUSINESS', 'Perfekt für
Geschâftsessen.'); 
	define('CREATE_CORP_ACCOUNT', 'Erstellen Sie ein Geschäftskonto für Gruppenbestellungen.'); 
	define('CREATE_CORP_ACCOUNT2', 'Create a corporate account');
	define('LEARN_MORE', 'Mehr darüber'); 
	
	define('BEST_RESTAURANT', 'Die besten Restaurants sind hier');
	
	define('STAY_CONNECTED', 'Bleiben Sie online'); 
	define('NEWSLETTER_TITLE', 'Schreiben Sie sich in unsere Mailingliste ein.'); 
	define('NEWSLETTER_BODY', 'Geben Sie Ihre Emailaderesse an und Sie werden Angebote und Sonderangebote erhalten.'); 
	define('ENTER_EMAIL', 'Geben Sie Ihre Emailadresse ein'); 
	
	define('HOW_WORKS_TITLE', 'Wie es funktioniert?'); 
	define('HOW_WORKS_BODY1', 'Bestellenn Sie'); 
	define('HOW_WORKS_BODY2', 'Das Restaurant wird die Einzelheiten Ihrer Bestellung erhalten'); 
	define('HOW_WORKS_BODY3', 'Die Gerichte werden zu Ihnen nach Hause geliefert!'); 
	
	define('HELLO_RESTAURANT_OWNERS', 'Guten Tag Restaurantbesitzer,
Werden Sie heute ein Partnerrestaurant!'); 
	define('JOIN_TITLE', 'Treten Sie unserem Netz der Restaurants bei.'); 
	define('JOIN_BODY', 'Entdecken Sie, wie Sie durch unsere Hilfe Ihr Einkommmen verbessern können und neue Kundschaft finden.'); 
	define('NO_DELIVER_TITLE', 'Sie liefern nicht nach Hause? Kein Problem!'); 
	define('NO_DELIVER_BODY', 'Unsere Lieferanten beliefern Ihre Kunden für Sie. Bereiten Sie das Gericht zu und der Lieferant kümmert sich um den Rest.'); 
	define('JOIN_NOW', 'Kommen Sie zu uns');
	
	define('ABOUT_TITLE', 'Über uns');
	define('ABOUT_BODY', 'Venezvite ist der beste, leistungsfähigste und schnellstes Weg Gerichte und passende Getränke Ihres Lieblingsrestaurants zu Ihnen nach Hause oder zur Arbeit liefern zu lassen.'); 
