<?php
	// SEO settings
	define('SEO_TITLE', 'Die beliebtesten Restaurants - Lieferung nach Hause in Genf | Venezvite'); 
	define('SEO_KEYWORDS', ' Ein Lieferservice nach Hause Genf, Lieferung nach Hause Genf, Lieferung, venezvite, Lieferung Genf, Gerichte geliefert, Gerichte Lieferservice, Pizzaservice, Pizzeria, Pizza Lieferung, Essen liefern, Essen Lieferung, Essen nach Hause'); 
	define('SEO_DESCRIPTION', 'Liste der besten Restaurants die in Genf liefern!');
	
	 
	define('FOR_', ' Für');
	define('WHERE_ARE_U', 'Wo befinden Sie sich?'); 
	define('SEARCH_HINT', 'Gerichte, Küche, Name von Restaurants'); 
	define('SORT_HINT', '<strong> Sortieren / Filtern nach</strong> Preis, Küche ...');
	define('SORT_BY', 'Sortieren nach'); 
	define('FILTER_BY', 'Filtern nach'); 
	define('DISTANCE', 'Abstand');
	define('ALPHABETICALLY', 'Alphabetisch'); 
	define('CUISINES', 'Küche');
	define('RATINGS', 'Bewertungen'); 
	define('ANY_RATING', 'Alle Bewertungen');
	define('PRICE', 'Preis'); 
	define('ANY_PRICE', 'Alle Preise'); 
	define('CLEAR', 'Löschen'); 
	define('CLEAR_ALL_SORT_FILTER', 'Alles Löschen'); 
	define('SEARCH_CUISINES', 'Suche nach Küche');
	define('CLEAR_ALL', 'Alles Löschen'); 
	define('SHOW_MORE', '+ Mehr sehen'); 
	define('SHOW_LESS', '- Weniger sehen'); 
	define('AND_UP', '&amp; mehr'); 
	define('AND_LESS', '&amp; weniger');
	define('LIST_IS_FILTERED', 'Ihre Liste ist gefiltert.'); 
	
	define('_AT', ' '); 
	define('OPEN_RESTAURANTS_TITLE', '<span>{x}</span> Geöffnete zur Verfügung stehende Restaurants <span class="red">{datetime}</span>'); 
	define('SEE_MORE_RESTAURANTS', '<a id="see-advanced-orders" href="javascript:;">Entdecken Sie hier</a> {x} andere geöffnete Restaurants die nicht zur ausgewählten Zeit beliefern.'); 
	define('ORDERING_HOURS', 'Bestellzeiten'); 
	define('DELIVERED_IN', 'Geliefert in: {$time_interval} Minuten'); 
	define('ORDER', 'Bestellen'); 
	
	define('VIEW_MAP', 'Landkarten ansehen'); 
	define('HIDE_MAP', 'Landkarte schliessen'); 
	
	define('ADV_ORDERS_TITLE', 'Später bestellen');
	define('ADV_ORDERS_DESC', '{x} andere Restaurants in Ihrer Lieferzone sind verfügbar aber nicht im ausgewählten Zeitraum.');
	define('REOPENS', 'Öffnet wieder um'); 
	define('NEXT_DELIVERY', 'Nächste Lieferung');
	define('ACCEPTS_CASH', 'Barzahlung akzeptiert'); 
	define('PRE_ORDER', 'Vorbestellung'); 
	
	define('NO_DELIVERY', 'Adresse befindet sich ausserhalb der Lieferzonde des Restaurants. Bitte schauen Sie auf die Liste der möglichen Restaurants.');
	define('CLOSED_ALL_WEEK', 'Dieses Restaurant ist die ganze Woche geschlossen. Suchen Sie ein anderes Restaurant aus.');
	
	
	define('NO_MATCHING_RESTAURANT', 'Tut uns leid, es gibt kein Restaurant das Ihren Kriterien entspricht...'); 
	define('MODIFY_SEARCH', 'Bitte ändern Sie Ihre Suche oder empfehlen Sie uns ein Restaurant, das Sie gerne auf der Venezvite-Liste hätten.'); 
	define('RECOMMEND_RESTAURANT', 'Möchten Sie uns ein Restaurant empfehlen?'); 
	define('YOUR_EMAIL', ' Ihre Emailadresse');
	define('RESTAURANT_NAME', 'Name des Restaurants'); 
	
	define('CITIES_WERE_IN', 'In diesen Städten finden Sie uns bereits.');
