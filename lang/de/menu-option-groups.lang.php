<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ADD_OPTIONS_GROUP', 'Insert a Menu Options Group');
	define('EDIT_ITEM', 'Update Menu Options Group');
	define('ADD_OPTIONS_GROUP_BUTTON', 'Add Menu Options Group');
	define('EDIT_ITEM_BUTTON', 'Update');
	
	define('OPTIONS_GROUP_NAME', 'Options group name (eg. Salads)');
	define('HOW_MANY', 'How many items?');
	define('EXACTLY', 'exactly');
	define('MAX_OF', 'maximum of');
	define('MIN_OF', 'minimum of');
	
	define('ADD_MENU_OPTIONS', 'Add menu options');
	define('NEW_MENU_OPTION', '... or add a new menu option');
	define('MENU_OPTION_NAME', 'Menu option name (eg. side salad)');
	define('PRICE_CURRENCY', 'Price ({$currency})'); 
	define('SAVE', 'Save');
	define('OPTION_DELETE_CONFIRMATION', 'Are you sure you want to remove this option?');
	
	define('DISABLED_ALERT', 'The selected options group is already attached to the following menu items list:\n\n' . 
		'{used_menu_items}\n\n' . 
		'In order to remove it, please disconnect it from any related menu items.');
	define('REMOVE_CONFIRMATION', 'Are you sure you want to remove this options group?');
	define('NO_MENU_OPTION_GROUPS', 'You have no menu option groups defined.');
