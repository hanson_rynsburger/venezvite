﻿<?php
	define('AR_EMAIL_UNCONFIRMED', 'Sie können das Konto dieses Restaurants nicht aktivieren solange dieses nicht seine Emailadresse bestätigt hat.'); 
	define('AR_SUCCESS', 'Restaurant erfolgreich bestätigt.' . "\n" . 
		'Die Verantwortlichen haben eben die Emailbestaetigung erhalten. Die erforderlichen Schritte zur Organisation der Restaurantdetails wurden geklärt.'); 
	define('AR_FAILURE', 'Leider konnte die Email mit den Kontobestätigungen des Restaurants nicht gesendet werden.' . "\n" . 
		'Bitte versuchen Sie es noch einmal, oder wenden Sie sich an den Systemadministrator und leiten den Vorgang weiter.' . "\n\n" . 
		'Das Konto des Restaurants wurde nicht aktiviert.'); 		
	
	define('AR_EMAIL_CONFIRMATION_SUBJECT', 'Ihr Venezvitekonto wurde akzeptiert.'); 
	define('AR_EMAIL_CONFIRMATION_BODY', ' Guten Tag {$restaurantName},			

Glückwunsch! Ihr Venezvitekonto wurde akzeptiert.							 

Sie können jetzt Ihr Profil vervollständigen, indem Sie die Einzelheiten Ihres Menüs herunterladen. Klicken Sie auf <a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html</a> indem Sie die Einzelheiten Ihrer Registrierung angeben. 

Willkommen bei Venezvite!'); 
