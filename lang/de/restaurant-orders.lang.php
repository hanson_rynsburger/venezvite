<?php
	// SEO settings
	define('SEO_TITLE', 'Restaurant Bestellungen');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('TIME_PERIOD', 'Zeitperiode');
	define('TOTAL_ORDERS', 'Gesamtaufträge');
	define('TOTAL_SALES', 'Summe der Verkäufe');
	define('TOTAL_EARNINGS', 'Total Earnings'); 
	define('ALL_TIME', 'Jederzeit');
	define('THIS_WEEK', 'Diese Woche');
	define('THIS_MONTH', 'Diesen Monat');
	define('PAST_MONTH', 'Letzten Monat');
	define('THIS_YEAR', 'Dieses Jahr');
	
	define('NEW_ORDERS', 'Neue Bestellungen');
	define('PAST_ORDERS', 'Vergangene Bestellungen'); 
	
	define('RECEIVED_DATE', 'Erhalten<br />' . 
		'<span>Datum/ Uhrzeit</span>'); 
	define('RECEIVED_FROM', 'Erhalten von'); 
	define('ORDER_TYPE', 'Bestellart'); 
	define('ORDER_AMOUNT', 'Betrag'); 
	define('ORDER_REQUIRED', 'Wann'); 
	define('ORDER_STATUS', 'Status'); 
	
	define('PENDING', 'Noch nicht erledigt'); 
	define('ACCEPT_ORDER', 'Annehmen'); 
	define('DECLINE_ORDER', 'Ablehnen'); 
	define('CONFIRM_ACCEPT', 'Sind Sie sicher, diese Bestellung AUFGEBEN zu wollen?'); 
	define('CONFIRM_DECLINE', 'Sind Sie sicher, diese Bestellung ABLEHNEN zu wollen?'); 
	define('ACCEPTED', 'Angenommen'); 
	define('DECLINED', 'Abgelehnt');
	
	define('NO_PENDING_ORDERS', 'Sie haben keine Bestellung aufgegeben.'); 
	define('VIEW_PAST_ORDERS', 'Ihre vergangenen Bestellungen'); 
	define('NO_PAST_ORDERS', 'Keine vergangenen Bestellungen einzusehen.'); 
