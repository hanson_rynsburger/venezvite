<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ADD_ITEM', 'Add Menu Item');
	define('EDIT_ITEM', 'Edit Menu Item');
	define('SELECT_CATEGORY', 'Select a Category');
	define('NEW_CATEGORY', 'Create a new category');
	define('MENU_ITEM_NAME', 'Menu Item Name');
	define('CHOOSE_PHOTO', 'Choose Photo');
	define('MENU_ITEM_DESCRIPTION', 'Menu Item Description');
	define('PRICE_CURRENCY', 'Price ({$currency})');
	define('DISCOUNT_PERCENT', 'Discount %');
	define('ADD_OPTIONS_SET', 'Select an Option Group');
	define('NEW_OPTIONS_SET', 'Create a new option group');
	define('ENABLED', 'Enabled');
	define('ADD_ITEM_BUTTON', 'Add Item');
	define('EDIT_ITEM_BUTTON', 'Update Item');
	
	define('REMOVE_CONFIRMATION', 'Are you sure you want to remove this item?');
	define('NO_MENU_CATEGORIES', 'You have no menu categories defined.');
