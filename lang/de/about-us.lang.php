<?php
	// SEO settings
	define('SEO_TITLE', ''); 
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ABOUT_US_TITLE', 'Über uns'); 
	define('ABOUT_US_P1', 'Venezvite ist der beste und schnellste Weg um Online Essen zu bestellen - sei es durch Lieferung oder Abholung. Bei uns gibt es weder Warteschlangen noch Missverständnisse. Unser Dienst ist schnell, einfach zu handhaben und dazu noch sicher.');
	define('ABOUT_US_P2', 'Venezvite ist eine Marketingplattform, die hungrige Gäste mit den besten Restaurants verbindet. Möchten Sie etwas essen? Bestellen Sie einfach heute! Sie möchten gerne Ihr Menü mit neuen Kunden teilen? Treten Sie unserem Restaurant-Netzwerk bei!'); 
	
	define('MEET_TEAM_TITLE', 'Über das Team');
	define('JOIN_TEAM', 'Dem Team beitreten');
	
	define('MEET_DELIVERY_TEAM_TITLE', 'Über das Lieferteam');
	define('VP_MEMBER', 'Mitglieder des Teams');
