<?php
	// SEO settings
	define('SEO_TITLE', ''); 
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ORDER_CONFIRMATION', 'Bestätigung der Bestellung'); 
	
	define('_AT', 'um'); 
	define('FOR_A', 'zum'); 
	
	define('THANKS', 'Danke.'); 
	define('ORDER_SENT', 'Die Einzelheiten Ihrer Bestellung wurden an das Restaurant weitergeleitet.'); 
	define('ORDER_DETAILS', 'Ihre Bestellung (<strong class="green">{order_no}</strong>) von <strong class="green">{order_value} </strong> am {order_date} wurde weitergeleitet an <strong class="green">{restaurant}</strong>. Sie werden eine Bestätigungsemail des Restaurants erhalten.');
	define('YOUR_ORDER_FOR', 'Ihre Bestellung ist für:'); 
	define('CONTACT_RESTAURANT', 'Wenn Sie Fragen zu Ihrer Bestellung haben nehmen Sie Kontakt auf mit <strong class="green">{restaurant}</strong> unter <strong class="green">{restaurant_phone}</strong>'); 
	
	define('WHAT_NEXT_TITLE', 'Der Vorgang:'); 
	define('WHAT_NEXT_BODY1', '{restaurant} erhält Ihre Bestellung'); 
	define('WHAT_NEXT_BODY2', 'Schickt Ihnen eine Bestellbeståtigung'); 
	define('WHAT_NEXT_BODY3', '{restaurant} bereitet Ihre Bestellung vor und der Lieferant kümmert sich um alles andere.'); 
