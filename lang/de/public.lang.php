<?php
	// SEO settings
	define('DEFAULT_SEO_TITLE', 'Venezvite | Lieferungen nach Hause Genf');
	define('DEFAULT_SEO_KEYWORDS', 'Lieferung nach Hause Genf, Kommt schnell, Venezvite, Lieferung Genf, Hauslieferung Genf, Pad Thai Genf, Mahlzeit zu Hause, Restaurant zu Hause Genf, Lieferung nach Hause, von zu Hause bestellen, online Essen bestellen, Pizza Lieferung Genf, Couscous zu Hause, Sushi Lieferung nach Hause, Genf Lieferung, Lieferung Nahrung Genf, Green Mango Genf, Cafe im Zentrum, Maloca Genf, ich fühle Bio in Genf, Barraco Chic, Pause Genf'); 
	define('DEFAULT_SEO_DESCRIPTION', 'Lieferung von Gerichten nach Hause oder ins Büro im Genfer Kanton, Venezvite ist der beste Weg Essen zu bestellen, mitzunehmen oder sich liefern zu lassen. Venezvite ist eine Marketingplattform die Sie mit ausgezeichneten Restaurants in Verbindung setzt. Haben Sie Hunger? Bestellen Sie oder kommen Sie in unsere Restaurants.');
	
	define('LOGO_SLOGAN', 'Lieferservice nach Hause oder ins Büro'); 
	
	define('FAVORITED_RESTAURANT', 'Favorited Restaurant');
	define('HELLO', 'Guten Tag'); 
	define('CITIES', 'Städte');
	define('HELP', 'Help');
	define('LOGIN', 'Sich anmelden'); 
	define('ORDERS_HISTORY', 'Bestellübersicht'); 
	define('MY_ACCOUNT', 'Mein Profil'); 
	define('PROFILE', 'Profil'); 
	define('ADDRESSES', 'Adressen'); 
	define('PAYMENTS', 'Bezahlung'); 
	define('RESTAURANT_GALLERY', 'Gallery');
	define('BUSINESS_INFO', 'Infos über das Unternehmen');
	define('FINANCIAL_INFO', 'Finanzielle Einzelheiten'); 
	define('MENU', 'Menü');
	define('SPICY', 'Würzig');
	define('VEGETARIAN', 'Vegetarische');
	define('NO_GLUTEN', 'Gluten-frei');
	define('NO_LACTOSE', 'Laktosefrei');
	define('RECOMMENDED', 'Empfohlen');
	define('MENU_CATEGORIES', 'Menu Categories');
	define('MENU_OPTION_GROUPS', 'Menu Option Groups');
	define('SEE_PROFILE', 'Mein Profil einsehen'); 
	define('LOGOUT', 'Sich abmelden'); 
	
	define('FB_LOGIN', '<strong>Sich anmelden</strong> mit <strong>Facebook</strong>');	
	define('OR_', 'oder'); 
	define('USERNAME_CORPORATE', 'Benutzername für Unternehmensmitglieder'); 
	define('PASSWORD', 'Passwort'); 
	define('KEEP_LOGGED', 'Speichern'); 
	define('OOPS_FORGOT', 'Oops, Ich habe mein <a href="{$link}">Passwort vergessen</a>'); 
	define('LOGIN_INCENTIVE_TITLE', 'Ist das Ihre erste Bestellung auf Venezvite?'); 
	define('LOGIN_INCENTIVE_DESC', '<strong>Sie brauchen kein Konto erstellen.</strong> Geben Sie Ihre genaue Adressen ein, bestellen Sie, wir kümmern uns um den Rest!');
	define('INVALID_USER_PASS', 'Ungültige Benutzernamen und Passwort-Kombination'); 
	define('UNABLE_TO_REGISTER', 'Leider wurde Ihr Konto nicht registriert.!\n' . 
		'Versuchen Sie es noch einmal. Oder nehmen Sie Kontakt mit uns auf um das Problem zu lösen.'); 
	define('LAST_NAME', 'Name'); 
	define('FIRST_NAME', 'Vorname');

	define('PHONE_PREFIX', 'Prefix');
	define('PHONE_NO', 'Telefonnummer'); 
	
	define('RESTAURANTS_LIST_CUSTOM_PATH', 'restaurants-die-nach-hause-liefern'); 
	define('BACK', 'Zurück');
	
	define('BREADCRUMBS_ENTER_ADDRESS', 'Ihre Adresse'); 
	define('BREADCRUMBS_SELECT_RESTAURANT', 'Suchen Sie ein Restaurant aus'); 
	define('BREADCRUMBS_CHOOSE_FOOD', 'Suchen Sie ein Essen aus'); 
	
	define('REQUIRED', 'benötigte');
	define('OPTIONAL', 'wenn gewünscht'); 
	define('WHERE_WHEN', 'Wo und Wann?'); 
	define('DELIVERY', 'Lieferung nach Hause'); 
	define('TAKEAWAY', 'Zum Mitnehmen'); 
	define('SELECT_SAVED_LOCATION', 'eine bereits angegebene Adresse aussuchen'); 
	define('SUBMIT_NEW_LOCATION', '... oder eine neue Adresse angeben'); 
	define('SCHEDULED_FOR', 'Wann:');
	define('SCHEDULED_TIME', 'Uhrzeit:'); 
	define('FIND', 'Finden'); 
	define('SUBMIT', 'Abschicken'); 
	
	define('TODAY', 'Heute'); 
	define('TOMORROW', 'Morgen'); 
	define('YESTERDAY', 'Gestern'); 
	define('ASAP', 'So früh wie möglich'); 
	
	define('NEW_', 'Neu'); 
	define('VITE', 'DP');
	define('REVIEWS', 'Beurteilungen'); 
	define('DELIVERY_FEE', 'Lieferkosten'); 
	define('MINIMUM_ORDER', 'Lieferminimum'); 
	define('APPLY', 'Eingeben');
	define('CANCEL', 'Abbrechen');
	define('PRODUCT_TOTAL', 'Summe der Produkte :'); 
	define('CLOSED_NOW', 'Schliesst bald'); 
	
	define('NEXT_DELIVERY_TIME', 'Nächste Verfügabare Lieferung ist {datetime}'); // Die angegebene Uhrzeit hat sich verändert.
	define('NEXT_TAKEAWAY_TIME', 'Nächstmögliche Uhrzeit zum Mitnehmen ist {datetime}'); // Die ausgesuchte Zeit hat sich geändert
	
	define('ITEMS', 'Artikel');
	define('CHECKOUT', 'Gehen Sie zur Kasse');
	define('MY_ORDER', 'Mein Warenkorb');
	define('EMPTY_BAG', 'Der Warenkorb ist leer!');
	define('EDIT', 'Prüfen');
	define('ADD', 'Add');
	define('DELETE', 'Löschen');
	define('DELIVERY_MIN', 'Lileferungsminimum');
	define('NO_TAKEAWAY_MIN', 'Kein Minimum für Mitnehmmalzeiten!');
	define('HAVE_PROMO', 'Haben Sie einen Rabattcode?');
	define('ENTER_PROMO', 'Geben Sie den Code ein');
	//define('DELIVERY_ADDRESS_LABEL', 'Geben Sie Ihre Adresse ein, um sicher zu gehen, dass Ihre Adresse im Lieferbereich des Restaurants liegt.'); 
	define('ENTER_ADDRESS', 'Adresse eingeben');
	define('GRAND_TOTAL', 'Summe:'); 
	
	define('STREET_ADDRESS', 'Strasse');
	define('BUILDING_NO', 'Hausnummer');
	define('CITY', 'Stadt');
	define('POSTAL_CODE', 'Postleitzahl');
	define('FLOOR_SUITE', 'Etage');
	define('ACCESS_CODE', 'Zugangscode');
	define('SPECIAL_INSTR', 'Hinweise zur Lieferung');
	define('SPECIAL_INSTR_HINT', 'Hinweise zur Lieferung');
	
	define('PARTNER_LOGIN', 'Sich bei Geschäftspartnern anmelden');
	
	define('FOOTER_WHO_WE_ARE', 'Wer sind wir');
	define('FOOTER_ABOUT_US', 'Über uns');
	define('FOOTER_NEWS_PRESS', 'Neuigkeiten &amp; Presse');
	define('FOOTER_BLOG', 'Blog');
	define('FOOTER_YOUR_BUSINESS', 'Für Ihr Unternehmen');
	define('FOOTER_BECOME_PARTNER', 'Werden Sie ein Restaurantpartner');
	
	define('FOOTER_CREATE_ACCOUNT', 'Ein Geschäftskonto erstellen'); 
	define('FOOTER_CATERING_ORDER', 'Verfügbare Zuliefererdienste');
	define('FOOTER_CONTACT_HELP', 'Kontakt &amp; Hilfe');
	define('FOOTER_HELP_FAQ', 'Hilfe &amp; FAQ');
	define('FOOTER_CONTACT', 'Kontaktieren Sie uns');
	define('FOOTER_SUGGEST', 'Suggérer un Restaurant');
	define('FOOTER_MORE', 'Plus+');
	define('FOOTER_SPECIALS', 'Angebote &amp; Rabatte');
	
	define('SUCCESSFUL_RECOMMANDATION', 'Vielen Dank für die Annahme, dieses Restaurant zu uns.');
	
	define('FOOTER_COPYRIGHT', 'Bester Weg zur Bestellung von Mahlzeiten und Weinen. Alle Rechte vorbehalten.');
	define('FOOTER_USE_TERMS', 'Nutzungsbedingungen');
	define('FOOTER_PRIVACY', 'Datenschutzerklärung');
	define('FOOTER_SITE_BY', 'Seite gemacht durch');
	
	define('MORE_MONEY', 'Das Bestellminimum wurde nicht erreicht' . 
		(@$current_page=='checkout' ? '<br />(ODER die aktuelle Restaurant erlaubt keine Barzahlungen)' : '') . '.');
	define('CODE_ALREADY_USED', 'Sie haben den Code schon verwendet!' . "\n" . 
		'Man kann einen Rabattcode nur ein Mal verwendet.');
	define('DA_PROMO_CODE', 'Rabattcode');
	define('PROMO_VALID_FROM', 'Dieser Rabattcode gilt bis {$date}.');
	define('PROMO_VALID_UNTIL', 'Dieser Rabattcode ist falsch. Läuft ab am {$date}.');
	define('INVALID_PROMO_CODE', 'Dieser Rabattcode ist ungültig!');
	define('LOGIN_FOR_PROMO', 'Bitte melden Sie sich an, um einen Gutschein zu benutzen.');
	define('PROMO_USED', 'Dieser Rabattode wurde schon verwendet.');
	define('PROMO_MIN_VALUE', 'Dieser Rabattcode ist nur gültig bei Bestellungen mit {$value} CHF oder mehr.');
	define('INVALID_OR_REGISTERED_PROMO', 'Ihr Rabattcode ist ungültig oder betrifft einen bestimmten Verbraucher. Loggen Sie sich erneut ein und versuchen Sie es noch einmal!');
	define('EMAIL_IN_USE', 'Benutzername ist schon vergeben.');
	
	define('R_EMAIL_CONFIRMATION_SUBJECT', 'Ihr Konto Venezvite');
	define('R_EMAIL_CONFIRMATION_BODY', 'Guten Tag {$firstName},

Ihr Konto Venezvite ist aktiviert. Glückwunsh! Die Details Ihre Kontos sind:

 - Benutzername: {$username}
 - Passwort: {$password}

Drucken Sie eine Kopie oder speichern Sie diese Information. Sie können sich nun in der Website und um Nahrung zu Inhalt Ihres Herzens!

Vielen Dank für Ihre Wahl von Venezvite.');
	
	define('HELLO_RESTAURANT', 'Liebe Parnter und Restaurantbetreiber');
	define('HUNGRY_FOR_BUSINESS', 'Möchten Sie neue Kunden finden? Es ist einfach. Registriert venezvite.com');
	
	define('HOW_IT_WORKS', 'Wie geht das?');
	define('HOW_WE_HELP', 'Unsere Rolle');
	define('JOIN', 'Kommen Sie zu uns');
	
	define('WHO_ARE_WE', 'Wer sind wir?');
	define('FOR_YOUR_COMPANY', 'Ihr Unternehemn betreffend');
	define('CONTACT_HELP', 'Kontakt &amp; Hilfe');
	define('SUBMENU_PLUS', 'Plus +');

	define('ORDER_FROM', 'Order from');
	define('CONTINUE_TO_CHECKOUT', 'Continue to checkout');
	
	define('NO_FAVORITED_RESTAURANT', 'No Favorited Restaurants');
