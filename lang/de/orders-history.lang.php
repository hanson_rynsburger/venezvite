<?php
	// SEO settings
	define('SEO_TITLE', 'Auflistung aller Bestellungen');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('VIEW_ORDER', 'Bestellung ansehen'); 
	
	define('PENDING', 'Warten'); 
	define('ACCEPTED', 'Angenommen'); 
	define('DECLINED', 'Abgelehnt'); 
	
	define('NO_PENDING_ORDERS', 'Sie haben keine Bestellung aufgegeben.'); 
	define('VIEW_PAST_ORDERS', 'Frühere Bestellungen ansehen'); 
	define('NO_PAST_ORDERS', 'Keine früheren Bestellungen.'); 
