<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	//define('UNABLE_TO_REGISTER', 'Unfortunately we were unable to register your corporate account. Either try to register again, or contact us to report this issue.');
	define('REGISTRATION_CONFIRMATION', 'In a few seconds you will receive an email. To complete your registration, please click on the link to confirm your email address.');
	define('SOME_ERRORS', 'There were some errors while trying to create your account:');
	define('REGISTRATION_SUCCESS', 'You have successfully registered your corporate account!');
	
	define('HELLO_CORPORATE', 'Hello Corporate Offices');
	define('SIGN_UP_CORPORATE', 'Sign up to create a corporate account and fuel your employees!');
	define('SIGN_UP_LINK', 'Sign up');
	
	define('CONTACT_INFO', 'Contact info <span>* (' . REQUIRED . ')</span>');
	define('JOB_TITLE', 'Job title');
	define('DEPARTMENT', 'Department');
	define('MOBILE', 'Mobile');
	
	define('COMPANY_INFO', 'Company information');
	define('SELECT_COUNTRY', 'Select your country');
	define('COMPANY_CITY', 'City / Village / Town * (You must select from the list)');
	define('COMPANY_NAME', 'Company name');
	define('COMPANY_ADDRESS', 'Company street address');
	define('COMPANY_PHONE', 'Company phone');
	define('EMPLOYEES_NO', 'Number of employees');
	
	define('ATTACH_EMAIL_ADDRESSES', 'Attach email addresses to your account');
	define('YOUR_EMAIL', 'Your email address');
	define('CONFIRM_YOUR_EMAIL', 'Confirm your email address');
	define('ATTACH_MORE_EMAILS', 'Attach more email addresses');
	
	define('CREATE_ACCOUNT_TITLE', 'Create your account');
	define('USERNAME', 'Username');
	define('CONFIRM_USERNAME', 'Confirm your username');
	define('CONFIRM_PASSWORD', 'Confirm password');
	define('AGREE_TERMS', 'I accept the <a class="popup" href="{$link}">terms and conditions</a>.');
	define('CREATE_ACCOUNT', 'Create an account');
	
	define('INVALID_ADDRESS', 'Oops! {address} doesn\'t seem to be a valid address!');
	define('USERNAME_IN_USE', 'It seems like the submitted username is already used by a different company registered in the website.');
	define('INVALID_EMAILS', 'None of the submitted email addresses seem to be valid!');
	
	define('RC_EMAIL_CONFIRMATION_SUBJECT', 'Confirm your Venezvite account');
	define('RC_EMAIL_CONFIRMATION_BODY', 'Hello {$companyName},

In order for your corporate account to be enabled by one of our team\'s members, please first confirm your email address, by clicking on the link below:

<a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/confirm-corporate-account.html?check={$uniqueHash}">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/confirm-corporate-account.html?check={$uniqueHash}</a>

Thank you for choosing Venezvite!');
