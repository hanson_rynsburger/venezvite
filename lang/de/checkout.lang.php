<?php
	// SEO settings
	define('SEO_TITLE', 'Ein Konto erstellen oder sich anmelden - Lieferservice Genf / Venezvite.'); 
	define('SEO_KEYWORDS', ' Lieferservice direkt nach Hause, Lieferservice direkt nach Hause in Genf, Lieferungservice Genf, kommen Sie schnell, venezvite, Lieferservice in Genf, catering Genf, pad thai Genf, catering lieferung Genf, Lieferservice, Essen Lieferung Genf, Gerichte nach Hause liefern Genf, Essen liefern'); 
	define('SEO_DESCRIPTION', 'Erstellen Sie ein Konto um Online eine Lieferung nach Hause in Genf bestellen zu können.'); 
	
	
	define('ORDER_ERRORS', 'Fehler mit Ihrer Bestellung:'); 
	define('USED_EMAIL', 'Emailadresse befindet sich schon in unserem System, loggen Sie sich ein, klicken Sie auf Passwort.'); 
	define('PHONE_MANDATORY', 'Bitte geben Sie Ihre Telefonnummer an, bevor Sie weiter navigieren.'); 
	define('CANT_FIND_ADDRESS', 'Wir können Ihre Adresse nicht finden. Versuchen Sie es noch einmal. Oder nehmen Sie Kontakt mit uns auf um ein Problem mitzuteilen.');
	define('NO_BUILDING_NO', 'We couldn\'t find a building number in the address you\'ve just submitted.'); 
	define('OUTSIDE_ADDRESS', 'Ihre Adresse liegt ausserhalb der Lieferzone des Restaurants.'); 
	define('DIFFERENT_CHARGES_MINIMUM', 'Die eingegebene Adressen hat die Lieferkosten verändert oder erreicht nicht die Minimallieferung. Bitte fügen Sie Ihrere Bestellung etwas hinzu.'); 
	define('DIFFERENT_MINIMUM', 'Ihre eingegeben Adresse verändert die Minimallieferung. Bitte fügen Sie Ihrer Bestellung etwas hinzu.');
	define('DIFFERENT_CHARGES', 'Die neu eingegebene Aderesse hat die Lieferkosten verändert. Bitte überprüfen Sie Ihre Bestellung und bestätigen Sie.<br />' . 
		'Bitte geben Sie immer Ihre genaue Adresse an um ein Restaurant zu finden.');
	define('WRONG_CC_TYPE', 'Ihre Kreditkarte ist unbekannt oder wird nicht unterstützt.'); 
	define('FILL_FIELDS', 'Bitte füllen Sie alle Pflichtelder aus.'); 
	
	define('ALMOST_DONE', '<span>Letzter Schritt</span> Geben Sie Ihre Informationen'); 
	define('CREATE_ACCOUNT', 'Ein Konto anlegene');
	define('ACCOUNT_INFO', 'Ihre Kundendaten');
	define('EDIT_MY_INFO', 'Bearbeiten meine Informationen'); 
	define('ENTER_EMAIL', 'Emailadresse'); 
	define('CREATE_PASS', 'Passwort anlegen'); 
	define('NEWSLETTER_ACCEPT', 'Ja, ich möchte Angebote und neue Restaurants per Email erhalten.'); 
	
	define('DELIVERY_DETAILS', 'Lieferdetails'); 
	define('FOR_DELIVERY', 'Lieferung nach Hause'); 
	define('CHANGE_TAKEAWAY', 'Wechseln zu Abholung'); 
	define('FOR_TAKEAWAY', 'Abholung'); 
	define('CHANGE_DELIVERY', 'Wechseln zu Lieferung nach Hause'); 
	
	define('ENTER_NEW_ADDRESS', 'Geben Sie eine neue Adresse ein'); 
	define('EDIT_ADDRESS', 'Überprüfen Sie diese Adresse'); 
	define('SAVE_ADDRESS', 'Speichern Sie ihre Eingaben um Sie nicht jedes Mal neu eingeben zu müssen.'); 
	define('TAKEAWAY_DESC', 'Sie bestellen gerade ein Gericht zum Mitnehmen. Ihre Bestellung wird zum Abholen im Restaurant bereit sein.'); 
	
	define('PAYMENT_OPTIONS', 'Zahlungsarten');
	define('INVOICE', 'Invoice');
	define('CASH', 'Barzahlung');
	define('CREDIT', 'Kreditkarten');
	define('CARD_NAME', 'Karteninhaber'); 
	define('CCN', 'Nummer der Karte'); 
	define('EXPIRATION_DATE', 'Gültig bis'); 
	define('CURRENCY', 'Devise (' . OPTIONAL . ')'); 
	define('USE_NEW_CARD', 'Neue Kreditkarte'); 
	
	define('TOO_MUCH_CASH', 'Über folgendem Betrag nehmen wir kein Bargeld an: ' . MAX_CASH_VALUE . ' {$currency}.'); 
	define('UNPAID_CASH', 'Es steht noch ein Bargeldbetrag aus. Bevor dieser nicht ausgeglichen wurde, können Sie nicht mehr bar bezahlen.<br />' .
		'Bitte führen Sie Ihre Bestellung fort mit einer Kreditkarte.'); 
	define('UNAPPROVED_CASH', 'Ihre Bezahlung mit Bargeld wurde nicht bestätigt. Bitte warten Sie auf die Bestätigung oder bezahlen Sie mit einer Kreditkarte.'); 
	define('NO_CASH', 'Dieses Restaurant nimmt keine Bezahlungen in bar an.'); 
	define('CASH_PAYMENT_DESC', 'Bezahlen Sie bar bei der Lieferung<br />
(Der Lieferant dieses Restaurants nimmt nur Bargeld an.)'); 
	define('INVOICING_DESC', 'If your Corporate Account is approved for ordering food on Venezvite.com without paying online in advance (but receiving an invoice to be paid at a later time), you can use this option to place your order.' . "\n\n" . 
        'Be warned that, in case your account is NOT approved for such an option, your order will be ignored!' . "\n\n" . 
        'For details about how your account could become approved for invoice ordering, please <a class="popup" href="' . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/contact.html">contact us</a>!');
	
	define('AGREE_TERMS', 'Ich akzeptiere die <a class="popup" href="tou-customers.html">Nutzungsbedingungen</a>.');
	define('SAVE_INFO', 'Speichern.');
	define('PLACE_ORDER_X', 'Meine Bestellung abschicken ({x})'); 
	
	
	
	define('P_ORDER_EMAIL_TITLE', 'Neue Bestellung Venezvite'); 
	define('P_ORDER_EMAIL_ORDER_TYPE', 'Bestellart'); 
	define('P_ORDER_EMAIL_DELIVERY', 'Lieferung'); 
	define('P_ORDER_EMAIL_TAKEAWAY', 'Zum Mitnehmen'); 
	define('P_ORDER_EMAIL_ORDER_NO', 'Bestellnummer'); 
	define('P_ORDER_EMAIL_DELIVERY_DATE_HOUR', 'Datum/Uhrzeit der Bestellung'); 
	define('P_ORDER_EMAIL_RESTAURANT', 'Restaurant'); 
	define('P_ORDER_EMAIL_PHONE', 'Telefon'); 
	define('P_ORDER_EMAIL_ORDER', 'Bestellung'); 
	define('P_ORDER_EMAIL_PAYMENT_DONE', 'Bezahlung: Schon bezahlt'); 
	define('P_ORDER_EMAIL_PAYMENT_TBD', 'Bezahlug: steht noch aus'); 
	define('P_ORDER_EMAIL_TOTAL', 'Summe der bestellten Produkte'); 
	define('P_ORDER_EMAIL_ADDRESS', 'Adresse'); 
	define('P_ORDER_EMAIL_LAST_NAME', 'Name'); 
	define('P_ORDER_EMAIL_FIRST_NAME', 'Vorname'); 
	define('P_ORDER_EMAIL_COMPANY', 'Firma'); 
	define('P_ORDER_EMAIL_STREET', 'Strasse'); 
	define('P_ORDER_EMAIL_ZIP', 'Postleitzahl'); 
	define('P_ORDER_EMAIL_CITY', 'Stadt'); 
	define('P_ORDER_EMAIL_FLOOR_CODE', 'Etage und Eingangscode'); 
	define('P_ORDER_EMAIL_NOTES', 'Notizen'); 
	define('P_ORDER_EMAIL_CASH_ON_DELIVERY', 'Barzahlung bei der Lieferung'); 
	define('P_ORDER_EMAIL_ORDER_READY', 'Die Bestellung muss am <span style="color:#c00">{$datetime}</span> bereit stehen'); 
	define('P_ORDER_EMAIL_ACTIVATION_LINK', 'Zugang zur Annahme der Bestellung:'); 
	define('P_ORDER_EMAIL_DELIVERY_DELAY', 'Geschätzte Lieferzeit: {$time} <span style="color:#c00">+/- 15 Minuten</span>'); 
	define('P_ORDER_EMAIL_TAKEAWAY_TIME', 'Abholzeit: {$time}'); 

	define('VIEW_IMAGES', 'View images');
