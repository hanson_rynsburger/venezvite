<?php
	// FOOTER CONTACT
	define('CONTACT_PANEL_BODY', '<strong class="subtitle">We are online!</strong>
<p>Chat or message us directly. We are online 9:00 - 24:00 everyday.</p>
<br />
<strong class="subtitle">Customer Service</strong>
<p>If you have a food or delivery related problem, please contact the restaurant directly.</p> 
<p>For any billing or additional questions chat or message us.</p>
<p>email: hello@venezvite.com</p>
<br />
<strong class="subtitle">Restaurant Support</strong>
<p>If you are restaurant and need to speak with a Venezvite team member<br />
	please call us between 14:00 - 19:00 (GMT+1)</p>
<p>phone: +41 22 575 29 07</p>');
	
	
	// FOOTER RESTAURANT SUGGESTION
	define('SUGGESTION_PANEL_BODY', '<strong class="subtitle">Looking for a restaurant but it\'s not here? Tell them and then tell us!</strong>
<p>Are you a restaurant owner/manager? Sign up today and a Venezvite member will contact you.</p>');
	define('SUGGESTION_PANEL_NAME', 'Your name');
	define('SUGGESTION_PANEL_RESTAURANT', 'Restaurant name');
	define('SUGGESTION_PANEL_ADDRESS', 'Restaurant address');
	define('SUGGESTION_PANEL_COMMENTS', 'Comments');
	
	
	// FOOTER SPECIAL OFFERS
	define('SPECIALS_PANEL_BODY', '<strong class="subtitle">Because we like you too!</strong>
<p>Enjoy 7.00 off your first order with Venezvite.com. Valid even if you already placed an order with us before, but never used the promo code. Valid for one use only.</p>
<p>Promo code: <strong>likevenezvite</strong></p>

<strong class="subtitle">Tell a friend</strong>
<p>Give the gift of food &amp; wine delivery with venezvite.com. Invite a friend and on their first order they receive 4.00 off and you receive 4.00 off your next order as well. Only 1 promo code can be used per purchase.</p>
<p><strong>No promo code required.</strong></p>

<strong class="subtitle">Happy Holidays!</strong>
<p>Enjoy 10% off for the holidays. Offer expires 1/1/16.</p>
<p>Promo code: <strong>happyholidays2015</strong></p>');
