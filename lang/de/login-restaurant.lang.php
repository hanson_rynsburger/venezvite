<?php
	// SEO settings
	define('SEO_TITLE', 'Restaurant Login');
	define('SEO_KEYWORDS', ''); 
	define('SEO_DESCRIPTION', ''); 
	
	
	define('LR_LOGIN_UNAPPROVED', 'Ihre Bestellung wurde noch nicht bestätigt. Entweder haben Sie Ihre Emailadress nicht bestätigt oder wir haben Ihre Eingaben noch nicht überprüft.\n' . 
		'Haben Sie noch etwas Geduld Ihr Konto wurde noch nicht bestätigt. Entweder Sie haben Ihre Emailadresse noch nicht bestätigt oder wir sind gerade dabei, Ihre Angaben zu überprüfen.'); 
	define('LR_LOGIN_ERROR', 'Wir erkennen leider Ihre Angaben nicht. Bitte versuchen Sie es erneut. Haben Sie Ihr Passwort vergessen, klicken Sie bitte auf den unteren Link.'); 
	
	define('LOGIN_ACCOUNT', 'Loggen Sie sich auf ihr Konto ein'); 
	define('USERNAME', 'Benutzername (Emailadresse)'); 
	define('FORGOT', 'Vergessen'); 
	
	define('CONFIRM_ORDERS', 'Bestätigen Sie Ihre Bestellung'); 
	define('CONFIRM_ORDERS_DESC', 'Bestätigen Sie eingehende Bestellungen'); 
	define('MANAGE_RESTAURANT', 'Verwalten Sie Ihr Restaurantkonto'); 
	define('MANAGE_RESTAURANT_DESC', 'Aktualisieren Sie Ihr Menü und Ihr Konto');
	define('SALES', 'Verkäufe'); 
	define('SALES_DESC', 'Beurteilung Ihrer Verkäufe'); 
