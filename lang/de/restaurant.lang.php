<?php
	// SEO settings
	//define('SEO_TITLE', '');
	//define('SEO_KEYWORDS', '');
	//define('SEO_DESCRIPTION', '');
	
	
	define('WHEN_WHERE_DESC', 'Damit Sie sicher sind, beliefert zu werden, geben Sie Ihre genaue Adresse ein und die Uhrzeit ein.'); 
	
	define('RETURN2LIST', 'Zurück zur Restaurantliste');
	define('ADD_TO_FAVES', 'Zu Favoriten hinzufügen'); 
	define('VERY_POPULAR_RESTAURANT', 'Ein gut bekanntes Restaurant'); 
	define('POPULAR_RESTAURANT', 'Ein bekanntes Restaurant'); 
	define('OPEN', 'Geöffnet'); 
	define('CLOSED', 'Geschlossen'); 
	define('CLOSES_IN', 'Geschlossen in {x} Minuten'); 
	
	define('MORE_INFO', 'Mehr Informationen'); 
	
	define('VISIT_WEB', 'Besuchen Sie die Webseite'); 
	define('SERVING_IN', 'Bedient Kunden von');
	define('HOURS_DELIVERY_ZONES', 'Uhrzeiten &amp; Lieferzonen'); 
	define('TAKEAWAY_HOURS', 'Mitnahmezeiten'); 
	define('DELIVERY_HOURS', 'Lieferzeiten'); 
	define('NO_DELIVERY', 'Keine Lieferung');
	define('DELIVERY_ZONES', 'Lieferzonen'); 
	define('ABOUT', 'Über das Restaurant'); 
	
	define('SEARCH_HINT', 'Suchen einen Bestandeil des Gerichtesu');
	define('FILTER', 'Filtern');
	define('FILTER_PHOTOS', 'Fotos');
	
	define('ADD_ITEM', 'Hinzufügen'); 
	define('SHOW_IMAGE', 'Bild vergrössern'); 
	define('HIDE_IMAGE', 'Bild verkleinern'); 
	define('VIEW_DETAILS', 'Details ansehen'); 
	define('ADD2BAG', 'Dem Warenkorb hinzufügen'); 
	define('SPECIAL_INSTRUCTIONS', 'Notizen / Zusatsinformationen'); 
	define('SPECIAL_INSTRUCTIONS_EG', '(z.B. Bitte um zusätzliche Sauce)'); 
	define('EDIT_ITEM', 'Überprüfen'); 
	define('UPDATE_BAG', 'Aktualisieren'); 
	define('OPTIONS_EXACTLY', 'Genau {x} Optionen aussuchen'); 
	define('OPTIONS_AT_LEAST', 'Mindestens {x} Optionen aussuchen'); 
	define('OPTIONS_UP_TO', 'Select up to {x} option(s)');
	
	define('ITEM_ADDED', 'Artikel hinzugefügt');
	define('GO2CHECKOUT', 'Zur Kasse gehen');
	
	define('NO_MENUS_FOR_SEARCH', 'Keine Menüpunkte gefunden, die Ihren Suchkriterien entsprechen.');
	
	define('REVIEWS_TITLE', 'Veröffentlichte Meinungen'); 
	define('WRITE_REVIEW', ' Ihre Meinung veröffentlichen');
	
	define('DELIVERY_SCHEDULE', 'Lieferzeit'); 
