<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('INVALID_FILE', 'Eine oder mehrere Dateien wurden nicht heruntergeladen. Wir unterstützen diese Datei nicht.'); 
	define('CANT_SAVE_RESTAURANT', 'Leider können wir Information über Ihr Restaurant nicht speichern. Versuchen Sie es noch einmal. Oder schicken das Problem an uns weiter.'); 
	define('REGISTRATION_CONFIRMATION', 'In einigen Sekunden werden Sie ein Email erhalten. Vervollständigen Sie Ihre Einschreibung durch einen Klick auf den Link. Dadurch wird Ihre Adresse bestätigt.'); 
	define('SOME_ERRORS', 'Beim Kontoerstellen treten Fehler auf:');
	define('REGISTRATION_SUCCESS', 'Sie haben Ihr Restaurant mit Erfolg gespeichert.'); 
	
	define('CONTACT_INFO', 'Kontaktinformationen <span>* (' . REQUIRED . ')</span>'); 
	define('MOBILE', 'Mobil'); 
	
	define('RESTAURANT_INFO', 'Informationen über das Restaurant'); 
	define('SELECT_COUNTRY', 'Wähle dein Land');
	define('RESTAURANT_EMAIL', 'Emailadresse'); 
	define('RESTAURANT_NAME', 'Name des Restaurants'); 
	define('RESTAURANT_ADDRESS', 'Strasse des Restaurants'); 
	define('RESTAURANT_CITY', 'Ort * (Wählen Sie eine Stadt in der Liste aus)'); 
	define('RESTAURANT_PHONE', 'Telefonnummer des Restaurants'); 
	define('ENTER_VALID_PHONE', 'Enter a valid phone number');
	define('RESTAURANT_URL', 'WebSeite des Restaurants'); 
	define('RESTAURANT_DESC', 'Über Ihr Restaurant');
	
	define('PRIMARY_CUISINE_TYPE', 'Suchen Sie sich den Typ Ihrer Küche aus'); 
	define('CHOOSE_CUISINE', 'Wählen Sie Ihre Küche');
	define('CHOOSE_SERVICES', 'Wählen Sie, welchen Service Sie anbieten'); 
	define('CATERING', 'Cateringservice'); 
	define('CUSTOM_DELIVERY', 'Wir liefern nicht und wir möchten mit einem Lieferanten zusammenarabeiten.'); 
	define('SELECT_TIME', 'Auswählen'); 
	
	define('HOURS_OPERATION', 'Öffnungszeiten');
	define('SAME_EVERY_DAY', 'Die selben Uhrzeiten für jeden Tag einsetzen '); 
	define('DELIVERY_ZONES', 'Lieferzone '); 
	define('ZONE', 'Zone 1'); 
	define('ZONE_RANGE', 'Zone 1 in Kilometerangaben');
	define('MIN_DELIVERY', 'Bestellminimum für die Zone 1'); 
	define('DELIVERY_FEE_ZONE', 'Lieferkosten für die Zone 1'); 
	define('SELECT_TIME_INTERVAL', 'Durchschnittliche Lieferzeit (nur Transport)'); 
	define('ADD_ZONE', 'Fügen Sie eine andere Zone hinzu'); 
	define('PHOTOS', 'Fotos'); 
	define('UPLOAD_PHOTOS', 'Fotos herunterladen (.jpg oder .png)'); 
	define('UPLOAD_MENU', 'Menü herunterladen (.docx, .pdf oder .jpg)'); 
	
	define('CREATE_ACCOUNT_TITLE', 'Ihr Konto erstellen'); 
	define('USERNAME', 'Benutzername: Ihre Emailadresse eingeben'); 
	define('CONFIRM_USERNAME', 'Ihre Emailadressse bestätigen'); 
	define('CONFIRM_PASSWORD', 'Passwort');	 
	define('AGREE_TERMS', 'Ich akzeptiere <a class="popup" href="{$link}">die Allgemeinen GeschUaftsbedingungen</a>.'); 
	define('CREATE_ACCOUNT', 'Ein Konto anlegen'); 
	
	define('INVALID_ADDRESS', 'Oops! {address} ist keine gültige Adresse!');
	
	define('RR_EMAIL_CONFIRMATION_SUBJECT', 'Bestätigen Sie Ihre Emailadresse');
	define('RR_EMAIL_CONFIRMATION_BODY', 'Guten Tag {$restaurantName},

Um Ihr Konto zu aktivieren, bestätigen Sie bitte Ihre Emailadresse unter folgendem Link:

<a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'de') . '/confirm-restaurant.html?check={$uniqueHash}">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'de') . '/confirm-restaurant.html?check={$uniqueHash}</a>

Vielen Dank, dass Sie sich Venezvite entschieden haben!');
