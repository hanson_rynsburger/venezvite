<?php
	define('PAYMENT_TYPE', 'Bezahlungsart'); 
	define('CC', 'Kreditkarte'); 
	define('DELIVERY_CASH', 'Barzahlung bei Lieferung'); 
	define('TAKEAWAY_CASH', 'Barzahlung bei Abholung'); 
	define('NO_LATER_THAN', 'Nicht später als'); 
	define('MINUTES', 'Minuten'); 
	define('NOTES', 'Notizen'); 
	define('CUSTOMER', 'Kunde'); 
	define('DELIVERY_DATETIME', 'Datum/Uhrzeit der Lieferung'); 
	define('DELIVERY_TO', 'Lieferung nach'); 
	define('FLOOR', 'Stockwerk'); 
	define('DELIVERY_INSTRUCTIONS', 'Lieferungshinweise'); 
	define('TAKEAWAY_DATETIME', 'Datum/Uhrzeit der Abholung'); 
	define('TOTAL_PRODUCTS', 'Summe der Produkte'); 
	define('DISCOUNT', 'Rabatt'); 
	define('TOTAL', 'Endsumme'); 
