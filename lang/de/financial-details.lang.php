<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('EDIT_DETAILS', 'Bearbeiten Sie finanzielle Einzelheiten');
	define('BUSINESS_NAME', 'Registrierung / Firmenname');
	define('IBAN', 'IBAN');
	define('BANK', 'Bank Name');
	define('SWIFT', 'SWIFT-Code');
	
	define('COMMISSION_AGREEMENT', 'Venezvite Commission Agreement');
	define('COMMISSION_PERCENTAGE', 'Venezvite Kommission: {$percentage}% + Versandkosten');
	define('COMMISSION_PAYMENT', 'Zahlung: Der 15. Tag eines jeden Monats, während es ein Minimum von 500.');
	define('THANK_YOU', 'Thank you for being a Venezvite partner.');
	define('QUESTIONS_EMAIL', 'Bei Fragen wenden Sie sich bitte per E-Mail: hello@venezvite.com');
