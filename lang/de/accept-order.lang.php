﻿<?php
	define('AO_EMAIL_TITLE', 'Ihre Bestellung wurde angenommen!'); 
	define('AO_EMAIL_BODY', 'Guten Tag {$userName},

Diese Email bestätigt, dass die registrierte Bestellung bei Venezvite durch {$restaurantName} akzeptiert wurde.		

Sie werden verständigt, wenn das Gericht bereit ist.'); 
