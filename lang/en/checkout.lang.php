<?php
	// SEO settings
	define('SEO_TITLE', 'Sign in or create an account - Food Delivery in Geneva | Venezvite');
	define('SEO_KEYWORDS', 'Creat an account, food delivery, wine delivery');
	define('SEO_DESCRIPTION', 'Sign in or create an account to order food and wine delivery to your home');
	
	
	define('ORDER_ERRORS', 'Error with your order:');
	define('USED_EMAIL', 'Email address is already in our system. Log in, click on I forgot my password.');
	define('PHONE_MANDATORY', 'Before you continue using Venezvite, please update your phone number!');
	define('CANT_FIND_ADDRESS', 'We cannot determine the location of the submitted address. Please try again, or contact us to report this issue!');
	define('NO_BUILDING_NO', 'We couldn\'t find a building number in the address you\'ve just submitted.');
	define('OUTSIDE_ADDRESS', 'Address you entered is outside the restaurant\'s delivery zones.');
	define('DIFFERENT_CHARGES_MINIMUM', 'The address you entered changed the delivery fee and required minimum. Please add more items to your order to meet the new minimum.');
	define('DIFFERENT_MINIMUM', 'Address you entered changed the required minimum. Please add more items to your order to meet the new minimum.');
	define('DIFFERENT_CHARGES', 'Please note the new address you entered changed the delivery fee. Please review and confirm your order.<br />' . 
		'Hint: always use your exacte address to search for restaurants.');
	define('WRONG_CC_TYPE', 'Your credit card type is either unknown or unsupported. Please try a different one or contact us to report this issue!');
	define('FILL_FIELDS', 'Please fill in all necessary fields.');
	
	define('ALMOST_DONE', '<span>Almost done!</span> Enter your Information');
	define('CREATE_ACCOUNT', 'Create an Account');
	define('ACCOUNT_INFO', 'Your account info');
	define('EDIT_MY_INFO', 'Edit my information'); 
	define('ENTER_EMAIL', 'Enter your Email Address');
	define('CREATE_PASS', 'Create a Password');
	define('NEWSLETTER_ACCEPT', 'Yes, I would like to receive discounts, and be notified of new restaurants by email.');
	
	define('DELIVERY_DETAILS', 'Delivery Details');
	define('FOR_DELIVERY', 'For Delivery');
	define('CHANGE_TAKEAWAY', 'Change to Take-away');
	define('FOR_TAKEAWAY', 'For Take-away');
	define('CHANGE_DELIVERY', 'Change to Delivery');
	
	define('ENTER_NEW_ADDRESS', 'Enter New Address');
	define('EDIT_ADDRESS', 'Edit this address');
	define('SAVE_ADDRESS', 'Save your information so you don\'t have to enter it each time you place an order.');
	define('TAKEAWAY_DESC', 'You are placing a Take-away order, your food will be ready at the restaurant for you to pick up.');
	
	define('PAYMENT_OPTIONS', 'Payment Options');
	define('INVOICE', 'Invoice');
	define('CASH', 'Cash');
	define('CREDIT', 'Credit');
	define('CARD_NAME', 'Name on Card');
	define('CCN', 'Credit Card Number');
	define('EXPIRATION_DATE', 'Expiration Date');
	define('CURRENCY', 'Currency (' . OPTIONAL . ')');
	define('USE_NEW_CARD', 'Use a new card');
	
	define('TOO_MUCH_CASH', 'You can only place cash orders for values less than ' . MAX_CASH_VALUE . ' {$currency}.');
	define('UNPAID_CASH', 'Our records show you have an unpaid cash order. This option will remain disabled until payment is received.<br />' . 
		'Please continue your order using a credit card.');
	define('UNAPPROVED_CASH', 'Your previous cash order hasn\'t been approved yet. Please stand by until it will get approved, or continue your order using a credit card.');
	define('NO_CASH', 'The current restaurant doesn\'t allow cash payments.');
	define('CASH_PAYMENT_DESC', 'Pay cash on delivery.<br />
(Cash only. This restaurant\'s deliverer does not accept credit card payments on delivery.)');
	define('INVOICING_DESC', 'If your Corporate Account is approved for ordering food on Venezvite.com without paying online in advance (but receiving an invoice to be paid at a later time), you can use this option to place your order.' . "\n\n" . 
        'Be warned that, in case your account is NOT approved for such an option, your order will be ignored!' . "\n\n" . 
        'For details about how your account could become approved for invoice ordering, please <a class="popup" href="' . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/contact.html">contact us</a>!');
	
	define('AGREE_TERMS', 'I agree to the <a class="popup" href="tou-customers.html">Terms of Use</a>.');
	define('SAVE_INFO', 'Save your information.');
	define('PLACE_ORDER_X', 'Place my order ({x})');
	
	
	
	define('P_ORDER_EMAIL_TITLE', 'New Venezvite Order');
	define('P_ORDER_EMAIL_ORDER_TYPE', 'Order Type');
	define('P_ORDER_EMAIL_DELIVERY', 'Delivery');
	define('P_ORDER_EMAIL_TAKEAWAY', 'Take away');
	define('P_ORDER_EMAIL_ORDER_NO', 'Order Number');
	define('P_ORDER_EMAIL_DELIVERY_DATE_HOUR', 'Delivery Date and Hour');
	define('P_ORDER_EMAIL_RESTAURANT', 'Restaurant');
	define('P_ORDER_EMAIL_PHONE', 'Phone');
	define('P_ORDER_EMAIL_ORDER', 'Order');
	define('P_ORDER_EMAIL_PAYMENT_DONE', 'Payment: ALREADY PAID');
	define('P_ORDER_EMAIL_PAYMENT_TBD', 'Payment: TO BE PAID');
	define('P_ORDER_EMAIL_TOTAL', 'Total Products');
	define('P_ORDER_EMAIL_ADDRESS', 'Address');
	define('P_ORDER_EMAIL_LAST_NAME', 'Last Name');
	define('P_ORDER_EMAIL_FIRST_NAME', 'First Name');
	define('P_ORDER_EMAIL_COMPANY', 'Company');
	define('P_ORDER_EMAIL_STREET', 'Street');
	define('P_ORDER_EMAIL_ZIP', 'Postal Code');
	define('P_ORDER_EMAIL_CITY', 'City');
	define('P_ORDER_EMAIL_FLOOR_CODE', 'Floor and entry code');
	define('P_ORDER_EMAIL_NOTES', 'Notes');
	define('P_ORDER_EMAIL_CASH_ON_DELIVERY', 'CASH ON DELIVERY');
	define('P_ORDER_EMAIL_ORDER_READY', 'The order needs to be ready for <span style="color:#c00">{$datetime}</span>');
	define('P_ORDER_EMAIL_ACTIVATION_LINK', 'Access to Accept Order:');
	define('P_ORDER_EMAIL_DELIVERY_DELAY', 'Estimated delivery time: {$time} <span style="color:#c00">+/- 15 minutes</span>');
	define('P_ORDER_EMAIL_TAKEAWAY_TIME', 'Pick-up time: {$time}');

	define('VIEW_IMAGES', 'View images');