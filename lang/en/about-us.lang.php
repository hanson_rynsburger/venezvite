<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ABOUT_US_TITLE', 'About us');
	define('ABOUT_US_P1', 'Venezvite is the best way to order food and wine delivery online. No need to wait on a line, no missunderstanding errors, no need to repeat yourself. Our service is secure, easy to use and fast.');
	define('ABOUT_US_P2', 'Venezvite is a marketing platform that connects the best restaurants and vendors to customers who are hungry, thirsty, or in need of goods delivered to them quickly! Our restaurant and vendor partners offer delivery in less than 1 hour.');
	
	define('MEET_TEAM_TITLE', 'Meet the team');
	define('JOIN_TEAM', 'Join the Team!');
	
	define('MEET_DELIVERY_TEAM_TITLE', 'Meet the delivery team');
	define('VP_MEMBER', VITE . ' Team Member');
