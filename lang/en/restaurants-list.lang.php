<?php
	// SEO settings
	define('SEO_TITLE', 'The Best Restaurants Available for Food and Wine Delivery | Venezvite');
	define('SEO_KEYWORDS', 'Restaurant who deliver, restaurants who deliver in geneva, food delivery geneva, food delivery, wine delivery geneva, wine delivery, delivery food geneva');
	define('SEO_DESCRIPTION', 'The best restaurants available now for delivery in geneva!');
	
	
	define('FOR_', 'For?');
	define('WHERE_ARE_U', 'Where are you?');
	define('SEARCH_HINT', 'Food items, cuisines, restaurant names');
	define('SORT_HINT', '<strong>Sort / Filter by</strong> Price, Cuisine ...');
	define('SORT_BY', 'Sort by');
	define('FILTER_BY', 'Filter by');
	define('DISTANCE', 'Distance');
	define('ALPHABETICALLY', 'Alphabetically');
	define('CUISINES', 'Cuisines');
	define('RATINGS', 'Ratings');
	define('ANY_RATING', 'Any rating');
	define('PRICE', 'Price');
	define('ANY_PRICE', 'Any price');
	define('CLEAR', 'Clear');
	define('CLEAR_ALL_SORT_FILTER', 'Clear all sort / filter');
	define('SEARCH_CUISINES', 'Search Cuisines');
	define('CLEAR_ALL', 'Clear all');
	define('SHOW_MORE', '+ Show more');
	define('SHOW_LESS', '- Show less');
	define('AND_UP', '&amp; up');
	define('AND_LESS', '&amp; less');
	define('LIST_IS_FILTERED', 'Your list is filtered.');
	
	define('_AT', ' ');
	define('OPEN_RESTAURANTS_TITLE', '<span>{x}</span> open restaurants available for: <span class="red">{datetime}</span>');
	define('SEE_MORE_RESTAURANTS', '<a id="see-advanced-orders" href="javascript:;">Click here to see</a> {x} more restaurants within your area but not available during your selected time.');
	define('ORDERING_HOURS', 'Ordering Hours');
	define('DELIVERED_IN', 'Delivered in: {$time_interval} minutes');
	define('ORDER', 'Order');
	
	define('VIEW_MAP', 'View list on Map');
	define('HIDE_MAP', 'Hide Map');
	
	define('ADV_ORDERS_TITLE', 'Advanced orders only');
	define('ADV_ORDERS_DESC', '{x} more restaurants are within your area, but not available during your selected time.');
	define('REOPENS', 'Reopens');
	define('NEXT_DELIVERY', 'Next Delivery');
	define('ACCEPTS_CASH', 'Accepts cash');
	define('PRE_ORDER', 'Pre-Order');
	
	define('NO_DELIVERY', 'Address you entered is outside the restaurant\'s delivery zones. Please see the list of available restaurants below.');
	define('CLOSED_ALL_WEEK', 'The restaurant is closed during the whole next week. Please select a new restaurant from the list below.');
	
	
	define('NO_MATCHING_RESTAURANT', 'Sorry, there are no restaurants matching your criteria...');
	define('MODIFY_SEARCH', 'Please modify your search, or recomment a restaurant you\'d like to be listed on Venezvite.');
	define('RECOMMEND_RESTAURANT', 'Do you have a restaurant you would like to recommend?');
	define('YOUR_EMAIL', 'Your email address');
	define('RESTAURANT_NAME', 'Restaurant\'s name');
	
	define('CITIES_WERE_IN', 'Here are the cities we\'re in');
