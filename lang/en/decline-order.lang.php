﻿<?php
	define('DO_EMAIL_TITLE', 'Your order on Venezvite was declined');
	define('DO_EMAIL_BODY', 'Hello {$userName},

Your food order placed on Venezvite has just been declined by {$restaurantName}.
{$reason}
You can either contact the restaurant for more details, or try to order from a different one.

Your paid amount (if any) will be refunded to your bank account as soon as possible.');
	
	define('DO_EMAIL_REASON', 'The restaurant\'s reason for this decline was: ');
