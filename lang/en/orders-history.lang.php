<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('VIEW_ORDER', 'View Order');
	
	define('PENDING', 'Pending');
	define('ACCEPTED', 'Accepted');
	define('DECLINED', 'Declined');
	
	define('NO_PENDING_ORDERS', 'No pending orders available.');
	define('VIEW_PAST_ORDERS', 'View past orders');
	define('NO_PAST_ORDERS', 'No past orders available.');
