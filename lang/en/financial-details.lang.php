<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('EDIT_DETAILS', 'Edit financial details');
	define('BUSINESS_NAME', 'Account / Business name');
	define('IBAN', 'IBAN number');
	define('BANK', 'Bank name');
	define('SWIFT', 'SWIFT code');
	
	define('COMMISSION_AGREEMENT', 'Venezvite Commission Agreement');
	define('COMMISSION_PERCENTAGE', 'Venezvite Commission: {$percentage}% + Delivery cost');
	define('COMMISSION_PAYMENT', 'Payment: The 15th day of each month, while there is a minimum of 500.');
	define('THANK_YOU', 'Thank you for being a Venezvite partner.');
	define('QUESTIONS_EMAIL', 'For any questions please email: hello@venezvite.com');
