<?php
	// SEO settings
	//define('SEO_TITLE', '');
	//define('SEO_KEYWORDS', '');
	//define('SEO_DESCRIPTION', '');
	
	
	define('WHEN_WHERE_DESC', 'To confirm this restaurant delivers to you, please enter your exact address and desired time.');
	
	define('RETURN2LIST', 'Return to restaurant list');
	define('ADD_TO_FAVES', 'Add to faves');
	define('ADDED_TO_FAVES', 'Favorited');
	define('VERY_POPULAR_RESTAURANT', 'A very popular restaurant');
	define('POPULAR_RESTAURANT', 'A popular restaurant');
	define('OPEN', 'Open');
	define('CLOSED', 'Closed');
	define('CLOSES_IN', 'Closes in {x} minutes');
	
	define('MORE_INFO', 'More information');
	
	define('VISIT_WEB', 'Visit Website');
	define('SERVING_IN', 'Serving customers in');
	define('HOURS_DELIVERY_ZONES', 'Hours &amp; Delivery Zones');
	define('TAKEAWAY_HOURS', 'Take-Away Hours');
	define('DELIVERY_HOURS', 'Delivery Hours');
	define('NO_DELIVERY', 'No delivery');
	define('DELIVERY_ZONES', 'Delivery Zones');
	define('ABOUT', 'About');
	
	define('SEARCH_HINT', 'Search menu items');
	define('FILTER', 'Filter');
	define('FILTER_PHOTOS', 'Photos');
	
	define('ADD_ITEM', 'Add Item');
	define('SHOW_IMAGE', 'View full image');
	define('HIDE_IMAGE', 'Hide full image');
	define('VIEW_DETAILS', 'View details');
	define('ADD2BAG', 'Add to bag');
	define('SPECIAL_INSTRUCTIONS', 'Special Instructions &amp; Notes');
	define('SPECIAL_INSTRUCTIONS_EG', '(e.g. extra sauce)');
	define('EDIT_ITEM', 'Edit Item');
	define('UPDATE_BAG', 'Update bag');
 	define('OPTIONS_EXACTLY', 'Select exactly {x} option(s)');
	define('OPTIONS_AT_LEAST', 'Select at least {x} option(s)');
	define('OPTIONS_UP_TO', 'Select up to {x} option(s)');
	
	define('ITEM_ADDED', 'Item added');
	define('GO2CHECKOUT', 'Proceed to checkout');
	
	define('NO_MENUS_FOR_SEARCH', 'No menu items matching your search criteria.');
	
	define('REVIEWS_TITLE', 'Reviews');
	define('WRITE_REVIEW', 'Write a review');
	define('LAST_ORDER', 'Last order: ');
	
	define('DELIVERY_SCHEDULE', 'Delivery schedule');
	define('VIEW_IMAGES', 'View images');
	
	define('DELIVERED_ON_TIME', 'Was your delivery on time?');
	define('ORDER_CORRECT', 'Was your order correct?');
	define('FOOD_GOOD', 'Was the food good?');
	define('SKIP_QUESTION', 'Skip this question');

	define('OVERALL_EXPERIENCE', 'Rate your overall experience');
	define('REVIEW_YOUR_ORDER', 'Would you like to review your order?');
	define('GUIDELINE', 'Review guidelines');
	define('REMAINING', 'characters remaining. Minimum 3 words');
	define('SUBMIT_REVIEW', 'Submit your review');
	
	define('NO_REVIEWS', 'No reviews yet' );
	
	define('THANK_YOU_REVIEW', 'Thank you for submitting your review for this restaurant');
	define('CLOSE_MODAL', 'Close');