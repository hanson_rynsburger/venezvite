<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('RESTAURANT_DETAILS', 'Restaurant Details');
	define('RESTAURANT_DESC', 'Restaurant description');
	define('EDIT_RESTAURANT', 'Edit restaurant details');
	define('RESTAURANT_NAME', 'Restaurant name');
	define('ADDRESS', 'Address');
	define('PHONE', 'Phone');
	define('MOBILE', 'Mobile');
	define('WEBSITE', 'Website');
	define('RESTAURANT_INFO', 'Restaurant info');
	
	define('ADMIN_DETAILS', 'Admin Details');
	define('NOTICE_EMAIL', 'Notifications email');
	define('EDIT_ADMIN_DETAILS', 'Edit admin details');
	define('LOGIN_EMAIL', 'Signin email');
	define('NEW_PASS', 'New password');
	define('CONFIRM_PASS', 'Confirm password');
	
	define('HOLIDAYS', 'Holidays / Vacations');
	define('NO_HOLIDAYS', 'No holidays or vacations');
	
	define('EDIT_HOLIDAYS', 'Edit Holidays / Vacations');
	
	define('HOURS_OPERATIONS', 'Hours of Operations');
	define('CLOSED', 'Closed');
	define('EDIT_HOURS_OPERATIONS', 'Edit the hours of operations');
	define('MAIN_INTERVAL', 'Main interval');
	define('SECONDARY_INTERVAL', 'Secondary interval (optional)');
	
	define('DELIVERY_ZONES', 'Delivery Zones');
	define('ZONE', 'Zone');
	define('MINIMUM', 'minimum');
	define('FEE', 'Fee');
	define('MINUTES', 'minutes');
	define('DELIVERY_RANGE', 'Range (km)');
	define('DELIVERY_FEE2', 'Delivery fee ($currency)');
	define('MIN_DELIVERY', 'Minimum delivery ($currency)');
	define('ESTIMATED_TIME', 'Estimated time (min.)');
	define('ZONE_UPDATE', 'Update');
	define('ZONE_ADD', 'Add');
	define('CONFIRM_DELIVERY_ZONE', 'Are you sure you want to remove the current delivery zone?');
