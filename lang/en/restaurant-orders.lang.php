<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('TIME_PERIOD', 'Time Period');
	define('TOTAL_ORDERS', 'Total Orders');
	define('TOTAL_SALES', 'Total sales');
	define('TOTAL_EARNINGS', 'Total Earnings');
	define('ALL_TIME', 'All time');
	define('THIS_WEEK', 'This week');
	define('THIS_MONTH', 'This month');
	define('PAST_MONTH', 'Past month');
	define('THIS_YEAR', 'This year');
	
	define('NEW_ORDERS', 'New Orders');
	define('PAST_ORDERS', 'Past Orders');
	
	define('RECEIVED_DATE', 'Received<br />' . 
		'<span>Date / Time</span>');
	define('RECEIVED_FROM', 'From');
	define('ORDER_TYPE', 'Type of Order');
	define('ORDER_AMOUNT', 'Amount');
	define('ORDER_REQUIRED', 'When');
	define('ORDER_STATUS', 'Status');
	
	define('PENDING', 'Pending');
	define('ACCEPT_ORDER', 'Accept');
	define('DECLINE_ORDER', 'Decline');
	define('CONFIRM_ACCEPT', 'Are you sure you want to ACCEPT this order?');
	define('CONFIRM_DECLINE', 'Are you sure you want to DECLINE this order?');
	define('ACCEPTED', 'Accepted');
	define('DECLINED', 'Declined');
	
	define('NO_PENDING_ORDERS', 'No pending orders available.');
	define('VIEW_PAST_ORDERS', 'View past orders');
	define('NO_PAST_ORDERS', 'No past orders available.');
