<?php
	// SEO settings
	define('DEFAULT_SEO_TITLE', 'Venezvite | Food Delivery in Geneva from Local Restaurants');
	define('DEFAULT_SEO_KEYWORDS', 'food delivery geneva, food delivery, wine delivery geneva, wine delivery, delivery food geneva');
	define('DEFAULT_SEO_DESCRIPTION', 'Venezvite is the best way to order food for delivery or takeaway in the Canton of Geneva and the Canton of Vaud. Venezvite is a marketing platform that connects great restaurants with hungry people! Are you hungry? Start your order or join our restaurant network now!');

	define('LOGO_SLOGAN', 'Delivery to your home or Office');

	define('FAVORITED_RESTAURANT', 'Favorited Restaurant');
	define('HELLO', 'Hello');
	define('CITIES', 'Cities');
	define('HELP', 'Help');
	define('LOGIN', 'Sign in');
	define('ORDERS_HISTORY', 'Orders History');
	define('MY_ACCOUNT', 'My Account');
	define('PROFILE', 'Profile');
	define('ADDRESSES', 'Addresses');
	define('PAYMENTS', 'Payments');
	define('RESTAURANT_GALLERY', 'Gallery');
	define('BUSINESS_INFO', 'Business Info');
	define('FINANCIAL_INFO', 'Financial Details');
	define('MENU', 'Menu');
	define('SPICY', 'Spicy');
	define('VEGETARIAN', 'Vegetarian');
	define('NO_GLUTEN', 'Gluten free');
	define('NO_LACTOSE', 'Lactose free');
	define('RECOMMENDED', 'Recommended');
	define('MENU_CATEGORIES', 'Menu Categories');
	define('MENU_OPTION_GROUPS', 'Menu Option Groups');
	define('SEE_PROFILE', 'See My Profile');
	define('LOGOUT', 'Sign out');

	define('FB_LOGIN', '<strong>Sign in</strong> with <strong>facebook</strong>');
	define('OR_', 'or');
	define('USERNAME_CORPORATE', 'username for corporate members');
	define('PASSWORD', 'Password');
	define('KEEP_LOGGED', 'Keep me logged in');
	define('OOPS_FORGOT', 'Oops, I forgot my <a href="{$link}">password</a>');
	define('LOGIN_INCENTIVE_TITLE', 'First order with Venezvite?');
	define('LOGIN_INCENTIVE_DESC', '<strong>You don\'t need to sign in or create an account.</strong> Enter your exact address, place your first order, and we\'ll do the rest!');
	define('INVALID_USER_PASS', 'Invalid user/pass combination');
	define('UNABLE_TO_REGISTER', 'Unfortunately we were unable to register your account!\n' . 
		'Either try to register again, or contact us to report this issue!');
	define('LAST_NAME', 'Last Name');
	define('FIRST_NAME', 'First Name');

	define('PHONE_PREFIX', 'Prefix');
	define('PHONE_NO', 'Phone Number');

	define('RESTAURANTS_LIST_CUSTOM_PATH', 'food-delivery');
	define('BACK', 'Back');

	define('BREADCRUMBS_ENTER_ADDRESS', 'Enter your Address');
	define('BREADCRUMBS_SELECT_RESTAURANT', 'Select a Restaurant');
	define('BREADCRUMBS_CHOOSE_FOOD', 'Choose your food');

	define('REQUIRED', 'required');
	define('OPTIONAL', 'optional');
	define('WHERE_WHEN', 'Where and When?');
	define('DELIVERY', 'Delivery');
	define('TAKEAWAY', 'Take Away');
	define('SELECT_SAVED_LOCATION', 'select a saved location');
	define('SUBMIT_NEW_LOCATION', '... or submit a new one');
	define('SCHEDULED_FOR', 'Schedule for:');
	define('SCHEDULED_TIME', 'Time:');
	define('FIND', 'Find');
	define('SUBMIT', 'Submit');

	define('TODAY', 'Today');
	define('TOMORROW', 'Tomorrow');
	define('YESTERDAY', 'Yesterday');
	define('ASAP', 'ASAP');

	define('NEW_', 'New');
	define('VITE', 'DP');
	define('REVIEWS', 'reviews');
	define('DELIVERY_FEE', 'Delivery Fee');
	define('MINIMUM_ORDER', 'Minimum Order');
	define('APPLY', 'Apply');
	define('CANCEL', 'Cancel');
	define('PRODUCT_TOTAL', 'Product Total:');
	define('CLOSED_NOW', 'Closed for now');

	define('NEXT_DELIVERY_TIME', 'Next available delivery time is {datetime}'); // L'heure choisi a changé.
	define('NEXT_TAKEAWAY_TIME', 'Next available time for a take away order is {datetime}'); // L'heure choisi a changé.

	define('ITEMS', 'items');
	define('CHECKOUT', 'Checkout');
	define('MY_ORDER', 'My Order');
	define('EMPTY_BAG', 'Your bag is hungry!');
	define('EDIT', 'Edit');
	define('ADD', 'Add');
	define('DELETE', 'Delete');
	define('DELIVERY_MIN', 'Delivery minimum');
	define('NO_TAKEAWAY_MIN', 'No minimum for take-away orders!');
	define('HAVE_PROMO', 'Do you have a promo code?');
	define('ENTER_PROMO', 'Enter promo code');
	//define('DELIVERY_ADDRESS_LABEL', 'Please enter your address to confirm if this restaurant delivers to you.');
	define('ENTER_ADDRESS', 'Enter your exact address');
	define('GRAND_TOTAL', 'Grand Total:');

	define('STREET_ADDRESS', 'Street Address');
	define('BUILDING_NO', 'Building Number');
	define('CITY', 'City');
	define('POSTAL_CODE', 'Postal Code');
	define('FLOOR_SUITE', 'Floor / Suite');
	define('ACCESS_CODE', 'Access Code');
	define('SPECIAL_INSTR', 'Special Instruction');
	define('SPECIAL_INSTR_HINT', 'Special delivery instructions');

	define('PARTNER_LOGIN', 'Partner sign in');

	define('FOOTER_WHO_WE_ARE', 'Who We Are');
	define('FOOTER_ABOUT_US', 'About Us');
	define('FOOTER_NEWS_PRESS', 'News &amp; Press');
	define('FOOTER_BLOG', 'Blog');
	define('FOOTER_YOUR_BUSINESS', 'For Your Business');
	define('FOOTER_BECOME_PARTNER', 'Become a Restaurant Partner');

	define('FOOTER_CREATE_ACCOUNT', 'Create a Corporate Account');
	define('FOOTER_CATERING_ORDER', 'Place a Catering Order');
	define('FOOTER_CONTACT_HELP', 'Contact &amp; Help');
	define('FOOTER_HELP_FAQ', 'Help &amp; FAQ');
	define('FOOTER_CONTACT', 'Contact Us');
	define('FOOTER_SUGGEST', 'Suggest a Restaurant');
	define('FOOTER_MORE', 'More+');
	define('FOOTER_SPECIALS', 'Special Offers');

	define('SUCCESSFUL_RECOMMANDATION', 'Thank you for suggesting this restaurant to us.');
	
	define('FOOTER_COPYRIGHT', 'Delivery to your home or office. All rights reserved.');
	define('FOOTER_USE_TERMS', 'Terms of use');
	define('FOOTER_PRIVACY', 'Privacy policy');
	define('FOOTER_SITE_BY', 'Site by');

	define('MORE_MONEY', 'The order minimum hasn\'t been reached' . 
		(@$current_page=='checkout' ? '<br />(OR the current restaurant doesn\'t allow cash payments)' : '') . '.');
	define('CODE_ALREADY_USED', 'It seems you have already used the currently submitted code!' . "\n" . 
		'You cannot use a promo code more than once.');
	define('DA_PROMO_CODE', 'Promo Code');
	define('PROMO_VALID_FROM', 'This promo code is valid {$date}.');
	define('PROMO_VALID_UNTIL', 'This promo code in invalid. It expired {$date}.');
	define('INVALID_PROMO_CODE', 'Invalid promo code!');
	define('LOGIN_FOR_PROMO', 'Please sign in or register before using a promo code.');
	define('PROMO_USED', 'This promo code has already been used.');
	define('PROMO_MIN_VALUE', 'This promo code is valid only on orders {$value} CHF or more.');
	define('INVALID_OR_REGISTERED_PROMO', 'Your code is either invalid, or meant for a specific user. Please sign in and try again!');
	define('EMAIL_IN_USE', 'Email is already in use');

	define('R_EMAIL_CONFIRMATION_SUBJECT', 'Your Venezvite account');
	define('R_EMAIL_CONFIRMATION_BODY', 'Hello {$firstName},

This is a confirmation that your Venezvite account is now active. Congratulations! Your account\'s details are:

 - username: {$username}
 - password: {$password}

Please keep this information safe, so that you can access it easily in case you need it. You can now sign in the website and order food to your heart\'s content!

Thank you for using Venezvite!');

	define('HELLO_RESTAURANT', 'Hello, Restaurant Partner &amp; Owner');
	define('HUNGRY_FOR_BUSINESS', 'Hungry for more business? It\'s Easy. Join venezvite.com');

	define('HOW_IT_WORKS', 'How it works');
	define('HOW_WE_HELP', 'How we help');
	define('JOIN', 'Join');

	define('WHO_ARE_WE', 'Who are we?');
	define('FOR_YOUR_COMPANY', 'For your company');
	define('CONTACT_HELP', 'Contact &amp; Help');
	define('SUBMENU_PLUS', 'Plus +');

	define('ORDER_FROM', 'Order from');
	define('CONTINUE_TO_CHECKOUT', 'Continue to checkout');
	
	define('NO_FAVORITED_RESTAURANT', 'No Favorited Restaurants');
