﻿<?php
    define('AR_EMAIL_UNCONFIRMED', 'You cannot approve this restaurant\'s account, since it hasn\'t confirmed its email address yet!');
    define('AR_SUCCESS', 'Restaurant successfully approved!' . "\n" . 
        'Its representative has just received an email confirmation, containing the path they should use for managing their restaurant\'s details.');
    define('AR_FAILURE', 'Unfortunately the restaurant approval confirmation email message couldn\'t be sent.' . "\n" . 
        'Please try again, or contact the system administrator to report this issue!' . "\n\n" . 
        'The restaurant\'s account hasn\'t been activated.');
    
    define('AR_EMAIL_CONFIRMATION_SUBJECT', 'Your Vezenvite account has been approved');
    define('AR_EMAIL_CONFIRMATION_BODY', 'Hello {$restaurantName},

Your restaurant\'s account has just been approved. Congratulations!

Log in to your account <a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html</a>, with your username and password to finalize your restaurant\'s profile.

Welcome to Venezvite!');
