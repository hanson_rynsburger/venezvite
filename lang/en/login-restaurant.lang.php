<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
    define('LR_LOGIN_UNAPPROVED', 'Your restaurant\'s account hasn\'t been approved yet. Either you haven\'t confirmed your email address, or our team is still analyzing your submitted data.\n' . 
        'Please wait a bit more, or if you\'ve subscribed a few days ago and you still haven\'t received any confirmation from us, please contact us to check your submission\'s status.');
    define('LR_LOGIN_ERROR', 'Oops, we don\'t recognize your sign in info. Please try again or if you forgot your password click on the link above.');
	
	define('LOGIN_ACCOUNT', 'Sign in to your account');
	define('USERNAME', 'Username (your email address)');
	define('FORGOT', 'I forgot');
	
	define('CONFIRM_ORDERS', 'Confirm Orders');
	define('CONFIRM_ORDERS_DESC', 'Confirm incoming orders from computer/tablet/phone');
	define('MANAGE_RESTAURANT', 'Manage your Restaurant');
	define('MANAGE_RESTAURANT_DESC', 'Update menu and restaurant settings');
	define('SALES', 'Sales');
	define('SALES_DESC', 'Complete analysis of your restaurant sales');
