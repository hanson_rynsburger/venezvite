<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('ORDER_CONFIRMATION', 'Order Confirmation');
	
	define('_AT', 'at');
	define('FOR_A', 'for a');
	
	define('THANKS', 'Thank you.');
	define('ORDER_SENT', 'Your order has been sent to the restaurant.');
	define('ORDER_DETAILS', 'Your order (<strong class="green">{order_no}</strong>) for <strong class="green">{order_value}</strong> on {order_date} was sent to <strong class="green">{restaurant}</strong>. An email will be sent to you when the restaurant has confirmed your order.');
	define('YOUR_ORDER_FOR', 'Your order is for:');
	define('CONTACT_RESTAURANT', 'If you have any questions about your order please contact <strong class="green">{restaurant}</strong> at <strong class="green">{restaurant_phone}</strong>');
	
	define('WHAT_NEXT_TITLE', 'What happens now?');
	define('WHAT_NEXT_BODY1', '{restaurant} will confirm your order through our system, which:');
	define('WHAT_NEXT_BODY2', 'Generates a confirmation email that is sent to your inbox, then:');
	define('WHAT_NEXT_BODY3', '{restaurant} prepares and the delivery partner delivers your meal.');

	define('THANK_YOU_YOUR_ORDER', 'Thank you<br/> for your order, {name}');
	define('ESTIMATED_DELIVERY', 'Estimated Delivery: {time}');
	define('PLUS_MINUS', '&plusmn; 15 minutes'); 
	define('ORDER_DETAIL', 'Order details');
	define('ORDERED_FROM', 'Ordered from: ');
	define('DELIVER_TO', 'Deliver to: ');
	
	define('SUB_TOTAL', 'Sub Total');
	define('PROMO_CODE', 'Promo Code');