﻿<?php
	define('FP_TITLE', 'Forgot Your Password?');
	define('FP_USERNAME', 'Username (Your email address)');
	define('FP_RECOVER', 'Recover');
	
	define('FP_EMAIL_SUBJECT', 'Your Venezvite account password change request');
	/*define('FP_EMAIL_BODY', 'Hello {$userName},

A new password request has been registered for your Venezvite account. If you were the one who initiated this request, then please access the link below to obtain a new password:

<a href="{$link}" style="background: #d50008; border-radius: 25px; color: #fff; display: block; line-height: 50px; margin: 0 auto; text-align: center; text-decoration: none; width: 250px;">Reset password</a>

If you haven\'t requested a new password change, then please ignore this message. Your account will remain unchanged.

Thank you for being part of Venezvite!');*/
	
	define('FP_EMAIL_BODY', '<style>table td { line-height: 1.4 }</style>
<table style="background-color: #f2f2f2; width: 100%;">
	<tr><td align="center" valign="top" style="text-align: center;">
		<table align="center" style="width: 600px; background-color: #ffffff; margin-top: 20px; padding: 15px;">
			<tr>
				<td align="center">
<img src="//www.venezvite.com/i/mail/logo.png"  style="margin-top: 60px; margin-bottom: 50px;"/>
				</td>
			</tr>
			<tr>
				<td align="left">
Hi {$userName}<br/><br/>
You\'re receiving this email because you requested a password reset for your user account on venezvite.com.You can set a new password here:<br/><br/>
You can set a new password here:
				</td>
			</tr>
			<tr>
				<td align="center">
					<a href="{$link}" style="background: #d50008; border-radius: 25px; color: #fff; display: block; line-height: 50px; margin: 0 auto; text-align: center; text-decoration: none; width: 250px; margin-top: 30px; margin-bottom: 20px;">Reset password</a>
				</td>
			</tr>
			<tr>
				<td align="left">
Thank you for using venezvite.com<br/><br/>
Best,<br/>The Venezvite Team
				</td>
			</tr>
			<tr>
				<td align="center" style="padding-top: 90px; padding-bottom: 20px;">
					<a href="https://www.facebook.com/likevenezvite" style="line-height: 14px; font-size: 14px; text-decoration: none; margin-right: 10px; color: #99aac9;"><img src="//www.venezvite.com/i/mail/facebook.png" style="vertical-align: middle; margin-right: 5px;"/> likevenezvite</a>
					<a href="https://twitter.com/venezvite" style="line-height: 14px; font-size: 14px; text-decoration: none; margin-right: 10px; color: #99aac9;"><img src="//www.venezvite.com/i/mail/twitter.png" style="vertical-align: middle; margin-right: 5px;" />#venezvite</a>
					<a href="http://venezvite.com" style="line-height: 14px; font-size: 14px; text-decoration: none; color: #99aac9;"><img src="//www.venezvite.com/i/mail/link.png" style="vertical-align: middle; margin-right: 5px;" />venezvite.com</a>
				</td>
			</tr>
		</table>
	</td></tr>
	<tr>
		<td align="center">
			<img src="//www.venezvite.com/i/mail/logo@2x.png"  style="margin-top: 60px; margin-bottom: 40px;"/>
			<br/>
			<div style="font-size: 12px; font-style: italic; margin-bottom: 30px;">Copyright &copy; 2016 Venezvite, All rights reserved.</div>
		<td>
	</tr>
</table>
');
	
	define('FP_MESSAGE', 'If the email address you entered matches our records, you will receive an email with instructions to reset your password. Thank you!');
