<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('INVALID_FILE', 'One or more files were not uploaded. We do not support that file type.');
	define('CANT_SAVE_RESTAURANT', 'Unfortunately, we weren\'t able to save your restaurant\'s details. Please try again or contact us to report this issue.');
	define('REGISTRATION_CONFIRMATION', 'In a few seconds you will receive an email. To complete your registration, please click on the link to confirm your email address.');
	define('SOME_ERRORS', 'There were some errors while trying to create your account:');
	define('REGISTRATION_SUCCESS', 'You have successfully registered your restaurant!');
	
	define('CONTACT_INFO', 'Contact info <span>* (' . REQUIRED . ')</span>');
	define('MOBILE', 'Mobile');
	
	define('RESTAURANT_INFO', 'Restaurant information');
	define('SELECT_COUNTRY', 'Select your country');
	define('RESTAURANT_EMAIL', 'Email address');
	define('RESTAURANT_NAME', 'Restaurant name');
	define('RESTAURANT_ADDRESS', 'Restaurant street address');
	define('RESTAURANT_CITY', 'City / Village / Town * (You must select from the list)');
	define('RESTAURANT_PHONE', 'Restaurant phone');
	define('ENTER_VALID_PHONE', 'Enter a valid phone number');
	define('RESTAURANT_URL', 'Restaurant website');
	define('RESTAURANT_DESC', 'Restaurant Info (Describe your restaurant)');
	
	define('PRIMARY_CUISINE_TYPE', 'Select your primary cuisine type(s) <span>Select as many</span>');
	define('CHOOSE_CUISINE', 'Choose cuisine');
	define('CHOOSE_SERVICES', 'Please check the services that you provide');
	define('CATERING', 'Catering');
	define('CUSTOM_DELIVERY', 'We do not deliver and would like to use a delivery partner.');
	define('SELECT_TIME', 'Select time');
	
	define('HOURS_OPERATION', 'Hours of operations');
	define('SAME_EVERY_DAY', 'Apply same hours of operations for every day');
	define('DELIVERY_ZONES', 'Delivery zones');
	define('ZONE', 'Zone 1');
	define('ZONE_RANGE', 'Zone 1 range in km');
	define('MIN_DELIVERY', 'Minimum order for zone 1 deliveries');
	define('DELIVERY_FEE_ZONE', 'Delivery fee for zone 1 deliveries');
	define('SELECT_TIME_INTERVAL', 'Average delivery time (transportation only)');
	define('ADD_ZONE', 'Add another zone');
	define('PHOTOS', 'Photos');
	define('UPLOAD_PHOTOS', 'Upload photos (.jpg or .png)');
	define('UPLOAD_MENU', 'Upload menu (.docx, .pdf or .jpg)');
	
	define('CREATE_ACCOUNT_TITLE', 'Create your account');
	define('USERNAME', 'Username: Enter your email address');
	define('CONFIRM_USERNAME', 'Confirm your email address');
	define('CONFIRM_PASSWORD', 'Confirm password');
	define('AGREE_TERMS', 'I accept the <a class="popup" href="{$link}">terms and conditions</a>.');
	define('CREATE_ACCOUNT', 'Create an account');
	
	define('INVALID_ADDRESS', 'Oops! {address} doesn\'t seem to be a valid address!');
	
	define('RR_EMAIL_CONFIRMATION_SUBJECT', 'Confirm your Venezvite account');
	define('RR_EMAIL_CONFIRMATION_BODY', 'Hello {$restaurantName},

In order for your restaurant\'s account to be enabled by one of our team\'s members, please first confirm your email address, by clicking on the link below:

<a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'en') . '/confirm-restaurant.html?check={$uniqueHash}">' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . (@$_SESSION['s_venezvite']['language'] ? $_SESSION['s_venezvite']['language']->languageAcronym : 'en') . '/confirm-restaurant.html?check={$uniqueHash}</a>

Thank you for choosing Venezvite!');
