﻿<?php
	define('CPC_ADMIN_PANEL', 'Forgot Your Password?');
	
	define('CPC_TITLE', 'Change your Venezvite account\'s password');
	define('CPC_PASSWORD', 'New password');
	define('CPC_CONFIRM_PASSWORD', 'Confirm new password');
	define('CPC_UPDATE', 'Update password');
	
	define('CPC_EMAIL_SUBJECT', 'Your new Venezvite account password');
	define('CPC_EMAIL_BODY', 'Hello {$userName},

Here is your new Venezvite account password, changed on your request:

<strong>{$password}</strong>

You may now use it and log in your account.');
	
	define('CPC_SUCCESS', 'You have successfully confirmed your password request!' . "\n" . 
		'Please check your email account to find out your new Venezvite account\'s password.');
	define('CPC_ERROR', 'Unfortunately we could\'t send you a new password via email, so your old password hasn\'t been changed yet.\n' . 
		'Please try confirming your password change request again, or contact us to report this isssue!');
