﻿<?php
	define('AO_EMAIL_TITLE', 'Your order on Venezvite was accepted');
	define('AO_EMAIL_BODY', 'Hello {$userName},

Your order placed on Venezvite has just been accepted by {$restaurantName}.

Thank you for ordering with Venezvite.');
