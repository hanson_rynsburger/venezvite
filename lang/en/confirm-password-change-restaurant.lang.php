﻿<?php
	define('CPCR_ADMIN_PANEL', 'Our Partner Admin Panel');
	
	define('CPCR_TITLE', 'Change your Venezvite account\'s password');
	define('CPCR_PASSWORD', 'New password');
	define('CPCR_CONFIRM_PASSWORD', 'Confirm new password');
	define('CPCR_UPDATE', 'Update password');
	
	define('CPCR_EMAIL_SUBJECT', 'Your new Venezvite account password');
	define('CPCR_EMAIL_BODY', 'Hello {$restaurantName},

Here is your new Venezvite account password, changed on your request:

<strong>{$password}</strong>

You may now use it and log in your account, by accessing <a href="{$link}">{$link}</a>');
	
	define('CPCR_SUCCESS', 'You have successfully confirmed your password request!' . "\n" . 
		'Please check your email account to find out your new Venezvite account\'s password.');
	define('CRCR_ERROR', 'Unfortunately we could\'t send you a new password via email, so your old password hasn\'t been changed yet.\n' . 
		'Please try confirming your password change request again, or contact us to report this isssue!');
