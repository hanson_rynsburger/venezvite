﻿<?php
    define('CR_SUCCESS', 'Thank you for confirming your email address!' . "\n" . 
        'Next please wait until one of our team\'s members will approve your restaurant\'s account. You will be notified via email as soon as this happens!');
