<?php
	// SEO settings
	define('SEO_TITLE', '');
	define('SEO_KEYWORDS', '');
	define('SEO_DESCRIPTION', '');
	
	
	define('HUNGRY_TITLE', 'Order. <span>Get it delivered.</span>');
	define('ALREADY_MEMBER', 'Already a member? Log in');
	define('NOW_ACCEPTING', 'Now accepting');
	
	define('EASY_ORDERING_TITLE', 'Easy online ordering');
	define('EASY_ORDERING_BODY', 'No need to pick up the phone. Quick and accurate ordering!');
	define('MORE_FOOD_TITLE', 'More than food');
	define('MORE_FOOD_BODY', 'You can also get beer, wine, liquor, groceries and more.');
	define('DISCOUNTS_TITLE', 'Discounts &amp; deals');
	define('DISCOUNTS_BODY', 'Get exclusive offers and save on your delivery favorites.');
	define('TELL_FRIENDS_TITLE', 'Tell your friends');
	define('TELL_FRIENDS_BODY', 'Tell your friends about us and each of you can get 4 CHF.');
	
	define('DONT_BE_SHY', 'The tastemakers\' choice');
	define('CLICKS_AWAY', '4 clicks away from food/wine happiness!');
	
	define('BROWSE_CITY', 'Browse By City:');
	
	define('PERFECT_BUSINESS', 'Perfect for
business lunches');
	define('CREATE_CORP_ACCOUNT', 'Create a corporate account for group ordering!');
	define('CREATE_CORP_ACCOUNT2', 'Create a corporate account');
	define('LEARN_MORE', 'Learn more');

	define('BEST_RESTAURANT', 'The best restaurants are here');
	
	define('STAY_CONNECTED', 'Stay Connected.');
	define('NEWSLETTER_TITLE', 'Join our mailing list for special offers');
	define('NEWSLETTER_BODY', 'Venezvite is the best way to order food for delivery and take-away. Enter your e-mail address to receive promo deals.');
	define('ENTER_EMAIL', 'Enter your email address');
	
	define('HOW_WORKS_TITLE', 'How it works?');
	define('HOW_WORKS_BODY1', 'Place your order');
	define('HOW_WORKS_BODY2', 'Order is beamed to the restaurant');
	define('HOW_WORKS_BODY3', 'Yippie! Your food is delivered!');
	
	define('HELLO_RESTAURANT_OWNERS', 'Hello Restaurant Owner,
Become a Restaurant Partner today!');
	define('JOIN_TITLE', 'Join our Restaurant Network');
	define('JOIN_BODY', 'Become a Restaurant Partner. Find out how we can help you grow your business, make new customers, increase your daily revenue.');
	define('NO_DELIVER_TITLE', 'Don\'t deliver? Don\'t worry!');
	define('NO_DELIVER_BODY', 'Our delivery partners in your local city will deliver for you. Simply prepare the food and the delivery partner picks it up and delivers.');
	define('JOIN_NOW', 'Join now');
	
	define('ABOUT_TITLE', 'About Us');
	define('ABOUT_BODY', 'Venezvite is the best, easiest, and fastest way to order food and wine straight to your house or office... or anywhere! Get your food and wine delivered straight to your door from the best restaurants near you.');
