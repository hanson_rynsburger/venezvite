<?php
	define('PAYMENT_TYPE', 'Payment Type');
	define('CC', 'Credit Card');
	define('DELIVERY_CASH', 'Cash on Delivery');
	define('TAKEAWAY_CASH', 'Cash on Takeaway');
	define('NO_LATER_THAN', 'No later than');
	define('MINUTES', 'minutes');
	define('NOTES', 'Notes');
	define('CUSTOMER', 'Customer');
	define('DELIVERY_DATETIME', 'Delivery Date/Time');
	define('DELIVERY_TO', 'Delivery to');
	define('FLOOR', 'Floor/Access Code');
	define('DELIVERY_INSTRUCTIONS', 'Delivery Instructions');
	define('TAKEAWAY_DATETIME', 'Takeaway Date/Time');
	define('TOTAL_PRODUCTS', 'Total Products');
	define('DISCOUNT', 'Discount');
	define('TOTAL', 'Total');
