<?php
    if (!empty($is_included) && !empty($_GET['check']) && strlen($_GET['check'])==40) {
        $restaurants = restaurant::listUnapproved();
        foreach ($restaurants as $restaurant) {
            if ($_GET['check']==$restaurant->encodeID4Approval()) {
                // We've found the restaurant pending approval
                // Now let's see if its email account has been confirmed
                if (empty($restaurant->dateValidated)) {
                    // It hasn't been, so we cannot approve this account yet
                    $_SESSION['s_venezvite']['error'] = array(AR_EMAIL_UNCONFIRMED);
                } else {
                    // Everything's OK, let's approve it
                    if ($restaurant->approve()) {
                        $_SESSION['s_venezvite']['error'] = array(AR_SUCCESS);
                    } else {
                        $_SESSION['s_venezvite']['error'] = array(AR_FAILURE);
                    }
                }
                
                break;
            }
        }
        
        // It seems we couldn't find any restaurant matching the submitted key
    }
    
    // Redirect the visitor to the homepage
    header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
    die();
