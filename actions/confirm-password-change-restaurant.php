<?php
	if (!@$is_included || strlen(@$_GET['check'])!=32 || !is_numeric(@$_GET['id']) || 
		!($restaurantAdmin = restaurantAdmin::getByID($_GET['id'])) || $_GET['check']!=$restaurantAdmin->restaurant->encodeID4Confirmation()) {
		
		// Redirect the visitor to the homepage
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	}
	
	if (@trim($_POST['new_password'])!='' && $_POST['new_password']==@$_POST['confirm_new_password']) {
		// The currently submitted restaurant seem to be legit,
		// so let's change its password
		$success = $restaurantAdmin->resetPassword($_POST['new_password']);
		
		if ($success) {
			$_SESSION['s_venezvite']['error'][] = CPCR_SUCCESS;
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
			die();
			
		} else {
			$_SESSION['s_venezvite']['error'][] = CRCR_ERROR;
		}
	}
	
	unset($restaurantAdmin);
