<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		@is_array($_POST['menu_option_groups'])) {
		
		$order = 1;
		foreach ($_POST['menu_option_groups'] as $id_menu_option_group) {
			if (is_numeric($id_menu_option_group) && ($menu_option_group = menuItemCategory::get_by_id($id_menu_option_group)) && 
				$menu_option_group->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
				// The current restaurant admin is allowed to change the submitted menu category's order
				
				$menu_option_group->reorder($order);
				$order++;
			}
		}
	}
	
	echo json_encode(array());
	die();
