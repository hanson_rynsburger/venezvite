<?php
	if (@$is_included) {
		
		if (!empty($restaurantAdmin)) {
			// There's no reason for the authenticated restaurant admins to access this page
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/restaurant-orders.html');
			die();
		}
		
		if (!empty($_POST['username']) && isEmail(checkQuotes($_POST['username']))) {
			// It seems the login form has been submitted (correctly)
			$restaurantAdmin = restaurantAdmin::getByEmail(checkQuotes($_POST['username']));
			
			if ($restaurantAdmin) {
				$restaurantAdmin->sendPasswordResetMessage();
				unset($restaurantAdmin); // We don't want to log in the admin...
			}
			
			$_SESSION['s_venezvite']['error'][] = FPR_MESSAGE;
		}
	}
