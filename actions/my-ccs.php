<?php
	if (!@$is_included || empty($entity)) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	}
	
	$user_payment_tokens = userPaymentToken::list_($entity);
	
	if (is_numeric(@$_POST['del_'])) {
		foreach ($user_payment_tokens as $user_payment_token) {
			if ($user_payment_token->id==$_POST['del_']) {
				// Delete the payment token
				$user_payment_token->remove();
				
				echo json_encode(array('success' => true));
				die();
			}
		}
	}
