<?php
	if (!@$is_included) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	} elseif (empty($restaurantAdmin)) {
		// Go to the login page
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
		die();
	}
	
	
	
	if (!empty($_POST['options_group']) && 
		(empty($_POST['selectables']) || is_numeric($_POST['selectables'])) && 
		in_array(@$_POST['selectables_rule'], array('E', 'UT', 'AL')) && 
		($options = explode(',', @$_POST['options'])) && !empty($options)) {
		
		$options_group_name = checkQuotes($_POST['options_group']);
		
		if (@is_numeric($_POST['id']) && ($options_group = menuItemCategory::get_by_id($_POST['id'])) && 
			$options_group->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
			
			$options_group->update_new(array(
					'menuItemCategory'	=> $options_group_name, 
					'selectables'		=> $_POST['selectables'], 
					'selectablesRule'	=> $_POST['selectables_rule'], 
					'menuItemOptionIDs'	=> json_encode($options)
				));
			//$options_group->deleteOptions();
			
		} else {
			$options_group = menuItemCategory::insert_new(array(
					'idRestaurant'		=> $restaurantAdmin->restaurant->idRestaurant, 
					'menuItemCategory'	=> $options_group_name, 
					'selectables'		=> $_POST['selectables'], 
					'selectablesRule'	=> $_POST['selectables_rule'], 
					'menuItemOptionIDs'	=> json_encode($options)
				));
		}
		
		
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/menu-option-groups.html' . (!empty($options_group) ? '#menu_option_group_' . $options_group->idMenuItemCategory : ''));
		die();
	}
	
	
	
	$menu_options_groups = menuItemCategory::list_new($restaurantAdmin->restaurant, true);
	$menu_options = menuItemOption::list_($restaurantAdmin->restaurant);
	$menu_items = menuItem::list_($restaurantAdmin->restaurant, false, true, true);
