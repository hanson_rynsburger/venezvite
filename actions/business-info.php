<?php
	if (!@$is_included) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	} elseif (empty($restaurantAdmin)) {
		// Go to the login page
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
		die();
	}
	
	if (!empty($_POST['name']) && !empty($_POST['address']) && !empty($_POST['phone']) && !empty($_POST['description'])) {
		$output = array(
			'success'	=> true
		);

		$restaurantAdmin->restaurant->update(array(
				'restaurantName'		=> $_POST['name'], 
				'address'				=> $_POST['address'], 
				'website'				=> @$_POST['website'], 
				'phone'					=> $_POST['phone'], 
				'mobile'				=> @$_POST['mobile'], 
				'restaurantDescription'	=> $_POST['description']
			));
		$restaurantAdmin->serialize();
		
		$output['fields'] = array(
			'entity-name'			=> htmlspecialchars($_POST['name']), 
			'entity-address'		=> htmlspecialchars($_POST['address']) . "\n" . htmlspecialchars($restaurantAdmin->restaurant->city . ', ' . $restaurantAdmin->restaurant->zipCode), 
			'entity-phone'			=> htmlspecialchars($_POST['phone']), 
			'entity-mobile'			=> (!empty($_POST['mobile']) ? htmlspecialchars($_POST['mobile']) . "\n" : ''), 
			'entity-website'		=> (!empty($_POST['website']) ? htmlspecialchars($_POST['website']) . "\n" : ''), 
			'entity-description'	=> htmlspecialchars($_POST['description']), 
			'top-user-name'			=> htmlspecialchars($_POST['name'])
		);
		
		echo json_encode($output);
		die();
		
	} elseif (!empty($_POST['first_name']) && !empty($_POST['last_name']) && !empty($_POST['admin_email']) && !empty($_POST['email'])) {
		$output = array(
			'success'	=> true
		);
		
		$restaurantAdmin->restaurant->update(array(
				'contactFirstName'	=> $_POST['first_name'], 
				'contactLastName'	=> $_POST['last_name'], 
				'email'				=> $_POST['email']
			));
		$restaurantAdmin->serialize();
		
		$admin_update = array('email' => $_POST['admin_email']);
		if (!empty($_POST['password']) && !empty($_POST['confirm_password']) && $_POST['password']==$_POST['confirm_password']) {
			$admin_update['password'] = $restaurantAdmin->encodeAuthentication($_POST['password']);
		}
		$restaurantAdmin->update($admin_update);
		
		$output['fields'] = array(
			'entity-first-name'		=> htmlspecialchars($_POST['first_name']), 
			'entity-last-name'		=> htmlspecialchars($_POST['last_name']), 
			'entity-admin-email'	=> htmlspecialchars($_POST['admin_email']), 
			'entity-email'			=> htmlspecialchars($_POST['email'])
		);
		
		echo json_encode($output);
		die();
		
	} elseif (!empty($_POST['first_start']) && is_array($_POST['first_start']) && count($_POST['first_start'])==7 && 
		!empty($_POST['first_end']) && is_array($_POST['first_end']) && count($_POST['first_end'])==7 && 
		!empty($_POST['last_start']) && is_array($_POST['last_start']) && count($_POST['last_start'])==7 && 
		!empty($_POST['last_end']) && is_array($_POST['last_end']) && count($_POST['last_end'])==7) {

		$restaurantAdmin->restaurant->clearTimetable();

		for ($i=0; $i<7; $i++) {
			$hours = array();
			if (!empty($_POST['first_start'][$i]) && !empty($_POST['first_end'][$i])) {
				$hours[] = array(
					'start'	=> $_POST['first_start'][$i], 
					'end'	=> $_POST['first_end'][$i]
				);
			}

			if (!empty($_POST['last_start'][$i]) && !empty($_POST['last_end'][$i])) {
				$hours[] = array(
					'start'	=> $_POST['last_start'][$i], 
					'end'	=> $_POST['last_end'][$i]
				);
			}

			$restaurantAdmin->restaurant->insertOperationHours(($i + 1), $hours);
		}

		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/business-info.html#entity-hours');
		die();
	}
	elseif (!empty($_POST['customdate'])) {
		$cdate = date('Y-m-d', strtotime( $_POST['customdate'] ));
		$cttrowid = $restaurantAdmin->restaurant->insertCustomTimetable( $cdate, $_POST['start'], $_POST['end'] );

		if ( intval( $cttrowid ) == 0 ) {
			echo json_encode(
					array(
							'success'		=> false,
							'message' 		=> $cttrowid
					)
			);
		} 
		else {
			echo json_encode(
				array(
					'success'		=> true, 
					'newdate' 		=> $cdate,
					'newdate_id'	=> $cttrowid
				)
			);
		}

		die;
	}
	elseif (!empty($_POST['delctt']) && !empty($_POST['delid'])) {
		$restaurantAdmin->restaurant->deleteCustomTimetable( $_POST['delid'] );
		echo json_encode(
				array(
						'success'		=> true
				)
		);

		die;
	}
	
	$timetable = $restaurantAdmin->restaurant->listTimetable();
	//$deliveryHours_ = $restaurantAdmin->restaurant->listDeliveryHours();
	$delivery_zones = deliveryZone::list_($restaurantAdmin->restaurant);

	$customTimetable = $restaurantAdmin->restaurant->getRestaurantCustomTimetable();