<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		@is_numeric($_POST['menu_category']) && ($menu_category = menuCategory::getByID($_POST['menu_category'])) && 
		$menu_category->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
		
		// The current restaurant admin is allowed to remove the submitted menu category
		$menu_items = menuItem::list_($restaurantAdmin->restaurant, false, true, true);
		$used_category = false;
		foreach ($menu_items as $menu_item) {
			if ($menu_item->menuCategory->idMenuCategory==$menu_category->idMenuCategory) {
				$used_category = true;
				break;
			}
		}
		
		if (!$used_category) {
			$menu_category->delete();
			
			echo json_encode(array(
					'success' => true
				));
			die();
		}		
	}
	
	echo json_encode(array());
	die();
