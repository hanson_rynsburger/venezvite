<?php
	if (!@$is_included || strlen(@$_GET['check'])!=32 || !is_numeric(@$_GET['id']) || 
		!($user = user::getByID($_GET['id'])) || $_GET['check']!=$user->encodeID4Confirmation()) {
		
		// Redirect the visitor to the homepage
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	}
	
	if (@trim($_POST['new_password'])!='' && $_POST['new_password']==@$_POST['confirm_new_password']) {
		// The currently submitted user seem to be legit,
		// so let's change its password
		$success = $user->resetPassword($_POST['new_password']);
		
		if ($success) {
			$_SESSION['s_venezvite']['error'][] = CPC_SUCCESS;
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
			die();
			
		} else {
			$_SESSION['s_venezvite']['error'][] = CPC_ERROR;
		}
	}
	
	unset($user);
