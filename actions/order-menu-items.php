<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		@is_array($_POST['menu_items'])) {
		
		$order = 1;
		foreach ($_POST['menu_items'] as $id_menu_item) {
			if (is_numeric($id_menu_item) && ($menu_item = menuItem::getByID($id_menu_item)) && 
				$menu_item->menu->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
				// The current restaurant admin is allowed to change the submitted menu category's order
				
				$menu_item->reorder($order);
				$order++;
			}
		}
	}
	
	echo json_encode(array());
	die();
