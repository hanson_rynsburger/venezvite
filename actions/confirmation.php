<?php
	if (!@$is_included || !is_array(@$_SESSION['s_venezvite']['order']) || empty($entity)) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	}
	
	
	setlocale(LC_TIME, $_SESSION['s_venezvite']['language']->localeCode); // 'fra'
	$current_datetime = strftime('%d %B %Y ' . _AT . ' %H:%M', time());

	$timestamp = strtotime($_SESSION['s_venezvite']['order']['date']);
	switch (date('Y-m-d', $timestamp)) {
		case date('Y-m-d'):
			$day = TODAY;
			break;
		case date('Y-m-d', strtotime('+1 day')):
			$day = TOMORROW;
			break;
	}
	$order_datetime_type = (@$day ? $day . ', ' : '') . strftime('%d %B %Y ' . FOR_A . ' %H:%M', $timestamp) . ' ' . ($_SESSION['s_venezvite']['order']['type']!='T' ? DELIVERY : TAKEAWAY);
	
	$order_date = (@$day ? $day . ', ' : '') . strftime( '%d %B %Y ', $timestamp );
	$order_time = strftime( '%H:%M', $timestamp );
	$order_type = ($_SESSION['s_venezvite']['order']['type']!='T' ? DELIVERY : TAKEAWAY);
