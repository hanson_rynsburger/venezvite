<?php
	if (@$is_included && 
		@is_numeric($_POST['order']) && ($order = order::getByID($_POST['order'])) && 
		((!empty($entity) && (get_class($entity)=='user' ? 
				!empty($order->user) && $order->user->idUser==$entity->idUser : 
				!empty($order->corporateAccount) && $order->corporateAccount->idCorporateAccount==$entity->idCorporateAccount)) || 
			(!empty($restaurantAdmin) && 
				$order->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant))
	) {
		/*
		echo json_encode(array(
				'items'			=> $order->items, 
				'promo_code'	=> ($order->promoCode ? $order->promoCode->value : null), 
				'delivery'		=> $order->deliveryCost, 
				'discount'		=> $order->discount, 
				'value'			=> $order->value, 
				'customer'		=> ($order->user ? $order->user : $order->corporateAccount), 
				'address'		=> $order->userAddress, 
				'notes'			=> $order->notes, 
				'date'			=> $order->dateDesired
			));
		*/
		echo (require REL . '/views/inc/order-row.inc.php');
		
	} else {
		// Nothing to do here
		//echo json_encode(array());
		echo '';
	}
	
	die();
