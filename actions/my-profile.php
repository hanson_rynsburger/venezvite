<?php
	if (!@$is_included || empty($entity)) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	}

	if (!empty($_POST['first_name']) && !empty($_POST['last_name'])) {
		$output = array(
			'success'	=> true
		);
		
		if (get_class($entity)=='user') {
			$user->update($_POST['first_name'], $_POST['last_name'], $entity->email, '', $entity->phone);
			
			$output['fields'] = array(
				'entity-first-name'	=> htmlspecialchars($_POST['first_name']), 
				'entity-last-name'	=> htmlspecialchars($_POST['last_name']), 
				'top-user-name'		=> htmlspecialchars($_POST['first_name'])
			);
		}
		
		echo json_encode($output);
		die();
		
	} elseif (!empty($_POST['email'])) {
		$output = array(
			'success'	=> true
		);
		
		if (get_class($entity)=='user') {
			$user->update($entity->firstName, $entity->lastName, $_POST['email'], '', $entity->phone);
			
			$output['fields'] = array(
				'entity-email-address' => htmlspecialchars($_POST['email'])
			);
		}
		
		echo json_encode($output);
		die();

	} elseif (!empty($_POST['password']) && !empty($_POST['confirm_password']) && $_POST['password']==$_POST['confirm_password']) {
		if (get_class($entity)=='user') {
			$user->update($entity->firstName, $entity->lastName, $entity->email, $_POST['password'], $entity->phone);
		}
		
		echo json_encode(array('success' => true));
		die();
	} elseif (!empty($_POST['phone'])) {
		$output = array(
			'success'	=> true
		);

		if (get_class($entity)=='user') {
			$user->update($entity->firstName, $entity->lastName, $entity->email, '', $_POST['phone']);
			
			$output['fields'] = array(
				'entity-phone-number' => htmlspecialchars($_POST['phone'])
			);
		}

		echo json_encode($output);
		die();
	}
