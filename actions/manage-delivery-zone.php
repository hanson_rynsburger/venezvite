<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		/*isset($_POST['id']) && (empty($_POST['id']) || (is_numeric($_POST['id']) && ($menu_item_option = menuItemOption::getByID($_POST['id'])))) && 
		!empty($_POST['option_name']) && isset($_POST['price']) && (empty($_POST['price']) || is_numeric($_POST['price']))*/
		isset($_POST['zone_id']) && (empty($_POST['zone_id']) || (is_numeric($_POST['zone_id']) && ($delivery_zone = deliveryZone::getByID($_POST['zone_id'])) && 
			$delivery_zone->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant)) && 
		!empty($_POST['paths']) && !empty($_POST['range']) && 
		isset($_POST['fee']) && (empty($_POST['fee']) || is_numeric($_POST['fee'])) && 
		isset($_POST['minimum']) && (empty($_POST['minimum']) || is_numeric($_POST['minimum'])) && 
		!empty($_POST['time']) && isset($_POST['is_delete'])) {
		
		
		if ($restaurantAdmin->restaurant->delivery!='Y' || $restaurantAdmin->restaurant->customDelivery=='Y') {
			// Restaurants that don't offer delivery can't have delivery zones, 
			// and the one we're delivering for can't edit their zones
			echo json_encode(array());
			die();
		}
		
		if (!empty($delivery_zone)) {
			if ($_POST['is_delete']!='true') {
				// Should be update
				// First let's check if the new range is different than the original one
				// If it is, we'll also redefine the zone's polygon (based on the new range)
				if ($_POST['range']!=$delivery_zone->range) {
					$_POST['paths'] = deliveryZone::generatePath($restaurantAdmin->restaurant->latitude, $restaurantAdmin->restaurant->longitude, $_POST['range']);
				}
				
				$delivery_zone->update(array(
						'range'				=> $_POST['range'], 
						'coordinates'		=> json_encode($_POST['paths']), 
						'deliveryFee'		=> $_POST['fee'], 
						'minimumDelivery'	=> $_POST['minimum'], 
						'estimatedTime'		=> $_POST['time'], 
					));
				
			} else {
				// Remove
				$delivery_zone->remove();
			}
			
		} else {
			// Insert
			deliveryZone::insert($restaurantAdmin->restaurant, $_POST['range'], $_POST['fee'], $_POST['time'], $_POST['minimum'], json_encode($_POST['paths']));
		}
		
		echo json_encode(array(
				'success' => true
			));
		die();
	}
	
	echo json_encode(array());
	die();
