<?php
	if (@$is_included && in_array(@$_POST['content'], array('footer-contact', 'footer-suggestion', 'footer-specials'))) {
		
		echo '<div class="modal-body modal-body-wide">' . 
				'<a class="close" href="javascript:;">&times;</a>';
		require_once REL . '/views/inc/' . $_POST['content'] . '.inc.php';
		echo '</div>';
	}
	
	die();
