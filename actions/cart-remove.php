<?php
	if (@$is_included && is_numeric(@$_POST['index']) && !empty($_POST['restaurant'])) {
		
		if (!empty($_SESSION['s_venezvite']['carts'][$_POST['restaurant']][$_POST['index']])) {
			unset($_SESSION['s_venezvite']['carts'][$_POST['restaurant']][$_POST['index']]);
			$_SESSION['s_venezvite']['carts'][$_POST['restaurant']] = array_values($_SESSION['s_venezvite']['carts'][$_POST['restaurant']]);
			
			echo json_encode(array(
					'success' => true
				));
			die();
		}
	}
	
	echo json_encode(array());
	die();
