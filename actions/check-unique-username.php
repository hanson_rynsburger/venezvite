<?php
	if (@$is_included && !empty($_POST['username'])) {
		if (@$_POST['corporate']) {
			$already_exists = corporateAccount::checkDuplicateUsername($_POST['username']);
		
			if (!$already_exists) {
				// Since we have a unified login, let's check through the users usernames, too
				$already_exists = user::checkDuplicateEmail($_POST['username']);
			}
			
		} else {
			$already_exists = restaurantAdmin::getByEmail($_POST['username']);
		}
		
		if (!$already_exists) {
			// The submitted restaurant username is not used by any of the registered restaurants
			// (or corporate accounts)
			echo 'true';
			die();
		}
	}
	
	echo 'false';
	die();
