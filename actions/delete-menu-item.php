<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		@is_numeric($_POST['menu_item']) && ($menu_item = menuItem::getByID($_POST['menu_item'])) && 
		$menu_item->menu->idMenu==$restaurantAdmin->restaurant->menus[0]->idMenu) {
		
		// The current restaurant admin is allowed to change the submitted menu item's state
		$menu_item->delete();
		
		echo json_encode(array(
				'success' => true
			));
		die();		
	}
	
	echo json_encode(array());
	die();
