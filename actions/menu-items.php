<?php
	if (!@$is_included) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	} elseif (empty($restaurantAdmin)) {
		// Go to the login page
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
		die();
	}
	
	
	
	if (@is_numeric($_POST['category']) && ($menu_category = menuCategory::getByID($_POST['category'])) && 
		!empty($_POST['menu_item']) && isset($_POST['description']) && @is_numeric($_POST['price']) && 
		isset($_POST['discount']) && (empty($_POST['discount']) || is_numeric($_POST['discount']))) {
		
		//var_dump($_FILES);
		//var_dump($_POST);
		//die();
		
		$menu_item_name = checkQuotes($_POST['menu_item']);
		$description = checkQuotes(@$_POST['description']);
		
		// Check if a photo has been uploaded
		$photo = false;
		if (!empty($_FILES['photo']) && ($size = @getimagesize($_FILES['photo']['tmp_name']))) {
			// This must be an image
			$file = filefy($_FILES['photo']['name']);
			
			if (is_file(REL . '/i/restaurants/' . $file)) {
				$file = getUniqueFile($file, REL . '/i/restaurants/');
			}
			
			if (move_uploaded_file($_FILES['photo']['tmp_name'], REL . '/i/restaurants/' . $file)) {
				if ($size[0]>680 || $size[1]>680) {
					require_once REL . '/classes/phpThumb/ThumbLib.inc.php';
					
					$thumb = PhpThumbFactory::create(REL . '/i/restaurants/' . $file);
					$thumb->adaptiveResize(680, 680)->save(REL . '/i/restaurants/' . $file);
				}
				
				$photo = $file;
			}
		}
		
		$price = $_POST['price'];
		$discount = (@is_numeric($_POST['discount']) ? ($_POST['discount']>100 ? 100 : $_POST['discount']) : 0);
		$spicy = (@$_POST['spicy']=='Y' ? 'Y' : 'N');
		$veggie = (@$_POST['veggie']=='Y' ? 'Y' : 'N');
		$glut_free = (@$_POST['glut_free']=='Y' ? 'Y' : 'N');
		$lact_free = (@$_POST['lact_free']=='Y' ? 'Y' : 'N');
		$recommended = (@$_POST['recommended']=='Y' ? 'Y' : 'N');
		$enabled = (@$_POST['enabled']=='Y' ? 'Y' : 'N');
		
		$menu_option_groups = array();
		if (@$_POST['option_groups']) {
			$option_groups = explode(',', $_POST['option_groups']);
			foreach ($option_groups as $option_group) {
				if (is_numeric($option_group) && ($menu_option_group = menuItemCategory::get_by_id($option_group)) && 
					$menu_option_group->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
					
					$menu_option_groups[] = $option_group;
				}
			}
		}
		
		if (@is_numeric($_POST['id']) && ($menu_item = menuItem::getByID($_POST['id'])) && 
			$menu_item->menu->idMenu==$restaurantAdmin->restaurant->menus[0]->idMenu) {
			
			// The current restaurant admin is allowed to update the current menu item
			// Let's first remove the older image, in case there's a new one submitted
			if ($photo && !empty($menu_item->photo)) {
				@unlink(REL . '/i/restaurants/' . $menu_item->photo);
			}
			
			$menu_item->update($menu_category, $menu_item_name, $photo, $description, $spicy, $veggie, $glut_free, $lact_free, $recommended, $price, $discount, $enabled, $menu_option_groups);
			
		} else {
			// Insert
			$menu_item = menuItem::insert($restaurantAdmin->restaurant->menus[0], $menu_category, $menu_item_name, $photo, $description, $spicy, $veggie, $glut_free, $lact_free, $recommended, $price, $discount, $enabled, $menu_option_groups);
		}
		
		
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/menu-items.html' . (!empty($menu_item) ? '#menu_item_' . $menu_item->idMenuItem : ''));
		die();
	}
	
	
	
	$menu_categories = menuCategory::list_($restaurantAdmin->restaurant);
	$menu_options_groups = menuItemCategory::list_new($restaurantAdmin->restaurant);
	/*
	$allowRecommended = true;
	$recommendedMenuItems = 0;
	foreach($menuItems4Edit as $menuItem){
		if($menuItem->recommended=='Y'){
			$recommendedMenuItems += 1;
		}
		
		if($recommendedMenuItems>=5){
			$allowRecommended = false;
			break;
		}
	}
	*/
	$menu_items = menuItem::list_($restaurantAdmin->restaurant, false, true, true);
