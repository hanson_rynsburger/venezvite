<?php
	if (@$is_included) {
		
		$query = 'SELECT `idRestaurant`, `restaurantName`, `latitude`, `longitude` 
			FROM `restaurants`;'; /* 
			WHERE `customDelivery` = \'Y\'*/
		$restaurant_results = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$updated_delivery_zones = 0;
		while ($row = mysql_fetch_assoc($restaurant_results)) {
			$restaurant = new restaurant;
			$restaurant->idRestaurant = $row['idRestaurant'];
			
			$delivery_zones = deliveryZone::list_($restaurant);
			foreach ($delivery_zones as $delivery_zone) {
				if (empty($delivery_zone->coordinates)) {
					$path = deliveryZone::generatePath($row['latitude'], $row['longitude'], $delivery_zone->range);
					
					$delivery_zone->update(array(
							'coordinates' => json_encode($path)
						));
					$updated_delivery_zones++;
				}
			}
		}
	}
	
	echo $updated_delivery_zones, ' updated delivery zones';
	die();
