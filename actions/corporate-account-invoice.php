<?php
	if(@$is_included && strlen(@$_GET['check'])>7) {
		$ids = explode('.', $_GET['check']);
		
		if (count($ids)==2 && is_numeric($ids[0])) {
			$idCorporateAccount = $ids[0];
			$idOrder = substr($ids[1], 5);
			
			if(is_numeric($idOrder)){
				$order = order::getByID($idOrder);
				
				if($order && $order->corporateAccount && $order->corporateAccount->idCorporateAccount==$idCorporateAccount){
					// All seems to be OK, now let's build the invoice
					require_once REL . '/classes/fpdf17/fpdf.php';
					
					$exchangeRate = exchangeRate::getLatest();
					
					class PDF extends FPDF {
						function Footer(){
							$this->SetFontSize(11.14);
							$this->SetY(270);
							$this->MultiCell(0, 6, 'Swift Code: MRMDUS33' . "\n" . 
								'Beneficiary: Venezvite, LLC  Account #: 619812028  Aba Routing #: 021001088' . "\n" . 
								'Bank Name: HSBC  Bank Address: 120 Broadway New York, NY 10271', 0, 'C');
						}
					}
					
					$pdf = new PDF();
					$pdf->SetMargins(15.2, 15.5);
					$pdf->SetDisplayMode('fullpage', 'single');
					$pdf->SetTitle(utf8_decode(CAI_TITLE));
					$pdf->SetAuthor('Venezvite, LLC.');
					$pdf->SetCreator(ROOT);
					
					$pdf->SetFillColor(250, 249, 247);
					$pdf->SetLineWidth(0.4);
					$pdf->SetDrawColor(200, 199, 199);
					
					$pdf->AddPage();
					$pdf->SetFont('Arial', '', 10.28);
					
					$pdf->Rect(0, 0, 210, 297, 'F');
					
					$pdf->Image((!strstr('http:', IMG) ? 'http:' : '') . IMG . 'invoice/logo.gif', 14.8, 14.5, 35.7, 12.1, 'GIF', ROOT);
					$pdf->Image((!strstr('http:', IMG) ? 'http:' : '') . IMG . 'invoice/thank-you-text-' . $_SESSION['s_venezvite_language']->languageAcronym . '.gif', 15.5, 31.0, 129.6, 12.6, 'GIF');
					
					$pdf->Image((!strstr('http:', IMG) ? 'http:' : '') . IMG . 'invoice/invoice-' . $_SESSION['s_venezvite_language']->languageAcronym . '.gif', 171.1, 17.3, 24.8, 5.6, 'GIF');
					
					$pdf->SetY(28.2);
					$pdf->SetFont('Arial', 'B');
					$pdf->MultiCell(0, 5, 'Venezvite, LLC', 0, 'R'); 
					$pdf->SetFont('');
					$pdf->MultiCell(0, 5, '110 Green Street A601' . "\n" . 
						'Brooklyn, NY 11222 USA' . "\n" . 
						'hello@venezvite.com', 0, 'R');
					
					$pdf->SetY(52.2);
					$pdf->SetFont('Arial', 'B');
					$pdf->MultiCell(0, 5, utf8_decode(CAI_BILL_TO) . ':' . "\n\n" . 
						utf8_decode($order->corporateAccount->companyName)); 
					$pdf->SetFont('');
					$pdf->MultiCell(0, 5, utf8_decode($order->corporateAccount->address) . "\n" . 
						$order->corporateAccount->zipCode . ', ' . utf8_decode($order->corporateAccount->city)); 
					
					$pdf->SetXY(73, 62.2);
					$pdf->MultiCell(0, 5, utf8_decode($order->corporateAccount->contactFirstName . ' ' . $order->corporateAccount->contactLastName) . "\n" . 
						$order->corporateAccount->phone . "\n" . 
						$order->corporateAccount->emails[0]); 
					
					setlocale(LC_TIME, $_SESSION['s_venezvite_language']->localeCode);
					
					$pdf->SetY(57.2);
					$pdf->MultiCell(0, 5, utf8_decode(CAI_INVOICE) . ' #: ' . (1321 + $order->idOrder) . "\n" . 
						utf8_decode(CAI_DATE_PROCESSED) . ': ' . strftime('%d %B %Y', strtotime($order->datePreApproved)) . "\n" . 
						utf8_decode(CAI_DESIRED_DELIVERY) . ': ' . strftime('%d %B %Y', strtotime($order->dateDesired)) . "\n" . 
						utf8_decode(CAI_DESIRED_DELIVERY_TIME) . ': ' . date('H:i', strtotime($order->dateDesired)), 0, 'R'); 
					
					$pdf->Image((!strstr('http:', IMG) ? 'http:' : '') . IMG . 'invoice/bg-items-top.gif', 12.7, 81.8, 186.3, 3.9, 'GIF');
					$pdf->Image((!strstr('http:', IMG) ? 'http:' : '') . IMG . 'invoice/bg-items-middle.gif', 12.7, 85.7, 186.3, 145, 'GIF');
					$pdf->Image((!strstr('http:', IMG) ? 'http:' : '') . IMG . 'invoice/order-details-' . $_SESSION['s_venezvite_language']->languageAcronym . '.gif', 39.1, 93, 131.3, 5.9, 'GIF');
					
					$pdf->Line(15.6, 107.2, 196, 107.2);
					
					$pdf->SetY(110.4);
					$pdf->SetFont('Arial', 'B');
					$pdf->MultiCell(0, 5, utf8_decode($order->restaurant->restaurantName), 0, 'C'); 
					$pdf->SetFont('');
					$pdf->MultiCell(0, 5, utf8_decode($order->restaurant->address) . "\n" . 
						$order->restaurant->zipCode . ', ' . utf8_decode($order->restaurant->city) . "\n" . 
						$order->restaurant->phone, 0, 'C');
					
					$pdf->Line(15.6, 133, 196, 133);
					
					$pdf->SetXY(22, 139);
					// Start drawing the actual invoice items
					// First, the header
					$pdf->SetFont('Arial', 'B');
					$pdf->Cell(27, 5, utf8_decode(CAI_QUANTITY));
					$pdf->Cell(90.5, 5, utf8_decode(CAI_ITEM_DESCRIPTION));
					$pdf->Cell(30, 5, utf8_decode(CAI_UNIT_PRICE));
					$pdf->Cell(21, 5, utf8_decode(CAI_TOTAL));
					$pdf->Ln();
					
					$pdf->SetY(147);
					// And now, the items themselves
					$pdf->SetFont('');
					$total = 0;
					foreach($order->items as $menuItem){
						$options = array();
						$optionsValue = 0;
						foreach($menuItem->options as $orderItemOption){
							$options[] = $orderItemOption->menuItemOption->menuItemOption;
							$optionsValue += $orderItemOption->price;
						}
						
						$pdf->SetX(22);
						$pdf->Cell(27, 5, $menuItem->quantity);
						$pdf->Cell(90.5, 5, $menuItem->menuItemName . (!empty($options) ? ' (+ ' . implode(', ', $options) . ')' : ''));
						$pdf->Cell(30, 5, $menuItem->pricePerItem . ' CHF');
						$pdf->Cell(21, 5, number_format(($menuItem->pricePerItem + $optionsValue) * $menuItem->quantity, 2) . ' CHF');
						$pdf->Ln();
						
						$total += ($menuItem->pricePerItem + $optionsValue) * $menuItem->quantity;
					}
					
					$pdf->SetFont('Arial', 'B');
					$pdf->SetXY(139, 210);
					$pdf->Cell(0, 5, utf8_decode(CAI_SUBTOTAL) . ': ' . $order->value . ' CHF');
					$pdf->Ln();
					$pdf->SetFont('');
					$pdf->SetX(139);
					$pdf->Cell(0, 5, utf8_decode(CAI_DELIVERY_COST) . ': ' . $order->deliveryCost . ' CHF');
					$pdf->Ln();
					$pdf->SetFont('Arial', 'B', 15.42);
					$pdf->SetX(139);
					$pdf->Cell(0, 10, utf8_decode(CAI_TOTAL) . ': ' . number_format(($order->value + $order->deliveryCost), 2) . ' CHF');
					
					$pdf->Ln();
					$pdf->SetFont('Arial', '', 10.28);
					$pdf->SetXY(22, 220);
					$pdf->Cell(0, 10, 'For your convenience, the currency conversion in USD is $' . number_format(($order->value + $order->deliveryCost) * $exchangeRate->usd, 2));
					
					$pdf->Image((!strstr('http:', IMG) ? 'http:' : '') . IMG . 'invoice/bg-items-bottom-gradient.gif', 12.7, 230.7, 186.3, 21.2, 'GIF');
					$pdf->Image((!strstr('http:', IMG) ? 'http:' : '') . IMG . 'invoice/thank-you-face-' . $_SESSION['s_venezvite_language']->languageAcronym . '.png', 86.7, 238.5, 47.0, 25.7, 'PNG');
					
					$pdf->Output((isset($_GET['save']) ? REL . '/tmp_/' : '') . (!empty($_GET['name']) ? $_GET['name'] : 'venezvite-invoice-' . date('YmdHi') . '.pdf'), (isset($_GET['save']) ? 'F' : 'D'));
				}
			}
		}
	}
	
	die();
