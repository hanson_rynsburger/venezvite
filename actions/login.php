<?php
	if (@$is_included && !empty($_POST['email']) && !empty($_POST['password'])) {
		$user = user::getFromSession();
		if ($user) {
			// This shouldn't even happen
			die();
		}
		
		$corporateAccount = corporateAccount::getFromSession();
		if ($corporateAccount) {
			// This shouldn't even happen
			die();
		}
		
		$user = user::authenticate(checkQuotes($_POST['email']), checkQuotes($_POST['password']), (@$_POST['remember']=='Y' ? 'Y' : null));
		if (!$user) {
			$corporateAccount = corporateAccount::authenticate(checkQuotes($_POST['email']), checkQuotes($_POST['password']), (@$_POST['remember']=='Y' ? 'Y' : null));
			if (!$corporateAccount) {
				echo json_encode(array(
						'error' => INVALID_USER_PASS
					));
				die();
			}
		}
		
		echo json_encode(array(
				'success' => true
			));
		die();
	}
	
	echo json_encode(array());
	die();
