<?php
	if (@$is_included && !empty($_COOKIE['c_venezvite']) && ($restaurant = restaurant::getByURL(@json_decode($_COOKIE['c_venezvite'])->restaurant)) && !empty($_SESSION['s_venezvite']['carts'][$restaurant->customURL])) {
		if (@$_SERVER['HTTPS']!='on') {
			// Redirect to https
			header('Location: https://' . str_replace(array('http:', '//'), '', ROOT) . $_SESSION['s_venezvite']['language']->languageAcronym . '/checkout.html');
			die();
		}
		
		
		check4Search();
		
		if (@$_SESSION['s_venezvite']['search']['coords'] && ($delivery_zone = $restaurant->checkDeliveryLocation($_SESSION['s_venezvite']['search']['coords']))) { // @$_SESSION['s_venezvite']['search']['type']=='D'
			// Get the restaurant's current delivery zone
			$restaurant->current_delivery_zone = array(
				'id'				=> @$delivery_zone->idDeliveryZone, 
				'delivery_fee'		=> @$delivery_zone->deliveryFee, 
				'minimum_delivery'	=> @$delivery_zone->minimumDelivery, 
				'estimated_time'	=> @$delivery_zone->estimatedTime
			);
		}
		
		
		if ($restaurant->customDelivery!='Y' && $restaurant->cashPayment=='Y') {
			if (!empty($entity)) {
				$cash_orders_status = $entity->canPlaceCashOrders();
				
				if ($cash_orders_status['paid'] && !$cash_orders_status['pending']) {
					$can_place_cash_orders = true;
					$cash_payment_desc = CASH_PAYMENT_DESC;
				} else {
					$can_place_cash_orders = false;
					
					if (!$cash_orders_status['paid']) {
						$cash_payment_desc = UNPAID_CASH;
					} elseif ($cash_orders_status['pending']) {
						$cash_payment_desc = UNAPPROVED_CASH;
					}
				}
			} else {
				$can_place_cash_orders = true;
				$cash_payment_desc = CASH_PAYMENT_DESC;
			}
			
		} else {
			$can_place_cash_orders = false;
			$cash_payment_desc = NO_CASH;
		}
		
		
		$total_amount = getCartTotal($restaurant);
		$exchange_rate = exchangeRate::getLatest();
		
		
		if (!empty($_POST)) {
			//echo '<pre>';
			//var_dump($_POST);
			//die();
			$error = false;
			if ((!empty($entity) || 
				(!empty($_POST['last_name']) && !empty($_POST['first_name']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['phone']))) && 
				
				in_array(@$_POST['type'], array('D', 'T')) && 
				($_POST['type']=='D' ? 
					(is_numeric(@$_POST['delivery_address']) && ($delivery_address = userAddress::getByID($_POST['delivery_address']))) || 
					(!empty($_POST['delivery_street']) && /*!empty($_POST['delivery_building_no']) && */!empty($_POST['delivery_city']) && !empty($_POST['delivery_zip_code']))
					: 
					true) && 
				
				in_array(@$_POST['payment'], array('CC', 'C', 'LC', 'I')) && 
				($_POST['payment']=='CC' ? 
					in_array(@$_POST['currency'], array('CHF', 'EUR', 'USD')) && 
					(
						(!empty($_POST['cc_name']) && !empty($_POST['cc_number']) && !empty($_POST['cc_month']) && !empty($_POST['cc_year'])/* && !empty($_POST['cvc'])*/) || 
						(!empty($entity) && is_numeric(@$_POST['payment_token']) && ($payment_token = userPaymentToken::getByID($_POST['payment_token'])) && 
							$payment_token->user->idUser==$entity->idUser)
					)
					: 
					($_POST['payment']=='C' ? 
						$can_place_cash_orders : 
						($_POST['payment']=='LC' ? 
							!empty($_POST['lc_number']) && !empty($_POST['cvc']) : 
							!empty($corporateAccount)))) && 
				
				((!empty($entity) && count(order::list_($entity, null, true, true))) || 
					@$_POST['terms']=='Y')) {
				
				function updateSearchedAddress() {
					// Updating the searched address' values
					global $delivery_address;
					global $address_coordinates;
					
					$_SESSION['s_venezvite']['search']['id'] = (empty($delivery_address) ? null : $delivery_address->idAddress);
					$_SESSION['s_venezvite']['search']['location'] = (empty($delivery_address) ? 
						/*checkQuotes($_POST['delivery_building_no']) . ' ' . */checkQuotes($_POST['delivery_street']) . ', ' . checkQuotes($_POST['delivery_city']) : 
						/*$delivery_address->buildingNo . ' ' . */$delivery_address->address . ', ' . $delivery_address->city);
					$_SESSION['s_venezvite']['search']['building_no'] = ''; //(empty($delivery_address) ? checkQuotes($_POST['delivery_building_no']) : $delivery_address->buildingNo);
					$_SESSION['s_venezvite']['search']['address'] = (empty($delivery_address) ? checkQuotes($_POST['delivery_street']) : $delivery_address->address);
					$_SESSION['s_venezvite']['search']['city'] = (empty($delivery_address) ? checkQuotes($_POST['delivery_city']) : $delivery_address->city);
					$_SESSION['s_venezvite']['search']['zip_code'] = (empty($delivery_address) ? checkQuotes($_POST['delivery_zip_code']) : $delivery_address->zipCode);
					$_SESSION['s_venezvite']['search']['coords'] = array(
						'lat'	=> $address_coordinates['lat'], 
						'lng'	=> $address_coordinates['lng']
					);
				}
				
				
				if (empty($user) && empty($corporateAccount)) {
					// This should be a new user registration
					// First let's check for the submitted email address' unicity
					$user = user::checkDuplicateEmail($_POST['email']);
					$corporate_account = corporateAccount::checkDuplicateUsername($_POST['email']);
					
					if ($user || $corporate_account) {
						// The submitted email address is used by one of the registered users or corporate accounts
						if (!empty($_POST['ajax'])) {
							echo json_encode(array(
									'error' => USED_EMAIL
								));
							die();
							
						} else {
							$_SESSION['s_venezvite']['checkout_error'] = USED_EMAIL;
							$error = true;
						}
					}
					
					if (!$error && empty($_POST['ajax'])) {
						// It's all good, let's register it
						// We only manage the submitted users on postback - NOT on AJAX validation
						
						$user = user::insert(
							null, 
							null, 
							checkQuotes($_POST['first_name']), 
							checkQuotes($_POST['last_name']), 
							$_POST['email'], 
							checkQuotes($_POST['password']), 
							null, 
							checkQuotes($_POST['phone']), 
							(@$_POST['newsletter']=='Y' ? 'Y' : 'N'), 
							null, 
							$_SESSION['s_venezvite']['language']->idLanguage, 
							$_SERVER['REMOTE_ADDR']
						);
						/*
						if ($user) {*/
							$user->login();
							/*
						} else {
							echo json_encode(array(
									'error' => 'Unable to register user!'
								));
							die();
						}*/
						
						$entity = $user;
					}
					
				} elseif (empty($_POST['ajax']) && 
					!empty($_POST['last_name']) && !empty($_POST['first_name']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['phone'])) {
					// Must be a user edit
					// We only manage the submitted users on postback - NOT on AJAX validation
					
					if (get_class($entity)=='user') {
						$user->update(
							checkQuotes($_POST['first_name']), 
							checkQuotes($_POST['last_name']), 
							$_POST['email'], 
							checkQuotes($_POST['password']), 
							checkQuotes($_POST['phone'])
						);
						
					//} else {
					//	// WE DON'T HAVE A CORPORATE ACCOUNT UPDATE METHOD YET!...
					//	$corporateAccount->update(
					//	
					//	);
					}
					
				} elseif (trim($entity->phone)=='' && empty($_POST['phone'])) {
					if (!empty($_POST['ajax'])) {
						echo json_encode(array(
								'error' => PHONE_MANDATORY
							));
						die();
						
					} else {
						$_SESSION['s_venezvite']['checkout_error'] = PHONE_MANDATORY;
						$error = true;
					}
				}
				
				if (!$error && $_POST['type']=='D') {
					/*
					if (strpos($_POST['delivery_city'], ', Suisse')==(strlen($_POST['delivery_city']) - 8)) {
						$_POST['delivery_city'] = substr($_POST['delivery_city'], 0, -8);
						
					} else if (strpos($_POST['delivery_city'], ', Switzerland')==(strlen($_POST['delivery_city']) - 13)) {
						$_POST['delivery_city'] = substr($_POST['delivery_city'], 0, -13);
					}
					*/
					if (!empty($_POST['delivery_street']) && /*!empty($_POST['delivery_building_no']) && */!empty($_POST['delivery_city']) && !empty($_POST['delivery_zip_code'])) {
						// Let's determine this address' geo location first.
						$address = checkQuotes($_POST['delivery_street']) . /*' ' . checkQuotes($_POST['delivery_building_no']) . */', ' . checkQuotes($_POST['delivery_city']) . ', ' . checkQuotes($_POST['delivery_zip_code']);
						$geocoding_url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&key=' . GOOGLE_KEY;
						
						$geocoding_response = @file_get_contents($geocoding_url);
						if ($geocoding_response && ($json = json_decode($geocoding_response))) {
							if ($json->status=='OK' && count(@$json->results)>0) {
								$match = $json->results[0];
								
								$building_no_found = false;
								foreach ($match->address_components as $address_component) {
									if (in_array('street_number', $address_component->types)) {
										$building_no_found = true;
									}
								}
								
								if ($building_no_found) {
									$address_coordinates = array(
										'lat'	=> $match->geometry->location->lat, 
										'lng'	=> $match->geometry->location->lng
									);
									
								} else {
									if (!empty($_POST['ajax'])) {
										echo json_encode(array(
												'error' => NO_BUILDING_NO
											));
										die();
										
									} else {
										$_SESSION['s_venezvite']['checkout_error'] = NO_BUILDING_NO;
										$error = true;
									}
								}
							}
						}
						
					} else {
						$address_coordinates = array(
							'lat'	=> $delivery_address->latitude, 
							'lng'	=> $delivery_address->longitude
						);
					}
					
					if (!$error && empty($address_coordinates)) {
						// We couldn't determine the location of the submitted address.
						if (!empty($_POST['ajax'])) {
							echo json_encode(array(
									'error' => CANT_FIND_ADDRESS
								));
							die();
							
						} else {
							$_SESSION['s_venezvite']['checkout_error'] = CANT_FIND_ADDRESS;
							$error = true;
						}
					}
					
					if (!$error && empty($_POST['ajax'])) {
						// We only manage the submitted delivery address on postback - NOT on AJAX validation
						
						if (empty($delivery_address)) {
							// This must be a new address. Let's try to insert it.
							$delivery_address = userAddress::insert(
								!empty($user) ? $user : $corporateAccount, 
								checkQuotes($_POST['delivery_street']), 
								'', //checkQuotes($_POST['delivery_building_no']), 
								checkQuotes(@$_POST['delivery_floor_suite']), 
								checkQuotes(@$_POST['delivery_access_code']), 
								checkQuotes($_POST['delivery_city']), 
								checkQuotes($_POST['delivery_zip_code']), 
								$address_coordinates['lat'], 
								$address_coordinates['lng'], 
								'', 
								'Y', 
								checkQuotes(@$_POST['delivery_instructions']), 
								checkQuotes(@$_POST['addr_type'])
							);
							
						} elseif (!empty($_POST['delivery_street']) && /*!empty($_POST['delivery_building_no']) && */!empty($_POST['delivery_city']) && !empty($_POST['delivery_zip_code'])) {
							$delivery_address->update(
								checkQuotes($_POST['delivery_street']), 
								'', //checkQuotes($_POST['delivery_building_no']), 
								checkQuotes(@$_POST['delivery_floor_suite']), 
								checkQuotes(@$_POST['delivery_access_code']), 
								checkQuotes($_POST['delivery_city']), 
								checkQuotes($_POST['delivery_zip_code']), 
								$address_coordinates['lat'], 
								$address_coordinates['lng'], 
								'', 
								'Y', 
								checkQuotes(@$_POST['delivery_instructions']), 
								checkQuotes(@$_POST['addr_type'])
							);
						}
					}
					
					// Let's check if the currently submitted delivery address is within
					// the current restaurant's delivery zones.
					if (!$error && !($delivery_zone = $restaurant->checkDeliveryLocation(array(
							'lat'	=> $address_coordinates['lat'], 
							'lng'	=> $address_coordinates['lng']
						)))) {
						
						if (!empty($_POST['ajax'])) {
							echo json_encode(array(
									'error' => OUTSIDE_ADDRESS
								));
							die();
							
						} else {
							$_SESSION['s_venezvite']['checkout_error'] = OUTSIDE_ADDRESS;
							$error = true;
						}
					}
					
					if (!$error && $restaurant->current_delivery_zone['id']!=$delivery_zone->idDeliveryZone) {
						// Let's check if the current delivery address' delivery zone is outside 
						// the searched address' delivery fee or required minimum.
						if ($delivery_zone->deliveryFee >= $restaurant->current_delivery_zone['delivery_fee'] && 
							$delivery_zone->minimumDelivery >= $total_amount) {
							
							if (!empty($_POST['ajax'])) {
								echo json_encode(array(
										'error'			=> DIFFERENT_CHARGES_MINIMUM, 
										'delivery_fee'	=> $delivery_zone->deliveryFee, 
										'min_delivery'	=> $delivery_zone->minimumDelivery
									));
								die();
								
							} else {
								$_SESSION['s_venezvite']['checkout_error'] = DIFFERENT_CHARGES_MINIMUM;
								$error = true;
							}
							
						} else if ($delivery_zone->minimumDelivery >= $total_amount) {
							if (!empty($_POST['ajax'])) {
								echo json_encode(array(
										'error'			=> DIFFERENT_MINIMUM, 
										'min_delivery'	=> $delivery_zone->minimumDelivery
									));
								die();
								
							} else {
								$_SESSION['s_venezvite']['checkout_error'] = DIFFERENT_MINIMUM;
								$error = true;
							}
							
						} else if ($delivery_zone->deliveryFee >= $restaurant->current_delivery_zone['delivery_fee']) {
							updateSearchedAddress();
							
							if (!empty($_POST['ajax'])) {
								echo json_encode(array(
										'error'			=> DIFFERENT_CHARGES, 
										'delivery_fee'	=> $delivery_zone->deliveryFee
									));
								die();
								
							} else {
								$_SESSION['s_venezvite']['checkout_error'] = DIFFERENT_CHARGES;
								$error = true;
							}
						}
					}
					
					updateSearchedAddress();
				}
				
				
				if (!$error && $_POST['payment']=='CC' && empty($payment_token)) {
					$_POST['cc_number'] = str_replace(array(' ', '-', '.', '/', '\\'), '', $_POST['cc_number']); // Clean it up a bit
					$card_type = getCardType($_POST['cc_number']);
					if (!$card_type/* || !in_array($card_type, array('American Express', 'Visa', 'Mastercard', 'Discover'))*/) {
						if (!empty($_POST['ajax'])) {
							echo json_encode(array(
									'error' => WRONG_CC_TYPE
								));
							die();
							
						} else {
							$_SESSION['s_venezvite']['checkout_error'] = WRONG_CC_TYPE;
							$error = true;
						}
					}
				}
				
				if (!$error) {
					if (!empty($_POST['ajax'])) {
						echo json_encode(array(
								'success' => true
							));
						die();
						
					} else {
						$payment_total = $total_amount + ($_POST['type']=='D' ? $delivery_zone->deliveryFee : 0);
						
						if ($payment_total>0) {
							if ($_POST['payment']=='CC') {
								// The actual payment functionality
								$currency_credentials = json_decode(PAYEEZY_CURRENCIES, true);
								define('PAYEEZY_HMAC_KEY', $currency_credentials[$_POST['currency']]['HMAC_KEY']);
								define('PAYEEZY_KEY_ID', $currency_credentials[$_POST['currency']]['KEY_ID']);
								define('PAYEEZY_EXACT_ID', $currency_credentials[$_POST['currency']]['EXACT_ID']);
								define('PAYEEZY_PASSWORD', $currency_credentials[$_POST['currency']]['PASSWORD']);
								
								
								// Let's convert the total amount in the user's submitted currency
								// The exchange rate uses CHF as default currency. We now need to determine the actual exchange rate
								// based on the current restaurant's currency
								$exchange_rate_multiplier = $exchange_rate->get_from_matrix(strtolower($restaurant->currency), strtolower($_POST['currency']));
								$payment_total = round($payment_total * $exchange_rate_multiplier, 2);
								
								$payment_data = array(
									'ExactID'			=> PAYEEZY_EXACT_ID, 
									'Password'			=> PAYEEZY_PASSWORD, 
									'Transaction_Type'	=> '00', // Transaction Code I.E. Purchase='00' Pre-Authorization='01' etc.
									'Customer_Ref'		=> (!empty($user) ? $user->idUser : $corporateAccount->idCorporateAccount), // User's ID
									//'Client_IP'			=> $_SERVER['REMOTE_ADDR'], // This value is only used for fraud investigation.
									//'Client_Email'		=> (!empty($user) ? $user->email : $corporateAccount->emails[0]), // This value is only used for fraud investigation.
									'Language'			=> strtoupper($_SESSION['s_venezvite']['language']->languageAcronym), // EN/FR/ES
									'DollarAmount'		=> $payment_total, 
									'Currency'			=> $_POST['currency']
								);
								
								if (empty($payment_token)) {
									// Regular CC payment
									$payment_data['Card_Number'] = $_POST['cc_number']; // For Testing, Use Test#s VISA='4111111111111111' MasterCard='5500000000000004' etc.
									$payment_data['Expiry_Date'] = $_POST['cc_month'] . $_POST['cc_year']; // This value should be in the format MM/YY
									$payment_data['CardHoldersName'] = checkQuotes($_POST['cc_name']);
									//$payment_data['CVDCode'] = $_POST['cvc']; // Credit card verification code
									//$payment_data['CVD_Presence_Ind'] = '1'; // Meaning that the user would provide a CCV code
								} else {
									// Stored CC / token payment
									$payment_data['TransarmorToken'] = $payment_token->token;
									$payment_data['Expiry_Date'] = date('my', strtotime($payment_token->expiration_date));
									$payment_data['CardHoldersName'] = $payment_token->cardholder_name;
									$payment_data['CardType'] = $payment_token->card_type;
								}
								
								
								$client = new SoapClientHMAC(PAYEEZY_URL); 
								$payment_result = @$client->SendAndCommit($payment_data);
								
								if (@$payment_result) {
									if (@$payment_result->Transaction_Error || !@$payment_result->Transaction_Approved) {
										if (@$payment_result->Transaction_Error) {
											$_SESSION['s_venezvite']['checkout_error'] = $payment_result->EXact_Message;
										} elseif (!@$payment_result->Transaction_Approved) {
											$_SESSION['s_venezvite']['checkout_error'] = str_replace('Credit Floor', 'Insufficient funds', @$payment_result->Bank_Message);
										} else {
											error_log(print_r($payment_data, 1));
											$_SESSION['s_venezvite']['checkout_error'] = 'Unknown payment error. Please contact us to report this issue!';
										}
										
										$error = true;
										
									} elseif (@$_POST['save_payment']=='Y') {
										// Store the submitted card
										userPaymentToken::insert($entity, array(
												'user_id'			=> $entity->idUser, // In the future, this can also be a corporate account
												'cardholder_name'	=> checkQuotes($_POST['cc_name']), 
												'token'				=> $payment_result->TransarmorToken, 
												'card_type'			=> $card_type, 
												'expiration_date'	=> ('20' . $_POST['cc_year'] . '-' . $_POST['cc_month'] . '-01'), 
												'default'			=> 'Y'
											));
									}
									
								} else {
									error_log(print_r($payment_data, 1));
									$_SESSION['s_venezvite']['checkout_error'] = 'Unknown payment error. Please contact us to report this issue!';
									
									$error = true;
								}
								
							} elseif ($_POST['payment']=='LC') {
								// Lunch-check payment functionality
								$soapClient = new SoapClient(LUNCHCARD_API_URL, array(
										'trace'			=> 1, 
										'exceptions'	=> 0
									));
								
								$soapHeader = new SoapHeader(LUNCHCARD_BASE_URL, 'MyHeader', array(
										'UserId'	=> LUNCHCARD_USERNAME, 
										'Password'	=> LUNCHCARD_PASSWORD
									));
							    $soapClient->__setSoapHeaders($soapHeader);
							    
								$soapParams = array(
									'trxReferenceNr'	=> (!empty($user) ? $user->idUser : $corporateAccount->idCorporateAccount), // User's ID
									'dateYYYYMMDD'		=> date('Ymd'), 
									'timeHHMMSS'		=> date('His'), 
									'merchantName'		=> 'Venezvite, LLC.', 
									'merchantId'		=> LUNCHCARD_MERCHANT_ID, 
									'terminalId'		=> LUNCHCARD_TERMINAL_ID, 
									'cardNr'			=> $_POST['lc_number'], 
									'cvc'				=> $_POST['cvc'], 
									'amount'			=> $payment_total, 
									'currency'			=> 'CHF'
								);
								$soapResult = $soapClient->__soapCall('Payment', array('Payment' => $soapParams));
								
								if (@$soapResult->PaymentResult->Status != 'OK') {
									// Unexpected result
									if (@$soapResult->PaymentResult->Status) {
										$_SESSION['s_venezvite']['checkout_error'] = $soapResult->PaymentResult->Status . ': ' . $soapResult->PaymentResult->ExtendedError;
									} elseif (@$soapResult->faultstring) {
										$_SESSION['s_venezvite']['checkout_error'] = $soapResult->faultstring;
									} else {
										$_SESSION['s_venezvite']['checkout_error'] = 'Unknown payment error. Please contact us to report this issue!';
									}
									
									$error = true;
								}
							}
						}
						
						if (!$error) {
							// Proceed to registering the order
							switch($_POST['payment']) {
								case 'C':
									$payment_type = 'cash';
									break;
								case 'CC':
									$payment_type = 'cc';
									break;
								case 'LC':
									$payment_type = 'lunch-check';
									break;
								case 'I':
									$payment_type = 'invoice';
									break;
								//case 'PP':
									//$payment_type = 'paypal';
									//break;
							}
							$date_desired = $_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time'];
							
							$order = order::new_insert(
								$entity, // User or Corporate Account
								$_SESSION['s_venezvite']['carts'][$restaurant->customURL], // Shopping cart's content
								($_POST['type']=='D' ? @$delivery_address : null), 
								$restaurant, 
								$_POST['type'], // Delivery or Take-away
								$payment_type, // Cash, CC, Lunch-Check, Invoice or... others
								(!empty($payment_result) ? $payment_result->Transaction_Tag : null), 
								$total_amount, 
								($_POST['type']=='D' ? @$delivery_zone : null), 
								$exchange_rate, 
								$date_desired
							);
							
							$tarCurrency = (!empty($_POST['currency']) ? $_POST['currency'] : 'CHF');
							
							$_SESSION['s_venezvite']['order'] = array(
								'id'			=> $order->idOrder . (!empty($payment_result) ? '-' . $payment_result->Transaction_Tag : ''), 
								'value'			=> $payment_total . ' ' . (!empty($_POST['currency']) ? $_POST['currency'] : 'CHF'), 
								'restaurant'	=> $restaurant->restaurantName . ' (' . $restaurant->address . ')', 
								'type'			=> $_POST['type'], // Delivery or Take-away
								'date'			=> $date_desired, 
								'contact_phone'	=> $restaurant->phone, 
								'cart_content'	=> $_SESSION['s_venezvite']['carts'][$restaurant->customURL], 
								'address'		=> ($_POST['type']=='D' ? @$delivery_address : null), 
								'delivery_fee'	=> ($_POST['type']=='D' ? @$delivery_zone->deliveryFee : null), 
								'currency'		=> $tarCurrency, 
								'multiplier' 	=> $exchange_rate->get_from_matrix(strtolower($restaurant->currency), strtolower($tarCurrency))
							);
							
							unset($_SESSION['s_venezvite']['carts'][$restaurant->customURL]);
							unset($_SESSION['s_venezvite']['search']);
							
							// It's all good, let's redirect to the confirmation page
							header('Location: http://' . str_replace(array('https:', '//'), '', ROOT) . $_SESSION['s_venezvite']['language']->languageAcronym . '/confirmation.html');
							
						} else {
							// Reload the page so that, in case there's a newly registered user, it'll already be logged in
							header('Location: https://' . str_replace(array('http:', '//'), '', ROOT) . $_SESSION['s_venezvite']['language']->languageAcronym . '/checkout.html');
						}
						die();
					}
				}
				
			} else {
				if (!empty($_POST['ajax'])) {
					echo json_encode(array(
							'error' => FILL_FIELDS
						));
					die();
					
				} else {
					$_SESSION['s_venezvite']['checkout_error'] = FILL_FIELDS;
					$error = true;
				}
			}
		}
		
		
		if (!empty($entity)) {
			$user_payment_tokens = userPaymentToken::list_($entity);
		}
		
	} else {
		header('Location: http://' . str_replace(array('https:', '//'), '', ROOT) . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	}
