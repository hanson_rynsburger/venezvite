<?php
	if (@$is_included && !empty($_POST['email'])) {
		if (@$entity && get_class($entity)=='user' && $entity->email==$_POST['email']) {
			$user = false;
		} else {
			$user = user::checkDuplicateEmail($_POST['email']);
		}
		if (@$entity && get_class($entity)=='corporateAccount' && in_array($_POST['email'], $entity->emails)) {
			$corporate_account = false;
		} else {
			$corporate_account = corporateAccount::checkDuplicateUsername($_POST['email']);
  		}
		
		if (!$user && !$corporate_account) {
			// The submitted email address is not used by any of the registered users or corporate accounts
			echo 'true';
			die();
		}
	}
	
	echo 'false';
	die();
