<?php
	if (@$is_included && (!empty($restaurantAdmin) || !empty($_SESSION['s_venezvite_backend'])) && 
		!empty($_GET['id']) && is_numeric($_GET['id']) && ($order = order::getByID($_GET['id']))) {
		
		if (!empty($restaurantAdmin) ? 
			$order->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant : 
			!empty($_SESSION['s_venezvite_backend'])
		) {
			// The order exists, and it belongs to the current restaurant
			$order->decline();
		}
	}
	
	header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/restaurant-orders.html');
	die();
