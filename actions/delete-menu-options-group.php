<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		@is_numeric($_POST['options_group']) && ($options_group = menuItemCategory::get_by_id($_POST['options_group'])) && 
		$options_group->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
		
		// The current restaurant admin is allowed to remove the submitted menu options group
		$menu_items = menuItem::list_($restaurantAdmin->restaurant, false, true, true);
		$used_group = false;
		foreach ($menu_items as $menu_item) {
			if (!empty($menu_item->menuItemGroups) && in_array($options_group, $menu_item->menuItemGroups)) {
				$used_group = true;
				break;
			}
		}
		
		if (!$used_group) {
			$options_group->delete();
			
			echo json_encode(array(
					'success' => true
				));
			die();
		}		
	}
	
	echo json_encode(array());
	die();
