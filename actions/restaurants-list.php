<?php
	if (@$is_included) {

		if (!strstr($_SERVER['REQUEST_URI'], RESTAURANTS_LIST_CUSTOM_PATH)) {
			// Permanent redirect to the new custom URL
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/' . RESTAURANTS_LIST_CUSTOM_PATH . '/restaurants-list.html');
		}

		if (!empty($_POST['recommended_zip_code']) && !empty($_POST['recommended_email']) && !empty($_POST['recommended_restaurant'])) {
			// This is a new restaurant recommandation
			recommendedRestaurant::insert(checkQuotes($_POST['recommended_zip_code']), checkQuotes($_POST['recommended_email']), checkQuotes($_POST['recommended_restaurant']), $_SERVER['REMOTE_ADDR']);
			echo json_encode(array(
					'message' => SUCCESSFUL_RECOMMANDATION
				));
			die();
		}

		check4Search();

		if (!empty($_SESSION['s_venezvite']['search']) && is_array($_SESSION['s_venezvite']['search']) && count($_SESSION['s_venezvite']['search'])==17) {
			$advance_order_time = ORDER_COOKING_TIME + (@$_SESSION['s_venezvite']['search']['type']=='T' ? 0 : DELIVERY_TIME);
			$dateTime = strtotime($_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time']);

			$search_restaurants = restaurant::search($_SESSION['s_venezvite']['search']['location'], $_SESSION['s_venezvite']['search']['search_term'], $dateTime, $_SESSION['s_venezvite']['search']['coords'], $user, $_SESSION['s_venezvite']['search']['sort'], $_SESSION['s_venezvite']['search']['cuisines'], $_SESSION['s_venezvite']['search']['category'], $_SESSION['s_venezvite']['search']['rating'], $_SESSION['s_venezvite']['search']['type'], $_SESSION['s_venezvite']['search']['price_level']);

			$open_restaurants = array();
			$closed_restaurants = array();
			$restaurant_locations = array();
			$cuisine_types = array();

			foreach ($search_restaurants as $search_restaurant) {
				if ($search_restaurant->opened) {
					$open_restaurants[] = (require REL . '/views/inc/restaurant-row.inc.php');
					$restaurant_locations[] = array($search_restaurant->customURL, $search_restaurant->latitude, $search_restaurant->longitude, $search_restaurant->restaurantName);
				} else {
					$closed_restaurants[] = (require REL . '/views/inc/restaurant-row.inc.php');
				}

				foreach ($search_restaurant->cuisineTypes as $cuisine_type) {
					if (!array_key_exists($cuisine_type->idCuisineType, $cuisine_types)) {
						$cuisine_types[$cuisine_type->idCuisineType] = array(
							'cuisine_type'	=> $cuisine_type->cuisineType, 
							'restaurants'	=> 1
						);
					} else {
						$cuisine_types[$cuisine_type->idCuisineType]['restaurants']++;
					}
				}
			}

			asort($cuisine_types);
		} else {
			// No search parameters, return to home
			unset($_SESSION['s_venezvite']['search']);

			header('HTTP/1.0 404 Not Found');
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
			die();
		}
	}
