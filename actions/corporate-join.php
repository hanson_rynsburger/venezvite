<?php
	if (@$is_included) {
		
		if (!empty($_POST)) {
			if (!empty($_POST['last_name']) && !empty($_POST['first_name']) && !empty($_POST['job_title']) && isset($_POST['department']) && !empty($_POST['mobile']) && 
				is_numeric(@$_POST['country']) && ($country = country::getByID($_POST['country'])) && 
				!empty($_POST['city']) && !empty($_POST['company_name']) && !empty($_POST['address']) && !empty($_POST['zip']) && 
				is_numeric(@$_POST['latitude']) && is_numeric(@$_POST['longitude']) && !empty($_POST['phone']) && isset($_POST['employees']) && 
				is_array(@$_POST['email']) && !empty($_POST['email'][0]) && is_array(@$_POST['confirm_email']) && !empty($_POST['confirm_email'][0]) && 
				!empty($_POST['username']) && $_POST['username']==@$_POST['confirm_username'] && 
				!($already_exists = corporateAccount::checkDuplicateUsername($_POST['username'])) && !($already_exists = user::checkDuplicateEmail($_POST['username'])) && 
				strlen(@$_POST['password'])>=5 && $_POST['password']==@$_POST['confirm_password'] && 
				@$_POST['terms']=='Y'
			) {
				$error = false;
				
				$approvedEmails = array();
				foreach ($_POST['email'] as $email) {
					if (isEmail($email)) {
						$approvedEmails[] = $email;
					}
				}
				if (count($approvedEmails)==0) {
					$error = true;
					$_SESSION['s_venezvite']['corporate_registration_message'] = INVALID_EMAILS;
				}
				
				if (!$error) {
					$corporateAccount = corporateAccount::insert(
						checkQuotes($_POST['first_name']), 
						checkQuotes($_POST['last_name']), 
						checkQuotes($_POST['job_title']), 
						checkQuotes(@$_POST['department']), 
						checkQuotes($_POST['address']), 
						$_POST['city'], 
						$_POST['zip'], 
						$_POST['latitude'], 
						$_POST['longitude'], 
						checkQuotes($_POST['company_name']), 
						checkQuotes($_POST['phone']), 
						checkQuotes($_POST['mobile']), 
						$_POST['employees'], 
						$_POST['username'], 
						checkQuotes($_POST['password']), 
						$approvedEmails, 
						$_SESSION['s_venezvite']['language']->idLanguage, 
						$_SERVER['REMOTE_ADDR']
					);
					
					if ($corporateAccount) {
						$_SESSION['s_venezvite']['corporate_registration_message'] = REGISTRATION_CONFIRMATION;
						header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/corporate-join.html');
						die();
						
					} else {
						$_SESSION['s_venezvite']['corporate_registration_message'] = UNABLE_TO_REGISTER;
					}
				}
			}
		}
		
		$countries = country::list_();
	}
