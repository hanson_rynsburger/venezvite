<?php
	if (!empty($is_included) && !empty($current_page)) {
		if (empty($_SESSION['s_venezvite']['language'])) {
			// This is the first access of the website, so let's try to determine where the visitor comes from, 
			// in order to choose its default language
			//$ipinfodb = getCoordsByIP();
			
			$language = language::getByCountryCode('CH'/*$ipinfodb['countryCode']*/);
			if (!$language) {
				// It seems the current country doesn't exist, so let's choose the default language
				$language = language::getDefault();
			}
			
			if ($language) {
				$_SESSION['s_venezvite']['language'] = $language;
			} else {
				die('There\'s no default language defined!');
			}
		}
		/*
		if (@$_SERVER['HTTPS']!='on') {
			// Redirect to https
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: https:' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/' . ($current_page!='home' ? ($current_page!='restaurant' ? implode('/', $_GET['page']) . '.html' : $restaurant->customURL . '.html') : ''));
			die();
		}
		*/
		
		
		if (!empty($_GET['lang']) && is_dir(REL . DS . 'lang' . DS . $_GET['lang']) && is_file(REL . DS . 'lang' . DS . $_GET['lang'] . DS . 'public.lang.php')) {
			// Permanently redirect the old "lang" format to the new "language" one
			// (used for /LANG-CODE/ style URLs)
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . ROOT . $_GET['lang'] . '/' . 
				($current_page!='home' ? ($current_page!='restaurant' ? implode('/', $_GET['page']) . '.html' : $restaurant->customURL . '.html') : ''));
			die();
		}
		
		if (!empty($_GET['language'])) {
			if ($_GET['language'] != $_SESSION['s_venezvite']['language']->languageAcronym) {
				$language = language::getByAcronym($_GET['language']);
				
				if ($language) {
					$_SESSION['s_venezvite']['language'] = $language;
					// Redirect the user to the page he was in before the language change
					//header('Location: ' . (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ROOT));
					//die();
				} else {
					// Unknown language
					header('HTTP/1.0 404 Not Found');
					die();
				}
			}
			
		} else {
			// Default to the English version
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/' . ($current_page!='home' ? ($current_page!='restaurant' ? implode('/', $_GET['page']) . '.html' : $restaurant->customURL . '.html') : ''));
			die();
		}
		
		if (!in_array($current_page, array('checkout', 'payment', 'cart-load', 'cart-remove', 'check-unique-email', 'login', 'register', 'use-promo', 'restaurants-list')) && @$_SERVER['HTTPS']=='on') {
			// Redirect to http
			header('Location: http://' . str_replace(array('https:', '//'), '', ROOT) . $_SESSION['s_venezvite']['language']->languageAcronym . '/' . 
				($current_page!='home' ? ($current_page!='restaurant' ? implode('/', $_GET['page']) . '.html' : $restaurant->customURL . '.html') : ''));
			die();
		}
		
		// Language handling
		require_once REL . DS . 'lang' . DS . $_SESSION['s_venezvite']['language']->languageAcronym . DS . 'public.lang.php';
		if (is_file(REL . DS . 'lang' . DS . $_SESSION['s_venezvite']['language']->languageAcronym . DS . $current_page . '.lang.php')) {
			require_once REL . DS . 'lang' . DS . $_SESSION['s_venezvite']['language']->languageAcronym . DS . $current_page . '.lang.php';
		} else {
			//error_log('No specific language file for the ' . $current_page . ' page.');
		}
		
		
		$user = user::getFromSession();
		$corporateAccount = corporateAccount::getFromSession();
		if (!empty($user) || !empty($corporateAccount)) {
			$entity = (!empty($user) ? $user : $corporateAccount);
			//$searchAddresses = $entity->listSearchAddresses();
			$delivery_addresses = userAddress::list_($entity);
		}
		$restaurantAdmin = restaurantAdmin::getFromSession();
		
		$languages = language::list_();
		
		
		if (!empty($_POST['suggestion_name']) && !empty($_POST['suggestion_restaurant']) && !empty($_POST['suggestion_address'])) {
			// This is a new restaurant recommandation
			recommendedRestaurant::insert(checkQuotes($_POST['suggestion_address']), checkQuotes($_POST['suggestion_name']), checkQuotes($_POST['suggestion_restaurant']), $_SERVER['REMOTE_ADDR']);
			$_SESSION['s_venezvite']['error'][] = SUCCESSFUL_RECOMMANDATION;
		}
		
		
		/**
		 * Check if the required search fields have been submitted and, if so, 
		 * process them accordingly
		 * 
		 * Used in the restaurants list and restaurant's menu pages
		 * 
		 **/
		function check4Search() {
			global $current_page;
			global $entity;
			global $restaurant;
			
			$advance_order_time = (@$restaurant->preparationTime ? $restaurant->preparationTime : ORDER_COOKING_TIME) + 
				(@$_SESSION['s_venezvite']['search']['type']=='T' ? 0 : 
					(@$restaurant->current_delivery_zone ? @$restaurant->current_delivery_zone['estimated_time'] : DELIVERY_TIME)
				);
			if (empty($_SESSION['s_venezvite']['search']['date']) || empty($_SESSION['s_venezvite']['search']['time']) || 
				strtotime($_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time'])<strtotime('+' . $advance_order_time . ' minutes')) {
				// The currently submitted date and time are, somehow, older than ADVANCE_ORDER_TIME minutes past the current date/time
				// so let's change the submitted date value, according to the hour's value
				$_SESSION['s_venezvite']['search']['date'] = date('Y-m-d', strtotime('+' . $advance_order_time . ' minutes'));
				$_SESSION['s_venezvite']['search']['time'] = getCurrentOrderTime(@$restaurant->current_delivery_zone['estimated_time'], @$restaurant->preparationTime);
			}
			//error_log(print_r($_POST, 1));
			
			if ((in_array(@$_POST['type'], array('D', 'T')) && 
					@$_POST['location'] && 
					isset($_POST['latitude']) && (empty($_POST['latitude']) || is_numeric($_POST['latitude'])) && 
					isset($_POST['longitude']) && (empty($_POST['longitude']) || is_numeric($_POST['longitude']))) || 
				@$_GET['s']) {
				// New search
				//error_log('New search');
				$city = null;
				if (is_numeric(@$_POST['location']) && !empty($entity)) {
					// Maybe a delivery address?
					$delivery_address = userAddress::getByID($_POST['location']);
					
					if ($delivery_address && 
						(get_class($entity)=='user' ? $entity->idUser==@$delivery_address->user->idUser : $entity->idCorporateAccount==@$delivery_address->corporateAccount->idCorporateAccount)
					) {
						$_POST['delivery_address_id'] = $delivery_address->idAddress;
						$_POST['building_no'] = $delivery_address->buildingNo;
						$_POST['address'] = $delivery_address->address;
						$city = $delivery_address->city;
						$_POST['zip_code'] = $delivery_address->zipCode;
						$_POST['latitude'] = $delivery_address->latitude;
						$_POST['longitude'] = $delivery_address->longitude;
					}
				}
				
				
				if (empty($_POST['delivery_address_id'])) {
					if (@$_GET['s']) {
						// Address search query
						// Let's split it to see if it contains the required components
						$search = explode('|', $_GET['s']);
						
						if (count($search)==3 && is_numeric($search[1]) && is_numeric($search[2])) {
							$_POST['location'] = $search[0];
							$_POST['latitude'] = $search[1];
							$_POST['longitude'] = $search[2];
							
						} else {
							// Something's not right, fuck it
							header('HTTP/1.0 404 Not Found');
							header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
							die();
						}
						
					} else {
						// Regular search
						
						if (@$_POST['latitude'] && @$_POST['longitude']) {
							// Most likely, certain address chosen from the Google Autosuggest widget
							
							if (@$_POST['city_name']) {
								// Maybe the currently selected address has a city attached that we already have in the DB
								// In this case, we'll store it in the search session for adding it to the address form later on, in the checkout process
								//$cities = city::search($_POST['city_name']);
								
								//if(count($cities)>0){
									$city = $_POST['city_name'];//array('id'=>$cities[0]->idCity, 'name'=>$cities[0]->cityName);
								//}
							}
							
						} else {
							// This is just a random string, so let's see if it matches any address (using Google Geocoding API)
							
							if (is_numeric($_POST['location']) && (strlen($_POST['location'])==4 || $_POST['location']==5)) {
								// This might be a zip code, either from Switzerland or from France
								// Let's see if we already have it in the DB
								
								$zipCode = zipCode::get($_POST['location']);
								if (!$zipCode) {
									
									$geocodingResponse = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . $_POST['location'] . ',+' . (strlen($_POST['location'])==4 ? 'Switzerland' : 'France') . '&sensor=false'));
									if ($geocodingResponse && !empty($geocodingResponse->status) && $geocodingResponse->status=='OK' && !empty($geocodingResponse->results)) {
										
										$geometry = $geocodingResponse->results[0]->geometry;
										$zipCode = zipCode::insert($_POST['location'], $geometry->location->lat, $geometry->location->lng);
										
									} else {
										// Nothing found!
										header('HTTP/1.0 404 Not Found');
										header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
										die();
									}
								}
								
								$_POST['location'] = $zipCode->zipCode . ', ' . (strlen($_POST['location'])==4 ? 'Switzerland' : 'France');
								$_POST['zip_code'] = $zipCode->zipCode;
								$_POST['latitude'] = $zipCode->latitude;
								$_POST['longitude'] = $zipCode->longitude;
								$_POST['country_code'] = (strlen($_POST['location'])==4 ? 'CH' : 'FR');
								
							} else {
								// Let's search through our cities table for the currently submitted value
								$cities = city::search(checkQuotes($_POST['location']));
								
								if (count($cities)>0) {
									$_POST['location'] = $cities[0]->cityName . ', ' . $cities[0]->country->countryAcronym;
									$_POST['latitude'] = $cities[0]->latitude;
									$_POST['longitude'] = $cities[0]->longitude;
									$_POST['country_code'] = $cities[0]->country->countryAcronym;
									
									$city = $cities[0]->cityName;//array('id'=>$cities[0]->idCity, 'name'=>$cities[0]->cityName);
									
								} else {
									// It seems to be something outside any known value, so let's see what Google Geocoding API suggests
									
									$geocodingResponse = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode(checkQuotes($_POST['location'])) . '&sensor=false'));
									if ($geocodingResponse && !empty($geocodingResponse->status) && $geocodingResponse->status=='OK' && !empty($geocodingResponse->results)) {
										$_POST['location'] = $geocodingResponse->results[0]->formatted_address;
										
										$geometry = $geocodingResponse->results[0]->geometry;
										$_POST['latitude'] = $geometry->location->lat;
										$_POST['longitude'] = $geometry->location->lng;
										
									} else {
										// Nothing found!
										header('HTTP/1.0 404 Not Found');
										header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
										die();
									}
								}
							}
						}
					}
				}
				
				
				$_SESSION['s_venezvite']['search'] = array(
					'id'			=> @$_POST['delivery_address_id'], 
					'type'			=> @$_POST['type'], 
					'location'		=> checkQuotes($_POST['location']), 
					'search_term'	=> (@$_POST['search_term'] ? checkQuotes($_POST['search_term']) : null), 
					'building_no'	=> (@$_POST['building_no'] ? checkQuotes($_POST['building_no']) : null), 
					'address'		=> (@$_POST['address'] ? checkQuotes($_POST['address']) : null), 
					'city'			=> $city, 
					'country_code'	=> (@$_POST['country_code'] ? checkQuotes($_POST['country_code']) : null), 
					'zip_code'		=> (@$_POST['zip_code'] ? checkQuotes($_POST['zip_code']) : null), 
					'coords'		=> array(
						'lat'	=> $_POST['latitude'], 
						'lng'	=> $_POST['longitude']
					), 
					'date'			=> (@$_POST['date'] ? $_POST['date'] : null), 
					'time'			=> (@$_POST['time'] ? $_POST['time'] : null), 
					'category'		=> null, 
					'sort'			=> (@$_POST['sort'] ? $_POST['sort'] : null), 
					'cuisines'		=> (@$_POST['cuisines'] ? $_POST['cuisines'] : null), 
					'rating'		=> (@$_POST['rating'] ? $_POST['rating'] : null), 
					'price_level'	=> (@$_POST['price_level'] ? $_POST['price_level'] : null)
				);
				
				// Redirect the user to the same page, to prevent re-submitting the form (on refresh)
				header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/' . ($current_page!='restaurant' ? implode('/', $_GET['page']) . '.html' : $restaurant->customURL . '.html'));
				die();
			}
		}
		
		
		if (@$_SESSION['s_venezvite']['search']['coords']) { // @$_SESSION['s_venezvite']['search']['type']=='D'
			if (empty($restaurant) && ($restaurant_path = @json_decode($_COOKIE['c_venezvite'])->restaurant)) {
				$restaurant = restaurant::getByURL($restaurant_path);
			}
			
			if (!empty($restaurant) && ($delivery_zone = $restaurant->checkDeliveryLocation($_SESSION['s_venezvite']['search']['coords']))) {
				// Get the restaurant's current delivery zone
				$restaurant->current_delivery_zone = array(
						'id'				=> ($delivery_zone ? $delivery_zone->idDeliveryZone : ''), 
						'delivery_fee'		=> ($delivery_zone ? $delivery_zone->deliveryFee : ''), 
						'minimum_delivery'	=> ($delivery_zone ? $delivery_zone->minimumDelivery : ''), 
						'estimated_time'	=> ($delivery_zone ? $delivery_zone->estimatedTime : '')
				);
			}
		}
	}
