<?php
	if (!@$is_included) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	} elseif (empty($restaurantAdmin)) {
		// Go to the login page
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
		die();
	}
	
	
	
	if (!empty($_POST['category'])) {
		if (@is_numeric($_POST['id']) && ($menu_category = menuCategory::getByID($_POST['id'])) && 
			$menu_category->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
			// This should be an update
			$menu_category->update(checkQuotes($_POST['category']));
			
		} else {
			// Insert
			$menu_category = menuCategory::insert($restaurantAdmin->restaurant, checkQuotes($_POST['category']));
		}
		
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/menu-categories.html' . (!empty($menu_category) ? '#menu_category_' . $menu_category->idMenuCategory : ''));
		die();
	}
	
	
	
	$menu_categories = menuCategory::list_($restaurantAdmin->restaurant);
	$menu_items = menuItem::list_($restaurantAdmin->restaurant, false, true, true);
