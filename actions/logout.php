<?php
	if (@$is_included) {
		if (user::isLogged()) {
			$user = user::getFromSession();
			$user->logout();
		} else if(corporateAccount::isLogged()) {
			$corporateAccount = corporateAccount::getFromSession();
			$corporateAccount->logout();
		} else if(restaurantAdmin::isLogged()) {
			$restaurantAdmin = restaurantAdmin::getFromSession();
			$restaurantAdmin->logout();
		}
	}
	
	header('Location: ' . (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/'));
	die();
