<?php
	function loadLatestVersion($file) {
		if(/*DEBUG && */is_file(REL . '/' . $file)){
			return $file . '?' . filemtime(REL . '/' . $file);
		}else{
			// Weird, but...
			return $file;
		}
	}
	
	function checkErrors() {
		if (!empty($_SESSION['s_venezvite']['error'])) {
			echo '<script>', 
				'show_top_message(\'' . str_replace(array("\n", "\r"), '', 
					str_replace('\'', '\\\'', 
					nl2br(implode('\n', $_SESSION['s_venezvite']['error'])))) . '\');', 
				'</script>';
			unset($_SESSION['s_venezvite']['error']);
		}
	}
	
	/**
	 * Validate an email address.
	 * Provide email address (raw input)
	 * Returns true if the email address has the email 
	 * address format and the domain exists.
	 */
	function isEmail($email) {
		$isValid = true;
		$atIndex = strrpos($email, '@');
		
		if (is_bool($atIndex) && !$atIndex) {
			$isValid = false;
		} else {
			$domain = substr($email, $atIndex+1);
			$local = substr($email, 0, $atIndex);
			$localLen = strlen($local);
			$domainLen = strlen($domain);
			
			if ($localLen<1 || $localLen>64) {
				// local part length exceeded
				$isValid = false;
			} else if($domainLen<1 || $domainLen>255) {
				// domain part length exceeded
				$isValid = false;
			} else if($local[0]=='.' || $local[$localLen-1]=='.') {
				// local part starts or ends with '.'
				$isValid = false;
			} else if(preg_match('/\\.\\./', $local)) {
				// local part has two consecutive dots
				$isValid = false;
			} else if(!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
				// character not valid in domain part
				$isValid = false;
			} else if(preg_match('/\\.\\./', $domain)) {
				// domain part has two consecutive dots
				$isValid = false;
			} else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace('\\\\', '', $local))) {
				// character not valid in local part unless 
				// local part is quoted
				if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace('\\\\', '', $local))) {
					$isValid = false;
				}
			}
			/*
			if($isValid && !(checkdnsrr($domain, 'MX') || checkdnsrr($domain, 'A'))){
			// domain not found in DNS
			$isValid = false;
			}
			*/
		}
		
		return $isValid;
	}
	
	function checkQuotes($string) {
		return get_magic_quotes_gpc() ? stripslashes($string) : $string;
	}
	
	function getUniqueFile($file, $path){
		// Returns unique file names for specific paths
		$appendix = mt_rand(1, 9999);
		
		$fileComponents = explode('.', $file);
		$fileExtension = array_pop($fileComponents);
		$fileName = implode('.', $fileComponents);
		
		while(is_file($path . $fileName . '-' . $appendix . '.' . $fileExtension)){
			getUniqueFile($file, $path);
		}
		
		return $fileName . '-' . $appendix . '.' . $fileExtension;
	}
	
	function urlfy($path){
		$path = trim($path);
		
		$path = str_replace(' ', '-', $path);
		$path = str_replace('"', '', $path);
		$path = str_replace('\'', '', $path);
		$path = str_replace('.', '', $path);
		$path = str_replace(',', '-', $path);
		$path = str_replace('?', '', $path);
		$path = str_replace('=', '-', $path);
		$path = str_replace('&', '&amp;', $path);
		$path = str_replace('%', '', $path);
		$path = str_replace('/', '-', $path);
		$path = str_replace('<', '(', $path);
		$path = str_replace('>', ')', $path);
		
		while(strstr($path, '--')){
			$path = str_replace('--', '-', $path);
		}
		
		$path = htmlspecialchars($path);
		//$path = strtolower($path);
		
		return $path;
	}
	
	function filefy($file){
		$file = trim($file);
		
		$file = str_replace(' ', '-', $file);
		$file = str_replace(',', '-', $file);
		$file = str_replace('(', '', $file);
		$file = str_replace(')', '', $file);
		$file = str_replace('=', '-', $file);
		$file = str_replace('%', '', $file);
		
		while(strstr($file, '--')){
			$file = str_replace('--', '-', $file);
		}
		
		$file = htmlspecialchars($file);
		$file = strtolower($file);
		
		return $file;
	}
	
	function sendMail($email, $name, $title, $message, $attachments = null, $template = null, $escape = true){
		require_once REL . VERSION_REL_PATH . 'classes' . DS . 'phpMailer' . DS . 'class.phpmailer.php';
		
		$mail = new PHPMailer();
		
		if(!empty($_SESSION['s_venezvite']['language'])){
			if($_SESSION['s_venezvite']['language']->languageAcronym!='en'){
				$mail->SetLanguage($_SESSION['s_venezvite']['language']->languageAcronym); // Hopefully, you'd have copied the current language's phpMailer translation
			}
		}
		
		if(IS_SMTP){
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->SMTPDebug = false; // enables SMTP debug information (for testing)
			$mail->SMTPAuth = true; // enable SMTP authentication
			//$mail->SMTPSecure = 'ssl';
			
			$mail->Host = EMAIL_HOST; // sets the SMTP server
			$mail->Port = EMAIL_PORT; // set the SMTP port for the GMAIL server
			$mail->Username = EMAIL_USERNAME; // SMTP account username
			$mail->Password = EMAIL_PASSWORD; // SMTP account password
		}
		
		$mail->From = EMAIL_ADDRESS;
		$mail->FromName = EMAIL_NAME;
		$mail->AddReplyTo(EMAIL_ADDRESS, EMAIL_NAME);
		
		if (strstr($email, ',')) {
			// These should be multiple email addresses, so let's split them
			$email = explode(',', $email);
		}
		
		if (!is_array($email)) {
			$mail->AddAddress($email, $name);
		} else {
			foreach ($email as $email_) {
				if (!empty($email_)) {
					$mail->AddAddress($email_, $name);
				}
			}
		}
		
		$message = str_replace('{$root}', 'http:' . ROOT, $message);
		
		$mail->Subject = $title;
		$mail->AltBody = preg_replace('/(\r\n)+/', "\r\n", strip_tags($message));
		
		if($template && is_file(REL . '/newsletter/' . $template . '.html')){
			$html = file_get_contents(REL . '/newsletter/' . $template . '.html');
			$html = str_replace('{$root}', 'http:' . ROOT, $html);
			$html = str_replace('{$title}', $title, $html);
			$html = str_replace('{$content}', ($escape ? nl2br($message) : $message), $html);
		}else{
			$html = ($escape ? nl2br($message) : $message);
		}

		$mail->MsgHTML($html);
		
		if($attachments){
			foreach($attachments as $attachment){
				$mail->AddAttachment($attachment);
			}
		}
		
		if(DEBUG){
			error_log($mail->Subject);
			error_log($mail->Body);
		}
		
		/* echo "<hr/>";
		echo( $mail->Body );;
		exit(); */
		
		return $mail->Send();
		/*
		$headers = 'From: ' . EMAIL_NAME . ' <' . EMAIL_ADDRESS . '>
X-Mailer: PHP
X-Priority: 1
Return-Path: <' . EMAIL_ADDRESS . '>';
		
		return @mail($email, $title, $message, $headers);
		*/
	}
	
	function getCoordsByIP(){
		$ipinfodb = json_decode(@file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key=8c1ad82a94010058b2a961fb653771daff4e6573f29bfc9ff824ce1c0e5490f0&format=json&ip=' . ($_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '212.147.77.227' : $_SERVER['REMOTE_ADDR'])), true);
		if(!$ipinfodb){
			// Default to Lausanne / Pully
			$ipinfodb = array('latitude'=>46.5103, 'longitude'=>6.66183, 'countryCode'=>'CH', 'countryName'=>'Switzerland');
		}
		
		return $ipinfodb;
	}
	
	/**
	* Returns distance between two coordinates
	* in kilometers
	* 
	**/
	function getLatLngDistance($origin, $destination, $straightLine = false){
		/*
		if(!$straightLine){
			// Let's first try to return the distance from Google Maps
			$url = 'http://maps.googleapis.com/maps/api/directions/json?origin=' . $origin['lat'] . ',' . $origin['lng'] . 
				'&destination=' . $destination['lat'] . ',' . $destination['lng'] . '&avoid=highways&sensor=false';
			$distance = json_decode(@file_get_contents($url));
			
			if (!empty($distance) && !empty($distance->status) && $distance->status=='OK' && !empty($distance->routes)) {
				
				$total = 0;
				foreach($distance->routes[0]->legs as $leg){
					$total = $leg->distance->value;
				}
				
				return round($total / 1000, 2);
				
			} else {
				return round(6371 * 2 * asin(sqrt(pow(sin(($origin['lat'] - abs($destination['lat'])) * pi() / 180 / 2), 2) + cos($origin['lat'] * pi() / 180) * cos(abs($destination['lat']) * pi() / 180) * pow(sin(($origin['lng'] - $destination['lng']) * pi() / 180 / 2), 2))), 2);
			}
			
		} else {
			*/
			$distance = round(6371 * 2 * asin(sqrt(pow(sin(($origin['lat'] - abs($destination['lat'])) * pi() / 180 / 2), 2) + cos($origin['lat'] * pi() / 180) * cos(abs($destination['lat']) * pi() / 180) * pow(sin(($origin['lng'] - $destination['lng']) * pi() / 180 / 2), 2))), 2);
			
			/*
			$R = 6371000; // metres
			$phi1 = deg2rad($origin['lat']);
			$phi2 = deg2rad($destination['lat']);
			$delta_phi = deg2rad($destination['lat'] - $origin['lat']);
			$delta_lambda = deg2rad($destination['lng'] - $origin['lng']);
			
			$a = sin($delta_phi / 2) * sin($delta_phi / 2) + 
				cos($phi1) * cos($phi2) * 
				sin($delta_lambda / 2) * sin($delta_lambda / 2);
			$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
			$d = $R * $c;
			*/
			/*
			$phi1 = deg2rad($destination['lat']);
			$phi2 = deg2rad($origin['lat']);
			$delta_lambda = deg2rad($origin['lng'] - $destination['lng']);
			$R = 6371000; // gives d in metres
			$d = acos(sin($phi1) * sin($phi2) + cos($phi1) * cos($phi2) * cos($delta_lambda)) * $R;
			*/
			
			return $distance * 1.35; // 1.35 = 35% tolerance
		//}
	}
	
	function formatDate($date, $full = false){
		setlocale(LC_TIME, $_SESSION['s_venezvite']['language']->localeCode);
		$timestamp = strtotime($date);
		
		$is_asap = false;
		if ($timestamp>=strtotime('-45 minutes') && $timestamp<=strtotime('-15 minutes')) {
			$is_asap = true;
		}
		
		return ($full ? strftime('%x @ ' . ($is_asap ? ASAP . ' (' : ' ') . '%H:%M' . ($is_asap ? ')' : ''), $timestamp) : strftime('%x', $timestamp));
	}
	
	function prettyDateTime($datetime, $full = true, $separator = ' ', $delivery_time = null, $preparation_time = null) {
		setlocale(LC_TIME, $_SESSION['s_venezvite']['language']->localeCode);
		$timestamp = strtotime($datetime);
		
		switch (date('Y-m-d', $timestamp)) {
			case date('Y-m-d'):
				$date = TODAY;
				break;
			case date('Y-m-d', strtotime('-1 day')):
				$date = YESTERDAY;
				break;
			case date('Y-m-d', strtotime('+1 day')):
				$date = TOMORROW;
				break;
			default:
				$date = strftime('%x', $timestamp);
		}
		
		$advance_order_time = ($preparation_time!==null ? $preparation_time : ORDER_COOKING_TIME) + (@$_SESSION['s_venezvite']['search']['type']=='T' ? 0 : ($delivery_time!==null ? $delivery_time : DELIVERY_TIME));
		if ($timestamp<strtotime('+' . ($advance_order_time + 15) . ' minutes')) {
			$time = ASAP;
		} else {
			$time = date('H:i', $timestamp);
		}
		
		return $date . ($full ? $separator . $time : '');
	}
	
	function commonDatesList($selected = null, $return_array = false) {
	    
	    $temp_array = array();
	    
		for ($i=0; $i<7; $i++) {
			$timestamp = strtotime('+' . $i . ' days');
			$date = date('Y-m-d', $timestamp);
			$dow = date('N', $timestamp);
			
			if ( $return_array ) {
				$temp_array[ $date ] = prettyDateTime($date, false);
			}
			else {
				echo '<option data-dow="', $dow, '"', ($selected==$date ? ' selected="selected"' : ''), ' value="', $date, '">', prettyDateTime($date, false), '</option>';
			}
		}
		
		if ( $return_array ) { return $temp_array; }
	}
	
	function commonTimesList($date, $selected = null, $delivery_time = null, $preparation_time = null, $return_array = false) {
		$temp_array = array();

		if (empty($date)) {
			$date = date('Y-m-d');
		}
		$advance_order_time = ($preparation_time!==null ? $preparation_time : ORDER_COOKING_TIME) + (@$_SESSION['s_venezvite']['search']['type']=='T' ? 0 : ($delivery_time!==null ? $delivery_time : DELIVERY_TIME));
		$time = (int)date('G') * 4 + round(((int)date('i') + $advance_order_time) / 15); // +(ADVANCE_ORDER_TIME / 15) because we only display time that is ADVANCE_ORDER_TIME minutes past the current time, rounded to its closest 15' interval
		$current_time = null;

		for ($i=0; $i<96; $i++) {
			if ($time>95) {
				$time = $time - 96;
			}
			if ($time>12 && $time<28) {
				// We don't display the 03:00-07:00 interval
				$time++;
				continue;
			}
			$hour = floor($time / 4);
			$minute = ($time / 4 - $hour) * (15 * 4);
			if ($hour<10) {
				$hour = '0' . $hour;
			}
			if ($minute<10) {
				$minute = '0' . $minute;
			}
			$hour_minute = $hour . ':' . $minute;

			$displayed_hour_minute = $hour_minute;
			if (strtotime($date)==strtotime('today') && empty($current_time)) {
				$current_time = $hour_minute;
				$displayed_hour_minute = ASAP;
			}

			if ( $return_array ) {
			    $temp_array[ $hour_minute ] = $displayed_hour_minute;
			}
			else {
				echo '<option', ($selected==$hour_minute ? ' selected="selected"' : ($hour_minute==$current_time ? ' selected="selected"' : '')), ' value="', $hour_minute, '">', $displayed_hour_minute, '</option>';
			}
			$time++;
		}

		if ( $return_array ) { return $temp_array; }
	}
	
	/**
	 * Returns the nearest time that an order could be placed for, based on the ADVANCE_ORDER_TIME constant's value
	 * 
	 **/
	function getCurrentOrderTime($delivery_time = null, $preparation_time = null) {
		$advance_order_time = ($preparation_time!==null ? $preparation_time : ORDER_COOKING_TIME) + (@$_SESSION['s_venezvite']['search']['type']=='T' ? 0 : ($delivery_time!==null ? $delivery_time : DELIVERY_TIME));
		$time = (int)date('G') * 4 + round(((int)date('i') + $advance_order_time) / 15); // +(ADVANCE_ORDER_TIME / 15) because we only display time that is ADVANCE_ORDER_TIME minutes past the current time, rounded to its closest 15' interval
		
		if ($time>95) {
			// For times past midnight, we'll get back to 0
			$time = $time - 96;
		}
		$hour = floor($time / 4);
		$minute = ($time / 4 - $hour) * (15 * 4);
		if ($hour<10) {
			$hour = '0' . $hour;
		}
		if ($minute<10) {
			$minute = '0' . $minute;
		}
		$hour_minute = $hour . ':' . $minute;
		
		return $hour_minute;
	}
	
	function passwordCheck($password){
		$score = 0;
		$len = strlen($password);
		$regexes = array(
				'/[a-z]/', // lowercase letters
				'/[A-Z]/', // uppercase letters
				'/[\d]/', // numbers
				'/(.*[0-9].*[0-9].*[0-9])/', // >= 3 numbers
				'/.[!,@,#,$,%,^,&,*,?,_,~]/', // special characters
				'/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/', // >= 2 special characters
				'/([a-z].*[A-Z])|([A-Z].*[a-z])/', // both upper and lowercase letters
				'/([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])/', // both numbers and letters
				'/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/' // letters, numbers, special characters
			);
		
		if($len>2 && $len<5)
			$score = ($score + 3);
		else if($len>4 && $len<8)
			$score = ($score + 6);
		else if($len>7 && $len<16)
			$score = ($score + 12);
		else if($len>15)
			$score = ($score + 18);
		
		if(preg_match($regexes[0], $password)) $score = ($score + 1);
		if(preg_match($regexes[1], $password)) $score = ($score + 5);
		
		if(preg_match($regexes[2], $password)) $score = ($score + 5);
		if(preg_match($regexes[3], $password)) $score = ($score + 5);
		
		if(preg_match($regexes[4], $password)) $score = ($score + 5);
		if(preg_match($regexes[5], $password)) $score = ($score + 5);
		
		if(preg_match($regexes[6], $password)) $score = ($score + 2);
		if(preg_match($regexes[7], $password)) $score = ($score + 2);
		if(preg_match($regexes[8], $password)) $score = ($score + 2);
		
		return ($score * 2); // For 100%
	}
	
	/**
	 * Check for existing translations for the current menu category
	 * in the languages files. If it exists, it returns it.
	 * If not, the original menu category will be returned (for custom values)
	 * 
	 **/
	function translateMenuCategories($category){
		$constant = 'MC_' . strtoupper(str_replace(' ', '_', $category));
		
		if(defined($constant)){
			return constant($constant);
		}else{
			return $category;
		}
	}
	
	/**
	 * Transform decimal coordinates into integer numbers
	 * to be used by the pointLocation class
	 * 
	 **/
	function coord2Point($coordinate) {
		return round($coordinate, 5) * 100000;
	}
	
	function getCardType($ccn) {
		$card_types = array(
			'Visa'					=> '/^4\d{12}(\d{3})?$/',
			'Mastercard'			=> '/^(5[1-5]\d{4}|677189)\d{10}$/',
			'Discover'				=> '/^(6011|65\d{2}|64[4-9]\d)\d{12}|(62\d{14})$/',
			'American Express'		=> '/^3[47]\d{13}$/',
			'Diners Club'			=> '/^3(0[0-5]|[68]\d)\d{11}$/',
			'JCB'					=> '/^35(28|29|[3-8]\d)\d{12}$/',
			'Switch'				=> '/^6759\d{12}(\d{2,3})?$/',
			'Solo'					=> '/^6767\d{12}(\d{2,3})?$/',
			'Dankort'				=> '/^5019\d{12}$/',
			'Maestro'				=> '/^(5[06-8]|6\d)\d{10,17}$/',
			'Forbrugsforeningen'	=> '/^600722\d{10}$/',
			'Laser'					=> '/^(6304|6706|6709|6771(?!89))\d{8}(\d{4}|\d{6,7})?$/'
		);
		
		foreach ($card_types as $brand => $val) {
			if (preg_match($val, $ccn)) {
				return $brand;
			}
		}
	}
	
	function getCartTotal($restaurant) {
		$total = 0;
		
		if (!empty($_SESSION['s_venezvite']['carts'][$restaurant->customURL])) {
			foreach ($_SESSION['s_venezvite']['carts'][$restaurant->customURL] as $item) {
				if (!empty($item['menu_item'])) {
					// Menu item
					$base_price = $item['unit_price'];
					$quantity = $item['quantity'];
					
					$option_prices = 0;
					foreach ($item['options'] as $option) {
						$option_prices += $option['price'];
					}
					
					$total += ($base_price + $option_prices) * $quantity;
					
				} elseif (!empty($item['promo_code'])) {
					// Promo code
					$total -= $item['value'];
				}
			}
		}
		if ($total<0) {
			$total = 0;
		}
		
		return $total;
	}
	
	function output(){
		$html = ob_get_contents();
		ob_end_clean();
		
		ob_start('ob_gzhandler');
		echo preg_replace('/\s\s+/', '', $html);
	}
	
	@ob_start('ob_gzhandler'); // GZip compress the script...

	
	/**
	 * Author: Hanson
	 * Desc: Draw Drop Down Box
	 * Usage: 
	 * hs_drop_down( "type", array(
			"icon-head" => "<i class='fa fa-map-marker'></i>",
			"icon-tail" => "<i class='fa fa-chevron-down'></i>",
			"text-align" => "center",
			"values" => array(
			'D' => DELIVERY,
			'T' => TAKEAWAY
			),
			"active" => "D"
		) );
	 */
	function hs_drop_down( $form_name, $options ) {
	    if ( empty( $options['text-align'] ) ) $options['text-align'] = "center";
	    if ( empty($options['values']) ) exit( "no data" );

	    $keys = array_keys( $options['values'] );
	    if ( empty($options['active']) ) {
	        $options['active'] = $keys[0];
	    }
	    else if ( in_array( $options['active'], $keys ) == false ) {
	    	$options['active'] = $keys[0];
	    }

	    echo '<div class="h-drop-down h-drop-down-' . $form_name . '">';
	    echo "<div class=\"h-drop-down-visible text-{$options['text-align']}\">";
	    if ( !empty( $options['icon-head'] ) ) echo "<span class=\"icon-head\">{$options['icon-head']}</span>";
	    if ( !empty( $options['icon-tail'] ) ) echo "<span class=\"icon-tail\">{$options['icon-tail']}</span>";
		
	    echo "<span class=\"label\">" . $options['values'][$options['active']] . "</span></div>";
	    
	    echo "<ul class=\"h-drop-down-list\" ";
	    if ( !empty( $options['callback'] ) ) {
	        echo "data-callback=\"{$options['callback']}\"";
	    }
	    echo ">";
	    
	    foreach( $options['values'] as $val=>$txt ) {
	        if ( $val == $options["active"] )
	        	echo "<li data-value=\"{$val}\" class=\"active\">{$txt}</li>";
	       	else
	       		echo "<li data-value=\"{$val}\">{$txt}</li>";
	    }

	    echo "</ul><input type=\"hidden\" name=\"{$form_name}\" value=\"{$options['active']}\" /></div>";
	}