<?php
	if (@$is_included && strlen(@$_GET['check'])==32) {
		$corporateAccounts = corporateAccount::listUnconfirmed();
		
		foreach ($corporateAccounts as $corporateAccount) {
			if ($_GET['check']==$corporateAccount->encodeID4Confirmation()) {
				// We've found the corporate account pending confirmation
				// Everything's OK, let's approve it
				$corporateAccount->confirm();
				
				$_SESSION['s_venezvite']['error'][] = CCA_SUCCESS;
				break;
			}
		}
		
		// It seems we couldn't find any corporate account matching the submitted key
	}
	
	// Redirect the visitor to the homepage
	header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
	die();
