<?php
	if (@$is_included) {
		header('Content-type: application/xml; charset="utf-8"', true);
	    echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php
	    $restaurants = restaurant::list_();
        $root = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/';
?>
    <url>
        <loc><?php echo $root; ?></loc>
        <changefreq>monthly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc><?php echo $root; ?>about-us.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url>
    <!--url>
        <loc><?php echo $root; ?>catering.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url>
    <url>
        <loc><?php echo $root; ?>contact.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url>
    <url>
        <loc><?php echo $root; ?>corporate-accounts.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url>
    <url>
        <loc><?php echo $root; ?>help-faq.html</loc>
        <changefreq>monthly</changefreq>
        <priority>0.4</priority>
    </url-->
    <url>
        <loc><?php echo $root; ?>how-it-works.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url>
    <url>
        <loc><?php echo $root; ?>how-we-help.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url>
    <!--url>
        <loc><?php echo $root; ?>join-team.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url-->
    <url>
        <loc><?php echo $root; ?>restaurant-join.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc><?php echo $root; ?>tou-customers.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.1</priority>
    </url>
    <url>
        <loc><?php echo $root; ?>tou-restaurants.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.1</priority>
    </url>
        <loc><?php echo $root; ?>privacy-policy.html</loc>
        <changefreq>yearly</changefreq>
        <priority>0.1</priority>
    </url>
<?php
        foreach($restaurants as $restaurant){
?>
    <url>
        <loc><?php echo $root, $restaurant->customURL; ?>.html</loc>
        <changefreq>weekly</changefreq>
        <priority>1</priority>
    </url>
<?php
        }
?>
</urlset>
<?php
	}
	
	die();
