<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		@is_numeric($_POST['id']) && ($menu_item_option = menuItemOption::getByID($_POST['id'])) && 
		$menu_item_option->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
		
		// The current restaurant admin is allowed to remove the submitted menu item option
		$menu_item_option->delete();
		
		echo json_encode(array(
				'success' => true
			));
		die();
	}
	
	echo json_encode(array());
	die();
