<?php
	if (@$is_included && !empty($restaurant)) {
		/* --- writing review --- */
		if ( @$_POST['writing-review'] == '1' ) {
			$restaurant->addReview(
					$entity->idUser,
					$_POST['rating'],
					sanitize( $_POST['isOnTime'] ),
					sanitize( $_POST['isCorrect'] ),
					sanitize( $_POST['isGood'] ),
					$_POST['reviewContent']
			);
		}
		/* --- end of writing review --- */

		/* --- writing review eligibility --- */
		$eligible_review = false;
		if ( !empty( $entity ) ) {
			$row = order::getUserLastOrder($entity->idUser, $restaurant->idRestaurant);
			if ( !empty($row) ) {
				$myreview = $restaurant->isUserReviewed( $entity->idUser );

				if ( empty($myreview) ) {
					$eligible_review = true;
				}
			}
		}
		/* --- end of eligibility --- */

		/* --- pagination --- */
		$pageOffset = 1;
		if ( isset( $_GET['rpgn'] ) ) {
			$pageOffset = intval($_GET['rpgn'] );
		}
		$pageOffset = max( 1, $pageOffset );
		$reviews = $restaurant->getReviewList($pageOffset);

		if ( @$_GET['ajax'] == '1' || @$_POST['writing-review'] == '1' ) {
			require_once REL . DS . 'views' . DS . 'inc' . DS . 'reviews.tpl.php';
			die();
		}
		/* --- end of pagination --- */

		/* --- favoriting --- */
		if ( @$_POST['favoriting'] == '1' ) {
			if ( !empty( $entity ) ) {
				$entity->addToFavorites($restaurant);
				die( "success" );
			}
			else {
				die( "Please login" );
			}
		}
		else if ( @$_POST['favoriting'] === '0' ) {
			if ( !empty( $entity ) ) {
				$entity->removeFavorites($restaurant);
				die( "success" );
			}
			else {
				die( "Please login" );
			}
		}
		/* --- end of favoriting --- */
		
		setcookie('c_venezvite', json_encode(array('restaurant' => $restaurant->customURL)), 0, '/');	// Storing the most recently visited restaurant, 
																										// to know what cart to load in the checkout page
		check4Search();
		
		if (@$_SESSION['s_venezvite']['search']['type']=='D' && !$restaurant->checkDeliveryLocation($_SESSION['s_venezvite']['search']['coords'])) {
			// Redirect the user to the restaurants list page, since the current restaurant
			// doesn't seem to deliver to the currently submitted address
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/' . RESTAURANTS_LIST_CUSTOM_PATH . '/restaurants-list.html#no-delivery');
			die();
		}

		$search_datetime = date('Y-m-d H:i');
		if (@$_SESSION['s_venezvite']['search']['date'] && @$_SESSION['s_venezvite']['search']['time']) {
			$search_datetime = $_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time'];
		}
		$current_date_hours = $restaurant->getDateHours(@$_SESSION['s_venezvite']['search']['type'], $search_datetime);

		if ($restaurant->opened) {
			if (empty($open_status)) {
				$open_status = OPEN;
			}
		} else {
			$open_status = CLOSED;
		}

		if (@$_SESSION['s_venezvite']['search']['coords']) { // @$_SESSION['s_venezvite']['search']['type']=='D'
			// Get the restaurant's current delivery zone
			$delivery_zone = $restaurant->checkDeliveryLocation($_SESSION['s_venezvite']['search']['coords']);
			$restaurant->current_delivery_zone = array(
				'id'				=> $delivery_zone->idDeliveryZone, 
				'delivery_fee'		=> $delivery_zone->deliveryFee, 
				'minimum_delivery'	=> $delivery_zone->minimumDelivery, 
				'estimated_time'	=> $delivery_zone->estimatedTime
			);
		}

		$menu_categories = array(0 => array(RECOMMENDED => array()));
		foreach ($restaurant->menus[0]->menuItems as $menu_item) {
			if ($menu_item->recommended=='Y') {
				$menu_categories[0][RECOMMENDED][] = $menu_item;
			}

			if (!array_key_exists($menu_item->menuCategory->idMenuCategory, $menu_categories)) {
				$menu_categories[$menu_item->menuCategory->idMenuCategory] = array($menu_item->menuCategory->category => array());
			}
			$menu_categories[$menu_item->menuCategory->idMenuCategory][$menu_item->menuCategory->category][] = $menu_item;
		}
		if (empty($menu_categories[0][RECOMMENDED])) {
			// No recommended menu items
			unset($menu_categories[0]);
		}

		if (!empty($_GET['sc'])) {
			$is_valid_prefill = true;
			
			$pre_menu_items = explode('|', $_GET['sc']);
			if (count($pre_menu_items)) {
				foreach ($pre_menu_items as $pre_menu_item) {
					$pre_menu_item = explode('*', $pre_menu_item);
					$pre_menu_item = explode(',', $pre_menu_item[0]);
					
					if (count($pre_menu_item)!=2 || !is_numeric($pre_menu_item[0]) || !is_numeric($pre_menu_item[1])) {
						$is_valid_prefill = false;
						break;
					}
				}
			} else {
				$is_valid_prefill = false;
			}

			if ($is_valid_prefill) {
				// Clear the shopping cart session in order to add the details of the currently submitted order request
				unset($_SESSION['s_venezvite']['carts'][$restaurant->customURL]);
			}
		}
		
		if ( !empty( $entity ) ) {
			$is_faved = $entity->isRestaurantFavorited( $restaurant );
		}
	}

	function sanitize( $val ) {
		if ( $val == 'Yes' )
			return 1;
		else if ( $val == 'No' )
			return 0;
		else
			return null;
	}