<?php
	if (@$is_included && strlen(@$_GET['check'])==40) {
		$corporateAccounts = corporateAccount::listUnapproved();
		
		foreach ($corporateAccounts as $corporateAccount) {
			if ($_GET['check']==$corporateAccount->encodeID4Approval()) {
				// We've found the corporate account pending approval
				// Now let's see if its email account has been confirmed
				if (empty($corporateAccount->dateValidated)) {
					// It hasn't been, so we cannot approve this account yet
					$_SESSION['s_venezvite']['error'][] = ACA_EMAIL_UNCONFIRMED;
					
				} else {
					// Everything's OK, let's approve it
					if ($corporateAccount->approve()) {
						$_SESSION['s_venezvite']['error'][] = ACA_SUCCESS;
					} else {
						$_SESSION['s_venezvite']['error'][] = ACA_FAILURE;
					}
				}
				
				break;
			}
		}
		
		// It seems we couldn't find any corporate account matching the submitted key
	}
	
	// Redirect to homepage
	header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
	die();
