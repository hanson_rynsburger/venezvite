<?php
	if (!@$is_included) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	} elseif (empty($restaurantAdmin)) {
		// Go to the login page
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
		die();
	}
	
	
	$startDate = date('Y-m-d', strtotime(date('Y-m') . '-01')); // We default to current month's orders
	$endDate = date('Y-m-d H:i:s');
	
	if (!empty($_POST['sales_period'])) {
		switch ($_POST['sales_period']) {
			case 'PM': // Past month
				$startDate = date('Y-m', strtotime('-1 month')) . '-01';
				$endDate = date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m') . '-01')));
				break;
			case 'CY': // Current year
				$startDate = date('Y') . '-01-01';
				break;
			case 'AT': // All time
				$startDate = '2012-01-01';
				break;
		}
	}
	
	$accepted = 0;
	$declined = 0;
	
	$orders = order::list_(null, $restaurantAdmin->restaurant, false, true, $startDate, $endDate);
	$total_orders = count($orders);
	foreach ($orders as $order) {
		if (!empty($order->dateProcessed)) {
			if ($order->rejectionReason) {
				$declined++;
			} else {
				$accepted++;
			}
		}
	}
	
	if (!isset($_GET['past'])) {
		$orders = order::list_(null, $restaurantAdmin->restaurant, true, false, $startDate, $endDate);
	}
	
	
	$total_sales = $restaurantAdmin->restaurant->getTotalSales($startDate, $endDate);
	$total_earnings = $restaurantAdmin->restaurant->getTotalEarnings($startDate, $endDate);
