<?php
	if (@$is_included) {
		
		if (!empty($user) || !empty($corporateAccount)) {
			// There's no reason for the authenticated users to access this page
			header('Location: ' . (!strstr(@$_SERVER['HTTP_REFERER'], 'forgot-password') ? $_SERVER['HTTP_REFERER'] : ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/'));
			die();
		}
		
		if (!empty($_POST['username']) && isEmail(checkQuotes($_POST['username']))) {
			// It seems the login form has been submitted (correctly)
			$user = user::getByEmail(checkQuotes($_POST['username']));
			
			
			if ($user) {
				$user->sendPasswordResetMessage();
				unset($user); // We don't want to log in the user...
			}
			
			$_SESSION['s_venezvite']['error'][] = FP_MESSAGE;
		}
	}
