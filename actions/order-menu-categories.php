<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		@is_array($_POST['menu_categories'])) {
		
		$order = 1;
		foreach ($_POST['menu_categories'] as $id_menu_category) {
			if (is_numeric($id_menu_category) && ($menu_category = menuCategory::getByID($id_menu_category)) && 
				$menu_category->restaurant->idRestaurant==$restaurantAdmin->restaurant->idRestaurant) {
				// The current restaurant admin is allowed to change the submitted menu category's order
				
				$menu_category->reorder($order);
				$order++;
			}
		}
	}
	
	echo json_encode(array());
	die();
