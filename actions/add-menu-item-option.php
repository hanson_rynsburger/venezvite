<?php
	if (!empty($is_included) && !empty($restaurantAdmin) && 
		isset($_POST['id']) && (empty($_POST['id']) || (is_numeric($_POST['id']) && ($menu_item_option = menuItemOption::getByID($_POST['id'])))) && 
		!empty($_POST['option_name']) && isset($_POST['price']) && (empty($_POST['price']) || is_numeric($_POST['price']))) {
		
		$update = false;
		$option_name = checkQuotes($_POST['option_name']);
		$price = (float)$_POST['price'];
		
		if (empty($menu_item_option)) {
			$menu_item_option = menuItemOption::insert($restaurantAdmin->restaurant, $option_name, $price);
			
		} else {
			$menu_item_option->update(array(
					'menuItemOption'	=> $option_name, 
					'price'				=> $price
				));
			
			$menu_item_option->menuItemOption = $option_name;
			$menu_item_option->price = $price;
			$update = true;
		}
		
		echo json_encode(array(
				'menu_option'	=> array(
					'id'	=> $menu_item_option->idMenuItemOption, 
					'text'	=> htmlspecialchars($menu_item_option->menuItemOption) . ' (' . number_format($menu_item_option->price, 2) . ' ' . $restaurantAdmin->restaurant->currency . ')'
				), 
				'success' 		=> true, 
				'update'		=> $update
			));
		die();		
	}
	
	echo json_encode(array());
	die();
