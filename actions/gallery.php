<?php
	if (!@$is_included) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	} elseif (empty($restaurantAdmin)) {
		// Go to the login page
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
		die();
	}

	if ( !empty( $_POST['res_banner_id'] ) ) {
		$ri = restaurantImage::getByID( $_POST['res_banner_id'] );
		if ( !empty($ri) ) $ri->delete();

		if (!empty($_FILES['res_banner']) && ($size = @getimagesize($_FILES['res_banner']['tmp_name']))) {
			$pathPath = pathinfo( $_FILES['res_banner']['name'] );

			do {
				$new_filename = uniqid( "res_" ) . "." . $pathPath['extension'];
			}
			while( is_file( REL . '/i/restaurants/' . $new_filename ) );

			if (move_uploaded_file($_FILES['res_banner']['tmp_name'], REL . '/i/restaurants/' . $new_filename)) {
				restaurantImage::insertSpecific($restaurantAdmin->restaurant, $new_filename, 1);
			}
		}
		exit(1);
	}
	else if ( !empty( $_POST['res_thumbnail_id'] ) ) {
		$ri = restaurantImage::getByID( $_POST['res_thumbnail_id'] );
		if ( !empty($ri) ) $ri->delete();

		if (!empty($_FILES['res_thumbnail']) && ($size = @getimagesize($_FILES['res_thumbnail']['tmp_name']))) {
			$pathPath = pathinfo( $_FILES['res_thumbnail']['name'] );

			do {
				$new_filename = uniqid( "res_" ) . "." . $pathPath['extension'];
			}
			while( is_file( REL . '/i/restaurants/' . $new_filename ) );

			if (move_uploaded_file($_FILES['res_thumbnail']['tmp_name'], REL . '/i/restaurants/' . $new_filename)) {
				restaurantImage::insertSpecific($restaurantAdmin->restaurant, $new_filename, 2);
			}
		}
	}
	else if ( $_POST['res_gallery_id'] === "0" ) {
		if (!empty($_FILES['res_gallery']) && ($size = @getimagesize($_FILES['res_gallery']['tmp_name']))) {
			$pathPath = pathinfo( $_FILES['res_gallery']['name'] );

			do {
				$new_filename = uniqid( "res_" ) . "." . $pathPath['extension'];
			}
			while( is_file( REL . '/i/restaurants/' . $new_filename ) );

			if (move_uploaded_file($_FILES['res_gallery']['tmp_name'], REL . '/i/restaurants/' . $new_filename)) {
				restaurantImage::insert($restaurantAdmin->restaurant, $new_filename);
			}
		}
	}

	if ( $_POST['delete'] == "1" ) {
		restaurantImage::getByID( $_POST['id'] )->delete();
		die( "success" );
	}

	$resImages = restaurantImage::list_( $restaurantAdmin->restaurant );
	$banner = $thumbnail = null;
	$gallery = array();

	foreach ( $resImages as $ri ) {
		if ($ri->order == 1 ) {
			$banner = $ri;
		}
		else if ( $ri->order == 2 ) {
			$thumbnail = $ri;
		}
		else {
			array_push( $gallery, $ri );
		}
	}
