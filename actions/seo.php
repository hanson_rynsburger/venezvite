<?php
	if (!empty($restaurant)) {
		// Set the page's custom title as the restaurant's name
		if (!defined('SEO_TITLE')) {
			$title = ($restaurant->delivery=='Y' ? DELIVERY : TAKEAWAY) . ' ' . 
				$restaurant->cuisineTypes[0]->cuisineType . ' ' . 
				$restaurant->city . ' - ' . 
				$restaurant->restaurantName . ' - Venezvite';
			define('SEO_TITLE', $title);
		}
		// ... and its description, the restaurant's description!
		if (!defined('SEO_DESCRIPTION')) {
			define('SEO_DESCRIPTION', $restaurant->restaurantDescription);
		}
	}
	
	
	
	$seoTitle = DEFAULT_SEO_TITLE;
	if (defined('SEO_TITLE') && SEO_TITLE!='') {
		$seoTitle = SEO_TITLE;
	}
	
	$seoKeywords = DEFAULT_SEO_KEYWORDS;
	if (defined('SEO_KEYWORDS') && SEO_KEYWORDS!='') {
		$seoKeywords = SEO_KEYWORDS;
	}
	
	$seoDescription = DEFAULT_SEO_DESCRIPTION;
	if (defined('SEO_DESCRIPTION') && SEO_DESCRIPTION!='') {
		$seoDescription = SEO_DESCRIPTION;
	}
