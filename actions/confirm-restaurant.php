<?php
    if (!empty($is_included) && !empty($_GET['check']) && strlen($_GET['check'])==32) {
        $restaurants = restaurant::listUnconfirmed();
        foreach ($restaurants as $restaurant) {
            if ($_GET['check']==$restaurant->encodeID4Confirmation()) {
                // We've found the restaurant pending confirmation
                // Everything's OK, let's approve it
                $restaurant->confirm();
                
                $_SESSION['s_venezvite']['error'] = array(CR_SUCCESS);
                break;
            }
        }
        
        // It seems we couldn't find any restaurant matching the submitted key
    }
    
    // Redirect the visitor to the homepage
    header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
    die();
