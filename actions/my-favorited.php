<?php
	if (!@$is_included || empty($entity)) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	}

	$restaurants = $entity->listFavorites();