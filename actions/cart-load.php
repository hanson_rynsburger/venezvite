<?php
	if (@$is_included && !empty($_POST['restaurant']) && ($restaurant = restaurant::getByURL($_POST['restaurant']))){
		
		// Everything seems to be fine, let's check for the current restaurant's cart session
		if (empty($_SESSION['s_venezvite']['carts']) || empty($_SESSION['s_venezvite']['carts'][$restaurant->customURL])) {
			// No menu items selected (for the current restaurant)
			echo json_encode(array());
			die();
		} else {
			echo json_encode(array(
					'items' => $_SESSION['s_venezvite']['carts'][$restaurant->customURL]
				));
			die();
		}
	}
	
	echo json_encode(array());
	die();
