<?php
	if (@$is_included && !empty($_POST['data'])) {
		parse_str($_POST['data'], $data);
		
		if (count($data)>=3 && is_numeric(@$data['menu_item_id']) && ($menu_item = menuItem::getByID($data['menu_item_id'])) && is_numeric(@$data['quantity']) && $data['quantity']>=1 && isset($data['instructions'])) {
			//error_log(print_r($menu_item, 1));
			
			$options = array();
			$options_price = 0;
			if (is_array(@$data['menu_item_options'])) {
				foreach ($data['menu_item_options'] as $menu_item_option_id) {
					if (!is_numeric($menu_item_option_id) || !($menu_item_option = menuItemOption::getByID($menu_item_option_id)) || $menu_item->menu->restaurant->idRestaurant!=$menu_item_option->restaurant->idRestaurant) {
						die();
					} else {
						//error_log(print_r($menu_item_option, 1));
						$options[] = array(
							'id'	=> $menu_item_option->idMenuItemOption, 
							'name'	=> $menu_item_option->menuItemOption, 
							'price'	=> $menu_item_option->price
						);
						$options_price += $menu_item_option->price;
					}
				}
			}
			$restaurant_index = $menu_item->menu->restaurant->customURL; // Shortcut...
			
			// Everything seems to be fine so far, let's save the currently ordered menu item.
			if (empty($_SESSION['s_venezvite']['carts'])) {
				$_SESSION['s_venezvite']['carts'] = array();
			}
			if (empty($_SESSION['s_venezvite']['carts'][$restaurant_index])) {
				// One array for each restaurant, to allow users to select food from multiple restaurants.
				// When pressing "Checkout" on a certain restaurant's page, we'll only consider that restaurant's selected menu items.
				$_SESSION['s_venezvite']['carts'][$restaurant_index] = array();
			}
			
			
			if (is_numeric(@$data['edit']) && !empty($_SESSION['s_venezvite']['carts'][$restaurant_index][$data['edit']]) && $_SESSION['s_venezvite']['carts'][$restaurant_index][$data['edit']]['menu_item']==$menu_item->idMenuItem) {
				// This is an edit, so let's bypass the add functionality
				$_SESSION['s_venezvite']['carts'][$restaurant_index][$data['edit']] = array(
					'menu_item'		=> $menu_item->idMenuItem, 
					'name'			=> $menu_item->menuItemName, 
					//'description'	=> $menu_item->menuItemDescription, 
					'unit_price'	=> $menu_item->getPrice(), 
					'quantity'		=> $data['quantity'], 
					'options'		=> $options, 
					'instructions'	=> checkQuotes($data['instructions'])
				);
				
			} else {
				$existing = false;
				for ($i=0; $i<count($_SESSION['s_venezvite']['carts'][$restaurant_index]); $i++){
					if (!empty($_SESSION['s_venezvite']['carts'][$restaurant_index][$i]['menu_item']) && $_SESSION['s_venezvite']['carts'][$restaurant_index][$i]['menu_item']==$menu_item->idMenuItem) {
						// The current menu item has already been chosen by the current user
						// Let's see if its options are the same with the just-submitted one's
						if (isset($_SESSION['s_venezvite']['carts'][$restaurant_index][$i]['options']) && count($_SESSION['s_venezvite']['carts'][$restaurant_index][$i]['options'])==count($options)) {
							// Checking further...
							$identicalOptions = 0;
							
							foreach ($_SESSION['s_venezvite']['carts'][$restaurant_index][$i]['options'] as $existingMenuOptions) {
								foreach ($options as $newMenuOptions) {
									if ($existingMenuOptions['id']==$newMenuOptions['id']) {
										$identicalOptions++;
										break;
									}
								}
							}
							
							if (count($_SESSION['s_venezvite']['carts'][$restaurant_index][$i]['options'])==$identicalOptions) {
								// The submitted menu item IS identical to an existing one, so let's just increment its quantity.
								$_SESSION['s_venezvite']['carts'][$restaurant_index][$i]['quantity'] += (int)$data['quantity'];
								
								// Let's replace the existing instructions with the current ones (if any).
								if (!empty($data['instructions'])) {
									$_SESSION['s_venezvite']['carts'][$restaurant_index][$i]['instructions'] = checkQuotes($data['instructions']);
								}
								
								$existing = true;
								break;
							}
						}
					}
				}
				
				if (!$existing) {
					// This is the first time the current menu item is chosen
					$_SESSION['s_venezvite']['carts'][$restaurant_index][] = array(
						'menu_item'		=> $menu_item->idMenuItem, 
						'name'			=> $menu_item->menuItemName, 
						//'description'	=> $menu_item->menuItemDescription, 
						'unit_price'	=> $menu_item->getPrice(), 
						'quantity'		=> $data['quantity'], 
						'options'		=> $options, 
						'instructions'	=> checkQuotes($data['instructions'])
					);
				}
			}
			
			
			if (!empty($_SESSION['s_venezvite']['search']) && is_array($_SESSION['s_venezvite']['search']) && count($_SESSION['s_venezvite']['search'])==17) {
				// All good, success
				echo json_encode(array(
						'success' => true
					));
			} else {
				// Ask for order destination and time
				echo json_encode(array(
						'no_address' => true
					));
			}
			die();
		}
	}
	
	echo json_encode(array());
	die();
