<?php
	if (@$is_included) {
		function draw_time_interval($name, $day, $index, $selected) {
			echo '<div class="left">';
			echo '<select class="', $name, ' rounded-select" name="', $name, '[', $day, '][', $index, ']">';
			echo '<option value="">', SELECT_TIME, '</option>';
			
			for ($j=0; $j<96; $j++) {
				$hour = floor($j / 4);
				$minute = ($j / 4 - $hour) * (15 * 4);
				
				if ($hour<10) {
					$hour = '0' . $hour;
				}
				if ($minute<10) {
					$minute = '0' . $minute;
				}
				$time = $hour . ':' . $minute;
				
				echo '<option', ($selected && $selected==$time ? ' selected="selected"' : ''), ' value="', $time, '">', $time, '</option>';
			}
			
			echo '</select>';
			echo '</div>';
		}
		
		
		if (!empty($_POST)/* && !empty($_FILES)*/) {
			if (!empty($_POST['last_name']) && !empty($_POST['first_name']) && !empty($_POST['mobile']) && /*!empty($_POST['email']) && */
				!empty($_POST['restaurant']) && 
				is_numeric(@$_POST['country']) && ($country = country::getByID($_POST['country'])) && 
				!empty($_POST['address']) && !empty($_POST['city']) && is_numeric(@$_POST['latitude']) && is_numeric(@$_POST['longitude']) && 
				!empty($_POST['zip']) && !empty($_POST['phone']) && /*!empty($_POST['website']) && */isset($_POST['description']) && 
				is_array($_POST['cuisines']) && is_numeric(@$_POST['cuisines'][0]) && ($cuisine_type = cuisineType::getByID($_POST['cuisines'][0])) && 
				is_array(@$_POST['hour_start']) && count($_POST['hour_start'])==7 && is_array(@$_POST['hour_end']) && count($_POST['hour_end'])==7 && 
				(@$_POST['delivery']=='Y' && empty($_POST['custom_delivery']) ? 
					is_array(@$_POST['zone_range']) && (empty($_POST['zone_range'][0]) || is_numeric($_POST['zone_range'][0])) && 
					is_array(@$_POST['zone_minimum']) && (empty($_POST['zone_minimum'][0]) || is_numeric($_POST['zone_minimum'][0])) && 
					is_array(@$_POST['zone_fee']) && (empty($_POST['zone_fee'][0]) || is_numeric($_POST['zone_fee'][0])) && 
					is_array(@$_POST['zone_estimated_time']) && (empty($_POST['zone_estimated_time'][0]) || is_numeric($_POST['zone_estimated_time'][0]))
					: 
					empty($_POST['zone_range']) && empty($_POST['zone_minimum']) && empty($_POST['zone_fee']) && empty($_POST['zone_estimated_time'])) && 
				/*isset($_FILES['photos']) && !empty($_FILES['menu']) && */
				!empty($_POST['username']) && $_POST['username']==@$_POST['confirm_username'] && 
				!($restaurant_admin = restaurantAdmin::getByEmail($_POST['username'])) && 
				strlen(@$_POST['password'])>=5 && $_POST['password']==@$_POST['confirm_password'] && 
				@$_POST['terms']=='Y'
			) {
				$error = false;
				/*
				// Validating the photos as images
				$invalid_image = false;
				if (!empty($_FILES['photos']['tmp_name'][0])) {
					foreach ($_FILES['photos']['tmp_name'] as $index => $photo) {
						if (!($image_size = getimagesize($photo))) {
							unset($_FILES['photos']['name'][$index]);
							unset($_FILES['photos']['type'][$index]);
							unset($_FILES['photos']['error'][$index]);
							unset($_FILES['photos']['size'][$index]);
							
							@unlink($_FILES['photos']['tmp_name'][$index]);
							unset($_FILES['photos']['tmp_name'][$index]);
							$invalid_image = true;
						}
					}
					if ($invalid_image) { // !count($_FILES['photos']['tmp_name']) || 
						$_SESSION['s_venezvite']['restaurant_registration_message'] = INVALID_FILE;
						
						//if (!count($_FILES['photos']['tmp_name'])) {
						//	$error = true;
						//}
					}
				}
				*/
				if (!$error) {
					// Get the list of valid cuisine types submitted
					$cuisine_types = array($cuisine_type);
					if (count($_POST['cuisines'])>1) {
						for ($i=1; $i<count($_POST['cuisines']); $i++) {
							if ($cuisine_type = cuisineType::getByID($_POST['cuisines'][$i])) {
								$cuisine_types[] = $cuisine_type;
							}
						}
					}
					
					// Get the list of valid delivery zones submitted
					$delivery_zones = array();
					if (@$_POST['delivery']=='Y') {
						if (@$_POST['custom_delivery']!='Y') {
							foreach ($_POST['zone_range'] as $index => $range) {
								if (!empty($range) && isset($_POST['zone_minimum'][$index]) && isset($_POST['zone_fee'][$index]) && isset($_POST['zone_estimated_time'][$index])) {
									// Insert a new delivery zone per current restaurant
									$delivery_zones[] = array(
										$_POST['zone_fee'][$index], 
										$range, 
										$_POST['zone_estimated_time'][$index], 
										$_POST['zone_minimum'][$index]
									);
								}
							}
						} else {
							// VP-specific delivery zones
							$delivery_zones[] = array(6, 1.1, 15, 30);
							$delivery_zones[] = array(7, 2.2, 20, 30);
							$delivery_zones[] = array(9, 3, 25, 30);
							$delivery_zones[] = array(10, 4, 30, 30);
							//$delivery_zones[] = array(12, 6, 60, 45);
							//$delivery_zones[] = array(12, 7, 100, 45);
						}
					}
					
					// Get the list of valid timetable sets submitted
					$hours_of_operation = array();
					foreach ($_POST['hour_start'] as $dow => $hour_start) {
						if (!isset($hours_of_operation[$dow])) {
							$hours_of_operation[$dow] = array();
						}
						
						foreach ($hour_start as $index => $start) {
							if (!empty($start) && !empty($_POST['hour_end'][$dow][$index]) && 
								(int)str_replace(':', '', $start) < (int)str_replace(':', '', $_POST['hour_end'][$dow][$index])) {
								$hours_of_operation[$dow][] = array(
									'start'	=> $start, 
									'end'	=> $_POST['hour_end'][$dow][$index]
								);
							}
						}
					}
					
					
					// Insert the actual restaurant record
					$restaurant = restaurant::insert(
						checkQuotes(trim($_POST['restaurant'])), 
						checkQuotes($_POST['first_name']), 
						checkQuotes($_POST['last_name']), 
						checkQuotes($_POST['address']), 
						checkQuotes($_POST['city']), 
						checkQuotes($_POST['zip']), 
						$_POST['latitude'], 
						$_POST['longitude'], 
						checkQuotes(@$_POST['website']), 
						checkQuotes($_POST['phone']), 
						checkQuotes($_POST['mobile']), 
						@$_POST['delivery'], 
						@$_POST['catering'], 
						'Y', 
						checkQuotes($_POST['description']), 
						$_SESSION['s_venezvite']['language']->idLanguage, 
						$_SERVER['REMOTE_ADDR'], 
						$_POST['username'], 
						checkQuotes($_POST['password']), 
						$cuisine_types, 
						$delivery_zones, 
						$hours_of_operation, 
						$_POST['username'], // $_POST['email']
						@$_POST['custom_delivery'], 
						@$_POST['takeaway'], 
						$country
					);
					
					if ($restaurant) {
						/*
						// Let's check for the uploaded files, now re-ordered
						if (!empty($_FILES['photos']['tmp_name'][0])) {
							foreach ($_FILES['photos']['tmp_name'] as $index => $photo) {
								// This must be an image
								$file = filefy($_FILES['photos']['name'][$index]);
								if (is_file(REL . '/i/restaurants/' . $file)) {
									$file = getUniqueFile($file, REL . '/i/restaurants/');
								}
								
								if (move_uploaded_file($photo, REL . '/i/restaurants/' . $file)) {
									require_once REL . '/classes/phpThumb/ThumbLib.inc.php';
									$thumb = PhpThumbFactory::create(REL . '/i/restaurants/' . $file);
									
									$size = getimagesize(REL . '/i/restaurants/' . $file);
									if ($size[0]>605 || $size[1]>340) {
										$thumb->adaptiveResize(605, 340)->save(REL . '/i/restaurants/' . $file);
									}
									// Create the small thumb
									$thumb->adaptiveResize(136, 100)->save(REL . '/i/restaurants/tn_' . $file);
									
									// Now attach the currently handled image to the just-registered restaurant
									restaurantImage::insert($restaurant, $file);
								}
								
								@unlink($photo);
							}
						}
						
						if (!empty($_FILES['menu']['tmp_name'])) {
							// Let's see if the uploaded menu file is valid / allowed
							if (in_array($_FILES['menu']['type'], array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword', 'application/pdf', 'image/jpeg'))) {
								// We need some security check here, so there won't be any PHP / harmfull scripts uploaded on the server
								$file = filefy($_FILES['menu']['name']);
								if (is_file(REL . '/i/restaurants/' . $file)) {
									$file = getUniqueFile($file, REL . '/i/restaurants/');
								}
								
								if (move_uploaded_file($_FILES['menu']['tmp_name'], REL . '/i/restaurants/' . $file)) {
									// Now attach the currently handled menu file to the just-registered restaurant
									$restaurant->updateMenuFile($file);
								} else {
									$_SESSION['s_venezvite']['restaurant_registration_message'] = INVALID_FILE;
								}
								
								@unlink($_FILES['menu']['tmp_name']);
								
							} else {
								$_SESSION['s_venezvite']['restaurant_registration_message'] = INVALID_FILE;
							}
						}
						*/
					} else {
						$_SESSION['s_venezvite']['restaurant_registration_message'] = CANT_SAVE_RESTAURANT;
						$error = true;
					}
				}
				
				if (!$error) {
					unset($_POST);
					unset($_FILES);
					
					if (empty($_SESSION['s_venezvite']['restaurant_registration_message'])) {
						$_SESSION['s_venezvite']['restaurant_registration_message'] = REGISTRATION_CONFIRMATION;
					}
					
					//header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/restaurant-join.html');
					//die();
				}
				/*
			} else {
				error_log('incorrect post fields');
				error_log(print_r($_POST, 1));
				error_log(print_r($_FILES, 1));*/
			}
		}
		
		
		$countries = country::list_();
		$cuisine_types = cuisineType::list_();
	}
