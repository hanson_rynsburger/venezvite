<?php
	if (!@$is_included || !empty($entity)) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	} elseif (!empty($restaurantAdmin)) {
		// Get in My Account
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/restaurant-orders.html');
		die();
	}
	
	
	if (!empty($_POST['username']) && !empty($_POST['password'])){
		// It seems the login form has been submitted (correctly)
		restaurantAdmin::authenticate(checkQuotes($_POST['username']), checkQuotes($_POST['password']));
		
		if (restaurantAdmin::isLogged()) {
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/restaurant-orders.html');
		} else {
			// So the same combination won't be submitted again and again, by hitting Refresh
			header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
		}
		
		die();
	}
