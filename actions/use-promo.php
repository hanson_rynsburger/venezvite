<?php
	if (@$is_included && !empty($_POST['restaurant']) && ($restaurant = restaurant::getByURL($_POST['restaurant'])) && !empty($_POST['code'])) {
		
		if (!empty($user)) {
			$email_invitations = emailInvitation::list_($user);
			
			foreach ($email_invitations as $email_invitation) {
				if ($email_invitation->promoCode==$_POST['code']) {
					
			 		if (!empty($email_invitation->datePromoUsed)) {
						echo json_encode(array(
								'message' => CODE_ALREADY_USED
							));
						
					} else {
						$_SESSION['s_venezvite']['carts'][$_POST['restaurant']][] = array(
							'invitation'	=> $email_invitation->idInvitation, 
							'title'			=> DA_PROMO_CODE, 
							'code'			=> $email_invitation->promoCode, 
							'value'			=> $email_invitation->value
						);
						
						echo json_encode(array(
								'success' => true
							));
					}
					
					die();
				}
			}
		}
		
		
		// Next, let's try to see if the currently submitted code is not a custom promo code
		$promo_code = promoCode::getByCode($_POST['code'], @$user);
		if ($promo_code) {
			// It exists, so let's check if its inner conditions are met
			// First, if it has any start / end dates outside the current one
			if (!empty($promo_code->dateStart) && strtotime($promo_code->dateStart) > time()) {
				echo json_encode(array(
						'message' => str_replace('{$date}', date('F jS, Y', strtotime($promo_code->dateStart)), PROMO_VALID_FROM)
					));
				die();
			}
			
			if (!empty($promo_code->dateEnd) && strtotime($promo_code->dateEnd) < time()) {
				echo json_encode(array(
						'message' => str_replace('{$date}', date('F jS, Y', strtotime($promo_code->dateEnd)), PROMO_VALID_UNTIL)
					));
				die();
			}
			
			// Next, let's check if the current promo code is only meant for an user (which is NOT the current one)
			if (!empty($promo_code->user) && (!empty($user) ? $promo_code->user->idUser!=$user->idUser : true)) {
				// It is, so we'll return the invalid promo code message
				echo json_encode(array(
						'message' => INVALID_PROMO_CODE
					));
				die();
			}
			
			// Next, let's check if the current promo code can only be used a certain number of times, which it already has been
			if (!empty($promo_code->usageNo)) {
				if (empty($user)) {
					// An unlogged user cannot use a promo code that has a limited number of uses
					echo json_encode(array(
							'message' => LOGIN_FOR_PROMO
						));
					die();
					
				} elseif (!empty($promo_code->usages) && $promo_code->usageNo<=$promo_code->usages) {
					echo json_encode(array(
							'message' => PROMO_USED
						));
					die();
				}
			}
			
			// Finally, we're checking if the current order's total value is higher than the potentially set promo code minimum order value
			$total_amount = getCartTotal($restaurant);
			if ($promo_code->minOrderValue && $total_amount<$promo_code->minOrderValue) {
				echo json_encode(array(
						'message' => str_replace('{$value}', $promo_code->minOrderValue, PROMO_MIN_VALUE)
					));
				die();
			}
			
			// Everything seem to be fine, so let's add it into the shopping cart
			$_SESSION['s_venezvite']['carts'][$_POST['restaurant']][] = array(
				'promo_code'	=> $promo_code->idPromoCode, 
				'title'			=> DA_PROMO_CODE, 
				'code'			=> $promo_code->promoCode, 
				'value'			=> $promo_code->value
			);
			
			echo json_encode(array(
					'success' => true
				));
			die();
		}
	}
	
	echo json_encode(array(
			'message' => INVALID_OR_REGISTERED_PROMO
		));
	die();
