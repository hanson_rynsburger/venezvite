<?php
	// We need to initiate a few classes here, otherwise the login method below 
	// won't be able to find them (because of FB's namespace)
	$restaurant = new restaurant;
	$emailInvitation = new emailInvitation;
	
	
	require REL . VERSION_REL_PATH . 'classes' . DS . 'facebook-sdk' . DS . 'autoload.php';
	use Facebook\FacebookSession;
	use Facebook\FacebookRequest;
	use Facebook\GraphUser;
	use Facebook\FacebookRequestException;
	
	
	if (@$is_included && 
		(!empty($_POST['token']) || 
			(isset($_POST['gender']) && !empty($_POST['first_name']) && !empty($_POST['last_name']) && !empty($_POST['username']) && !empty($_POST['password']) && 
			isset($_POST['referer']) && isset($_POST['phone']) && !empty($_POST['newsletter']) && in_array($_POST['newsletter'], array('Y', 'N'))))
	) {
		// Language handling
		//require_once REL . '/lang/' . $_SESSION['s_venezvite']['language']->languageAcronym . '/checkout.php';
		
		if (!empty($_POST['token'])) {
			FacebookSession::setDefaultApplication(FB_APP_ID, FB_APP_SECRET);
			$session = new FacebookSession($_POST['token']);
			
			try {
				if (!$session->validate()) {
		            $session = null;
		        }
			} catch(Exception $ex) {
				$session = null;
			}
			
			if (!empty($session)) {
				// Proceed knowing you have a logged in user who's authenticated.
				$FacebookRequest = new FacebookRequest(
					$session, 'GET', '/me'
				);
				$request = $FacebookRequest->execute();
				$me = $request->getGraphObject(GraphUser::className());
				
			} else {
				echo json_encode(array(
						'error' => UNABLE_TO_REGISTER
					));
				die();
			}
			
			// Now that we know the current FB user is valid, let's see if it already has a Venezvite account or not
			$user = user::getByFB($me->getId());
			if ($user) {
				// The FB user is already a valid Venezvite user, so let's log him/her in the website
				$user->login();
				echo json_encode(array(
						'success' => true
					));
				die();
				
			} else {
				// The current FB user doesn't seem to be attached to any of the Venezvite users, so let's check if his/her email address is already in the system
				$user = user::getByEmail($me->getProperty('email'));
				if ($user) {
					// It is, so let's just update the existing user record cu the current FB user's ID
					$user->updateFbID($me->getId());
					
				} else {
					// This seems to be a brand new user, so let's create the propper record
					$_POST['gender'] = str_replace('male', 'M', str_replace('female', 'F', $me->getProperty('gender')));
					$_POST['first_name'] = $me->getFirstName();
					$_POST['last_name'] = $me->getLastName();
					$_POST['username'] = $me->getProperty('email');
					$_POST['password'] = user::generateRandomPassword();
					
					// First, let's check if the current FB user's email address is not already used in a corporate account
					if (corporateAccount::checkDuplicateUsername($_POST['username'])) {
						echo json_encode(array(
								'error' => C_EMAIL_IN_USE
							));
						die();
					}
					
					// Carry on...
				}
			}
			
		} else {
			if (!isEmail(checkQuotes($_POST['username']))) {
				echo json_encode(array(
						'error' => R_INVALID_USERNAME
					));
				die();
			} elseif (user::checkDuplicateEmail($_POST['username'])) {
				echo json_encode(array(
						'error' => C_EMAIL_IN_USE
					));
				die();
			} elseif (corporateAccount::checkDuplicateUsername($_POST['username'])) {
				echo json_encode(array(
						'error' => C_EMAIL_IN_USE
					));
				die();
			}
			
			if (passwordCheck(checkQuotes($_POST['password']))<50) {
				echo json_encode(array(
						'error' => VALIDATION_PASS_STRENGTH
					));
				die();
			}
		}
		
		$user = user::insert(
			(!empty($me) ? $me->getId() : null), 
			(!empty($_POST['gender']) ? $_POST['gender'] : null), 
			checkQuotes($_POST['first_name']), 
			checkQuotes($_POST['last_name']), 
			$_POST['username'], 
			checkQuotes($_POST['password']), 
			checkQuotes($_POST['referer']), 
			checkQuotes($_POST['phone']), 
			$_POST['newsletter'], 
			(!empty($me) ? 'https://graph.facebook.com/' . $me->getId() . '/picture?type=square' : null), 
			$_SESSION['s_venezvite']['language']->idLanguage, 
			$_SERVER['REMOTE_ADDR']
		);
		
		if ($user) {
			$user->login();
			echo json_encode(array(
					'success' => true
				));
			die();
			
		} else {
			echo json_encode(array(
					'error' => UNABLE_TO_REGISTER
				));
			die();
		}
		
	} else {
		error_log(print_r($_REQUEST, true));
	}
	
	echo json_encode(array(
			'error' => 'Unknown error'
		));
	die();
