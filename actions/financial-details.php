<?php
	if (!@$is_included) {
		// Nothing to do here
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/');
		die();
	} elseif (empty($restaurantAdmin)) {
		// Go to the login page
		header('Location: ' . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html');
		die();
	}
	
	
	if (!empty($_POST['company']) && !empty($_POST['iban']) && !empty($_POST['bank']) && !empty($_POST['swift'])) {
		$output = array(
			'success'	=> true
		);
		
		$restaurantAdmin->restaurant->update(array(
				'bankAccountName'	=> $_POST['company'], 
				'iban'				=> $_POST['iban'], 
				'bankName'			=> $_POST['bank'], 
				'swiftCode'			=> $_POST['swift']
			));
		$restaurantAdmin->serialize();
		
		$output['fields'] = array(
			'entity-company'	=> htmlspecialchars($_POST['company']), 
			'entity-iban'		=> htmlspecialchars($_POST['iban']), 
			'entity-bank'		=> htmlspecialchars($_POST['bank']), 
			'entity-swift'		=> htmlspecialchars($_POST['swift'])
		);
		
		echo json_encode($output);
		die();
	}
