<?php
	// Database connection info
	define('DB_NAME', 'venezvitecom');
	define('DB_USER', 'root');
	define('DB_PASS', 'password');
	
	// Set up the email account used to send and receive messages regarding the website
	define('IS_SMTP', false);
	define('EMAIL_HOST', '');
	define('EMAIL_PORT', 25);
	define('EMAIL_USERNAME', '');
	define('EMAIL_PASSWORD', '');
	define('EMAIL_ADDRESS', '');
	define('EMAIL_NAME', 'Venezvite.com');
	
	define('ORDERS_EMAIL_ADDRESS', '');
	
	// Default project paths
	define('ROOT', '//localhost/venezvite.com/'); // The project's URL root address (78.96.4.105)
	define('IMG', ROOT . 'i/'); // The project's images files URL (could be stored on a different sub-domain, for faster access)
	define('CSS_PATH', 'css/'); // The project's stylesheet files directory name
	define('JS_PATH', 'js/'); // The project's javascript files directory name
	define('DS', DIRECTORY_SEPARATOR); // Shorter version of the DIRECTORY_SEPARATOR predefined constant
	define('VERSION_REL_PATH', DS);
	
	define('ADMIN_PATH', ''); // Backoffice's folder name
	define('ADMIN_MASTER_USER', '');
	define('ADMIN_MASTER_PASS', '');
	define('ADMIN_USER', '');
	define('ADMIN_PASS', '');
	define('ADMIN_EXEC_USER', '');
	define('ADMIN_EXEC_PASS', '');
	
	
	
	define('DEBUG', true); // For the development and test environments
	
	define('COOKIES_EXPIRATION', 365); // Number of days until the cookies expire
	define('ORDER_COOKING_TIME', 30); // In minutes, multiple of 15
	define('DELIVERY_TIME', 30); // In minutes, multiple of 15
	define('MAX_CASH_VALUE', 500); // Maximum value of a cash order, in CHF
	
	// Facebook details
	define('FB_APP_ID', '');
	define('FB_APP_SECRET', '');
	define('FB_ADMINS', '');
	
	// Google APIs Key
	define('GOOGLE_KEY', '');
	
	// Payeezy payment gateway settings
	define('PAYEEZY_URL', 'https://api.demo.globalgatewaye4.firstdata.com/transaction/v19/wsdl'); // https://api.globalgatewaye4.firstdata.com/transaction/v19/wsdl | API endpoint
	define('PAYEEZY_CURRENCIES', json_encode(array(
			'CHF'	=> array(
				'HMAC_KEY'	=> '', 
				'KEY_ID'	=> '', 
				'EXACT_ID'	=> '', 
				'PASSWORD'	=> ''
			), 
			'USD'	=> array(
				'HMAC_KEY'	=> '',  
				'KEY_ID'	=> '', 
				'EXACT_ID'	=> '', 
				'PASSWORD'	=> '', 
			), 
			'EUR'	=> array(
				'HMAC_KEY'	=> '', 
				'KEY_ID'	=> '', 
				'EXACT_ID'	=> '', 
				'PASSWORD'	=> ''
			)
		)));
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Define the absolute server path (for includes and such)
	define('REL', realpath(dirname(__FILE__))); // Absolute path to the server's root
	require_once REL . DS . 'actions' . DS . 'common.php';
