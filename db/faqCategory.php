<?php
    function listFaqCategories($idLanguage, $listQuestions){
        global $db;
        
        $query = 'SELECT DISTINCT FC.`idCategory`, FC.`category`
            FROM `faq-categories` FC';
        
        if($listQuestions){
            $query .= '
            INNER JOIN `faq-questions` FQ ON FQ.`idCategory` = FC.`idCategory`';
        }
        
        $query .= '
            WHERE (FC.`idLanguage` = \'' . mysql_real_escape_string($idLanguage) . '\')
            ORDER BY FC.`order`;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $faqCategories = array();
        while($row = mysql_fetch_assoc($result)){
            $faqCategory = new faqCategory;
            $faqCategory->idCategory = $row['idCategory'];
            $faqCategory->category = $row['category'];
            
            if($listQuestions){
                $faqCategory->questions = faqQuestion::list_($faqCategory);
            }
            
            $faqCategories[] = $faqCategory;
        }
        
        return $faqCategories;
    }
?>