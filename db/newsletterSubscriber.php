<?php
    function newsletterSubscriberSearch($email){
        global $db;
        
        $query = 'SELECT `idNewsletterSubscriber`, `email`, `ip`, `dateAdded`
            FROM `newsletter-subscribers`
            WHERE (`email` = \'' . mysql_real_escape_string($email) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        if(mysql_num_rows($result)>0){
            $row = mysql_fetch_assoc($result);
            
            $newsletterSubscriber = new newsletterSubscriber;
            $newsletterSubscriber->idNewsletterSubscriber = $row['idNewsletterSubscriber'];
            $newsletterSubscriber->email = $row['email'];
            $newsletterSubscriber->ip = $row['ip'];
            $newsletterSubscriber->dateAdded = $row['dateAdded'];
            
            return $newsletterSubscriber;
        }else{
            return false;
        }
    }
    
    function newsletterSubscriberInsert($email, $ip){
        global $db;
        
        $query = 'INSERT INTO `newsletter-subscribers`
            SET `email` = \'' . mysql_real_escape_string($email) . '\', 
            `ip` = \'' . mysql_real_escape_string($ip) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $newsletterSubscriber = new newsletterSubscriber;
        $newsletterSubscriber->idNewsletterSubscriber = mysql_insert_id($db->link);
        $newsletterSubscriber->email = $email;
        $newsletterSubscriber->ip = $ip;
        $newsletterSubscriber->dateAdded = date('Y-m-d H:i:s');
        
        return $newsletterSubscriber;
    }
?>