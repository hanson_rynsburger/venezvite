<?php
    function insertRestaurantImage($restaurant, $file){
        global $db;
        
        $query = 'SET @order = (SELECT COUNT(`idImage`)
                FROM `restaurants-images`
                WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\'));';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $query = 'INSERT INTO `restaurants-images`
            SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
            `file` = \'' . mysql_real_escape_string($file) . '\', 
            `order` = (@order + 1);';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $restaurantImage = new restaurantImage;
        $restaurantImage->idImage = mysql_insert_id($db->link);
        $restaurantImage->file = $file;
    }
    
    function insertRestaurantImage2($restaurant, $file, $order){
    	global $db;

    	$query = 'INSERT INTO `restaurants-images`
            SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\',
            `file` = \'' . mysql_real_escape_string($file) . '\',
            `order` = \'' . mysql_real_escape_string($order) . '\';';
    	mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
    
    	$restaurantImage = new restaurantImage;
    	$restaurantImage->idImage = mysql_insert_id($db->link);
    	$restaurantImage->file = $file;
    	$restaurantImage->order = $order;
    }
    
    function listRestaurantImages($restaurant){
        global $db;
        
        $query = 'SELECT `idImage`, `file`, `order`
            FROM `restaurants-images`
            WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')
            ORDER BY `order` ASC;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $restaurantImages = array();
        while($row = mysql_fetch_assoc($result)){
            $restaurantImage = new restaurantImage;
            $restaurantImage->idImage = $row['idImage'];
            $restaurantImage->file = $row['file'];
            $restaurantImage->order = $row['order'];
            
            $restaurantImages[] = $restaurantImage;
        }
        
        return $restaurantImages;
    }
    
    function getRestaurantImageByID($idImage){
        global $db;
        
        $query = 'SELECT `idImage`, `idRestaurant`, `file`, `order`
            FROM `restaurants-images`
            WHERE (`idImage` = \'' . mysql_real_escape_string($idImage) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $row = mysql_fetch_assoc($result);
            
            $restaurantImage = new restaurantImage;
            $restaurantImage->idImage = $row['idImage'];
            
            $restaurantImage->restaurant = new restaurant;
            $restaurantImage->restaurant->idRestaurant = $row['idRestaurant'];
            
            $restaurantImage->file = $row['file'];
            $restaurantImage->order = $row['order'];
            
            return $restaurantImage;
        }else{
            return false;
        }
    }
    
    function deleteRestaurantImage($restaurantImage){
        global $db;
        
        $query = 'DELETE FROM `restaurants-images`
            WHERE (`idImage` = \'' . mysql_real_escape_string($restaurantImage->idImage) . '\');';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
    }
    
    function reorderRestaurantImage($restaurantImage, $order){
        global $db;
        
        $query = 'UPDATE `restaurants-images`
            SET `order` = \'' . mysql_real_escape_string($order) . '\'
            WHERE (`idImage` = \'' . mysql_real_escape_string($restaurantImage->idImage) . '\');';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
    }
?>