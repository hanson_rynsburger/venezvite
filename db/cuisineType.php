<?php
    function listCuisineTypes($restaurant, $onlyUsed){
        global $db;
        
        $query = 'SELECT CT.`idCuisineType`, CT.`cuisineType`, CT.`showInFilter`, COUNT(RCT.`idRestaurant`) AS restaurants 
            FROM `cuisine-types` CT';
        
        if($restaurant || $onlyUsed){
            // So we would only list the cuisine types selected by a certain restaurant (or all the restaurants, in general)
            $query .= ' 
            INNER JOIN `restaurants-cuisine-types` RCT ON RCT.`idCuisineType` = CT.`idCuisineType`';
            
            if($restaurant){
                $query .= ' 
                AND RCT.`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\'';
            }else{
                $query .= ' 
            INNER JOIN `restaurants` R ON R.`idRestaurant` = RCT.`idRestaurant`
                AND R.`visible` = \'Y\'
                AND R.`dateApproved` IS NOT NULL';
            }
        }else{
            $query .= ' 
            LEFT OUTER JOIN `restaurants-cuisine-types` RCT ON RCT.`idCuisineType` = CT.`idCuisineType`';
        }
        
        $query .= '
            GROUP BY CT.`idCuisineType` 
            ORDER BY CT.`cuisineType`;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $cuisineTypes = array();
        while($row = mysql_fetch_assoc($result)){
            $cuisineType = new cuisineType;
            $cuisineType->idCuisineType = $row['idCuisineType'];
            $cuisineType->cuisineType = $row['cuisineType'];
            $cuisineType->showInFilter = $row['showInFilter'];
            
            $cuisineType->restaurants = $row['restaurants'];
            
            $cuisineTypes[] = $cuisineType;
        }
        
        return $cuisineTypes;
    }
    
    function getCuisineTypeByID($idCuisineType){
        global $db;
        
        $query = 'SELECT `idCuisineType`, `cuisineType`, `showInFilter`
            FROM `cuisine-types`
            WHERE (`idCuisineType` = \'' . mysql_real_escape_string($idCuisineType) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $row = mysql_fetch_assoc($result);
            
            $cuisineType = new cuisineType;
            $cuisineType->idCuisineType = $row['idCuisineType'];
            $cuisineType->cuisineType = $row['cuisineType'];
            $cuisineType->showInFilter = $row['showInFilter'];
            
            return $cuisineType;
        }else{
            return false;
        }
    }
    /*
    function listCuisineTypesForFilter(){
        global $db;
        
        $query = 'SELECT `idCuisineType`, `cuisineType`
            FROM `cuisine-types`
            WHERE `showInFilter` = \'Y\'
            ORDER BY `cuisineType`;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $cuisineTypes = array();
        while($row = mysql_fetch_assoc($result)){
            $cuisineType = new cuisineType;
            $cuisineType->idCuisineType = $row['idCuisineType'];
            $cuisineType->cuisineType = $row['cuisineType'];
            
            $cuisineTypes[] = $cuisineType;
        }
        
        return $cuisineTypes;
    }
    */
?>