<?php
    function insertExchangeRate($usd, $eur = 1) {
        global $db;
        
        $query = 'INSERT INTO `exchange-rates`
            SET `eur` = \'' . mysql_real_escape_string($eur) . '\', 
			`usd` = \'' . mysql_real_escape_string($usd) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
    }
    
    function getLatestExchangeRate(){
        global $db;
        
        $query = 'SELECT `idExchangeRate`, `chf`, `eur`, `usd` 
            FROM `exchange-rates` 
            ORDER BY `dateAdded` DESC
            LIMIT 0, 1;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if (mysql_num_rows($result)==1) {
            $row = mysql_fetch_assoc($result);
            
            $exchangeRate = new exchangeRate;
            $exchangeRate->idExchangeRate = $row['idExchangeRate'];
            $exchangeRate->chf = $row['chf'];
            $exchangeRate->eur = $row['eur'];
            $exchangeRate->usd = $row['usd'];
            
            return $exchangeRate;
            
        } else {
            return false;
        }
    }
