<?php
    function getPromoCodeByCode($promoCode, $user) {
        global $db;
        
        $query = 'SELECT PC.`idPromoCode`, U.`idUser`, PC.`promoCode`, PC.`value`, PC.`usageNo`, PC.`minOrderValue`, PC.`dateStart`, PC.`dateEnd`, 
                COUNT(UPC.`idUserPromoCode`) AS usages 
            FROM `promo-codes` PC 
            LEFT OUTER JOIN `users` U ON U.`idUser` = PC.`idUser` 
            LEFT OUTER JOIN `users-promo-codes` UPC ON UPC.`idPromoCode` = PC.`idPromoCode` ';
        
        if ($user) {
            $query .= '
                AND (UPC.`idUser` = \'' . mysql_real_escape_string($user->idUser) . '\') ';
        }
        
        $query .= '
            WHERE (PC.`promoCode` = \'' . mysql_real_escape_string($promoCode) . '\')
            GROUP BY PC.`idPromoCode`;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if (mysql_num_rows($result)==1) {
            $row = mysql_fetch_assoc($result);
            
            $promoCode = new promoCode;
            $promoCode->idPromoCode = $row['idPromoCode'];
            
            if (!empty($row['idUser'])) {
                $promoCode->user = new user;
                $promoCode->user->idUser = $row['idUser'];
            }
            
            $promoCode->promoCode = $row['promoCode'];
            $promoCode->value = $row['value'];
            $promoCode->usageNo = $row['usageNo'];
            $promoCode->minOrderValue = $row['minOrderValue'];
            $promoCode->dateStart = $row['dateStart'];
            $promoCode->dateEnd = $row['dateEnd'];
            
            $promoCode->usages = $row['usages'];
            
            return $promoCode;
            
        } else {
            return false;
        }
    }
    
    function getPromoCodeByID($idPromoCode) {
        global $db;
        
        $query = 'SELECT `idPromoCode` 
            FROM `promo-codes` 
            WHERE (`idPromoCode` = \'' . mysql_real_escape_string($idPromoCode) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if (mysql_num_rows($result)==1) {
            $row = mysql_fetch_assoc($result);
            
            $promoCode = new promoCode;
            $promoCode->idPromoCode = $row['idPromoCode'];
            
            return $promoCode;
        } else {
            return false;
        }
    }
    
    function usePromoCode($promoCode, $user, $order){
        global $db;
        
        $query = 'INSERT INTO `users-promo-codes`
            SET `idUser` = \'' . mysql_real_escape_string($user->idUser) . '\', 
            `idPromoCode` = \'' . mysql_real_escape_string($promoCode->idPromoCode) . '\', 
            `idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
    }
    
    function getPromoCodeByOrder($order) {
        global $db;
        
        $query = 'SELECT PC.`idPromoCode`, PC.`promoCode`, PC.`value`, PC.`usageNo`, PC.`minOrderValue`, PC.`dateStart`, PC.`dateEnd` 
            FROM `promo-codes` PC 
            INNER JOIN `users-promo-codes` UPC ON UPC.`idPromoCode` = PC.`idPromoCode` 
            	AND (UPC.`idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\')
			GROUP BY PC.`idPromoCode`;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if (mysql_num_rows($result)==1) {
            $row = mysql_fetch_assoc($result);
            
            $promoCode = new promoCode;
            $promoCode->idPromoCode = $row['idPromoCode'];
            $promoCode->promoCode = $row['promoCode'];
            $promoCode->value = $row['value'];
            $promoCode->usageNo = $row['usageNo'];
            $promoCode->minOrderValue = $row['minOrderValue'];
            $promoCode->dateStart = $row['dateStart'];
            $promoCode->dateEnd = $row['dateEnd'];
            
            return $promoCode;
            
        } else {
            return false;
        }
    }
