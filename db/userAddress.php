<?php
	function listUserAddresses($entity){
		global $db;
		
		$query = 'SELECT UA.`idAddress`, U.`idUser`, U.`firstName`, U.`lastName`, CA.`idCorporateAccount`, CA.`contactFirstName`, CA.`contactLastName`, CA.`companyName`, UA.`address`, UA.`buildingNo`, UA.`floorSuite`, UA.`accessCode`, UA.`city`, UA.`zipCode`, UA.`latitude`, UA.`longitude`, UA.`phone`, UA.`default`, UA.`instructions`, UA.`dateAdded`, UA.`addrType`
			FROM `users-addresses` UA';
		
		if (!empty($entity)) {
			if (!empty($entity->idUser)) {
				$query .= '
			INNER JOIN `users` U ON U.`idUser` = UA.`idUser`
				AND (U.`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\')
			LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = UA.`idCorporateAccount`
				AND CA.`idCorporateAccount` IS NOT NULL';
			
			} elseif(!empty($entity->idCorporateAccount)) {
				$query .= '
			LEFT OUTER JOIN `users` U ON U.`idUser` = UA.`idUser`
				AND U.`idUser` IS NOT NULL
			INNER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = UA.`idCorporateAccount`
				AND (CA.`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\')';
			}
			
		} else {
			$query .= '
			LEFT OUTER JOIN `users` U ON U.`idUser` = UA.`idUser`
			LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = UA.`idCorporateAccount`';
		}
		
		$query .= '
			ORDER BY `default` ASC, `dateAdded` DESC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$userAddresses = array();
		while($row = mysql_fetch_assoc($result)){
			$userAddress = new userAddress;
			$userAddress->idAddress = $row['idAddress'];
			
			if (!empty($row['idUser'])) {
				$userAddress->user = new user;
				$userAddress->user->idUser = $row['idUser'];
				$userAddress->user->firstName = $row['firstName'];
				$userAddress->user->lastName = $row['lastName'];
			} elseif(!empty($row['idCorporateAccount'])) {
				$userAddress->corporateAccount = new corporateAccount;
				$userAddress->corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
				$userAddress->corporateAccount->contactFirstName = $row['contactFirstName'];
				$userAddress->corporateAccount->contactLastName = $row['contactLastName'];
				$userAddress->corporateAccount->companyName = $row['companyName'];
			}
			
			$userAddress->address = $row['address'];
			$userAddress->buildingNo = $row['buildingNo'];
			$userAddress->floorSuite = $row['floorSuite'];
			$userAddress->accessCode = $row['accessCode'];
			$userAddress->city = $row['city'];
			$userAddress->zipCode = $row['zipCode'];
			$userAddress->latitude = $row['latitude'];
			$userAddress->longitude = $row['longitude'];
			$userAddress->phone = $row['phone'];
			$userAddress->default = $row['default'];
			$userAddress->instructions = $row['instructions'];
			$userAddress->dateAdded = $row['dateAdded'];
			$userAddress->addrType = $row['addrType'];
			
			$userAddresses[] = $userAddress;
		}
		
		return $userAddresses;
	}
	
	function insertUserAddress($entity, $address, $buildingNo, $floorSuite, $accessCode, $city, $zipCode, $latitude, $longitude, $phone, $default, $instructions, $addrType){
		global $db;
		
		if ($default=='Y') {
			$query = 'UPDATE `users-addresses`
				SET `default` = \'N\'
				WHERE (';
			
			if (!empty($entity->idUser)) {
				$query .= '`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
			} elseif (!empty($entity->idCorporateAccount)) {
				$query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
			}
			
			$query .= ');';
			mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		}
		
		$query = 'INSERT INTO `users-addresses`
			SET ';
		
		if (@$entity->idUser) {
			$query .= '`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
		} elseif(@$entity->idCorporateAccount) {
			$query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
		}
		
		$query .= ', 
			`address` = \'' . mysql_real_escape_string($address) . '\', 
			`buildingNo` = \'' . mysql_real_escape_string($buildingNo) . '\', 
			`floorSuite` = ' . (!empty($floorSuite) ? '\'' . mysql_real_escape_string($floorSuite) . '\'' : 'NULL') . ', 
			`accessCode` = ' . (!empty($accessCode) ? '\'' . mysql_real_escape_string($accessCode) . '\'' : 'NULL') . ', 
			`city` = \'' . mysql_real_escape_string($city) . '\', 
			`zipCode` = \'' . mysql_real_escape_string($zipCode) . '\', 
			`latitude` = \'' . mysql_real_escape_string($latitude) . '\', 
			`longitude` = \'' . mysql_real_escape_string($longitude) . '\', 
			`phone` = \'' . mysql_real_escape_string($phone) . '\', 
			`addrType` = \'' . mysql_real_escape_string($addrType) . '\',
			`default` = \'' . mysql_real_escape_string($default) . '\', 
			`instructions` = \'' . mysql_real_escape_string($instructions) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');

		$userAddress = new userAddress;
		$userAddress->idAddress = mysql_insert_id($db->link);

		if (!empty($entity->idUser)) {
			$userAddress->user = $entity;
		} elseif(!empty($entity->idCorporateAccount)) {
			$userAddress->corporateAccount = $entity;
		}

		$userAddress->address = $address;
		$userAddress->buildingNo = $buildingNo;
		$userAddress->floorSuite = $floorSuite;
		$userAddress->accessCode = $accessCode;
		$userAddress->city = $city;
		$userAddress->zipCode = $zipCode;
		$userAddress->latitude = $latitude;
		$userAddress->longitude = $longitude;
		$userAddress->phone = $phone;
		$userAddress->default = $default;
		$userAddress->instructions = $instructions;
		$userAddress->addrType = $addrType;

		return $userAddress;
	}
	
	function getUserAddressByID($idAddress){
		global $db;
		
		$query = 'SELECT UA.`idAddress`, U.`idUser`, U.`firstName`, U.`lastName`, U.`email`, CA.`idCorporateAccount`, CA.`contactFirstName`, CA.`contactLastName`, CA.`emails`, UA.`shippingProfileID`, UA.`address`, UA.`buildingNo`, UA.`floorSuite`, UA.`accessCode`, UA.`city`, UA.`zipCode`, UA.`latitude`, UA.`longitude`, UA.`phone`, UA.`default`, UA.`instructions`, UA.`dateAdded`, UA.`addrType`
			FROM `users-addresses` UA
			LEFT OUTER JOIN `users` U ON U.`idUser` = UA.`idUser`
			LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = UA.`idCorporateAccount`
			WHERE (UA.`idAddress` = \'' . mysql_real_escape_string($idAddress) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			$row = mysql_fetch_assoc($result);
			
			$userAddress = new userAddress;
			$userAddress->idAddress = $row['idAddress'];
			
			if (!empty($row['idUser'])) {
				$userAddress->user = new user;
				$userAddress->user->idUser = $row['idUser'];
				$userAddress->user->firstName = $row['firstName'];
				$userAddress->user->lastName = $row['lastName'];
				$userAddress->user->email = $row['email'];
			} elseif(!empty($row['idCorporateAccount'])) {
				$userAddress->corporateAccount = new corporateAccount;
				$userAddress->corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
				$userAddress->corporateAccount->contactFirstName = $row['contactFirstName'];
				$userAddress->corporateAccount->contactLastName = $row['contactLastName'];
				$userAddress->corporateAccount->emails = explode(',', $row['emails']);
			}
			
			$userAddress->shippingProfileID = $row['shippingProfileID'];
			$userAddress->address = $row['address'];
			$userAddress->buildingNo = $row['buildingNo'];
			$userAddress->floorSuite = $row['floorSuite'];
			$userAddress->accessCode = $row['accessCode'];
			$userAddress->city = $row['city'];
			$userAddress->zipCode = $row['zipCode'];
			$userAddress->latitude = $row['latitude'];
			$userAddress->longitude = $row['longitude'];
			$userAddress->phone = $row['phone'];
			$userAddress->default = $row['default'];
			$userAddress->instructions = $row['instructions'];
			$userAddress->dateAdded = $row['dateAdded'];
			$userAddress->addrType = $row['addrType'];
			
			return $userAddress;
		} else {
			return false;
		}
	}
	
	function removeUserAddress($userAddress) {
		global $db;
		
		$query = 'DELETE FROM `users-addresses`
			WHERE (`idAddress` = \'' . mysql_real_escape_string($userAddress->idAddress) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function updateUserAddress($userAddress, $address, $buildingNo, $floorSuite, $accessCode, $city, $zipCode, $latitude, $longitude, $phone, $default, $instructions, $addrType) {
		global $db;
		
		if($default=='Y'){
			$query = 'UPDATE `users-addresses`
				SET `default` = \'N\'
				WHERE (';
			
			if(!empty($userAddress->user)){
				$query .= '`idUser` = \'' . mysql_real_escape_string($userAddress->user->idUser) . '\'';
			}elseif(!empty($userAddress->corporateAccount)){
				$query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($user->idCorporateAccount) . '\'';
			}
			
			$query .= ');';
			mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		}
		
		$query = 'UPDATE `users-addresses`
			SET `address` = \'' . mysql_real_escape_string($address) . '\', 
			`buildingNo` = \'' . mysql_real_escape_string($buildingNo) . '\', 
			`floorSuite` = ' . (!empty($floorSuite) ? '\'' . mysql_real_escape_string($floorSuite) . '\'' : 'NULL') . ', 
			`accessCode` = ' . (!empty($accessCode) ? '\'' . mysql_real_escape_string($accessCode) . '\'' : 'NULL') . ', 
			`city` = \'' . mysql_real_escape_string($city) . '\', 
			`zipCode` = \'' . mysql_real_escape_string($zipCode) . '\', 
			`latitude` = \'' . mysql_real_escape_string($latitude) . '\', 
			`longitude` = \'' . mysql_real_escape_string($longitude) . '\', 
			`phone` = \'' . mysql_real_escape_string($phone) . '\', 
			`default` = \'' . mysql_real_escape_string($default) . '\', 
			`instructions` = \'' . mysql_real_escape_string($instructions) . '\',
			`addrType` = \'' . mysql_real_escape_string($addrType) . '\'
			WHERE (`idAddress` = \'' . mysql_real_escape_string($userAddress->idAddress) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function updateShippingProfile($userAddress, $shippingProfileID) {
		global $db;
		
		$query = 'UPDATE `users-addresses`
			SET `shippingProfileID` = \'' . mysql_real_escape_string($shippingProfileID) . '\'
			WHERE (`idAddress` = \'' . mysql_real_escape_string($userAddress->idAddress) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
		
	function setDefaultUserAddress($userAddress, $entity) {
		global $db;
		
		$query = 'UPDATE `users-addresses`
			SET `default` = \'N\'
			WHERE (';
		
		if (!empty($userAddress->user)) {
			$query .= '`idUser` = \'' . mysql_real_escape_string($userAddress->user->idUser) . '\'';
		} elseif(!empty($userAddress->corporateAccount)) {
			$query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($user->idCorporateAccount) . '\'';
		}
		
		$query .= ');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		
		$query = 'UPDATE `users-addresses`
			SET `default` = \'Y\'
			WHERE (`idAddress` = \'' . mysql_real_escape_string($userAddress->idAddress) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
