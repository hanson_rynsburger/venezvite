<?php
    function listOrderItemOptions($orderItem){
        global $db;
        
        $query = 'SELECT OIO.`idOrderItemOption`, RMIO.`idMenuItemOption`, RMIO.`menuItemOption` AS originalMenuItemOption, RMIO.`price` AS originalPrice, OIO.`menuItemOption`, OIO.`price`
            FROM `orders-items-options` OIO
            INNER JOIN `orders-items` OI ON OI.`idOrderItem` = OIO.`idOrderItem`
                AND (OI.`idOrderItem` = \'' . mysql_real_escape_string($orderItem->idOrderItem) . '\')
            LEFT OUTER JOIN `restaurants-menus-items-options` RMIO ON RMIO.`idMenuItemOption` = OIO.`idMenuItemOption`;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $orderItemOptions = array();
        while($row = mysql_fetch_assoc($result)){
            $orderItemOption = new orderItemOption;
            $orderItemOption->idOrderItemOption = $row['idOrderItemOption'];
            
            $orderItemOption->menuItemOption = new menuItemOption;
            $orderItemOption->menuItemOption->idMenuItemOption = $row['idMenuItemOption'];
            $orderItemOption->menuItemOption->menuItemOption = $row['originalMenuItemOption'];
            $orderItemOption->menuItemOption->price = $row['originalPrice'];
            
            $orderItemOption->menuItemOptionName = $row['menuItemOption'];
            $orderItemOption->price = $row['price'];
            
            $orderItemOptions[] = $orderItemOption;
        }
        
        return $orderItemOptions;
    }
    
    function insertOrderItemOption($orderItem, $idMenuItemOption, $menuItemOption, $price){
        global $db;
        
        $query = 'INSERT INTO `orders-items-options`
            SET `idOrderItem` = \'' . mysql_real_escape_string($orderItem->idOrderItem) . '\', 
            `idMenuItemOption` = \'' . mysql_real_escape_string($idMenuItemOption) . '\', 
            `menuItemOption` = \'' . mysql_real_escape_string($menuItemOption) . '\', 
            `price` = \'' . mysql_real_escape_string($price) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $orderItemOption = new orderItemOption;
        $orderItemOption->idOrderItemOption = mysql_insert_id($db->link);
        $orderItemOption->orderItem = $orderItem;
        
        $orderItemOption->menuItemOption = new menuItemOption;
        $orderItemOption->menuItemOption->idMenuItemOption = $idMenuItemOption;
        
        $orderItemOption->menuItemOptionName = $menuItemOption;
        $orderItemOption->price = $price;
        
        return $orderItemOption;
    }
?>