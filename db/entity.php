<?php
    /*
    function checkUserDuplicateEmail($email){
        global $db;
        
        $query = 'SELECT `idUser`, `firstName`, `lastName`, `email`
            FROM `users`
            WHERE (`email` = \'' . mysql_real_escape_string($email) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if(mysql_num_rows($result)>0){
            return true;
        }else{
            return false;
        }
    }
    */
    function listPaymentProfiles($entity){
        global $db;
        
        $query = 'SELECT `paymentProfileID`, `default`
            FROM `customer-payment-profiles`
            WHERE (`customerProfileID` = \'' . mysql_real_escape_string($entity->customerProfileID) . '\')
            ORDER BY `default` ASC, `dateAdded` DESC;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $paymentProfiles = array();
        while($row = mysql_fetch_assoc($result)){
            $paymentProfiles[] = $row;
        }
        
        return $paymentProfiles;
    }
    
    function addCustomerProfile($entity, $customerProfileID){
        global $db;
        
        $query = 'UPDATE `' . (get_class($entity)=='user' ? 'users' : 'corporate-accounts') . '`
            SET `customerProfileID` = \'' . mysql_real_escape_string($customerProfileID) . '\'
            WHERE (`' . (get_class($entity)=='user' ? 'idUser' : 'idCorporateAccount') . '` = \'' . mysql_real_escape_string(get_class($entity)=='user' ? $entity->idUser : $entity->idCorporateAccount) . '\');';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
    }
    
    function insertPaymentProfile($entity, $paymentProfileID, $default){
        global $db;
        
        if($default=='Y' && !empty($entity->customerProfileID)){
            // Modify all customer profiles' "default" value
            $query = 'UPDATE `customer-payment-profiles`
                SET `default` = \'N\'
                WHERE (`customerProfileID` = \'' . mysql_real_escape_string($entity->customerProfileID) . '\');';
            mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        }
        
        $query = 'INSERT INTO `customer-payment-profiles`
            SET `paymentProfileID` = \'' . mysql_real_escape_string($paymentProfileID) . '\', 
            `customerProfileID` = \'' . mysql_real_escape_string($entity->customerProfileID) . '\', 
            `default` = \'' . mysql_real_escape_string($default) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
    }
    
    function removePaymentProfile($entity, $paymentProfileID){
        global $db;
        
        $query = 'DELETE FROM `customer-payment-profiles`
            WHERE `paymentProfileID` = \'' . mysql_real_escape_string($paymentProfileID) . '\'
            AND `customerProfileID` = \'' . mysql_real_escape_string($entity->customerProfileID) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
    }
    
    
    function canPlaceCashOrders($entity){
        global $db;
        
        // First we'll check if there are any unpaid cash orders
        $query = 'SELECT `idOrder` 
            FROM `orders` 
            WHERE (`paid` = \'N\'
                AND ';
        
        if (get_class($entity)=='user') {
            $query .= '`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
        } else {
            $query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
        }
        
        $query .= ');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $paid = (mysql_num_rows($result)==0);
        
        
        // ... and secondly, if there are any pending cash orders
        // (so users won't be able to place 10 consecutive cash orders)
        $query = 'SELECT `idOrder` 
            FROM `orders` 
            WHERE (`paid` IS NULL
                AND ';
        
        if (get_class($entity)=='user') {
            $query .= '`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
        } else {
            $query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
        }
        
        $query .= ');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $pending = (mysql_num_rows($result)>0);
        
        
        return array(
            'paid'      => $paid, 
            'pending'   => $pending
        );
    }
    
    
    function saveSearchAddress($entity, $address, $latitude, $longitude){
        global $db;
        
        // First let's check to see if the current address is not already in the DB
        $query = 'SELECT `idSearchAddress` 
            FROM `search-addresses` 
            WHERE (';
        
        if (get_class($entity)=='user') {
            $query .= '`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
        } else {
            $query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
        }
        
        $query .= ' 
            AND `latitude` = \'' . mysql_real_escape_string(round($latitude, 5)) . '\' 
            AND `longitude` = \'' . mysql_real_escape_string(round($longitude, 5)) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        if (mysql_num_rows($result)>0) {
            // Let's update the current existing address' lastUsed date, so that it will be the 1st one listed
            $query = 'UPDATE `search-addresses` 
                SET `dateLastUsed` = Now() 
                WHERE (`idSearchAddress` = \'' . mysql_result($result, 0, 'idSearchAddress') . '\');';
            mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
            
        } else {
            // It seems to be unique, so let's save it
            $query = 'INSERT INTO `search-addresses` 
                SET ';
            
            if (get_class($entity)=='user') {
                $query .= '`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
            } else {
                $query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
            }
            
            $query .= ', 
                `address` = \'' . mysql_real_escape_string($address) . '\', 
                `latitude` = \'' . mysql_real_escape_string($latitude) . '\', 
                `longitude` = \'' . mysql_real_escape_string($longitude) . '\';';
            $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        }
    }
    
    
    function listSearchAddresses($entity){
        global $db;
        
        // First let's check to see if the current address is not already in the DB
        $query = 'SELECT * 
            FROM `search-addresses` 
            WHERE (';
        
        if (get_class($entity)=='user') {
            $query .= '`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
        } else {
            $query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
        }
        
        $query .= ')
            ORDER BY `dateLastUsed` DESC;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $searchAddresses = array();
        while($row = mysql_fetch_assoc($result)){
            $searchAddresses[] = $row;
        }
        
        return $searchAddresses;
    }
?>