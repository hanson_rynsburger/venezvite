<?php
	function restaurantInsert($restaurantName, $contactFirstName, $contactLastName, $address, $city, $zipCode, $latitude, $longitude, $website, $phone, $mobile, $email, $delivery, $catering, $cashPayment, $restaurantDescription, $idPreferredLanguage, $ip, $customURL, $customDelivery, $takeaway, $country) {
		global $db;
		
		$query = 'INSERT INTO `restaurants`
			SET `restaurantName` = \'' . mysql_real_escape_string($restaurantName) . '\', 
			`contactFirstName` = \'' . mysql_real_escape_string($contactFirstName) . '\', 
			`contactLastName` = \'' . mysql_real_escape_string($contactLastName) . '\', 
			`idCountry` = \'' . mysql_real_escape_string($country->idCountry) . '\', 
			`address` = \'' . mysql_real_escape_string($address) . '\', 
			`city` = \'' . mysql_real_escape_string($city) . '\', 
			`zipCode` = \'' . mysql_real_escape_string($zipCode) . '\', 
			`latitude` = ' . (empty($latitude) ? 'NULL' : '\'' . mysql_real_escape_string($latitude) . '\'') . ', 
			`longitude` = ' . (empty($longitude) ? 'NULL' : '\'' . mysql_real_escape_string($longitude) . '\'') . ', 
			`website` = ' . (empty($website) ? 'NULL' : '\'' . mysql_real_escape_string($website) . '\'') . ', 
			`phone` = \'' . mysql_real_escape_string($phone) . '\', 
			`mobile` = ' . (empty($mobile) ? 'NULL' : '\'' . mysql_real_escape_string($mobile) . '\'') . ', 
			`email` = \'' . mysql_real_escape_string($email) . '\', 
			`takeaway` = \'' . (!$takeaway || $takeaway=='N' ? 'N' : 'Y') . '\', 
			`delivery` = \'' . mysql_real_escape_string($delivery) . '\', 
			`customDelivery` = \'' . (!$customDelivery || $customDelivery=='N' ? 'N' : 'Y') . '\', 
			`catering` = \'' . mysql_real_escape_string($catering) . '\', 
			`cashPayment` = \'' . mysql_real_escape_string($cashPayment) . '\', 
			`restaurantDescription` = \'' . mysql_real_escape_string($restaurantDescription) . '\', 
			`idPreferredLanguage` = \'' . mysql_real_escape_string($idPreferredLanguage) . '\', 
			`ip` = \'' . mysql_real_escape_string($ip) . '\', 
			`customURL` = \'' . mysql_real_escape_string($customURL) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$restaurant = new restaurant;
		$restaurant->idRestaurant = mysql_insert_id($db->link);
		$restaurant->restaurantName = $restaurantName;
		$restaurant->contactFirstName = $contactFirstName;
		$restaurant->contactLastName = $contactLastName;
		$restaurant->country = $country;
		$restaurant->address = $address;
		$restaurant->city = $city;
		$restaurant->zipCode = $zipCode;
		$restaurant->latitude = $latitude;
		$restaurant->longitude = $longitude;
		$restaurant->website = $website;
		$restaurant->phone = $phone;
		$restaurant->mobile = $mobile;
		$restaurant->email = $email;
		$restaurant->takeaway = $takeaway;
		$restaurant->delivery = $delivery;
		$restaurant->customDelivery = $customDelivery;
		$restaurant->catering = $catering;
		$restaurant->cashPayment = $cashPayment;
		$restaurant->restaurantDescription = $restaurantDescription;
		
		$restaurant->preferredLanguage = new language;
		$restaurant->preferredLanguage->idLanguage = $idPreferredLanguage;
		
		$restaurant->ip = $ip;
		$restaurant->dateSubmitted = date('Y-m-d H:i:s');
		
		return $restaurant;
	}
	
	function attachRestaurantCuisineType($restaurant, $cuisineType){
		global $db;
		
		$query = 'INSERT INTO `restaurants-cuisine-types`
			SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`idCuisineType` = \'' . mysql_real_escape_string($cuisineType->idCuisineType) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function insertRestaurantOperationHours($restaurant, $day, $openingHour, $closingHour){
		global $db;
		
		$query = 'INSERT INTO `restaurants-timetable`
			SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`dow` = \'' . mysql_real_escape_string($day) . '\', 
			`openingHour` = \'' . mysql_real_escape_string($openingHour) . ':00\', 
			`closingHour` = \'' . mysql_real_escape_string($closingHour) . ':00\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function getRestaurantByID($idRestaurant){
		global $db;
		
		$query = 'SELECT R.`idRestaurant`, R.`restaurantName`, R.`contactFirstName`, R.`contactLastName`, R.`address`, R.`city`, R.`zipCode`, R.`latitude`, R.`longitude`, R.`website`, R.`phone`, R.`mobile`, R.`email`, R.`takeaway`, R.`delivery`, R.`customDelivery`, R.`catering`, R.`cashPayment`, R.`currency`, R.`restaurantDescription`, R.`menuFile`, L.`idLanguage`, R.`ip`, R.`customURL`, R.`preparationTime`, R.`maxOrdersPerHour`, R.`spatchoID`, R.`spatchoID2`, R.`dateSubmitted`, R.`dateValidated`, R.`dateApproved`
			FROM `restaurants` R
			LEFT OUTER JOIN `languages` L ON L.`idLanguage` = R.`idPreferredLanguage`
			WHERE (R.`idRestaurant` = \'' . mysql_real_escape_string($idRestaurant) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			$row = mysql_fetch_assoc($result);
			
			$restaurant = new restaurant;
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			
			$restaurant->address = $row['address'];
			$restaurant->city = $row['city'];
			$restaurant->zipCode = $row['zipCode'];
			$restaurant->latitude = $row['latitude'];
			$restaurant->longitude = $row['longitude'];
			$restaurant->phone = $row['phone'];
			
			$restaurant->email = $row['email'];
			$restaurant->takeaway = $row['takeaway'];
			$restaurant->delivery = $row['delivery'];
			$restaurant->customDelivery = $row['customDelivery'];
			$restaurant->cashPayment = $row['cashPayment'];
			$restaurant->currency = $row['currency'];
			// ...
			$restaurant->customURL = $row['customURL'];
			$restaurant->preparationTime = $row['preparationTime'];
			$restaurant->maxOrdersPerHour = $row['maxOrdersPerHour'];
			$restaurant->spatchoID = $row['spatchoID'];
			$restaurant->spatchoID2 = $row['spatchoID2'];
			$restaurant->dateSubmitted = $row['dateSubmitted'];
			
			$restaurant->timetable = $restaurant->listTimetable();
			$restaurant->deliveryHours = $restaurant->listDeliveryHours();
			$restaurant->deliveryZones = deliveryZone::list_($restaurant);
			
			return $restaurant;
			
		} else {
			return false;
		}
	}
	
	function updateRestaurantMenuFile($restaurant, $file){
		global $db;
		
		$query = 'UPDATE `restaurants`
			SET `menuFile` = \'' . mysql_real_escape_string($file) . '\'
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function listUnconfirmedRestaurants(){
		global $db;
		
		$query = 'SELECT `idRestaurant`, `restaurantName`, `email`
			FROM `restaurants`
			WHERE `dateValidated` IS NULL;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$restaurants = array();
		while($row = mysql_fetch_assoc($result)){
			$restaurant = new restaurant;
			
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			$restaurant->email = $row['email'];
			
			$restaurants[] = $restaurant;
		}
		
		return $restaurants;
	}
	
	function confirmRestaurant($restaurant){
		global $db;
		
		$query = 'UPDATE `restaurants`
			SET `dateValidated` = Now()
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function listUnapprovedRestaurants(){
		global $db;
		
		$query = 'SELECT `idRestaurant`, `restaurantName`, `email`, `dateValidated`
			FROM `restaurants`
			WHERE `dateApproved` IS NULL;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$restaurants = array();
		while($row = mysql_fetch_assoc($result)){
			$restaurant = new restaurant;
			
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			$restaurant->email = $row['email'];
			$restaurant->dateValidated = $row['dateValidated'];
			
			$restaurants[] = $restaurant;
		}
		
		return $restaurants;
	}
	
	function approveRestaurant($restaurant){
		global $db;
		
		$query = 'UPDATE `restaurants`
			SET `dateApproved` = Now()
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	/*
	function updateRestaurantBankInfo($restaurant, $bankAccountNo, $iban, $bankAccountName, $businessAddress, $bankName, $bankAddress, $swiftCode, $cashPayment){
		global $db;
		
		$query = 'UPDATE `restaurants`
			SET `cashPayment` = \'' . ($cashPayment ? 'Y' : 'N') . '\', 
			`bankAccountNo` = \'' . mysql_real_escape_string($bankAccountNo) . '\', 
			`iban` = \'' . mysql_real_escape_string($iban) . '\', 
			`bankAccountName` = \'' . mysql_real_escape_string($bankAccountName) . '\', 
			`businessAddress` = \'' . mysql_real_escape_string($businessAddress) . '\', 
			`bankName` = \'' . mysql_real_escape_string($bankName) . '\', 
			`bankAddress` = \'' . mysql_real_escape_string($bankAddress) . '\', 
			`swiftCode` = \'' . mysql_real_escape_string($swiftCode) . '\'
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	*/
	function updateRestaurant($restaurant, $data){
		global $db;
		
		$conditions = array();
		foreach($data as $column => $value){
			$conditions[] = '`' . $column . '` = ' . ($value===null ? 'NULL' : '\'' . mysql_real_escape_string($value) . '\'');
		}
		
		$query = 'UPDATE `restaurants` 
			SET ' . implode(", \n", $conditions) . '
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : 'DB query error!');
	}
	
	function checkCustomTimetable($restaurant, $dateTime) {
		$customTimetable = listRestaurantCustomTimetable($restaurant);
		
		foreach ($customTimetable as $exception) {
			if ($exception['date']==date('Y-m-d', $dateTime)) {
				if ((empty($exception['openingHour']) && empty($exception['closingHour'])) || 
					(!empty($exception['openingHour']) && strtotime(date('Y-m-d', $dateTime) . ' ' . $exception['openingHour'])>$dateTime) || 
					(!empty($exception['closingHour']) && strtotime(date('Y-m-d', $dateTime) . ' ' . $exception['closingHour'])<$dateTime)) {
					
					return false;
				}
				
				break;
			}
		}
		
		return true;
	}
	
	function searchRestaurants($location, $searchTerm, $dateTime, $coords, $user, $sort, $filters, $category, $rating, $type, $price_level) {
		global $db;
		//$time = microtime(true);
		$query = 'INSERT INTO `search-terms`
			SET `location` = \'' . mysql_real_escape_string($location) . '\', 
			`searchTerm` = \'' . mysql_real_escape_string($searchTerm) . '\', 
			`ip` = \'' . mysql_real_escape_string($_SERVER['REMOTE_ADDR']) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$query = '(SELECT DISTINCT R.`idRestaurant`, R.`restaurantName`, R.`address`, R.`city`, R.`zipCode`, R.`latitude`, R.`longitude`, R.`takeaway`, R.`delivery`, R.`customDelivery`, R.`catering`, R.`cashPayment`, R.`currency`, (SELECT RDZ.`minimumDelivery`
				FROM `restaurants-delivery-zones` RDZ
				WHERE RDZ.`idRestaurant` = R.`idRestaurant`
				ORDER BY RDZ.`minimumDelivery` ASC
				LIMIT 0, 1) AS minimumDelivery, R.`customURL`, R.`preparationTime`, R.`dateApproved`, (SELECT MAX(RDZ.`range`)
				FROM `restaurants-delivery-zones` RDZ
				WHERE RDZ.`idRestaurant` = R.`idRestaurant`) AS max_range, CASE WHEN \'' . $type . '\' = \'D\' THEN
						(SELECT RDH.`idDeliveryHour`
						FROM `restaurants-delivery-hours` RDH
						WHERE RDH.`idRestaurant` = R.`idRestaurant`
							AND RDH.`dow` = \'' . date('N', $dateTime) . '\'
							AND RDH.`hourStart` <= \'' . date('H:i:s', $dateTime) . '\'
							AND RDH.`hourEnd` >= \'' . date('H:i:s', $dateTime) . '\'
						LIMIT 0, 1)
					ELSE 
						(SELECT RT.`idTimetable`
						FROM `restaurants-timetable` RT
						WHERE RT.`idRestaurant` = R.`idRestaurant`
							AND RT.`dow` = \'' . date('N', $dateTime) . '\'
							AND RT.`openingHour` <= \'' . date('H:i:s', $dateTime) . '\'
							AND RT.`closingHour` >= \'' . date('H:i:s', $dateTime) . '\'
						LIMIT 0, 1)
					END AS opened, (SELECT SUM(ROUND(RMI.`price` * ((100 - RMI.`discount`) / 100), 2)) / COUNT(RMI.`idMenuItem`) 
						FROM `restaurants-menus-items` RMI 
						WHERE RMI.`idMenu` = RM.`idMenu`) AS priceAverage, (SELECT AVG(DISTINCT RR1.`rating`)
				FROM `restaurants-ratings` RR1
				WHERE RR1.`idRestaurant` = R.`idRestaurant`) AS rating, (SELECT COUNT(DISTINCT RR2.`idRating`)
				FROM `restaurants-ratings` RR2
				WHERE RR2.`idRestaurant` = R.`idRestaurant`) AS reviews, R.`ourFavorite`, (SELECT UF.`idFavorite`
					FROM `users-favorites` UF
					WHERE UF.`idRestaurant` = R.`idRestaurant`
						AND UF.`idUser` ' . ($user ? '= \'' . mysql_real_escape_string($user->idUser) . '\'' : 'IS NULL') . ') AS userFavorite, \'N\' AS promoted
			FROM `restaurants` R
			INNER JOIN `restaurants-cuisine-types` RCT ON RCT.`idRestaurant` = R.`idRestaurant`';
		
		if ($filters && is_array($filters)) {
			$query .= '
				AND RCT.`idCuisineType` IN (' . mysql_real_escape_string(implode(', ', $filters)) . ')';
		}
		
		$query .= '
			INNER JOIN `cuisine-types` CT ON CT.`idCuisineType` = RCT.`idCuisineType`
			INNER JOIN `restaurants-menus` RM ON RM.`idRestaurant` = R.`idRestaurant`
			INNER JOIN `restaurants-menus-items` RMI ON RMI.`idMenu` = RM.`idMenu`
				AND RMI.`enabled` = \'Y\'';
		
		if ($category) {
			if ($category=='open-late') {
				$query .= '
			INNER JOIN `restaurants-timetable` RT ON RT.`idRestaurant` = R.`idRestaurant`
				AND (RT.`dow` = \'' . date('N', $dateTime) . '\') 
				AND (RT.`closingHour` > \'22:00\' 
					OR RT.`closingHour` <= \'03:00\')';
			} else if(in_array($category, array('wine-liquor', 'bakery-desserts'))) {
				
				$query .= '
			INNER JOIN `restaurants-menus-categories` RMC ON RMC.`idMenuCategory` = RMI.`idMenuCategory`';
				
				if ($category=='wine-liquor') {
					$query .= '
				AND (RMC.`category` LIKE \'%Drinks%\'
					OR RMC.`category` LIKE \'%Boissons%\'
					OR RMC.`category` LIKE \'%Wine%\'
					OR RMC.`category` LIKE \'%Vins%\'
					OR RMC.`category` LIKE \'%Champagne%\')';
				} else if($category=='bakery-desserts') {
					$query .= '
				AND (RMC.`category` LIKE \'%Cupcakes%\'
					OR RMC.`category` LIKE \'%Pātisseries%\'
					OR RMC.`category` LIKE \'%pastries%\'
					OR RMC.`category` LIKE \'%Desserts%\'
					OR RMC.`category` LIKE \'%sucreries%\'
					OR RMC.`category` LIKE \'%Sweets%\'
					OR RMC.`category` LIKE \'%Patisserie%\'
					OR RMC.`category` LIKE \'%chocolat%\'
					OR RMC.`category` LIKE \'%Confiture%\')';
				}
			}
		}
		
		$query .= '
			WHERE (R.`visible` = \'Y\'';
		
		if ($filters) {
			/*if(in_array('catering', $filters)){
				$query .= '
				AND R.`catering` = \'Y\'';
			}else*/if (in_array('venezvite', $filters)) {
				$query .= '
				AND R.`ourFavorite` = \'Y\'';
			}
		}
		
		if ($type && $type=='D') {
			$query .= '
				AND R.`delivery` = \'Y\'';
		}
		
		if ($category) {
			if ($category=='catering') {
				$query .= '
				AND R.`catering` = \'Y\'';
			}
		}
		
		if (is_numeric($rating)) {
			$query .= '
				AND (SELECT AVG(DISTINCT RR1.`rating`)
					FROM `restaurants-ratings` RR1
					WHERE RR1.`idRestaurant` = R.`idRestaurant`) >= ' . $rating;
		}
		
		$query .= '
				AND (R.`restaurantName` LIKE \'%' . mysql_real_escape_string($searchTerm) . '%\'
					OR R.`restaurantDescription` LIKE \'%' . mysql_real_escape_string($searchTerm) . '%\'
					OR CT.`cuisineType` LIKE \'%' . mysql_real_escape_string($searchTerm) . '%\'
					OR RMI.`menuItemName` LIKE \'%' . mysql_real_escape_string($searchTerm) . '%\'
					OR RMI.`menuItemDescription` LIKE \'%' . mysql_real_escape_string($searchTerm) . '%\')
				AND R.`dateApproved` IS NOT NULL)';
		/*
		if ($price_level) {
			$query .= ' 
			HAVING `priceAverage` < 20';
		}
		*/
		$query .= ') 
			UNION
			(SELECT DISTINCT R.`idRestaurant`, R.`restaurantName`, R.`address`, R.`city`, R.`zipCode`, R.`latitude`, R.`longitude`, R.`takeaway`, R.`delivery`, R.`customDelivery`, R.`catering`, R.`cashPayment`, R.`currency`, (SELECT RDZ.`minimumDelivery`
				FROM `restaurants-delivery-zones` RDZ
				WHERE RDZ.`idRestaurant` = R.`idRestaurant`
				ORDER BY RDZ.`minimumDelivery` ASC
				LIMIT 0, 1) AS minimumDelivery, R.`customURL`, R.`preparationTime`, R.`dateApproved`, 5 AS max_range, NULL AS opened, 0 AS priceAverage, 0 AS rating, 0 AS reviews, R.`ourFavorite`, NULL AS userFavorite, \'Y\' AS promoted
			FROM `restaurants` R
			WHERE (R.`visible` = \'Y\'
				AND R.`email` IS NULL
				AND (R.`restaurantName` LIKE \'%' . mysql_real_escape_string($searchTerm) . '%\'
					OR R.`restaurantDescription` LIKE \'%' . mysql_real_escape_string($searchTerm) . '%\')))
			ORDER BY `promoted` ASC';
		
		switch ($sort) {
			case 'alpha':
			case 'alphabetically':
				$query .= ', restaurantName ASC';
				break;
			case 'delivery':
			case 'delivery_minimum':
				$query .= ', minimumDelivery ASC, '/*distance ASC, */ . 'rating DESC';
				break;
			case 'rating':
			case 'ratings':
				$query .= ', rating DESC'/*, distance ASC'*/;
				break;
			case 'open':
				$query .= ', opened DESC, '/*distance ASC, */ . 'rating DESC';
				break;
			case 'price':
				$query .= ', priceAverage ASC, '/*distance ASC, */ . 'rating DESC';
				break;
			/*
			case 'distance':
			default:
				$query .= ', distance ASC, rating DESC';
			*/
		}
		
		$query .= ';';
		//error_log(nl2br($query));
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		//error_log(microtime(true) - $time);
		$restaurants = array();
		while ($row = mysql_fetch_assoc($result)) {
			if ($price_level) {
				// It's much faster to do this check here than include it in SQL
				switch ($price_level) {
					case 1:
						if ($row['priceAverage']>=20) {
							continue 2;
						}
						break;
					case 2:
						if ($row['priceAverage']>=25) {
							continue 2;
						}
						break;
					case 3:
						if ($row['priceAverage']>=35) {
							continue 2;
						}
						break;
					case 4:
						if ($row['priceAverage']>=45) {
							continue 2;
						}
						break;
				}
			}
			
			
			$restaurant = new restaurant;
			
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			$restaurant->address = $row['address'];
			$restaurant->city = $row['city'];
			$restaurant->zipCode = $row['zipCode'];
			$restaurant->latitude = $row['latitude'];
			$restaurant->longitude = $row['longitude'];
			$restaurant->takeaway = $row['takeaway'];
			$restaurant->delivery = $row['delivery'];
			$restaurant->customDelivery = $row['customDelivery'];
			$restaurant->catering = $row['catering'];
			$restaurant->cashPayment = $row['cashPayment'];
			$restaurant->currency = $row['currency'];
			$restaurant->ourFavorite = $row['ourFavorite'];
			$restaurant->customURL = $row['customURL'];
			$restaurant->preparationTime = $row['preparationTime'];
			$restaurant->dateApproved = $row['dateApproved'];
			
			if ($type=='D') {
				// In case of deliveries, let's check the custom delivery schedule records
				// If there's a no-delivery day for the searched date, 
				// we'll just skip the current restaurant from the searches
				$customDeliveryHours = listRestaurantCustomDeliveryHours($restaurant);
				
				foreach ($customDeliveryHours as $exception) {
					if ($exception['date']==date('Y-m-d', $dateTime)) {
						if ((empty($exception['hourStart']) && empty($exception['hourEnd'])) || 
							(!empty($exception['hourStart']) && strtotime(date('Y-m-d', $dateTime) . ' ' . $exception['hourStart'])>$dateTime) || 
							(!empty($exception['hourEnd']) && strtotime(date('Y-m-d', $dateTime) . ' ' . $exception['hourEnd'])<$dateTime)) {
							
							continue 2;
						}
						
						break;
					}
				}
			}
			
			if (!empty($row['dateApproved'])) {
				$restaurant->cuisineTypes = cuisineType::list_($restaurant);
				
				$restaurant->deliveryZones = deliveryZone::list_($restaurant);
				if ($type=='D') {
					$restaurant->deliveryHours = $restaurant->listDeliveryHours();
					
				} else {
					$restaurant->timetable = $restaurant->listTimetable();
				}
			}
			$restaurant->max_range = $row['max_range'];
			//$restaurant->distance = $row['distance'];
			$restaurant->opened = (!empty($row['opened']));
			$restaurant->rating = (!empty($row['rating']) ? number_format($row['rating'], 1) : '0');
			$restaurant->reviews = $row['reviews'];
			$restaurant->userFavorite = $row['userFavorite'];
			
			if ($restaurant->opened) {
				// Let's see if the current restaurant isn't closed today
				$restaurant->opened = checkCustomTimetable($restaurant, $dateTime);
			}
			
			$restaurant->priceLevel = $restaurant->getPriceLevel(/*$row['priceAverage']*/);
			
			$ris = restaurantImage::list_( $restaurant );
			
			$restaurant->images = array();
			foreach ( $ris as $ri ) {
				if ( $ri->order == 2 ) {
					$restaurant->images[] = $ri->file;
					break;
				}
			}
			if ( $ris && count($restaurant->images) == 0 ) {
				$restaurant->images[] = $ris[0]->file;
			}
			
			$restaurants[] = $restaurant;
		}
		
		return $restaurants;
	}
	
	function getRestaurantByURL($url, $get_price_level = true) {
		global $db;
		
		//date_default_timezone_set('Europe/Zurich');
		$type = 'T';
		if (@$_SESSION['s_venezvite']['search']['type']=='D') {
			$type = 'D';
		}
		$timestamp = time();
		if (@$_SESSION['s_venezvite']['search']['date'] && @$_SESSION['s_venezvite']['search']['time']) {
			$timestamp = strtotime($_SESSION['s_venezvite']['search']['date'] . ' ' . $_SESSION['s_venezvite']['search']['time']);
		}
		
		$query = 'SELECT DISTINCT R.`idRestaurant`, R.`restaurantName`, R.`idCountry`, R.`address`, R.`city`, R.`zipCode`, R.`latitude`, R.`longitude`, R.`website`, R.`phone`, R.`mobile`, R.`email`, R.`takeaway`, R.`delivery`, R.`customDelivery`, R.`catering`, R.`cashPayment`, R.`currency`, R.`restaurantDescription`, R.`menuFile`, R.`ip`, R.`customURL`, R.`preparationTime`, R.`dateSubmitted`, R.`dateValidated`, R.`dateApproved`, CASE WHEN \'' . $type . '\' = \'D\' THEN
					(SELECT RDH.`idDeliveryHour`
					FROM `restaurants-delivery-hours` RDH
					WHERE RDH.`idRestaurant` = R.`idRestaurant`
						AND RDH.`dow` = \'' . date('N', $timestamp) . '\'
						AND RDH.`hourStart` <= \'' . date('H:i:s', $timestamp) . '\'
						AND RDH.`hourEnd` >= \'' . date('H:i:s', $timestamp) . '\'
					LIMIT 0, 1)
				ELSE 
					(SELECT RT.`idTimetable`
					FROM `restaurants-timetable` RT
					WHERE RT.`idRestaurant` = R.`idRestaurant`
						AND RT.`dow` = \'' . date('N', $timestamp) . '\'
						AND RT.`openingHour` <= \'' . date('H:i:s', $timestamp) . '\'
						AND RT.`closingHour` >= \'' . date('H:i:s', $timestamp) . '\'
					LIMIT 0, 1)
				END AS opened'/*', SUM(ROUND(RMI.`price` * ((100 - RMI.`discount`) / 100), 2)) / COUNT(RMI.`idMenuItem`) AS priceAverage'*/ . ', (SELECT AVG(DISTINCT RR1.`rating`)
				FROM `restaurants-ratings` RR1
				WHERE RR1.`idRestaurant` = R.`idRestaurant`) AS rating, (SELECT COUNT(DISTINCT RR2.`idRating`)
				FROM `restaurants-ratings` RR2
				WHERE RR2.`idRestaurant` = R.`idRestaurant`) AS reviews
			FROM `restaurants` R
			INNER JOIN `restaurants-menus` RM ON RM.`idRestaurant` = R.`idRestaurant`
			INNER JOIN `restaurants-menus-items` RMI ON RMI.`idMenu` = RM.`idMenu`
				AND RMI.`enabled` = \'Y\'
			WHERE (R.`customURL` = \'' . mysql_real_escape_string($url) . '\'
				AND R.`dateApproved` IS NOT NULL);';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			$row = mysql_fetch_assoc($result);
			
			$restaurant = new restaurant;
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			
			$restaurant->country = new country;
			$restaurant->country->idCountry = $row['idCountry'];
			
			$restaurant->address = $row['address'];
			$restaurant->city = $row['city'];
			$restaurant->zipCode = $row['zipCode'];
			$restaurant->latitude = $row['latitude'];
			$restaurant->longitude = $row['longitude'];
			$restaurant->website = $row['website'];
			$restaurant->phone = $row['phone'];
			$restaurant->email = $row['email'];
			$restaurant->takeaway = $row['takeaway'];
			$restaurant->delivery = $row['delivery'];
			$restaurant->customDelivery = $row['customDelivery'];
			$restaurant->catering = $row['catering'];
			$restaurant->cashPayment = $row['cashPayment'];
			$restaurant->currency = $row['currency'];
			$restaurant->restaurantDescription = $row['restaurantDescription'];
			$restaurant->customURL = $row['customURL'];
			$restaurant->preparationTime = $row['preparationTime'];
			$restaurant->dateSubmitted = $row['dateSubmitted'];
			
			$restaurant->images = restaurantImage::list_($restaurant);
			$restaurant->timetable = $restaurant->listTimetable();
			
			if ($restaurant->delivery=='Y') {
				// In case of deliveries, let's check the custom delivery schedule records
				// If there's a no-delivery day for the searched date, 
				// we'll just mark the current restaurant as not offering deliveries
				$customDeliveryHours = listRestaurantCustomDeliveryHours($restaurant);
				
				foreach ($customDeliveryHours as $exception) {
					if ($exception['date']==date('Y-m-d', $timestamp)) {
						if ((empty($exception['hourStart']) && empty($exception['hourEnd'])) || 
							(!empty($exception['hourStart']) && strtotime(date('Y-m-d', $timestamp) . ' ' . $exception['hourStart'])>$timestamp) || 
							(!empty($exception['hourEnd']) && strtotime(date('Y-m-d', $timestamp) . ' ' . $exception['hourEnd'])<$timestamp)) {
							
							$restaurant->delivery = 'N';
						}
						
						break;
					}
				}
			}
			if ($restaurant->delivery=='Y') {
				$restaurant->deliveryHours = $restaurant->listDeliveryHours();
			}
			
			$restaurant->menus = restaurantMenu::list_($restaurant, true, false, true);
			$restaurant->cuisineTypes = cuisineType::list_($restaurant);
			$restaurant->deliveryZones = deliveryZone::list_($restaurant);
			
			$restaurant->opened = (!empty($row['opened']));
			$restaurant->priceLevel = ($get_price_level ? $restaurant->getPriceLevel(/*$row['priceAverage']*/) : 1);
			$restaurant->rating = (!empty($row['rating']) ? number_format($row['rating'], 1) : '0');
			$restaurant->reviews = $row['reviews'];
			
			if ($restaurant->opened) {
				// Let's see if the current restaurant isn't closed today
				$restaurant->opened = checkCustomTimetable($restaurant, time());
			}
			
			return $restaurant;
			
		} else {
			return false;
		}
	}
	
	function listRestaurants() {
		global $db;
		
		$query = 'SELECT DISTINCT R.`idRestaurant`, R.`restaurantName`, R.`address`, R.`city`, R.`zipCode`, R.`latitude`, R.`longitude`, R.`website`, R.`phone`, R.`mobile`, R.`customURL`, R.`preparationTime`, (SELECT AVG(DISTINCT RR1.`rating`)
				FROM `restaurants-ratings` RR1
				WHERE RR1.`idRestaurant` = R.`idRestaurant`) AS rating, (SELECT COUNT(DISTINCT RR2.`idRating`)
				FROM `restaurants-ratings` RR2
				WHERE RR2.`idRestaurant` = R.`idRestaurant`) AS reviews 
			FROM `restaurants` R
			INNER JOIN `restaurants-menus` RM ON RM.`idRestaurant` = R.`idRestaurant`
			INNER JOIN `restaurants-menus-items` RMI ON RMI.`idMenu` = RM.`idMenu`
				AND RMI.`enabled` = \'Y\'
			WHERE R.`dateApproved` IS NOT NULL
				AND R.`visible` = \'Y\'
			ORDER BY `dateSubmitted` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$restaurants = array();
		while($row = mysql_fetch_assoc($result)){
			$restaurant = new restaurant;
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			$restaurant->address = $row['address'];
			$restaurant->city = $row['city'];
			$restaurant->zipCode = $row['zipCode'];
			$restaurant->latitude = $row['latitude'];
			$restaurant->longitude = $row['longitude'];
			$restaurant->website = $row['website'];
			$restaurant->phone = $row['phone'];
			$restaurant->customURL = $row['customURL'];
			$restaurant->preparationTime = $row['preparationTime'];
			
			$restaurant->images = restaurantImage::list_($restaurant);
			$restaurant->cuisineTypes = cuisineType::list_($restaurant);
			
			$restaurant->priceLevel = $restaurant->getPriceLevel();
			$restaurant->rating = (!empty($row['rating']) ? number_format($row['rating'], 1) : '0');
			$restaurant->reviews = $row['reviews'];
			
			$restaurants[] = $restaurant;
		}
		
		return $restaurants;
	}
	
	function listRestaurantCustomTimetable($restaurant) {
		global $db;
		
		$query = 'SELECT `idCustomTimetable`, `date`, `openingHour`, `closingHour` 
			FROM `restaurants-custom-timetable` 
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\') 
			ORDER BY `date` ASC, openingHour ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$customTimetable = array();
		while ($row = mysql_fetch_assoc($result)) {
			$customTimetable[] = $row;
		}
		
		return $customTimetable;
	}
	
	function insertCustomTimetable( $resid, $date, $st, $ed ) {
		global $db;
		
		$query = "SELECT * FROM `restaurants-custom-timetable` where `idRestaurant`='" . mysql_real_escape_string($resid) . "' and `date`='" . mysql_real_escape_string($date). "'";
		// $res = mysql_query( $query, $db-link );
		$res = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if ( mysql_num_rows( $res ) > 0 ) return "Already exists";
		
		$query = 'INSERT INTO `restaurants-custom-timetable`
			SET `idRestaurant` = \'' . mysql_real_escape_string($resid) . '\',
			`date` = \'' . mysql_real_escape_string($date) . '\',
			`openingHour` = \''. mysql_real_escape_string($st) . ':00\',
			`closingHour` = \''. mysql_real_escape_string($ed) .':00\'';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		return mysql_insert_id( $db->link );
	}
	
	function deleteCustomTimetable( $cttid ) {
		global $db;
		
		$query = 'DELETE FROM `restaurants-custom-timetable`
			WHERE (`idCustomTimetable` = \'' . mysql_real_escape_string($cttid) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function listRestaurantTimetable($restaurant) {
		global $db;
		
		$customTimetable = listRestaurantCustomTimetable($restaurant);
		
		$query = 'SELECT `dow`, `openingHour`, `closingHour`
			FROM `restaurants-timetable`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')
			ORDER BY dow ASC, openingHour ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$timetable = array();
		while ($row = mysql_fetch_assoc($result)) {
			foreach ($customTimetable as $exception) {
				$timestamp = strtotime($exception['date']);
				$dow = date('N', $timestamp);
				
				if ($timestamp>strtotime('-1 days') && $timestamp<strtotime('+' . (8 - date('N')) . ' days') && $dow==$row['dow']) {
					if ((empty($exception['openingHour']) && empty($exception['closingHour'])) ||
						(!empty($exception['openingHour']) && $exception['openingHour']>$row['closingHour']) || 
						(!empty($exception['closingHour']) && $exception['closingHour']<$row['openingHour'])) {
						
						unset($row);
						
					} else {
						if (!empty($exception['openingHour'])) {
							$row['openingHour'] = $exception['openingHour'];
						}
						if (!empty($exception['closingHour'])) {
							$row['closingHour'] = $exception['closingHour'];
						}
					}
					
					break;
				}
			}
			
			if (!empty($row)) {
				$timetable[] = $row;
		 	}
		}
		
		return $timetable;
	}
	
	function clearRestaurantTimetable($restaurant){
		global $db;
		
		$query = 'DELETE FROM `restaurants-timetable`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function insertRestaurantDeliveryHours($restaurant, $day, $hourStart, $hourEnd){
		global $db;
		
		$query = 'INSERT INTO `restaurants-delivery-hours`
			SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`dow` = \'' . mysql_real_escape_string($day) . '\', 
			`hourStart` = \'' . mysql_real_escape_string($hourStart) . ':00\', 
			`hourEnd` = \'' . mysql_real_escape_string($hourEnd) . ':00\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function listRestaurantCustomDeliveryHours($restaurant) {
		global $db;
		
		$query = 'SELECT `date`, `hourStart`, `hourEnd`
			FROM `restaurants-custom-delivery-hours`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')
			ORDER BY `date` ASC, hourStart ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$customDeliveryHours = array();
		while ($row = mysql_fetch_assoc($result)) {
			$customDeliveryHours[] = $row;
		}
		
		return $customDeliveryHours;
	}
	
	function listRestaurantDeliveryHours($restaurant) {
		global $db;
		
		$customDeliveryHours = listRestaurantCustomDeliveryHours($restaurant);
		
		$query = 'SELECT `dow`, `hourStart`, `hourEnd`
			FROM `restaurants-delivery-hours`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')
			ORDER BY dow ASC, hourStart ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$deliveryHours = array();
		while($row = mysql_fetch_assoc($result)){
			foreach ($customDeliveryHours as $exception) {
				$timestamp = strtotime($exception['date']);
				$dow = date('N', $timestamp);
				
				if ($timestamp>strtotime('-1 days') && $timestamp<strtotime('+' . (8 - date('N')) . ' days') && $dow==$row['dow']) {
					if ((empty($exception['hourStart']) && empty($exception['hourEnd'])) || 
						(!empty($exception['hourStart']) && $exception['hourStart']>$row['hourEnd']) || 
						(!empty($exception['hourEnd']) && $exception['hourEnd']<$row['hourStart'])) {
						
						unset($row);
						
					} else {
						if (!empty($exception['hourStart'])) {
							$row['hourStart'] = $exception['hourStart'];
						}
						if (!empty($exception['hourEnd'])) {
							$row['hourEnd'] = $exception['hourEnd'];
						}
					}
					
					break;
				}
			}
			
			if (!empty($row)) {
				$deliveryHours[] = $row;
		 	}
		}
		
		return $deliveryHours;
	}
	
	function clearRestaurantDeliveryHours($restaurant){
		global $db;
		
		$query = 'DELETE FROM `restaurants-delivery-hours`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function checkRestaurantDuplicateEmail($email){
		global $db;
		
		$query = 'SELECT R.`idRestaurant`, R.`restaurantName`
			FROM `restaurants` R
			INNER JOIN `restaurants-admins` RA ON RA.`idRestaurant` = R.`idRestaurant`
			WHERE (RA.`email` = \'' . mysql_real_escape_string($email) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if(mysql_num_rows($result)>0){
			return true;
		}else{
			return false;
		}
	}
	
	// IT'S NOT PERFECTLY ACCURATE!!!
	// (PROBABLY BECAUSE THERE ARE A FEW `orders-items-options` IN THE 2ND QUERY, AND IT ONLY TAKES INTO CONSIDERATION THE 1ST ONE)
	function getTotalRestaurantSales($restaurant, $startDate, $endDate) {
		global $db;
		/*
		$query = 'SELECT SUM(`itemsValue` + (CASE WHEN `optionsValue` IS NULL THEN 0 ELSE `optionsValue` END)) 
			FROM (
				SELECT (
						SELECT SUM(OI.`quantity` * OI.`pricePerItem`) 
						FROM `orders-items` OI 
						WHERE OI.`idOrder` = O.`idOrder` 
					) AS `itemsValue`, (
						SELECT SUM(OI.`quantity` * OIO.`price`)
						FROM `orders-items` OI 
						INNER JOIN `orders-items-options` OIO ON OIO.`idOrderItem` = OI.`idOrderItem` 
						WHERE OI.`idOrder` = O.`idOrder` 
					) AS `optionsValue` 
				FROM `orders` O 
				WHERE (O.`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\' 
					AND O.`datePreApproved` IS NOT NULL 
					AND O.`dateProcessed` IS NOT NULL 
					AND O.`rejectionReason` IS NULL)
				GROUP BY O.`idOrder`
			) AS X;';*/
		$query = 'SELECT SUM(`value`) AS value, SUM(`deliveryCost`) AS deliveryCost 
			FROM `orders` 
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\' 
				AND `paid` = \'Y\' 
				AND `dateDesired` >= \'' . mysql_real_escape_string($startDate) . '\' 
				AND `dateDesired` <= \'' . mysql_real_escape_string($endDate) . '\' 
				AND `datePreApproved` IS NOT NULL 
				AND `dateProcessed` IS NOT NULL 
				AND (`rejectionReason` IS NULL 
					OR `rejectionReason` = \'\'));';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		return mysql_result($result, 0, 0) + ($restaurant->customDelivery=='Y' ? 0 : mysql_result($result, 0, 1));
	}
	
	function getRestaurantOrdersCount($restaurant) {
		global $db;
		
		$query = 'SELECT COUNT(`idOrder`) 
			FROM `orders` 
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		return mysql_result($result, 0, 0);
	}
	
	function getReviewList( $restaurant_id, $user_id = null ) {
		global $db;
		
		$query = 'SELECT `users`.`firstName`, `users`.`lastName`, `rr`.`rating`, `rr`.`dateAdded`, `rr`.`isOnTime`, `rr`.`isCorrect`, `rr`.`isGood`, `rr`.`review`, `rr`.`idUser`, `rr`.`idRestaurant` 
FROM `restaurants-ratings` as rr
INNER JOIN `users` on `rr`.`idUser` = `users`.`idUser`
where `rr`.`idRestaurant` = \'' . mysql_real_escape_string( $restaurant_id ) . '\'';
		
		if ( $user_id != null ) $query .= " and `rr`.`idUser`='" . mysql_escape_string( $user_id ) . "'";
		
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$res = array();
		while ( $row = mysql_fetch_assoc( $result ) ) {
			$row['firstInitial'] 	= substr( $row['firstName'] , 0, 1 );
			$row['lastInitial'] 	= substr( $row['lastName'] , 0, 1 );
			
			$lastOrder = order::getUserLastOrder( $row['idUser'], $row['idRestaurant'] );
			
			if ( $lastOrder ) $row['lastOrder'] = $lastOrder['dateAdded'];
			else $row['lastOrder'] = "";
			
			array_push( $res, $row );
		}
		
		return $res;
	}
	
	function addReview( $userId, $idRestaurant, $rating, $isOnTime, $isCorrect, $isGood, $review ) {
		global $db;
		
		$query = sprintf( "INSERT INTO `restaurants-ratings`
SET `idRestaurant` = '%d',
`idUser` = '%d',
`rating` = '%d',
`isOnTime` = %s,
`isCorrect` = %s,
`isGood` = %s,
`review` = '%s'", $idRestaurant, $userId, $rating, 
			is_null( $isOnTime )?"null":"'{$isOnTime}'", 
			is_null( $isCorrect )?"null":"'{$isCorrect}'", 
			is_null( $isGood )?"null":"'{$isGood}'", 
			mysql_escape_string($review)
		);
		
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		return true;
	}
