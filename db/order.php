<?php
	function listOrders($entity, $restaurant, $future, $past, $startDate, $endDate) {
		global $db;
		
		$query = 'SELECT O.`idOrder`, 
			U.`idUser`, U.`firstName`, U.`lastName`, U.`phone` AS userPhone, 
			CA.`idCorporateAccount`, CA.`contactFirstName`, CA.`contactLastName`, CA.`phone` AS corporateAccountPhone, 
			UA.`idAddress`, UA.`address`, UA.`buildingNo`, UA.`floorSuite`, UA.`accessCode`, UA.`city`, UA.`zipCode`, UA.`phone` AS addressPhone, 
			R.`idRestaurant`, R.`restaurantName`, R.`address` as restaurant_address, R.`customURL`, R.`preparationTime`, R.`currency`, 
			O.`type`, O.`payment`, O.`transactionID`, (
					SELECT SUM(OI.`quantity` * OI.`pricePerItem`)
					FROM `orders-items` OI 
					WHERE OI.`idOrder` = O.`idOrder`
				) AS `itemsValue`, (
					SELECT SUM(OI.`quantity` * OIO.`price`)
					FROM `orders-items` OI 
					LEFT OUTER JOIN `orders-items-options` OIO ON OIO.`idOrderItem` = OI.`idOrderItem`
					WHERE OI.`idOrder` = O.`idOrder`
				) AS `optionsValue`, O.`deliveryCost`, O.`notes`, ' . /*'DATE_FORMAT(' . */'O.`dateAdded`' . /*', \'%d %b, %H:%i\') AS dateAdded' . */', O.`dateDesired`, O.`dateReviewed`, O.`dateProcessed`, O.`rejectionReason` 
			FROM `orders` O ';
		
		if (!empty($entity)) {
			if (!empty($entity->idUser)) {
				// Only list the orders for the current user
				$query .= '
			INNER JOIN `users` U ON U.`idUser` = O.`idUser`
				AND (U.`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\')
			LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = O.`idCorporateAccount`
				AND CA.`idCorporateAccount` IS NOT NULL';
			} elseif (!empty($entity->idCorporateAccount)) {
				// Only list the orders for the current corporate account
				$query .= '
			LEFT OUTER JOIN `users` U ON U.`idUser` = O.`idUser`
				AND U.`idUser` IS NOT NULL
			INNER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = O.`idCorporateAccount`
				AND (CA.`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\')';
			}
			
		} else {
			$query .= '
			LEFT OUTER JOIN `users` U ON U.`idUser` = O.`idUser`
			LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = O.`idCorporateAccount`';
		}
		
		$query .= '
			LEFT OUTER JOIN `users-addresses` UA ON UA.`idAddress` = O.`idAddress`
			INNER JOIN `restaurants` R ON R.`idRestaurant` = O.`idRestaurant`';
		
		if ($restaurant) {
			// Only list the orders for the current restaurant
			$query .= '
				AND (R.`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')';
		}
		
		if ($future && !$past) {
			$query .= ' 
			WHERE O.`dateDesired` > Now()';
		} elseif ($past && !$future) {
			$query .= ' 
			WHERE (O.`dateDesired` >= \'' . mysql_real_escape_string($startDate) . '\' 
				AND O.`dateDesired` <= \'' . mysql_real_escape_string($endDate) . '\')';
		}
		
		if ($restaurant) {
			// Don't display the un-pre-approved orders
			// (made by corporate accounts using the invoicing option)
			// to restaurants
			$query .= '
				AND O.`datePreApproved` IS NOT NULL';
		}
		
		$query .= ' 
			GROUP BY O.`idOrder` 
			ORDER BY O.`dateDesired` ';
		
		if ($future && !$past) {
			$query .= 'ASC';
		} else if($past && !$future) {
			$query .= 'DESC';
		}
		
		$query .= ';';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$orders = array();
		while ($row = mysql_fetch_assoc($result)) {
			$order = new order;
			$order->idOrder = $row['idOrder'];
			
			if (!empty($row['idUser'])) {
				$order->user = new user;
				$order->user->idUser = $row['idUser'];
				$order->user->firstName = $row['firstName'];
				$order->user->lastName = $row['lastName'];
				$order->user->phone = $row['userPhone'];
				
			} elseif(!empty($row['idCorporateAccount'])) {
				$order->corporateAccount = new corporateAccount;
				$order->corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
				$order->corporateAccount->contactFirstName = $row['contactFirstName'];
				$order->corporateAccount->contactLastName = $row['contactLastName'];
				$order->corporateAccount->phone = $row['corporateAccountPhone'];
			}
			
			$order->userAddress = new userAddress;
			$order->userAddress->idAddress = $row['idAddress'];
			$order->userAddress->address = $row['address'];
			$order->userAddress->buildingNo = $row['buildingNo'];
			$order->userAddress->floorSuite = $row['floorSuite'];
			$order->userAddress->accessCode = $row['accessCode'];
			$order->userAddress->city = $row['city'];
			$order->userAddress->zipCode = $row['zipCode'];
			$order->userAddress->phone = $row['addressPhone'];
			
			$order->restaurant = new restaurant;
			$order->restaurant->idRestaurant = $row['idRestaurant'];
			$order->restaurant->restaurantName = $row['restaurantName'];
			$order->restaurant->address = $row['restaurant_address'];
			$order->restaurant->customURL = $row['customURL'];
			$order->restaurant->preparationTime = $row['preparationTime'];
			$order->restaurant->currency = $row['currency'];
			
			$order->type = $row['type'];
			$order->payment = $row['payment'];
			$order->transactionID = $row['transactionID'];
			$order->value = ($row['itemsValue'] + $row['optionsValue']);
			$order->deliveryCost = $row['deliveryCost'];
			$order->notes = $row['notes'];
			$order->dateAdded = $row['dateAdded'];
			$order->dateDesired = $row['dateDesired'];
			$order->dateReviewed = $row['dateReviewed'];
			$order->dateProcessed = $row['dateProcessed'];
			$order->rejectionReason = $row['rejectionReason'];
			
			$order->items = orderItem::list_($order);
			
			$orders[] = $order;
		}
		
		return $orders;
	}
	
	function insertOrder($entity, $userAddress, $restaurant, $type, $payment, $transactionID, $value, $deliveryCost, $exchangeRate, $notes, $dateDesired){
		global $db;
		
		$query = 'INSERT INTO `orders`
			SET ';
		
		if (!empty($entity->idUser)) {
			$query .= '`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
			
		} elseif(!empty($entity->idCorporateAccount)) {
			
			$query .= '`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
		}
		
		$query .= ', 
			`idAddress` = ' . (!empty($userAddress) && !empty($userAddress->idAddress) ? '\'' . mysql_real_escape_string($userAddress->idAddress) . '\'' : 'NULL') . ', 
			`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`type` = \'' . mysql_real_escape_string($type) . '\', 
			`payment` = \'' . mysql_real_escape_string($payment) . '\', 
			`transactionID` = ' . (!empty($transactionID) ? '\'' . mysql_real_escape_string($transactionID) . '\'' : 'NULL') . ', 
			`value` = \'' . mysql_real_escape_string($value) . '\'';
		
		if ($deliveryCost) {
			$query .= ', 
			`deliveryCost` = \'' . mysql_real_escape_string($deliveryCost) . '\'';
		}
		
		if ($exchangeRate) {
			$query .= ', 
			`idExchangeRate` = \'' . mysql_real_escape_string($exchangeRate->idExchangeRate) . '\'';
		}
		
		$query .= ', 
			`notes` = ' . (!empty($notes) ? '\'' . mysql_real_escape_string($notes) . '\'' : 'NULL') . ', 
			`dateDesired` = \'' . mysql_real_escape_string($dateDesired) . '\'';
		
		if (in_array($payment, array('cc', 'lunch-check'))) {
			// Only set the payment date for CC- or LC-paid orders
			$query .= ', 
			`paid` = \'Y\'';
		}
		
		if ($payment!='invoice') {
			// This must be a regular order (and NOT an invoiced corporate one), so let's set the pre-approved date value to the current date
			// (since regular orders don't require a pre-approval, being paid and all...)
			$query .= ', 
			`datePreApproved` = Now()';
		}
		
		$query .= ';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$order = new order;
		$order->idOrder = mysql_insert_id($db->link);
		
		if (!empty($entity->idUser)) {
			$order->user = $entity;
			
		} elseif(!empty($entity->idCorporateAccount)) {
			$order->corporateAccount = $entity;
		}
		
		$order->userAddress = $userAddress;
		$order->restaurant = $restaurant;
		$order->type = $type;
		$order->payment = $payment;
		$order->transactionID = $transactionID;
		$order->value = $value;
		if ($deliveryCost) {
			$order->deliveryCost = $deliveryCost;
		}
		$order->exchangeRate = $exchangeRate;
		$order->notes = $notes;
		$order->dateDesired = $dateDesired;
		
		return $order;
	}
	
	function getOrderByID($idOrder) {
		global $db;
		
		$query = 'SELECT O.`idOrder`, U.`idUser`, U.`firstName`, U.`lastName`, U.`email`, U.`phone` AS userPhone, CA.`idCorporateAccount`, CA.`contactFirstName`, CA.`contactLastName`, CA.`address` AS corporateAddress, CA.`city` AS corporateCity, CA.`zipCode` AS corporateZipCode, CA.`companyName`, CA.`phone` AS corporatePhone, CA.`emails`, 
	UA.`idAddress`, UA.`address`, UA.`buildingNo`, UA.`floorSuite`, UA.`accessCode`, UA.`city`, UA.`zipCode`, UA.`latitude`, UA.`longitude`, UA.`phone`, 
	R.`idRestaurant`, R.`restaurantName`, R.`address` AS restaurantAddress, R.`city` AS restaurantCity, R.`zipCode` AS restaurantZipCode, R.`phone` AS restaurantPhone, R.`email` AS restaurantEmail, R.`customDelivery`, R.`currency`, R.`preparationTime`, R.`spatchoID`, R.`spatchoID2`, 
	O.`type`, O.`payment`, O.`transactionID`, O.`value`, O.`deliveryCost`, O.`notes`, O.`dateAdded`, O.`dateDesired`, O.`datePreApproved`, O.`dateReviewed`, O.`dateProcessed`, O.`rejectionReason`
FROM `orders` O
LEFT OUTER JOIN `users` U ON U.`idUser` = O.`idUser`
LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = O.`idCorporateAccount`
LEFT OUTER JOIN `users-addresses` UA ON UA.`idAddress` = O.`idAddress`
INNER JOIN `restaurants` R ON R.`idRestaurant` = O.`idRestaurant`
WHERE (O.`idOrder` = \'' . mysql_real_escape_string($idOrder) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			$row = mysql_fetch_assoc($result);
			
			$order = new order;
			$order->idOrder = $row['idOrder'];
			
			if (!empty($row['idUser'])) {
				$order->user = new user;
				$order->user->idUser = $row['idUser'];
				$order->user->firstName = $row['firstName'];
				$order->user->lastName = $row['lastName'];
				$order->user->email = $row['email'];
				$order->user->phone = $row['userPhone'];
				
			} elseif (!empty($row['idCorporateAccount'])) {
				$order->corporateAccount = new corporateAccount;
				$order->corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
				$order->corporateAccount->contactFirstName = $row['contactFirstName'];
				$order->corporateAccount->contactLastName = $row['contactLastName'];
				$order->corporateAccount->address = $row['corporateAddress'];
				$order->corporateAccount->city = $row['corporateCity'];
				$order->corporateAccount->zipCode = $row['corporateZipCode'];
				$order->corporateAccount->companyName = $row['companyName'];
				$order->corporateAccount->phone = $row['corporatePhone'];
				$order->corporateAccount->emails = explode(',', $row['emails']);
			}
			
			$order->userAddress = new userAddress;
			$order->userAddress->idAddress = $row['idAddress'];
			$order->userAddress->address = $row['address'];
			$order->userAddress->buildingNo = $row['buildingNo'];
			$order->userAddress->floorSuite = $row['floorSuite'];
			$order->userAddress->accessCode = $row['accessCode'];
			$order->userAddress->city = $row['city'];
			$order->userAddress->zipCode = $row['zipCode'];
			$order->userAddress->latitude = $row['latitude'];
			$order->userAddress->longitude = $row['longitude'];
			$order->userAddress->phone = $row['phone'];
			
			$order->restaurant = new restaurant;
			$order->restaurant->idRestaurant = $row['idRestaurant'];
			$order->restaurant->restaurantName = $row['restaurantName'];
			$order->restaurant->address = $row['restaurantAddress'];
			$order->restaurant->city = $row['restaurantCity'];
			$order->restaurant->zipCode = $row['restaurantZipCode'];
			$order->restaurant->phone = $row['restaurantPhone'];
			$order->restaurant->email = $row['restaurantEmail'];
			$order->restaurant->customDelivery = $row['customDelivery'];
			$order->restaurant->currency = $row['currency'];
			$order->restaurant->preparationTime = $row['preparationTime'];
			$order->restaurant->spatchoID = $row['spatchoID'];
			$order->restaurant->spatchoID2 = $row['spatchoID2'];
			$order->restaurant->deliveryZones = deliveryZone::list_($order->restaurant);
			
			$order->type = $row['type'];
			$order->payment = $row['payment'];
			$order->transactionID = $row['transactionID'];
			$order->value = $row['value'];
			$order->deliveryCost = $row['deliveryCost'];
			$order->notes = $row['notes'];
			$order->dateAdded = $row['dateAdded'];
			$order->dateDesired = $row['dateDesired'];
			$order->datePreApproved = $row['datePreApproved'];
			$order->dateReviewed = $row['dateReviewed'];
			$order->dateProcessed = $row['dateProcessed'];
			$order->rejectionReason = $row['rejectionReason'];
			
			$order->items = orderItem::list_($order);
			$order->emailInvitation = emailInvitation::getByOrder($order);
			$order->promoCode = promoCode::getByOrder($order);
			
			return $order;
			
		} else {
			return false;
		}
	}
	
	function reviewOrder($order){
		global $db;
		
		$query = 'UPDATE `orders`
			SET `dateReviewed` = Now()
			WHERE (`idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function preApproveOrder($order){
		global $db;
		
		$query = 'UPDATE `orders`
			SET `datePreApproved` = Now()
			WHERE (`idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function acceptOrder($order) {
		global $db;
		
		$query = 'UPDATE `orders` 
			SET `dateProcessed` = Now() 
			WHERE (`idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}

	function declineOrder($order, $rejectionReason) {
		global $db;

		$query = 'UPDATE `orders`
			SET `dateProcessed` = Now(), 
			`rejectionReason` = \'' . (!empty($rejectionReason) ? mysql_real_escape_string($rejectionReason) : '-') . '\'
			WHERE (`idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}

	function getForDatetime($restaurant, $datetime) {
		global $db;

		$query = 'SELECT COUNT(`idOrder`) AS orders 
			FROM `orders` 
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\'
				AND `dateDesired` > DATE_SUB(\'' . date('Y-m-d H:i:s', strtotime($datetime)) . '\', INTERVAL 30 MINUTE)
				AND `dateDesired` < DATE_ADD(\'' . date('Y-m-d H:i:s', strtotime($datetime)) . '\', INTERVAL 30 MINUTE));';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');

		return mysql_result($result, 0, 'orders');
	}

	function setOrderPaymentStatus($order, $payment){
		global $db;

		$query = 'UPDATE `orders`
			SET `paid` = \'' . mysql_real_escape_string($payment) . '\'
			WHERE (`idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}

	function getUserLastOrder( $userId, $restaurantId ) {
		global $db;

		$query = sprintf( "SELECT * FROM `orders` where `idUser`='%d' and `idRestaurant`='%d' and `paid`='Y' order by `dateAdded` desc",
			mysql_real_escape_string($userId),
			mysql_real_escape_string($restaurantId) );

		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');

		$row = mysql_fetch_assoc( $result );

		if ( $row ) {
			return $row;
		}

		return null;
	}