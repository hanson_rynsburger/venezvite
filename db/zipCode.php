<?php
    function getZipCode($zipCode_){
        global $db;
        
        $query = 'SELECT `idZipCode`, `latitude`, `longitude`
            FROM `zip-codes`
            WHERE (`zipCode` = \'' . mysql_real_escape_string($zipCode_) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $zipCode = new zipCode;
            
            $zipCode->idZipCode = mysql_result($result, 0, 'idZipCode');
            $zipCode->zipCode = $zipCode_;
            $zipCode->latitude = mysql_result($result, 0, 'latitude');
            $zipCode->longitude = mysql_result($result, 0, 'longitude');
            
            return $zipCode;
        }else{
            return false;
        }
    }
    
    function insertZipCode($zipCode_, $latitude, $longitude){
        global $db;
        
        $query = 'INSERT INTO `zip-codes`
            SET `zipCode` = \'' . mysql_real_escape_string($zipCode_) . '\', 
            `latitude` = \'' . mysql_real_escape_string($latitude) . '\', 
            `longitude` = \'' . mysql_real_escape_string($longitude) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $zipCode = new zipCode;
        $zipCode->idZipCode = mysql_insert_id($db->link);
        $zipCode->zipCode = $zipCode_;
        $zipCode->latitude = $latitude;
        $zipCode->longitude = $longitude;
        
        return $zipCode;
    }
?>