<?php
	function listMenuItemOptions($restaurant){
		global $db;
		
		$query = 'SELECT `idMenuItemOption`, `menuItemOption`, `price`
			FROM `restaurants-menus-items-options`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')
			ORDER BY `menuItemOption` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$menuItemOptions = array();
		while($row = mysql_fetch_assoc($result)){
			$menuItemOption = new menuItemOption;
			$menuItemOption->idMenuItemOption = $row['idMenuItemOption'];
			$menuItemOption->menuItemOption = $row['menuItemOption'];
			$menuItemOption->price = $row['price'];
			
			$menuItemOptions[] = $menuItemOption;
		}
		
		return $menuItemOptions;
	}
	
	function insertMenuItemOption($restaurant, $menuItemOption, $price){
		global $db;
		
		$query = 'INSERT INTO `restaurants-menus-items-options`
			SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`menuItemOption` = \'' . mysql_real_escape_string($menuItemOption) . '\', 
			`price` = \'' . mysql_real_escape_string($price) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$menuItemOption_ = new menuItemOption;
		$menuItemOption_->idMenuItemOption = mysql_insert_id($db->link);
		$menuItemOption_->menuItemOption = $menuItemOption;
		$menuItemOption_->price = $price;
		
		return $menuItemOption_;
	}
	
	function getMenuItemOptionByID($idMenuItemOption){
		global $db;
		
		$query = 'SELECT `idMenuItemOption`, `idRestaurant`, `menuItemOption`, `price`
			FROM `restaurants-menus-items-options`
			WHERE (`idMenuItemOption` = \'' . mysql_real_escape_string($idMenuItemOption) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			$row = mysql_fetch_assoc($result);
			
			$menuItemOption = new menuItemOption;
			$menuItemOption->idMenuItemOption = $row['idMenuItemOption'];
			
			$menuItemOption->restaurant = new restaurant;
			$menuItemOption->restaurant->idRestaurant = $row['idRestaurant'];
			
			$menuItemOption->menuItemOption = $row['menuItemOption'];
			$menuItemOption->price = $row['price'];
			
			return $menuItemOption;
		}else{
			return false;
		}
	}
	
	function listOptionsByMenuItemCategory($menuItemCategory){
		global $db;
		
		$query = 'SELECT DISTINCT RMIO.`idMenuItemOption`, RMIO.`menuItemOption`, RMIO.`price`
			FROM `restaurants-menus-items-options` RMIO
			INNER JOIN `restaurants-menus-items-categories-options` RMICO ON RMICO.`idMenuItemOption` = RMIO.`idMenuItemOption` 
				AND (RMICO.`idMenuItemCategory` = \'' . mysql_real_escape_string($menuItemCategory->idMenuItemCategory) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$menuItemOptions = array();
		while($row = mysql_fetch_assoc($result)){
			$menuItemOption = new menuItemOption;
			$menuItemOption->idMenuItemOption = $row['idMenuItemOption'];
			$menuItemOption->menuItemOption = $row['menuItemOption'];
			$menuItemOption->price = $row['price'];
			
			$menuItemOptions[] = $menuItemOption;
		}
		
		return $menuItemOptions;
	}
	
	function insertMenuItemOptionPerCategory($idMenuItemOption, $menuItemOptionCategory){
		global $db;
		
		$query = 'INSERT INTO `restaurants-menus-items-categories-options`
			SET `idMenuItemCategory` = \'' . mysql_real_escape_string($menuItemOptionCategory->idMenuItemCategory) . '\', 
			`idMenuItemOption` = \'' . mysql_real_escape_string($idMenuItemOption) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		/*
		$menuItemOption_ = new menuItemOption;
		$menuItemOption_->idMenuItemOption = mysql_insert_id($db->link);
		$menuItemOption_->menuItemOption = $menuItemOption;
		$menuItemOption_->price = $price;
		
		return $menuItemOption_;
		*/
	}
	
	function update_menu_item_option($menu_item_option, $data) {
		global $db;
		
		$conditions = array();
		foreach($data as $column => $value) {
			$conditions[] = '`' . $column . '` = \'' . mysql_real_escape_string($value) . '\'';
		}
		
		$query = 'UPDATE `restaurants-menus-items-options` 
			SET ' . implode(", \n", $conditions) . ' 
			WHERE (`idMenuItemOption` = \'' . mysql_real_escape_string($menu_item_option->idMenuItemOption) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function delete_menu_item_option($menu_item_option) {
		global $db;
		
		$query = 'DELETE FROM `restaurants-menus-items-options`
			WHERE (`idMenuItemOption` = \'' . mysql_real_escape_string($menu_item_option->idMenuItemOption) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
