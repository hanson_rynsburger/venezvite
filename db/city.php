<?php
    function searchCities($city, $country){
        global $db;
        
        $query = '(SELECT CI.`idCity`, CO.`idCountry`, CO.`countryName`, CO.`countryAcronym`, CI.`cityName`, CI.`latitude`, CI.`longitude`
            FROM `cities` CI
            INNER JOIN `countries` CO ON CO.`idCountry` = CI.`idCountry`';
        
        if($country){
            // So you would only search for cities names within a certain country
            $query .= '
                AND CO.`idCountry` = \'' . mysql_real_escape_string($country->idCountry) . '\'';
        }
        
        $query .= '
            WHERE (UPPER(CI.`cityName`) = \'' . mysql_real_escape_string(strtoupper($city)) . '\')
            ORDER BY `cityName`)
            UNION
            (SELECT CI.`idCity`, CO.`idCountry`, CO.`countryName`, CO.`countryAcronym`, CI.`cityName`, CI.`latitude`, CI.`longitude`
            FROM `cities` CI
            INNER JOIN `countries` CO ON CO.`idCountry` = CI.`idCountry`';
        
        if($country){
            // So you would only search for cities names within a certain country
            $query .= '
                AND CO.`idCountry` = \'' . mysql_real_escape_string($country->idCountry) . '\'';
        }
        
        $query .= '
            WHERE (UPPER(CI.`cityName`) LIKE \'%' . mysql_real_escape_string(strtoupper($city)) . '%\'
                AND UPPER(CI.`cityName`) <> \'' . mysql_real_escape_string(strtoupper($city)) . '\')
            ORDER BY `cityName`)
            LIMIT 0, 30;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $cities = array();
        while($row = mysql_fetch_assoc($result)){
            $city = new city;
            $city->idCity = $row['idCity'];
            
            $city->country = new country;
            $city->country->idCountry = $row['idCountry'];
            $city->country->countryName = $row['countryName'];
            $city->country->countryAcronym = $row['countryAcronym'];
            
            $city->cityName = $row['cityName'];
            $city->latitude = $row['latitude'];
            $city->longitude = $row['longitude'];
            
            $cities[] = $city;
        }
        
        return $cities;
    }
    
    function getCityByID($idCity){
        global $db;
        
        $query = 'SELECT CI.`idCity`, CO.`idCountry`, CO.`countryName`, CO.`countryAcronym`, CI.`cityName`, CI.`latitude`, CI.`longitude`
            FROM `cities` CI
            INNER JOIN `countries` CO ON CO.`idCountry` = CI.`idCountry`
            WHERE (CI.`idCity` = \'' . mysql_real_escape_string($idCity) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $row = mysql_fetch_assoc($result);
            
            $city = new city;
            $city->idCity = $row['idCity'];
            
            $city->country = new country;
            $city->country->idCountry = $row['idCountry'];
            $city->country->countryName = $row['countryName'];
            $city->country->countryAcronym = $row['countryAcronym'];
            
            $city->cityName = $row['cityName'];
            $city->latitude = $row['latitude'];
            $city->longitude = $row['longitude'];
            
            return $city;
        }else{
            return false;
        }
    }
?>