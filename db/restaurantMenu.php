<?php
    function restaurantMenuCreate($restaurant, $menuName){
        global $db;
        
        $query = 'SET @order = (SELECT COUNT(`idMenu`)
                FROM `restaurants-menus`
                WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\'));';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $query = 'INSERT INTO `restaurants-menus`
            SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
            `menuName` = \'' . mysql_real_escape_string($menuName) . '\', 
            `order` = (@order + 1);';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $restaurantMenu = new restaurantMenu;
        $restaurantMenu->idMenu = mysql_insert_id($db->link);
        $restaurantMenu->menuName = $menuName;
    }
    
    function listRestaurantMenus($restaurant, $items, $allItems, $options, $sort) {
        global $db;
        
        $query = 'SELECT `idMenu`, `menuName`
            FROM `restaurants-menus`
            WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')
            ORDER BY `order` ASC;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $restaurantMenus = array();
        while($row = mysql_fetch_assoc($result)){
            $restaurantMenu = new restaurantMenu;
            $restaurantMenu->idMenu = $row['idMenu'];
            $restaurantMenu->menuName = $row['menuName'];
            
            if ($items) {
                $restaurantMenu->menuItems = menuItem::list_($restaurant, $restaurantMenu, $allItems, $options, $sort);
            }
            
            $restaurantMenus[] = $restaurantMenu;
        }
        
        return $restaurantMenus;
    }
