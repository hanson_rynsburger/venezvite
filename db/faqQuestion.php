<?php
    function listFaqQuestions($faqCategory){
        global $db;
        
        $query = 'SELECT `idQuestion`, `question`, `answer`
            FROM `faq-questions`';
        
        if($faqCategory){
            $query .= '
            WHERE (`idCategory` = \'' . mysql_real_escape_string($faqCategory->idCategory) . '\')';
        }
        
        $query .= '
            ORDER BY `order`;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $faqQuestions = array();
        while($row = mysql_fetch_assoc($result)){
            $faqQuestion = new faqQuestion;
            $faqQuestion->idQuestion = $row['idQuestion'];
            $faqQuestion->question = $row['question'];
            $faqQuestion->answer = $row['answer'];
            
            $faqQuestions[] = $faqQuestion;
        }
        
        return $faqQuestions;
    }
?>