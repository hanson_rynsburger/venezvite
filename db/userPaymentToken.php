<?php
	function listUserPaymentTokens($entity){
		global $db;
		
		$query = 'SELECT UPT.`id`, 
				U.`idUser`, U.`firstName`, U.`lastName`, 
				CA.`idCorporateAccount`, CA.`contactFirstName`, CA.`contactLastName`, CA.`companyName`, 
				UPT.`cardholder_name`, UPT.`token`, UPT.`card_type`, UPT.`expiration_date`, UPT.`date_added`, UPT.`default` 
			FROM `users-payment-tokens` UPT';
		
		if (!empty($entity)) {
			if (!empty($entity->idUser)) {
				$query .= ' 
			INNER JOIN `users` U ON U.`idUser` = UPT.`user_id` 
				AND (U.`idUser` = \'' . mysql_real_escape_string($entity->idUser) . '\') 
			LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = UPT.`corporate_account_id` 
				AND CA.`idCorporateAccount` IS NOT NULL';
				
			} elseif (!empty($entity->idCorporateAccount)) {
				$query .= ' 
			LEFT OUTER JOIN `users` U ON U.`idUser` = UPT.`user_id` 
				AND U.`idUser` IS NOT NULL 
			INNER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = UPT.`corporate_account_id` 
				AND (CA.`idCorporateAccount` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\')';
			}
			
		} else {
			$query .= '
			LEFT OUTER JOIN `users` U ON U.`idUser` = UPT.`user_id` 
			LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = UPT.`corporate_account_id`';
		}
		
		$query .= '
			ORDER BY `default` ASC, `date_added` DESC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$userPaymentTokens = array();
		while($row = mysql_fetch_assoc($result)){
			$userPaymentToken = new userPaymentToken;
			$userPaymentToken->id = $row['id'];
			
			if (!empty($row['idUser'])) {
				$userPaymentToken->user = new user;
				$userPaymentToken->user->idUser = $row['idUser'];
				$userPaymentToken->user->firstName = $row['firstName'];
				$userPaymentToken->user->lastName = $row['lastName'];
				
			} elseif (!empty($row['idCorporateAccount'])) {
				$userPaymentToken->corporateAccount = new corporateAccount;
				$userPaymentToken->corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
				$userPaymentToken->corporateAccount->contactFirstName = $row['contactFirstName'];
				$userPaymentToken->corporateAccount->contactLastName = $row['contactLastName'];
				$userPaymentToken->corporateAccount->companyName = $row['companyName'];
			}
			
			$userPaymentToken->cardholder_name = $row['cardholder_name'];
			$userPaymentToken->token = $row['token'];
			$userPaymentToken->card_type = $row['card_type'];
			$userPaymentToken->expiration_date = $row['expiration_date'];
			$userPaymentToken->date_added = $row['date_added'];
			$userPaymentToken->default = $row['default'];
			
			$userPaymentTokens[] = $userPaymentToken;
		}
		
		return $userPaymentTokens;
	}
	
	function insertUserPaymentToken($entity, $data) {
		global $db;
		
		if ($data['default']=='Y') {
			$query = 'UPDATE `users-payment-tokens`
				SET `default` = \'N\'
				WHERE (';
			
			if (!empty($entity->idUser)) {
				$query .= '`user_id` = \'' . mysql_real_escape_string($entity->idUser) . '\'';
				
			} elseif (!empty($entity->idCorporateAccount)) {
				$query .= '`corporate_account_id` = \'' . mysql_real_escape_string($entity->idCorporateAccount) . '\'';
			}
			
			$query .= ');';
			mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		}
		
		
		$conditions = array();
		foreach($data as $column => $value) {
			$conditions[] = '`' . $column . '` = \'' . mysql_real_escape_string($value) . '\'';
		}
		
		$query = 'INSERT INTO `users-payment-tokens` 
			SET ' . implode(", \n", $conditions) . ';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$userPaymentToken = new userPaymentToken;
		$userPaymentToken->id = mysql_insert_id($db->link);
		
		return $userPaymentToken;
	}
	
	function getUserPaymentTokenByID($id) {
		global $db;
		
		$query = 'SELECT UPT.`id`, 
				U.`idUser`, U.`firstName`, U.`lastName`, 
				CA.`idCorporateAccount`, CA.`contactFirstName`, CA.`contactLastName`, CA.`companyName`, 
				UPT.`cardholder_name`, UPT.`token`, UPT.`card_type`, UPT.`expiration_date`, UPT.`date_added`, UPT.`default` 
			FROM `users-payment-tokens` UPT 
			LEFT OUTER JOIN `users` U ON U.`idUser` = UPT.`user_id` 
			LEFT OUTER JOIN `corporate-accounts` CA ON CA.`idCorporateAccount` = UPT.`corporate_account_id` 
			WHERE (UPT.`id` = \'' . mysql_real_escape_string($id) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			$row = mysql_fetch_assoc($result);
			
			$userPaymentToken = new userPaymentToken;
			$userPaymentToken->id = $row['id'];
			
			if (!empty($row['idUser'])) {
				$userPaymentToken->user = new user;
				$userPaymentToken->user->idUser = $row['idUser'];
				$userPaymentToken->user->firstName = $row['firstName'];
				$userPaymentToken->user->lastName = $row['lastName'];
				
			} elseif (!empty($row['idCorporateAccount'])) {
				$userPaymentToken->corporateAccount = new corporateAccount;
				$userPaymentToken->corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
				$userPaymentToken->corporateAccount->contactFirstName = $row['contactFirstName'];
				$userPaymentToken->corporateAccount->contactLastName = $row['contactLastName'];
			}
			
			$userPaymentToken->cardholder_name = $row['cardholder_name'];
			$userPaymentToken->token = $row['token'];
			$userPaymentToken->card_type = $row['card_type'];
			$userPaymentToken->expiration_date = $row['expiration_date'];
			$userPaymentToken->date_added = $row['date_added'];
			$userPaymentToken->default = $row['default'];
			
			return $userPaymentToken;
			
		} else {
			return false;
		}
	}
	
	function removeUserPaymentToken($userPaymentToken) {
		global $db;
		
		$query = 'DELETE FROM `users-payment-tokens`
			WHERE (`id` = \'' . mysql_real_escape_string($userPaymentToken->id) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	/*
	function updateUserPaymentToken($userPaymentToken) {
		global $db;
		
		if($default=='Y'){
			$query = 'UPDATE `users-payment-tokens`
				SET `default` = \'N\'
				WHERE (';
			
			if (!empty($userPaymentToken->user)) {
				$query .= '`user_id` = \'' . mysql_real_escape_string($userPaymentToken->user->idUser) . '\'';
				
			} elseif (!empty($userPaymentToken->corporateAccount)) {
				$query .= '`corporate_account_id` = \'' . mysql_real_escape_string($userPaymentToken->corporateAccount->idCorporateAccount) . '\'';
			}
			
			$query .= ');';
			mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		}
		
		$query = 'UPDATE `users-payment-tokens`
			SET `address` = \'' . mysql_real_escape_string($address) . '\', 
			`buildingNo` = \'' . mysql_real_escape_string($buildingNo) . '\', 
			`floorSuite` = ' . (!empty($floorSuite) ? '\'' . mysql_real_escape_string($floorSuite) . '\'' : 'NULL') . ', 
			`accessCode` = ' . (!empty($accessCode) ? '\'' . mysql_real_escape_string($accessCode) . '\'' : 'NULL') . ', 
			`city` = \'' . mysql_real_escape_string($city) . '\', 
			`zipCode` = \'' . mysql_real_escape_string($zipCode) . '\', 
			`latitude` = \'' . mysql_real_escape_string($latitude) . '\', 
			`longitude` = \'' . mysql_real_escape_string($longitude) . '\', 
			`phone` = \'' . mysql_real_escape_string($phone) . '\', 
			`default` = \'' . mysql_real_escape_string($default) . '\', 
			`instructions` = \'' . mysql_real_escape_string($instructions) . '\'
			WHERE (`idAddress` = \'' . mysql_real_escape_string($userPaymentToken->idAddress) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	*/
