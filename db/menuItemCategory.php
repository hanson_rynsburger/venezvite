<?php
	function listMenuItemCategories($restaurant) {
		global $db;
		
		$query = 'SELECT MIC.`menuItemCategory`, MIC.`idMenuItemCategory`, MIC.`selectables`, MIC.`selectablesRule` 
			FROM `menus-items-categories` MIC 
			INNER JOIN `restaurants-menus-items` RMI ON RMI.`idMenuItem` = MIC.`idMenuItem` 
			INNER JOIN `restaurants-menus` RM ON RM.`idMenu` = RMI.`idMenu` 
				AND (RM.`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\') 
			GROUP BY MIC.`menuItemCategory` 
			ORDER BY MIC.`menuItemCategory` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$menuItemCategories = array();
		while($row = mysql_fetch_assoc($result)){
			$menuItemCategory = new menuItemCategory;
			$menuItemCategory->idMenuItemCategory = $row['idMenuItemCategory'];
			
			$menuItemCategory->menuItemCategory = $row['menuItemCategory'];
			$menuItemCategory->selectables = $row['selectables'];
			$menuItemCategory->selectablesRule = $row['selectablesRule'];
			
			$menuItemCategories[] = $menuItemCategory;
		}
		
		return $menuItemCategories;
	}
	
	function list_menu_item_categories($restaurant, $load_options) {
		global $db;
		
		$query = 'SELECT `idMenuItemCategory`, `menuItemCategory`, `selectables`, `selectablesRule`, `menuItemOptionIDs` 
			FROM `restaurants-menus-items-categories` 
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\') 
			ORDER BY `order` ASC, `menuItemCategory` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$menu_item_categories = array();
		while($row = mysql_fetch_assoc($result)){
			$menu_item_category = new menuItemCategory;
			$menu_item_category->idMenuItemCategory = $row['idMenuItemCategory'];
			
			$menu_item_category->restaurant = new restaurant;
			$menu_item_category->restaurant->idRestaurant = $restaurant->idRestaurant;
			
			$menu_item_category->menuItemCategory = $row['menuItemCategory'];
			$menu_item_category->selectables = $row['selectables'];
			$menu_item_category->selectablesRule = $row['selectablesRule'];
			
			if ($load_options) {
				$option_ids = @json_decode($row['menuItemOptionIDs']);
				
				foreach ($option_ids as $option_id) {
					if (($menu_item_option = menuItemOption::getByID($option_id))) {
						$menu_item_category->menuItemOptions[] = $menu_item_option;
					}
				}
				
			} else {
				$menu_item_category->menuItemOptions = @json_decode($row['menuItemOptionIDs']);
			}
			
			$menu_item_categories[] = $menu_item_category;
		}
		
		return $menu_item_categories;
	}
	
	function insertMenuItemCategory($menuItem, $menuItemCategory, $selectables, $selectablesRule){
		global $db;
		
		$query = 'INSERT INTO `menus-items-categories` 
			SET `idMenuItem` = ' . ($menuItem ? '\'' . mysql_real_escape_string($menuItem->idMenuItem) . '\'' : 'NULL') . ', 
			`menuItemCategory` = \'' . mysql_real_escape_string($menuItemCategory) . '\', 
			`selectables` = \'' . mysql_real_escape_string($selectables) . '\', 
			`selectablesRule` = \'' . mysql_real_escape_string($selectablesRule) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$menuItemCategory_ = new menuItemCategory;
		$menuItemCategory_->idMenuItemCategory = mysql_insert_id($db->link);
		$menuItemCategory_->menuItemCategory = $menuItemCategory;
		$menuItemCategory_->selectables = $selectables;
		$menuItemCategory_->selectablesRule = $selectablesRule;
		
		return $menuItemCategory_;
	}
	
	function getMenuItemCategoryByID($idMenuItemCategory){
		global $db;
		
		$query = 'SELECT MIC.`idMenuItemCategory`, RMI.`idMenuItem`, RM.`idRestaurant`, MIC.`menuItemCategory`, MIC.`selectables`, MIC.`selectablesRule` 
			FROM `menus-items-categories` MIC 
			LEFT OUTER JOIN `restaurants-menus-items` RMI ON RMI.`idMenuItem` = MIC.`idMenuItem` 
			LEFT OUTER JOIN `restaurants-menus` RM ON RM.`idMenu` = RMI.`idMenu` 
			WHERE (MIC.`idMenuItemCategory` = \'' . mysql_real_escape_string($idMenuItemCategory) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			$row = mysql_fetch_assoc($result);
			
			$menuItemCategory = new menuItemCategory;
			$menuItemCategory->idMenuItemCategory = $row['idMenuItemCategory'];
			
			if($row['idMenuItem']){
				$menuItemCategory->menuItem = new menuItem;
				$menuItemCategory->menuItem->idMenuItem = $row['idMenuItem'];
				
				$menuItemCategory->menuItem->menu = new restaurantMenu;
				//$menuItemCategory->menuItem->menu->idMenu = $row['idMenu'];
				
				$menuItemCategory->menuItem->menu->restaurant = new restaurant;
				$menuItemCategory->menuItem->menu->restaurant->idRestaurant = $row['idRestaurant'];
			}
			
			$menuItemCategory->menuItemCategory = $row['menuItemCategory'];
			$menuItemCategory->selectables = $row['selectables'];
			$menuItemCategory->selectablesRule = $row['selectablesRule'];
			
			$menuItemCategory->menuItemOptions = menuItemOption::listByMenuItemCategory($menuItemCategory);
			
			return $menuItemCategory;
		}else{
			return false;
		}
	}
	
	function get_menu_item_category_by_id($id) {
		global $db;
		
		$query = 'SELECT `idMenuItemCategory`, `idRestaurant`, `menuItemCategory`, `selectables`, `selectablesRule`, `menuItemOptionIDs`, `order` 
			FROM `restaurants-menus-items-categories` 
			WHERE (`idMenuItemCategory` = \'' . mysql_real_escape_string($id) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			$row = mysql_fetch_assoc($result);
			
			$menu_item_category = new menuItemCategory;
			$menu_item_category->idMenuItemCategory = $row['idMenuItemCategory'];
			
			$menu_item_category->restaurant = new restaurant;
			$menu_item_category->restaurant->idRestaurant = $row['idRestaurant'];
			
			$menu_item_category->menuItemCategory = $row['menuItemCategory'];
			$menu_item_category->selectables = $row['selectables'];
			$menu_item_category->selectablesRule = $row['selectablesRule'];
			$menu_item_category->order = $row['order'];
			
			if (($option_ids = @json_decode($row['menuItemOptionIDs']))) {
				foreach ($option_ids as $option_id) {
					if (($menu_item_option = menuItemOption::getByID($option_id))) {
						$menu_item_category->menuItemOptions[$menu_item_option->menuItemOption . '-' . $menu_item_option->idMenuItemOption] = $menu_item_option;
					}
				}
				ksort($menu_item_category->menuItemOptions); // Sort the options by name
				$menu_item_category->menuItemOptions = array_values($menu_item_category->menuItemOptions);
			}
			
			return $menu_item_category;
			
		} else {
			return false;
		}
	}
	
	function listCategoriesByMenuItem($menuItem, $options) {
		global $db;
		
		$query = 'SELECT `idMenuItemCategory`, `menuItemCategory`, `selectables`, `selectablesRule` 
			FROM `menus-items-categories` 
			WHERE (`idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\') 
			ORDER BY `menuItemCategory` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$menuItemCategories = array();
		while($row = mysql_fetch_assoc($result)){
			$menuItemCategory = new menuItemCategory;
			$menuItemCategory->idMenuItemCategory = $row['idMenuItemCategory'];
			$menuItemCategory->menuItemCategory = $row['menuItemCategory'];
			$menuItemCategory->selectables = $row['selectables'];
			$menuItemCategory->selectablesRule = $row['selectablesRule'];
			
			if($options){
				$menuItemCategory->menuItemOptions = menuItemOption::listByMenuItemCategory($menuItemCategory);
			}
			
			$menuItemCategories[] = $menuItemCategory;
		}
		
		return $menuItemCategories;
	}
	
	
	function insert_menu_item_category($data) {
		global $db;
		
		$conditions = array();
		foreach($data as $column => $value) {
			$conditions[] = '`' . $column . '` = \'' . mysql_real_escape_string($value) . '\'';
		}
		
		$query = 'INSERT INTO `restaurants-menus-items-categories` 
			SET ' . implode(", \n", $conditions) . ';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$menu_item_category = new menuItemCategory;
		$menu_item_category->idMenuItemCategory = mysql_insert_id($db->link);
		
		return $menu_item_category;
	}
	
	
	function updateMenuItemCategoryMenuItem($menuItemCategory, $menuItem){
		global $db;
		
		$query = 'UPDATE `menus-items-categories` 
			SET `idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\' 
			WHERE (`idMenuItemCategory` = \'' . mysql_real_escape_string($menuItemCategory->idMenuItemCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function updateMenuItemCategory($menuItemCategory, $menuItem, $menuItemCategory_, $selectables, $selectablesRule){
		global $db;
		
		$query = 'UPDATE `menus-items-categories` 
			SET `idMenuItem` = ' . ($menuItem ? '\'' . mysql_real_escape_string($menuItem->idMenuItem) . '\'' : 'NULL') . ', 
			`menuItemCategory` = \'' . mysql_real_escape_string($menuItemCategory_) . '\', 
			`selectables` = \'' . mysql_real_escape_string($selectables) . '\', 
			`selectablesRule` = \'' . mysql_real_escape_string($selectablesRule) . '\'
			WHERE (`idMenuItemCategory` = \'' . mysql_real_escape_string($menuItemCategory->idMenuItemCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function update_menu_item_category($menu_options_group, $data) {
		global $db;
		
		$conditions = array();
		foreach($data as $column => $value) {
			$conditions[] = '`' . $column . '` = \'' . mysql_real_escape_string($value) . '\'';
		}
		
		$query = 'UPDATE `restaurants-menus-items-categories` 
			SET ' . implode(", \n", $conditions) . ' 
			WHERE (`idMenuItemCategory` = \'' . mysql_real_escape_string($menu_options_group->idMenuItemCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function deleteMenuItemCategoryOptions($menuItemCategory){
		global $db;
		
		$query = 'DELETE FROM `restaurants-menus-items-categories-options` 
			WHERE (`idMenuItemCategory` = \'' . mysql_real_escape_string($menuItemCategory->idMenuItemCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function delete_menu_item_category($menu_options_group) {
		global $db;
		
		$query = 'DELETE FROM `restaurants-menus-items-categories` 
			WHERE (`idMenuItemCategory` = \'' . mysql_real_escape_string($menu_options_group->idMenuItemCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function reorderMenuItemCategory($menuItemCategory, $order){
		global $db;
		
		$query = 'UPDATE `restaurants-menus-items-categories`
			SET `order` = \'' . mysql_real_escape_string($order) . '\'
			WHERE (`idMenuItemCategory` = \'' . mysql_real_escape_string($menuItemCategory->idMenuItemCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
