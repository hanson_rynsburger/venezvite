<?php
    function listLanguages($all){
        global $db;
        
        $query = 'SELECT `idLanguage`, `languageName`, `languageAcronym`, `localeCode`, `default`, `visible`
            FROM `languages`';
        
        if(!$all){
            $query .= '
            WHERE `visible` = \'Y\'';
        }
        
        $query .= ';';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $languages = array();
        while($row = mysql_fetch_assoc($result)){
            $language = new language;
            $language->idLanguage = $row['idLanguage'];
            $language->languageName = $row['languageName'];
            $language->languageAcronym = $row['languageAcronym'];
            $language->localeCode = $row['localeCode'];
            $language->default = $row['default'];
            $language->visible = $row['visible'];
            
            $languages[] = $language;
        }
        
        return $languages;
    }
    
    function getDefaultLanguage(){
        global $db;
        
        $query = 'SELECT `idLanguage`, `languageName`, `languageAcronym`, `localeCode`, `default`
            FROM `languages`
            WHERE `default` = \'Y\'
                AND `visible` = \'Y\';';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $language = new language;
            $language->idLanguage = mysql_result($result, 0, 'idLanguage');
            $language->languageName = mysql_result($result, 0, 'languageName');
            $language->languageAcronym = mysql_result($result, 0, 'languageAcronym');
            $language->localeCode = mysql_result($result, 0, 'localeCode');
            $language->default = mysql_result($result, 0, 'default');
            
            return $language;
        }else{
            return false;
        }
    }
    
    function getLanguageByAcronym($acronym){
        global $db;
        
        $query = 'SELECT `idLanguage`, `languageName`, `languageAcronym`, `localeCode`, `default`
            FROM `languages`
            WHERE (`languageAcronym` = \'' . mysql_real_escape_string($acronym) . '\' 
                AND `visible` = \'Y\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $language = new language;
            $language->idLanguage = mysql_result($result, 0, 'idLanguage');
            $language->languageName = mysql_result($result, 0, 'languageName');
            $language->languageAcronym = mysql_result($result, 0, 'languageAcronym');
            $language->localeCode = mysql_result($result, 0, 'localeCode');
            $language->default = mysql_result($result, 0, 'default');
            
            return $language;
        }else{
            return false;
        }
    }
    
    function getLanguageByCountryCode($code){
        global $db;
        
        $query = 'SELECT L.`idLanguage`, L.`languageName`, L.`languageAcronym`, L.`localeCode`, L.`default`
            FROM `languages` L
            INNER JOIN `countries` C ON C.`idDefaultLanguage` = L.`idLanguage`
                AND (C.`countryAcronym` = \'' . mysql_real_escape_string($code) . '\')
            WHERE L.`visible` = \'Y\';';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $language = new language;
            $language->idLanguage = mysql_result($result, 0, 'idLanguage');
            $language->languageName = mysql_result($result, 0, 'languageName');
            $language->languageAcronym = mysql_result($result, 0, 'languageAcronym');
            $language->localeCode = mysql_result($result, 0, 'localeCode');
            $language->default = mysql_result($result, 0, 'default');
            
            return $language;
        }else{
            return false;
        }
    }
?>