<?php
	function corporateAccountInsert($contactFirstName, $contactLastName, $jobTitle, $department, $address, $city, $zipCode, $latitude, $longitude, $companyName, $phone, $mobile, $employeesNo, $username, $password, $salt, $emails, $idPreferredLanguage, $ip){
		global $db;
		
		$query = 'INSERT INTO `corporate-accounts`
			SET `contactFirstName` = \'' . mysql_real_escape_string($contactFirstName) . '\', 
			`contactLastName` = \'' . mysql_real_escape_string($contactLastName) . '\', 
			`jobTitle` = \'' . mysql_real_escape_string($jobTitle) . '\', 
			`department` = ' . (empty($department) ? 'NULL' : '\'' . mysql_real_escape_string($department) . '\'') . ', 
			`address` = \'' . mysql_real_escape_string($address) . '\', 
			`city` = \'' . mysql_real_escape_string($city) . '\', 
			`zipCode` = \'' . mysql_real_escape_string($zipCode) . '\', 
			`latitude` = ' . (empty($latitude) ? 'NULL' : '\'' . mysql_real_escape_string($latitude) . '\'') . ', 
			`longitude` = ' . (empty($longitude) ? 'NULL' : '\'' . mysql_real_escape_string($longitude) . '\'') . ', 
			`companyName` = \'' . mysql_real_escape_string($companyName) . '\', 
			`phone` = \'' . mysql_real_escape_string($phone) . '\', 
			`mobile` = ' . (empty($mobile) ? 'NULL' : '\'' . mysql_real_escape_string($mobile) . '\'') . ', 
			`employeesNo` = ' . (empty($employeesNo) ? 'NULL' : '\'' . mysql_real_escape_string($employeesNo) . '\'') . ', 
			`username` = \'' . mysql_real_escape_string($username) . '\', 
			`password` = \'' . mysql_real_escape_string($password) . '\', 
			`salt` = \'' . mysql_real_escape_string($salt) . '\', 
			`emails` = \'' . mysql_real_escape_string(implode(',', $emails)) . '\', 
			`idPreferredLanguage` = \'' . mysql_real_escape_string($idPreferredLanguage) . '\', 
			`ip` = \'' . mysql_real_escape_string($ip) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$corporateAccount = new corporateAccount;
		$corporateAccount->idCorporateAccount = mysql_insert_id($db->link);
		$corporateAccount->contactFirstName = $contactFirstName;
		$corporateAccount->contactLastName = $contactLastName;
		$corporateAccount->jobTitle = $jobTitle;
		$corporateAccount->department = $department;
		$corporateAccount->address = $address;
		$corporateAccount->city = $city;
		$corporateAccount->zipCode = $zipCode;
		$corporateAccount->latitude = $latitude;
		$corporateAccount->longitude = $longitude;
		$corporateAccount->companyName = $companyName;
		$corporateAccount->phone = $phone;
		$corporateAccount->mobile = $mobile;
		$corporateAccount->employeesNo = $employeesNo;
		$corporateAccount->username = $username;
		$corporateAccount->emails = $emails;
		
		$corporateAccount->preferredLanguage = new language;
		$corporateAccount->preferredLanguage->idLanguage = $idPreferredLanguage;
		
		$corporateAccount->ip = $ip;
		$corporateAccount->dateSubmitted = date('Y-m-d H:i:s');
		
		return $corporateAccount;
	}
	/*
	function getCorporateAccountByID($idRestaurant){
		global $db;
		
		$query = 'SELECT R.`idRestaurant`, R.`restaurantName`, R.`contactFirstName`, R.`contactLastName`, R.`address`, CI.`idCity`, CI.`cityName`, R.`zipCode`, R.`latitude`, R.`longitude`, R.`website`, R.`phone`, R.`mobile`, R.`email`, R.`delivery`, R.`catering`, R.`minimumDelivery`, R.`restaurantDescription`, R.`menuFile`, L.`idLanguage`, R.`ip`, R.`customURL`, R.`dateSubmitted`, R.`dateValidated`, R.`dateApproved`
			FROM `restaurants` R
			INNER JOIN `cities` CI ON CI.`idCity` = R.`idCity`
			LEFT OUTER JOIN `languages` L ON L.`idLanguage` = R.`idPreferredLanguage`
			WHERE (R.`idRestaurant` = \'' . mysql_real_escape_string($idRestaurant) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			$row = mysql_fetch_assoc($result);
			
			$restaurant = new restaurant;
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			// ...
			$restaurant->latitude = $row['latitude'];
			$restaurant->longitude = $row['longitude'];
			// ...
			$restaurant->email = $row['email'];
			$restaurant->minimumDelivery = $row['minimumDelivery'];
			// ...
			$restaurant->customURL = $row['customURL'];
			$restaurant->dateSubmitted = $row['dateSubmitted'];
			
			$restaurant->timetable = $restaurant->listTimetable();
			$restaurant->deliveryHours = $restaurant->listDeliveryHours();
			
			return $restaurant;
		}else{
			return false;
		}
	}
	*/
	function listUnconfirmedCorporateAccounts(){
		global $db;
		
		$query = 'SELECT `idCorporateAccount`, `companyName`, `emails`
			FROM `corporate-accounts`
			WHERE `dateValidated` IS NULL;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$corporateAccounts = array();
		while ($row = mysql_fetch_assoc($result)) {
			$corporateAccount = new corporateAccount;
			
			$corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
			$corporateAccount->companyName = $row['companyName'];
			//$corporateAccount->emails = explode(',', $row['emails']);
			
			$corporateAccounts[] = $corporateAccount;
		}
		
		return $corporateAccounts;
	}
	
	function confirmCorporateAccount($corporateAccount) {
		global $db;
		
		$query = 'UPDATE `corporate-accounts`
			SET `dateValidated` = Now()
			WHERE (`idCorporateAccount` = \'' . mysql_real_escape_string($corporateAccount->idCorporateAccount) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function listUnapprovedCorporateAccounts() {
		global $db;
		
		$query = 'SELECT `idCorporateAccount`, `companyName`, `emails`, `dateValidated`
			FROM `corporate-accounts`
			WHERE `dateApproved` IS NULL;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$corporateAccounts = array();
		while ($row = mysql_fetch_assoc($result)) {
			$corporateAccount = new corporateAccount;
			$corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
			$corporateAccount->companyName = $row['companyName'];
			$corporateAccount->emails = explode(',', $row['emails']);
			$corporateAccount->dateValidated = $row['dateValidated'];
			
			$corporateAccounts[] = $corporateAccount;
		}
		
		return $corporateAccounts;
	}
	
	function approveCorporateAccount($corporateAccount) {
		global $db;
		
		$query = 'UPDATE `corporate-accounts`
			SET `dateApproved` = Now()
			WHERE (`idCorporateAccount` = \'' . mysql_real_escape_string($corporateAccount->idCorporateAccount) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	/*
	function listCorporateAccounts(){
		global $db;
		
		$query = 'SELECT DISTINCT R.`customURL`
			FROM `restaurants` R
			INNER JOIN `restaurants-menus` RM ON RM.`idRestaurant` = R.`idRestaurant`
			INNER JOIN `restaurants-menus-items` RMI ON RMI.`idMenu` = RM.`idMenu`
				AND RMI.`enabled` = \'Y\'
			WHERE R.`dateApproved` IS NOT NULL
			ORDER BY `dateSubmitted` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$restaurants = array();
		while($row = mysql_fetch_assoc($result)){
			$restaurant = new restaurant;
			$restaurant->customURL = $row['customURL'];
			
			$restaurants[] = $restaurant;
		}
		
		return $restaurants;
	}
	*/
	function checkCorporateAccountDuplicateUsername($username){
		global $db;
		
		$query = 'SELECT `idCorporateAccount`, `companyName`
			FROM `corporate-accounts`
			WHERE (`username` = \'' . mysql_real_escape_string($username) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)>0) {
			return true;
		}
		
		return false;
	}
	
	function authenticateCorporateAccount($username){
		global $db;
		
		$query = 'SELECT CA.`idCorporateAccount`, CA.`customerProfileID`, CA.`contactLastName`, CA.`contactFirstName`, CA.`jobTitle`, CA.`department`, CA.`address`, CA.`city`, CA.`zipCode`, CA.`latitude`, CA.`longitude`, CA.`companyName`, CA.`phone`, CA.`mobile`, CA.`employeesNo`, CA.`username`, CA.`password`, CA.`salt`, CA.`emails`, L.`idLanguage`, CA.`dateSubmitted`, CA.`dateValidated`
			FROM `corporate-accounts` CA
			LEFT OUTER JOIN `languages` L ON L.`idLanguage` = CA.`idPreferredLanguage` 
			WHERE (CA.`username` = \'' . mysql_real_escape_string($username) . '\'
				AND CA.`dateApproved` IS NOT NULL);';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			// It seems to be a valid call
			$row = mysql_fetch_assoc($result);
			
			$corporateAccount = new corporateAccount;
			$corporateAccount->idCorporateAccount = $row['idCorporateAccount'];
			$corporateAccount->customerProfileID = $row['customerProfileID'];
			$corporateAccount->contactLastName = $row['contactLastName'];
			$corporateAccount->contactFirstName = $row['contactFirstName'];
			$corporateAccount->jobTitle = $row['jobTitle'];
			$corporateAccount->department = $row['department'];
			$corporateAccount->address = $row['address'];
			$corporateAccount->city = $row['city'];
			$corporateAccount->zipCode = $row['zipCode'];
			$corporateAccount->latitude = $row['latitude'];
			$corporateAccount->longitude = $row['longitude'];
			$corporateAccount->companyName = $row['companyName'];
			$corporateAccount->phone = $row['phone'];
			$corporateAccount->mobile = $row['mobile'];
			$corporateAccount->employeesNo = $row['employeesNo'];
			$corporateAccount->username = $username;
			$corporateAccount->password = $row['password'];
			$corporateAccount->salt = $row['salt'];
			$corporateAccount->emails = explode(',', $row['emails']);
			
			$corporateAccount->preferredLanguage = new language;
			$corporateAccount->preferredLanguage->idLanguage = $row['idLanguage'];
			
			$corporateAccount->dateSubmitted = $row['dateSubmitted'];
			$corporateAccount->dateValidated = $row['dateValidated'];
			
			return $corporateAccount;
			
		}
		
		return false;
	}
