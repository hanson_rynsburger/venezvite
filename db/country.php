<?php
	function listCountries(){
		global $db;
		
		$query = 'SELECT `idCountry`, `idDefaultLanguage`, `countryName`, `countryAcronym`, `countryFlag`, `latitude`, `longitude` 
			FROM `countries` 
			ORDER BY `countryName` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$countries = array();
		while($row = mysql_fetch_assoc($result)){
			$country = new country;
			$country->idCountry = $row['idCountry'];
			
			$country->defaultLanguage = new language;
			$country->defaultLanguage->idLanguage = $row['idDefaultLanguage'];
			
			$country->countryName = $row['countryName'];
			$country->countryAcronym = $row['countryAcronym'];
			$country->countryFlag = $row['countryFlag'];
			$country->latitude = $row['latitude'];
			$country->longitude = $row['longitude'];
			
			$countries[] = $country;
		}
		
		return $countries;
	}
	
	function getCountryByID($id_country) {
		global $db;
		
		$query = 'SELECT `idCountry`, `idDefaultLanguage`, `countryName`, `countryAcronym`, `countryFlag`, `latitude`, `longitude` 
			FROM `countries` 
			WHERE (`idCountry` = \'' . mysql_real_escape_string($id_country) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			$row = mysql_fetch_assoc($result);
			
			$country = new country;
			$country->idCountry = $row['idCountry'];
			
			$country->defaultLanguage = new language;
			$country->defaultLanguage->idLanguage = $row['idDefaultLanguage'];
			
			$country->countryName = $row['countryName'];
			$country->countryAcronym = $row['countryAcronym'];
			$country->countryFlag = $row['countryFlag'];
			$country->latitude = $row['latitude'];
			$country->longitude = $row['longitude'];
			
			return $country;
			
		} else {
			return false;
		}
	}
