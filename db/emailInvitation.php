<?php
    function listEmailInvitations($user){
        global $db;
        
        $query = 'SELECT `idInvitation`, `promoCode`, `value`, `datePromoUsed`
            FROM `users-email-invitations`
            WHERE (`idUser` = \'' . mysql_real_escape_string($user->idUser) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $emailInvitations = array();
        while($row = mysql_fetch_assoc($result)){
            $emailInvitation = new emailInvitation;
            $emailInvitation->idInvitation = $row['idInvitation'];
            $emailInvitation->promoCode = $row['promoCode'];
            $emailInvitation->value = $row['value'];
            $emailInvitation->datePromoUsed = $row['datePromoUsed'];
            
            $emailInvitations[] = $emailInvitation;
        }
        
        return $emailInvitations;
    }
    
    function insertEmailInvitation($user, $email){
        global $db;
        
        $query = 'INSERT INTO `users-email-invitations`
            SET `idUser` = \'' . mysql_real_escape_string($user->idUser) . '\', 
            `email` = \'' . mysql_real_escape_string($email) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $emailInvitation = new emailInvitation;
        $emailInvitation->idInvitation = mysql_insert_id($db->link);
        $emailInvitation->user = $user;
        $emailInvitation->email = $email;
        
        return $emailInvitation;
    }
    
    function getEmailInvitationByID($idInvitation) {
        global $db;
        
        $query = 'SELECT UEI.`idInvitation`, U.`idUser`, U.`firstName`, U.`lastName`, U.`email` AS userEmail, UEI.`email` AS invitationEmail, UEI.`promoCode`, UEI.`value`, UEI.`dateAdded`, UEI.`datePromoUsed`
            FROM `users-email-invitations` UEI
            INNER JOIN `users` U ON U.`idUser` = UEI.`idUser`
            WHERE (UEI.`idInvitation` = \'' . mysql_real_escape_string($idInvitation) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $row = mysql_fetch_assoc($result);
            
            $emailInvitation = new emailInvitation;
            $emailInvitation->idInvitation = $row['idInvitation'];
            
            $emailInvitation->user = new user;
            $emailInvitation->user->idUser = $row['idUser'];
            $emailInvitation->user->firstName = $row['firstName'];
            $emailInvitation->user->lastName = $row['lastName'];
            $emailInvitation->user->email = $row['userEmail'];
            
            $emailInvitation->email = $row['invitationEmail'];
            $emailInvitation->promoCode = $row['promoCode'];
            $emailInvitation->value = $row['value'];
            $emailInvitation->dateAdded = $row['dateAdded'];
            $emailInvitation->datePromoUsed = $row['datePromoUsed'];
            
            return $emailInvitation;
            
        } else {
            return false;
        }
    }
    
    function getEmailInvitationByEmail($email){
        global $db;
        
        $query = 'SELECT UEI.`idInvitation`, U.`idUser`, U.`firstName`, U.`lastName`, U.`email` AS userEmail, UEI.`email` AS invitationEmail, UEI.`promoCode`, UEI.`value`, UEI.`dateAdded`, UEI.`datePromoUsed`
            FROM `users-email-invitations` UEI
            INNER JOIN `users` U ON U.`idUser` = UEI.`idUser`
            WHERE (UEI.`email` = \'' . mysql_real_escape_string($email) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $row = mysql_fetch_assoc($result);
            
            $emailInvitation = new emailInvitation;
            $emailInvitation->idInvitation = $row['idInvitation'];
            
            $emailInvitation->user = new user;
            $emailInvitation->user->idUser = $row['idUser'];
            $emailInvitation->user->firstName = $row['firstName'];
            $emailInvitation->user->lastName = $row['lastName'];
            $emailInvitation->user->email = $row['userEmail'];
            
            $emailInvitation->email = $row['invitationEmail'];
            $emailInvitation->promoCode = $row['promoCode'];
            $emailInvitation->value = $row['value'];
            $emailInvitation->dateAdded = $row['dateAdded'];
            $emailInvitation->datePromoUsed = $row['datePromoUsed'];
            
            return $emailInvitation;
        }else{
            return false;
        }
    }
    
    function getEmailInvitationByPromoCode($promoCode){
        global $db;
        
        $query = 'SELECT UEI.`idInvitation`, U.`idUser`, U.`firstName`, U.`lastName`, U.`email` AS userEmail, UEI.`email` AS invitationEmail, UEI.`promoCode`, UEI.`value`, UEI.`dateAdded`, UEI.`datePromoUsed`
            FROM `users-email-invitations` UEI
            INNER JOIN `users` U ON U.`idUser` = UEI.`idUser`
            WHERE (UEI.`promoCode` = \'' . mysql_real_escape_string($promoCode) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if(mysql_num_rows($result)==1){
            $row = mysql_fetch_assoc($result);
            
            $emailInvitation = new emailInvitation;
            $emailInvitation->idInvitation = $row['idInvitation'];
            
            $emailInvitation->user = new user;
            $emailInvitation->user->idUser = $row['idUser'];
            $emailInvitation->user->firstName = $row['firstName'];
            $emailInvitation->user->lastName = $row['lastName'];
            $emailInvitation->user->email = $row['userEmail'];
            
            $emailInvitation->email = $row['invitationEmail'];
            $emailInvitation->promoCode = $row['promoCode'];
            $emailInvitation->value = $row['value'];
            $emailInvitation->dateAdded = $row['dateAdded'];
            $emailInvitation->datePromoUsed = $row['datePromoUsed'];
            
            return $emailInvitation;
        }else{
            return false;
        }
    }
    
    function setEmailInvitationCode($emailInvitation, $promoCode) {
        global $db;
        
        $query = 'UPDATE `users-email-invitations`
            SET `promoCode` = \'' . mysql_real_escape_string($promoCode) . '\'
            WHERE (`idInvitation` = \'' . mysql_real_escape_string($emailInvitation->idInvitation) . '\');';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
    }
    
    function useEmailInvitationCode($emailInvitation, $order) {
        global $db;
        
        $query = 'UPDATE `users-email-invitations`
            SET `idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\', 
            `datePromoUsed` = Now()
            WHERE (`idInvitation` = \'' . mysql_real_escape_string($emailInvitation->idInvitation) . '\');';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
    }
    
    function getEmailInvitationByOrder($order) {
        global $db;
        
        $query = 'SELECT `idInvitation`, `email`, `promoCode`, `value`, `dateAdded`, `datePromoUsed` 
            FROM `users-email-invitations` 
            WHERE (`idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\');';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        if (mysql_num_rows($result)==1) {
            $row = mysql_fetch_assoc($result);
            
            $emailInvitation = new emailInvitation;
            $emailInvitation->idInvitation = $row['idInvitation'];
            $emailInvitation->email = $row['email'];
            $emailInvitation->promoCode = $row['promoCode'];
            $emailInvitation->value = $row['value'];
            $emailInvitation->dateAdded = $row['dateAdded'];
            $emailInvitation->datePromoUsed = $row['datePromoUsed'];
            
            return $emailInvitation;
            
        } else {
            return false;
        }
    }
?>