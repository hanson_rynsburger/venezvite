<?php
    function listOrderItems($order){
        global $db;
        
        $query = 'SELECT OI.`idOrderItem`, RMI.`idMenuItem`, RMI.`photo`, RMI.`menuItemDescription`, RMI.`spicy`, RMI.`vegetarian`, OI.`menuItemName`, OI.`quantity`, OI.`pricePerItem`, OI.`instructions`, RMC.`idMenuCategory`, RMC.`category`
            FROM `orders-items` OI
            INNER JOIN `orders` O ON O.`idOrder` = OI.`idOrder`
                AND (O.`idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\')
            LEFT OUTER JOIN `restaurants-menus-items` RMI ON RMI.`idMenuItem` = OI.`idMenuItem`
            LEFT OUTER JOIN `restaurants-menus-categories` RMC ON RMC.`idMenuCategory` = RMI.`idMenuCategory`;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $orderItems = array();
        while($row = mysql_fetch_assoc($result)){
            $orderItem = new orderItem;
            $orderItem->idOrderItem = $row['idOrderItem'];
            
            $orderItem->menuItem = new menuItem;
            $orderItem->menuItem->idMenuItem = $row['idMenuItem'];
            $orderItem->menuItem->photo = $row['photo'];
            $orderItem->menuItem->menuItemDescription = $row['menuItemDescription'];
            $orderItem->menuItem->spicy = $row['spicy'];
            $orderItem->menuItem->vegetarian = $row['vegetarian'];
            
            $orderItem->menuItem->menuCategory = new menuCategory;
            $orderItem->menuItem->menuCategory->idMenuCategory = $row['idMenuCategory'];
            $orderItem->menuItem->menuCategory->category = $row['category'];
            
            $orderItem->menuItemName = $row['menuItemName'];
            $orderItem->quantity = $row['quantity'];
            $orderItem->pricePerItem = $row['pricePerItem'];
            $orderItem->instructions = $row['instructions'];
            
            $orderItem->options = orderItemOption::list_($orderItem);
            
            $orderItems[] = $orderItem;
        }
        
        return $orderItems;
    }
    
    function insertOrderItem($order, $menuItem, $quantity, $instructions){
        global $db;
        
        $query = 'INSERT INTO `orders-items`
            SET `idOrder` = \'' . mysql_real_escape_string($order->idOrder) . '\', 
            `idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\', 
            `menuItemName` = \'' . mysql_real_escape_string($menuItem->menuItemName) . '\', 
            `quantity` = \'' . mysql_real_escape_string($quantity) . '\', 
            `pricePerItem` = \'' . mysql_real_escape_string($menuItem->getPrice()) . '\', 
            `instructions` = \'' . mysql_real_escape_string($instructions) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $orderItem = new orderItem;
        $orderItem->idOrderItem = mysql_insert_id($db->link);
        $orderItem->order = $order;
        $orderItem->menuItem = $menuItem;
        $orderItem->menuItemName = $menuItem->menuItemName;
        $orderItem->quantity = $quantity;
        $orderItem->pricePerItem = $menuItem->getPrice();
        $orderItem->instructions = $instructions;
        
        return $orderItem;
    }
?>