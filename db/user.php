<?php
	function checkUserDuplicateEmail($email){
		global $db;
		
		$query = 'SELECT `idUser`, `firstName`, `lastName`, `email`
			FROM `users`
			WHERE (`email` = \'' . mysql_real_escape_string($email) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)>0) {
			return true;
		}
		
		return false;
	}
	
	function userInsert($idFacebook, $gender, $firstName, $lastName, $email, $password, $salt, $referer, $phone, $newsletter, $avatar, $idPreferredLanguage, $ip){
		global $db;
		
		$query = 'INSERT INTO `users`
			SET `idFacebook` = ' . (!empty($idFacebook) ? '\'' . mysql_real_escape_string($idFacebook) . '\'' : 'NULL') . ', 
			`gender` = ' . (!empty($gender) ? '\'' . mysql_real_escape_string($gender) . '\'' : 'NULL') . ', 
			`firstName` = \'' . mysql_real_escape_string($firstName) . '\', 
			`lastName` = \'' . mysql_real_escape_string($lastName) . '\', 
			`email` = \'' . mysql_real_escape_string($email) . '\', 
			`password` = \'' . mysql_real_escape_string($password) . '\', 
			`salt` = \'' . mysql_real_escape_string($salt) . '\', 
			`referer` = ' . (!empty($referer) ? '\'' . mysql_real_escape_string($referer) . '\'' : 'NULL') . ', 
			`phone` = ' . (!empty($phone) ? '\'' . mysql_real_escape_string($phone) . '\'' : 'NULL') . ', 
			`newsletter` = \'' . mysql_real_escape_string($newsletter) . '\', 
			`avatar` = ' . (!empty($avatar) ? '\'' . mysql_real_escape_string($avatar) . '\'' : 'NULL') . ', 
			`idPreferredLanguage` = \'' . mysql_real_escape_string($idPreferredLanguage) . '\', 
			`ip` = \'' . mysql_real_escape_string($ip) . '\', 
			`dateValidated` = Now();'; // We don't validate email addresses anymore...
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$user = new user;
		$user->idUser = mysql_insert_id($db->link);
		$user->idFacebook = $idFacebook;
		//$user->gender = $gender;
		$user->firstName = $firstName;
		$user->lastName = $lastName;
		$user->email = $email;
		$user->phone = $phone;
		$user->avatar = $avatar;
		
		$user->preferredLanguage = new language;
		$user->preferredLanguage->idLanguage = $idPreferredLanguage;
		
		return $user;
	}
	
	function authenticateUser($username){
		global $db;
		
		$query = 'SELECT U.`idUser`, U.`idFacebook`, U.`customerProfileID`, U.`gender`, U.`firstName`, U.`lastName`, U.`password`, U.`salt`, U.`phone`, U.`avatar`, L.`idLanguage`, U.`dateSubmitted`, U.`dateValidated`
			FROM `users` U
			LEFT OUTER JOIN `languages` L ON L.`idLanguage` = U.`idPreferredLanguage` 
			WHERE (U.`email` = \'' . mysql_real_escape_string($username) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			// It seems to be a valid call
			$row = mysql_fetch_assoc($result);
			
			$user = new user;
			$user->idUser = $row['idUser'];
			$user->idFacebook = $row['idFacebook'];
			$user->customerProfileID = $row['customerProfileID'];
			$user->gender = $row['gender'];
			$user->firstName = $row['firstName'];
			$user->lastName = $row['lastName'];
			$user->email = $username;
			$user->password = $row['password'];
			$user->salt = $row['salt'];
			$user->phone = $row['phone'];
			$user->avatar = $row['avatar'];
			
			$user->preferredLanguage = new language;
			$user->preferredLanguage->idLanguage = $row['idLanguage'];
			
			$user->dateSubmitted = $row['dateSubmitted'];
			//$user->dateValidated = $row['dateValidated'];
			
			//$user->addresses = userAddress::list_($user);
			$user->ratings = $user->listRatings();
			$user->favorites = $user->listFavorites();
			
			return $user;
		}else{
			return false;
		}
	}
	
	function getUserByFB($fbID){
		global $db;
		
		$query = 'SELECT U.`idUser`, U.`customerProfileID`, U.`gender`, U.`firstName`, U.`lastName`, U.`email`, U.`password`, U.`salt`, U.`phone`, U.`avatar`, L.`idLanguage`, U.`dateSubmitted`, U.`dateValidated`
			FROM `users` U
			LEFT OUTER JOIN `languages` L ON L.`idLanguage` = U.`idPreferredLanguage` 
			WHERE (U.`idFacebook` = \'' . mysql_real_escape_string($fbID) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			// It seems to be a valid call
			$row = mysql_fetch_assoc($result);
			
			$user = new user;
			$user->idUser = $row['idUser'];
			$user->idFacebook = $fbID;
			$user->customerProfileID = $row['customerProfileID'];
			$user->gender = $row['gender'];
			$user->firstName = $row['firstName'];
			$user->lastName = $row['lastName'];
			$user->email = $row['email'];
			$user->password = $row['password'];
			$user->salt = $row['salt'];
			$user->phone = $row['phone'];
			$user->avatar = $row['avatar'];
			
			$user->preferredLanguage = new language;
			$user->preferredLanguage->idLanguage = $row['idLanguage'];
			
			$user->dateSubmitted = $row['dateSubmitted'];
			//$user->dateValidated = $row['dateValidated'];
			
			//$user->addresses = userAddress::list_($user);
			$user->ratings = $user->listRatings();
			$user->favorites = $user->listFavorites();
			
			return $user;
		}else{
			return false;
		}
	}
	
	function updateUserFbID($user, $fbID){
		global $db;
		
		$query = 'UPDATE `users`
			SET `idFacebook` = \'' . mysql_real_escape_string($fbID) . '\'
			WHERE (`idUser` = \'' . mysql_real_escape_string($user->idUser) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function getUserByID($idUser){
		global $db;
		
		$query = 'SELECT `idUser`, `firstName`, `lastName`, `email`, `salt`
			FROM `users`
			WHERE (`idUser` = \'' . mysql_real_escape_string($idUser) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			// It seems to be a valid call
			$row = mysql_fetch_assoc($result);
			
			$user = new user;
			$user->idUser = $row['idUser'];
			$user->firstName = $row['firstName'];;
			$user->lastName = $row['lastName'];;
			$user->email = $row['email'];;
			$user->salt = $row['salt'];;
			
			return $user;
		}else{
			return false;
		}
	}
	
	function updateUserPassword($user, $password){
		global $db;
		
		$query = 'UPDATE `users`
			SET `password` = \'' . mysql_real_escape_string($password) . '\'
			WHERE (`idUser` = \'' . mysql_real_escape_string($user->idUser) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function listRatedRestaurants($user){
		global $db;
		
		$query = 'SELECT R.`idRestaurant`, R.`restaurantName`, R.`customURL` 
			FROM `restaurants-ratings` RR 
			INNER JOIN `restaurants` R ON R.`idRestaurant` = RR.`idRestaurant` 
			WHERE (RR.`idUser` = \'' . mysql_real_escape_string($user->idUser) . '\')
			ORDER BY RR.`dateAdded` DESC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$ratings = array();
		while($row = mysql_fetch_assoc($result)){
			$restaurant = new restaurant;
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			$restaurant->customURL = $row['customURL'];
			
			$ratings[] = $restaurant;
		}
		
		return $ratings;
	}
	
	function addRestaurantToRatings($user, $restaurant, $rating){
		global $db;
		
		$query = 'INSERT INTO `restaurants-ratings`
			SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`idUser` = \'' . mysql_real_escape_string($user->idUser) . '\', 
			`rating` = \'' . mysql_real_escape_string($rating) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function listFavoriteRestaurants($user){
		global $db;
		
		$query = 'SELECT R.`idRestaurant`, R.`restaurantName`, R.`customURL`
			FROM `users-favorites` UF
			INNER JOIN `restaurants` R ON R.`idRestaurant` = UF.`idRestaurant`
			WHERE (UF.`idUser` = \'' . mysql_real_escape_string($user->idUser) . '\')
			ORDER BY UF.`dateAdded` DESC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$favorites = array();
		while($row = mysql_fetch_assoc($result)){
			$restaurant = new restaurant;
			$restaurant->idRestaurant = $row['idRestaurant'];
			$restaurant->restaurantName = $row['restaurantName'];
			$restaurant->customURL = $row['customURL'];
			
			$favorites[] = $restaurant;
		}
		
		return $favorites;
	}
	
	function addRestaurantToFavorites($user, $restaurant){
		global $db;
		
		$query = 'INSERT INTO `users-favorites`
			SET `idUser` = \'' . mysql_real_escape_string($user->idUser) . '\', 
			`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function removeRestaurantFromFavorites($user, $restaurant){
		global $db;
		
		$query = sprintf(
				'delete from `users-favorites` where idUser=\'%d\' and idRestaurant=\'%d\'',
				mysql_real_escape_string($user->idUser),
				mysql_real_escape_string($restaurant->idRestaurant)
		);

		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}

	function isRestaurantFavorited( $user, $restaurant ) {
		global $db;

		$query = sprintf(
				"SELECT idFavorite from `users-favorites` where idUser='%d' and idRestaurant='%d'",
				mysql_real_escape_string($user->idUser),
				mysql_real_escape_string($restaurant->idRestaurant)
		);
		
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		return mysql_num_rows($result);
	}
	
	function userUpdate($user, $firstName, $lastName, $email, $password, $salt, $phone){
		global $db;
		
		$query = 'UPDATE `users`
			SET ';
		
		if (!empty($firstName)) {
			$query .= '`firstName` = \'' . mysql_real_escape_string($firstName) . '\', 
			';
		}
		
		if (!empty($lastName)) {
			$query .= '`lastName` = \'' . mysql_real_escape_string($lastName) . '\', 
			';
		}
		
		if (!empty($email)) {
			$query .= '`email` = \'' . mysql_real_escape_string($email) . '\', 
			';
		}
		
		if(!empty($password) && !empty($salt)){
			$query .= '`password` = \'' . mysql_real_escape_string($password) . '\', 
			`salt` = \'' . mysql_real_escape_string($salt) . '\', 
			';
		}
		
		$query .= '`phone` = \'' . mysql_real_escape_string($phone) . '\' 
			WHERE (`idUser` = \'' . mysql_real_escape_string($user->idUser) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
