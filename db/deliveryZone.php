<?php
	function getDeliveryZoneByID($idDeliveryZone){
		global $db;
		
		$query = 'SELECT `idDeliveryZone`, `idRestaurant`, `range`, `deliveryFee`, `minimumDelivery`, `estimatedTime` 
			FROM `restaurants-delivery-zones` 
			WHERE (`idDeliveryZone` = \'' . mysql_real_escape_string($idDeliveryZone) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			$row = mysql_fetch_assoc($result);
			
			$deliveryZone = new deliveryZone;
			$deliveryZone->idDeliveryZone = $row['idDeliveryZone'];
			
			$deliveryZone->restaurant = new restaurant;
			$deliveryZone->restaurant->idRestaurant = $row['idRestaurant'];
			
			$deliveryZone->range = $row['range'];
			$deliveryZone->deliveryFee = $row['deliveryFee'];
			$deliveryZone->minimumDelivery = $row['minimumDelivery'];
			$deliveryZone->estimatedTime = $row['estimatedTime'];
			
			return $deliveryZone;
			
		} else {
			return false;
		}
	}
	
	function insertDeliveryZone($restaurant, $range, $deliveryFee, $estimatedTime, $minimumDelivery, $coordinates = null) {
		global $db;
		
		$query = 'INSERT INTO `restaurants-delivery-zones`
			SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`range` = \'' . mysql_real_escape_string($range) . '\', 
			`coordinates` = ' . (!empty($coordinates) ? '\'' . mysql_real_escape_string($coordinates) . '\'' : 'NULL')  . ', 
			`deliveryFee` = \'' . mysql_real_escape_string($deliveryFee) . '\', 
			`minimumDelivery` = \'' . mysql_real_escape_string($minimumDelivery) . '\', 
			`estimatedTime` = \'' . mysql_real_escape_string($estimatedTime) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$deliveryZone = new deliveryZone;
		$deliveryZone->idDeliveryZone = mysql_insert_id($db->link);
		//$deliveryZone->restaurant = $restaurant;
		$deliveryZone->range = $range;
		$deliveryZone->coordinates = $coordinates;
		$deliveryZone->deliveryFee = $deliveryFee;
		$deliveryZone->minimumDelivery = $minimumDelivery;
		$deliveryZone->estimatedTime = $estimatedTime;
		
		return $deliveryZone;
	}
	
	function listDeliveryZones($restaurant){
		global $db;
		
		$query = 'SELECT `idDeliveryZone`, `range`, `coordinates`, `deliveryFee`, `minimumDelivery`, `estimatedTime`
			FROM `restaurants-delivery-zones`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')
			ORDER BY `range` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$deliveryZones = array();
		while($row = mysql_fetch_assoc($result)){
			$deliveryZone = new deliveryZone;
			$deliveryZone->idDeliveryZone = $row['idDeliveryZone'];
			$deliveryZone->range = $row['range'];
			$deliveryZone->coordinates = $row['coordinates'];
			$deliveryZone->deliveryFee = $row['deliveryFee'];
			$deliveryZone->minimumDelivery = $row['minimumDelivery'];
			$deliveryZone->estimatedTime = $row['estimatedTime'];
			
			$deliveryZones[] = $deliveryZone;
		}
		
		return $deliveryZones;
	}
	
	function updateDeliveryZone($deliveryZone, $data){
		global $db;
		
		$conditions = array();
		foreach($data as $column => $value){
			$conditions[] = '`' . $column . '` = ' . ($value===null ? 'NULL' : '\'' . mysql_real_escape_string($value) . '\'');
		}
		
		$query = 'UPDATE `restaurants-delivery-zones` 
			SET ' . implode(", \n", $conditions) . '
			WHERE (`idDeliveryZone` = \'' . mysql_real_escape_string($deliveryZone->idDeliveryZone) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function removeDeliveryZone($deliveryZone){
		global $db;
		
		$query = 'DELETE FROM `restaurants-delivery-zones`
			WHERE (`idDeliveryZone` = \'' . mysql_real_escape_string($deliveryZone->idDeliveryZone) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
