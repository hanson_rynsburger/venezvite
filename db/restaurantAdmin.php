<?php
	function restaurantAdminInsert($restaurant, $email, $password, $salt){
		global $db;
		
		$query = 'INSERT INTO `restaurants-admins`
			SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`email` = \'' . mysql_real_escape_string($email) . '\', 
			`password` = \'' . mysql_real_escape_string($password) . '\', 
			`salt` = \'' . mysql_real_escape_string($salt) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
	
	function authenticateRestaurantAdmin($username){
		global $db;
		
		$query = 'SELECT RA.`idRestaurantAdmin`, 
	R.`idRestaurant`, R.`restaurantName`, R.`contactFirstName`, R.`contactLastName`, R.`address`, R.`city`, R.`zipCode`, R.`latitude`, R.`longitude`, R.`website`, R.`phone`, R.`mobile`, R.`email`, R.`delivery`, R.`customDelivery`, R.`cashPayment`, R.`currency`, R.`restaurantDescription`, R.`bankAccountNo`, R.`iban`, R.`bankAccountName`, R.`businessAddress`, R.`bankName`, R.`bankAddress`, R.`swiftCode`, R.`customURL`, R.`commission`, R.`dateApproved`, 
	RA.`password`, RA.`salt`, RA.`dateAdded` 
FROM `restaurants-admins` RA 
INNER JOIN `restaurants` R ON R.`idRestaurant` = RA.`idRestaurant` 
WHERE (RA.`email` = \'' . mysql_real_escape_string($username) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		if (mysql_num_rows($result)==1) {
			// It seems to be a valid call
			$row = mysql_fetch_assoc($result);
			
			$restaurantAdmin = new restaurantAdmin;
			$restaurantAdmin->idRestaurantAdmin = $row['idRestaurantAdmin'];
			
			$restaurantAdmin->restaurant = new restaurant;
			$restaurantAdmin->restaurant->idRestaurant = $row['idRestaurant'];
			$restaurantAdmin->restaurant->restaurantName = $row['restaurantName'];
			$restaurantAdmin->restaurant->contactFirstName = $row['contactFirstName'];
			$restaurantAdmin->restaurant->contactLastName = $row['contactLastName'];
			$restaurantAdmin->restaurant->address = $row['address'];
			$restaurantAdmin->restaurant->city = $row['city'];
			$restaurantAdmin->restaurant->zipCode = $row['zipCode'];
			$restaurantAdmin->restaurant->latitude = $row['latitude'];
			$restaurantAdmin->restaurant->longitude = $row['longitude'];
			$restaurantAdmin->restaurant->website = $row['website'];
			$restaurantAdmin->restaurant->phone = $row['phone'];
			$restaurantAdmin->restaurant->mobile = $row['mobile'];
			$restaurantAdmin->restaurant->email = $row['email'];
			$restaurantAdmin->restaurant->restaurantDescription = $row['restaurantDescription'];
			$restaurantAdmin->restaurant->menus = restaurantMenu::list_($restaurantAdmin->restaurant);
			
			$restaurantAdmin->restaurant->delivery = $row['delivery'];
			$restaurantAdmin->restaurant->customDelivery = $row['customDelivery'];
			$restaurantAdmin->restaurant->cashPayment = $row['cashPayment'];
			$restaurantAdmin->restaurant->currency = $row['currency'];
			$restaurantAdmin->restaurant->bankAccountNo = $row['bankAccountNo'];
			$restaurantAdmin->restaurant->iban = $row['iban'];
			$restaurantAdmin->restaurant->bankAccountName = $row['bankAccountName'];
			$restaurantAdmin->restaurant->businessAddress = $row['businessAddress'];
			$restaurantAdmin->restaurant->bankName = $row['bankName'];
			$restaurantAdmin->restaurant->bankAddress = $row['bankAddress'];
			$restaurantAdmin->restaurant->swiftCode = $row['swiftCode'];
			$restaurantAdmin->restaurant->customURL = $row['customURL'];
			$restaurantAdmin->restaurant->commission = $row['commission'];
			$restaurantAdmin->restaurant->dateApproved = $row['dateApproved'];
			
			$restaurantAdmin->email = $username;
			$restaurantAdmin->password = $row['password'];
			$restaurantAdmin->salt = $row['salt'];
			$restaurantAdmin->dateAdded = $row['dateAdded'];
			
			return $restaurantAdmin;
			
		} else {
			return false;
		}
	}
	
	function getRestaurantAdminByID($idRestaurantAdmin){
		global $db;
		
		$query = 'SELECT RA.`idRestaurantAdmin`, R.`idRestaurant`, R.`restaurantName`, RA.`email`, RA.`salt`
			FROM `restaurants-admins` RA
			INNER JOIN `restaurants` R ON R.`idRestaurant` = RA.`idRestaurant`
			WHERE (RA.`idRestaurantAdmin` = \'' . mysql_real_escape_string($idRestaurantAdmin) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			// It seems to be a valid call
			$row = mysql_fetch_assoc($result);
			
			$restaurantAdmin = new restaurantAdmin;
			$restaurantAdmin->idRestaurantAdmin = $row['idRestaurantAdmin'];
			
			$restaurantAdmin->restaurant = new restaurant;
			$restaurantAdmin->restaurant->idRestaurant = $row['idRestaurant'];
			$restaurantAdmin->restaurant->restaurantName = $row['restaurantName'];
			
			$restaurantAdmin->email = $row['email'];;
			$restaurantAdmin->salt = $row['salt'];;
			
			return $restaurantAdmin;
		}else{
			return false;
		}
	}
	
	function updateRestaurantAdmin($restaurantAdmin, $data) {
		global $db;
		
		$conditions = array();
		foreach($data as $column => $value){
			$conditions[] = '`' . $column . '` = ' . ($value===null ? 'NULL' : '\'' . mysql_real_escape_string($value) . '\'');
		}
		
		$query = 'UPDATE `restaurants-admins` 
			SET ' . implode(", \n", $conditions) . '
			WHERE (`idRestaurantAdmin` = \'' . mysql_real_escape_string($restaurantAdmin->idRestaurantAdmin) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
	}
