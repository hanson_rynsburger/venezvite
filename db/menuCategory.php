<?php
	function listMenuCategories($restaurant) {
		global $db;
		
		$query = /*'(SELECT `idMenuCategory`, \'\' AS idRestaurant, `category`, `order`
			FROM `restaurants-menus-categories`
			WHERE `idRestaurant` IS NULL)
			UNION
			(' . */'SELECT `idMenuCategory`, `idRestaurant`, `category`, `order`
			FROM `restaurants-menus-categories`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')' . /*')' . */'
			ORDER BY idRestaurant ASC, `order` ASC;';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$menuCategories = array();
		while($row = mysql_fetch_assoc($result)){
			$menuCategory = new menuCategory;
			$menuCategory->idMenuCategory = $row['idMenuCategory'];
			
			if(!empty($row['idRestaurant'])){
				$menuCategory->restaurant = new restaurant;
				$menuCategory->restaurant->idRestaurant = $row['idRestaurant'];
			}
			
			$menuCategory->category = $row['category'];
			$menuCategory->order = $row['order'];
			
			$menuCategories[] = $menuCategory;
		}
		
		return $menuCategories;
	}
	
	function insertMenuCategory($restaurant, $category) {
		global $db;
		
		$query = 'SET @order = (SELECT COUNT(`idMenuCategory`)
			FROM `restaurants-menus-categories`
			WHERE (`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\'));';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$query = 'INSERT INTO `restaurants-menus-categories`
			SET `idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\', 
			`category` = \'' . mysql_real_escape_string($category) . '\', 
			`order` = (@order + 1);';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$menuCategory = new menuCategory;
		$menuCategory->idMenuCategory = mysql_insert_id($db->link);
		$menuCategory->category = $category;
		
		return $menuCategory;
	}
	
	function getMenuCategoryByID($idMenuCategory){
		global $db;
		
		$query = 'SELECT `idMenuCategory`, `idRestaurant`, `category`, `order`
			FROM `restaurants-menus-categories`
			WHERE (`idMenuCategory` = \'' . mysql_real_escape_string($idMenuCategory) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			$row = mysql_fetch_assoc($result);
			
			$menuCategory = new menuCategory;
			$menuCategory->idMenuCategory = $row['idMenuCategory'];
			
			$menuCategory->restaurant = new restaurant;
			$menuCategory->restaurant->idRestaurant = $row['idRestaurant'];
			
			$menuCategory->category = $row['category'];
			$menuCategory->order = $row['order'];
			
			return $menuCategory;
		}else{
			return false;
		}
	}
	
	function updateMenuCategory($menuCategory, $category){
		global $db;
		
		$query = 'UPDATE `restaurants-menus-categories`
			SET `category` = \'' . mysql_real_escape_string($category) . '\'
			WHERE (`idMenuCategory` = \'' . mysql_real_escape_string($menuCategory->idMenuCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function deleteMenuCategory($menuCategory){
		global $db;
		
		$query = 'DELETE FROM `restaurants-menus-categories`
			WHERE (`idMenuCategory` = \'' . mysql_real_escape_string($menuCategory->idMenuCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function reorderMenuCategory($menuCategory, $order){
		global $db;
		
		$query = 'UPDATE `restaurants-menus-categories`
			SET `order` = \'' . mysql_real_escape_string($order) . '\'
			WHERE (`idMenuCategory` = \'' . mysql_real_escape_string($menuCategory->idMenuCategory) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
