<?php
	function listMenuItems($restaurant, $menu, $all, $categories, $sort) {
		global $db;
		
		$query = 'SELECT DISTINCT RMI.`idMenuItem`, 
				RM.`idMenu`, RM.`menuName`, 
				RMC.`idMenuCategory`, RMC.`category`, RMC.`order` AS categoryOrder, 
				RMI.`menuItemCategoryIDs`, RMI.`menuItemName`, RMI.`photo`, RMI.`menuItemDescription`, RMI.`spicy`, RMI.`vegetarian`, RMI.`glutenFree`, RMI.`lactoseFree`, RMI.`recommended`, RMI.`price`, RMI.`discount`, RMI.`order` AS menuItemOrder, RMI.`enabled` 
			FROM `restaurants-menus-items` RMI
			INNER JOIN `restaurants-menus` RM ON RM.`idMenu` = RMI.`idMenu`
				AND (RM.`idRestaurant` = \'' . mysql_real_escape_string($restaurant->idRestaurant) . '\')';
		
		if ($menu) {
			$query .= '
				AND (RM.`idMenu` = \'' . $menu->idMenu . '\')';
		}
		
		$query .= '
			INNER JOIN `restaurants-menus-categories` RMC ON RMC.`idMenuCategory` = RMI.`idMenuCategory`';
		
		if (!$all) {
			$query .= '
			WHERE RMI.`enabled` = \'Y\'';
		}
		
		$query .= '
			ORDER BY ' . ($sort ? $sort : 'categoryOrder ASC, menuItemOrder ASC') . ';';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		$menuItems = array();
		while ($row = mysql_fetch_assoc($result)) {
			$menuItem = new menuItem;
			$menuItem->idMenuItem = $row['idMenuItem'];
			
			$menuItem->menu = new restaurantMenu;
			$menuItem->menu->idMenu = $row['idMenu'];
			$menuItem->menu->menuName = $row['menuName'];
			
			$menuItem->menuCategory = new menuCategory;
			$menuItem->menuCategory->idMenuCategory = $row['idMenuCategory'];
			$menuItem->menuCategory->category = $row['category'];
			
			$menuItem->menuItemName = $row['menuItemName'];
			$menuItem->photo = $row['photo'];
			$menuItem->menuItemDescription = $row['menuItemDescription'];
			$menuItem->spicy = $row['spicy'];
			$menuItem->vegetarian = $row['vegetarian'];
			$menuItem->glutenFree = $row['glutenFree'];
			$menuItem->lactoseFree = $row['lactoseFree'];
			$menuItem->recommended = $row['recommended'];
			$menuItem->price = $row['price'];
			$menuItem->discount = $row['discount'];
			$menuItem->order = $row['menuItemOrder'];
			$menuItem->enabled = $row['enabled'];
			
			if ($categories) {
				$menuItem->menuItemCategories = menuItemCategory::listByMenuItem($menuItem);
				
				if (($group_ids = @json_decode($row['menuItemCategoryIDs']))) {
					foreach ($group_ids as $group_id) {
						if (($menu_option_group = menuItemCategory::get_by_id($group_id))) {
							if ($menu_option_group->order) {
								$menuItem->menuItemGroups[$menu_option_group->order] = $menu_option_group;
							} else {
								$menuItem->menuItemGroups[] = $menu_option_group;
							}
						}
					}
				}
				
			} else {
				if (($group_ids = @json_decode($row['menuItemCategoryIDs']))) {
					foreach ($group_ids as $group_id) {
						if (($menu_option_group = menuItemCategory::get_by_id($group_id))) {
							if ($menu_option_group->order) {
								$menuItem->menuItemGroups[$menu_option_group->order] = $menu_option_group->idMenuItemCategory;
							} else {
								$menuItem->menuItemGroups[] = $menu_option_group->idMenuItemCategory;
							}
						}
					}
				}
			}
			ksort($menuItem->menuItemGroups);
			
			$menuItems[] = $menuItem;
		}
		
		return $menuItems;
	}
	
	function insertMenuItem($menu, $menuCategory, $menuItemName, $photo, $menuItemDescription, $spicy, $vegetarian, $glutenFree, $lactoseFree, $recommended, $price, $discount, $enabled, $menu_option_groups) {
		global $db;
		
		$query = 'SET @order = (SELECT COUNT(`idMenuItem`)
			FROM `restaurants-menus-items`
			WHERE (`idMenu` = \'' . mysql_real_escape_string($menu->idMenu) . '\'));';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$query = 'INSERT INTO `restaurants-menus-items`
			SET `idMenu` = \'' . mysql_real_escape_string($menu->idMenu) . '\', 
			`idMenuCategory` = \'' . mysql_real_escape_string($menuCategory->idMenuCategory) . '\', 
			`menuItemCategoryIDs` = ' . ($menu_option_groups ? '\'' . mysql_real_escape_string(json_encode($menu_option_groups)) . '\'' : 'NULL') . ', 
			`menuItemName` = \'' . mysql_real_escape_string($menuItemName) . '\', 
			`photo` = ' . ($photo ? '\'' . mysql_real_escape_string($photo) . '\'' : 'NULL') . ', 
			`menuItemDescription` = \'' . mysql_real_escape_string($menuItemDescription) . '\', 
			`spicy` = \'' . mysql_real_escape_string($spicy) . '\', 
			`vegetarian` = \'' . mysql_real_escape_string($vegetarian) . '\', 
			`glutenFree` = \'' . mysql_real_escape_string($glutenFree) . '\', 
			`lactoseFree` = \'' . mysql_real_escape_string($lactoseFree) . '\', 
			`recommended` = \'' . mysql_real_escape_string($recommended) . '\', 
			`price` = \'' . mysql_real_escape_string($price) . '\', 
			`discount` = ' . (!empty($discount) ? '\'' . mysql_real_escape_string($discount) . '\'' : '0') . ', 
			`order` = (@order + 1), 
			`enabled` = \'' . mysql_real_escape_string($enabled) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
		
		$menuItem = new menuItem;
		$menuItem->idMenuItem = mysql_insert_id($db->link);
		$menuItem->menuItemName = $menuItemName;
		
		return $menuItem;
	}
	
	function getMenuItemByID($idMenuItem){
		global $db;
		
		$query = 'SELECT RMI.`idMenuItem`, RM.`idMenu`, R.`idRestaurant`, R.`customURL`, RM.`menuName`, RMC.`idMenuCategory`, RMC.`category`, RMI.`menuItemName`, RMI.`photo`, RMI.`menuItemDescription`, RMI.`spicy`, RMI.`vegetarian`, RMI.`glutenFree`, RMI.`lactoseFree`, RMI.`recommended`, RMI.`price`, RMI.`discount`, RMI.`order`, RMI.`enabled`
			FROM `restaurants-menus-items` RMI
			INNER JOIN `restaurants-menus` RM ON RM.`idMenu` = RMI.`idMenu`
			INNER JOIN `restaurants` R ON R.`idRestaurant` = RM.`idRestaurant`
			INNER JOIN `restaurants-menus-categories` RMC ON RMC.`idMenuCategory` = RMI.`idMenuCategory`
			WHERE (RMI.`idMenuItem` = \'' . mysql_real_escape_string($idMenuItem) . '\');';
		$result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
		
		if(mysql_num_rows($result)==1){
			$row = mysql_fetch_assoc($result);
			
			$menuItem = new menuItem;
			$menuItem->idMenuItem = $row['idMenuItem'];
			
			$menuItem->menu = new restaurantMenu;
			$menuItem->menu->idMenu = $row['idMenu'];
			
			$menuItem->menu->restaurant = new restaurant;
			$menuItem->menu->restaurant->idRestaurant = $row['idRestaurant'];
			$menuItem->menu->restaurant->customURL = $row['customURL'];
			
			$menuItem->menu->menuName = $row['menuName'];
			
			$menuItem->menuCategory = new menuCategory;
			$menuItem->menuCategory->idMenuCategory = $row['idMenuCategory'];
			$menuItem->menuCategory->category = $row['category'];
			
			$menuItem->menuItemName = $row['menuItemName'];
			$menuItem->photo = $row['photo'];
			$menuItem->menuItemDescription = $row['menuItemDescription'];
			$menuItem->spicy = $row['spicy'];
			$menuItem->vegetarian = $row['vegetarian'];
			$menuItem->glutenFree = $row['glutenFree'];
			$menuItem->lactoseFree = $row['lactoseFree'];
			$menuItem->recommended = $row['recommended'];
			$menuItem->price = $row['price'];
			$menuItem->discount = $row['discount'];
			$menuItem->order = $row['order'];
			$menuItem->enabled = $row['enabled'];
			
			$menuItem->menuItemCategories = menuItemCategory::listByMenuItem($menuItem);
			
			return $menuItem;
			
		} else {
			return false;
		}
	}
	
	function updateMenuItem($menuItem, $menuCategory, $menuItemName, $photo, $menuItemDescription, $spicy, $vegetarian, $glutenFree, $lactoseFree, $recommended, $price, $discount, $enabled, $menu_option_groups) {
		global $db;
		
		$query = 'UPDATE `restaurants-menus-items`
			SET `idMenuCategory` = \'' . mysql_real_escape_string($menuCategory->idMenuCategory) . '\', 
			`menuItemCategoryIDs` = ' . ($menu_option_groups ? '\'' . mysql_real_escape_string(json_encode($menu_option_groups)) . '\'' : 'NULL') . ', 
			`menuItemName` = \'' . mysql_real_escape_string($menuItemName) . '\'';
		
		if($photo){
			$query .= ', 
			`photo` = \'' . mysql_real_escape_string($photo) . '\'';
		}
		
		$query .= ', 
			`menuItemDescription` = \'' . mysql_real_escape_string($menuItemDescription) . '\', 
			`spicy` = \'' . mysql_real_escape_string($spicy) . '\', 
			`vegetarian` = \'' . mysql_real_escape_string($vegetarian) . '\', 
			`glutenFree` = \'' . mysql_real_escape_string($glutenFree) . '\', 
			`lactoseFree` = \'' . mysql_real_escape_string($lactoseFree) . '\', 
			`recommended` = \'' . mysql_real_escape_string($recommended) . '\', 
			`price` = \'' . mysql_real_escape_string($price) . '\', 
			`discount` = ' . (!empty($discount) ? '\'' . mysql_real_escape_string($discount) . '\'' : '0') . ', 
			`enabled` = \'' . mysql_real_escape_string($enabled) . '\'
			WHERE (`idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function changeMenuItemState($menuItem, $enable) {
		global $db;
		
		$query = 'UPDATE `restaurants-menus-items`
			SET `enabled` = \'' . ($enable ? 'Y' : 'N') . '\'
			WHERE (`idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function deleteMenuItem($menuItem) {
		global $db;
		
		$query = 'DELETE FROM `restaurants-menus-items`
			WHERE (`idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function reorderMenuItem($menuItem, $order) {
		global $db;
		
		$query = 'UPDATE `restaurants-menus-items`
			SET `order` = \'' . mysql_real_escape_string($order) . '\'
			WHERE (`idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	
	function removeMenuItemOptionCategories($menuItem){
		global $db;
		
		$query = 'UPDATE `menus-items-categories` 
			SET `idMenuItem` = NULL 
			WHERE (`idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\');';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	/*
	function addMenuItemOption($menuItem, $menuItemOption){
		global $db;
		
		$query = 'INSERT INTO `restaurants-menus-items-options`
			SET `idMenuItem` = \'' . mysql_real_escape_string($menuItem->idMenuItem) . '\', 
			`idMenuItemOption` = \'' . mysql_real_escape_string($menuItemOption->idMenuItemOption) . '\';';
		mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
	}
	*/
