<?php
    function recommendedRestaurantInsert($zipCode, $email, $restaurant, $ip){
        global $db;
        
        $query = 'INSERT INTO `recommended-restaurants`
            SET `zipCode` = \'' . mysql_real_escape_string($zipCode) . '\', 
            `email` = \'' . mysql_real_escape_string($email) . '\', 
            `restaurant` = \'' . mysql_real_escape_string($restaurant) . '\', 
            `ip` = \'' . mysql_real_escape_string($ip) . '\';';
        mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . '<br />' . mysql_error() : '');
        
        $recommendedRestaurant = new recommendedRestaurant;
        $recommendedRestaurant->idRestaurant = mysql_insert_id($db->link);
        $recommendedRestaurant->zipCode = $zipCode;
        $recommendedRestaurant->email = $email;
        $recommendedRestaurant->restaurant = $restaurant;
        $recommendedRestaurant->ip = $ip;
        $recommendedRestaurant->dateAdded = date('Y-m-d H:i:s');
        
        return $recommendedRestaurant;
    }
?>