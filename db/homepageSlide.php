<?php
    function listHomepageSlides($language){
        global $db;
        
        $query = 'SELECT `idSlide`, `image`, `description`, `url`, `order`
            FROM `homepage-slides`
            WHERE (`idLanguage` = \'' . mysql_real_escape_string($language->idLanguage) . '\')
            ORDER BY `order` ASC;';
        $result = mysql_query($query, $db->link) or die(DEBUG ? nl2br($query) . "\n" . mysql_error() : '');
        
        $homepageSlides = array();
        while($row = mysql_fetch_assoc($result)){
            $homepageSlide = new homepageSlide;
            
            $homepageSlide->idSlide = $row['idSlide'];
            $homepageSlide->image = $row['image'];
            $homepageSlide->description = $row['description'];
            $homepageSlide->url = $row['url'];
            $homepageSlide->order = $row['order'];
            
            $homepageSlides[] = $homepageSlide;
        }
        
        return $homepageSlides;
    }
?>