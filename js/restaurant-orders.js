
$('tr[data-order]').click(function() {
		var row = $(this);
		
		if (!row.next('tr').hasClass('loading') && !row.next('tr').hasClass('order-row')) {
			var new_row = $('<tr class="loading">' + 
						'<td colspan="6"><img class="fb-loading" src="i/dot.png" style="margin:0;position:static" /></td>' + 
					'</tr>');
			
			row.after(new_row);
			$.ajax({
					data		: {
						order : row.data('order')
					}, 
					//dataType	: 'json', 
					success		: function(data) {
						if (data)
							new_row.replaceWith($(data));
						else
							new_row.remove();
					}, 
					url			: root + lang + '/get-order-details.html', 
					type		: 'POST'
				});
			
		} else {
			row.next('tr').remove();
		}
	});


$('a.accept,a.decline').click(function(e) {
		e.stopPropagation();
		
		if (!confirm($(this).hasClass('accept') ? langConfirmAccept : langConfirmDecline))
			return false;
	});
