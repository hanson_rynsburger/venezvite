$('#list-search-form select').select2({
	minimumResultsForSearch	: -1
})
.on('select2-opening', function() {
	$('.select2-drop').css('z-index', (getHighestZIndex() + 1));
});

setupAddressAutocomplete($('#list-search-form').children('form'), true); // We don't submit the form automatically

$.validator.setDefaults({
	ignore: [] // So that it would validate the custom, hidden select fields, too
});

$('#list-search-form form').first().validate({
	rules : {
		'location' : 'required'
	}
});

$('#list-search-form select[name=location]').change(function() {
	if ($(this).val()=='-1') {
		this.disabled = true;
		$(this).next('span.select2').css('display', 'none');
		
		$('#list-search-form input[name=location]')[0].disabled = false;
	}
});

function initGallery() {
	$('#restaurant-gallery ul').empty();
	var gallery_width = 0;
	
	$.each($('#restaurant-gallery img'), function(index, image) {
		var width = $('#restaurant-gallery>div').width();
		gallery_width += width;
		$(image).width(width);
		
		var li = $('<li' + (index===0 ? ' class="selected"' : '') + '></li>');
		li.click(function() {
				var image = $($('#restaurant-gallery img')[index]);
				$('#restaurant-gallery>div').animate({
						scrollLeft: image.position().left
					});
				
				$('#restaurant-gallery li.selected').removeClass('selected');
				$($('#restaurant-gallery li')[index]).addClass('selected');
			});
		$('#restaurant-gallery ul').append(li);
	});

	$('#restaurant-gallery>div>div').width(gallery_width);
}

initGallery();
$(window).resize(initGallery);

$('#restaurant-gallery').on('swipeleft', function() {
	var index = $('#restaurant-gallery li').index($('#restaurant-gallery li.selected'));
	if (index < ($('#restaurant-gallery li').length - 1))
		$($('#restaurant-gallery li')[index + 1]).click();
	
}).on('swiperight', function() {
	var index = $('#restaurant-gallery li').index($('#restaurant-gallery li.selected'));
	if (index > 0)
		$($('#restaurant-gallery li')[index - 1]).click();
});

$('#restaurant-categories-search div.right>strong').click(function() {
	if ($(window).width()<=550)
		$('#restaurant-menu-filter').toggle();
});

$('#restaurant-menu-filter input').click(function(e) {
	$(this).parents('li').first().toggleClass('selected', this.checked);

	if ($(window).width()<=550)
		$('#restaurant-menu-filter').toggle();

	e.stopPropagation();
});

$('#cart-summary-container a.rounded-red-button,#restaurant-main-menu a').click(function() {
	return false;
});

$('#cart-summary-container a.rounded-red-button').click(function() {
	$('html,body').animate({
		scrollTop: ($('#shopping-cart').offset().top - $('#body>header').height())
	}, 1000);
});

$('#restaurant-main-menu a').click(function() {
	var href = $(this).data('href');
	
	if ($(window).width()>974) {
		// Only for desktop
		$.each($('#restaurant-main-menu a'), function(index, link) {
			if ($(link).data('href')!=href) {
				$('#' + $(link).data('href')).animate({
					height: 0
				});

				$(link).removeClass('selected');
			}
		});
	}

	if ($('#' + href).height()==0) {
		$('#' + href).animate({
			height: $('#' + href)[0].scrollHeight
		}, {
			complete: function() {
				$('#' + href).css('height', 'auto');
			}
		});

		$(this).addClass('selected');
	} else if ($(window).width()<=974) {
		// Tablet and lower
		$('#' + href).animate({
			height: 0
		});

		$(this).removeClass('selected');
	}
});

if ($(window).width()>974) {
	// 	Only for desktop
	$(document).ready(function() {
		$('#restaurant-main-menu a').first().click();
		//$('#restaurant-menu-list li li').first().click(); // FOR TEST ONLY!
	});
}


var restaurant_map = new google.maps.Map($('#restaurant-map>div')[0], {
	center				: {
		lat	: parseFloat($('#restaurant-map>div').data('lat')), 
		lng	: parseFloat($('#restaurant-map>div').data('lng'))
	}, 
	disableDefaultUI	: true, 
	maxZoom				: 17, 
	minZoom				: 12, 
	zoom				: 15
});

// Draw the delivery zone polygons
for (var i=0, j=delivery_paths.length; i<j; i++) {
	if (delivery_paths[i]) {
		var delivery_path = [];
		for (var k=0, l=(delivery_paths[i].length - 1); k<l; k++) {
			delivery_path.push(new google.maps.LatLng(delivery_paths[i][k][0], delivery_paths[i][k][1]));
		}
		
		var delivery_zone = new google.maps.Polygon({
			fillColor		: '#FF0000', 
			fillOpacity		: 0.1, 
			map				: restaurant_map, 
			paths			: delivery_path, 
			strokeColor		: '#FF0000', 
			strokeOpacity	: 0.5, 
			strokeWeight	: 1
		});
	}
}

new google.maps.Marker({
	icon		: {
		anchor	: new google.maps.Point(6.5, 29), 
		origin	: new google.maps.Point(13, 0), 
		size	: new google.maps.Size(13, 29), 
		url		: img_root + 'markers.png'
	}, 
	map			: restaurant_map, 
	position	: restaurant_map.getCenter()
});

if ($('#list-search-form input[name=latitude]').val() && $('#list-search-form input[name=longitude]').val()) {
	// We have an order address, so let's add its marker on the map
	var search_location = new google.maps.LatLng(parseFloat($('#list-search-form input[name=latitude]').val()), parseFloat($('#list-search-form input[name=longitude]').val())), 
		bounds = new google.maps.LatLngBounds(restaurant_map.getCenter(), restaurant_map.getCenter());
	
	new google.maps.Marker({
		icon		: {
			anchor	: new google.maps.Point(6.5, 29), 
			origin	: new google.maps.Point(0, 0), 
			size	: new google.maps.Size(13, 29), 
			url		: img_root + 'markers.png'
		}, 
		map			: restaurant_map, 
		position	: search_location
	});

	bounds.extend(search_location);
	restaurant_map.fitBounds(bounds);
}


function searchMenuItems() {
	var search = $('#restaurant-categories-search input[type=text]').val().trim().toLowerCase(), 
		selected_filters = $('#restaurant-categories-search input[type=checkbox]:checked').map(function() {
				return $(this).parents('li').first().attr('class').replace(' selected', '');
			}).get(), 
		category_labels = $('#restaurant-menu-list .category-name'), 
		visible_categories = [], 
		categories_height = 0;
	
	$.each(category_labels, function(index, category_label) {
			var category_row = $(category_label).parent('li'), 
				categories_list = category_row.find('ul'), 
				menu_items = categories_list.find('li'), 
				category_items_height = 0;
			
			$.each(menu_items, function(index, menu_item) {
					var text = $(menu_item).text().toLowerCase().trim(), 
						menu_filter_ids = ($(menu_item).children('div').first().data('filter') ? 
							$(menu_item).children('div').first().data('filter').toString().split(',') : 
							[]);
					
					if (text.indexOf(search)==-1 || (selected_filters.length ? !$.arrayIntersect(selected_filters, menu_filter_ids).length : false)) { // The search itself
						// No match found for the current menu item, we'll hide it
						if ($(menu_item).height()>0) {
							$(menu_item).animate({
									height: 0
								});
						}
						
					} else {
						if ($(menu_item).height()==0) {
							$(menu_item).animate({
									height: menu_item.scrollHeight
								}, {
									complete: function() {
										$(menu_item).css('height', 'auto');
									}
								});
						}
						category_items_height += menu_item.scrollHeight;
					}
				});
			
			if (category_items_height==0) {
				// The current category doesn't have any visible menu items anymore, 
				// so let's hide it completely
				if (category_row.height()>0) {
					category_row.animate({
							height: 0
						});
				}
				
			} else {
				var category_height = $(category_label).outerHeight();
				if ($(window).width()>550 || $(category_label).hasClass('open'))
					// We're only updating the currently open category's menu items
					category_height += category_items_height;
				
				if (category_row.height()!=category_height) {
					category_row.animate({
							height: category_height
						}, {
							complete: function() {
								$(this).css('height', '');
							}
						});
				}
				if (categories_list.height()!=category_items_height && ($(window).width()>550 || $(category_label).hasClass('open'))) {
					categories_list.animate({
							height: category_items_height
						}, {
							complete: function() {
								categories_list.css('height', 'auto');
							}
						});
				}
				
				categories_height += category_height;
				visible_categories.push(category_label);
			}
		});
	
	
	var no_results = $('#restaurant-menu-container #no-results');
	
	if (!visible_categories.length) {
		if (no_results.height()==0) {
			no_results.animate({
				height: no_results[0].scrollHeight
			}, {
				complete: function() {
					no_results.css('height', 'auto');
				}
			});
		}
	} else {
		if (no_results.height()>0) {
			no_results.animate({
				height: 0
			});
		}
	}
}

	$('#restaurant-categories-search input[type=text]').bind('keyup blur', searchMenuItems);

	$('#restaurant-categories-search input[type=text]').bind('paste cut delete', function() {
		var input = this;
		setTimeout(function() {
			searchMenuItems.call(input);
		}, 10);
	});

	$('#restaurant-categories-search input[type=checkbox]').bind('click', searchMenuItems);

	$('#restaurant-menu-list .category-name').click(function() {
		var category_label = $(this), 
			category_row = category_label.parent('li'), 
			category = category_label.next('ul');

		category_label.toggleClass( 'toggle' );
		
		if ($(window).width()<=550) {
			$.each($('#restaurant-menu-list ul'), function(index, category_) {
				var category_label_ = $(category_).prev('.category-name'), 
					category_row_ = category_label_.parent('li');
				
				if (category_label_.hasClass('open')) {
					$(category_).animate({
						height: 0
					});
					category_row_.animate({
						height: category_label_.outerHeight()
					}, {
						complete: function() {
							category_label_.removeClass('open');
							category_row_.css('height', 'auto');
						}
					});
				}
			});

			if (category.height()>0)
		 		// We don't re-open an already displayed category
				return false;
			
			var items_height = 0;
			$.each(category.find('li'), function(index, menu_item) {
				items_height += $(menu_item).height();
			});
			
			category.animate({
				height: items_height
			}, {
				complete: function() {
					category.css('height', 'auto');
				}
			});

			category_row.animate({
				height: (category_label.outerHeight() + items_height)
			}, {
				complete: function() {
					category_label.addClass('open');
					category_row.css('height', 'auto');

					$('html,body').animate({
						scrollTop: category_row.offset().top - 75
					});
				}
			});
		} else {
			category.animate({
				height: (category.height()==0 ? category[0].scrollHeight : 0)
			}, {
				complete: function() {
					if (category.height()!=0)
						category.css('height', 'auto');
				}
			});
		}
	});

$('#menu-item-photo a').click(function() {
	var link = $(this);
	
	if ($('#menu-item-photo>div')[0].scrollHeight>($('#menu-item-photo>div').height() + 1)) { // +1 because of some CSS percentage calculations not being correctly rounded
		$('#menu-item-photo>div').animate({
				height: ($('#menu-item-photo>div')[0].scrollHeight - parseInt($('#menu-item-photo img').css('margin-top')))
			}, {
				complete: function() {
					link.text(langHideImage);
				}, 
				//progress: checkMenuItemDetailsSize
			});
		$('#menu-item-photo img').animate({
				'margin-top': 0
			});
		
	} else {
		$('#menu-item-photo>div').animate({
				height: 0
			}, {
				complete: function() {
					link.text(langViewImage);
				}
			});
		$('#menu-item-photo img').animate({
				'margin-top': -((parseInt($('#menu-item-photo img').css('min-height')) / 100) * $('#menu-item-photo img').height())
			});
	}
});

$('#restaurant-menu-list li li:not(.menu-placeholder)').click(function() {
	// Clear the menu item add panel's fields
	$('#menu-item-details input[name=menu_item_id]').val($(this).data('menu-item-id'));
	$('#menu-item-details input[name=edit]').val('');
	$('#menu-item-quantity input').val('1');
	$('#menu-item-options input').attr('checked', false);
	$('#menu-item-options textarea').val('');
	
	$("#menu-item-options>span.ribbon").show();
	$("#menu-item-options>div").show();
	
	// Set the edit popup's title (in case the previous operation was an edit)...
	$('#menu-item-details>strong').text(langAddItem);
	// ... and the submit button's text
	$('#menu-item-details input[type=submit]').val(langAdd2Basket);
	
	show_modal($('#menu-item-popup'));
	//checkMenuItemDetailsSize();
	
	// Now let's prefill the menu item popup with the current item's details
	var details = $(this).data('details');
	
	if (details.photo) {
		$('#menu-item-photo img').attr('src', img_root + 'restaurants/' + details.photo);
		$('#menu-item-photo').css('display', 'block');
		$('#menu-item-description').removeClass('radius');
	} else {
		$('#menu-item-photo').css('display', 'none');
		$('#menu-item-photo img').attr('src', img_root + 'dot.png');
		$('#menu-item-description').addClass('radius');
	}
	$('#menu-item-description>strong').html($(this).find('.title').html());
	$('#menu-item-description>p').text(details.description);
	$('#menu-item-price')
		.text(details.price)
		.data('base-price', details.price);
	
	$('#menu-item-options>div').empty();
	for (var i=0, j=details.option_categories.length; i<j; i++) {
		var option_category = details.option_categories[i], 
			options_list = [];
		
		for (var k=0, l=option_category.options.length; k<l; k++) {
			var option = option_category.options[k];
			options_list.push('<li><label><input class="custom-radio" data-price="' + option.price + '" name="menu_item_options[]" type="checkbox" value="' + option.id + '" /> <span>' + option.name + '</span> ' + option.price + '</label></li>');
		}
		
		if (options_list.length) {
			var rule = '';
			switch (option_category.rule) {
				case 'E':
					rule = '<span>' + langSelectExact.replace('{x}', option_category.selectables) + '</span>';
					break;
				case 'UT':
					rule = '<span>' + langSelectUpTo.replace('{x}', option_category.selectables) + '</span>';
					break;
			}
			
			$('#menu-item-options>div').append('<strong>' + option_category.name + rule + '</strong>');
			$('#menu-item-options>div').append('<ul data-rule="' + option_category.rule + '" data-selectables="' + option_category.selectables + '">' + options_list.join('') + '</ul>');
		}
	}

	$.each($('#menu-item-options input.custom-radio'), function(index, input) {
		doCustomRadio(input);
	});

	if ( $("#menu-item-options>div").children().length == 0 ) {
		$("#menu-item-options>span.ribbon").hide();
		$("#menu-item-options>div").hide();
	} 
	$(window).resize(); // So that the modal would rearrange itself in the proper location
});
//$(window).resize(checkMenuItemDetailsSize);

	$('#menu-item-options>div').on('click', 'input[name="menu_item_options[]"]', function() {
		var parent = $(this).parents('ul').first(), 
			set = parent.prev('strong'), 
			ruleLabel = set.find('span'), 
			rule = parent.data('rule'), 
			selectables = parent.data('selectables'), 
			selected = parent.find('input:checked').length;
		
		ruleLabel.removeClass('red');
		switch (rule) {
			case 'E':
			case 'UT':
				if (selectables>0 && selected>selectables) {
					ruleLabel.addClass('red');
					this.checked = false; // Refuse a new option in case the maximum number of selections has been reached
				}
				break;
		}
		
		checkMenuItemPrice();
	});

	function checkOptionSets() {
		var valid = true;
		$.each($('#menu-item-options ul'), function(index, list) {
			var set = $(list).prev('strong'), 
				rule = $(list).data('rule'), 
				selectables = $(list).data('selectables'), 
				selected = $(list).find('input:checked').length;
			
			set.find('span').remove();
			switch (rule) {
				case 'E':
					if (selectables>0 && selected!=selectables) {
						set.append('<span class="red">' + langSelectExact.replace('{x}', selectables) + '</span>');
						valid = false;
						return false;
					}
					break;
				case 'AL':
					if (selected<selectables) {
						set.append('<span class="red">' + langSelectAtLeast.replace('{x}', selectables) + '</span>');
						valid = false;
						return false;
					}
					break;
			}
		});
		return valid;
	}

	function checkMenuItemPrice() {
		var base_price = parseFloat($('#menu-item-price').data('base-price')), 
			quantity = parseInt($('#menu-item-quantity input').val());
		
		$.each($('#menu-item-options li'), function(index, item) {
			var option = $(item).find('input');
			
			if (option[0].checked) {
				base_price += parseFloat(option.data('price'));
			}
		});
		
		$('#menu-item-price').text((base_price * quantity).toFixed(2));
	}

	$.each($('#menu-item-quantity a'), function(index, link) {
		$(link).click(function() {
			var value = parseInt($('#menu-item-quantity input').val().trim());

			if (index==0 && value>=2)
				value--;
			else if (index==1)
				value++;

			$('#menu-item-quantity input').val(value);
			checkMenuItemPrice();
		});
	});

	$('#menu-item-popup').submit(function() {
		// Add the menu item to the shopping cart
		if (!checkOptionSets())
			return false;

		$.ajax({
			data		: {
				data : $('#menu-item-popup').serialize()
			}, 
			dataType	: 'json', 
			success		: function(data) {
				//loading.remove();

				if (data) {
					if (data.no_address) {
						show_modal($('#list-search-form'));
						$('#list-search-form span.select2').css('width', ''); // Somebody, for some reason sets it to 100%, and it messes them up							
						$('.pac-container').css('z-index', (parseInt($('#list-search-form').parent('div.modal-container').css('z-index')) + 1));

					} else if (data.success) {
						$('#menu-item-details .close').click();
						loadCartContent(restaurant_url, function() {
							var message = '<strong>' + langItemAdded + '</strong><br />';
							if ($('#checkout-button a.rounded-red-button').hasClass('disabled'))
								message += langMoreMoney;
							else
								message += '<a href="javascript:;">' + langGoCheckout + '</a>';
							show_top_message(message);
							
							setTimeout(function() {
								$('#top-message div>a').click(function() {
										$('#cart-checkout input[type=submit]').click();
									});
							}, 500);

							cartItemCountUpdate();
						});
					}
				};
			}, 
			url			: root + lang + '/cart-add.html', 
			type		: 'POST'
		});

		return false;
	});

	function editCartItem() {
		if (!$('li[data-menu-item-id=' + $(this).data('menu-item-id') + ']').length)
			return;

		var menu_item = $(this).parents('li').first(), 
			quantity = parseInt(menu_item.find('span.quantity').text()), 
			selected_options = menu_item.find('.menu-item-option').map(function() {
				return $(this).data('menu-item-option');
			}).get(),

			instructions = (menu_item.find('em').length ? menu_item.find('em').text() : '');

		// Emulate a click on the currently edited menu item's row, 
		// and then fill it with the current selection's quantity, menu options
		// and comment
		$('li[data-menu-item-id=' + $(this).data('menu-item-id') + ']').first().click();	// FIRST because the same menu item could be listed twice
																							// (if it's also recommended)

		// Change the edit popup's title...
		$('#menu-item-details>strong').text(langEditItem);
		// ... and the submit button's text
		$('#menu-item-details input[type=submit]').val(langUpdateBag);

		$('#menu-item-details input[name=edit]').val($('#shopping-cart-items>li').index(menu_item)); // What cart item we're editing
		$('#menu-item-quantity input').val(quantity);
		var menu_options = $('#menu-item-options>div input[type=checkbox]');
		for (var i=0, j=menu_options.length; i<j; i++) {
			if ($.inArray(parseInt($(menu_options[i]).val()), selected_options)>-1)
				menu_options[i].checked = true;
		}
		$('#menu-item-options textarea').val(instructions);

		checkMenuItemPrice();
		return false; // So that the actual link href won't trigger
	}

	//$('#shopping-cart-items a.edit').click(editCartItem);
	//loadCartContent(restaurant_url);

	if (location.hash.indexOf('#no-menu-items')===0) {
		// Display the "Your shopping cart is empty" message
		show_top_message('Your shopping cart is empty.');

		$('html,body').animate({
				scrollTop: ($('#shopping-cart').offset().top - $('#body>header').height() - $('#top-message').height())
			}, 1000);
		location.hash = ''; // Delete the hash
	}

	$(window).scroll(function() {
		if (document.body.scrollTop > ($('#restaurant-intro-image').outerHeight() - $('#body>header').outerHeight())) {
			$('#shopping-cart-container').css({
				position	: 'fixed', 
				top			: $('#body>header').outerHeight()
			});
		} else {
			$('#shopping-cart-container').css({
				position	: 'absolute', 
				top			: ''
			});
		}
	});

	function rearrangeLayout() {
		// Rearrange the layout, depending on the window's width
		if ($(window).width()<=974) {
			if (!$('#restaurant-map').next('#restaurant-details .details-panel:first-of-type>div.left').length)
				$('#restaurant-map').after($('#restaurant-details .details-panel:first-of-type>div.left'));
		} else {
			if (!$('#restaurant-details .details-panel:first-of-type>div.left').next('#restaurant-map').length)
				$('#restaurant-details .details-panel:first-of-type>div.left').after($('#restaurant-map'));
		}

		if ($(window).width()<=550)
			restaurant_map.setZoom(16);
		else
			restaurant_map.setZoom(15);

		var center = restaurant_map.getCenter();
		google.maps.event.trigger(restaurant_map, 'resize');
		restaurant_map.setCenter(center);
	}

	$(window).resize(rearrangeLayout);

	$(document).ready(function() {
		if ($(window).width()<=550) {
			$('#restaurant-menu-list .category-name').first().click();
		}

		rearrangeLayout();

		$(document).on('click', "ul.review-pagination > li > a", function() {
			var self = $(this);

			self.closest( '.bg-container' ).addClass('loading');
			$.ajax({
				data		: {
					rpgn : $(this).attr('data-page'),
					ajax : 1
				}, 
				success		: function(data) {
					if (data) {
						$("#restaurant-reviews").html(
							$(data).find(".main-content")
						);
					}
				}
			}).always( function() {
				self.closest( '.bg-container' ).removeClass('loading');
			});
		});

		$(document).on('click', "#restaurant-reviews a.write-review", function() {
			show_modal( $("#write-review-modal") );
			$(document).trigger('resize');
			$("#write-review-modal .slider-container").flexslider( 0 );
		});

		$("#write-review-modal .slider-container").flexslider({
			animation: "slide",
			slideshow: false,
			touch: false,
			keyboard: false,
			smoothHeight: true,
			controlNav: false
		});

		$(document).on('click', "div.skip > a", function() {
			$("#write-review-modal .slider-container li.flex-active-slide input:checked").prop( 'checked', '' );
			$("#write-review-modal .slider-container").flexslider( "next" );
		});

		$(document).on('click', '.yes-no input[type=radio]', function() {
			$("#write-review-modal .slider-container").flexslider( "next" );
		})

		$(document).on('keyup', '.modal-container textarea.review-content', function() {
			$("span.left-characters").text( 2000 - $(this).val().length );
		});

		$(document).on('click', '.submit-review', function(e) {
			var obj = $(this);
			obj.parent().parent().find("textarea.review-content").removeClass('error');

			if ( obj.parent().parent().find("textarea.review-content").val().split(' ').length < 3 ) {
				setTimeout( function() {
					obj.parent().parent().find("textarea.review-content").addClass('error');
				}, 300 );
			}
			else {
				obj.addClass('disabled');

				$.ajax({
					data		: obj.closest('form').serialize(),
					type		: 'POST',
					success		: function(data) {
						if (data) {
							$("#write-review-modal .slider-container").flexslider( "next" );

							$("#restaurant-reviews").html(
								$(data).find(".main-content")
							);
						}
					}
				}).always( function() {
					obj.removeClass('disabled');
				});
			}

			e.preventDefault();
			e.stopPropagation();
			return false;
		});
		
		$(document).on('click', "#favorite a.to-be-faved", function() {
			var self = $(this);
			self.addClass('disabled');
			
			$.ajax({
				data		: {
					favoriting: '1'
				},
				type		: 'POST',
				success		: function(data) {
					if (data == 'success') {
						self.removeClass('to-be-faved').addClass('faved').addClass('to-be-unfaved').text( self.data('faved-lang') );
					}
				}
			}).always( function() {
				self.removeClass('disabled');
			});
		} );
		
		$(document).on('click', "#favorite a.to-be-unfaved", function() {
			var self = $(this);
			self.addClass('disabled');
			$.ajax({
				data		: {
					favoriting: '0'
				},
				type		: 'POST',
				success		: function(data) {
					if (data == 'success') {
						self.removeClass('to-be-unfaved').removeClass('faved').addClass('to-be-faved').text( self.data('unfaved-lang') );
					}
				}
			}).always( function() {
				self.removeClass('disabled');
			});
		} );

		$(document).on('click', "#favorite a.open-login-modal", function() {
			show_modal($('#login-panel'));
		});
	});