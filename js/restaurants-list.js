$( document ).ready( function() {
	/* initialize */

	/* initialize address autocomplete */
	setupAddressAutocomplete($('#list-search-form').parent('form'));

	// Display the '+ Show more' link, in case of too many cuisine types
	if ($('#list-filter-form #cuisines-filter').length && $('#list-filter-form #cuisines-filter')[0].scrollHeight>$('#list-filter-form #cuisines-filter').height())
		$('#show-more-cuisines').css('display', 'block');

	if ($('#list-filter-form #cuisines-filter').length) {
		for (var i=0, j=$('#list-filter-form #cuisines-filter').children('li').length; i<j; i++) {
			var cuisine = $($('#list-filter-form #cuisines-filter').children('li')[i]), 
				input = cuisine.find('input'), 
				text = cuisine.text(), 
				checked = input[0].checked;

			var new_cuisine = "<li";
			if ( checked ) {
				new_cuisine += " class=\"selected\"";
			}
			new_cuisine += ">" + text + "</li>"; 
			$('.slide-2 ul.filters').append($(new_cuisine));
		}
	}

	/* --- Initialize Events --- */

	// Expand or contract the sort / filter panel's cuisines list, on desktop
	$('#show-more-cuisines').click(function() {
		var link = $(this), 
			list = link.prev('ul'), 
			less = (list[0].scrollHeight>list.height()), 
			height = list.height();

		if (!list[0].minHeight) {
			list[0].minHeight = height;
			
			list.css({
				'height'		: height + 'px', 
				'max-height'	: 'none'
			});
		}

		list.animate({
			height: (less ? list[0].scrollHeight : list[0].minHeight)
		}, {
			complete	: function() {
				if (less)
					link.text(langShowLess);
				else
					link.text(langShowMore);
			}
		});

		var difference = (less ? (list[0].scrollHeight - list.height()) : (list[0].minHeight - list[0].scrollHeight));
		$('#list-filter-form').animate({
			'height': (parseInt($('#list-filter-form').css('height')) + difference)
		});

		$('#list-body #left-banner').animate({
			'padding-top': (parseInt($('#list-body #left-banner').css('padding-top')) + difference)
		});
	});

	// Clear the sort / filter selections, on desktop
	$('#list-filter-form .filter-caption a').click(function() {
		$(this).parent().parent().find( 'input' ).attr( 'checked', false );
		$("#restaurant-list-form").submit();
	});

	$('#list-search-form input[type=radio],#list-search-form input[type=checkbox]').click(function() {
		if ($(window).width()<768)
			return false;	
		$("#restaurant-list-form").submit();
	});

	$.each($('#results-list li'), function(index, row) {
		var link = $(row).find('a.rounded-green-button,a.rounded-red-button');

		$(row).click(function() {
			document.location = link[0].href;
		});
	});

	$(".list-filter-cuisine-types > ul > li input, .list-filter-rating > ul > li input, .list-filter-price > ul > li input").click( function() {
		$("#restaurant-list-form").submit();
	} );

	$(window).scroll( function() {
		if ( $("#list-body>.main-content>.clearfix>.left-sidebar").outerHeight() < $("#list-body>.main-content>.clearfix>.right-contents").outerHeight() ) {
			var windowTop = $( window ).scrollTop();

			if ( windowTop > $("#list-search-form").height() ) {
				$("#list-body div.left-sidebar").addClass( 'following' );

				var rightcontent = $("#list-body .right-contents");
				if ( windowTop + $("#list-body .left-sidebar").height() + 75 > rightcontent.outerHeight() + rightcontent.offset().top ) {
					$("#list-body div.left-sidebar").addClass( 'end' );
				}
				else {
					$("#list-body div.left-sidebar").removeClass( 'end' );
				}
			}
			else {
				$("#list-body div.left-sidebar").removeClass( 'following' );
			}
		}
	} );

	var googleMapCustomStyle = [{featureType:"administrative",elementType:"labels.text.fill",stylers:[{color:"#444444"}]},{featureType:"landscape",elementType:"all",stylers:[{color:"#f2f2f2"}]},{featureType:"poi",elementType:"all",stylers:[{visibility:"off"}]},{featureType:"road",elementType:"all",stylers:[{saturation:-100},{lightness:45}]},{featureType:"road.highway",elementType:"all",stylers:[{visibility:"simplified"}]},{featureType:"road.arterial",elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"transit",elementType:"all",stylers:[{visibility:"off"}]},{featureType:"water",elementType:"all",stylers:[{color:"#c1c8cc"},{visibility:"on"}]}];

	/* View Map Event Handler */
	var user_location = new google.maps.LatLng(parseFloat($('#list-search-form input[name=latitude]').val()), parseFloat($('#list-search-form input[name=longitude]').val())), 
		restaurants_bounds, 
		map;

	$('#view-map').click(function() {
		var hidden = ($('#map-container').height()==0);
		
		if (!map) {
			// The map hasn't been initialized yet, so let's
			map = new google.maps.Map($('#restaurants-map')[0], {
				center	: user_location, 
				maxZoom	: 17, 
				minZoom	: 12, 
				zoom	: 14
			});

			map.setOptions( {styles: googleMapCustomStyle} );

			new google.maps.Marker({
					icon		: {
						anchor	: new google.maps.Point(6.5, 29), 
						origin	: new google.maps.Point(0, 0), 
						size	: new google.maps.Size(13, 29), 
						url		: img_root + 'markers.png'
					}, 
					map			: map, 
					position	: user_location, 
					title		: $('#list-search-form input[name=location]').val()
				});

			if (restaurant_locations.length) {
				restaurants_bounds = new google.maps.LatLngBounds(user_location, user_location);

				for (var i=0, j=restaurant_locations.length; i<j; i++) {
					var center = new google.maps.LatLng(parseFloat(restaurant_locations[i][1]), parseFloat(restaurant_locations[i][2])), 
						marker = new google.maps.Marker({
							icon		: {
								anchor	: new google.maps.Point(6.5, 29), 
								origin	: new google.maps.Point(13, 0), 
								size	: new google.maps.Size(13, 29), 
								url		: img_root + 'markers.png'
							}, 
							id			: restaurant_locations[i][0], 
							map			: map, 
							position	: center, 
							title		: restaurant_locations[i][3]
						});

					google.maps.event.addListener(marker, 'click', function(event) {
							$('html,body').animate({
									scrollTop: ($('#restaurant-' + this.id).offset().top - $('#body>header').height())
								}, 1000);
						});
			 		
			 		restaurants_bounds.extend(center);
				}

				map.fitBounds(restaurants_bounds);
			}
		}

		$('#map-container').animate({
				height:	(hidden ? $('#map-container').css('max-height') : 0)
			}, {
				complete	: function() {
					$('#view-map strong').text(hidden ? langHideMap : langViewMap);
					
					google.maps.event.trigger(map, 'resize');
					map.setCenter(restaurants_bounds ? restaurants_bounds.getCenter() : user_location);
					setFilterTop();
				}, 
				/* progress	: function() {
					
				} */
			});
	});

	/* Google Map Markers */
	if ($('#venezvite-cities').length) {
		// No results, displaying the map where Venezvite is currently in
		// First, let's get the cities list
		map = new google.maps.Map($('#venezvite-cities>div')[0], {
			center	: user_location, 
			maxZoom	: 17, 
			minZoom	: 4, 
			zoom	: 14
		});

		map.setOptions( {styles: googleMapCustomStyle} );

		var cities_bounds = new google.maps.LatLngBounds;
		$.each($('#venezvite-cities ul>li'), function(index, city) {
				var latlng = new google.maps.LatLng($(city).data('latlng').lat, $(city).data('latlng').lng), 
					marker = new google.maps.Marker({
						icon		: {
							anchor	: new google.maps.Point(6.5, 29), 
							origin	: new google.maps.Point(13, 0), 
							size	: new google.maps.Size(13, 29), 
							url		: img_root + 'markers.png'
						}, 
						map			: map, 
						position	: latlng, 
						title		: $(city).text()
					});

				google.maps.event.addListener(marker, 'click', function(event) {
						document.location = $(city).find('a').attr('href');
					});

				cities_bounds.extend(latlng);
			});

		map.fitBounds(cities_bounds);
	}
	/* End of Google Map Markers */

	/* Scroll To Advanced Orders */
	$('#see-advanced-orders').click(function() {
		$('html,body').animate({
			scrollTop: ($('#results-list h2').offset().top - $('#body>header').height())
		}, 1000);
	});

	if ($('#recommend-restaurant').length) {
		var rform = $('#recommend-restaurant').wrap( "<form></form>" ).parent();
		
		rform.validate({
			rules			: {
				'recommended_zip_code'		: {
					required	: true
				}, 
				'recommended_email'			: {
					email		: true, 
					required	: true
				}, 
				'recommended_restaurant'	: {
					required	: true
				}
			}, 
			submitHandler: function(form) {
				var loading = $('<img class="loading" src="i/dot.png" />');
				$('#recommend-restaurant input[type=submit]').after(loading);

				$.ajax({
						data		: rform.serialize(), 
						dataType	: 'json',
						url			: document.location.href, 
						type		: 'POST',
						success		: function(data) {
							loading.remove();
							if (data && data.message) {
								$('#recommend-restaurant input[type=text]').val('');
								show_top_message(data.message);
							}
						}, 
				});

				return false;
			}
		});
	}

	$("#list-filter-handle").click( function() {
		if ( $( "#list-search-form" ).hasClass( 'visible' ) ) {
			$( "#list-search-form" ).css( 'overflow', 'hidden' );
			$( "#list-search-form" ).removeClass( 'visible' );
		}
		else {
			$( "#list-search-form" ).addClass( "visible" );
			setTimeout( function() {
				$( "#list-search-form" ).css('overflow', 'visible');
			}, 200 );
		}
	});

	$("#find-mobile").click( function() {
		$("input[name=time]").val( $("input[name=time_m]").val() );
		$("input[name=date]").val( $("input[name=date_m]").val() );
	});

	$("#list-sort-handle").click( function() {
		$("#mobile-filters").toggleClass( 'visible' );
	} );

	/* --- mobile sort filter --- */
	$( ".slide-1 a.big-white-button" ).click( function() {
		// $("#mobile-filters").removeClass( 'visible' );
		selectCustomDropDown( "sort_m", "distance" );
		selectCustomDropDown( "reviews", "" );
		selectCustomDropDown( "prices", "" );
		$(".slide-2 .big-white-button").trigger( 'click' );
	} );

	$( ".slide-1 a.cuisines" ).click( function() {
		$(this).closest( "#mobile-filters").addClass( "second-slide" );
	} );

	$(".slide-1 a.btn-cancel").click( function() {
		$("#mobile-filters").removeClass( 'visible' );
	} );
	
	$("#mobile-filter-notice a").click( function() {
		$("input[name=price_level]").prop('checked', false);
		$("input[name=rating]").prop('checked', false);
		$("#cuisines-filter input[type=checkbox]").prop( 'checked', false );
		
		$('form#restaurant-list-form').submit();
	});

	$(".slide-1 a.btn-apply").click( function() {
		$("#mobile-filters").removeClass( 'visible' );

		/* setting actual values for filters */
		$( "input[name=sort]").val( $("input[name=sort_m").val() );
		$("input[name=rating]").prop('checked', false);
		if ( $( "input[name=reviews]").val() )
			$("input[name=rating][value=" + $( "input[name=reviews]").val( ) + "]").prop('checked', true);

		$("input[name=price_level]").prop('checked', false);
		if ( $( "input[name=prices]").val( ) )
			$("input[name=price_level][value=" + $( "input[name=prices]").val( ) + "]").prop('checked', true);

		$(".slide-2 ul.filters li").prop('checked', false);
		$(".slide-2 ul.filters li").each( function( idx, val ) {
			if ( $(val).hasClass('selected') ) {
				$( $("#cuisines-filter input[type=checkbox]")[idx] ).prop( 'checked', true );
			} 
		} );

		$('form#restaurant-list-form').submit();
		/* --- end --- */
	} );
	/* --- end of mobile cuisine --- */

	/* --- mobile cuisine --- */
	$( document ).on( 'click', ".slide-2 ul.filters li", function() {
		$(this).toggleClass( 'selected' );
	} );

	$(".slide-2 input[name=search_cuisines]").on( 'keyup', function() {
		var txt = $(".slide-2 input[name=search_cuisines]").val();
		$(".slide-2 ul.filters li").hide();
		$(".slide-2 ul.filters li").each( function( idx, val ) {
			if ( $(val).text().indexOf( txt ) >= 0 ) {
				$(val).show();
			}
		});
	} );  

	$(".slide-2 .big-white-button").click( function() {
		$(".slide-2 ul.filters li").removeClass( 'selected' );
	} );

	$(".slide-2 a.btn-cancel").click( function() {
		$('.slide-2 ul.filters li').removeClass( 'selected' );
		
		for (var i=0, j=$('#list-filter-form #cuisines-filter').children('li').length; i<j; i++) {
			var cuisine = $($('#list-filter-form #cuisines-filter').children('li')[i]), 
				input = cuisine.find('input'), 
				checked = input[0].checked;

			if ( checked ) {
				$( $('.slide-2 ul.filters li')[i] ).addClass('selected');
			} 
		}
		
		$(this).closest( "#mobile-filters").removeClass( "second-slide" );
	} );

	$(".slide-2 a.btn-apply").click( function() {
		$(this).closest( "#mobile-filters").removeClass( "second-slide" );
	} );
	/* --- end of mobile cuisine --- */

	/* custom drop down change event */
	$( ".h-drop-down-type" ).on( 'hs-dropdown.on.change', function( e, el, val ) {
		$(el).closest('form').submit();
	} );

	$( ".h-drop-down-location" ).on( 'hs-dropdown.on.change', function( e, el, val ) {
		if ( val == "-1" ) {
			$(el).closest( ".h-drop-down-location" ).remove();
			$('#list-search-form input[name=location]').show();
			$('#list-search-form input[name=location]').removeAttr( 'disabled' );

			/* initialize address autocomplete */
			setupAddressAutocomplete( $('#restaurant-list-form') );
		}
		else {
			$(el).closest('form').submit();
		}
	} );

	$( ".h-drop-down-time, .h-drop-down-date, .h-drop-down-sort" ).on( 'hs-dropdown.on.change' , function( e, el, val ) {
		$("#restaurant-list-form").submit();
	} );

	/* custom functions */
	function setFilterTop() {
		var paddingTop = ($('#list-filter-form').height()>0 ? $('#list-filter-form').height() + parseInt($('#list-filter-form').css('margin-bottom')) : 0);
		$('#list-body #left-banner').css('padding-top', paddingTop);
	}
});