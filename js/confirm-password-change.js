
$('#confirm-password-change-body form').first().validate({
		rules			: {
			'new_password'			: {
				minlength	: 5, 
				required	: true
			}, 
			'confirm_new_password'	: {
				equalTo		: '#confirm-password-change-body input[name=new_password]'
			}
		}
	});
