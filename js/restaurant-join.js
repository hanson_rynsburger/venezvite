
function invalidAddress() {
	$('#restaurant-join-body form')[0].submitted = false;
	show_top_message(langInvalidAddress.replace('{address}', $('#restaurant-join-body input[name=address]').val() + ', ' + $('#restaurant-join-body input[name=city]').val()));
	
	$('html,body').animate({
			scrollTop: ($('#restaurant-join-body input[name=address]').offset().top - $('#body>header').height())
		}, 500);
}

function getLocationCoords() {
	if (typeof google!='undefined') {
		var country_abbr = $('select[name=country] option:selected').data('abbr'), 
			geocoder = new google.maps.Geocoder();
		
		geocoder.geocode({
				address					: ($('#restaurant-join-body input[name=address]').val() + ', ' + $('#restaurant-join-body input[name=city]').val()), 
				componentRestrictions	: {
					country : country_abbr
				}
			}, 
			function(results, status) {
				if (status==google.maps.GeocoderStatus.OK) {
					var found_address = false;
					
					for (var i=0, j=results[0].address_components.length; i<j; i++) {
						var address_component = results[0].address_components[i];
						
						if ($.arrayIntersect(['street_number', 'street_address', 'route'], address_component.types).length) {
							found_address = true;
							break;
						}
					}
					
					if (found_address) {
						$('#restaurant-join-body input[name=latitude]').val(results[0].geometry.location.lat());
						$('#restaurant-join-body input[name=longitude]').val(results[0].geometry.location.lng());
						
						$('#restaurant-join-body form')[0].submit();
						
					} else
						invalidAddress();
					
				} else
					invalidAddress();
			});
	};
}

$('select[name=country]').change(function() {
		var country_abbr = $('select[name=country] option:selected').data('abbr'), 
			city = $('#restaurant-join-body input[name=city]').clone(); // So that we could use it as the source for a new Autocomplete setup
		
		$('#restaurant-join-body input[name=city]').next('label.error').remove();
		$('#restaurant-join-body input[name=city]').remove();
		$('#restaurant-join-body input[name=address]').after(city);
		
		var autocomplete = new google.maps.places.Autocomplete(city[0], {
				componentRestrictions: {
					country: country_abbr
				}, 
				types: ['(cities)']
			});
		/*
		google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				
				if (place.geometry) {
					//console.log(place.address_components);
					$('#restaurant-join-body input[name=zip]').val('');
					for (var i=0, j=place.address_components.length; i<j; i++) {
						if ($.inArray('postal_code', place.address_components[i].types)>-1)
							$('#restaurant-join-body input[name=zip]').val(place.address_components[i].long_name);
					}
				}
			});*/
	});
$('select[name=country]').change();


function updateDeliveryZonesState() {
	if ($('input[name=delivery]')[0].checked && !$('input[name=custom_delivery]')[0].checked) {
		$('#delivery-zones')
			.css('display', 'list-item')
			.animate({
					height: $('#delivery-zones')[0].scrollHeight
				}, {
					complete: function() {
						$('#delivery-zones').css('height', 'auto');
					}, 
					start	: function() {
						$('#delivery-zones').find('input,select').removeAttr('disabled');
						$('#delivery-zones').find('span.select2').css('width', '');
					}
				});
	} else {
		$('#delivery-zones')
			.animate({
					height: 0
				}, {
					complete: function() {
						$('#delivery-zones').css('display', 'none');
						$('#delivery-zones').find('input,select').attr('disabled', 'disabled');
					}
				});
	}
}

$('input[name=delivery]').click(function() {
		if (!this.checked)
			$('input[name=custom_delivery]')[0].checked = false;
		
		updateDeliveryZonesState();
	});
$('input[name=custom_delivery]').click(function() {
		if (this.checked)
			$('input[name=delivery]')[0].checked = true;
		
		updateDeliveryZonesState();
	});


function doCustomSelect(select) {
	function addMultiplePlaceholder() {
		if ($(this).data('placeholder')) {
			var select2 = $(this).next('span.select2');
			select2.find('input.select2-search__field').attr('placeholder', $(this).data('placeholder'));
		}
	}
	
	$(select)
		.select2({
			minimumResultsForSearch	: -1
		})
		.on('select2:select', addMultiplePlaceholder)
		.on('select2:unselect', addMultiplePlaceholder);
}
$.each($('#restaurant-join-body select'), function(index, select) {
		doCustomSelect(select);
	});


$('#restaurant-join-body').on('change', 'select.hour_start,select.hour_end', function() {
		var _this = $(this), 
			parent = _this.parents('div.time').first(), 
			_other = parent.find('select.hour_' + (_this.hasClass('hour_start') ? 'end' : 'start'));
		
		if (_this.val()=='' || 
			_other.val()=='' || 
			(_this.hasClass('hour_start') && 
				parseInt(_this.val().replace(':', '')) > parseInt(_other.val().replace(':', ''))) || 
			(_this.hasClass('hour_end') && 
				parseInt(_this.val().replace(':', '')) < parseInt(_other.val().replace(':', '')))) {
			
			if (_other.val() != _this.val()) {
				_other.val(_this.val());
				_other.change();
			}
		}
		
		if ($('#timetable input[type=checkbox]')[0].checked) {
			// Get all of the current select's siblings and update their values accordingly
			var siblings = $('#timetable div.' + parent[0].className.replace(/\s/g, '.') + ' select.' + _this[0].className.replace(/\s/g, '.')), 
				current_index = $('#timetable div.' + parent[0].className.replace(/\s/g, '.')).index(parent);
			
			$('#timetable input[type=checkbox]')[0].checked = false;
			$.each(siblings, function(index, select) {
					if (current_index!=index) {
						$(select).val(_this.val());
						$(select).change();
					}
				});
			setTimeout(function() {
					$('#timetable input[type=checkbox]')[0].checked = true;
				}, 50);
		}
	});
$('#timetable input[type=checkbox]').click(function() {
		if (this.checked) {
			// Get the last custom value selected, and replicate it across all of its siblings
			var first;
		 	$.each($('#timetable div.first.time'), function(index, container) {
		 			if ($(container).find('select.hour_start').val() || $(container).find('select.hour_end').val())
		 				first = $(container);
			 	});
			first.find('select.hour_start').change();
			setTimeout(function() {
					first.find('select.hour_end').change();
				}, 75);
			
			var second;
		 	$.each($('#timetable div.time.second'), function(index, container) {
		 			if ($(container).find('select.hour_start').val() || $(container).find('select.hour_end').val())
		 				second = $(container);
				});
			if (second) {
				second.find('select.hour_start').change();
				setTimeout(function() {
						second.find('select.hour_end').change();
					}, 75);
			}
		}
	});

$('#restaurant-join-body div.time>a').click(function() {
		var link = $(this), 
			row = $(this).parents('div.time').first();
		
		if (link.text()=='+' && !row.next('div.time').hasClass('second')) {
			// Add
			var clone = row.clone();
			/*
			clone.find('select')
				.removeClass('select2-hidden-accessible')
				.removeAttr('aria-hidden');*/
			clone.find('span:not(.select2)').text('');
			clone.find('span.select2').remove();
			clone.find('a').remove();
			clone.find('label.error').remove();
			clone.removeClass('first').addClass('second');
			$.each(clone.find('select'), function(index, select) {
					select.name = $(clone.find('select')[index]).attr('name').replace(/\[0\]/g, '[1]');
				});
			row.after(clone);
			
			$.each(clone.find('select'), function(index, select) {
					doCustomSelect(select);
					injectRoundedSelect(select);
				});
			link.text('-');
			
		} else if (link.text()=='-' && row.next('div.time').hasClass('second')) {
			// Remove
			row.next('div.time').remove();
			link.text('+');
		}
	});


$('#delivery-zones>a').click(function() {
		var html = $('#delivery-zones>div').first()
			.clone().outerHtml()
			.replace(/1/g, $('#delivery-zones>div').length + 1)
			.replace(/\[0\]/g, '[' + $('#delivery-zones>div').length + ']'), 
			clone = $(html);
		 
		clone.find('span.select2').remove();
		clone.find('label.error').remove();
		
		$('#delivery-zones>div:last-of-type').after(clone);
		
		doCustomSelect(clone.find('select.rounded-select')[0]);
		injectRoundedSelect(clone.find('select.rounded-select')[0]);
	});

$.validator.setDefaults({
		ignore: [] // So that it would validate the custom, hidden select fields, too
	});

$.validator.addMethod('phoneno', function(value, element) {
		var filter = /^\+?\d+(\s?\d*)*\d+$/ig;
		return (filter.test(value) && value.length>=12);
	}, langEnterValidPhone);

var form_validator = $('#restaurant-join-body form').first().validate({
		errorPlacement	: function(error, element) {
			if (element.hasClass('rounded-select'))
				error.insertAfter(element.next('span.select2'));
			else if ( element.attr('name') == 'phone') 
				error.insertAfter(element.parent());
			else if ( element.attr('type') == 'checkbox') 
				element.closest('label').append( error );
			else
				error.insertAfter(element);
		}, 
		messages		: {
			'username'	: {
				remote: langEmailInUse
			}, 
			'website'	: {
				full_url: $.validator.messages.url
			}
		}, 
		rules			: {
			'last_name'			: 'required', 
			'first_name'		: 'required', 
			/*'email'				: {
				email		: true, 
				required	: true
			}, */
			'mobile'			: 'required', 
			
			'country'			: 'required', 
			'restaurant'		: 'required', 
			'address'			: 'required', 
			'city'				: 'required', 
			'zip'				: 'required', 
			'phone'				: {
				//minlength	: 5, 
				phoneno		: true, 
				required	: true
			}, 
			'website'			: {
				//required	: true, 
				full_url	: true
			}, 
			
			'cuisines[]'		: 'required', 

			'zone_range[]'		: 'number', 
			'zone_minimum[]'	: 'number', 
			'zone_fee[]'		: 'number', 
			
			'username'			: {
				email		: true, 
				required	: true, 
				remote		: {
					data	: {
						username: function() {
							return $('#restaurant-join-body input[name=username]').val();
						}
					}, 
					type	: 'POST', 
					url		: root + lang + '/check-unique-username.html'
				}
			}, 
			'confirm_username'	: {
				equalTo		: '#restaurant-join-body input[name=username]'
			}, 
			'password'			: {
				minlength	: 5, 
				required	: true
			}, 
			'confirm_password'	: {
				equalTo		: '#restaurant-join-body input[name=password]'
			}, 
			'terms'			: 'required'
		}, 
		submitHandler	: function(form) {
			// Prevent multiple sumbits
			if (!form.submitted)
				form.submitted = true;
			else
				return false;
			
			
			$('#error-container').animate({
					height: 0
				}, {
					complete: function() {
						$('#error-container>div>span').text('');
					}
				});
			
			getLocationCoords();
		}
	});

	$('#restaurant-join-body form').first().submit(function() {
		var form = $(this);
		
		setTimeout(function() {
				$.each(form.find('label.error'), function(index, label) {
						if (label.style.display!='none') {
							var for_ = $(label).attr('for'), 
								element = $('input[name="' + for_ + '"],select[name="' + for_ + '"],textarea[name="' + for_ + '"]').first();
							
							var top = element.offset().top;
							if (element.css('display')=='none') {
								element.css('display', 'block');
								top = element.offset().top;
								element.css('display', '');
							}
							
							$('html,body').animate({
									scrollTop: (top - $('#body>header').height())
								}, 500);
							
							return false;
						}
					});
			}, 10);
	});

$(document).ready(function() {
		$("input[name=phone]").intlTelInput({
				autoHideDialCode	: false, 
				initialCountry		: 'ch', 
				nationalMode		: false, 
				onlyCountries		: ['ch', 'it', 'fr', 'pt'/*, 'us', 'gb', 'sw', 'de', 'no', 'nl', 'pl', 'fl'*/]
			});
	});
