
// Transform all of the CC forms select fields into custom ones
$('.white-container .rounded-select').select2({
		minimumResultsForSearch	: -1
	});


$('.white-container .rounded-red-button.delete').click(function() {
		if (confirm('Are you sure you want to remove your stored CC from your Venezvite account?')) {
			var id = $(this).data('id'), 
				container = $(this).parents('.white-container').first();
			
			$.ajax({
					data		: {
						del_ : id
					}, 
					dataType	: 'json', 
					success		: function(data) {
						//loading.remove();
						
						if (data) {
							if (data.success) {
								container.remove();
								
								if (!$('.white-container').length)
									location.reload();
							}
						}
					}, 
					url			: root + lang + '/my-ccs.html', 
					type		: 'POST'
				});
		}
	});

//$('#add-cc-button').first().click();
