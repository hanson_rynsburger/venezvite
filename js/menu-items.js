
$('#add-menu-item-form select').select2({
		minimumResultsForSearch	: -1
	});


$('#add-menu-item-form select[name=category]').change(function() {
		if (this.value=='-1')
			// Create a new category
			window.open(root + lang + '/menu-categories.html', '_self');
	});


$('#select-photo').click(function() {
		$(this).siblings('input[type=file]').click();
	});

$('#select-photo').siblings('input[type=file]').change(function() {
		set_photo(this.files[0].name);
	});

function set_photo(file_name) {
		var input = $('#select-photo').siblings('input[type=file]').first(), 
			select_photo = input.prevAll('#select-photo').first(), 
			selected_photo = input.prev('#selected-photo');
		
		select_photo.css('display', 'none');
		
		selected_photo
			.html($('<strong>' + file_name + '<a href="javascript:;">&times;</a></strong>'))
			.css('display', 'block');
		
		selected_photo.find('a').click(function() {
				selected_photo.next('input[type=file]').val('');
				selected_photo
					.empty()
					.css('display', 'none');
				select_photo.css('display', 'block');
			});
}

$('label.quarter>a').click(function() {
		var checkbox = $(this).next('input[type=checkbox]');
		
		if (checkbox.length) {
			checkbox[0].checked = !(checkbox[0].checked);
			$(this).toggleClass('selected', checkbox[0].checked);
		}
	});


function update_option_category_ids() {
	var option_category_ids = [];
	$.each($('#menu-item-option-categories').find('span'), function(index, category) {
			option_category_ids.push($(category).data('id'));
		});
	
	$('#add-menu-item-form input[name=option_groups]').val(option_category_ids.join(','));
}

$('#menu-item-option-category').change(function() {
		var id = this.value, 
			text = this[this.selectedIndex].text;
		
		if (id=='-1') {
			// Create a new options group
			window.open(root + lang + '/menu-option-groups.html', '_self');
			return;
		}
		
		if (id && !$('#menu-item-option-categories').find('span[data-id=' + id + ']').length)
			$('#menu-item-option-categories').append('<span class="pill-element" data-id="' + id + '"><a href="javascript:;">' + escapeHtml(text) + '</a> <a class="remove" href="javascript:;">&times;</a></span>');
		
		update_option_category_ids();
		/*
		setTimeout(function() {
				$('#menu-item-option-category').select2('val', ''); // Reset it, so it won't appear as anything is selected in particular
			}, 1);*/
	});

$('#menu-item-option-categories').on('click', 'a:not(.remove)', function() {
		var id = $(this).parents('span').first().data('id');
		window.open(root + lang + '/menu-option-groups.html#menu_option_group_' + id, 'menu-option-groups');
	});

$('#menu-item-option-categories').on('click', 'a.remove', function() {
		$(this).parents('span').first().remove();
		update_option_category_ids();
	});



$('#add-menu-item-form').validate({
		errorPlacement	: function(error, element) {
			if (element.hasClass('rounded-select'))
				error.insertAfter(element.next('span.select2'));
			else
				error.insertAfter(element);
		}, 
		rules			: {
			'category'		: 'required', 
			'menu_item'		: 'required', 
			//'description'	: 'required', 
			'price'			: {
				max			: 9999.99, 
				min			: 0.01, 
				number		: true, 
				required	: true
			}, 
			'discount'		: {
				max		: 100, 
				min		: 0, 
				number	: true
			}
		}
	});

$('.white-container input[type=button]').click(function() {
		// Reset to Add form
		$('#add-menu-item-form').find('strong.title').text(langAddItem);
		
		$('#add-menu-item-form').find('input[name=id]').val('');
		$('#add-menu-item-form').find('select[name=category]').select2('val', '');
		
		$('#add-menu-item-form').find('input[name=menu_item]').val('');
		if ($('#selected-photo').find('a')) {
			// There's already a photo being displayed, let's remove it
			$('#selected-photo').find('a').click();
		}
		$('#add-menu-item-form').find('textarea[name=description]').val('`');
		$('#add-menu-item-form').find('input[name=price]').val('');
		$('#add-menu-item-form').find('input[name=discount]').val('');
		$('#add-menu-item-form').find('input[name=spicy]')[0].checked = false;
		$('#add-menu-item-form').find('input[name=spicy]').prev('a').toggleClass('selected', $('#add-menu-item-form').find('input[name=spicy]')[0].checked);
		$('#add-menu-item-form').find('input[name=veggie]')[0].checked = false;
		$('#add-menu-item-form').find('input[name=veggie]').prev('a').toggleClass('selected', $('#add-menu-item-form').find('input[name=veggie]')[0].checked);
		$('#add-menu-item-form').find('input[name=glut_free]')[0].checked = false;
		$('#add-menu-item-form').find('input[name=glut_free]').prev('a').toggleClass('selected', $('#add-menu-item-form').find('input[name=glut_free]')[0].checked);
		$('#add-menu-item-form').find('input[name=lact_free]')[0].checked = false;
		$('#add-menu-item-form').find('input[name=lact_free]').prev('a').toggleClass('selected', $('#add-menu-item-form').find('input[name=lact_free]')[0].checked);
		$('#add-menu-item-form').find('input[name=recommended]')[0].checked = false;
		$('#add-menu-item-form').find('input[name=enabled]')[0].checked = false;
		
		$('#menu-item-option-categories').empty();
		update_option_category_ids();
		
		$('.white-container input[type=submit]').val(langAddButton);
		$('.white-container input[type=button]').css('display', 'none');
	});



$('#menu-items').sortable({
		axis		: 'y', 
		containment	: 'parent', 
		handle		: '.drag-icon', 
		scroll		: false, 
		tolerance	: 'pointer', 
		update		: function(e, ui) {
			var order = [];
			
			$.each($('#menu-items .menu-item-category'), function(index, category) {
					order.push($(category).data('id'));
				});
			
			$.ajax({
					data		: {
						menu_categories : order
					}, 
					dataType	: 'json', 
					url			: root + lang + '/order-menu-categories.html', 
					type		: 'POST'
				});
		}
	});


$.each($('#menu-items .menu-items'), function(index, list) {
		$(list).sortable({
				axis		: 'y', 
				containment	: 'parent', 
				handle		: '.drag-icon', 
				scroll		: false, 
				tolerance	: 'pointer', 
				update		: function(e, ui) {
					var order = [];
					
					$.each($(list).find('li'), function(index, item) {
							order.push($(item).data('id'));
						});
					
					$.ajax({
							data		: {
								menu_items : order
							}, 
							dataType	: 'json', 
							url			: root + lang + '/order-menu-items.html', 
							type		: 'POST'
						});
				}
			});
	});


$('.white-container h2').click(function() {
		var closed = $(this).hasClass('closed'), 
			list = $(this).next('ul');
		
		$(this).toggleClass('closed', !closed);
		list.animate({
				height		: (closed ? list[0].scrollHeight : 0)
			}, {
				complete	: function() {
					if (closed)
						list.css('height', 'auto')
				}
			});
	});


$('#menu-items-body')
	.on('click', '.actions.edit', function() {
		var data = $(this).parents('li').first().data('menu-item');
		
		$('#add-menu-item-form').find('strong.title').text(langEditItem);
		
		$('#add-menu-item-form').find('input[name=id]').val(data.id);
		$('#add-menu-item-form').find('select[name=category]').select2('val', data.category);
		
		$('#add-menu-item-form').find('input[name=menu_item]').val(data.menu_item);
		if (data.photo) {
			set_photo(data.photo);
		} else if ($('#selected-photo').find('a')) {
			// There's already a photo being displayed, let's remove it
			$('#selected-photo').find('a').click();
		}
		$('#add-menu-item-form').find('textarea[name=description]').val(data.description);
		$('#add-menu-item-form').find('input[name=price]').val(data.price);
		$('#add-menu-item-form').find('input[name=discount]').val(data.discount);
		$('#add-menu-item-form').find('input[name=spicy]')[0].checked = (data.spicy=='Y');
		$('#add-menu-item-form').find('input[name=spicy]').prev('a').toggleClass('selected', $('#add-menu-item-form').find('input[name=spicy]')[0].checked);
		$('#add-menu-item-form').find('input[name=veggie]')[0].checked = (data.veggie=='Y');
		$('#add-menu-item-form').find('input[name=veggie]').prev('a').toggleClass('selected', $('#add-menu-item-form').find('input[name=veggie]')[0].checked);
		$('#add-menu-item-form').find('input[name=glut_free]')[0].checked = (data.glut_free=='Y');
		$('#add-menu-item-form').find('input[name=glut_free]').prev('a').toggleClass('selected', $('#add-menu-item-form').find('input[name=glut_free]')[0].checked);
		$('#add-menu-item-form').find('input[name=lact_free]')[0].checked = (data.lact_free=='Y');
		$('#add-menu-item-form').find('input[name=lact_free]').prev('a').toggleClass('selected', $('#add-menu-item-form').find('input[name=lact_free]')[0].checked);
		$('#add-menu-item-form').find('input[name=recommended]')[0].checked = (data.recommended=='Y');
		$('#add-menu-item-form').find('input[name=enabled]')[0].checked = (data.enabled=='Y');
		
		$('#menu-item-option-categories').empty();
		if (data.option_groups) {
			for (var i=0, j=data.option_groups.length; i<j; i++) {
				
				$('#menu-item-option-category')
					.val(data.option_groups[i])
					.change();
			}
			
		} else
			update_option_category_ids();
		
		$('.white-container input[type=submit]').val(langEditButton);
		$('.white-container input[type=button]').css('display', 'block');
		
		$('html,body').animate({
				scrollTop: ($('#add-menu-item-form').offset().top - $('#body>header').height())
			});
	})
	.on('click', '.actions.delete', function() {
		if (confirm(langRemoveConfirmation)) {
			var deleted_button = $(this), 
				id = deleted_button.parents('li').first().data('id');
			
			$.ajax({
					data		: {
						menu_item : id
					}, 
					dataType	: 'json', 
					success		: function(data) {
						
						if (data && data.success) {
							var list = deleted_button.parents('ul').first();
							
							deleted_button.parents('li').first().remove();
							if (!list.find('li').length) {
								// There are no more menu items in the current category
								list.prev('h2').remove();
								list.remove();
							}
						}
					}, 
					url			: root + lang + '/delete-menu-item.html', 
					type		: 'POST'
				});
		}
	})
	.on('click', '.actions.enabled', function() {
		var state_button = $(this), 
			id = state_button.parents('li').first().data('id');
		
		$.ajax({
				data		: {
					menu_item : id
				}, 
				dataType	: 'json', 
				success		: function(data) {
					
					if (data && data.success)
						state_button.parents('li').first().toggleClass('disabled');
				}, 
				url			: root + lang + '/change-menu-item-state.html', 
				type		: 'POST'
			});
	});



if (location.hash.indexOf('#menu_item_')===0 && $('#menu-items li[data-id=' + location.hash.substr(11) + ']').length) {
	// Scroll to the submitted element
	$(window).bind('load', function() {
			$('html,body').animate({
					scrollTop: ($('#menu-items li[data-id=' + location.hash.substr(11) + ']').offset().top - $('#body>header').height())
				});
		});
}
