
	function invalidAddress() {
		$('#corporate-join-body form')[0].submitted = false;
		show_top_message(langInvalidAddress.replace('{address}', $('#corporate-join-body input[name=address]').val() + ', ' + $('#corporate-join-body input[name=city]').val()));
		
		$('html,body').animate({
				scrollTop: ($('#corporate-join-body input[name=address]').offset().top - $('#body>header').height())
			}, 500);
	}

	function getLocationCoords() {
		if (typeof google!='undefined') {
			var country_abbr = $('select[name=country] option:selected').data('abbr'), 
				geocoder = new google.maps.Geocoder();
			
			geocoder.geocode({
					address					: ($('#corporate-join-body input[name=address]').val() + ', ' + $('#corporate-join-body input[name=city]').val()), 
					componentRestrictions	: {
						country : country_abbr
					}
				}, 
				function(results, status) {
					if (status==google.maps.GeocoderStatus.OK) {
						var found_address = false;
						
						for (var i=0, j=results[0].address_components.length; i<j; i++) {
							var address_component = results[0].address_components[i];
							
							if ($.arrayIntersect(['street_number', 'street_address', 'route'], address_component.types).length) {
								found_address = true;
								break;
							}
						}
						
						if (found_address) {
							$('#corporate-join-body input[name=latitude]').val(results[0].geometry.location.lat());
							$('#corporate-join-body input[name=longitude]').val(results[0].geometry.location.lng());
							
							$('#corporate-join-body form')[0].submit();
							
						} else
							invalidAddress();
						
					} else
						invalidAddress();
				});
		};
	}

	$('select[name=country]').change(function() {
		var country_abbr = $('select[name=country] option:selected').data('abbr'), 
			city = $('#corporate-join-body input[name=city]').clone(); // So that we could use it as the source for a new Autocomplete setup
		
		$('#corporate-join-body input[name=city]').next('label.error').remove();
		$('#corporate-join-body input[name=city]').remove();
		$('#corporate-join-body input[name=address]').before(city);
		
		var autocomplete = new google.maps.places.Autocomplete(city[0], {
				componentRestrictions: {
					country: country_abbr
				}, 
				types: ['(cities)']
			});
	});

	$('select[name=country]').change();


	function doCustomSelect(select) {
		function addMultiplePlaceholder() {
			if ($(this).data('placeholder')) {
				var select2 = $(this).next('span.select2');
				select2.find('input.select2-search__field').attr('placeholder', $(this).data('placeholder'));
			}
		}
		
		$(select)
			.select2({
				minimumResultsForSearch	: -1
			})
			.on('select2:select', addMultiplePlaceholder)
			.on('select2:unselect', addMultiplePlaceholder);
	}
	
	$.each($('#corporate-join-body select'), function(index, select) {
		doCustomSelect(select);
	});

	$('#email-addresses>a').click(function() {
		var html = $('#email-addresses>div').first()
			.clone().outerHtml()
			.replace(/\[0\]/g, '[' + $('#email-addresses>div').length + ']'), 
			clone = $(html);
		
		clone.find('span.select2').remove();
		clone.find('label.error').remove();
		
		$('#email-addresses>div:last-of-type').after(clone);
	});

	$.validator.setDefaults({
		ignore: [] // So that it would validate the custom, hidden select fields, too
	});

	$.validator.addMethod("phoneno", function(value, element) {
		var filter = /^\+?\d+(\s?\d*)*\d+$/ig;
		if ( filter.test( value ) ) return true;
		else return false;
	}, "Enter valid phone number");

	var form_validator = $('#corporate-join-body form').first().validate({
		errorPlacement	: function(error, element) {
			if (element.hasClass('rounded-select'))
				error.insertAfter(element.next('span.select2'));
			else if ( element.attr('name') == 'phone') 
				error.insertAfter(element.parent());
			else if ( element.attr('type') == 'checkbox') 
				element.closest('label').append( error );
			else
				error.insertAfter(element);
		}, 
		messages		: {
			'username'	: {
				remote: langUsernameInUse
			}
		}, 
		rules			: {
			'last_name'			: 'required', 
			'first_name'		: 'required', 
			'job_title'			: 'required', 
			'mobile'			: 'required', 
			
			'country'			: 'required', 
			'city'				: 'required', 
			'company_name'		: 'required', 
			'address'			: 'required', 
			'zip'				: 'required', 
			'phone'				: {
				phoneno		: true
			}, 
			'email[0]'			: {
				email		: true, 
				required	: true
			}, 
			'confirm_email[0]'	: {
				equalTo: '#corporate-join-body input[name="email[0]"]'
			}, 
			
			'username'			: {
				//email		: true, 
				required	: true, 
				remote		: {
					data	: {
						corporate	: true, 
						username	: function() {
							return $('#corporate-join-body input[name=username]').val();
						}
					}, 
					type	: 'POST', 
					url		: root + lang + '/check-unique-username.html'
				}
			}, 
			'confirm_username'	: {
				equalTo		: '#corporate-join-body input[name=username]'
			}, 
			'password'			: {
				minlength	: 5, 
				required	: true
			}, 
			'confirm_password'	: {
				equalTo		: '#corporate-join-body input[name=password]'
			}, 
			'terms'			: 'required'
		}, 
		submitHandler	: function(form) {
			// Prevent multiple sumbits
			if (!form.submitted)
				form.submitted = true;
			else
				return false;
			
			
			$('#error-container').animate({
					height: 0
				}, {
					complete: function() {
						$('#error-container>div>span').text('');
					}
				});
			
			getLocationCoords();
		}
	});

	$('#corporate-join-body form').first().submit(function() {
		var form = $(this);
		
		setTimeout(function() {
				$.each(form.find('label.error'), function(index, label) {
						if (label.style.display!='none') {
							var for_ = $(label).attr('for'), 
								element = $('input[name="' + for_ + '"],select[name="' + for_ + '"]').first();
							
							var top = element.offset().top;
							if (element.css('display')=='none') {
								element.css('display', 'block');
								top = element.offset().top;
								element.css('display', '');
							}
							
							$('html,body').animate({
									scrollTop: (top - $('#body>header').height())
								}, 500);
							
							return false;
						}
					});
			}, 10);
	});

	$(document).ready( function() {
		$("input[name=phone]").intlTelInput( {autoHideDialCode: false, nationalMode: false, onlyCountries: ["ch", "it", "fr", "pt", "us", "gb", "sw", "de", "no", "nl", "pl", "fl"]});
	});