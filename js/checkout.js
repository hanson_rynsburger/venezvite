
$('#cart-summary-container>a').click(function() {
		$('html,body').animate({
				scrollTop: ($('#shopping-cart').offset().top - $('#body>header').height())
			}, 1000);
	});

	$('#checkout-login').click(function() {
		$('#main-navigation #login-button').click();
	});

	// Transform all of the checkout form's select fields into custom ones
	$('#checkout-form .rounded-select').select2({
		minimumResultsForSearch	: -1
	});

	$('#edit-account').click(function() {
		$('#account-info').animate({
				height: 0
			});
		$('#account-fields').animate({
				height: $('#account-fields')[0].scrollHeight
			}, {
				complete: function() {
					$('#account-fields').find('input,select,textarea').removeAttr('disabled');
					$('#account-fields').css('height', 'auto').css('overflow', 'visible');
				}, 
			});
	});

	$('#transportation-switch a').click(function() {
		if ($('#takeaway-details').height()==0) {
			// User must be toggling between the delivery to the takeaway option
			$('#transportation-switch>strong').text(lang4Takeaway);
			$('#transportation-switch>a').text(langChange2Delivery);

			$('#takeaway-details').animate({
					height: $('#takeaway-details')[0].scrollHeight
				}, {
					complete: function() {
						$('#takeaway-details').css('height', 'auto');
					}
				});
			$('#delivery-details').animate({
					height: 0
				}, {
					complete: function() {
						$('#delivery-details').find('input,select,textarea').attr('disabled', 'disabled');
					}
				});
			
			if ($('#cart-checkout select[name=type]').val()!='T') {
				$('#cart-checkout select[name=type]').val('T');
				$('#cart-checkout select[name=type]').change();
			}
			$('#transportation-details input[name=type]').val('T');
			
		} else {
			// User must be toggling between the takeaway to the delivery option
			$('#transportation-switch>strong').text(lang4Delivery);
			$('#transportation-switch>a').text(langChange2Takeaway);
			
			$('#delivery-details').animate({
					height: $('#delivery-details')[0].scrollHeight
				}, {
					complete: function() {
						$('#delivery-details').find('input,select,textarea').removeAttr('disabled');
						$('#delivery-details').css('height', 'auto');
					}
				});
			$('#takeaway-details').animate({
					height: 0
				});
			
			if ($('#cart-checkout select[name=type]').val()!='D') {
				$('#cart-checkout select[name=type]').val('D');
				$('#cart-checkout select[name=type]').change();
			}
			$('#transportation-details input[name=type]').val('D');
		}
	});


	$('#checkout-form select[name=delivery_address]').change(function() {
		if ($(this).val()) {
			$('#delivery-new-address').animate({
					height: 0
				}, {
					complete: function() {
						$('#delivery-new-address').find('input,select,textarea').attr('disabled', 'disabled');
					}
				});
			$('#delivery-existing-address').animate({
					height: $('#delivery-existing-address')[0].scrollHeight
				});
			
			var address = $(this).find('option:selected').data('address');
			for (var field in address) {
				if ($('td#view-address-' + field).length)
					$('td#view-address-' + field).text(address[field] ? address[field] : '-');
			}
			
		} else {
			// Clean the add/edit form
			if (init_address_set) {
				$('#delivery-new-address input[type!=radio],#delivery-new-address textarea').val('');
				$('#delivery-new-address input[type=radio]').prop('checked', '');
			}

			$('#delivery-new-address').animate({
					height: $('#delivery-new-address')[0].scrollHeight
				}, {
					complete : function() {
						$('#delivery-new-address').find('input,select,textarea').removeAttr('disabled');
						$('#delivery-new-address').css('height', 'auto');
					}
				});
			$('#delivery-existing-address').animate({
					height: 0
				});
		}
		
		init_address_set = true;
	});
	
	var init_address_set = false;
	$('#checkout-form select[name=delivery_address]').change();
	
	$('#delivery-existing-address a').click(function() {
		// Prefill the add/edit form's values with the currently edited address first
		var address = $('#checkout-form select[name=delivery_address] option:selected').data('address');
		for (var field in address) {
			if ($('input[name=delivery_' + field + ']').length)
				$('input[name=delivery_' + field + ']').val(address[field] ? address[field] : '');
			else if ($('textarea[name=delivery_' + field + ']').length)
				$('textarea[name=delivery_' + field + ']').val(address[field] ? address[field] : '');
		}
		
		$('#delivery-new-address').animate({
				height: $('#delivery-new-address')[0].scrollHeight
			}, {
				complete : function() {
					$('#delivery-new-address').find('input,select,textarea').removeAttr('disabled');
				}
			});
		$('#delivery-existing-address').animate({
				height: 0
			});
	});

	$('#shopping-cart-items').on('click', 'a.delete', function() {
		if (!($('#shopping-cart-items').children('li').length - 1)) {
			// No more selected menu items, let's redirect 
			// the user to the previous restaurant's page
			document.location = root + restaurant_url + '.html#no-menu-items';
		}
	});

	//	loadCartContent(restaurant_url);

/*	$('#delivery-details select[name=date]').change(function() {
		$('#cart-checkout select[name=date]').select2('val', $(this).val());
	});

	$('#delivery-details select[name=time]').change(function() {
		$('#cart-checkout select[name=time]').select2('val', $(this).val());
	});

	$('#cart-checkout select[name=date]').on('change', function() {
		$('#delivery-details select[name=date]').select2('val', $(this).val());
	});

	$('#cart-checkout select[name=time]').change(function() {
		$('#delivery-details select[name=time]').select2('val', $(this).val());
	});

	$('#cart-checkout select[name=type]').on('change', function() {
		if (($(this).val()=='T' && $('#takeaway-details').height()==0) || 
			($(this).val()=='D' && $('#delivery-details').height()==0))
			$('#transportation-switch a').click();
	}); */
	
	
	$('#checkout-form select[name=payment_token]').change(function() {
		if ($(this).val()=='') {
			$('#saved-ccs').animate({
					height: 0
				}, {
					complete: function() {
						$('#saved-ccs').addClass('disabled');
						$('#saved-ccs').find('input,select,textarea').attr('disabled', 'disabled');
					}
				});
			$('#new-cc').animate({
					height: $('#new-cc')[0].scrollHeight
				}, {
					complete	: function() {
						$('#new-cc').removeClass('disabled');
						$('#new-cc').find('input,select,textarea').removeAttr('disabled');
						
						$('#new-cc').css('height', 'auto');
					}
				});
		}
	});
	$('#checkout-form select[name=payment_token]').change(); // In case there's just one option (the "Use new card" one), this should display the CC fields
	
	
	$('#checkout-form select[name=currency]').change(function() {
		checkTotalCartValue();
	});


	var loading;
	$('#checkout-body form input[type=submit]').click(function() {
		if ($(this).hasClass('disabled')) {
			//show_top_message(langMoreMoney);
			return false;
		}
		
		if ($('#checkout-body form').first().valid()) {
			loading = $('<img class="loading" src="i/dot.png" />');
			$(this).after(loading);
		}
	});

	$('#cart-checkout input[type=submit]').click(function() {
		if ($(this).hasClass('disabled')) {
			//show_top_message(langMoreMoney);
			return false;
		}

		if ($('#checkout-body form').first().valid()) {
			loading = $('<img class="loading" src="i/dot.png" />');
			$(this).after(loading);
		}

		$('#checkout-body form').first().submit();
	});

	$(window).scroll(function() {
		if (document.body.scrollTop > ($('#restaurant-intro-image').outerHeight() - $('#body>header').outerHeight())) {
			$('#shopping-cart-container').css({
					position	: 'fixed', 
					top			: $('#body>header').outerHeight()
				});
		} else {
			$('#shopping-cart-container').css({
					position	: 'absolute', 
					top			: ''
				});
		}
	});

	$(document).ready( function() {
		$(window).trigger('scroll');
		$('.preceed-to-checkout').remove();

		$('.toggle-buttons a').click(function() {
				if ($(this).data('target')=='cash') {
					$('#checkout-form input[name=payment]').val('C');
					
					if (!$(this).data('can-place-orders'))
						$('#checkout-body form input[type=submit],#cart-checkout input[type=submit]').addClass('disabled');
					
				} else {
					if ($(this).data('target')=='credit')
						$('#checkout-form input[name=payment]').val('CC');
					else if ($(this).data('target')=='luncheck')
						$('#checkout-form input[name=payment]').val('LC');
					else if ($(this).data('target')=='invoice')
						$('#checkout-form input[name=payment]').val('I');
					
					checkTotalCartValue(); // So that the potentially disabled submit buttons would get re-enabled, if all (other) conditions are met
				}
				
				setTimeout(checkFormValidity, 500); // So that the animations and field disablings would have time to happen
			});
		
		$('.toggle-buttons a:first-of-type').click();
		$('input[name=phone]').intlTelInput({
				autoHideDialCode	: false, 
				initialCountry		: 'ch',
				nationalMode		: false, 
				onlyCountries		: ['ch', 'it', 'fr', 'pt', 'us'/*, 'gb', 'sw', 'de', 'no', 'nl', 'pl', 'fl'*/]
			});
		
		$.validator.setDefaults({
				ignore: [] // So that it would validate the custom, hidden select fields, too
			});
		
		$.validator.addMethod('mmyycheck', function(value, element) {
				var mmyyarray = value.split('/');
				var mm = parseInt( mmyyarray[0] );
				var yy = parseInt( mmyyarray[1] );
				
				if ( mmyyarray.length == 2 ) {
					if ( ( mm > 0 && mm <= 12 ) && ( yy > 15 ) ) {
						return true;
					}
					else 
						return false;
				}
				else
					return false;
			}, 'Should look like mm/yy');
		
		$.validator.addMethod('phoneno', function(value, element) {
				var filter = /^\+?\d+(\s?\d*)*\d+$/ig;
				return (filter.test(value) && value.length>=12);
			}, 'Enter valid phone number');
		
		var form_validator = $('#checkout-body form').first().validate({
				errorPlacement	: function(error, element) {
					if (element.hasClass('rounded-select'))
						error.insertAfter(element.next('span.select2'));
					else if ( element.attr('name') == 'phone') 
						error.insertAfter(element.parent());
					else if ( element.attr('type') == 'checkbox') 
						element.closest('label').append( error );
					else
						error.insertAfter(element);
				}, 
				messages		: {
					email: {
						remote: langEmailInUse
					}
				}, 
				rules			: {
					'last_name'				: 'required', 
					'first_name'			: 'required', 
					'email'					: {
						email		: true, 
						required	: true, 
						remote		: {
							data	: {
								email: function() {
									return $('#checkout-body input[name=email]').val();
								}
							}, 
							type	: 'POST', 
							url		: root + lang + '/check-unique-email.html'
						}
					}, 
					'password'				: {
						minlength	: 5, 
						required	: {
							depends: function(element) {
								// Only require it on add
								return !$(element).hasClass('not-mandatory');
							}
						}
					},
					'phone'					: {
						phoneno		: true, 
						required	: true
					}, 
					
					'delivery_street'		: 'required', 
					//'delivery_building_no'	: 'required', 
					'delivery_city'			: 'required', 
					'delivery_zip_code'		: 'required', 
					
					'payment_token'			: 'required', 
					'cc_name'				: {
						minlength	: 3, 
						required	: true
					}, 
					'cc_number'				: {
						creditcard	: true, 
						required	: true
					}, 
					/* 'cc_month'				: 'required', 
					'cc_year'				: 'required', */
					'cc_month_year'			: {
						required	: true,
						mmyycheck	: true
					}, 
					'lc_number'				: 'required', 
					'cvc'					: {
						minlength	: 3, 
						required	: true
					}, 
					'terms'					: 'required'
				}, 
				submitHandler	: function(form) {
					$('#error-container').animate({
							height: 0
						}, {
							complete: function() {
								$('#error-container>div>span').text('');
							}
						});
					
					var mmyyarray = $('input[name=cc_month_year]').val().split('/');
					var mm = mmyyarray[0];
					var yy = mmyyarray[1];
					$('input[name=cc_month]').val( mm );
					$('input[name=cc_year]').val( yy );
					
					if ( mmyyarray.length == 2 ) {
						if ( ( mm > 0 && mm <= 12 ) && ( yy > 15 ) ) {
							return true;
						}
						else 
							return false;
					}
					
					// First let's check if the submitted email address is unique, 
					// or if the delivery address is within any of the current restaurant's
					// delivery zones
					$.ajax({
							data		: $('#checkout-body form').first().serialize() + '&ajax=1', 
							dataType	: 'json', 
							success		: function(data) {
								loading.remove();
								
								if (data) {
									if (data.success)
										form.submit();
										
									else if (data.error) {
										setTimeout(function() {
												// For some reason it's not filled as soon as the error triggers
												$('#error-container>div>span').html(data.error);
											}, 100);
										$('#error-container').animate({
												height: $('#error-container')[0].scrollHeight
											}, {
												complete: function() {
													$('#error-container').css('height', 'auto');
												}
											});
										$('html,body').animate({
												scrollTop: ($('#error-container').offset().top - $('#body>header').height())
											}, 1000);
										
										if (data.min_delivery) {
											$('#delivery-minimum').text(data.min_delivery + ' ' + (typeof current_currency!='undefined' ? current_currency : 'CHF'));
											
											if (current_delivery_zone)
												current_delivery_zone.minimum_delivery = data.min_delivery;
										}
										
										if (data.delivery_fee) {
											$('#delivery-fee').text(data.delivery_fee + ' ' + (typeof current_currency!='undefined' ? current_currency : 'CHF'));
											$('#total-delivery-fee td>span').text(data.delivery_fee);
											
											checkTotalCartValue();
										}
									}
								};
							}, 
							url			: root + lang + '/checkout.html', 
							type		: 'POST'
						});
				}
			});
		
		
		$('#checkout-body input,#checkout-body select').on('change keyup', checkFormValidity);
		function checkFormValidity() {
			$('#checkout-body input[type=submit]').toggleClass('visible', $('#checkout-body form').first().valid());
			
			if (form_validator) form_validator.resetForm();
		};
	});
