
$('#add-menu-category-form').validate({
		rules			: {
			'category' : 'required'
		}, 
		submitHandler	: function(form) {
			var duplicate = false;
			$.each($('#menu-categories li'), function(index, category) {
					var menu_category = $(category).data('menu-category');
					
					if ($('#add-menu-category-form input[name=category]').val().trim()==menu_category.category.trim() && 
						$('#add-menu-category-form input[name=id]').val().trim()!=menu_category.id.trim()) {
						
						duplicate = true;
						return false;
					}
				});
			
			if (duplicate) {
				alert(langDuplicateCategory);
				return false;
			} else
				form.submit();
		}
	});

$('.white-container input[type=button]').click(function() {
		// Reset to Add form
		$('#add-menu-category-form').find('strong.title').text(langAddItem);
		
		$('#add-menu-category-form').find('input[name=id]').val('');
		$('#add-menu-category-form').find('input[name=category]').val('');
		
		$('.white-container input[type=submit]').val(langAddButton);
		$('.white-container input[type=button]').css('display', 'none');
	});


$('#menu-categories').sortable({
		axis		: 'y', 
		containment	: 'parent', 
		handle		: '.drag-icon', 
		scroll		: false, 
		tolerance	: 'pointer', 
		update		: function(e, ui) {
			var order = [];
			
			$.each($('#menu-categories').find('li'), function(index, category) {
					order.push($(category).data('id'));
				});
			
			$.ajax({
					data		: {
						menu_categories : order
					}, 
					dataType	: 'json', 
					url			: root + lang + '/order-menu-categories.html', 
					type		: 'POST'
				});
		}
	});


$('#menu-categories-body')
	.on('click', '.actions.edit', function() {
		var data = $(this).parents('li').first().data('menu-category');
		
		$('#add-menu-category-form').find('strong.title').text(langEditItem);
		
		$('#add-menu-category-form').find('input[name=id]').val(data.id);
		$('#add-menu-category-form').find('input[name=category]').val(data.category);
		
		$('.white-container input[type=submit]').val(langEditButton);
		$('.white-container input[type=button]').css('display', 'block');
		
		$('html,body').animate({
				scrollTop: ($('#add-menu-category-form').offset().top - $('#body>header').height())
			});
	})
	.on('click', '.actions.delete', function() {
		// First let's check if the chosen category doesn't already have some menu items attached to it
		// If so, we'll reject the delete request
		if ($(this).hasClass('disabled')) {
			var menu_items = $(this).parents('li').first().data('menu-items');
			
			alert(langDisabledCategory.replace('{used_menu_items}', menu_items.join('\n')));
			return false;
		}
		
		if (confirm(langRemoveConfirmation)) {
			var deleted_button = $(this), 
				id = deleted_button.parents('li').first().data('id');
			
			$.ajax({
					data		: {
						menu_category : id
					}, 
					dataType	: 'json', 
					success		: function(data) {
						
						if (data && data.success)
							deleted_button.parents('li').first().remove();
					}, 
					url			: root + lang + '/delete-menu-category.html', 
					type		: 'POST'
				});
		}
	});



if (location.hash.indexOf('#menu_category_')===0 && $('#menu-categories li[data-id=' + location.hash.substr(15) + ']').length) {
	// Scroll to the submitted element
	$(window).bind('load', function() {
			$('html,body').animate({
					scrollTop: ($('#menu-categories li[data-id=' + location.hash.substr(15) + ']').offset().top - $('#body>header').height())
				});
		});
}
