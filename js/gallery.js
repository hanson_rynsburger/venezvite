;( function( $, window, undefined ) {
	$(document).ready( function() {
		$("a.delete-image").click( function() {
			$.ajax({
				data		: "delete=1&id=" + $(this).data('id'), 
				type		: 'POST',
				success		: function(data) {
					if (data) {
						location.reload();
					}
				}, 
			});
		} );
	});
} )( jQuery, window );