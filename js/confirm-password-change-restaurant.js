
$('#confirm-password-change-restaurant-body form').first().validate({
		rules			: {
			'new_password'			: {
				minlength	: 5, 
				required	: true
			}, 
			'confirm_new_password'	: {
				equalTo		: '#confirm-password-change-restaurant-body input[name=new_password]'
			}
		}
	});
