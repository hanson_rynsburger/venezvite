
function submitForm(form) {
	$.ajax({
		data		: $(form).serialize() + '&ajax=1', 
		dataType	: 'json', 
		success		: function(data) {
			//loading.remove();
			
			if (data) {
				if (data.success) {
					$(form).find('.rounded-red-button.disabled').click();
					
					if (data.fields) {
						for (var field in data.fields) {
							if ($('#' + field).length) {
								$('#' + field).text(data.fields[field]);
								$('#' + field).html($('#' + field).text().replace(/\n/g, '<br />'));
							}
						}
					}
				}
			}
		}, 
		url			: root + lang + '/business-info.html', 
		type		: 'POST'
	});
}


	$('#entity-details-form').validate({
		messages		: {
			'website'	: {
				full_url : $.validator.messages.url
			}
		}, 
		rules			: {
			'name'			: 'required', 
			'address'		: 'required', 
			'phone'			: 'required', 
			'website'		: {
				full_url : true
			}, 
			'description'	: 'required'
		}, 
		submitHandler	: submitForm
	});


	$('#entity-admin-form').validate({
		rules			: {
			'last_name'			: 'required', 
			'first_name'		: 'required', 
			'admin_email'		: {
				email		: true, 
				required	: true
			}, 
			password			: {
				minlength	: 5, 
				//required	: true
			}, 
			confirm_password	: {
				equalTo		: '#entity-password-value', 
				//required	: true
			}, 
			'email'				: {
				//email		: true, 
				required	: true
			}
		}, 
		submitHandler	: submitForm
	});


	$('#entity-hours-form select').select2({
		minimumResultsForSearch	: -1
	});



function getActionForm() {
	var content = '<div id="delivery-zone-form">' + 
			'<div class="half left">' + 
				'<label>' + 
					'<span>' + langRange + '</span>' + 
					'<input class="rounded-input" maxlength="4" name="range" value="' + (current_zone.data ? current_zone.data.range : '') + '" />' + 
				'</label>' + 
			'</div>' + 
			'<div class="half right">' + 
				'<label>' + 
					'<span>' + langFee + '</span>' + 
					'<input class="rounded-input" maxlength="4" name="fee" value="' + (current_zone.data ? current_zone.data.deliveryFee : '') + '" />' + 
				'</label>' + 
			'</div>' + 
			'<div class="clear half left">' + 
				'<label>' + 
					'<span>' + langMinDelivery + '</span>' + 
					'<input class="rounded-input" maxlength="5" name="minimum" value="' + (current_zone.data ? current_zone.data.minimumDelivery : '') + '" />' + 
				'</label>' + 
			'</div>' + 
			'<div class="half right">' + 
				'<label>' + 
					'<span>' + langEstimation + '</span>' + 
					'<input class="rounded-input" maxlength="3" name="time" value="' + (current_zone.data ? current_zone.data.estimatedTime : '') + '" />' + 
				'</label>' + 
			'</div>' + 
			'<div class="clear half left">' + 
				'<input class="rounded-red-button" id="manage-delivery-zone" type="button" value="' + (current_zone.data ? langUpdate : langAdd) + '" />' + 
			'</div>' + 
			'<div class="half right">' + 
				'<input class="disabled rounded-red-button" id="delete-delivery-zone" type="button" value="' + langDelete + '" />' + 
			'</div>' + 
			'<div class="clear"></div>' + 
		'</div>';
	
	return content;
}

function center_map() {
	var center = restaurant_map.getCenter();
	google.maps.event.trigger(restaurant_map, 'resize');
	restaurant_map.setCenter(center);
}

var restaurant_map = new google.maps.Map($('#delivery-zone-map>div')[0], {
		center				: {
			lat	: parseFloat($('#delivery-zone-map').data('lat')), 
			lng	: parseFloat($('#delivery-zone-map').data('lng'))
		}, 
		disableDefaultUI	: true, 
		maxZoom				: 17, 
		minZoom				: 12, 
		zoom				: 13
	}), 
	infowindow = new google.maps.InfoWindow(), 
	current_zone;

function set_paths(arr) {
	var paths = [];
	$.each(arr, function(index, path) {
			paths.push(new google.maps.LatLng(path[0], path[1]));
		});
	return paths;
}

var delivery_polygons = [];
$.each(delivery_zones, function(index, delivery_zone_) {
		var delivery_zone = new google.maps.Polygon({
				//clickable		: false, 
				data			: JSON.parse(delivery_zone_.data), 
				editable		: true, 
				fillColor		: '#FF0000', 
				fillOpacity		: 0.1, 
				map				: restaurant_map, 
				paths			: [set_paths(delivery_zone_.paths)], 
				strokeColor		: '#FF0000', 
				strokeOpacity	: 0.5, 
				strokeWeight	: 1, 
				zIndex			: delivery_zone_.index
			});
		delivery_zone.addListener('click', function(e) {
				current_zone = this;
				
				infowindow.setOptions({
						content: getActionForm()
					});
				infowindow.open(restaurant_map, this);
				infowindow.setPosition(e.latLng);
			});
		
		delivery_polygons.push(delivery_zone);
	});

$(window).resize(center_map);
/*
var drawingManager = new google.maps.drawing.DrawingManager({
		//drawingMode				: google.maps.drawing.OverlayType.POLYGON, 
		drawingControl			: true, 
		drawingControlOptions	: {
			position		: google.maps.ControlPosition.TOP_CENTER, 
			drawingModes	: [
				//google.maps.drawing.OverlayType.MARKER, 
				//google.maps.drawing.OverlayType.CIRCLE, 
				google.maps.drawing.OverlayType.POLYGON, 
				//google.maps.drawing.OverlayType.POLYLINE, 
				//google.maps.drawing.OverlayType.RECTANGLE
			]
		}, 
		polygonOptions: {
			//clickable		: false, 
			editable		: true, 
			fillColor		: '#FF0000', 
			fillOpacity		: 0.1, 
			strokeColor		: '#FF0000', 
			strokeOpacity	: 0.5, 
			strokeWeight	: 1, 
			zIndex			: 1
		}
	});
drawingManager.setMap(restaurant_map);
google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
		polygon.addListener('click', function(e) {
				current_zone = this;
				
				infowindow.setOptions({
						content: getActionForm()
					});
				infowindow.open(restaurant_map, polygon);
				infowindow.setPosition(e.latLng);
			});
		
		drawingManager.setOptions({
				drawingMode: null
			});
	});
*/
	function get_paths(zone) {
		var paths = [];
		zone.getPath().forEach(function(latLng, i)	{
				paths.push([latLng.lat(), latLng.lng()]);
			});
		return paths;
	}

	function manage_delivery_zone(delete_) {
		$.ajax({
			data		: {
				zone_id		: (current_zone.data ? current_zone.data.idDeliveryZone : null), 
				paths		: get_paths(current_zone), 
				range		: $('#delivery-zone-form input[name=range]').val(), 
				fee			: $('#delivery-zone-form input[name=fee]').val(), 
				minimum		: $('#delivery-zone-form input[name=minimum]').val(), 
				time		: $('#delivery-zone-form input[name=time]').val(), 
				is_delete	: (typeof delete_!='undefined' ? true : false)
			}, 
			dataType	: 'json', 
			success		: function(data) {
				if (data && data.success)
					location.reload();
			}, 
			url			: root + lang + '/manage-delivery-zone.html', 
			type		: 'POST'
		});
	}

	$('body')
	.on('click', '#manage-delivery-zone', function(e) {
		manage_delivery_zone();
	})
	.on('click', '#delete-delivery-zone', function(e) {
		if (confirm(langConfirmDeliveryZone)) {
			if (current_zone.data && current_zone.data.idDeliveryZone)
				// AJAX removal
				manage_delivery_zone(true);
			else
				current_zone.setMap(null);
		}
	});

	$.each($('#entity-delivery-zones a.green-button'), function(index, button) {
		$(button).click(function() {
				if ($(this).parents('.white-container').first().hasClass('disabled-container'))
					// You can't edit a custom delivery zone
					return false;

				$('#delivery-zone-map').css('display', 'block');
				center_map();
				
				$('#delivery-zone-map').animate({
						opacity: 1
					}, {
						complete	: function() {
							google.maps.event.trigger(delivery_polygons[index], 'click', {
							        latLng	: delivery_polygons[index].getPath().getAt(0)
							    });
						} 
					});
			});
	});

	$('#delivery-zone-map>a').click(function() {
		// Close map
		$('#delivery-zone-map').animate({
				opacity: 0
			}, {
				complete	: function() {
					$('#delivery-zone-map').css('display', 'none');
				}
			});
	});

	$('#date-open-close select').select2({
		minimumResultsForSearch	: -1
	});
	
$(document).ready( function() {
	$( "#datepicker" ).datepicker( {
		onSelect: function( dt, obj ) {
			var start = $("#datepicker-container select[name=holiday_start]").val();
			var end = $("#datepicker-container select[name=holiday_end]").val();

			if ( start > end ) { 
				alert ( "Please select correct closing time" );
			}

			$("#datepicker-container").addClass( "adding" );

			$.ajax({
				data		: {
					customdate: dt,
					ajax: 1,
					start: start,
					end: end
				},
				dataType	: 'json', 
				success		: function(data) {
					if (data) {
						if (data.success) {
							if ( $("#holiday-list td[colspan]").length > 0 ) {
								$("#holiday-list tr").remove();
							}

							var time = data.newdate;
							if ( start != "" ) {
								if ( end != "" ) {
									time = time + " " + start + " - " + end;
								}
							};

							var newtr = "<tr><td>" + time + "</td><td class='btn-delete-wrapper'><a href='javascript:void(0);' class='delete-holiday' data-id='" + data.newdate_id + "'> Delete  <i class='fa fa-trash-o'></i></a></td></tr>";
							$("#holiday-list").append( $(newtr) );
						}
						else {
							alert( data.message );
						}
					}
				}, 
				url			: root + lang + '/business-info.html', 
				type		: 'POST'
			}).always( function() {
				$("#datepicker-container").removeClass( "adding" );
			});
		}
	} );
	
	$( document ).on('click', 'a.delete-holiday', function() {
		var self = $(this);

		$.ajax({
			data		: 'delctt=1&delid=' + $(this).attr('data-id') + '&ajax=1', 
			dataType	: 'json',
			url			: root + lang + '/business-info.html', 
			type		: 'POST',
			success		: function(data) {
				if (data) {
					if (data.success) {
						self.closest('tr').remove();

						if ( $('#holiday-list tr').length == 0 ) {
							$('#holiday-list').append( '<tr><td colspan="2">' + $("#holiday-translation").text() + '</td></tr>' );
						}
					}
				}
			}
		}).always( function() {

		});;
	})
});