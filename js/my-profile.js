
function submitForm(form) {
	$.ajax({
		data		: $(form).serialize() + '&ajax=1', 
		dataType	: 'json', 
		success		: function(data) {
			//loading.remove();
			
			if (data) {
				if (data.success) {
					$(form).find('.rounded-red-button.disabled').click();

					if (data.fields) {
						for (var field in data.fields) {
							if ($('#' + field).length)
								$('#' + field).text(data.fields[field]);
						}
					}
				}
			}
		}, 
		url			: root + lang + '/my-profile.html', 
		type		: 'POST'
	});
}

$('#entity-name-form').validate({
		rules			: {
			'last_name'		: 'required', 
			'first_name'	: 'required'
		}, 
		submitHandler	: submitForm
	});

$('#entity-email-form').validate({
		messages		: {
			email: {
				remote: langEmailInUse
			}
		}, 
		rules			: {
			'email'					: {
				email		: true, 
				required	: true, 
				remote		: {
					data	: {
						email: function() {
							return $('#entity-email-form input[name=email]').val();
						}
					}, 
					type	: 'POST', 
					url		: root + lang + '/check-unique-email.html'
				}
			}
		}, 
		submitHandler	: submitForm
	});

$('#entity-password-form').validate({
		rules			: {
			password			: {
				minlength	: 5, 
				required	: true
			}, 
			confirm_password	: {
				equalTo		: '#entity-password-value', 
				required	: true
			}
		}, 
		submitHandler	: submitForm
	});


$('#entity-phone-form').validate({
	rules			: {
		phone		: {
			required	: true
		}
	}, 
	submitHandler	: submitForm
});

$('document').ready( function() {
	$('input[name=phone]').intlTelInput( {autoHideDialCode: false, nationalMode: false, onlyCountries: ["ch", "it", "fr", "pt", "us", "gb", "sw", "de", "no", "nl", "pl", "fl"]});
});