function togglePanels() {
	if ($(this).data('related')) {
		var related = $('#' + $(this).data('related'));
		related.css('display', 'block');
		related.animate({
			height	: related[0].scrollHeight, 
			opacity	: 1
		}, {
			complete: function() {
				related.css('height', 'auto');
			}
		});
	}

	var parent = $(this).parents('.white-container').first();
	parent.animate({
		height	: 0, 
		opacity	: 0
	}, {
		complete: function() {
			parent.css('display', 'none');
		}
	});
}

$('.green-button[data-related],.rounded-red-button[data-related]').click(togglePanels);
$('.white-container .rounded-red-button').click(function() {
		if ($(this).hasClass('disabled') || $(this).hasClass('right'))
			return;
		
		$(this).parents('.white-container').first().submit();
	});
