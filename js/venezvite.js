
var root = $('base').attr('href'), lang = $('html').attr('lang'), fb_app_id = $('meta[property="fb:app_id"]').attr('content');

// Close any visible floating panel, unless it's the currently clicked one
// To be updated as new panels get added to the website...
function handle_floating_panels(currently_clicked_link) {
	if ($.inArray(currently_clicked_link.id, ['main-menu-handle', 'my-account-menu'])==-1 && $('#main-navigation>ul').css('width')!='0px')
		($('#main-navigation #main-menu-handle').length ? $('#main-navigation #main-menu-handle') : $('#main-navigation #my-account-menu')).click();
	
	if (currently_clicked_link.id!='list-filter-handle' && currently_clicked_link.id!='list-sort-handle' && $('#list-search-form').css('height')!='0px' && $(window).width()<=635) // We don't close the search form when displaying its filter sub-form...
		$('#main-navigation #list-filter-handle').click();
	
	if (currently_clicked_link.id!='list-sort-handle' && currently_clicked_link.id!='list-filter-handle' && $('#list-filter-form').css('height')!='0px') // We don't close the filter sub-form when displaying its parent search form...
		$('#main-navigation #list-sort-handle').click();
}

$(window).resize(function() {
		$('#main-navigation>ul').css('height', ($(window).width()>768 ? '' : ($('body').height() - parseInt($('#main-navigation>ul').css('padding-top')))));
	});
$(window).resize();

$('#main-navigation #main-menu-handle,#main-navigation #my-account-menu').click(function() {

	if ( $(window).width()<=768 ) {			
		var menu_closed = ( parseInt( $('#main-navigation>ul').css('width') ) <= 0 );

		$('#main-navigation').toggleClass( 'toggle-menu' );
		$('#main-navigation>ul').animate({
			width: (menu_closed ? '80%' : '0')
		});
		 
		/* if (menu_closed) {
			handle_floating_panels(this);
			$zopim.livechat.hideAll();
		} else {
			$zopim.livechat.button.show();
		} */
	}
});

function show_top_message(message) {
	if ($('#top-message').length) {
		// Close any previously-displayed message, before showing the new one
		$('#top-message>a').click();

		setTimeout(function() {
			show_top_message(message);
		}, 500);

		return;
	}

	var message = $('<div id="top-message"><div>' + message + '</div><a href="javascript:;">&times;</a></div>');
	$('#body>header').after(message);

	$('#top-message').animate({
		height: $('#top-message')[0].scrollHeight
	}, {
		progress	: function() {

		}
	});

	$('#top-message>a').click(function() {
		$('#top-message').animate({
			height: 0
		}, {
			complete	: function() {
				$('#top-message').remove();
			}, 
			duration	: 400, 
			progress	: function() {

			}
		});
	});

	setTimeout(function() {
		$('#top-message>a').click();
	}, 5000);
}

function getHighestZIndex() {
	var z_index = 0;

	$.each($('body *'), function(index, element) {
		if ($(element).css('z-index') && parseInt($(element).css('z-index'))>z_index)
			z_index = parseInt($(element).css('z-index'));
	});

	return z_index;
}

function show_modal(element) {
	var modal = $('<div class="modal-container"></div>'), 
		parent = element.parent();
	modal.append(element);
	$('body').append(modal);

	modal.click(function() {
		if ($(arguments[0].target).hasClass('modal-container'))
			// Clicked outside the modal element
			element.find('a.close').click();
	});

	// Get the highest z-index level, and set the current modal
	// on top of it
	modal.css('z-index', (getHighestZIndex() + 1));

	// Now center the modal's content in it
	$(window).resize(function() {
		var top = ((window.innerHeight - element.outerHeight()) / 2);
		if (top<0) top = 0;

		element.css({
				left	: (($('body').outerWidth() - element.outerWidth()) / 2), 
				top		: top
			});
	});
	$(window).resize();

	modal.animate({
		opacity: 1
	}, {
		start: function() {
			$('body').addClass('hide-overflow');
		}
	});
	
	element.find('a.close').click(function() {
		modal.animate({
			opacity: 0
		}, {
			complete: function() {
				parent.append(element);
				modal.remove();
				
				if (!$('div.modal-container').length)
					$('body').removeClass('hide-overflow');
			}
		});
	});
}

$('#login-button').click(function() {
	show_modal($('#login-panel'));
});


$('#fb-login-button,#fb-register-button').click(function() {
		var button = $(this);
		
		FB.login(function(response) {
				if (response.authResponse) {
					// connected
					var token = response.authResponse.accessToken, 
						loading = $('<img class="fb-loading" src="i/dot.png" />');
					button.after(loading);
					
					FB.api('/me?fields=first_name,last_name,gender,email', function(response) { // ,location
							if (response.error)
								alert('Facebook ' + response.error.type + ' error:\n' + response.error.message);
							else {
								$.ajax({
										data		: {
											token : token
										}, 
										dataType	: 'json', 
										success		: function(data) {
											loading.remove();
											
											if (data) {
												if (data.success) {
													document.location.reload();
												} else if (data.error) {
													alert(data.error);
												}
											};
										}, 
										url			: root + lang + '/register.html', 
										type		: 'POST'
									});
							}
						});
				}
			}, 
			{
				scope: 'email' // ,user_location
			});
	});

$('#login-panel form').first().validate({
		rules			: {
			'email'		: {
				//email		: true, 
				required	: true
			}, 
			'password'	: {
				minlength	: 5, 
				required	: true
			}
		}, 
		submitHandler	: function(form) {
			// First let's check if the submitted email address is unique, 
			// or if the delivery address is within any of the current restaurant's
			// delivery zones
			
			$.ajax({
					data		: $('#login-panel form').first().serialize(), 
					dataType	: 'json', 
					success		: function(data) {
						loading.remove();
						
						if (data) {
							if (data.success)
								document.location.reload();
								
							else if (data.error)
								alert(data.error);
						};
					}, 
					url			: root + lang + '/login.html', 
					type		: 'POST'
				});
		}
	});

var loading;
$('#login-panel input[type=submit]').click(function() {
		if ($('#login-panel form').first().valid()) {
			loading = $('<img class="loading" src="i/dot.png" />');
			$(this).after(loading);
		}
	});



function fill_location_fields(form, place) {
	form.find('input[name=location]').val(place.formatted_address);
	
	form.find('input[name=building_no]').val('');
	form.find('input[name=address]').val('');
	form.find('input[name=city_name]').val('');
	form.find('input[name=country_code]').val('');
	form.find('input[name=zip_code]').val('');
	
	for (var i=0, j=place.address_components.length; i<j; i++) {
		if ($.inArray('street_number', place.address_components[i].types)>-1)
			form.find('input[name=building_no]').val(place.address_components[i].long_name);
		else if($.inArray('route', place.address_components[i].types)>-1)
			form.find('input[name=address]').val(place.address_components[i].long_name);
		else if($.inArray('locality', place.address_components[i].types)>-1)
			form.find('input[name=city_name]').val(place.address_components[i].long_name);
		else if($.inArray('country', place.address_components[i].types)>-1)
			form.find('input[name=country_code]').val(place.address_components[i].short_name);
		else if($.inArray('postal_code', place.address_components[i].types)>-1)
			form.find('input[name=zip_code]').val(place.address_components[i].long_name);
	}
	
	form.find('input[name=latitude]').val(place.geometry.location.lat().toFixed(5));
	form.find('input[name=longitude]').val(place.geometry.location.lng().toFixed(5));
}

function setupAddressAutocomplete(form, dont_submit) {
	var autocomplete = new google.maps.places.Autocomplete(form.find('input[type!=hidden][name=location]')[0], {
			componentRestrictions: {
				country: 'ch'//, 'fr'
			}, 
			types: ['geocode']
		});
	google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			
			if (place.geometry)
				fill_location_fields(form, place);
			
			// Let's store the currently searched address
			var callback = (!dont_submit ? function() {
					form.submit();
				} : function(){});
			saveSearchAddress(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), callback);
		});
	
	/*
	// WE SHOULD ONLY DO THIS AFTER YOU CLICK ON THE LOCATION ICON
	if (!form.find('input[name=location]').val().trim() && navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
				var geocoder = new google.maps.Geocoder();
				
				geocoder.geocode({
						'latLng': new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
					}, function(results, status) {
						if (status==google.maps.GeocoderStatus.OK) {
							if (results[1]) {
								fill_location_fields(form, results[1]);
								form.find('input[name=location]').addClass('found');
							}
						}
					});
				
			}, function error(msg) {
				// console.log(arguments);
			});
	}
	*/
}

function saveSearchAddress(address, latitude, longitude, callback) {
	$.ajax({
			data		: {
				address		: address, 
				latitude	: latitude, 
				longitude	: longitude
			}, 
			success		: callback, 
			url			: root + lang + '/save-search.html', 
			type		: 'POST'
		});
}



$('#footer-contact,#footer-suggestion,#footer-specials').click(function() {
		var content_id = this.id;
		
		$.ajax({
				data		: {
					content : content_id
				}, 
				//dataType	: 'json', 
				success		: function(data) {
					if (data) {
						$('body').append($(data));
						show_modal($(data));
						
						if (content_id=='footer-suggestion')
							setTimeout(validateRestaurantSuggestion, 1000);
					}
				}, 
				url			: root + lang + '/get-panel.html', 
				type		: 'POST'
			});
	});

function validateRestaurantSuggestion() {
	$('#suggest-restaurant').validate({
			rules			: {
				'suggestion_name'		: 'required', 
				'suggestion_restaurant'	: 'required', 
				'suggestion_address'	: 'required'
			}, 
			submitHandler	: function(form) {
				// Submit the restaurant to the current page itself, 
				// but via AJAX
				var loading = $('<img class="loading" src="i/dot.png" />');
				$('#suggest-restaurant input[type=submit]').after(loading);
				
				$.ajax({
						data		: $('#suggest-restaurant').serialize(), 
						dataType	: 'json', 
						success		: function(data) {
							loading.remove();
							
							if (data && data.message) {
								$('#suggest-restaurant input[type=text]').val('');
								show_top_message(data.message);
							}
						}, 
						url			: document.location.href, 
						type		: 'POST'
					});
			}
		});
}

$.each($('.review-stars:not(.no-change)'), function(index, review) {
	var stars = $(review).text().trim().length;
	$(review).empty();

	for (var i=1; i<=5; i++) {
		$(review).append($('<span' + (i<=stars ? ' class="selected"' : '') + '></span>'));
	}
});

$.each($('.price-range'), function(index, range) {
	var level = $(range).text().trim().length;
	$(range).empty();
	
	for (var i=1; i<=5; i++) {
		$(range).append((i<=level ? '<span class="selected">' : '') + '&euro;' + (i<=level ? '</span>' : ''));
	}
});



function handle_restaurant_datetime(datetimes) {
	var selected_dow = $('#checkout-button select[name=date] option:selected').data('dow'), 
		current_hours = [], 
		enabled_dows = [], 
		
		current_dow = (new Date(current_datetime)).getDay(), 
		current_hour = (new Date(current_datetime)).getHours(), 
		current_minute = (new Date(current_datetime)).getMinutes();
	
	if (current_dow==0)
		current_dow = 7;
	if (current_hour<10)
		current_hour = '0' + current_hour;
	if (current_minute<10)
		current_minute = '0' + current_minute;
	
	var current_time = current_hour + ':' + current_minute; 
	
	
	for (var i=0, j=datetimes.length; i<j; i++) {
		var datetime = datetimes[i], 
			dow = datetime.dow, 
			start = datetime.start, 
			end = datetime.end;
		
		if (dow==selected_dow)
			current_hours.push({
					start	: start, 
					end		: end
				});
		
		if ($.inArray(dow, enabled_dows)==-1)
			enabled_dows.push(dow);
	}
	
	$.each($('#checkout-button select[name=date] option'), function(index, option) {
			if ($.inArray($(option).data('dow').toString(), enabled_dows)==-1)
				// This date should be disabled
				option.disabled = true;
		});
	//$('#checkout-button select[name=date]').change(); // To refresh any potentially-disabled options
	
	
	var delivery_time = 0;
	if ($('#checkout-button select[name=type]').val()=='D' && current_delivery_zone)
		delivery_time = (parseInt(current_delivery_zone.estimated_time) * 100) / 60;
	
	$.each($('#checkout-button select[name=time] option'), function(index, option) {
			var disabled = true, 
				time = parseInt(option.value.replace(':', ''));
			
			if (current_dow!=selected_dow || 
				(parseInt(current_time.replace(':', '')) + delivery_time)<=time) {
				
				for (var i=0, j=current_hours.length; i<j; i++) {
					var time_ = current_hours[i], 
						start = parseInt(time_.start.replace(':', '')), 
						end = parseInt(time_.end.replace(':', ''));
					
					if (time>=start && time<=end)
						disabled = false;
				}
			}
			
			option.disabled = disabled;
		});
	//$('#checkout-button select[name=time]').change(); // To refresh any potentially-disabled options
	
	if ($('#checkout-button select[name=time] option:selected')[0].disabled) {
		// We should select the 1st available time (closest to the current time)
		// that's available for today's order
		
		$.each($('#checkout-button select[name=time] option'), function(index, option) {
				if (!option.disabled) {
					$('#checkout-button select[name=time] option:selected').removeAttr('selected');
					
					$(option).attr('selected', 'selected');
					$('#checkout-button select[name=time]').change();
					
					return false;
				}
			});
		
		
		// And now let's notify the visitor that the previously selected time
		// had to be changed
		var message = ($('#checkout-button select[name=type]').val()=='D' ? langNextDeliveryTime : langNextTakeawayTime), 
			next_datetime = message.replace('{datetime}', ($('#checkout-button select[name=date] option:selected').text() + ' ' + $('#checkout-button select[name=time] option:selected').text()));
		show_top_message(next_datetime);
		
		// ... and update the search time
		var data = $('<form></form>').append($('#shopping-cart').clone()).serialize(); // Since we don't have a form around the checkout page's shopping cart
		$.ajax({
				data		: data, 
				url			: root + lang + '/restaurants-list.html', 
				type		: 'POST'
			});
	}
}

if ($('#checkout-button select[name=date]').length && $('#checkout-button select[name=time]').length && $('#checkout-button select[name=type]').length && 
	typeof timetable=='object' && typeof delivery_hours=='object') {
	// Set up the shopping cart's date/time fields functionality, 
	// based on the current restaurant's timetable and delivery hours
	$('#checkout-button select[name=type]').on('change', function() {
			if ($(this).val()=='D') {
				// Delivery functionality, based on the restaurant's delivery hours
				handle_restaurant_datetime(delivery_hours);
				
			} else {
				// Take-away functionality, based on the restaurant's timetable
				handle_restaurant_datetime(timetable);
			}
		});
	
	$('#checkout-button select[name=date]').on('change', function() {
			// Trigger the time field's change, via the order type select
			$('#checkout-button select[name=type]').change();
		});
}


$('#shopping-cart-handle').click(function() {
	if ($(window).width() <= 550 ) {
		$('#shopping-cart-container').toggleClass('opened');
	}
	else {
		$('#shopping-cart-container').animate({
			right: ($('#shopping-cart-container').hasClass('opened') ? -$('#shopping-cart-container').outerWidth() : 0)
		}, {
			complete: function() {
				$(this).toggleClass('opened');
			}
		});
	}
});
//$('#shopping-cart-handle').click();

if ($('#shopping-cart-items').length) {
	// Shopping cart-related functionality
	function loadCartContent(restaurant, func) {
		$.ajax({
				data		: {
					restaurant : restaurant
				}, 
				dataType	: 'json', 
				success		: function(data) {
					//loading.remove();
					$('#shopping-cart-items').empty();

					if (data && data.items && data.items.length) {
						$('#shopping-cart-items').css('display', 'block');
						$('#restaurant-menu-list li[data-menu-item-id] div.right').removeClass('checked');

						for (var i=0, j=data.items.length; i<j; i++) {
							var item = data.items[i];

							if (item.menu_item) {
								// Menu item
								var menu_item = '<li class="menu-item"><div>' + 
										'<span class="left quantity">' + item.quantity + '</span>' + 
										'<div class="left menu-item-description"><strong>' + item.name + '</strong>';

								if (item.options.length) {
									menu_item += '<ul>';

									for (var k=0, l=item.options.length; k<l; k++) {
										var option = item.options[k];
										menu_item += '<li class="menu-item-option" data-menu-item-option="' + option.id + '"><div class="menu-item-option-name">' + option.name + '</div>' + 
											'<span class="price">' + parseFloat(option.price * item.quantity).toFixed(2) + '</span></li>';
									}

									menu_item += '</ul>';
								}

								if (item.instructions) {
									var instructions = $('<em></em>').text(item.instructions); // In order to escape any potentially malitious code
									menu_item += instructions.outerHtml();
								}

								menu_item += '<a class="edit" data-menu-item-id="' + item.menu_item + '" href="' + root + restaurant + '.html#edit-menu-item-' + item.menu_item + '">' + langEdit + '</a><a class="delete" data-menu-item-id="' + item.menu_item + '" href="javascript:;">' + langDelete + '</a></div>' + 
									'<span class="price">' + parseFloat(item.unit_price * item.quantity).toFixed(2) + '</span>' + 
									'<div class="clear"></div>' + 
								'</div></li>';

								menu_item = $(menu_item);
								if (typeof editCartItem!='undefined')
									menu_item.find('a.edit').click(editCartItem);
								menu_item.find('a.delete').click(deleteCartItem);

								$('#shopping-cart-items').append(menu_item);
								menu_item.animate({
									height: menu_item[0].scrollHeight
								});

								if ($('#restaurant-menu-list li[data-menu-item-id=' + item.menu_item + ']').length)
									$('#restaurant-menu-list li[data-menu-item-id=' + item.menu_item + '] div.right').addClass('checked');
							} else if (item.promo_code) {
								// Promo code
								var promo_code = $('<li class="promo-code"><div>' + 
										'<span class="left quantity">1</span>' + 
										'<div class="left menu-item-description"><strong>' + item.title + '</strong></div>' + 
										'<span class="price">-' + item.value + '</span>' + 
										'<div class="clear"></div>' + 
									'</div></li>');

								$('#shopping-cart-items').append(promo_code);
								promo_code.animate({
									height: promo_code[0].scrollHeight
								});
							}
						}

						checkTotalCartValue();

						if (location.hash.indexOf('#edit-menu-item-')===0 && $('li[data-menu-item-id=' + location.hash.replace('#edit-menu-item-', '') + ']').length)
							// Edit the specified menu item
							$('#shopping-cart-items .edit[data-menu-item-id=' + location.hash.replace('#edit-menu-item-', '') + ']').click();
							location.hash = ''; // Delete the edit hash
					}

					setCartVisibility();
					if (func)
						func();
				}, 
				url			: root + lang + '/cart-load.html', 
				type		: 'POST'
			});
	}

	function checkTotalCartValue() {
		var discount_total = 0, 
			grand_total = 0, 
			has_promo = false;

		$.each($('#shopping-cart-items>li'), function(index, item) {
				var base_price = parseFloat($(item).find('.price').last().text()), 
					option_prices = 0;
	
				$.each($(item).find('li'), function(index, option) {
					option_prices += parseFloat($(option).find('.price').text());
				});
	
				discount_total += base_price + option_prices;
				if (!$(item).hasClass('promo-code'))
					grand_total += base_price + option_prices;
				else
					has_promo = true;
			});
		
		if (discount_total<0)
			discount_total = 0;
		
		if (has_promo && $('#promo-code').length) {
			$('#promo-code').animate({
					height: 0
				}, {
					complete: function() {
						$('#promo-code').remove();
					}
				});
		}
		
		var delivery_fee = 0;
		
		if ($('#checkout-button select[name=type]').val()=='D')
			delivery_fee = parseFloat($('#total-delivery-fee td>span').data('value'));
		
		var exchange_rate = 1, 
			currency = (typeof current_currency!='undefined' ? current_currency : 'CHF');
		if ($('#checkout-form select[name=currency]').length && !$('#checkout-form select[name=currency]').is(':disabled')) {
			exchange_rate = parseFloat($('#checkout-form select[disabled!=disabled][name=currency] option:selected').data('exchange'));
			currency = $('#checkout-form select[disabled!=disabled][name=currency]').val();
		}
		
		$('#cart-total tbody span').first()
			.text((exchange_rate * discount_total).toFixed(2));
		$('#total-delivery-fee td>span')
			.text((exchange_rate * delivery_fee).toFixed(2));
		
		$('.grand-total-value').text((exchange_rate * (discount_total + delivery_fee)).toFixed(2));
		$('.grand-total>span.currency').text(currency);
		$('#cart-total strong').text(currency);
		
		if ($('#checkout-form').length) {
			// We're in the checkout page, so let's replace the dynamic submit button texts
			$('#checkout-form input.rounded-red-button')
				.val(langPlaceOrderX.replace('{x}', $('.grand-total-value').text() + ' ' + currency))
				.toggleClass('disabled', ($('#checkout-button select[name=type]').val()=='D' && (
							current_delivery_zone ? grand_total<parseFloat(current_delivery_zone.minimum_delivery) : true
						)
					));
			$('#checkout-button input.rounded-red-button')
				.val(langPlaceOrderX.replace('{x}', $('.grand-total-value').text() + ' ' + currency));
		}
		
		$('#checkout-button input.rounded-red-button')
			.toggleClass('disabled', ($('#checkout-button select[name=type]').val()=='D' && (
					current_delivery_zone ? grand_total<parseFloat(current_delivery_zone.minimum_delivery) : true
				)
			));

		if ($('#cart-summary').length) {
			// The restaurant page's mobile top menu bar / link
			$('#cart-summary>strong').text((discount_total + delivery_fee).toFixed(2));
			$('#cart-summary>span').find('span').text($('#shopping-cart-items>li').length);
		}

		if ($('#cart-summary-container').length) {
			// The checkout page's mobile top menu bar / link
			$('#cart-summary-container div.left>strong').find('span').text($('.grand-total-value').text());
			$('#cart-summary-container div.left>span').find('span').text($('#shopping-cart-items>li').length);
		}

		// Now let's see if the current order is supposed to be a delivery one and, if so, 
		// if it contains enough items to go over the delivery minimum amount.
		// If not, we'll disable the checkout button.
		$('#checkout-button a.rounded-red-button').toggleClass('disabled', (
			grand_total==0 || ($('#checkout-button select[name=type]').val()=='D' && 
			(current_delivery_zone ? grand_total<parseFloat(current_delivery_zone.minimum_delivery) : true))
		));

		if ( current_delivery_zone ) {
			if ( $('#checkout-button select[name=type]').val()=='D' && grand_total<parseFloat(current_delivery_zone.minimum_delivery) ) {
				$('#checkout-button a.rounded-red-button').html( "Minimum delivery " + current_delivery_zone.minimum_delivery );
			}
			else {
				$('#checkout-button a.rounded-red-button').html( "Checkout" );
			}
		}
		
		if ( $('#checkout-button a.rounded-red-button').hasClass('disabled') ) {
			$(".preceed-to-checkout").css('display', 'none');
		}
		else {
			$(".preceed-to-checkout").css('display', '');
		}
	}

	function deleteCartItem() {
		var id = $(this).data('menu-item-id'), 
			menu_item = $(this).parents('li').first();

		$.ajax({
			data		: {
				index		: $('#shopping-cart-items>li').index(menu_item), 
				restaurant	: restaurant_url
			}, 
			dataType	: 'json', 
			success		: function(data) {
				//loading.remove();

				if (data && data.success) {
					if ($('#restaurant-menu-list li[data-menu-item-id=' + id + ']').length)
						$('#restaurant-menu-list li[data-menu-item-id=' + id + '] div.right').removeClass('checked');

					menu_item.animate({
						height: 0
					}, {
						complete: function() {
							menu_item.remove();
							checkTotalCartValue();
							setCartVisibility();
							cartItemCountUpdate();
						}
					});
				}
			}, 
			url			: root + lang + '/cart-remove.html', 
			type		: 'POST'
		});
	}
	//$('#shopping-cart-items a.delete').click(deleteCartItem);

	function setCartVisibility() {
		$('#checkout-button select[name=type]').change();

		if (!$('#shopping-cart-items').children('li').length) {
			// No more selected menu items, let's display the 
			// "Your bag is empty" panel
			$('#empty-cart').animate({
					height: $('#empty-cart')[0].scrollHeight
				}, {
					start: function() {
						$('#shopping-cart-items').css('display', 'none');
					}
				});
			// ... hide the checkout button
			$('#cart-checkout').animate({
				height: 0
			});
		} else {
			// First let's hide the "Your bag is empty" panel
			$('#empty-cart').animate({
				height: 0
			}, {
				complete: function() {
					$('#shopping-cart-items').css('display', 'block');
				}
			});

			setTimeout(function() {
				// ... (re)display the checkout buttons
				$('#cart-checkout').animate({
						height: $('#cart-checkout')[0].scrollHeight
					});
			}, 400);
		}
	}

	loadCartContent(restaurant_url);

	// Transform all of the cart form's select fields
	// into custom ones
	$('#shopping-cart select').select2({
		minimumResultsForSearch	: -1
	});

	$('#checkout-button select[name=type]').on('change', function() {
		/* if ($(this).val()=='D' && $('#shopping-cart-items li').length) {
			if ($('#shopping-cart #order-delivery-costs>div').height()==0) {
				$('#shopping-cart #order-delivery-costs>div').animate({
						height: $('#shopping-cart #order-delivery-costs>div')[0].scrollHeight
					});
			}

			if ($('#total-delivery-fee').css('display')=='none')
				$('#total-delivery-fee').css('display', '');

		} else {
			if ($('#shopping-cart #order-delivery-costs>div').height()>0) {
				$('#shopping-cart #order-delivery-costs>div').animate({
						height: 0
					});
			}

			if ($('#total-delivery-fee').css('display')!='none')
				$('#total-delivery-fee').css('display', 'none');

		} */

		checkTotalCartValue();
	});

	$('#promo-code input[type=button]').click(function() {
		var promo_button = $(this), 
			promo_field = $('#promo-code input[name=promo]'), 
			promo_code = promo_field.val();

		if (promo_code.trim()=='') {
			promo_field.addClass('error');

		} else {
			// Let's try to validate the promo code
			var loading = $('<img class="loading" src="i/dot.png" />');
			promo_button.after(loading);
			promo_button.addClass('loading');

			$.ajax({
				data		: {
					restaurant	: restaurant_url, 
					code		: promo_code.trim()
				}, 
				dataType	: 'json', 
				success		: function(data) {
					loading.remove();
					promo_button.removeClass('loading');
					
					if (data && data.success) {
						$('#promo-code').animate({
								height: 0
							}, {
								complete: function() {
									$('#promo-code').remove();
								}
							});
						loadCartContent(restaurant_url);
						
					} else if (data.message)
						show_top_message(data.message);
				}, 
				url			: root + lang + '/use-promo.html', 
				type		: 'POST'
			});
		}
	});
}

function cartItemCountUpdate() {
	var cartHandle = $("#shopping-cart-handle");
	var cartItems = 0;

	$("ul#shopping-cart-items li.menu-item").each( function ( idx, obj ) {
		cartItems = cartItems + parseInt( $(obj).find( 'span.quantity' ).text() );
	} );

	if ( cartItems == 0 ) {
		cartHandle.find( "strong" ).remove();
	}
	else {
		if ( cartHandle.find( "strong" ).length == 0 ) {
			cartHandle.append( "<strong>" + cartItems + "</strong>" );
		}
		else {
			cartHandle.find( "strong" ).text( cartItems );
		}
	}
}

function doCustomRadio(input) {
	var span = $('<span class="custom-radio"></span>');

	$(input)
		.before(span)
		.click(function() {
				var input = this;
				setTimeout(function() {
						span.toggleClass('checked', input.checked);
					}, 10);
			});
}

function injectRoundedSelect(select) {
	var select2 = $(select).next('span.select2');
	select2.addClass('rounded-select');
	
	if ($(select).hasClass('hidden'))
		select2.addClass('hidden');
}

$(document).ready(function() {
	$.each($('input.custom-radio'), function(index, input) {
			doCustomRadio(input);
		});
	
	$.each($('.toggle-buttons'), function(index, buttons) {
			// Add the margin elements
			var links = $(buttons).find('a'), 
				content = $('.toggle-content[data-set=' + $(buttons).data('set') + ']');
	
			// links.first().prepend($('<span></span>'));
			// links.last().append($('<span></span>'));
	
			links.click(function() {
					var target = $(this).data('target');
		
					links.removeClass('checked');
					$(this).addClass('checked');
		
					content.children('div').find('input,select,textarea').attr('disabled', 'disabled');
					content.children('div').animate({
						height: 0
					}, {
						complete: function() {
							// $(this).find('input,select,textarea').attr('disabled', 'disabled');
						}
					});
		
					$.each(content.children('div[data-target=' + target + ']').find('input,select,textarea'), function(inxed, element) {
							if (!$(element).parents('div.disabled').length)
								$(element).removeAttr('disabled');
						});
					
					content.children('div[data-target=' + target + ']').animate({
							height: content.children('div[data-target=' + target + ']')[0].scrollHeight
						}, {
							complete	: function() {
								content.children('div[data-target=' + target + ']').css('height', 'auto');
							}
						});
				});
		});
	
	$.each($('select.rounded-select'), function(index, select) {
			// Inject the .rounded-select class to all new select2 elements
			injectRoundedSelect(select);
		});
	
	$(window).scroll(function() {
			var scrollTop = parseInt(window.pageYOffset || document.documentElement.scrollTop || body.scrollTop);
			$('#main-to-top').toggleClass('visible', (scrollTop>window.innerHeight));
		});
	
	$('#main-to-top').click(function() {
		$('html,body').animate({
				scrollTop: 0
			}/*, Math.round(parseInt(window.pageYOffset || document.documentElement.scrollTop || body.scrollTop) * .75)*/);
		
		return false;
	});

	$('a.popup').click(function() {
		window.open(this.href);
		return false;
	});

	$(window).scrollTop(0);
	$( "body" ).waypoint( function( direction ) {
			if ( direction == 'down' ) {
				$('#body>header').addClass('scrolled');
	
				setTimeout( function() {
					$('#body>header').addClass('doanimate');
				}, 200 );
	
				$('#main-navigation .top-menu-arrow').addClass('top-menu-arrow-dark');
			}
			else {
				$('#body>header').removeClass( 'doanimate' ).removeClass('scrolled');
				$('#main-navigation .top-menu-arrow').removeClass('top-menu-arrow-dark');
			}
		}, {
			offset: -100
		});
	
	$('#helper-handle > a').click(function() {
			$zopim.livechat.window.show();
		});
	
	$('#checkout-button a.rounded-red-button,#cart-checkout input.rounded-red-button').on('click', function() {
		if ($(this).hasClass('disabled')) {
			show_top_message(!$('#shopping-cart-items').children('li').length ? langEmptyCart : langMoreMoney);
			return false;
		}

		$(this).parents('form').first().submit();
	});
	
	$(".preceed-to-checkout > a.preceed").click( function() {
		$("#checkout-button a.rounded-red-button").trigger('click');
	});
});

jQuery.validator.addMethod('full_url', function(val, elem) {
	// if no url, don't do anything
	if (val.length == 0) { return true; }

	// if user has not entered http:// https:// or ftp:// assume they mean http://
	if(!/^(https?|ftp):\/\//i.test(val)) {
		val = 'http://' + val; // set both the value
		//$(elem).val(val); // also update the form element
	}

	return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(val);	
});

// Utility for intersecting arrays and returning the elements
// matching both inputs
jQuery.arrayIntersect = function(a, b) {
	return $.grep(a, function(i) {
		return $.inArray(i, b) > -1;
	});
};

jQuery.fn.outerHtml = function(s) {
	return s
		? this.before(s).remove()
		: jQuery('<p>').append(this.eq(0).clone()).html();
};

function escapeHtml(unsafe) {
	return (unsafe ? 
		unsafe
		 .replace(/&/g, "&amp;")
		 .replace(/</g, "&lt;")
		 .replace(/>/g, "&gt;")
		 .replace(/"/g, "&quot;")
		 .replace(/'/g, "&#039;")
	 	: unsafe);
}

new WOW().init();

window.fbAsyncInit = function(){
	FB.init({
			appId		: fb_app_id, 
			version		: 'v2.3', 
			xfbml		: true
		});
};

(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src='//connect.facebook.net/'+lang_code+'/sdk.js';fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));

window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?M291b30URCXngb6stC5r8sRA299pKnTB";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");

$zopim(function(){
		$zopim.livechat.setLanguage(lang);
	});