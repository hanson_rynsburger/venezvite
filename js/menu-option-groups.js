
$('#add-menu-option-group-form select').select2({
		minimumResultsForSearch	: -1
	});


function update_option_ids() {
	var option_ids = [];
	$.each($('#menu-item-options').find('span'), function(index, option) {
			option_ids.push($(option).data('id'));
		});
	
	$('#add-menu-option-group-form input[name=options]').val(option_ids.join(','));
}

$('#menu-item-option').change(function() {
		if (this.value=='-1') {
			$('#new-menu-item-option').animate({
					'height' : $('#new-menu-item-option')[0].scrollHeight
				}, {
					complete	: function() {
						$('#new-menu-item-option').css('height', 'auto');
					//}, 
					//start		: function() {
						$('#new-menu-item-option input').removeAttr('disabled');
						$('#new-menu-item-option a.disabled.rounded-red-button').css('display', 'none');
						$('#new-menu-item-option').find('input').val('');
					}
				});
			
		} else {
			var id = this.value, 
				text = this[this.selectedIndex].text, 
				data = $(this[this.selectedIndex]).data('option');
			
			if (id && !$('#menu-item-options').find('span[data-id=' + id + ']').length)
				// Add menu option "pills"
				$('#menu-item-options').append('<span class="pill-element" data-id="' + id + '" data-option="' + escapeHtml(JSON.stringify(data)) + '"><a href="javascript:;">' + escapeHtml(text) + '</a> <a class="remove" href="javascript:;">&times;</a></span>');
			
			update_option_ids();
			
			$('#new-menu-item-option').animate({
					'height' : 0
				}, {
					complete: function() {
						$('#new-menu-item-option').find('input').attr('disabled', 'disabled');
					}
				});
		}
	});

$('#menu-item-options').on('click', 'a:not(.remove)', function() {
		// Edit
		var data = $(this).parents('span').first().data('option');
		
		$('#new-menu-item-option').animate({
				'height' : $('#new-menu-item-option')[0].scrollHeight
			}, {
				complete	: function() {
					$('#new-menu-item-option').css('height', 'auto');
				//}, 
				//start		: function() {
					$('#new-menu-item-option').find('input').removeAttr('disabled');
					$('#new-menu-item-option a.disabled.rounded-red-button').css('display', 'block');
				}
			});
		
		$('#new-menu-item-option input[name=option_id]').val(data.id);
		$('#new-menu-item-option input[name=menu_option]').val(data.option_name);
		$('#new-menu-item-option input[name=price]').val(data.price);
	});

$('#menu-item-options').on('click', 'a.remove', function() {
		$(this).parents('span').first().remove();
		update_option_ids();
	});



var validator = $('#add-menu-option-group-form').validate({
		errorPlacement	: function(error, element) {
			if (element.hasClass('rounded-select'))
				error.insertAfter(element.next('span.select2'));
			else
				error.insertAfter(element);
		}, 
		rules			: {
			'options_group'		: 'required', 
			'selectables_rule'	: 'required', 
			'selectables'		: {
				min		: 0, 
				number	: true
			}, 
			//'menu_option'		: 'required', 
			'price'				: {
				min		: 0, 
				number	: true
			}
		}
	});


$('#new-menu-item-option a.rounded-red-button').click(function() {
		validator.resetForm();
		
		if (!$(this).hasClass('disabled')) {
			// Let's try to save the new menu item option
			var submit_data = {
				id			: $('#new-menu-item-option input[name=option_id]').val(), 
				option_name	: $('#new-menu-item-option input[name=menu_option]').val().trim(), 
				price		: $('#new-menu-item-option input[name=price]').val().trim()
			};
			
			$.ajax({
					data		: submit_data, 
					dataType	: 'json', 
					success		: function(data) {
						
						if (data && data.success) {
							if (!data.update) {
								var added = false, 
									new_option = $('<option value="' + data.menu_option.id + '">' + data.menu_option.text + '</option>');
								
								$.each($('#menu-item-option option'), function(index, option) {
										if (option.text.localeCompare(data.menu_option.text) > 0) {
											new_option.insertBefore($(option));
											added = true;
											
											return false;
										}
									});
								
								if (!added)
									new_option.insertBefore($($('#menu-item-option option')[$('#menu-item-option option').length - 1]));
								
								$('#new-menu-item-option input').val('');
								
							} else {
								$('#menu-item-option option[value=' + data.menu_option.id + ']').html(data.menu_option.text);
								$('#menu-item-option').select2({
										minimumResultsForSearch	: -1
									});
								$('#menu-item-option').next('span.select2-container').addClass('rounded-select');
								
								var pill = $('#menu-item-options span[data-id=' + data.menu_option.id + ']');
								if (pill.length) {
									pill.data('option', submit_data);
									pill.find('a:not(.remove)').html(data.menu_option.text);
								}
							}
							
							$('#menu-item-option').select2('val', data.menu_option.id);
							
						} else
							// Just close the form
							$('#menu-item-option').select2('val', '');
					}, 
					url			: root + lang + '/add-menu-item-option.html', 
					type		: 'POST'
				});
			
		} else if ($('#new-menu-item-option input[name=option_id]').val() && confirm(langOptionDeleteConfirmation)) {
			var id = $('#new-menu-item-option input[name=option_id]').val();
			
			$.ajax({
					data		: {
						id : id
					}, 
					dataType	: 'json', 
					success		: function(data) {
						
						if (data && data.success) {
							$('#menu-item-option option[value=' + id + ']').remove();
							$('#menu-item-options span[data-id=' + id + ']').remove(); // Just in case...
							
							$('#menu-item-option').select2('val', ''); // Default on the 1st, empty value, so that the edit form would close
						}
					}, 
					url			: root + lang + '/delete-menu-item-option.html', 
					type		: 'POST'
				});
		}
	});


$('.white-container input[type=button]').click(function() {
		// Reset to Add form
		$('#add-menu-option-group-form').find('strong.title').text(langAddItem);
		
		$('#add-menu-option-group-form').find('input[name=id]').val('');
		$('#add-menu-option-group-form').find('input[name=options_group]').val('');
		$('#add-menu-option-group-form').find('select[name=selectables_rule]').select2('val', '');
		$('#add-menu-option-group-form').find('input[name=selectables]').val('');
		
		$('#menu-item-option').select2('val', '');
		$('#menu-item-options').empty();
		update_option_ids();
		
		$('.white-container input[type=submit]').val(langAddButton);
		$('.white-container input[type=button]').css('display', 'none');
	});


$('#menu-option-groups').sortable({
		axis		: 'y', 
		containment	: 'parent', 
		handle		: '.drag-icon', 
		scroll		: false, 
		tolerance	: 'pointer', 
		update		: function(e, ui) {
			var order = [];
			
			$.each($('#menu-option-groups').find('li'), function(index, option_group) {
					order.push($(option_group).data('id'));
				});
			
			$.ajax({
					data		: {
						menu_option_groups : order
					}, 
					dataType	: 'json', 
					url			: root + lang + '/order-menu-option-groups.html', 
					type		: 'POST'
				});
		}
	});


$('#menu-option-groups-body')
	.on('click', '.actions.edit', function() {
		var data = $(this).parents('li').first().data('menu-option-group');
		
		$('#add-menu-option-group-form').find('strong.title').text(langEditItem);
		
		$('#add-menu-option-group-form').find('input[name=id]').val(data.id);
		$('#add-menu-option-group-form').find('input[name=options_group]').val(data.group);
		$('#add-menu-option-group-form').find('select[name=selectables_rule]').select2('val', data.rule);
		$('#add-menu-option-group-form').find('input[name=selectables]').val(data.selectables);
		
		$('#menu-item-options').empty();
		if (data.options) {
			for (var i=0, j=data.options.length; i<j; i++) {
				
				$('#menu-item-option')
					.val(data.options[i])
					.change();
			}
			
		} else
			update_option_ids();
		
		$('.white-container input[type=submit]').val(langEditButton);
		$('.white-container input[type=button]').css('display', 'block');
		
		$('html,body').animate({
				scrollTop: ($('#add-menu-option-group-form').offset().top - $('#body>header').height())
			});
	})
	.on('click', '.actions.delete', function() {
		// First let's check if the chosen category doesn't already have some menu items attached to it
		// If so, we'll reject the delete request
		if ($(this).hasClass('disabled')) {
			var menu_items = $(this).parents('li').first().data('menu-items');
			
			alert(langDisabledCategory.replace('{used_menu_items}', menu_items.join('\n')));
			return false;
		}
		
		if (confirm(langRemoveConfirmation)) {
			var deleted_button = $(this), 
				id = deleted_button.parents('li').first().data('id');
			
			$.ajax({
					data		: {
						options_group : id
					}, 
					dataType	: 'json', 
					success		: function(data) {
						
						if (data && data.success)
							deleted_button.parents('li').first().remove();
					}, 
					url			: root + lang + '/delete-menu-options-group.html', 
					type		: 'POST'
				});
		}
	});



if (location.hash.indexOf('#menu_option_group_')===0 && $('#menu-option-groups li[data-id=' + location.hash.substr(19) + ']').length) {
	// Scroll to the submitted element
	$(window).bind('load', function() {
			$('html,body').animate({
					scrollTop: ($('#menu-option-groups li[data-id=' + location.hash.substr(19) + ']').offset().top - $('#body>header').height())
				});
		});
}
