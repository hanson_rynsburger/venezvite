
$('#login-restaurant-body form').first().validate({
		rules			: {
			'username'	: {
				email		: true, 
				required	: true
			}, 
			'password'	: {
				minlength	: 5, 
				required	: true
			}
		}
	});
