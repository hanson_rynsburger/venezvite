
function submitForm(form) {
	$.ajax({
		data		: $(form).serialize() + '&ajax=1', 
		dataType	: 'json', 
		success		: function(data) {
			//loading.remove();
			
			if (data) {
				if (data.success) {
					$(form).find('.rounded-red-button.disabled').click();
					
					if (data.fields) {
						for (var field in data.fields) {
							if ($('#' + field).length)
								$('#' + field).text(data.fields[field]);
						}
					}
				}
			}
		}, 
		url			: root + lang + '/financial-details.html', 
		type		: 'POST'
	});
}

$('#entity-finance-form').validate({
		rules			: {
			'company'	: 'required', 
			'iban'		: 'required', 
			'bank'		: 'required', 
			'swift'		: 'required'
		}, 
		submitHandler	: submitForm
	});



if ($('#entity-company').text()=='' && $('#entity-company').text()=='' && $('#entity-company').text()=='' && $('#entity-company').text()=='')
	// No financial details filled, let's display the edit form
	$('#entity-finance').find('a.green-button').click();
