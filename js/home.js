$('#home-search-form select[name=type],#home-search-form select[name=location]').select2({
		dropdownCssClass		: 'home-search-dropdown', 
		minimumResultsForSearch	: -1
	})
	.on('select2-open', function() {
		$('#select2-drop').css({
				left	: '', 
				top		: '', 
				width	: ''
			});
	});

setupAddressAutocomplete($('#home-search-form').children('form').first());

$.validator.setDefaults({
	ignore: [] // So that it would validate the custom, hidden select fields, too
});

$('#home-search-form form').first().validate({
	errorPlacement: function(error, element) {
		element.parents('div').first().append(error); // So that it won't push the "Find" button on a new row
	}, 
	rules : {
		'location' : 'required'
	}
});


$('#home-login').click(function() {
	$('#main-navigation #login-button').click();
});


$('#home-search-form input[name=location]').focus();


if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(function(position) {
		var geocoder = new google.maps.Geocoder;
		
		geocoder.geocode({
				'location': {
					lat: position.coords.latitude, 
					lng: position.coords.longitude
				}
			}, function(results, status) {
				if (status===google.maps.GeocoderStatus.OK && results[0])
					$('#home-search-form input[type!=hidden][name=location]')
						.val(results[0].formatted_address)
						.addClass('found');
			});
	});
}



$('div.parallax').each(function() {
		var obj = $(this), 
			top = obj.offset().top - $(window).height(), 
			bottom = top + $(window).height() + obj.outerHeight(), 
			step = Math.floor(($(window).height() + obj.outerHeight()) / 100);
		
		$(window).scroll(function() {
			// Move the background
			var scroll = $(this).scrollTop();
			
			if (scroll > top && scroll < bottom) {
				obj.css({
						backgroundPosition: '50% ' + ((scroll - top) / step).toFixed(2) + '%'
					});
			}
		});
	});



function animateBulletContainer(container) {
	$(container).animate({
		scrollLeft: $(container).find('.bullet-item')[container.index].offsetLeft
	});
	
	$(container).next('.bullet-links').find('.bullet-link').removeClass('current-bullet-link');
	$($(container).next('.bullet-links').find('.bullet-link')[container.index]).addClass('current-bullet-link');
}

function resizeHomePanels() {
	$.each($('.bullet-panel'), function(index, container) {
		var item = $(container).find('.bullet-item').first();
		if (typeof container.index=='undefined')
			container.index = 0;
		if (!container.old_width)
			container.old_width = item.outerWidth();
		if (!container.old_padding)
			container.old_padding = item.outerWidth() - item.width();
		
		if (!$(container).next('.bullet-links').length) {
			var bullet_links = $('<div class="bullet-links"></div>');
			$(container).after(bullet_links);
		} else
			var bullet_links = $(container).next('.bullet-links');
		bullet_links.empty();
		
		if ($(container).outerWidth() < container.scrollWidth) {
			// We need to create bullets
			var elements = Math.floor($(container).outerWidth() / container.old_width), 
				bullets = Math.ceil($(container).find('.bullet-item').length / elements), 
				new_width = ($(container).outerWidth() / elements) - container.old_padding;
			
			$(container).find('.bullet-item').css('width', new_width);
			for (var i=0, j=bullets; i<j; i++) {
				var bullet_link = $('<span class="bullet-link"></span>');
				bullet_links.append(bullet_link);
				$(container).find('.bullet-item')[i].offsetLeft = $($(container).find('.bullet-item')[i]).offset().left;
				
				if (i==0)
					bullet_link.addClass('current-bullet-link');
			}
			
			$(container).on('swipeleft', function() {
					if (container.index >= (bullets - 1))
						return;
					
					container.index++;
					animateBulletContainer(container);
					
				}).on('swiperight', function() {
					if (container.index <= 0)
						return;
					
					container.index--;
					animateBulletContainer(container);
				});
		}
	});
}
$(window).resize(resizeHomePanels);
resizeHomePanels();

$(document).ready( function() {
	$( ".h-drop-down-location" ).on( 'hs-dropdown.on.change', function( e, el, val ) {
		if ( val =='-1') {
			// this.disabled = true;
			// $(this).next('span.select2').css('display', 'none');
			$(el).closest( ".h-drop-down-location" ).hide();
			$('#home-search-form input[name=location]').show();
			$('#home-search-form input[name=location]').removeAttr( 'disabled' );
		} else if ( val ) {
			$('#home-search-form form').first().submit();
		}
	} );
	
	$("a.slide-down").on( 'click', function() {
		$('html,body').animate({
			scrollTop: $("#home-top-callouts").offset().top - 75
		}, {
			duration: 1000,
			easing: "easeOutSine"
		});
	} );

	$("#home-search-form form input[type=submit]").on( 'click', function(e) {
		if( $("#home-search-form form").first().valid() ) {
			$(this).addClass('submitting').after( "<img class='home-search-loading' src='i/loading.gif'/>");
			return true;
		}
	} );
} );
