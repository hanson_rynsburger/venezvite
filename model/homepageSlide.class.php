<?php
    class homepageSlide {
        public $idSlide;
        public $language;
        public $image;
        public $description;
        public $url;
        public $order;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'homepageSlide.php';
        }
        
        public static function list_($language){
            self::__constructStatic();
            
            return listHomepageSlides($language);
        }
    }
