<?php
    class restaurantMenu {
        public $idMenu;
        public $restaurant;
        public $menuName;
        public $order;
        
        public $menuItems = array();
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'restaurantMenu.php';
        }
        
        public static function create($restaurant, $menuName = 'Default'){
            self::__constructStatic();
            
            return restaurantMenuCreate($restaurant, $menuName);
        }
        
        public static function list_($restaurant, $items = false, $allItems = false, $options = false, $sort = false) {
            self::__constructStatic();
            
            return listRestaurantMenus($restaurant, $items, $allItems, $options, $sort);
        }
    }
