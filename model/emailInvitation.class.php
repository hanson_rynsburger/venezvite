<?php
    class emailInvitation {
        public $idInvitation;
        public $user;
        public $email;
        public $promoCode;
        public $value;
        public $dateAdded;
        public $datePromoUsed;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic() {
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'emailInvitation.php';
        }
        
        public static function list_($user) {
            self::__constructStatic();
            
            return listEmailInvitations($user);
        }
        
        public static function insert($user, $email, $message) {
            self::__constructStatic();
            
            $body = 'Hi,

Your friend ' . $user->firstName . ' ' . $user->lastName . ' has sent you a sizzling hot invitation to save on your first order placed on venezvite.com. <a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . '">Claim it while it\'s still hot.</a>';
            
            if($message){
                $body .= '

' . htmlspecialchars($message);
            }
            
            $body .= '

Haven\'t heard of us? <a href="' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . '">Visit venezvite.com</a> or read on to learn more: Venezvite is the best way to order food online, for delivery or take-away, from a curated directory of superior quality restaurants.

We are now accepting lunch orders in Rolle!
Not in Rolle? Tell us where you are and recommend a restaurant. We are expanding everyday.';
            if(@sendMail($email, $email, $user->firstName . ' invites you on Venezvite', $body, null, 'simple')){
                insertEmailInvitation($user, $email);
            }
        }
        
        public static function getByID($idInvitation) {
            self::__constructStatic();
            
            return getEmailInvitationByID($idInvitation);
        }
        
        public static function getByEmail($email) {
            self::__constructStatic();
            
            return getEmailInvitationByEmail($email);
        }
        
        public static function getByCode($promoCode) {
            self::__constructStatic();
            
            return getEmailInvitationByPromoCode($promoCode);
        }
        
        public function setCode() {
            $promoCode = self::generateCode();
            setEmailInvitationCode($this, $promoCode);
            
            return $promoCode;
        }
        
        public function useCode($order) {
            useEmailInvitationCode($this, $order);
        }
        
        public static function generateCode() {
            $promoCode = 'EI' . strtoupper(substr(sha1(mt_rand()), 10, 8));
            
            if (self::getByCode($promoCode)) {
                // It seems the currently random generated code already exists! Let's try to generate a new one
                return self::generateCode();
            } else {
                return $promoCode;
            }
        }
        
        public static function getByOrder($order) {
            self::__constructStatic();
            
            return getEmailInvitationByOrder($order);
        }
    }
