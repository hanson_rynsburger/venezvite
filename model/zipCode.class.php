<?php
    class zipCode {
        public $idZipCode;
        public $zipCode;
        public $latitude;
        public $longitude;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'zipCode.php';
        }
        
        public static function get($zipCode){
            self::__constructStatic();
            
            return getZipCode($zipCode);
        }
        
        public static function insert($zipCode, $latitude, $longitude){
            self::__constructStatic();
            
            return insertZipCode($zipCode, $latitude, $longitude);
        }
    }
