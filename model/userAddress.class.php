<?php
    class userAddress {
        public $idAddress;
        public $user;
        public $corporateAccount;
        public $shippingProfileID;
        public $address;
        public $buildingNo;
        public $floorSuite;
        public $accessCode;
        public $city;
        public $zipCode;
        public $latitude;
        public $longitude;
        public $phone;
        public $instructions;
        public $default;
        public $dateAdded;
        public $addrType;
        
        function __construct() {
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'userAddress.php';
        }
        
        public static function list_($entity){
            self::__constructStatic();
            
            return listUserAddresses($entity);
        }
        
        public static function insert($entity, $address, $buildingNo, $floorSuite = null, $accessCode = null, $city, $zipCode, $latitude, $longitude, $phone, $default = 'N', $instructions = '', $addrType = 'home') {
            self::__constructStatic();
            
            $userAddresses = userAddress::list_($entity);
            if (count($userAddresses)==0) {
                // Set the first user address as default
                $default = 'Y';
            }
            
            return insertUserAddress($entity, $address, $buildingNo, $floorSuite, $accessCode, $city, $zipCode, $latitude, $longitude, $phone, $default, $instructions, $addrType);
        }
        
        public static function getByID($idAddress){
            self::__constructStatic();
            
            return getUserAddressByID($idAddress);
        }
        
        public function remove(){
            removeUserAddress($this);
        }
        
        public function update($address, $buildingNo, $floorSuite, $accessCode, $city, $zipCode, $latitude, $longitude, $phone, $default = 'N', $instructions = '', $addrType='home') {
            updateUserAddress($this, $address, $buildingNo, $floorSuite, $accessCode, $city, $zipCode, $latitude, $longitude, $phone, $default, $instructions, $addrType);
            
            $this->address = $address;
            $this->buildingNo = $buildingNo;
            $this->floorSuite = $floorSuite;
            $this->accessCode = $accessCode;
            $this->city = $city;
            $this->zipCode = $zipCode;
            $this->latitude = $latitude;
            $this->longitude = $longitude;
            $this->phone = $phone;
            $this->default = $default;
            $this->instructions = $instructions;
            $this->addrType = $addrType;
        }
        
        /**
         * Update an user address' Authorize.Net shipping profile ID
         * 
         **/
        public static function updateShippingProfile($userAddress, $shippingProfileID){
            self::__constructStatic();
            
            updateShippingProfile($userAddress, $shippingProfileID);
        }
        
        public function longFormat(){
            return !empty($this->idAddress) ? 
                ((!empty($this->buildingNo) ? $this->buildingNo . ' ' : '') . 
                $this->address . ', ' . 
                (!empty($this->floorSuite) ? $this->floorSuite . ', ' : '') . 
                (!empty($this->city) ? $this->city . ', ' : '') . 
                $this->zipCode . 
                (!empty($this->accessCode) ? ' (' . ACCESS_CODE . ' ' . $this->accessCode . ')' : '')) : null;
        }
        
        public function shortFormat(){
            return !empty($this->idAddress) ? 
                ((!empty($this->buildingNo) ? $this->buildingNo . ' ' : '') . 
                $this->address) : '';
        }
        
        public function setDefault($entity) {
            setDefaultUserAddress($this, $entity);
        }
    }
