<?php
    class city {
        public $idCity;
        public $country;
        public $cityName;
        public $latitude;
        public $longitude;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'city.php';
        }
        
        public static function search($city, $country = null){
            self::__constructStatic();
            
            return searchCities($city, $country);
        }
        
        public static function getByID($idCity){
            self::__constructStatic();
            
            return getCityByID($idCity);
        }
    }
