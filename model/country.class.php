<?php
	class country {
		public $idCountry;
		public $defaultLanguage;
		public $countryName;
		public $countryAcronym;
		public $countryFlag;
		public $latitude;
		public $longitude;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic() {
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'country.php';
		}
		
		public static function list_() {
			self::__constructStatic();
			return listCountries();
		}
		
		public static function getByID($id_country) {
			self::__constructStatic();
			return getCountryByID($id_country);
		}
	}
