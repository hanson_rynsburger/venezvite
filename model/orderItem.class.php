<?php
    class orderItem {
        public $idOrderItem;
        public $order;
        public $menuItem;
        public $menuItemName;
        public $quantity;
        public $pricePerItem;
        public $instructions;
        
        public $options = array();
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'orderItem.php';
        }
        
        public static function list_($order){
            self::__constructStatic();
            
            return listOrderItems($order);
        }
        
        public static function insert($order, $menuItem, $quantity, $instructions){
            self::__constructStatic();
            
            return insertOrderItem($order, $menuItem, $quantity, $instructions);
        }
    }
