<?php
	class order {
		public $idOrder;
		public $user;
		public $corporateAccount;
		public $userAddress;
		public $restaurant;
		public $type;
		public $payment;
		public $transactionID;
		public $value;
		public $deliveryCost;
		public $discount;
		public $exchangeRate;
		public $notes;
		public $dateAdded;
		public $paid;
		public $dateDesired;
		public $datePreApproved;
		public $dateReviewed;
		public $dateProcessed;
		public $rejectionReason;
		
		public $items = array();
		public $emailInvitation;
		public $promoCode;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'order.php';
		}
		
		public static function list_($entity = null, $restaurant = null, $future = true, $past = false, $startDate = null, $endDate = null) {
			self::__constructStatic();
			
			if (!$startDate) {
				$startDate = '2012-01-01';
			}
			if (!$endDate) {
				$endDate = date('Y-m-d H:i:s');
			}
			
			return listOrders($entity, $restaurant, $future, $past, $startDate, $endDate);
		}
		
		private static function getEmailTemplate($order, $template_file) {
			$entity = (@$order->user ? $order->user : $order->corporateAccount);
			$userFirstName = (get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName);
			$userLastName = (get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName);
			
			$template = file_get_contents(REL . VERSION_REL_PATH . 'newsletter/' . $template_file . '.html');
			$template = str_replace('{$root}', (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT, $template);
			$template = str_replace('{$orderTypeLabel}', P_ORDER_EMAIL_ORDER_TYPE, $template);
			$template = str_replace('{$orderType}', ($order->type=='D' ? P_ORDER_EMAIL_DELIVERY : P_ORDER_EMAIL_TAKEAWAY), $template);
			$template = str_replace('{$orderNoLabel}', P_ORDER_EMAIL_ORDER_NO, $template);
			$template = str_replace('{$orderNo}', $order->idOrder . (!empty($order->transactionID) ? '_' . $order->transactionID : ''), $template);
			
			if ($order->type=='D') {
				$template = str_replace('{$deliveryDateHour}', '<br />' . 
					P_ORDER_EMAIL_DELIVERY_DATE_HOUR . 
					': <strong>' . formatDate($order->dateDesired, true) . '</strong>', $template);
			} else {
				$template = str_replace('{$deliveryDateHour}', '', $template);
			}
			
			$template = str_replace('{$restaurant}', P_ORDER_EMAIL_RESTAURANT, $template);
			$template = str_replace('{$restaurantDetails}', $order->restaurant->restaurantName . 
				($order->restaurant->address ? '<br />' . $order->restaurant->address . ', ' . $order->restaurant->zipCode . ', ' . $order->restaurant->city : ''), $template);
			$template = str_replace('{$restaurantPhone}', P_ORDER_EMAIL_PHONE . ': ' . $order->restaurant->phone, $template);
			
			$template = str_replace('{$order}', P_ORDER_EMAIL_ORDER, $template);
			
			preg_match('/\{\$orderItem\}([\s\S]+)\{\/\$orderItem\}/', $template, $menuItemTemplate);
			preg_match('/\{\$orderItemOption\}([\s\S]+)\{\/\$orderItemOption\}/', $menuItemTemplate[1], $menuItemOptionTemplate);
			
			$cartContent = array();
			$totalQuantity = 0;
			
			foreach ($order->items as $item) {
				$menuItemOptions = array();
				$optionsPrice = 0;
				
				if (@$item->options) {
					foreach ($item->options as $menuItemOption) {
						$menuItemOptions[] = str_replace('{$orderItemOptionName}', $menuItemOption->menuItemOptionName, 
							str_replace('{$orderItemOptionPrice}', $menuItemOption->price, $menuItemOptionTemplate[1]));
						$optionsPrice += $menuItemOption->price;
					}
				}
				
				$menuItem = str_replace('{$orderItemIndex}', $item->quantity, 
					str_replace('{$orderItemName}', '<strong>' . $item->menuItemName . '</strong> (' . $item->menuItem->menuCategory->category . ')', 
					str_replace('{$orderItemInstructions}', ($item->instructions ? '<br />(' . $item->instructions . ')' : ''), 
					str_replace('{$orderItemPrice}', number_format($item->pricePerItem, 2), 
					str_replace('{$orderItemQuantity}', $item->quantity, 
					str_replace('{$orderItemTotal}', number_format(($item->pricePerItem + $optionsPrice) * $item->quantity, 2), $menuItemTemplate[1]))))));
				
				$menuItem = preg_replace('/\{\$orderItemOption\}([\s\S]+)\{\/\$orderItemOption\}/', ($menuItemOptions ? implode("\n", $menuItemOptions) : ''), $menuItem);
				$cartContent[] = $menuItem;
				
				$totalQuantity += $item->quantity;
			}
			
			if ($order->emailInvitation) {
				$menuItem = str_replace('{$orderItemIndex}', '1', 
					str_replace('{$orderItemName}', '<strong>' . DA_PROMO_CODE . '</strong>', 
					str_replace('{$orderItemInstructions}', '', 
					str_replace('{$orderItemPrice}', '-' . number_format($order->emailInvitation->value, 2), 
					str_replace('{$orderItemQuantity}', '1', 
					str_replace('{$orderItemTotal}', '-' . number_format($order->emailInvitation->value, 2), $menuItemTemplate[1]))))));
				
				$cartContent[] = preg_replace('/\{\$orderItemOption\}([\s\S]+)\{\/\$orderItemOption\}/', '', $menuItem);
				
				$totalQuantity++;
			}
			
			if ($order->promoCode) {
				$menuItem = str_replace('{$orderItemIndex}', '1', 
					str_replace('{$orderItemName}', '<strong>' . DA_PROMO_CODE . '</strong>', 
					str_replace('{$orderItemInstructions}', '', 
					str_replace('{$orderItemPrice}', '-' . number_format($order->promoCode->value, 2), 
					str_replace('{$orderItemQuantity}', '1', 
					str_replace('{$orderItemTotal}', '-' . number_format($order->promoCode->value, 2), $menuItemTemplate[1]))))));
				
				$cartContent[] = preg_replace('/\{\$orderItemOption\}([\s\S]+)\{\/\$orderItemOption\}/', '', $menuItem);
				
				$totalQuantity++;
			}
			
			$template = preg_replace('/(  )+/', '', preg_replace('/\{\$orderItem\}([\s\S]+)\{\/\$orderItem\}/', implode("\n", $cartContent), $template));
			
			if ($order->type=='D' && $order->deliveryCost) {
				// This should be included in the template, within a {$delivery} ... {/$delivery} pseudotag, but I'm too lazy for that now
				$template = str_replace('{$delivery}', '<tr>
								<td style="margin: 0; padding: 10px 0;"></td>
								<td colspan="3" style="margin: 0; padding: 10px 0;"><strong>' . P_ORDER_EMAIL_DELIVERY . '</strong></td>
								<td style="margin: 0; padding: 10px 10px 10px 0; text-align: right; vertical-align: top;"><strong>' . number_format($order->deliveryCost, 2) . '</strong></td>
							</tr>', $template);
				
			} else {
				$template = str_replace('{$delivery}', '', $template);
			}
			
			$template = str_replace('{$orderPayment}', ($order->payment=='cc' ? P_ORDER_EMAIL_PAYMENT_DONE : P_ORDER_EMAIL_PAYMENT_TBD), $template);
			$template = str_replace('{$orderTotalLabel}', P_ORDER_EMAIL_TOTAL, $template);
			$template = str_replace('{$orderQuantity}', $totalQuantity, $template);
			$template = str_replace('{$orderTotal}', number_format(($order->value + ($order->deliveryCost ? $order->deliveryCost : 0)), 2), $template);
			
			$template = str_replace('{$orderAddress}', P_ORDER_EMAIL_ADDRESS, $template);
			//$template = str_replace('{$orderLastNameLabel}', P_ORDER_EMAIL_LAST_NAME, $template);
			$template = str_replace('{$orderLastName}', $userLastName, $template);
			//$template = str_replace('{$orderFirstNameLabel}', P_ORDER_EMAIL_FIRST_NAME, $template);
			$template = str_replace('{$orderFirstName}', $userFirstName, $template);
			//$template = str_replace('{$orderCompanyLabel}', P_ORDER_EMAIL_COMPANY, $template);
			$template = str_replace('{$orderCompany}', (get_class($entity)=='user' ? '-' : $entity->companyName), $template);
			//$template = str_replace('{$orderStreetLabel}', P_ORDER_EMAIL_STREET, $template);
			$template = str_replace('{$orderStreet}', ($order->type=='D' ? $order->userAddress->address . ' ' . $order->userAddress->buildingNo : ''), $template);
			//$template = str_replace('{$orderZipLabel}', P_ORDER_EMAIL_ZIP, $template);
			$template = str_replace('{$orderZip}', ($order->type=='D' ? $order->userAddress->zipCode : ''), $template);
			//$template = str_replace('{$orderCityLabel}', P_ORDER_EMAIL_CITY, $template);
			$template = str_replace('{$orderCity}', ($order->type=='D' ? $order->userAddress->city : ''), $template);
			//$template = str_replace('{$orderPhoneLabel}', P_ORDER_EMAIL_PHONE, $template);
			$template = str_replace('{$orderPhone}', $entity->phone . (!empty($order->userAddress->phone) ? ', ' . $order->userAddress->phone : ''), $template);
			//$template = str_replace('{$orderFloorCodeLabel}', P_ORDER_EMAIL_FLOOR_CODE, $template);
			$template = str_replace('{$orderFloorCode}', ($order->type=='D' ? (!empty($order->userAddress->floorSuite) ? $order->userAddress->floorSuite : '-') . ' / ' . (!empty($order->userAddress->accessCode) ? $order->userAddress->accessCode : '-') : ''), $template);
			
			$template = str_replace('{$orderNotesLabel}', P_ORDER_EMAIL_NOTES, $template);
			$template = str_replace('{$orderNotes}', (!empty($order->notes) ? $order->notes : '-'), $template);
			
			if ($order->payment=='cash') {
				$template = str_replace('{$cashPayment}', '<tr>
				<td colspan="2" style="background: #ffc; border: #ccc 1px dashed; font-size: 16px; margin: 0; padding: 7px 10px; text-align: center;"><strong>' . P_ORDER_EMAIL_CASH_ON_DELIVERY . '</strong></td>
			</tr>', $template);
				
			} else {
				$template = str_replace('{$cashPayment}', '', $template);
			}
			
			return $template;
		}
		
		public static function new_insert($entity, $cart, $user_address, $restaurant, $transportation_type, $payment_type, $transaction_id = null, $order_value, $delivery_zone, $exchange_rate = null, $date_desired) {
			self::__constructStatic();
			
			$order = insertOrder($entity, $user_address, $restaurant, $transportation_type, $payment_type, $transaction_id, $order_value, ($delivery_zone ? $delivery_zone->deliveryFee : 0), $exchange_rate, ($user_address ? $user_address->instructions : null), $date_desired);
			if ($order) {
				
				$userFirstName = (get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName);
				$userLastName = (get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName);
				$userName = $userFirstName . ' ' . $userLastName;
				
				foreach ($cart as $item) {
					if (!empty($item['menu_item'])) {
						$menu_item = menuItem::getByID($item['menu_item']);
						
						if ($menu_item) {
							$order_item = orderItem::insert($order, $menu_item, $item['quantity'], $item['instructions']);
							
							foreach ($item['options'] as $option) {
								orderItemOption::insert($order_item, $option['id'], $option['name'], $option['price']);
							}
						}
						
					} else if (!empty($item['invitation']) || !empty($item['promo_code'])) {
						if (!empty($item['invitation'])) {
							$emailInvitation = emailInvitation::getByID($item['invitation']);
							
							if ($emailInvitation) {
								$emailInvitation->useCode($order);
							}
							
						} else {
							$promoCode = promoCode::getByID($item['promo_code']);
							
							if ($promoCode) {
								$promoCode->useCode($entity, $order); // This would only work for regular users for now - NOT FOR CORPORATE ACCOUNTS!
							}
						}
					}
				}
				
				$order_time = ($transportation_type=='D' ? strtotime('-' . $delivery_zone->estimatedTime . ' minutes', strtotime($date_desired)) : strtotime($date_desired));
				$email_title = date('H:i l, d M', $order_time) . 
					' ' . $restaurant->restaurantName . ' ' . P_ORDER_EMAIL_TITLE;
				
				// Send confirmation emails
				$order = order::getByID($order->idOrder); // Reload the $order object, to contain all the submitted items
				$template = self::getEmailTemplate($order, 'new_order');
				
				if ($payment_type!='invoice') {
					// This must be a regular order (and NOT an invoiced corporate one), so let's send a notification email:
					// to Venezvite...
					$message = str_replace('{$activationLink}', '', $template);
					
					$message = str_replace('{$orderDate}', 
						str_replace('{$datetime}', 
							formatDate(date('Y-m-d H:i:s', $order_time), true), 
							P_ORDER_EMAIL_ORDER_READY), 
						$message);
					$message = str_replace('{$supportPhone}', '<br />
							+41 (22) 575 29 07', $message);
					@sendMail(array(EMAIL_ADDRESS, ORDERS_EMAIL_ADDRESS), EMAIL_NAME, $email_title, $message, null, null, false);
					
					// to the restaurant...
					$url = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/restaurant-orders.html';
					$message = str_replace('{$activationLink}', '<br />' . 
						'<strong>' . P_ORDER_EMAIL_ACTIVATION_LINK . '</strong> &nbsp;<a href="' . $url . '" style="color: #508cca;">' . $url . '</a>', $template);
					
					$message = str_replace('{$orderDate}', 
						str_replace('{$datetime}', 
							formatDate(date('Y-m-d H:i:s', $order_time), true), 
							P_ORDER_EMAIL_ORDER_READY), 
						$message);
					$message = str_replace('{$supportPhone}', '<br />
						+41 (22) 575 29 07', $message);
					@sendMail($restaurant->email, $restaurant->restaurantName, $email_title, $message, null, null, false);
					
					if ($transportation_type=='D' && $restaurant->customDelivery=='Y') {
						// to the custom delivery company...
						$message = str_replace('{$activationLink}', '', $template);
						$message = str_replace('{$orderDate}', 
							str_replace('{$datetime}', 
								formatDate(date('Y-m-d H:i:s', strtotime('-' . $restaurant->preparationTime . ' minutes', strtotime($date_desired))), true), 
								P_ORDER_EMAIL_ORDER_READY), 
							$message);
						$message = str_replace('{$supportPhone}', '<br />
						+41 (22) 575 29 07', $message);
						
						$delivery_credentials = self::getDeliveryCredentials(strtotime($date_desired), ($delivery_zone ? ($delivery_zone->range <= 3) : true));
						@sendMail($delivery_credentials['notice_email'], $delivery_credentials['notice_name'], $email_title, $message, null, null, false);
					}
					
					// and to the user / corporate account...
					$message = str_replace('{$activationLink}', '', $template);
					$message = str_replace('{$orderDate}', 
						($transportation_type=='D' ? 
							str_replace('{$time}', date('H:i', strtotime($date_desired)), P_ORDER_EMAIL_DELIVERY_DELAY) : 
							str_replace('{$time}', date('H:i', strtotime($date_desired)), P_ORDER_EMAIL_TAKEAWAY_TIME)
						), $message);
					$message = str_replace('{$supportPhone}', '', $message);
					
					if (get_class($entity)=='user') {
						@sendMail($entity->email, $userName, $email_title, $message, null, null, false);
						
					} else {
						// Send a notification to all of the current corporate account's defined email addresses
						foreach($entity->emails as $email){
							@sendMail($email, $userName, $email_title, $message, null, null, false);
						}
					}
					
				} else {
					// This must be an invoiced corporate order, so let's add the pre-approval link for Venezvite only
					$url = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/pre-approve-corporate-order.html?check=' . $order->encodeID4PreApproval();
					$message = str_replace('{$activationLink}', '<br />' . 
						'<strong>Order pre-approve link</strong> &nbsp;<a href="' . $url . '" style="color: #508cca;">' . $url . '</a>', $template);
					
					$message = str_replace('{$orderDate}', 
						str_replace('{$datetime}', 
							formatDate(date('Y-m-d H:i:s', $order_time), true), 
							P_ORDER_EMAIL_ORDER_READY), 
						$message);
					$message = str_replace('{$supportPhone}', '<br />
							+41 (22) 575 29 07', $message);
					@sendMail(array(EMAIL_ADDRESS, ORDERS_EMAIL_ADDRESS), EMAIL_NAME, $email_title, $message, null, null, false);
				}
			}
			
			return $order;
		}
		
		public static function insert($entity, $cart, $userAddress, $restaurant, $type, $payment, $transactionID = null, $value, $deliveryCost, $exchangeRate = null, $notes, $dateDesired){
			self::__constructStatic();
			
			$userAddressObj = new userAddress;
			if($userAddress){
				foreach($userAddress as $property=>&$val){
					$userAddressObj->$property = &$val;
				}
				
				$userAddressObj->setDefault($entity);
			}
			
			$order = insertOrder($entity, $userAddressObj, $restaurant, $type, $payment, $transactionID, $value, $deliveryCost, $exchangeRate, $notes, $dateDesired);
			
			if ($order) {
				$userFirstName = (get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName);
				$userLastName = (get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName);
				$userName = $userFirstName . ' ' . $userLastName;
				
				
				//$spatchoCartContent = array();
				/*
				if ($payment=='cash') {
					$spatchoCartContent[] = 'CASH CASH CASH';
				}
				*/
				foreach ($cart as $item) {
					if (!empty($item['idMenuItem'])) {
						$menuItem = menuItem::getByID($item['idMenuItem']);
						
						if ($menuItem) {
							$orderItem = orderItem::insert($order, $menuItem, $item['quantity'], $item['instructions']);
							
							if (!empty($item['options'])) {
								foreach ($item['options'] as $menuItemOption) {
									orderItemOption::insert($orderItem, $menuItemOption['id'], $menuItemOption['name'], $menuItemOption['price']);
								}
							}
							/*
							$spatchoCartContent[] = $item['menuItemName'] . ' (' . $menuItem->menuCategory->category . '): ' . 
								number_format($item['price'], 2) . ' x' . $item['quantity'] . ' = ' . number_format(($item['price']) * $item['quantity'], 2);
							*/
						}
						
					} else if (!empty($item['idInvitation']) || !empty($item['idPromoCode'])) {
						
						if (!empty($item['idInvitation'])) {
							$emailInvitation = emailInvitation::getByID($item['idInvitation']);
							
							if ($emailInvitation) {
								$emailInvitation->useCode($order);
							}
							
						} else {
							$promoCode = promoCode::getByID($item['idPromoCode']);
							
							if ($promoCode) {
								$promoCode->useCode($entity, $order); // This would only work for regular users for now - NOT FOR CORPORATE ACCOUNTS!
							}
						}
					}
				}
				
				$order_time = ($type=='D' ? strtotime('-' . $restaurant->preparationTime . ' minutes', strtotime($dateDesired)) : strtotime($dateDesired));
				$email_title = date('H:i l, d M', $order_time) . 
					' ' . $restaurant->restaurantName . ' ' . P_ORDER_EMAIL_TITLE;
				
				// Send confirmation emails
				$order = order::getByID($order->idOrder); // Reload the $order object, to contain all the submitted items
				$template = self::getEmailTemplate($order, 'new_order');
				
				if ($payment!='invoice') {
					// This must be a regular order (and NOT an invoiced corporate one), so let's send a notification email:
					// to Venezvite...
					$message = str_replace('{$activationLink}', '', $template);
					
					$message = str_replace('{$orderDate}', 
						str_replace('{$datetime}', 
							formatDate(date('Y-m-d H:i:s', $order_time), true), 
							P_ORDER_EMAIL_ORDER_READY), 
						$message);
					$message = str_replace('{$supportPhone}', '<br />
							+41 (22) 575 29 07', $message);
					@sendMail(array(EMAIL_ADDRESS, ORDERS_EMAIL_ADDRESS), EMAIL_NAME, $email_title, $message, null, null, false);
					
					// to the restaurant...
					$url = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/restaurant-orders.html';
					$message = str_replace('{$activationLink}', '<br />' . 
						'<strong>' . P_ORDER_EMAIL_ACTIVATION_LINK . '</strong> &nbsp;<a href="' . $url . '" style="color: #508cca;">' . $url . '</a>', $template);
					
					$message = str_replace('{$orderDate}', 
						str_replace('{$datetime}', 
							formatDate(date('Y-m-d H:i:s', $order_time), true), 
							P_ORDER_EMAIL_ORDER_READY), 
						$message);
					$message = str_replace('{$supportPhone}', '<br />
						+41 (22) 575 29 07', $message);
					@sendMail($restaurant->email, $restaurant->restaurantName, $email_title, $message, null, null, false);
					
					if ($type=='D' && $restaurant->customDelivery=='Y') {
						/*
						// Try to send this via Spatcho to Team 1, too
						$amount2Collect = ($value + ($deliveryCost ? $deliveryCost : 0));
						self::sendCustomDeliveryConfirmation($order->idOrder, $spatchoCartContent, $notes, $amount2Collect, $restaurant, $entity, $userAddressObj, $dateDesired, $payment);
						*/
						// to the custom delivery company...
						$message = str_replace('{$activationLink}', '', $template);
						$message = str_replace('{$orderDate}', 
							str_replace('{$datetime}', 
								formatDate(date('Y-m-d H:i:s', strtotime('-' . $restaurant->preparationTime . ' minutes', strtotime($dateDesired))), true), 
								P_ORDER_EMAIL_ORDER_READY), 
							$message);
						$message = str_replace('{$supportPhone}', '<br />
						+41 (22) 575 29 07', $message);
						
						$delivery_credentials = self::getDeliveryCredentials(strtotime($dateDesired));
						@sendMail($delivery_credentials['notice_email'], $delivery_credentials['notice_name'], $email_title, $message, null, null, false);
					}
					
					// and to the user / corporate account...
					$message = str_replace('{$activationLink}', '', $template);
					$message = str_replace('{$orderDate}', 
						($type=='D' ? 
							str_replace('{$time}', date('H:i', strtotime($dateDesired)), P_ORDER_EMAIL_DELIVERY_DELAY) : 
							str_replace('{$time}', date('H:i', strtotime($dateDesired)), P_ORDER_EMAIL_TAKEAWAY_TIME)
						), $message);
					$message = str_replace('{$supportPhone}', '', $message);
					
					if (get_class($entity)=='user') {
						@sendMail($entity->email, $userName, $email_title, $message, null, null, false);
						
					} else {
						// Send a notification to all of the current corporate account's defined email addresses
						foreach($entity->emails as $email){
							@sendMail($email, $userName, $email_title, $message, null, null, false);
						}
					}
					
				} else {
					// This must be an invoiced corporate order, so let's add the pre-approval link for Venezvite only
					$url = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/pre-approve-corporate-order.html?check=' . $order->encodeID4PreApproval();
					$message = str_replace('{$activationLink}', '<br />' . 
						'<strong>Order pre-approve link</strong> &nbsp;<a href="' . $url . '" style="color: #508cca;">' . $url . '</a>', $template);
					
					$message = str_replace('{$orderDate}', 
						str_replace('{$datetime}', 
							formatDate(date('Y-m-d H:i:s', $order_time), true), 
							P_ORDER_EMAIL_ORDER_READY), 
						$message);
					$message = str_replace('{$supportPhone}', '<br />
							+41 (22) 575 29 07', $message);
					@sendMail(array(EMAIL_ADDRESS, ORDERS_EMAIL_ADDRESS), EMAIL_NAME, $email_title, $message, null, null, false);
				}
			}
			
			return $order;
		}
		
		public static function getDeliverySchedule($time) {
			return (((int)date('N', $time)>=1 && (int)date('N', $time)<=5 && 
					(int)date('Gi', $time)>=830 && (int)date('Gi', $time)<=1815) || 
					
					((int)date('N', $time)==6 && 
					(int)date('Gi', $time)>=930 && (int)date('Gi', $time)<=1615)/* || 
					
					((int)date('N', $time)==7 && 
					(int)date('Gi', $time)>=1230 && (int)date('Gi', $time)<=1415)*/);
		}
		
		private static function getDeliveryCredentials($time = null, $limited_delivery = true) {
			if (empty($time)) {
				$time = time();
			}
			
			if (
				self::getDeliverySchedule($time) && 
				$limited_delivery
			) {
				// Team 1
				return array(
					'spatcho_username'		=> '', 
					'spatcho_password'		=> '', 
					'spatcho_id'			=> '', 
					'spatcho_email_suffix'	=> '', 
					'notice_email'			=> '', 
					'notice_name'			=> ''
				);
			} else {
				// Venezvite
				return array(
					'spatcho_username'		=> '', 
					'spatcho_password'		=> '', 
					'spatcho_id'			=> '', 
					'spatcho_email_suffix'	=> '', 
					'notice_email'			=> '', 
					'notice_name'			=> ''
				);
			}
		}
		
		private static function sendCustomDeliveryConfirmation($idOrder, $spatchoCartContent, $notes, $amount2Collect, $restaurant, $entity, $userAddress, $dateDesired, $payment, $debug = false) {
			
			$delivery_zone = $restaurant->checkDeliveryLocation(array(
					'lat'	=> $userAddress->latitude, 
					'lng'	=> $userAddress->longitude
				));
			
			$delivery_credentials = self::getDeliveryCredentials(strtotime($dateDesired), ($delivery_zone->range <= 3));
			$url = 'https://www.spatcho.com/Zero/api/v1/customer/messages.json?username=' . $delivery_credentials['spatcho_username'] . '&password=' . $delivery_credentials['spatcho_password'];
			
			$entity_email = (get_class($entity)=='user' ? $entity->email : $entity->emails[0]);
			$entity_id = substr($entity_email, 0, 1) . strrev(get_class($entity)=='user' ? $entity->idUser : $entity->idCorporateAccount) . (get_class($entity)=='user' ? 'u' : 'c');
			
			$spatchoID = 'spatchoID' . $delivery_credentials['spatcho_id'];
			if (empty($restaurant->$spatchoID)) {
				// No proper Spatcho ID defined
				return false;
			}
			
			$fields = array(
				'date'			=> date('Y-m-d', strtotime('-' . $restaurant->preparationTime . ' minutes', strtotime($dateDesired))), 
				'external_id'	=> $idOrder, 
				'details'		=> implode("\r\n", $spatchoCartContent) . (!empty($notes) ? "\r\n" . $notes : ''), 
				'price'			=> (string)number_format($amount2Collect, 2), 
				'pick'			=> array(
					'customer_id'	=> $restaurant->$spatchoID, 
					'after'			=> (int)date('Hi', strtotime('-' . $restaurant->preparationTime . ' minutes', strtotime($dateDesired)))
				), 
				'drop'			=> array(
					'customer'	=> array(
						'first_name'	=> ($payment=='cash' ? 'CASH ' : '') . (get_class($entity)=='user' ? $entity->firstName : $entity->contactFirstName), 
						'last_name'		=> (get_class($entity)=='user' ? $entity->lastName : $entity->contactLastName), 
						'email'			=> $idOrder . '.' . $entity_id . $delivery_credentials['spatcho_email_suffix'] . '@venezvite.com', 
						'address'		=> array(
							'address1'		=> $userAddress->address, 
							'address2'		=> $userAddress->buildingNo, 
							'zip_code'		=> $userAddress->zipCode, 
							'city'			=> $userAddress->city, 
							//'country'		=> 'Switzerland', 
							'main_phone'	=> (!empty($userAddress->phone) ? $userAddress->phone : $entity->phone), 
							'floor_number'	=> (!empty($userAddress->floorSuite) ? $userAddress->floorSuite : ''), 
							'door_code'		=> (!empty($userAddress->accessCode) ? $userAddress->accessCode : '')
						)
					), 
					'before'	=> (int)date('Hi', strtotime($dateDesired))
				)
			);
			
			$ch = curl_init($url);
			
			//curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json'/*, 
					'Content-Length: ' . strlen(json_encode($fields))*/
				));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSLVERSION, 3);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			
			$result = curl_exec($ch);
			curl_close($ch);
			if ($debug) {
				//error_log(print_r($result, true));
			}
			if (!empty($result) && ($json = json_decode($result)) && !empty($json->status) && $json->status=='ok') {
				return true;
			} else {
				return false;
			}
		}
		
		/**
		 * Returns a "random" ID string used for pre-approval of corporate invoicing orders
		 * The "random" ID is composed of 10 random characters, followed by the actual ID of the order
		 * (so to extract the ID, a substraction of the last digits after the first 10 characters of this fake ID is needed)
		 * 
		 **/
		public function encodeID4PreApproval(){
			return substr(strrev(sha1(mt_rand())), 0, 10) . $this->idOrder;
		}
		
		public static function decodeID4PreApproval($id){
			return substr($id, 10);
		}
		
		public static function getByID($idOrder){
			self::__constructStatic();
			
			return getOrderByID($idOrder);
		}
		
		public static function getByPreApprovalID($id){
			self::__constructStatic();
			
			$idOrder = order::decodeID4PreApproval($id);
			
			return order::getByID($idOrder);
		}
		
		public function preApprove() {
			$userName = $this->corporateAccount->contactFirstName . ' ' . $this->corporateAccount->contactLastName;
			
			// Send confirmation emails
			$template = self::getEmailTemplate($this, 'new_order');
			
			// Let's send a notification email:
			// to Venezvite...
			$invoice = 'venezvite-invoice-' . date('YmdHi') . '.pdf';
			file_get_contents((!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/corporate-account-invoice.html?check=' . $this->corporateAccount->idCorporateAccount . '.' . substr(strrev(sha1(mt_rand())), 0, 5) . $this->idOrder . '&name=' . $invoice . '&save');
			
			$order_time = ($this->type=='D' ? strtotime('-' . $this->restaurant->preparationTime . ' minutes', strtotime($this->dateDesired)) : strtotime($this->dateDesired));
			$email_title = date('H:i l, d M', $order_time) . 
				' ' . $this->restaurant->restaurantName . ' ' . (defined('P_ORDER_EMAIL_TITLE') ? P_ORDER_EMAIL_TITLE : ORDER_EMAIL_TITLE);
			
			$message = str_replace('{$activationLink}', '', $template);
			$message = str_replace('{$orderDate}', 
				str_replace('{$datetime}', 
					formatDate(date('Y-m-d H:i:s', $order_time), true), 
					(defined('P_ORDER_EMAIL_ORDER_READY') ? P_ORDER_EMAIL_ORDER_READY : ORDER_EMAIL_ORDER_READY)), 
				$message);
			$message = str_replace('{$supportPhone}', '<br />
					+41 (22) 575 29 07', $message);
			@sendMail(array(EMAIL_ADDRESS, ORDERS_EMAIL_ADDRESS), EMAIL_NAME, $email_title, $message, array(REL . '/tmp_/' . $invoice), null, false);
			
			// to the restaurant...
			$url = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/restaurant-orders.html';
			$message = str_replace('{$activationLink}', '<br />' . 
				'<strong>' . (defined('P_ORDER_EMAIL_ACTIVATION_LINK') ? P_ORDER_EMAIL_ACTIVATION_LINK : ORDER_EMAIL_ACTIVATION_LINK) . '</strong> &nbsp;<a href="' . $url . '" style="color: #508cca;">' . $url . '</a>', $template);
			$message = str_replace('{$orderDate}', 
				str_replace('{$datetime}', 
					formatDate(date('Y-m-d H:i:s', $order_time), true), 
					(defined('P_ORDER_EMAIL_ORDER_READY') ? P_ORDER_EMAIL_ORDER_READY : ORDER_EMAIL_ORDER_READY)), 
				$message);
			$message = str_replace('{$supportPhone}', '<br />
				+41 (22) 575 29 07', $message);
			@sendMail($this->restaurant->email, $this->restaurant->restaurantName, $email_title, $message, null, null, false);
			
			if ($this->type=='D' && $this->restaurant->customDelivery=='Y') {
				/*
				// Try to send this via Spatcho to Team 1, too
				$amount2Collect = ($this->value + ($this->deliveryCost ? $this->deliveryCost : 0));
				self::sendCustomDeliveryConfirmation($this->idOrder, $spatchoCartContent, $this->notes, $amount2Collect, $this->restaurant, $this->corporateAccount, $this->userAddress, $this->dateDesired, $this->payment);
				*/
				// to the custom delivery company...
				$message = str_replace('{$activationLink}', '', $template);
				$message = str_replace('{$orderDate}', 
					str_replace('{$datetime}', 
						formatDate(date('Y-m-d H:i:s', strtotime('-' . $this->restaurant->preparationTime . ' minutes', strtotime($this->dateDesired))), true), 
						(defined('P_ORDER_EMAIL_ORDER_READY') ? P_ORDER_EMAIL_ORDER_READY : ORDER_EMAIL_ORDER_READY)), 
					$message);
				$message = str_replace('{$supportPhone}', '<br />
				+41 (22) 575 29 07', $message);
				
				$delivery_credentials = self::getDeliveryCredentials(strtotime($this->dateDesired));
				@sendMail($delivery_credentials['notice_email'], $delivery_credentials['notice_name'], $email_title, $message, null, null, false);
			}
			
			// and to the corporate account...
			$message = str_replace('{$activationLink}', '', $template);
			$message = str_replace('{$orderDate}', 
				($type=='D' ? 
					str_replace('{$time}', date('H:i', strtotime($this->dateDesired)), (defined('P_ORDER_EMAIL_DELIVERY_DELAY') ? P_ORDER_EMAIL_DELIVERY_DELAY : ORDER_EMAIL_DELIVERY_DELAY)) : 
					str_replace('{$time}', date('H:i', strtotime($this->dateDesired)), (defined('P_ORDER_EMAIL_TAKEAWAY_TIME') ? P_ORDER_EMAIL_TAKEAWAY_TIME : ORDER_EMAIL_TAKEAWAY_TIME))
				), $message);
			$message = str_replace('{$supportPhone}', '', $message);
			
			// Send a notification to all of the current corporate account's defined email addresses
			foreach ($this->corporateAccount->emails as $email) {
				@sendMail($email, $userName, $email_title, $message, array(REL . '/tmp_/' . $invoice), null, false);
			}
			@unlink(REL . '/tmp_/' . $invoice);
			
			
			preApproveOrder($this);
		}
		
		public function review() {
			reviewOrder($this);
		}
		
		public function accept($only_to_spatcho = false) {
			// Send confirmation emails
			$cart = array();
			$spatchoCartContent = array();
			
			if ($this->payment=='cash') {
				$spatchoCartContent[] = 'CASH CASH CASH';
			}
			
			foreach ($this->items as $orderItem) {
				$orderItemOptions = array();
				$optionsValue = 0;
				foreach ($orderItem->options as $orderItemOption) {
					$orderItemOptions[] = $orderItemOption->menuItemOptionName;
					$optionsValue += $orderItemOption->price;
				}
				
				$cart[] = $orderItem->quantity . 'x ' . $orderItem->menuItemName . (!empty($orderItemOptions) ? ' (+ ' . htmlspecialchars(implode(', ', $orderItemOptions)) . ')' : '') . ' ' . number_format(($orderItem->pricePerItem + $optionsValue) * $orderItem->quantity, 2) . ' ' . $this->restaurant->currency;
				
				$spatchoCartContent[] = $orderItem->menuItemName . ' (' . $orderItem->menuItem->menuCategory->category . '): ' . 
					number_format($orderItem->pricePerItem, 2) . ' x' . $orderItem->quantity . ' = ' . number_format($orderItem->pricePerItem * $orderItem->quantity, 2);
			}
			
			$userName = (!empty($this->user) ? $this->user->firstName . ' ' . $this->user->lastName : $this->corporateAccount->contactFirstName . ' ' . $this->corporateAccount->contactLastName);
			$message = $this->restaurant->restaurantName . ' just accepted the order from ' . $userName . (!empty($this->corporateAccount) ? ' (' . $this->corporateAccount->companyName . ')' : '') . '
Below you can find the order\'s content:

' . implode("\n", $cart) . '

Total: ' . $this->value . ' ' . $this->restaurant->currency . '
Desired date: ' . formatDate($this->dateDesired, true);
			
			if ($this->type=='D') {
				$message .= '
';
				if (!empty($this->corporateAccount)) {
					$message .= '
Company: ' . $this->corporateAccount->companyName;
				}
				
				$message .= '
Name: ' . $userName . '
Phone: ' . (!empty($this->userAddress->phone) ? $this->userAddress->phone : (!empty($this->user) ? $this->user->phone : $this->corporateAccount->phone)) . '
Address: ' . $this->userAddress->longFormat() . '
Notes: ' . (!empty($this->notes) ? $this->notes : '-');
			}
			
			if (!$only_to_spatcho) {
				// To Venezvite...
				@sendMail(array(EMAIL_ADDRESS, ORDERS_EMAIL_ADDRESS), EMAIL_NAME, $this->restaurant->restaurantName . ' accepted an order', $message);
			}
			
			if ($this->type=='D' && $this->restaurant->customDelivery=='Y') {
				// ... to the delivery company...
				$amount2Collect = ($this->value + ($this->deliveryCost ? $this->deliveryCost : 0));
				self::sendCustomDeliveryConfirmation($this->idOrder, $spatchoCartContent, $this->notes, $amount2Collect, $this->restaurant, (@$this->user ? $this->user : $this->corporateAccount), $this->userAddress, $this->dateDesired, $this->payment, $only_to_spatcho);
				/*
				$template = self::getEmailTemplate($this, 'new_order');
				$message = str_replace('{$activationLink}', '', $template);
				$message = str_replace('{$orderDate}', 
					str_replace('{$datetime}', 
						formatDate(date('Y-m-d H:i:s', strtotime('-' . $this->restaurant->preparationTime . ' minutes', strtotime($this->dateDesired))), true), 
						P_ORDER_EMAIL_ORDER_READY), 
					$message);
				$message = str_replace('{$supportPhone}', '<br />
				+41 (22) 575 29 07', $message);
				
				$delivery_credentials = self::getDeliveryCredentials();
				@sendMail($delivery_credentials['notice_email'], $delivery_credentials['notice_name'], $this->restaurant->restaurantName . ' accepted an order', $message, null, null, false);
				*/
			}
			
			if (!$only_to_spatcho) {
				// ... and to the user
				$message = str_replace('{$userName}', (!empty($this->user) ? $this->user->firstName : $this->corporateAccount->contactFirstName), 
					str_replace('{$restaurantName}', $this->restaurant->restaurantName, AO_EMAIL_BODY));
				@sendMail((!empty($this->user) ? $this->user->email : $this->corporateAccount->emails[0]), $userName, AO_EMAIL_TITLE, $message, null, 'simple');
				
				acceptOrder($this);
			}
		}
		
		public function decline($reason = null) {
			/*
			// Let's try to reimburse the payer's money
			$nvpStr = '&TRANSACTIONID=' . $this->transactionID . '&REFUNDTYPE=Full';
			$httpParsedResponseAr = PPHttpPost('RefundTransaction', $nvpStr);
			error_log(print_r($httpParsedResponseAr, true));
			if(!in_array(strtoupper($httpParsedResponseAr['ACK']), array('SUCCESS', 'SUCCESSWITHWARNING'))){
				error_log(print_r($httpParsedResponseAr, true));
				return false;
			}
			*/
			if (!empty($this->transactionID)) {
				require_once 'classes/Authorize.Net/AuthorizeNet.php';
				
				$transaction = new AuthorizeNetAIM;
				$response = $transaction->void($this->transactionID);
				
				if(empty($response->approved)){
					error_log('Payment refund error');
					error_log(print_r($response, true));
					//$response->response_reason_code
					//$response->response_code
					//$response->response_reason_text
				}
			}
			
			// Send confirmation emails
			$cart = array();
			foreach ($this->items as $orderItem) {
				$orderItemOptions = array();
				$optionsValue = 0;
				
				foreach ($orderItem->options as $orderItemOption) {
					$orderItemOptions[] = $orderItemOption->menuItemOptionName;
					$optionsValue += $orderItemOption->price;
				}
				
				$cart[] = $orderItem->quantity . 'x ' . $orderItem->menuItemName . (!empty($orderItemOptions) ? ' (+ ' . htmlspecialchars(implode(', ', $orderItemOptions)) . ')' : '') . ' ' . number_format(($orderItem->pricePerItem + $optionsValue) * $orderItem->quantity, 2) . ' ' . $this->restaurant->currency;
			}
			
			$userName = (!empty($this->user) ? $this->user->firstName . ' ' . $this->user->lastName : $this->corporateAccount->contactFirstName . ' ' . $this->corporateAccount->contactLastName);
			$message = $this->restaurant->restaurantName . ' just declined the order from ' . $userName . (!empty($this->corporateAccount) ? ' (' . $this->corporateAccount->companyName . ')' : '') . '
Below you can find the order\'s content:

' . implode("\n", $cart) . '

Total: ' . $this->value . ' ' . $this->restaurant->currency . '
Desired date: ' . formatDate($this->dateDesired, true);
			
			if ($this->type=='D') {
				$message .= '
';
				if(!empty($this->corporateAccount)){
					$message .= '
Company: ' . $this->corporateAccount->companyName;
				}
				
				$message .= '
Name: ' . $userName . '
Phone: ' . (!empty($this->userAddress->phone) ? $this->userAddress->phone : (!empty($this->user) ? $this->user->phone : $this->corporateAccount->phone)) . '
Address: ' . $this->userAddress->longFormat() . '
Notes: ' . (!empty($this->notes) ? $this->notes : '-');
			}
			
			if ($reason) {
				$message .= '

Reason for rejection: ' . $reason;
			}
			
			// To Venezvite...
			@sendMail(array(EMAIL_ADDRESS, ORDERS_EMAIL_ADDRESS), EMAIL_NAME, $this->restaurant->restaurantName . ' declined an order', $message);
			
			if ($this->type=='D' && $this->restaurant->customDelivery=='Y') {
				// ... to the delivery company...
				$delivery_zone = $this->restaurant->checkDeliveryLocation(array(
						'lat'	=> $this->userAddress->latitude, 
						'lng'	=> $this->userAddress->longitude
					));
				
				$delivery_credentials = self::getDeliveryCredentials(strtotime($this->dateDesired), ($delivery_zone->range <= 3));
				@sendMail($delivery_credentials['notice_email'], $delivery_credentials['notice_name'], $this->restaurant->restaurantName . ' declined an order', $message);
			}
			
			// ... and to the user
			$message = str_replace('{$userName}', (!empty($this->user) ? $this->user->firstName : $this->corporateAccount->contactFirstName), 
				str_replace('{$restaurantName}', $this->restaurant->restaurantName, 
				str_replace('{$reason}', ($reason ? "\n" . DO_EMAIL_REASON . $reason . "\n" : ''), DO_EMAIL_BODY)));
			@sendMail((!empty($this->user) ? $this->user->email : $this->corporateAccount->emails[0]), $userName, DO_EMAIL_TITLE, $message, null, 'simple');
			
			declineOrder($this, $reason);
			//return true;
		}
		
		/**
		 * Checks to see how many orders the submitted restaurant has for the submitted datetime, 
		 * and if that's below or above the restaurant's per-hour limit
		 * 
		 **/
		public static function checkOrderAcceptability($restaurant, $datetime) {
			self::__constructStatic();
			
			$orders = order::getForDatetime($restaurant, $datetime);
			return ($orders < $restaurant->maxOrdersPerHour);
		}
		
		public function setPaymentStatus($payment) {
			setOrderPaymentStatus($this, $payment);
		}
		
		public static function getUserLastOrder($userId, $restaurantId) {
			self::__constructStatic();
			
			return getUserLastOrder( $userId, $restaurantId );
		}
	}
