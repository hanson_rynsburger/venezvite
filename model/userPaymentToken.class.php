<?php
	class userPaymentToken {
		public $id;
		public $user;
		public $cardholder_name;
		public $token;
		public $card_type;
		public $expiration_date;
		public $date_added;
		public $default;
		
		function __construct() {
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'userPaymentToken.php';
		}
		
		public static function list_($entity) {
			self::__constructStatic();
			
			return listUserPaymentTokens($entity);
		}
		
		public static function insert($entity, $data) {
			self::__constructStatic();
			
			$userPaymentTokens = self::list_($entity);
			if (!count($userPaymentTokens)) {
				// Set the first payment token as default
				$data['default'] = 'Y';
			}
			
			return insertUserPaymentToken($entity, $data);
		}
		
		public static function getByID($id) {
			self::__constructStatic();
			
			return getUserPaymentTokenByID($id);
		}
		
		public function remove(){
			removeUserPaymentToken($this);
		}
		/*
		public function update($data) {
			updateUserPaymentToken($this, $data);
		}*/
	}
