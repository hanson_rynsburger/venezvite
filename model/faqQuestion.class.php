<?php
    class faqQuestion {
        public $idQuestion;
        public $faqCategory;
        public $question;
        public $answer;
        public $order;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'faqQuestion.php';
        }
        
        public static function list_($faqCategory = null){
            self::__constructStatic();
            
            return listFaqQuestions($faqCategory);
        }
    }
