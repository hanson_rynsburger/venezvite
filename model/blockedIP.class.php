<?php
    class blockedIP {
        public $idBlockedIP;
        public $ip;
        public $dateAdded;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'blockedIP.php';
        }
        
        /**
         * List all blocked IPs
         * 
         **/
        public static function list_() {
            self::__constructStatic();
            
            return listBlockedIPs();
        }
    }
