<?php
	class menuCategory {
		public $idMenuCategory;
		public $restaurant;
		public $category;
		public $order;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'menuCategory.php';
		}
		
		public static function list_($restaurant){
			self::__constructStatic();
			
			return listMenuCategories($restaurant);
		}
		
		public static function insert($restaurant, $category){
			self::__constructStatic();
			
			return insertMenuCategory($restaurant, $category);
		}
		
		public static function getByID($idMenuCategory){
			self::__constructStatic();
			
			return getMenuCategoryByID($idMenuCategory);
		}
		
		public function update($category){
			updateMenuCategory($this, $category);
		}
		
		public function delete(){
			deleteMenuCategory($this);
		}
		
		public function reorder($order){
			reorderMenuCategory($this, $order);
		}
	}
