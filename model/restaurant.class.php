<?php
	class restaurant {
		public $idRestaurant;
		public $restaurantName;
		public $contactFirstName;
		public $contactLastName;
		public $country;
		public $address;
		public $city;
		public $zipCode;
		public $latitude;
		public $longitude;
		public $website;
		public $phone;
		public $mobile;
		public $email;
		public $takeaway;
		public $delivery;
		public $customDelivery;
		public $catering;
		public $cashPayment;
		public $currency;
		public $restaurantDescription;
		public $menuFile;
		public $preferredLanguage;
		public $ip;
		public $bankAccountNo;
		public $iban;
		public $bankAccountName;
		public $businessAddress;
		public $bankName;
		public $bankAddress;
		public $swiftCode;
		public $ourFavorite;
		public $customURL;
		public $preparationTime;
		public $maxOrdersPerHour;
		public $commission;
		public $referrer;
		public $visible;
		public $spatchoID;
		public $spatchoID2;
		public $dateSubmitted;
		public $dateValidated;
		public $dateApproved;
		
		public $cuisineTypes = array();
		public $deliveryZones = array();
		public $timetable = array();
		public $deliveryHours = array();
		public $images = array();
		public $menus = array();
		public $priceLevel; // 1-5 values, corresponding to as many $ signs in the interface
		
		public $current_delivery_zone; // The delivery zone matching the current restaurant search
		public $max_range; // Highest delivery range per restaurant
		public $distance; // Distance from the restaurant to the searched GPS location (usually the potential client's zip code)
		public $opened; // True of false, depending if a restaurant is opened or closed at the time of a search
		public $rating;
		public $reviews; // Either integer (when searching for restaurants) or array (when getting a certain restaurant's details)
		public $userFavorite;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'restaurant.php';
		}
		
		public static function insert($restaurantName, $contactFirstName, $contactLastName, $address, $city, $zipCode, $latitude, $longitude, $website, $phone, $mobile, $delivery, $catering, $cashPayment, $restaurantDescription, $idPreferredLanguage, $ip, $username, $password, $cuisineTypes, $deliveryZones, $operationHours, $contactEmail = null, $customDelivery = 'N', $takeaway = 'Y', $country = null) {
			self::__constructStatic();
			
			$customURL = self::createCustomURL($restaurantName);
			if (!$country) {
				$country = country::getByID(1);
			}
			
			$restaurant = restaurantInsert($restaurantName, $contactFirstName, $contactLastName, $address, $city, $zipCode, $latitude, $longitude, $website, $phone, $mobile, ($contactEmail ? $contactEmail : $username), $delivery, $catering, $cashPayment, $restaurantDescription, $idPreferredLanguage, $ip, $customURL, $customDelivery, $takeaway, $country);
			
			if ($restaurant) {
				// The restaurant's main details have been successfully inserted in the database
				// Now let's insert its admin
				restaurantAdmin::insert($restaurant, $username, $password);
				
				// ... attach the selected cuisine types
				$cuisineTypeNames = array(); // used in the confirmation email
				foreach($cuisineTypes as $cuisineType){
					$restaurant->attachCuisineType($cuisineType);
					$cuisineTypeNames[] = $cuisineType->cuisineType;
				}
				
				// ... insert the submitted delivery zones
				$deliveryZoneNames = array(); // used in the confirmation email
				foreach ($deliveryZones as $deliveryZone) {
					$coordinates = deliveryZone::generatePath($latitude, $longitude, $deliveryZone[1]);
					
					$restaurant->deliveryZones[] = deliveryZone::insert($restaurant, $deliveryZone[1], $deliveryZone[0], $deliveryZone[2], $deliveryZone[3], json_encode($coordinates));
					$deliveryZoneNames[] = ' - ' . $deliveryZone[0] . ' CHF up to a ' . $deliveryZone[1] . 'km range (estimated reach ' . $deliveryZone[2] . ' minutes, minimum delivery ' . $deliveryZone[3] . ' CHF);';
				}
				
				// ... and opening hours
				$operationHoursNames = array(); // used in the confirmation email
				$timestamp = strtotime('next Monday');
				for ($i=1; $i<=7; $i++) {
					$operationHoursPerDay = array();
					if (!empty($operationHours[$i])) {
						$operationHoursPerDay = $restaurant->insertOperationHours($i, $operationHours[$i]);
					}
					
					$operationHoursNames[] = ' - ' . ucfirst(strftime('%A', $timestamp)) . ': ' . (!empty($operationHoursPerDay) ? implode(' / ', $operationHoursPerDay) : 'closed');
					$timestamp = strtotime('+1 day', $timestamp);
				}
				
				// Finally, let's create the default first menu entry
				$restaurant->menus[] = restaurantMenu::create($restaurant);
				
				// Let's send a message to the restaurant's admin, asking him/her to confirm his/her email address
				$restaurant->sendEmailConfirmationRequest();
				
				// Now we're sending the confirmation to the website's admin(s)
				$message = 'The following restaurant has subscribed in the website:
				
Restaurant\'s name: ' . $restaurantName . '
Contact\'s name: ' . $contactFirstName . ' ' . $contactLastName . '
Country: ' . $country->countryName . '
Address: ' . $address . ', ' . $zipCode . ', ' . $city . '
Website: ' . (!empty($website) ? $website : '-') . '
Phone: ' . $phone . '
Mobile: ' . (!empty($mobile) ? $mobile : '-') . '
Cuisine types: ' . implode(', ', $cuisineTypeNames) . '
Offers takeaway: ' . (empty($takeaway) || $takeaway=='N' ? 'No' : 'Yes') . '
Delivers: ' . (empty($delivery) || $delivery=='N' ? 'No' : 'Yes') . '
Custom / DP delivery: ' . (empty($customDelivery) || $customDelivery=='N' ? 'No' : 'Yes') . '
Offers catering: ' . (empty($catering) || $catering=='N' ? 'No' : 'Yes') . '

Restaurant\'s admin username: ' . $username . '

Delivery zones:
' . (count($deliveryZoneNames) ? implode("\n", $deliveryZoneNames) : '-') . '

Restaurant\'s description:
' . (!empty($restaurantDescription) ? $restaurantDescription : '-') . '

Hours of operation:
' . implode("\n", $operationHoursNames) . '

The subscription has been made from the ' . $_SERVER['REMOTE_ADDR'] . ' IP address on ' . date('F jS, Y H:i') . '.

To approve this restaurant\'s account, please access ' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/approve-restaurant.html?check=' . $restaurant->encodeID4Approval();
				@sendMail(EMAIL_ADDRESS, EMAIL_NAME, 'New restaurant subscription', $message);
			}
			
			return $restaurant;
		}
		
		private static function createCustomURL($restaurantName) {
			$restaurantName = html_entity_decode($restaurantName, ENT_COMPAT, 'UTF-8');
			
			setlocale(LC_CTYPE, 'en_US.UTF-8');
			$restaurantName = iconv('UTF-8', 'ASCII//TRANSLIT', $restaurantName);
			$restaurantName = strtolower(preg_replace('/[^a-zA-Z0-9]+/', '-', $restaurantName));
			
			// Let's check if this restaurant name is really unique
			if ($restaurant = getRestaurantByURL($restaurantName, false)) {
				// There is already a restaurant with the current custom URL
				// Let's add a random numeric suffix to it, and try again
				return self::createCustomURL($restaurantName . '-' . mt_rand(2, 100));
			} else {
				return $restaurantName;
			}
		}
		
		public function attachCuisineType($cuisineType) {
			attachRestaurantCuisineType($this, $cuisineType);
			$this->cuisineTypes[] = $cuisineType;
		}
		
		public function insertOperationHours($day, $intervals){
			$operationHoursPerDay = array();
			foreach ($intervals as $interval) {
				insertRestaurantOperationHours($this, $day, $interval['start'], $interval['end']);
				$operationHoursPerDay[] = $interval['start'] . '-' . $interval['end'];
			}
			
			#$this->timetable[] = $intervals;
			return $operationHoursPerDay;
		}
		
		public function sendEmailConfirmationRequest() {
			// Let's send a message to the restaurant's admin, asking him/her to confirm his/her email address
			$message = str_replace('{$uniqueHash}', $this->encodeID4Confirmation(), 
				str_replace('{$restaurantName}', $this->restaurantName, RR_EMAIL_CONFIRMATION_BODY));
			@sendMail($this->email . ',vvsupport@venezvite.com', $this->restaurantName, RR_EMAIL_CONFIRMATION_SUBJECT, $message, null, 'simple');
		}
		
		public static function getByID($idRestaurant){
			self::__constructStatic();
			
			return getRestaurantByID($idRestaurant);
		}
		
		public function updateMenuFile($file){
			updateRestaurantMenuFile($this, $file);
		}
		
		/**
		 * Public method for creating a unique hash
		 * used for confirming the restaurant admin's email address
		 * 
		 **/
		public function encodeID4Confirmation() {
			return md5(strrev(sha1($this->idRestaurant)));
		}
		
		public static function listUnconfirmed() {
			self::__constructStatic();
			
			return listUnconfirmedRestaurants();
		}
		
		public function confirm() {
			confirmRestaurant($this);
			
			@sendMail(EMAIL_ADDRESS, EMAIL_NAME, 'Restaurant confirmed', 'The "' . htmlspecialchars($this->restaurantName) . '" restaurant confirmed its email address.
Its account can now be approved by accessing ' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/approve-restaurant.html?check=' . $this->encodeID4Approval() . '.');
		}
		
		/**
		 * Public method for creating a unique hash
		 * used for approving the restaurant's account
		 * 
		 **/
		public function encodeID4Approval() {
			return sha1(strrev(md5($this->idRestaurant)));
		}
		
		public static function listUnapproved() {
			self::__constructStatic();
			
			return listUnapprovedRestaurants();
		}
		
		public function approve() {
			// Let's send the confirmation email to the restaurant's admin
			$message = str_replace('{$restaurantName}', $this->restaurantName, AR_EMAIL_CONFIRMATION_BODY);
			$mail = @sendMail($this->email, $this->restaurantName, AR_EMAIL_CONFIRMATION_SUBJECT, $message, null, 'simple');
			
			if ($mail) {
				approveRestaurant($this);
				return true;
		 	} else {
				return false;
			}
		}
		/*
		public function updateBankInfo($bankAccountNo, $iban, $bankAccountName, $businessAddress, $bankName, $bankAddress, $swiftCode, $cashPayment){
			$this->__constructStatic();
			updateRestaurantBankInfo($this, $bankAccountNo, $iban, $bankAccountName, $businessAddress, $bankName, $bankAddress, $swiftCode, $cashPayment);
			
			// Now let's update the reference variable, so we could re-serialize the authenticated admin's session
			$this->bankAccountNo = $bankAccountNo;
			$this->iban = $iban;
			$this->bankAccountName = $bankAccountName;
			$this->businessAddress = $businessAddress;
			$this->bankName = $bankName;
			$this->bankAddress = $bankAddress;
			$this->swiftCode = $swiftCode;
			$this->cashPayment = ($cashPayment ? 'Y' : 'N');
		}
		*/
		public function update($data) {
			$this->__constructStatic();
			updateRestaurant($this, $data);
			
			// Now let's update the reference variable, so we could re-serialize the authenticated admin's session
			foreach ($data as $key => $value) {
				$this->$key = $value;
			}
		}
		
		public static function search($location, $searchTerm, $dateTime, $coords, $user = false, $sort = false, $filters = false, $category = false, $rating = false, $type = false, $price_level = null){
			self::__constructStatic();
			
			$restaurants = searchRestaurants($location, $searchTerm, $dateTime, $coords, $user, $sort, $filters, $category, $rating, $type, $price_level);
			/*
			$destinations = array();
			foreach($restaurants as $restaurant){
				$destinations[] = $restaurant->latitude . ',' . $restaurant->longitude;
			}
			
			$origin = $coords['lat'] . ',' . $coords['lng'];
			$url = 'http://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $origin . '&destinations=' . implode('|', $destinations) . '&mode=driving&sensor=false';
			$distances = json_decode(@file_get_contents($url));
			
			if (@$distances && $distances->status=='OK') {
				for ($i=0; $i<count($distances->rows[0]->elements); $i++) {
					$element = $distances->rows[0]->elements[$i];
					
					if ($element->status=='OK') {
						$distance = $element->distance->value;
						
						if ($distance <= ($restaurants[$i]->max_range * 1000)) {
							$restaurants[$i]->distance = round($distance / 1000, 2);
							
						} else {
							unset($restaurants[$i]);
						}
						
					} else {
						if (@$restaurants[$i]) {
							// Get its straight-line distance?
							$distance = getLatLngDistance(array(
									'lat' => $restaurants[$i]->latitude, 
									'lng' => $restaurants[$i]->longitude
								), $coords);
							//usleep(330000);
							if ($distance <= $restaurants[$i]->max_range) {
								$restaurants[$i]->distance = $distance;
								
							} else {
								unset($restaurants[$i]);
							}
						}
					}
				}
				
			} else {
				*/
				/*// Apply the straight-line method
				$count = count($restaurants);
				
				for ($i=0; $i<$count; $i++) {
					$distance = getLatLngDistance(array(
							'lat' => $restaurants[$i]->latitude, 
							'lng' => $restaurants[$i]->longitude
						), $coords);
					if ($distance <= $restaurants[$i]->max_range) {
						$restaurants[$i]->distance = $distance;
						
					} else {
						unset($restaurants[$i]);
					}
				}*/
			//}
			
			foreach ($restaurants as $index => &$restaurant) {
				// We need the distance between the current restaurant and the searched address
				// for any order type, so that we could sort the resulting restaurants list
				// by distance.
			 	if (($delivery_zone = $restaurant->checkDeliveryLocation($coords))) {
					// This is the correct delivery zone for the current restaurant
					$restaurant->current_delivery_zone = $delivery_zone;
					$restaurant->distance = getLatLngDistance(array(
							'lat' => $restaurant->latitude, 
							'lng' => $restaurant->longitude
						), $coords);
					
				} else {
					unset($restaurants[$index]);
					continue;
				}
				
				// Check to see if the currently searched location is in any of 
				// the current restaurant's delivery zones. If it isn't, we'll skip it.
				if ($type=='D' && $restaurant->opened) {
					// Let's check again if the current restaurant is still available for placing
					// delivery orders, based on its just-determine delivery zone's estimated delivery time.
					foreach ($restaurant->deliveryHours as $delivery_hour) {
						if ($delivery_hour['dow']==date('N', $dateTime) && 
							strtotime(date('Y-m-d', $dateTime) . ' ' . $delivery_hour['hourStart'])<=$dateTime && 
							strtotime(date('Y-m-d', $dateTime) . ' ' . $delivery_hour['hourEnd'])>=$dateTime) {
							
							if ((strtotime(date('Y-m-d', $dateTime) . ' ' . $delivery_hour['hourEnd']) - $restaurant->current_delivery_zone->estimatedTime*60 - $restaurant->preparationTime*60)<time()) {
								$restaurant->opened = false;
							}
							break;
						}
					}
				}
			}
			
			if (!in_array($sort, array('alpha', 'alphabetically', 'price', 'delivery', 'delivery_minimum', 'rating', 'ratings', 'delivery_fee', 'open'))) {
				// Default to sort by distance
				usort($restaurants, array('restaurant', 'sortByDistance'));
			}
			$restaurants = array_values($restaurants); // Reset the restaurants array's indexes
			
			//ROUND(6371 * 2 * ASIN(SQRT(POW(SIN((' . mysql_real_escape_string($coords['lat']) . ' - ABS(R.`latitude`)) * PI() / 180 / 2), 2) + COS(' . mysql_real_escape_string($coords['lat']) . ' * PI() / 180) * COS(ABS(R.`latitude`) * PI() / 180) * POW(SIN((' . mysql_real_escape_string($coords['lng']) . ' - R.`longitude`) * PI() / 180 / 2), 2))), 2) AS distance, 
			/*
			AND 6371 * 2 * ASIN(SQRT(POW(SIN((' . mysql_real_escape_string($coords['lat']) . ' - ABS(R.`latitude`)) * PI() / 180 / 2), 2) + COS(' . mysql_real_escape_string($coords['lat']) . ' * PI() / 180) * COS(ABS(R.`latitude`) * PI() / 180) * POW(SIN((' . mysql_real_escape_string($coords['lng']) . ' - R.`longitude`) * PI() / 180 / 2), 2))) <= (SELECT MAX(RDZ.`range`)
				FROM `restaurants-delivery-zones` RDZ
				WHERE RDZ.`idRestaurant` = R.`idRestaurant`)
			*/
			//AND 6371 * 2 * ASIN(SQRT(POW(SIN((' . mysql_real_escape_string($coords['lat']) . ' - ABS(R.`latitude`)) * PI() / 180 / 2), 2) + COS(' . mysql_real_escape_string($coords['lat']) . ' * PI() / 180) * COS(ABS(R.`latitude`) * PI() / 180) * POW(SIN((' . mysql_real_escape_string($coords['lng']) . ' - R.`longitude`) * PI() / 180 / 2), 2))) <= 5
			
			return $restaurants;
		}
		
		/**
		 * Custom sorting-by-distance function
		 * for a search-resulting list of restaurants
		 * 
		 **/
		static function sortByDistance($a, $b){
			
			if ($a->distance == $b->distance) {
				return 0;
			}
			
			return ($a->distance > $b->distance) ? 1 : -1;
		}
		
		public static function getByURL($url){
			self::__constructStatic();
			
			return getRestaurantByURL($url);
		}
		
		/**
		 * List all restaurants for displaying in
		 * sitemap.xml
		 * 
		 **/
		public static function list_() {
			self::__constructStatic();
			
			return listRestaurants();
		}
		
		public function listTimetable(){
			$this->__constructStatic();
			return listRestaurantTimetable($this);
		}
		
		public function clearTimetable(){
			$this->__constructStatic();
			clearRestaurantTimetable($this);
		}
		
		public function insertDeliveryHours($day, $intervals){
			foreach($intervals as $interval){
				insertRestaurantDeliveryHours($this, $day, $interval['start'], $interval['end']);
			}
		}
		
		public function listDeliveryHours(){
			$this->__constructStatic();
			return listRestaurantDeliveryHours($this);
		}
		
		public function clearDeliveryHours(){
			$this->__constructStatic();
			clearRestaurantDeliveryHours($this);
		}
		
		public static function checkDuplicateEmail($email){
			self::__constructStatic();
			
			return checkRestaurantDuplicateEmail($email);
		}
		
		public function hasDiscounts(){
			if(!empty($this->dateApproved)){
				$menus = restaurantMenu::list_($this, true);
				foreach($menus[0]->menuItems as $menuItem){
					if($menuItem->discount>0){
						return true;
					}
				}
			}
			
			return false;
		}
		
		public function getTotalSales($startDate, $endDate) {
			$this->__constructStatic();
			
			return getTotalRestaurantSales($this, $startDate, $endDate);
		}
		
		public function getTotalEarnings($startDate, $endDate) {
			$this->__constructStatic();
			
			$totalSales = $this->getTotalSales($startDate, $endDate);
			return $totalSales * (1 - $this->commission / 100);
		}
		
		public function getCuisineTypesList() {
			$cuisineTypes = array();
			for ($i=0; $i<count($this->cuisineTypes); $i++) {
				$cuisineTypes[] = $this->cuisineTypes[$i]->cuisineType;
			}
			
			return $cuisineTypes;
		}
		
		public function getOrdersCount() {
			return getRestaurantOrdersCount($this);
		}
		
		public function getPriceLevel(/*$price_average*/) {
			$menus = restaurantMenu::list_($this, true, false, false, 'ROUND(RMI.`price` * ((100 - RMI.`discount`) / 100), 2) ASC');
			/*
			$prices = array();
			foreach ($menus[0]->menuItems as $menu_item) {
				$prices[] = $menu_item->getPrice();
			}
			*/
			if ($menus && ($average_menu_item = $menus[0]->menuItems[round(count($menus[0]->menuItems) / 2)])) {
				$price_average = $average_menu_item->getPrice(); // (array_sum($prices) / count($prices));
				
				switch ($price_average) {
					case $price_average<20:
						return 1;
					case $price_average<25:
						return 2;
					case $price_average<35:
						return 3;
					case $price_average<45:
						return 4;
					case $price_average>=45:
						return 5;
				}
				
			} else {
				return 1;
			}
		}
		
		public function checkDeliveryLocation($coordinates) {
			/*$origin = array(
				'lat' => $this->latitude, 
				'lng' => $this->longitude
			);
			$distance = getLatLngDistance($origin, $coordinates);
			
			foreach ($this->deliveryZones as $delivery_zone) {
				if ($distance <= $delivery_zone->range) {
					return true;
				}
			}*/
			$pointLocation = new pointLocation();
			$origin = coord2Point($coordinates['lat']) . ' ' . coord2Point($coordinates['lng']);
			foreach ($this->deliveryZones as $delivery_zone) {
				// Check to see the currently searched location is in the current delivery zone.
				// If it isn't, we'll skip this restaurant.
				$polygon_location = $pointLocation->pointInPolygon($origin, $delivery_zone->getStringPolygon());
				if ($polygon_location!='outside') {
					// This is the correct delivery zone for the current restaurant
					return $delivery_zone;
				}
			}
			
			return false;
		}
		
		/**
		 * Return a restaurant's $type operation's hours
		 * for $datetime (or now, if not specified)
		 * 
		 **/
		public function getDateHours($type, $datetime) {
			if (trim($datetime)=='') {
				$datetime = date('Y-m-d H:i');
			}
			$dow = date('N', strtotime($datetime));
			
			$hours = array();
			if ($type=='D') {
				// Get the current restaurant's delivery times
				// for $datetime
				foreach ($this->deliveryHours as $delivery_hour) {
					if ($delivery_hour['dow']==$dow) {
						$hours[] = substr($delivery_hour['hourStart'], 0, 5) . ' - ' . substr($delivery_hour['hourEnd'], 0, 5);
					}
				}
			} else {
				// Get the current restaurant's schedule times
				// for $datetime
				foreach ($this->timetable as $timetable) {
					if ($timetable['dow']==$dow) {
						$hours[] = substr($timetable['openingHour'], 0, 5) . ' - ' . substr($timetable['closingHour'], 0, 5);
					}
				}
			}
			
			return $hours;
		}
		
		/**
		 * Returns an array of dow's, start and end times, either for a restaurant's
		 * timetable or for its delivery hours, to be used for filling the shopping cart's
		 * date/time fields
		 * 
		 **/
		public function buildDatetimeList($datetimes) {
			$return = array();
			
			foreach ($datetimes as $datetime) {
				$return[] = array(
					'dow'	=> $datetime['dow'], 
					'start'	=> (!empty($datetime['openingHour']) ? $datetime['openingHour'] : $datetime['hourStart']), 
					'end'	=> (!empty($datetime['closingHour']) ? $datetime['closingHour'] : $datetime['hourEnd'])
				);
			}
			
			return $return;
		}
		
		/**
		 * Returns the next available time, past the submitted one
		 * (either for delivery, or take-away)
		 * 
		 **/
		public function getReopenTime($type, $datetime, $dow = null, $cycle = 0) {
			if ($cycle<7) {
				if (trim($datetime)=='') {
					$datetime = date('Y-m-d H:i');
				}
				$timestamp = strtotime($datetime);
				if (!$dow) {
					$dow = date('N', $timestamp);
				}
				
				if ($type=='D') {
					foreach ($this->deliveryHours as $delivery_hour) {
						$current_datetime = date('Y-m-d ' . $delivery_hour['hourStart'], strtotime('+' . $cycle . ' days', $timestamp));
						
						if ($delivery_hour['dow']==$dow && 
							$timestamp<strtotime($current_datetime)
						) {
							$reopening_time = prettyDateTime($current_datetime, true, ' ', 0, $this->preparationTime);
							break;
						}
					}
					
				} else {
					foreach ($this->timetable as $timetable) {
						$current_datetime = date('Y-m-d ' . $timetable['openingHour'], strtotime('+' . $cycle . ' days', $timestamp));
						
						if ($timetable['dow']==$dow && 
							$timestamp<strtotime($current_datetime)
						) {
							
							$reopening_time = prettyDateTime($current_datetime, true, ' ', 0, $this->preparationTime);
							break;
						}
					}
				}
				
				if (empty($reopening_time)) {
					return $this->getReopenTime($type, $datetime, (($dow + 1)>7 ? 1 : ($dow + 1)), ($cycle + 1));
				} else {
					return $reopening_time;
				}
				
			} else {
				return '-';
			}
		}
		
		public function getRestaurantCustomTimetable() {
			self::__constructStatic();
			return listRestaurantCustomTimetable( $this );
		}

		public function insertCustomTimetable( $newdate, $st, $ed ) {
			self::__constructStatic();
			return insertCustomTimetable( $this->idRestaurant, $newdate, $st, $ed );
		}
		
		public function deleteCustomTimetable( $cttid ) {
			self::__constructStatic();
			deleteCustomTimetable( $cttid );
			
			return true;
		}
		
		public function getReviewList( $offsetPage ) {
			self::__constructStatic();
			$temp = getReviewList( $this->idRestaurant );
			
			$paging = array();
			
			$paging["total"] = count( $temp );
			$paging["totalPage"] = ceil( $paging["total"] / REVIEW_PAGINATION );
			$paging["currentPage"] = $offsetPage;
			$paging["content"] = array();
			
			for ($i = ($offsetPage*REVIEW_PAGINATION-REVIEW_PAGINATION); $i<($offsetPage*REVIEW_PAGINATION); $i++ ) {
				if ( $i >= $paging["total"] ) break;
				
				array_push($paging["content"], $temp[$i]); 
			}
			
			return $paging;
		}
		
		public function isUserReviewed( $userId ) {
			self::__constructStatic();
			return getReviewList( $this->idRestaurant, $userId );
		}
		
		public function addReview( $userId, $rating, $isOnTime, $isCorrect, $isGood, $review ) {
			self::__constructStatic();
			return addReview( $userId, $this->idRestaurant, $rating, $isOnTime, $isCorrect, $isGood, $review );
		}
	}
