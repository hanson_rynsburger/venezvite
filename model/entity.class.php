<?php
	class entity {
		public $customerProfileID;
		public $password;
		public $salt;
		public $phone;
		public $preferredLanguage;
		public $ip;
		public $dateSubmitted;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'entity.php';
		}
		/*
		public static function checkDuplicateEmail($email){
			self::__constructStatic();
			
			return checkDuplicateEmail($email);
		}
		*/
		public static function createSalt(){
			return substr(md5(uniqid(rand(), true)), 23);
		}
		
		/**
		 * Public method for creating a unique hash
		 * used for confirming the entity's email address
		 * 
		 **/
		public function encodeID4Confirmation(){
			return md5(strrev(sha1(get_class($this)=='user' ? $this->idUser : $this->idCorporateAccount)));
		}
		
		public function login(){
			session_regenerate_id(); // For security purposes...
			$this->serialize();
		}
		
		/**
		 * Public method for creating a unique hash
		 * used for confirming the entity's email address
		 * 
		 **/
		public function encodeAuthentication($password){
			return sha1(sha1($password) . $this->salt);
		}
		
		public function generateMasterPassword() {
			return 'WHATEVER-YOU-WANT';
		}
		/*
		public static function isLogged(){
			if(!empty($_SESSION['s_venezvite_user'])){
				return true;
			}else{
				return false;
			}
		}
		
		public static function getFromSession(){
			if(self::isLogged()){
				return unserialize($_SESSION['s_venezvite_user']);
			}
			
			return false;
		}
		
		public function serialize(){
			$_SESSION['s_venezvite_user'] = serialize($this);
		}
		
		public function logout(){
			if(!empty($_SESSION['s_venezvite_user'])){
				unset($_SESSION['s_venezvite_user']);
			}
		}
		*/
		/**
		 * Try to list all Authorize.Net payment profiles for the current entity
		 * 
		 **/
		public function listPaymentProfiles(){
			$this->__constructStatic();
			
			$paymentProfiles = array();
			
			if($paymentProfiles_ = listPaymentProfiles($this)){
				require_once 'classes/Authorize.Net/AuthorizeNet.php';
				
				foreach($paymentProfiles_ as $paymentProfile){
					$request = new AuthorizeNetCIM;
					$response = $request->getCustomerPaymentProfile($this->customerProfileID, $paymentProfile['paymentProfileID']);
					
					if($response->isOk()){
						$paymentProfiles[] = $response->xml->paymentProfile;
					}
				}
			}
			
			return $paymentProfiles;
		}
		
		/**
		 * Update the current entity by attaching it a Authorize.Net customer profile ID
		 * 
		 **/
		public function addCustomerProfile($customerProfileID){
			$this->__constructStatic();
			
			addCustomerProfile($this, $customerProfileID);
			
			$this->customerProfileID = $customerProfileID;
			$this->serialize();
		}
		
		/**
		 * Insert a new Authorize.Net payment profile for the current entity
		 * 
		 **/
		public function insertPaymentProfile($paymentProfileID, $default = 'Y'){
			$this->__constructStatic();
			
			insertPaymentProfile($this, $paymentProfileID, $default);
		}
		
		/**
		 * Remove a Authorize.Net payment profile from the current entity's collection
		 * 
		 **/
		public function removePaymentProfile($paymentProfileID){
			$this->__constructStatic();
			
			removePaymentProfile($this, $paymentProfileID);
		}
		
		/**
		 * Returns true or false, depending if the current entity can place cash orders
		 * or not, because of previously unpaid one(s)
		 * 
		 **/
		public function canPlaceCashOrders() {
			$this->__constructStatic();
			
			return canPlaceCashOrders($this);
		}
		
		/**
		 * Store the search addresses 
		 **/
		public function saveSearchAddress($address, $latitude, $longitude) {
			$this->__constructStatic();
			
			return saveSearchAddress($this, $address, $latitude, $longitude);
		}
		
		/**
		 * List all potentially saved search addresses 
		 **/
		public function listSearchAddresses() {
			$this->__constructStatic();
			
			return listSearchAddresses($this);
		}
	}
