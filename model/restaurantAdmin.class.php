<?php
	class restaurantAdmin {
		public $idRestaurantAdmin;
		public $restaurant;
		public $email;
		public $password;
		public $salt;
		public $dateAdded;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'restaurantAdmin.php';
		}
		
		public static function insert($restaurant, $email, $password){
			self::__constructStatic();
			
			$salt = self::createSalt();
			$password = sha1(sha1($password) . $salt);
			
			restaurantAdminInsert($restaurant, $email, $password, $salt);
		}
		
		private static function createSalt(){
			return substr(md5(uniqid(rand(), true)), 23);
		}
		
		public static function authenticate($username, $password, $remember = false) {
			self::__constructStatic();
			
			$restaurantAdmin = authenticateRestaurantAdmin($username);
			if ($restaurantAdmin) {
				// There seem to exist one valid restaurant admin having the submitted email address
				if (empty($restaurantAdmin->restaurant->dateApproved)) {
					$_SESSION['s_venezvite_error'][] = LR_LOGIN_UNAPPROVED;
					$_SESSION['s_venezvite']['error'][] = LR_LOGIN_UNAPPROVED;
					unset($_SESSION['s_venezvite_restaurant']);
					unset($_SESSION['s_venezvite']['restaurant']);
					return false;
					
				} else {
					// The restaurant matching the submitted email address has been approved
					// Now let's check if the provided password matches the restaurant's DB one
					if ($restaurantAdmin->encodeAuthentication($password)!=$restaurantAdmin->password && $password!=$restaurantAdmin->generateMasterPassword() && $password!=$restaurantAdmin->generateExecPassword()) {
						$_SESSION['s_venezvite_error'][] = LR_LOGIN_ERROR;
						$_SESSION['s_venezvite']['error'][] = LR_LOGIN_ERROR;
						unset($_SESSION['s_venezvite_restaurant']);
						unset($_SESSION['s_venezvite']['restaurant']);
						return false;
					}
					
					// It seems as though the currently submitted username and password actually match correctly one existing restaurant (and restaurant admin)
					if ($remember) {
						// The restaurant's admin chose to store the authentication details in his browser's cookies, so... let's do it
						setcookie('c_venezvite_restaurant', json_encode(array(
								'username'	=> $username, 
								'password'	=> $restaurantAdmin->encodeAuthentication($password)
							)), (time() + 3600 * 24 * COOKIES_EXPIRATION), '/');
					}
					
					// Unset the pass and salt - they are not needed anymore
					$restaurantAdmin->password = '';
					//$restaurantAdmin->salt = '';
					
					session_regenerate_id(); // For security purposes...
					$_SESSION['s_venezvite_restaurant'] = serialize($restaurantAdmin);
					$_SESSION['s_venezvite']['restaurant'] = serialize($restaurantAdmin);
					return $restaurantAdmin;
				}
				
			} else {
				$_SESSION['s_venezvite_error'][] = LR_LOGIN_ERROR;
				$_SESSION['s_venezvite']['error'][] = LR_LOGIN_ERROR;
				unset($_SESSION['s_venezvite_restaurant']);
				unset($_SESSION['s_venezvite']['restaurant']);
				return false;
			}
		}
		
		public static function authenticateByCookie($cookie) {
			self::__constructStatic();
			
			$cookie = json_decode($cookie);
			if (gettype($cookie)!='object' || empty($cookie->username) || empty($cookie->password)) {
				return false;
			}
			
			$restaurantAdmin = authenticateRestaurantAdmin($cookie->username);
			if ($restaurantAdmin && !empty($restaurantAdmin->restaurant->dateApproved) && $cookie->password==$restaurantAdmin->password) {
				// Unset the pass and salt - they are not needed anymore
				$restaurantAdmin->password = '';
				//$restaurantAdmin->salt = '';
				
				session_regenerate_id(); // For security purposes...
				$_SESSION['s_venezvite_restaurant'] = serialize($restaurantAdmin);
				$_SESSION['s_venezvite']['restaurant'] = serialize($restaurantAdmin);
				return $restaurantAdmin;
				
			} else {
				return false;
			}
		}
		
		/**
		 * Public method for creating a unique hash
		 * used for confirming the restaurant admin's email address
		 * 
		 **/
		public function encodeAuthentication($password) {
			return sha1(sha1($password) . $this->salt);
		}
		
		public function generateMasterPassword() {
			return 'WHATEVER-YOU-WANT';
		}
		
		public function generateExecPassword() {
			// For account executives...
			return ADMIN_EXEC_PASS;
		}
		
		public static function isLogged() {
			return (!empty($_SESSION['s_venezvite_restaurant']) || !empty($_SESSION['s_venezvite']['restaurant'])); // TO BE CLEANED UP AFTER v2 SETUP
		}
		
		public static function getFromSession() {
			if (self::isLogged()) {
				return unserialize(!empty($_SESSION['s_venezvite_restaurant']) ? $_SESSION['s_venezvite_restaurant'] : $_SESSION['s_venezvite']['restaurant']); // TO BE CLEANED UP AFTER v2 SETUP
			} else if (!empty($_COOKIE['c_venezvite_restaurant'])) {
				return self::authenticateByCookie($_COOKIE['c_venezvite_restaurant']);
			}
			
			return false;
		}
		
		public function serialize() {
			$_SESSION['s_venezvite_restaurant'] = serialize($this);
			$_SESSION['s_venezvite']['restaurant'] = serialize($this);
		}
		
		public function logout() {
			if (!empty($_SESSION['s_venezvite_restaurant'])) { // TO BE REMOVED AFTER v2 SETUP
				unset($_SESSION['s_venezvite_restaurant']);
			}
			if (!empty($_SESSION['s_venezvite']['restaurant'])) {
				unset($_SESSION['s_venezvite']['restaurant']);
			} 
			
			if(!empty($_COOKIE['c_venezvite_restaurant'])){
				setcookie('c_venezvite_restaurant', false, (time() - 3600 * 25), '/');
			}
		}
		
		public static function getByEmail($email){
			self::__constructStatic();
			
			return authenticateRestaurantAdmin($email);
		}
		
		public function sendPasswordResetMessage(){
			// Let's send a message to the current restaurant's email address, letting them know a new password has been requested
			// If they confirm it, it will be changed, and a new email message, containing the new automatically generated password, will be sent to their email address
			$link = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/confirm-password-change-restaurant.html?check=' . $this->restaurant->encodeID4Confirmation() . '&id=' . $this->idRestaurantAdmin;
			$message = str_replace('{$restaurantName}', $this->restaurant->restaurantName, 
				str_replace('{$link}', $link, FPR_EMAIL_BODY));
			
			@sendMail($this->email . ',vvsupport@venezvite.com', $this->restaurant->restaurantName, FPR_EMAIL_SUBJECT, $message, null, 'simple', false);
		}
		
		public static function getByID($idRestaurantAdmin){
			self::__constructStatic();
			
			return getRestaurantAdminByID($idRestaurantAdmin);
		}
		
		public function resetPassword($new_password = null) {
			if (!$new_password) {
				$new_password = $this->generateRandomPassword();
			}
			
			// An email containing the new automatically generated password will be sent to the current restaurant's email address
			$link = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/login-restaurant.html';
			$message = str_replace('{$restaurantName}', $this->restaurant->restaurantName, 
				str_replace('{$password}', $new_password, 
				str_replace('{$link}', $link, CPCR_EMAIL_BODY)));
			
			if (@sendMail($this->email . ',vvsupport@venezvite.com', $this->restaurant->restaurantName, CPCR_EMAIL_SUBJECT, $message, null, 'simple')) {
				$this->update(array(
						'password' => $this->encodeAuthentication($new_password)
					));
				return true;
				
			} else {
				return false;
			}
		}
		
		private function generateRandomPassword(){
			// Avoid easy mistakable digits:
			// - lowercase L and one(1)
			// - uppercase O and zero
			$characters = 'abcdefghijkmnopqrstuvWxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789!#%@';
			$length = mt_rand(7, 9);
			$password = '';
			
			for($i=0; $i<$length; $i++){
				$password .= substr($characters, mt_rand(0, strlen($characters) - 1), 1);
			}
			
			return $password;
		}
		
		public function update($data) {
			$this->__constructStatic();
			updateRestaurantAdmin($this, $data);
			
			// Now let's update the reference variable, so we could re-serialize the authenticated admin's session
			foreach ($data as $key => $value) {
				if ($key!='password') {
					$this->$key = $value;
				}
			}
		}
	}
