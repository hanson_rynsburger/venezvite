<?php
    class menuItemOption {
        public $idMenuItemOption;
        public $restaurant;
        public $menuItemOption;
        public $price;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'menuItemOption.php';
        }
        
        public static function list_($restaurant){
            self::__constructStatic();
            
            return listMenuItemOptions($restaurant);
        }
        
        public static function insert($restaurant, $menuItemOption, $price){
            self::__constructStatic();
            
            return insertMenuItemOption($restaurant, $menuItemOption, $price);
        }
        
        public static function getByID($idMenuItemOption){
            self::__constructStatic();
            
            return getMenuItemOptionByID($idMenuItemOption);
        }
        
        public static function listByMenuItemCategory($menuItemCategory){
            self::__constructStatic();
            
            return listOptionsByMenuItemCategory($menuItemCategory);
        }
        
        public static function insertPerCategory($idMenuItemOption, $menuItemOptionCategory){
            self::__constructStatic();
            
            return insertMenuItemOptionPerCategory($idMenuItemOption, $menuItemOptionCategory);
        }
        
        public function update($data) {
            update_menu_item_option($this, $data);
        }
        
        public function delete(){
            delete_menu_item_option($this);
        }
    }
