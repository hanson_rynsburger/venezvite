<?php
    class promoCode {
        public $idPromoCode;
        public $user;
        public $promoCode;
        public $value;
        public $usageNo;
        public $minOrderValue;
        public $dateStart;
        public $dateEnd;
        
        public $usages;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic() {
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'promoCode.php';
        }
        
        public static function getByCode($promoCode, $user = null) {
            self::__constructStatic();
            
            return getPromoCodeByCode($promoCode, $user);
        }
        
        public static function getByID($idPromoCode) {
            self::__constructStatic();
            
            return getPromoCodeByID($idPromoCode);
        }
        
        public function useCode($user, $order) {
            usePromoCode($this, $user, $order);
        }
        
        public static function getByOrder($order) {
            self::__constructStatic();
            
            return getPromoCodeByOrder($order);
        }
    }
