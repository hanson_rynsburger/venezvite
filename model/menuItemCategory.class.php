<?php
	class menuItemCategory {
		public $idMenuItemCategory;
		public $menuItem;
		public $restaurant;
		public $menuItemCategory;
		public $selectables;
		public $selectablesRule;
		public $order;
		
		public $menuItemOptions = array();
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'menuItemCategory.php';
		}
		
		public static function list_($restaurant) {
			self::__constructStatic();
			
			return listMenuItemCategories($restaurant);
		}
		
		public static function list_new($restaurant, $load_options = false) {
			self::__constructStatic();
			
			return list_menu_item_categories($restaurant, $load_options);
		}
		
		public static function insert($menuItem, $menuItemCategory, $selectables, $selectablesRule){
			self::__constructStatic();
			
			return insertMenuItemCategory($menuItem, $menuItemCategory, $selectables, $selectablesRule);
		}
		
		public static function insert_new($data) {
			self::__constructStatic();
			
			return insert_menu_item_category($data);
		}
		
		public static function getByID($idMenuItemCategory){
			self::__constructStatic();
			
			return getMenuItemCategoryByID($idMenuItemCategory);
		}
		
		public static function get_by_id($id) {
			self::__constructStatic();
			
			return get_menu_item_category_by_id($id);
		}
		
		public static function listByMenuItem($menuItem, $options = true) {
			self::__constructStatic();
			
			return listCategoriesByMenuItem($menuItem, $options);
		}
		
		public function updateMenuItem($menuItem) {
			updateMenuItemCategoryMenuItem($this, $menuItem);
		}
		
		public function update($menuItem, $menuItemCategory, $selectables, $selectablesRule){
			updateMenuItemCategory($this, $menuItem, $menuItemCategory, $selectables, $selectablesRule);
		}
		
		public function update_new($data) {
			update_menu_item_category($this, $data);
		}
		
		public function deleteOptions(){
			deleteMenuItemCategoryOptions($this);
		}
		
		public function delete() {
			delete_menu_item_category($this);
		}
		
		public function reorder($order){
			reorderMenuItemCategory($this, $order);
		}
	}
