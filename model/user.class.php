<?php
	class user extends entity {
		public $idUser;
		public $idFacebook;
		public $gender;
		public $firstName;
		public $lastName;
		public $email;
		public $avatar;
		
		public $addresses = array();
		public $ratings = array();
		public $favorites = array();
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'user.php';
		}
		
		public static function checkDuplicateEmail($email){
			self::__constructStatic();
			
			return checkUserDuplicateEmail($email);
		}
		
		public static function insert($idFacebook, $gender = null, $firstName, $lastName, $email, $password, $referer = null, $phone = null, $newsletter = 'N', $avatar = null, $idPreferredLanguage, $ip){
			self::__constructStatic();
			
			$salt = self::createSalt();
			$cryptedPassword = sha1(sha1($password) . $salt);
			
			$user = userInsert($idFacebook, $gender, $firstName, $lastName, $email, $cryptedPassword, $salt, $referer, $phone, $newsletter, $avatar, $idPreferredLanguage, $ip);
			
			if ($user) {
				// Let's send a message to the user, asking him/her to confirm his/her email address
				$message = /*str_replace('{$uniqueHash}', $user->encodeID4Confirmation(), 
					*/str_replace('{$firstName}', $user->firstName, 
					str_replace('{$username}', $email, 
					str_replace('{$password}', $password, R_EMAIL_CONFIRMATION_BODY)));//);
				@sendMail($user->email, $user->firstName . ' ' . $user->lastName, R_EMAIL_CONFIRMATION_SUBJECT, $message, null, 'simple');
				
				// Now we're sending the confirmation to the website's admin(s)
				$message = 'The following user has subscribed in the website:

User\'s name: ' . $firstName . ' ' . $lastName . '
Username (email): ' . $email . '
Phone number: ' . (!empty($phone) ? $phone : '-') . '
Referer: ' . (!empty($referer) ? $referer : '-') . '
Newsletter subscriber: ' . str_replace('N', 'no', str_replace('Y', 'yes', $newsletter)) . '

The subscription has been made from the ' . $_SERVER['REMOTE_ADDR'] . ' IP address on ' . date('F jS, Y H:i') . '.';
				@sendMail(EMAIL_ADDRESS, EMAIL_NAME, 'New user subscription', $message);
				
				// Let's check if the current user's email is part of the list of users invited via email by other, already existing users
				// If so, let's send an email confirmation to the referral user, sending him the promotional code he/she earned for getting the current user in the website
				$emailInvitation = emailInvitation::getByEmail($email);
				if ($emailInvitation) {
					$promoCode = $emailInvitation->setCode();
					
					$message = 'Hello ' . $emailInvitation->user->firstName . '

' . $firstName . ' ' . $lastName . ' has subscribed successfully in Venezvite. Because of this, you receive a promotional code that allows you to have a discount on your next order on venezvite.com!

Your promotional code is: <strong>' . $promoCode . '</strong>

This code can only be used once, during the checkout process (you need to submit it in the "Promo Code" section, below the shopping cart).
Anytime a user to whom you recommended Venezvite via email registers with us, you will receive a new promo code. So don\'t hesitate and invite all your friends - you\'ll get cheaper food for weeks!';
					@sendMail($emailInvitation->user->email, $emailInvitation->user->firstName . ' ' . $emailInvitation->user->lastName, 'Your promotional code on Venezvite!', $message, null, 'simple');
				}
			}
			
			return $user;
		}
		
		public static function authenticate($username, $password, $remember = false) {
			self::__constructStatic();
			
			$user = authenticateUser($username);
			if ($user) {
				// There seem to exist one valid user having the submitted email address
				/*
				if(empty($user->dateValidated)){
					//$_SESSION['s_venezvite_error'][] = LOGIN_UNVERIFIED;
					unset($_SESSION['s_venezvite_user']);
					unset($_SESSION['s_venezvite']['user']);
					return false;
				}else{
					*/
					// The user account matching the submitted email address has been validated
					// Now let's check if the provided password matches the user's DB one
					if ($user->encodeAuthentication($password)!=$user->password/* && $password!=$user->generateMasterPassword()*/) {
						//$_SESSION['s_venezvite_error'][] = LOGIN_INCORRECT;
						unset($_SESSION['s_venezvite_user']);
						unset($_SESSION['s_venezvite']['user']);
						return false;
					}
					
					// It seems as though the currently submitted username and password actually match correctly one existing user account
					if ($remember) {
						// The user chose to store the authentication details in his browser's cookies, so... let's do it
						setcookie('c_venezvite_user', json_encode(array(
								'username'	=> $username, 
								'password'	=> $user->encodeAuthentication($password)
							)), (time() + 3600 * 24 * COOKIES_EXPIRATION), '/');
					}
					
					// Unset the pass and salt - they are not needed anymore
					$user->password = '';
					$user->salt = '';
					
					$user->login();
					return $user;
				//}
			} else {
				//$_SESSION['s_venezvite_error'][] = LOGIN_INCORRECT;
				unset($_SESSION['s_venezvite_user']);
				unset($_SESSION['s_venezvite']['user']);
				return false;
			}
		}
		
		public static function authenticateByCookie($cookie){
			self::__constructStatic();
			
			$cookie = json_decode($cookie);
			if (gettype($cookie)!='object' || empty($cookie->username) || empty($cookie->password)) {
				return false;
			}
			
			$user = authenticateUser($cookie->username);
			if ($user && $cookie->password==$user->password) {
				// Unset the pass and salt - they are not needed anymore
				$user->password = '';
				$user->salt = '';
				
				$user->login();
				return $user;
			} else {
				return false;
			}
		}
		
		public static function isLogged(){
			return (!empty($_SESSION['s_venezvite_user']) || !empty($_SESSION['s_venezvite']['user'])); // TO BE CLEANED UP AFTER v2 SETUP
		}
		
		public static function getFromSession(){
			if (self::isLogged()) {
				return unserialize(!empty($_SESSION['s_venezvite_user']) ? $_SESSION['s_venezvite_user'] : $_SESSION['s_venezvite']['user']); // TO BE CLEANED UP AFTER v2 SETUP
			} else if(!empty($_COOKIE['c_venezvite_user'])) {
				return self::authenticateByCookie($_COOKIE['c_venezvite_user']);
			}
			
			return false;
		}
		
		public function serialize(){
			$_SESSION['s_venezvite_user'] = serialize($this); // TO BE REMOVED AFTER v2 SETUP
			$_SESSION['s_venezvite']['user'] = serialize($this);
		}
		
		public function logout() {
			if (!empty($_SESSION['s_venezvite_user'])) { // TO BE REMOVED AFTER v2 SETUP
				unset($_SESSION['s_venezvite_user']);
			}
			if (!empty($_SESSION['s_venezvite']['user'])) {
				unset($_SESSION['s_venezvite']['user']);
			} 
			
			if (!empty($_COOKIE['c_venezvite_user'])) {
				setcookie('c_venezvite_user', false, (time() - 3600 * 25), '/');
			}
		}
		
		public static function getByEmail($email){
			self::__constructStatic();
			
			return authenticateUser($email);
		}
		
		public static function getByFB($fbID){
			self::__constructStatic();
			
			return getUserByFB($fbID);
		}
		
		public function updateFbID($fbID){
			updateUserFbID($this, $fbID);
			$this->idFacebook = $fbID;
			
			$this->login();
			echo json_encode($this);
			die();
		}
		
		public function sendPasswordResetMessage() {
			// Let's send a message to the current user's email address, letting them know a new password has been requested
			// If they confirm it, it will be changed, and a new email message, containing the new automatically generated password, will be sent to their email address
			$link = (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . $_SESSION['s_venezvite']['language']->languageAcronym . '/confirm-password-change.html?check=' . $this->encodeID4Confirmation() . '&id=' . $this->idUser;
			$message = str_replace('{$userName}', $this->firstName, 
				str_replace('{$link}', $link, FP_EMAIL_BODY));
			
			@sendMail($this->email . ',vvsupport@venezvite.com', $this->firstName . ' ' . $this->lastName, FP_EMAIL_SUBJECT, $message, null, 'simple', false);
		}
		
		public static function getByID($idUser){
			self::__constructStatic();
			
			return getUserByID($idUser);
		}
		
		public function resetPassword($new_password = null) {
			if (!$new_password) {
				$new_password = self::generateRandomPassword();
			}
			
			// An email containing the new automatically generated password will be sent to the current user's email address
			$message = str_replace('{$userName}', $this->firstName, 
				str_replace('{$password}', $new_password, CPC_EMAIL_BODY));
			
			if (@sendMail($this->email . ',vvsupport@venezvite.com', $this->firstName . ' ' . $this->lastName, CPC_EMAIL_SUBJECT, $message, null, 'simple')) {
				$this->updatePassword($new_password);
				return true;
				
			} else {
				return false;
			}
		}
		
		public static function generateRandomPassword(){
			// Avoid easy mistakable digits:
			// - lowercase L and one(1)
			// - uppercase O and zero
			$characters = 'abcdefghijkmnopqrstuvWxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789!#%@';
			$length = mt_rand(7, 9);
			$password = '';

			for($i=0; $i<$length; $i++){
				$password .= substr($characters, mt_rand(0, strlen($characters) - 1), 1);
			}

			return $password;
		}

		private function updatePassword($password){
			updateUserPassword($this, $this->encodeAuthentication($password));
		}

		public function listRatings(){
			return listRatedRestaurants($this);
		}

		public function addToRatings($restaurant, $rating){
			$this->__constructStatic();

			addRestaurantToRatings($this, $restaurant, $rating);

			$this->ratings[] = $restaurant;
			$this->serialize();
		}

		public function listFavorites(){
			return listFavoriteRestaurants($this);
		}

		public function addToFavorites($restaurant){
			$this->__constructStatic();

			addRestaurantToFavorites($this, $restaurant);

			$this->favorites[] = $restaurant;
			$this->serialize();
		}

		public function removeFavorites($restaurant){
			$this->__constructStatic();

			removeRestaurantFromFavorites($this, $restaurant);

			$newFavs = array();
			foreach( $this->favorites as $favR ) {
				if ( $favR->idRestaurant != $restaurant->idRestaurant ) {
					$newFavs[] = $restaurant;
				}
			}

			$this->favorites= $newFavs;
			$this->serialize();
		}

		public function isRestaurantFavorited( $restaurant ) {
			$this->__constructStatic();
			return isRestaurantFavorited($this, $restaurant);
		}

		public function update($firstName, $lastName, $email, $password, $phone) {
			$this->__constructStatic();

			$salt = '';
			$cryptedPassword = '';
			if (!empty($password)) {
				$salt = self::createSalt();
				$cryptedPassword = sha1(sha1($password) . $salt);
			}

			userUpdate($this, $firstName, $lastName, $email, $cryptedPassword, $salt, $phone);

			$this->firstName = $firstName;
			$this->lastName = $lastName;
			$this->email = $email;
			$this->phone = $phone;
			$this->serialize();
		}
	}
