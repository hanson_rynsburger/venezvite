<?php
	class recommendedRestaurant {
		public $idRestaurant;
		public $zipCode;
		public $email;
		public $restaurant;
		public $ip;
		public $dateAdded;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'recommendedRestaurant.php';
		}
		
		public static function insert($zipCode, $email, $restaurant, $ip){
			self::__constructStatic();
			
			recommendedRestaurantInsert($zipCode, $email, $restaurant, $ip);

			$message = 'The following details have been submitted as a new restaurant recommendation:

Restaurant\'s postal code / address: ' . $zipCode . '
Sender\'s email address / name: ' . $email . '
Restaurant\'s name: ' . $restaurant . '

The recommandation has been made from the ' . $ip . ' IP address on ' . date('F jS, Y H:i') . '.';
			
			if ( EMAIL_ADDRESS && EMAIL_NAME )
				@sendMail(EMAIL_ADDRESS, EMAIL_NAME, 'New restaurant recommendation', $message);
		}
	}
