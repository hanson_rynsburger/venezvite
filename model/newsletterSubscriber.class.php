<?php
    class newsletterSubscriber {
        public $idNewsletterSubscriber;
        public $email;
        public $ip;
        public $dateAdded;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'newsletterSubscriber.php';
        }
        
        public static function search($email){
            self::__constructStatic();
            
            return newsletterSubscriberSearch($email);
        }
        
        public static function insert($email, $ip){
            self::__constructStatic();
            
            newsletterSubscriberInsert($email, $ip);
            
            $message = 'The following email address has been subscribed to our mailing list: ' . $email . '
The subscription has been made from the ' . $ip . ' IP address on ' . date('F jS, Y H:i') . '.';
            @sendMail(EMAIL_ADDRESS, EMAIL_NAME, 'New mailing list subscriber', $message);
        }
    }
