<?php
    class language {
        public $idLanguage;
        public $languageName;
        public $languageAcronym;
        public $localeCode;
        public $default;
        public $visible;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'language.php';
        }
        
        public static function list_($all = false){
            self::__constructStatic();
            
            return listLanguages($all);
        }
        
        public static function getDefault(){
            self::__constructStatic();
            
            return getDefaultLanguage();
        }
        
        public static function getByAcronym($acronym){
            self::__constructStatic();
            
            return getLanguageByAcronym($acronym);
        }
        
        public static function getByCountryCode($code){
            self::__constructStatic();
            
            return getLanguageByCountryCode($code);
        }
    }
