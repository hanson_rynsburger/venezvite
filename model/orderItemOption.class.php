<?php
    class orderItemOption {
        public $idOrderItemOption;
        public $orderItem;
        public $menuItemOption;
        public $menuItemOptionName;
        public $price;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'orderItemOption.php';
        }
        
        public static function list_($orderItem){
            self::__constructStatic();
            
            return listOrderItemOptions($orderItem);
        }
        
        public static function insert($orderItem, $idMenuItemOption, $menuItemOption, $price){
            self::__constructStatic();
            
            insertOrderItemOption($orderItem, $idMenuItemOption, $menuItemOption, $price);
        }
    }
