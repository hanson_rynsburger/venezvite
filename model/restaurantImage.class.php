<?php
    class restaurantImage {
        public $idImage;
        public $restaurant;
        public $file;
        public $order;
        public $dateAdded;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'restaurantImage.php';
        }
        
        public static function insert($restaurant, $file){
            self::__constructStatic();
            
            return insertRestaurantImage($restaurant, $file);
        }
        
        public static function insertSpecific($restaurant, $file, $order) {
        	self::__constructStatic();
        
        	return insertRestaurantImage2($restaurant, $file, $order);
        }
        
        public static function list_($restaurant){
            self::__constructStatic();
            
            return listRestaurantImages($restaurant);
        }
        
        public static function getByID($idImage){
            self::__constructStatic();
            
            return getRestaurantImageByID($idImage);
        }
        
        public function delete(){
            deleteRestaurantImage($this);

            @unlink(REL . VERSION_REL_PATH . 'i/restaurants/' . $this->file);
            @unlink(REL . VERSION_REL_PATH . 'i/restaurants/tn_' . $this->file);
        }
        
        public function reorder($order){
            reorderRestaurantImage($this, $order);
        }
    }
