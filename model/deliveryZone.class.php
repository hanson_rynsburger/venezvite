<?php
	class deliveryZone {
		public $idDeliveryZone;
		public $restaurant;
		public $range;
		public $coordinates;
		public $deliveryFee;
		public $minimumDelivery;
		public $estimatedTime;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'deliveryZone.php';
		}
		
		public static function getByID($idDeliveryZone){
			self::__constructStatic();
			
			return getDeliveryZoneByID($idDeliveryZone);
		}
		
		public static function insert($restaurant, $range, $deliveryFee, $estimatedTime, $minimumDelivery, $coordinates = null) {
			self::__constructStatic();
			
			return insertDeliveryZone($restaurant, $range, $deliveryFee, $estimatedTime, $minimumDelivery, $coordinates);
		}
		
		public static function list_($restaurant){
			self::__constructStatic();
			
			return listDeliveryZones($restaurant);
		}
		
		public static function getClosest($restaurant, $coords) {
			self::__constructStatic();
			
			$deliveryZones = self::list_($restaurant);
			
			if( !$coords) {
				// There are no coordinates, so let's return the fartherst zone
				return $deliveryZones[0];
				
			} else {
				$distance = getLatLngDistance(array(
						'lat'	=> $restaurant->latitude, 
						'lng'	=> $restaurant->longitude
					), $coords);
				
				foreach($deliveryZones as $deliveryZone){
					if($deliveryZone->range>=$distance){
						return $deliveryZone;
						break;
					}
				}
				
				// It seems the currently search coordinates are outside the supported delivery range, so let's return the fartherst zone
				return $deliveryZones[0];
			}
		}
		
		public function update($data) {
			$this->__constructStatic();
			updateDeliveryZone($this, $data);
		}
		
		public function remove() {
			removeDeliveryZone($this);
		}
		
		
		public function getStringPolygon() {
			$string_path = array();
			$coordinates = json_decode($this->coordinates);
			
			if ($coordinates) {
				foreach ($coordinates as $coordinate) {
					$string_path[] = coord2Point($coordinate[0]) . ' ' . coord2Point($coordinate[1]);
				}
			}
			
			return $string_path;
		}
		
		public static function generatePath($latitude, $longitude, $radius) {
			$d2r = pi() / 180; // degrees to radians
			$r2d = 180 / pi(); // radians to degrees
			$earths_radius = 6371; // km
			
			$points = 24;
			
			// find the raidus in lat/lon
			$rlat = ($radius / $earths_radius) * $r2d;
			$rlng = $rlat / cos($latitude * $d2r);
			
			$path = array();
			for ($i=0; $i<=$points; $i++) { // one extra here makes sure we connect the
				$theta = pi() * ($i / ($points / 2));
				$ex = $longitude + ($rlng * cos($theta)); // center a + radius x * cos(theta)
				$ey = $latitude + ($rlat * sin($theta)); // center b + radius y * sin(theta)
				$path[] = array(round($ey, 5), round($ex, 5));
			}
			
			return $path;
		}
	}
