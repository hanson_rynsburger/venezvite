<?php
    class faqCategory {
        public $idCategory;
        public $language;
        public $category;
        public $order;
        
        public $questions = array();
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'faqCategory.php';
        }
        
        public static function list_($idLanguage, $listQuestions = false){
            self::__constructStatic();
            
            return listFaqCategories($idLanguage, $listQuestions);
        }
    }
