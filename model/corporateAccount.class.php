<?php
	class corporateAccount extends entity {
		public $idCorporateAccount;
		public $contactFirstName;
		public $contactLastName;
		public $jobTitle;
		public $department;
		public $address;
		public $city;
		public $zipCode;
		public $latitude;
		public $longitude;
		public $companyName;
		public $mobile;
		public $employeesNo;
		public $username;
		public $dateValidated;
		public $dateApproved;
		
		public $emails = array();
		
		function __construct() {
			$this->__constructStatic();
		}
		
		private static function __constructStatic() {
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'corporateAccount.php';
		}
		
		public static function insert($contactFirstName, $contactLastName, $jobTitle, $department, $address, $city, $zipCode, $latitude, $longitude, $companyName, $phone, $mobile, $employeesNo, $username, $password, $emails, $idPreferredLanguage, $ip) {
			self::__constructStatic();
			
			$salt = self::createSalt();
			$password = sha1(sha1($password) . $salt);
			
			$corporateAccount = corporateAccountInsert($contactFirstName, $contactLastName, $jobTitle, $department, $address, $city, $zipCode, $latitude, $longitude, $companyName, $phone, $mobile, $employeesNo, $username, $password, $salt, $emails, $idPreferredLanguage, $ip);
			
			if ($corporateAccount) {
				// The corporate account's main details have been successfully inserted in the database
				// Now send a message to the corporate account's contact person, asking him/her to confirm his/her email address
				$corporateAccount->sendEmailConfirmationRequest();
				
				// Now we're sending the confirmation to the website's admin(s)
				$message = 'The following corporate account has subscribed in the website:

Contact\'s name: ' . $contactFirstName . ' ' . $contactLastName . '
Job title: ' . $jobTitle . '
Department: ' . (!empty($department) ? $department : '-') . '
Address: ' . $address . ', ' . $zipCode . ', ' . $city . '
Company\'s name: ' . $companyName . '
Phone: ' . $phone . '
Mobile: ' . (!empty($mobile) ? $mobile : '-') . '

Corporate account\'s username: ' . $username . '
Attached email addresses: ' . implode(', ', $emails) . '

The subscription has been made from the ' . $_SERVER['REMOTE_ADDR'] . ' IP address on ' . date('F jS, Y H:i') . '.

To approve this corporate account, please access ' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/approve-corporate-account.html?check=' . $corporateAccount->encodeID4Approval();
				@sendMail(EMAIL_ADDRESS, EMAIL_NAME, 'New corporate account subscription', $message);
			}
			
			return $corporateAccount;
		}
		
		public function sendEmailConfirmationRequest() {
			// Let's send a message to the corporate account's admin, asking him/her to confirm his/her email address
			$message = str_replace('{$uniqueHash}', $this->encodeID4Confirmation(), 
				str_replace('{$companyName}', $this->companyName, RC_EMAIL_CONFIRMATION_BODY));
			@sendMail($this->emails[0], $this->companyName, RC_EMAIL_CONFIRMATION_SUBJECT, $message, null, 'simple');
		}
		/*
		public static function getByID($idRestaurant) {
			self::__constructStatic();
			
			return getRestaurantByID($idRestaurant);
		}
		*/
		public static function listUnconfirmed() {
			self::__constructStatic();
			
			return listUnconfirmedCorporateAccounts();
		}
		
		public function confirm() {
			confirmCorporateAccount($this);
			
			@sendMail(EMAIL_ADDRESS, EMAIL_NAME, 'Corporate account confirmed', 'The "' . htmlspecialchars($this->companyName) . '" company confirmed its email address.
Its account can now be approved by accessing ' . (!strstr(ROOT, 'http:') ? 'http:' : '') . ROOT . @$_SESSION['s_venezvite']['language']->languageAcronym . '/approve-corporate-account.html?check=' . $this->encodeID4Approval() . '.');
		}
		
		/**
		 * Public method for creating a unique hash
		 * used for approving the corporate account
		 * 
		 **/
		public function encodeID4Approval() {
			return sha1(strrev(md5($this->idCorporateAccount)));
		}
		
		public static function listUnapproved() {
			self::__constructStatic();
			
			return listUnapprovedCorporateAccounts();
		}
		
		public function approve() {
			// Let's send the confirmation email to the corporate account's admin
			$message = str_replace('{$companyName}', $this->companyName, ACA_EMAIL_CONFIRMATION_BODY);
			
			if (@sendMail($this->emails[0], $this->companyName, ACA_EMAIL_CONFIRMATION_SUBJECT, $message, null, 'simple')) {
				approveCorporateAccount($this);
				return true;
			}
			
			return false;
		}
		/*
		public static function list_() {
			self::__constructStatic();
			
			return listRestaurants();
		}
		*/
		public static function checkDuplicateUsername($username) {
			self::__constructStatic();
			
			return checkCorporateAccountDuplicateUsername($username);
		}
		
		public static function isLogged() {
			return (!empty($_SESSION['s_venezvite_corporate']) || !empty($_SESSION['s_venezvite']['corporate'])); // TO BE CLEANED UP AFTER v2 SETUP
		}
		
		public static function getFromSession() {
			if (self::isLogged()) {
				return unserialize(!empty($_SESSION['s_venezvite_corporate']) ? $_SESSION['s_venezvite_corporate'] : $_SESSION['s_venezvite']['corporate']); // TO BE CLEANED UP AFTER v2 SETUP
				
			} elseif (!empty($_COOKIE['c_venezvite_corporate'])) {
				return self::authenticateByCookie($_COOKIE['c_venezvite_corporate']);
			}
			
			return false;
		}
		
		public static function authenticate($username, $password, $remember) {
			self::__constructStatic();
			
			$corporateAccount = authenticateCorporateAccount($username);
			if ($corporateAccount) {
				// There seem to exist one valid corporate account having the submitted email address
				if (empty($corporateAccount->dateValidated)) {
					//$_SESSION['s_venezvite']['error'][] = LOGIN_UNVERIFIED;
					unset($_SESSION['s_venezvite_corporate']);
					unset($_SESSION['s_venezvite']['corporate']);
					return false;
					
				} else {
					// The corporate account matching the submitted email address has been validated
					// Now let's check if the provided password matches the corporate account's DB one
					if ($corporateAccount->encodeAuthentication($password) != $corporateAccount->password) {
						//$_SESSION['s_venezvite']['error'][] = LOGIN_INCORRECT;
						unset($_SESSION['s_venezvite_corporate']);
						unset($_SESSION['s_venezvite']['corporate']);
						return false;
					}
					
					// It seems as though the currently submitted username and password actually match correctly one existing corporate account
					if ($remember) {
						// The corporate account chose to store the authentication details in his browser's cookies, so... let's do it
						setcookie('c_venezvite_corporate', json_encode(array(
								'username'	=> $username, 
								'password'	=> $corporateAccount->encodeAuthentication($password)
							)), (time() + 3600 * 24 * COOKIES_EXPIRATION), '/');
					}
					
					// Unset the pass and salt - they are not needed anymore
					$corporateAccount->password = '';
					$corporateAccount->salt = '';
					
					$corporateAccount->login();
					return $corporateAccount;
				}
				
			} else {
				//$_SESSION['s_venezvite']['error'][] = LOGIN_INCORRECT;
				unset($_SESSION['s_venezvite_corporate']);
				unset($_SESSION['s_venezvite']['corporate']);
				return false;
			}
		}
		
		public static function authenticateByCookie($cookie) {
			self::__constructStatic();
			
			$cookie = json_decode($cookie);
			if (gettype($cookie)!='object' || empty($cookie->username) || empty($cookie->password)) {
				return false;
			}
			
			$corporateAccount = authenticateCorporateAccount($cookie->username);
			if ($corporateAccount && $cookie->password==$corporateAccount->password) {
				// Unset the pass and salt - they are not needed anymore
				$corporateAccount->password = '';
				$corporateAccount->salt = '';
				
				$corporateAccount->login();
				return $corporateAccount;
				
			} else {
				return false;
			}
		}
		
		public function serialize() {
			$_SESSION['s_venezvite_corporate'] = serialize($this); // TO BE REMOVED AFTER v2 SETUP
			$_SESSION['s_venezvite']['corporate'] = serialize($this);
		}
		
		public function logout() {
			if (!empty($_SESSION['s_venezvite_corporate'])) { // TO BE REMOVED AFTER v2 SETUP
				unset($_SESSION['s_venezvite_corporate']);
			}
			if (!empty($_SESSION['s_venezvite']['corporate'])) {
				unset($_SESSION['s_venezvite']['corporate']);
			}
			
			if (!empty($_COOKIE['c_venezvite_corporate'])) {
				setcookie('c_venezvite_corporate', false, (time() - 3600 * 25), '/');
			}
		}
	}
