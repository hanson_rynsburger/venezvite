<?php
    class cuisineType {
        public $idCuisineType;
        public $cuisineType;
        public $showInFilter;
        
        public $restaurants;
        
        function __construct(){
            $this->__constructStatic();
        }
        
        private static function __constructStatic(){
            require_once REL . VERSION_REL_PATH . 'db' . DS . 'cuisineType.php';
        }
        
        public static function list_($restaurant = null, $onlyUsed = false){
            self::__constructStatic();
            
            return listCuisineTypes($restaurant, $onlyUsed);
        }
        
        public static function getByID($idCuisineType){
            self::__constructStatic();
            
            return getCuisineTypeByID($idCuisineType);
        }
        
        /**
         * List the cuisine types marked to be displayed in the restaurants filter field
         * (initially, in the restaurants search page)
         * 
         **/
        /*
        public static function listForFilter(){
            self::__constructStatic();
            
            return listCuisineTypesForFilter();
        }
        */
    }
