<?php
	class menuItem {
		public $idMenuItem;
		public $menu;
		public $menuCategory;
		public $menuItemName;
		public $photo;
		public $menuItemDescription;
		public $spicy;
		public $vegetarian;
		public $glutenFree;
		public $lactoseFree;
		public $recommended;
		public $price;
		public $discount;
		public $order;
		public $enabled;
		
		public $menuItemCategories = array();
		public $menuItemGroups = array(); // TEMPORARY, MUST REPLACE THE OLD $menuItemCategories
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'menuItem.php';
		}
		
		public static function list_($restaurant, $menu = false, $all = false, $categories = false, $sort = false){
			self::__constructStatic();
			
			return listMenuItems($restaurant, $menu, $all, $categories, $sort);
		}
		
		public static function insert($menu, $menuCategory, $menuItemName, $photo, $menuItemDescription, $spicy, $vegetarian, $glutenFree, $lactoseFree, $recommended, $price, $discount, $enabled, $menu_option_groups = null) {
			self::__constructStatic();
			
			return insertMenuItem($menu, $menuCategory, $menuItemName, $photo, $menuItemDescription, $spicy, $vegetarian, $glutenFree, $lactoseFree, $recommended, $price, $discount, $enabled, $menu_option_groups);
		}
		
		public static function getByID($idMenuItem){
			self::__constructStatic();
			
			return getMenuItemByID($idMenuItem);
		}
		
		public function update($menuCategory, $menuItemName, $photo, $menuItemDescription, $spicy, $vegetarian, $glutenFree, $lactoseFree, $recommended, $price, $discount, $enabled, $menu_option_groups = null) {
			updateMenuItem($this, $menuCategory, $menuItemName, $photo, $menuItemDescription, $spicy, $vegetarian, $glutenFree, $lactoseFree, $recommended, $price, $discount, $enabled, $menu_option_groups);
		}
		
		public function changeState($enable) {
			changeMenuItemState($this, $enable);
		}
		
		public function delete() {
			if ($this->photo) {
				@unlink(REL . '/i/restaurants/' . $this->photo);
			}
			
			deleteMenuItem($this);
		}
		
		public function reorder($order) {
			reorderMenuItem($this, $order);
		}
		
		public function removeOptionCategories() {
			removeMenuItemOptionCategories($this);
		}
		/*
		public function addOption($menuItemOption){
			addMenuItemOption($this, $menuItemOption);
		}
		*/
		/**
		 * Will return the price according to the currently set discount percentage
		 * (if any)
		 **/
		public function getPrice(){
			return round($this->price * ((100 - $this->discount) / 100), 2);
		}
		
		
		
		public function getSpecsList() {
			$menu_item_specs = array();
			if ($this->spicy=='Y') {
				$menu_item_specs['spicy'] = 'Spicy';
			}
			if ($this->vegetarian=='Y') {
				$menu_item_specs['veggie'] = 'Vegetarian';
			}
			if ($this->glutenFree=='Y') {
				$menu_item_specs['glut-free'] = 'Gluten Free';
			}
			if ($this->lactoseFree=='Y') {
				$menu_item_specs['lact-free'] = 'Lactose Free';
			}
			if (!empty($this->photo)) {
				$menu_item_specs['photos'] = 'Photos';
			}
			
			$html = '';
			foreach ($menu_item_specs as $class => $name) {
				$html .= '<span class="food-type ' . $class . '" title="' . $name . '"></span>';
			}
			
			return array(
				'classes'	=> array_keys($menu_item_specs), 
				'html'		=> $html
			);
		}
		
		public function getDetails() {
			$details = array(
				'photo'				=> $this->photo, 
				'name'				=> $this->menuItemName, 
				'description'		=> $this->menuItemDescription, 
				'price'				=> number_format($this->price, 2), 
				'option_categories'	=> array()
			);
			
			foreach ($this->menuItemGroups as $menu_item_group) {
				$option_category = array(
					'name'			=> $menu_item_group->menuItemCategory, 
					'selectables'	=> $menu_item_group->selectables, 
					'rule'			=> $menu_item_group->selectablesRule, 
					'options'		=> array()
				);
				
				foreach ($menu_item_group->menuItemOptions as $option) {
					$option_category['options'][] = array(
						'id'	=> $option->idMenuItemOption, 
						'name'	=> $option->menuItemOption,
						'price'	=> number_format($option->price, 2)
					);
				}
				$details['option_categories'][] = $option_category;
			}
			
			return json_encode($details);
		}
	}
