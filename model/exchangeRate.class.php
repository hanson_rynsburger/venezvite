<?php
	class exchangeRate {
		public $idExchangeRate;
		public $chf;
		public $eur;
		public $usd;
		public $dateAdded;
		
		function __construct(){
			$this->__constructStatic();
		}
		
		private static function __constructStatic(){
			require_once REL . VERSION_REL_PATH . 'db' . DS . 'exchangeRate.php';
		}
		
		public static function insert($usd, $eur = 1){
			self::__constructStatic();
			
			return insertExchangeRate($usd, $eur);
		}
		
		public static function getLatest(){
			self::__constructStatic();
			
			return getLatestExchangeRate();
		}
		
		public function get_from_matrix($restaurant_currency, $submitted_currency) {
			// $this contains 3 exchange rates based on CHF as default currency
			// We now need to return the correct multiplier, based on the restaurant's currency
			// and the currency submitted by the user when requesting to pay
			
			$restaurant_exchange_rate_to_chf = $this->$restaurant_currency;
			$submitted_exchange_rate_to_chf = $this->$submitted_currency;
			
			return ((float)$restaurant_exchange_rate_to_chf * (float)$submitted_exchange_rate_to_chf);
		}
	}
