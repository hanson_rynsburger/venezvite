<?php
	class SoapClientHMAC extends SoapClient {
		public function __doRequest($request, $location, $action, $version, $one_way = NULL) {
			global $context;
			
			$hmackey = PAYEEZY_HMAC_KEY;
			$keyid = PAYEEZY_KEY_ID;
			$hashtime = date('c');
			$hashstr = "POST\ntext/xml; charset=utf-8\n" . sha1($request) . "\n" . $hashtime . "\n" . parse_url($location, PHP_URL_PATH);
			$authstr = base64_encode(hash_hmac('sha1', $hashstr, $hmackey, true));
			
			if (version_compare(PHP_VERSION, '5.3.11') == -1) {
				ini_set('user_agent', 'PHP-SOAP/' . PHP_VERSION . "\r\nAuthorization: GGE4_API " . $keyid . ':' . $authstr . "\r\nx-gge4-date: " . $hashtime . "\r\nx-gge4-content-sha1: " . sha1($request));
			} else {
				stream_context_set_option($context, array(
						'http' => array(
							'header' => 'authorization: GGE4_API ' . $keyid . ':' . $authstr . "\r\nx-gge4-date: " . $hashtime . "\r\nx-gge4-content-sha1: " . sha1($request)
						)
					));
			}
			
			try {
				return parent::__doRequest($request, $location, $action, $version, $one_way);
			} catch (SoapFault $e) {
				echo $e->getMessage();
			} catch (Exception $e) {
				throw $e;
			}
		}
		
		public function SoapClientHMAC($wsdl, $options = NULL) {
			global $context;
			
			$context = stream_context_create();
			$options['stream_context'] = $context;
			
			try {
				return parent::SoapClient($wsdl, $options);
			} catch (SoapFault $e) {
				echo $e->getMessage();
			} catch (Exception $e) {
				throw $e;
			}
		}
	}
