<?php
    class db {
        public $name = DB_NAME;
        public $user = DB_USER;
        public $pass = DB_PASS;
        
        public $debug = DEBUG;
        public $link;
        
        function __construct($utf = true){
            $this->connect($utf);
        }
        
        function connect($utf = false){
            if(!empty($this->link)){
                $this->disconnect();
            }
            
            $this->link = mysql_connect('myd5-32.infomaniak.ch', $this->user, $this->pass) or die($this->debug ? mysql_error() : '');
        	mysql_select_db($this->name) or die($this->debug ? mysql_error() : '');
            
            if($utf){
                mysql_query('SET NAMES \'utf8\';');
            }
            
            // Set the default time zone as +0200 UTC (Switzerland / Zurich)
            mysql_query('SET time_zone = \'+2:00\';'); // Europe/Zurich (IT DOESN'T WORK ONLINE)
        }
        
        function disconnect(){
            mysql_close($this->link);
            unset($this->link);
        }
    }
